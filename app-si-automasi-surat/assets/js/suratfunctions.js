	//SET NILAI DEFAULT KE SELECT2		
	function set_default_value_select2(data, element_selected)
	{
		// Fetch the preselected item, and add to the control
		var data = JSON.parse(data);
		var newOption = new Option(data.text, data.id, false, false);
		$(element_selected).append(newOption).trigger('change');

	    // manually trigger the `select2:select` event
	    $(element_selected).trigger({
	        type: 'select2:select',
	        params: {
	            data: data
	        }
	    });
	}	
	//usage : set_default_value_select2('{"id":"34043", "text": "KAB. SLEMAN"}', '#ajax-select-kota-tujuan');	


	function set_tags_select2(options_data, element_selected)
	{
		var select = $(element_selected);
		var parseddata = JSON.parse(options_data);		
        var newOptions = parseddata;

		$('option', select).remove();
		$.each(newOptions, function(text, key) {
		    var option = new Option(key, text, true, true);
		    select.append($(option));
		});
	}
	//usage : set_tags_select2('{"11650021#0#MHS01#MAHASISWA#MUKHLAS IMAM M" : "11650021 - MUKHLAS IMAM M"}', '#daftar-mhs');