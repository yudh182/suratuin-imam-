<div class="col-med-9">
	<div class="content-space">
		<ul id="crumbs">
			<li>
				<a title="Automasi Surat Mahasiswa" href="">Automasi Persuratan</a>
			</li>
			<li>
				<a title="Buat" href="">Buat</a>
			</li>
		</ul><br/>

		<div class="home-container" style="margin: 10px 0 150px 0;">
			<h2 class="h-border">Automasi Persuratan Mahasiswa (laman pegawai)</h2>
			<div class="row">
				<div class="col-md-3">
					<div class="small-box bg-emas">
			            <div class="inner">
			            	<h3>0</h3>
			            	<h4>Automasi Buat Surat</h4>
			              	<!--<p>Surat Keterangan Fakultas</p>-->
			            </div>
			            <div class="icon">
			              <i class="ion ion-pie-graph"></i>
			            </div>
			            <a href="<?= site_url('modul_surat_akademik/admin_master_surat_akademik/sub_jenis_surat'); ?>" class="small-box-footer">
			              buat <i class="fa fa-edit"></i>
			            </a>
			        </div>
				</div>
				<div class="col-md-3">
					<div class="small-box bg-emas">
			            <div class="inner">
			            	<h3>0</h3>
			            	<h4>Pengajuan Terbaru</h4>			              
			            </div>
			            <div class="icon">
			              <i class="ion ion-pie-graph"></i>
			            </div>
			            <a href="#" class="small-box-footer">
			              kelola <i class="fa fa-hand"></i>
			            </a>
			        </div>
				</div>
				<div class="col-md-3">
					<div class="small-box bg-emas">
			            <div class="inner">
			            	<h3>0</h3>
			             	<h4>Telah disahkan dan diarsipkan</h4>              
			            </div>
			            <div class="icon">
			              <i class="ion ion-pie-graph"></i>
			            </div>
			            <a href="#" class="small-box-footer">
			              buka laman <i class="fa fa-archive"></i>
			            </a>
			        </div>
				</div>
				<div class="col-md-3">
					<div class="small-box bg-emas">
			            <div class="inner">
			            	<h3>0</h3>
			            	<h4>Paraf dan Penandatanganan</h4>	
			              <p>Surat Keterangan Fakultas</p>
			            </div>
			            <div class="icon">
			              <i class="ion ion-pie-graph"></i>
			            </div>
			            <a href="#" class="small-box-footer">
			              buka laman <i class="fa fa-certificate"></i>
			            </a>
			        </div>
				</div>				
			</div>
		</div>

	</div>
</div>	