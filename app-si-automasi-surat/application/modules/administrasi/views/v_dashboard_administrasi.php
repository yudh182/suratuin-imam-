<div class="col-med-9">
	<div class="content-space">
		<ul id="crumbs">			
			<li>
				<a title="Automasi Surat Mahasiswa" href="">Administrasi Persuratan Mahasiswa</a>
			</li>			
		</ul><br>

		<div class="home-container" style="margin: 10px 0 150px 0;">
			<h2 class="h-border">Dashboard Administrasi Surat</h2>
			<div class="row">
				
				<div class="col-md-4">
					<div class="small-box bg-emas">
			            <div class="inner">
			            	<h4>Terima Pengajuan Surat</h4>
			            	<p>Terima dan proses surat yang diajukan mahasiswa</p>
			            </div>
			            <!-- <div class="icon">
			              <i class="fa fa-eye fa-2x"></i>
			            </div> -->
			            <a href="<?= site_url('administrasi/terima_pengajuan'); ?>" class="small-box-footer">Buka halaman</i>
			            </a>
			        </div>
				</div>
				<div class="col-md-3">
					<div class="small-box bg-emas">
			            <div class="inner">			            	
			             	<h4>Surat diterbitkan</h4>
			             	<p>Lihat surat-surat yang diterbitkan mahasiswa melalui aplikasi sistem automasi surat</p>
			            </div>
			            <div class="icon">
			              <i class="ion ion-pie-graph"></i>
			            </div>
			            <a href="<?= site_url('administrasi/arsip'); ?>" class="small-box-footer">
			              buka halaman</i>
			            </a>
			        </div>
				</div>
				<!-- <div class="col-md-3">
					<div class="small-box bg-emas">
			            <div class="inner">
			            	<h3>2</h3>
			            	<h4>Pengajuan Ditolak</h4>	
			              
			            </div>
			            <div class="icon">
			              <i class="ion ion-pie-graph"></i>
			            </div>
			            <a href="#" class="small-box-footer">
			              buka laman <i class="fa fa-certificate"></i>
			            </a>
			        </div>
				</div>		 -->		
			</div>
		</div>

	</div>
</div>