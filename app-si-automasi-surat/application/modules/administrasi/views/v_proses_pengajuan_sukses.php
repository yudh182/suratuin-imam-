<div class="col-md-12">
    <div class="bs-callout">
      <div class="inner">
        <?php if (isset($success)) : ?>
        <div class="alert alert-success">
            <h4><?= $success[0]; ?></h4>
        </div>

        <table class="table table-striped">
            <tbody>
                <tr>
                    <td>Nomor Surat</td>
                    <td><?= $surat_keluar['NO_SURAT']; ?></td>
                </tr>
                <tr>
                    
                    <td>Waktu Penyelesaian</td>
                    <td><?php echo (!empty($surat_keluar['WAKTU_PERSETUJUAN']) ? $surat_keluar['WAKTU_PERSETUJUAN'] : '-'); ?></td>
                </tr>
                <tr>
                    <td>Diproses oleh</td>
                   <td><?php echo $this->session->userdata('nama'); ?></td>
                </tr>
                <tr>
                    <td>Pemohon Pengesahan Surat</td>
                   <td><?php echo (!empty($surat_keluar['PEMBUAT_SURAT']) ? $surat_keluar['PEMBUAT_SURAT'] : '-'); ?></td>
                </tr>
            </tbody>
        </table>           
        <hr>
    <?php endif; ?>

    <?php if (isset($errors)) : ?>
        <div class="alert alert-success">
            <h4>Surat gagal diproses / diperbarui</h4>
        </div>
    <?php endif; ?>
     </div>			                    
        
    </div>
</div>