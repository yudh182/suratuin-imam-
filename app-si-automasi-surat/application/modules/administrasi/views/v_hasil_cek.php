<?php if (!empty($surat_keluar) && $surat_keluar['KD_STATUS_SIMPAN'] == '3'){
	echo '<div class="alert alert-warning">Surat ini telah disahkan sebelumnya</div>';
} elseif (!empty($surat_keluar) && $surat_keluar['KD_STATUS_SIMPAN'] == '0') {
	echo '<div class="alert alert-danger">Surat ini berstatus dibatalkan</div>';

} else { ?>
	<div class="bs-callout bs-callout-warning">
		<table>
			<tr>
				<td>Status Surat</td>
				<td>: Bernomor</td>
			</tr>
			<tr>
				<td>Waktu Pembuatan</td>
				<td>: <?php  echo (isset($surat_keluar['WAKTU_SIMPAN']) ? $surat_keluar['WAKTU_SIMPAN'] : ''); ?></td>
			</tr>
		</table>
	</div>
<div class="bs-callout">
	<div class="row">
	<iframe width="700" height="400" src="<?php echo $link_pdf; ?>">		
	</iframe>
			<br>
			<form id="administrasi-surat">
				<input type="hidden" name="id_surat_administrasi" value="<?php  echo (isset($surat_keluar['WAKTU_SIMPAN']) ? $surat_keluar['ID_SURAT_KELUAR'] : ''); ?>">
			<div class="pull-left"><button id="batalkan" class="btn btn-danger">Tolak</button></div>
			<div class="pull-right"><button id="finalkan" class="btn btn-success">Proses dan Sahkan</button></div>
			</form>		
</div>
</div>
<div id="hasil-aksi"></div>

<script type="text/javascript">
	// $("form" ).on( "submit", function( event ) {
	// 	event.preventDefault();
	// });
	
	$(document).ready(function() {
		$('#finalkan').click(function(e){
			e.preventDefault();
			var me = $('form#administrasi-surat');
			var val_id_surat = $("input[name=id_surat_administrasi]").val();			
			
			var request = $.ajax({
				url: "<?php echo site_url('mhs_surat_keluar/kelola/set_final'); ?>",
				type: 'POST',
				cache: false,
				// data: me.serialize(),
				data: {id_surat_keluar: val_id_surat},
				dataType: 'html',
				//dataType: 'json', //format data balikan
				beforeSend: function() {
				  // $('#btn_simpan_sesi').text('proses menyimpan...');	
				   $('#loading1').show();
				   setTimeout(function () {
				        $("#loading1").hide();
				        //form.submit();
				   }, 3000); // in milliseconds
				 }
			});

			request.done(function( msg ) {				
				//$('#btn_simpan_sesi').text('perbarui');
				$('#loading1').hide();

				$('#hasil-aksi').html(msg);
				// $('#next-content').html(
				// 	'<iframe src="'+msg+'"></iframe>'
				// );
			});

			request.fail(function( jqXHR, textStatus ) {
				console.log(jqXHR);				
				$('#loading1').hide();


				if (jqXHR.status==422) { //unprocessable
					$('#hasil-aksi').html(jqXHR.responseText);
					//$('#btn_simpan_sesi').text('simpan');

				} else if (jqXHR.status==501){ //Not Implemented (internal server error)
					$('#hasil-aksi').html(jqXHR.responseText);
				}
			});
		});

		$('#batalkan').click(function(e){
			e.preventDefault();
			var me = $('form#administrasi-surat');
			var val_id_surat = $("input[name=id_surat_administrasi]").val();			
			
			var request = $.ajax({
				url: "<?php echo site_url('mhs_surat_keluar/kelola/set_batal'); ?>",
				type: 'POST',
				cache: false,
				// data: me.serialize(),
				data: {id_surat_keluar: val_id_surat},
				dataType: 'html',
				//dataType: 'json', //format data balikan
				beforeSend: function() {
				  // $('#btn_simpan_sesi').text('proses menyimpan...');	
				   $('#loading1').show();
				   setTimeout(function () {
				        $("#loading1").hide();
				        //form.submit();
				   }, 3000); // in milliseconds
				 }
			});

			request.done(function( msg ) {				
				//$('#btn_simpan_sesi').text('perbarui');
				$('#loading1').hide();

				$('#hasil-aksi').html(msg);
				// $('#next-content').html(
				// 	'<iframe src="'+msg+'"></iframe>'
				// );
			});

			request.fail(function( jqXHR, textStatus ) {
				console.log(jqXHR);				
				$('#loading1').hide();


				if (jqXHR.status==422) { //unprocessable
					$('#hasil-aksi').html(jqXHR.responseText);
					//$('#btn_simpan_sesi').text('simpan');

				} else if (jqXHR.status==501){ //Not Implemented (internal server error)
					$('#hasil-aksi').html(jqXHR.responseText);
				}
			});
		});
});
</script>

<?php } ?>