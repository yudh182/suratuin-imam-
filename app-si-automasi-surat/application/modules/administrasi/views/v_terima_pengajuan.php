<div class="col-md-9">
<div class="content-space">
	<ul id="crumbs">
			<li>
				<a title="Administrasi TU" href="#">Administrasi</a>
			</li>
			<li>
				<a title="Verifikasi pengajuan surat" href="#">Verifikasi pengajuan</a>
			</li>			
	</ul>

	<div class="box border-left border-right" style="margin-top: 20px;">
		<div class="box-header">
			<h3>Terima Pengajuan Surat</h3>	
		</div>
		<div class="box-body">
			<div class="container-isi-form">
				<form method="POST" id="form_verifikasi_pengajuan" action="<?php echo base_url(); ?>administrasi/cek_pengajuan" role="form" class="form-horizontal">
					<!-- <div class="form-group">
						<label for="keperluan" class="col-md-3 col-sm-4 control-label">Opsi</label>
						<div class="col-md-9 col-sm-12">
							<select class="form-control" name="opsi_cari">			
								<option value="1" selected>1. Scan QR-Code</option>
								<option value="2">2. Cari dengan NIM</option>
							</select>
						</div>
					</div> -->
					
					<div class="form-group" id="group-scan-qrcode">
						<div class="col-md-3">
							<select class="form-control">
								<option value="urlqr" selected="">Scan Kode QR</option>
								<option value="id">ID Surat</option>
							</select>	
						</div>
						<div class="col-md-9">							
								<input type="text" name="kunci_cari" id="kunci_cari" placeholder="Inputkan data" value="" class="form-control" />							
						</div>
											
					</div>

					<!-- <div class="form-group hide" id="group-cari-nim">
						<label for="keperluan" class="col-md-3 col-sm-4 control-label">Cari dengan NIM</label>
						<div class="col-md-9 col-sm-8">
							<input type="text" name="kunci_cari" id="kunci_cari" placeholder="Inputkan NIM" value="" class="form-control" />
						</div>
					</div> -->



					<div class="form-group">
						<div class="col-md-3">
							<input type="hidden" name="kd_pegawai" value="" />
							<input type="hidden" name="cek_pengajuan" value="ok" />	
						</div>
						<div class="col-md-9">							
							<button type="submit" style="margin: 10px 0px" class="btn btn-inverse btn-medium btn-uin" name="btn-cek-pengajuan" id="btn-cek-pengajuan">Cek Pengajuan Surat</button>				
						</div>
						
					</div>

				</form>
			</div>
		</div>
		<div class="overlay" style="visibility: hidden;">
		  <i class="fa fa-refresh fa-spin"></i>
		</div>
	</div>
	<div class="clearfix"></div>

	

	<div id="loadingbar" style="display: none;">
		<center><img src="http://surat.uin-suka.ac.id/asset/img/ajax-loading.gif"><br />Harap menunggu<br>Sedang memverifikasi pengajuan surat</center>
	</div>

	<div id="container-cek-surat">
		<div class="box border-left border-right" style="margin-top: 20px;">
			<div class="box-header">
				<h3>Hasil Cek</h3>	
			</div>
			<div class="box-body"></div>
		</div>
	</div>

	<div class="clearfix"></div>

</div></div><!-- closinng wrapper content -->

<script type="text/javascript">
	$("form" ).on( "submit", function( event ) {
		event.preventDefault();
	});

	$(document).ready(function() {

		$('form#form_verifikasi_pengajuan').submit(function(e){
			var me = $(this);
			//alert('Action URL : '+me.attr('action')+', DATA Serialize :'+me.serialize());
			
			var request = $.ajax({
				url: me.attr('action'),
				type: 'POST',
				cache: false,
				data: me.serialize(),
				dataType: 'html',//dataType: 'json', //format data balikan			
				beforeSend: function() {
					state_cek_ajax(); //set UI ke keadaan progress ajax		
				}
			});

			request.done(function( msg, textStatus, jqXHR) {
				console.log(jqXHR);
				state_normal(); //kembalikan UI ke keadaan normal
				// // $('[name="id_surat"]').val(jqXHR.getResponseHeader('X-ID-SESI'));
				$('#container-cek-surat').show();
				$('#container-cek-surat .box-body').html(msg);
				// state_normal();

				// $('.form-group').removeClass('has-error')
				// 				.removeClass('has-success');
				// $('.text-danger').remove();
								
				// $('#loadingbar').hide();
			});

			request.fail(function( jqXHR, textStatus ) {
				console.log(jqXHR);				
				state_normal(); //kembalikan UI ke keadaan normal

				if (jqXHR.status==422) { //unprocessable (form input belum diisi dengan benar)					
					// if ($('[name=id_sesi_buat]') !== 'new'){
					// 	alert("tak boleh kosong");
					// } 
					
					//$('#result-aksi').html(jqXHR.responseText);					
					 				
					// $.each(jqXHR.responseJSON.messages, function(key, value) {
					// 	var element = $('#' + key);
						
					// 	element.closest('div.form-group')
					// 		.removeClass('has-error')
					// 		.addClass(value.length > 0 ? 'has-error' : 'has-success')
					// 		.find('.text-danger').remove();
						
					// 	element.after(value);						
					// });
					// $('#btn_simpan_sesi').text('simpan');

				} else if (jqXHR.status==501){ //Not Implemented (internal server error)
					$('#result-aksi').html(jqXHR.responseText);
				} else {
					$('#next-content').html(jqXHR.responseText);
				}
			});
		});

	});

	function state_cek_ajax(){
		$('#btn-cek-pengajuan').addClass('disabled');
		$('#btn-cek-pengajuan').text('mengecek surat..');
		//$('#loadingbar').show();
	}

	function state_normal(){
		$('#btn-cek-pengajuan').removeClass('disabled');
		$('#btn-cek-pengajuan').text('Cek Pengajuan Surat');
		$('#loadingbar').hide();
	}
</script>