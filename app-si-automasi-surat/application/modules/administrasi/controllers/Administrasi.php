<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');

/**
* Diakses oleh Pegawai TU masing-masing unit fakultas
* @author Mukhlas Imam Muhajir <muhajiros.space@gmail.com>
*/
class Administrasi extends MX_Controller{
	public function
	 __construct()
	{
		parent::__construct();
		$this->load->helper('common/Trans_general');
		$this->load->module('repo/Api_foto','api_foto');
		$this->load->model('common/Mdl_tnde','apiconn');
	}

	public function index()
	{
		$view = 'v_dashboard_administrasi';

		$this->load->view('common/layout-pgw/v_header');
		$this->load->view('common/layout-pgw/v_sidebar');
		$this->load->view($view);
		$this->load->view('common/layout-pgw/v_footer');
	}

	public function terima_pengajuan()
	{		
		$view = 'v_terima_pengajuan';
		$this->load->view('common/layout-pgw/v_header');
		$this->load->view('common/layout-pgw/v_sidebar');
		$this->load->view($view);

		$this->load->view('common/layout-pgw/v_footer');
	}


	public function arsip()
	{	$get_skr =  $this->api_client_tnde->get_surat_keluar();
		exit(json_encode($get_skr));
		$view = 'v_arsip_surat_unit';
		$this->load->view('common/layout-pgw/v_header');
		$this->load->view('common/layout-pgw/v_sidebar');
		$this->load->view($view);
		$this->load->view('common/layout-pgw/v_footer');
	}

	public function cari_pengajuan()
	{
		$nim = $this->input->post('nim');


	}

	/**
	 * Cek surat yang diajukan oleh mahasiswa
	 */
	public function cek_pengajuan()
	{			
		//cek incoming $_POST		
		$qrcode = $this->input->post('kunci_cari');
		
		if (!empty($qrcode)){
			$parts = parse_url($qrcode);
			parse_str($parts['query'], $query);

			$id_surat = $query["id_surat"];

			$parameter = array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('MD_SURAT_KELUAR2','ID_SURAT_KELUAR', $id_surat));
			$data['surat_keluar'] = $this->apiconn->api_tnde('tnde_general/get_data', 'json', 'POST', $parameter);	
			$data['link_pdf'] = site_url("mhs_surat_keluar/cetak_surat_tersimpan/").$query["id_surat"];

			$view_hasil_cek = $this->load->view('v_hasil_cek', $data, true);

			// exit(json_encode($data));

			// $html = '<iframe width="700" height="400" src="'.site_url("mhs_surat_keluar/cetak_surat_tersimpan/").$query["id_surat"].'"></iframe>
			// <br>
			// <form>
			// <div class="pull-left"><button class="btn btn-danger">Tolak</button></div>
			// <div class="pull-right"><button class="btn btn-success">Proses dan Sahkan</button></div>
			// ';

			// $html .='
			// <div class="row">
			// 	<div class="col-md-6">
			// 		<div class="small-box">
		 //            	<div class="inner">				         
		 //            		<h4>Dibuat pada :</h4>		            	
		 //            		<h5>9 mei 2018 13:15:21</h5>	
		 //            	<hr>
		 //            </div>			                    		            
		 //        </div>
			// </div>';

			$this->output
				->set_status_header(200)
				->set_content_type('text/html')
				->set_output($view_hasil_cek);
				// ->set_output($html);
		} else {
			$this->output
			->set_status_header(422)
			->set_content_type('text/html')
			->set_output("<h4>tidak dapat melakukan pengecekan atau pencarian</h4>");
		}
	}

	public function test_encode($username='')
	{
		$this->load->library('encryption');
		$this->encryption->initialize(
	        array(
	        		'driver' => 'openssl',	        		
	                'cipher' => 'aes-256',
	                'mode' => 'ctr',
	                'key' => '<a 32-character random string>'
	        )
		);
		$enc_username=$this->encryption->encrypt($username);
		echo $enc_username;
	}

	/**
	* Siganture keaslian surat
	*/
	public function sigsurat()
	{
		$data_surat = [
			'id_surat_keluar' => 'T.001',
			'unit_id' => 'UN02006',
			'pembuat_surat' => '11650021',			
		];
		$string = json_encode($data_surat);

		$secret = 'kunci-rahasia';
		$sig = hash_hmac('sha256', $string, $secret);
		$base64sig = base64_encode($sig); //hash diencode
		echo 'keyedhash: '.$sig;
		echo '<br>';
		echo 'Encoded keyedhash: '.$base64sig;
		echo '<br>';
		echo 'Panjang karakter: '.strlen($base64sig);
		echo '<hr>';
		echo 'surat dibuat oleh: '.strlen($base64sig);

	}

	public function generate()
	{
		$this->load->library('encryption');
		//$this->encryption->initialize(array('driver' => 'openssl'));

		// Get a hex-encoded representation of the key:
		$key = bin2hex($this->encryption->create_key(16));
		$base64_safe_url = strtr(base64_encode($key), '+/=', '-_,');
		echo $key;
	}
	
	/**
	 * simple method to encrypt or decrypt a plain text string
	 * initialization vector(IV) has to be the same when encrypting and decrypting
	 * 
	 * @param string $action: can be 'encrypt' or 'decrypt'
	 * @param string $string: string to encrypt or decrypt
	 *
	 * @return string
	 */
	function encrypt_decrypt($action, $string) {
	    $output = false;
	    $encrypt_method = "AES-256-CBC";
	    $secret_key = 'This is my secret key';
	    $secret_iv = 'This is my secret iv';
	    // hash
	    $key = hash('sha256', $secret_key);
	    
	    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
	    $iv = substr(hash('sha256', $secret_iv), 0, 16);
	    if ( $action == 'encrypt' ) {
	        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
	        $output = base64_encode($output);
	    } else if( $action == 'decrypt' ) {
	        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
	    }
	    return $output;
	}

	function test()
	{
		$plain_txt = "This is my plain text";
		echo "Plain Text =" .$plain_txt. "\n";
		$encrypted_txt = $this->encrypt_decrypt('encrypt', $plain_txt);
		echo "Encrypted Text = " .$encrypted_txt. "\n";
		$decrypted_txt = $this->encrypt_decrypt('decrypt', $encrypted_txt);
		echo "Decrypted Text =" .$decrypted_txt. "\n";
		if ( $plain_txt === $decrypted_txt ) echo "SUCCESS";
		else echo "FAILED";
		echo "\n";
	}	

	public function proses_pengajuan()
	{}

	public function tolak_pengajuan()
	{}	

	/**
	* Menunda persetujuan dengan mengirimkan catatan ke pemohon dan atau pegawai pelaksana (TU) terkait
	*/
	public function aksi_tunda()
	{}



	public function test_email()
	{
		$id_surat_keluar = $this->input->get('id');

		echo json_encode( $this->api_client_tnde->psd_email($id_surat_keluar) );
	}

}