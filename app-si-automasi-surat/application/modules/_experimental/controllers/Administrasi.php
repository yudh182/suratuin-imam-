<?php
/* administrasi surat
 * role: Pegawai TU
 */
 
 class Administrasi extends MX_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->module('psd_elektronik/Api_client_tnde', 'api_client_tnde');
		$this->load->model('common/Api_simpeg', 'api_client_simpeg');
	}
	
	public function foto_profil($kd_pgw)
	{
		$this->load->module('common/Api_foto','api_foto');
		//echo '<img src="'.$this->api_foto->foto_pgw($kd_pgw).'" />';
		echo $this->api_foto->foto_pgw($kd_pgw);
	}
	public function terima_pengajuan($id_surat_keluar=null)
	{	
		$id_surat_keluar = isset($id_surat_keluar) ? $id_surat_keluar : null;
		$data['surat_keluar'] = $this->api_client_tnde->detail_surat_keluar($id_surat_keluar);
		$data['penerima_skr'] = $this->api_client_tnde->get_penerima_sk($id_surat_keluar);

		echo json_encode($data);
	}
	
	
	public function arsip_surat_keluar()
	{
	}

	/*
	 * 1) Cek detail surat berdasarkan ID 2) cek apakah pegawai TU adalah yang berwenang
	 * type ACTION
	 */
	public function cek_pengajuan()
	{
		$par_id = $this->input->post('id_surat_keluar');
		$par_pgw_tu = $this->session->userdata('username');
		
		$surat_keluar = $this->api_client_tnde->detail_surat_keluar($par_id);
		$jab_pgw_tu = $this->api_client_simpeg->get_jabatan_struktural($par_pgw_tu);
				
	}
		
	/**
	 * Finalkan Surat secara langsung oleh TU, tanpa psd via email
	 * Ubah kolom-kolom: STATUS_SIMPAN=1, TGL_PERSETUJUAN=date('d/m/Y h:i:s')	 
	 * type ACTION
	 */
	public function finalkan_surat()
	{
	}
	
	/**
	 * Batalkan Surat
	 * Ubah kolom-kolom: STATUS_SIMPAN=0, TGL_DITOLAK=date('d/m/Y h:i:s')
	 * type ACTION
	 */
	public function batalkan_surat()
	{
	}
	
 }