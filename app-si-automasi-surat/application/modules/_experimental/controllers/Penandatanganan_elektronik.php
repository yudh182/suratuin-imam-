<?php
/* Mahasiswa mengirim permohonan tte
 * Penerbitan surat bertandatanagan elektronik
 * role: mahasiswa
 */
 
 class Penandatanganan_elektronik extends MX_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->module('apiconsumer/Api_client_tnde', 'api_client_tnde');
		$this->load->model('common/Api_simpeg', 'api_client_simpeg');
	}
	
	public function ajukan_permohonan()
	{
	
	}


	/*
	 * 1) Cek detail surat berdasarkan ID 2) cek apakah pegawai TU adalah yang berwenang
	 * type ACTION
	 */
	public function monitor_permohonan()
	{
		$par_id = $this->input->post('id_surat_keluar');
		$par_pgw_tu = $this->session->userdata('username');
		
		$surat_keluar = $this->api_client_tnde->detail_surat_keluar($par_id);
		$jab_pgw_tu = $this->api_client_simpeg->get_jabatan_struktural($par_pgw_tu);
				
	}
	
	public function cetak_surat_tersedia()
	{	
	}
	
 }