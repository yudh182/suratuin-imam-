<?php
class Convert_model extends CI_Model {

    function __construct() {
        parent::__construct();    
    }
    function submenu($halaman){
        switch ($halaman) {
            case 'masih_kul' :
                return 'Surat Keterangan Masih Kuliah';
                break;
            case 'tdk_men_bea' :
                return 'Surat Keterangan Tidak Menerima Beasiswa';
                break;
            case 'kel_baik' :
                return 'Surat Keterangan Berkelakuan Baik';
                break;
            case 'bbs_teori' :
                return 'Surat Keterangan Bebas Teori';
                break;
            case 'ket_lulus' :
                return 'Surat Keterangan Lulus';
                break;
            case 'pndh_studi' :
                return 'Surat Keterangan Pindah Studi';
                break;
            case 'ijin_pen' :
                return 'Surat Ijin Penelitian';
                break;
            case 'ijin_studi_pen' :
                return 'Surat Studi Pendahuluan';
                break;
            case 'ijin_obs' :
                return 'Surat Ijin Observasi';
                break;
            case 'perm_KP' :
                return 'Surat Permohonan Kerja Praktik';
                break;
            default :
            return 'cetak_surat';
                break;
            }  
    }
    public function ubah_bulan($mm){
        switch ($mm) {
        case 1 :
            return "Januari";
            break;
        case 2 :
            return "Februari";
            break;
        case 3 :
            return "Maret";
            break;
        case 4 :
            return "April";
            break;
        case 5 :
            return "Mei";
            break;
        case 6 :
            return "Juni";
            break;
        case 7 :
            return "Juli";
            break;
        case 8 :
            return "Agustus";
            break;
        case 9 :
            return "September";
            break;
        case 10 :
            return "Oktober";
            break;
        case 11 :
            return "November";
            break;
        case 12 :
            return "Desember";
            break;
        default :
            return 0;
            break;
        }
    }
    function get_idttd($kd_fak, $kd_sk){
        switch ($kd_fak) {
            case 01:
                if ($kd_sk == 'KM.02.02' || $kd_sk == 'PP.08.1' || $kd_sk == 'PN.01.01' || 
                    $kd_sk == 'PP.01.2'  || $kd_sk == 'PP.05.3' || $kd_sk == 'KM.002'){
                            return 2;
                     }
                
                else{
                    return 5;
                }
                break;
            case 02:
                if ($kd_sk == 'KM.02.02' || $kd_sk == 'PP.08.1' || $kd_sk == 'PN.01.01' || 
                    $kd_sk == 'PP.01.2'  || $kd_sk == 'PP.05.3' || $kd_sk == 'KM.002'){
                            return 7;
                     }
                else{
                    return 10;
                }
                break;
            case 03:
                if ($kd_sk == 'KM.02.02' || $kd_sk == 'PP.08.1' || $kd_sk == 'PN.01.01' || 
                    $kd_sk == 'PP.01.2'  || $kd_sk == 'PP.05.3' || $kd_sk == 'KM.002'){
                            return 12;
                     }
                else{
                    return 15;
                }
                break;
            case 04:
                if ($kd_sk == 'KM.02.02' || $kd_sk == 'PP.08.1' || $kd_sk == 'PN.01.01' || 
                    $kd_sk == 'PP.01.2'  || $kd_sk == 'PP.05.3' || $kd_sk == 'KM.002'){
                            return 17;
                     }
                else{
                    return 20;
                }
                break;
            case 05:
                if ($kd_sk == 'KM.02.02' || $kd_sk == 'PP.08.1' || $kd_sk == 'PN.01.01' || 
                    $kd_sk == 'PP.01.2'  || $kd_sk == 'PP.05.3' || $kd_sk == 'KM.002'){
                            return 22;
                     }
                else{
                    return 25;
                }
                break;
            case 06:
                if ($kd_sk == 'KM.02.02' || $kd_sk == 'PP.08.1' || $kd_sk == 'PN.01.01' || 
                    $kd_sk == 'PP.01.2'  || $kd_sk == 'PP.05.3' || $kd_sk == 'KM.002'){
                            return 27;
                     }
                
                else{
                    return 30;
                }
                break;
            case 07:
                if ($kd_sk == 'KM.02.02' || $kd_sk == 'PP.08.1' || $kd_sk == 'PN.01.01' || 
                    $kd_sk == 'PP.01.2'  || $kd_sk == 'PP.05.3' || $kd_sk == 'KM.002'){
                            return 32;
                     }
                else{
                            return 35;
                }
                break;

          
            default:
                if ($kd_sk == 'KM.02.02' || $kd_sk == 'PP.08.1' || $kd_sk == 'PN.01.01' || 
                    $kd_sk == 'PP.01.2'  || $kd_sk == 'PP.05.3' || $kd_sk == 'KM.002'){
                            return 37;
                     }
                else{
                            return 40;
                }
           
                break;
        }
          
    }
   public function kd_unit($kd_fak){
        switch ($kd_fak) {
            case 01:
                return 'UN02001';
                break;
            case 02:
                return 'UN02002';
                break;
            case 03:
                return 'UN02003';
                break;
            case 04:
                return 'UN02004';
                break;
            case 05:
                return 'UN02005';
                break;
            case 06:
                return 'UN02006';
                break;
            case 07:
                return 'UN02007';
                break;
            default:
                return 'UN02008';
                break;
        }

    }
    public function tlp($kd_fak){
        switch ($kd_fak) {
            case 01:
                return 'Telepon (0274) 513949 Faksimili (0274) 552883';
                break;
            case 02:
                return 'Telepon (0274) 515856 Faksimili (0274) 552230';
                break;
            case 03:
                return 'Telepon (0274) 512840 Faksimili (0274) 545614';
                break;
            case 04:
                return 'Telepon (0274) 513056 Faksimili (0274) 519734';
                break;
            case 05:
                return 'Telepon (0274) 512156 Faksimili (0274) 512156';
                break;
            case 06:
                return 'Telepon (0274) 519739 Faksimili (0274) 540971';
                break;
            case 07:
                return 'Telepon (0274) 585300 Faksimili (0274) 519571';
                break;
            default:
                return 'Telepon (0274) 545411 Faksimili (0274) 586117';
                break;
        }

    }
     public function kd_psd($kd_fak,$kd_sk){
        switch ($kd_fak) {
            case 01:
                if ($kd_sk == 'KM.02.02' || $kd_sk == 'PP.08.1' || $kd_sk == 'PN.01.01' || 
                    $kd_sk == 'PP.01.2'  || $kd_sk == 'PP.05.3' || $kd_sk == 'KM.002'){
                            return '84';
                     }
                
                else{
                    return '71';
                }
                break;
            case 02:
                if ($kd_sk == 'KM.02.02' || $kd_sk == 'PP.08.1' || $kd_sk == 'PN.01.01' || 
                    $kd_sk == 'PP.01.2'  || $kd_sk == 'PP.05.3' || $kd_sk == 'KM.002'){
                            return '87';
                     }
                else{
                    return '72';
                }
                break;
            case 03:
                if ($kd_sk == 'KM.02.02' || $kd_sk == 'PP.08.1' || $kd_sk == 'PN.01.01' || 
                    $kd_sk == 'PP.01.2'  || $kd_sk == 'PP.05.3' || $kd_sk == 'KM.002'){
                            return '90';
                     }
                else{
                    return '73';
                }
                break;
            case 04:
                if ($kd_sk == 'KM.02.02' || $kd_sk == 'PP.08.1' || $kd_sk == 'PN.01.01' || 
                    $kd_sk == 'PP.01.2'  || $kd_sk == 'PP.05.3' || $kd_sk == 'KM.002'){
                            return '93';
                     }
                else{
                    return '74';
                }
                break;
            case 05:
                if ($kd_sk == 'KM.02.02' || $kd_sk == 'PP.08.1' || $kd_sk == 'PN.01.01' || 
                    $kd_sk == 'PP.01.2'  || $kd_sk == 'PP.05.3' || $kd_sk == 'KM.002'){
                            return '96';
                     }
                else{
                    return '75';
                }
                break;
            case 06:
                if ($kd_sk == 'KM.02.02' || $kd_sk == 'PP.08.1' || $kd_sk == 'PN.01.01' || 
                    $kd_sk == 'PP.01.2'  || $kd_sk == 'PP.05.3' || $kd_sk == 'KM.002'){
                            return '99';
                     }
                else{
                    return '76';
                }
                break;
            case 07:
                if ($kd_sk == 'KM.02.02' || $kd_sk == 'PP.08.1' || $kd_sk == 'PN.01.01' || 
                    $kd_sk == 'PP.01.2'  || $kd_sk == 'PP.05.3' || $kd_sk == 'KM.002'){
                            return '102';
                     }
                else{
                            return '77';
                }
                break;

          
            default:
                if ($kd_sk == 'KM.02.02' || $kd_sk == 'PP.08.1' || $kd_sk == 'PN.01.01' || 
                    $kd_sk == 'PP.01.2'  || $kd_sk == 'PP.05.3' || $kd_sk == 'KM.002'){
                            return '105';
                     }
                else{
                            return '78';
                }
           
                break;
        }
    }
    public function web($kd_fak){
        switch ($kd_fak) {
            case 01:
                return 'website: http://adab.uin-suka.ac.id';
                break;
            case 02:
                return 'website: http://dakwah.uin-suka.ac.id';
                break;
            case 03:
                return 'website: http://syariah.uin-suka.ac.id';
                break;
            case 04:
                return 'website: http://tarbiyah.uin-suka.ac.id';
                break;
            case 05:
                return 'website: http://ushuluddin.uin-suka.ac.id';
                break;
            case 06:
                return 'website: http://saintek.uin-suka.ac.id';
                break;
            case 07:
                return 'website: http://fishum.uin-suka.ac.id';
                break;
            default:
                return 'website: http://febi.uin-suka.ac.id';
                break;
        }

    }

    public function get_idjabatan($kd_fak,$kd_sk){
        switch ($kd_fak) {
            case 01:
                if ($kd_sk == 'PP.08.1' || $kd_sk == 'PN.01.01' || 
                    $kd_sk == 'PP.01.2'  || $kd_sk == 'PP.05.3' || $kd_sk == 'KM.002'){
                            return 'ST000307';
                     }
                else if($kd_sk == 'KM.02.02' ){
                            return 'ST000309';
                }
                
                else{
                    return 'ST000182';
                }
                break;
            case 02:
                if ($kd_sk == 'PP.08.1' || $kd_sk == 'PN.01.01' || 
                    $kd_sk == 'PP.01.2'  || $kd_sk == 'PP.05.3' || $kd_sk == 'KM.002'){
                            return 'ST000330';
                     }
                else if($kd_sk == 'KM.02.02' ){
                            return 'ST000332';
                }
                else{
                    return 'ST000189';
                }
                break;
            case 03:
                if ($kd_sk == 'PP.08.1' || $kd_sk == 'PN.01.01' || 
                    $kd_sk == 'PP.01.2'  || $kd_sk == 'PP.05.3' || $kd_sk == 'KM.002'){
                            return 'ST000386';
                     }
                else if($kd_sk == 'KM.02.02'){
                            return 'ST000388';
                }
                else{
                    return 'ST000196';
                }
                break;
            case 04:
                if ( $kd_sk == 'PP.08.1' || $kd_sk == 'PN.01.01' || 
                    $kd_sk == 'PP.01.2'  || $kd_sk == 'PP.05.3' || $kd_sk == 'KM.002'){
                            return 'ST000358';
                     }
                else if($kd_sk == 'KM.02.02'){
                            return 'ST000360';
                }
                else{
                    return 'ST000203';
                }
                break;
            case 05:
                if ($kd_sk == 'PP.08.1' || $kd_sk == 'PN.01.01' || 
                    $kd_sk == 'PP.01.2'  || $kd_sk == 'PP.05.3' || $kd_sk == 'KM.002'){
                            return 'ST000410';
                     }
                else if($kd_sk == 'KM.02.02'){
                            return 'ST000412';
                }
                else{
                    return 'ST000210';
                }
                break;
            case 06:
                if ($kd_sk == 'PP.08.1' || $kd_sk == 'PN.01.01' || 
                    $kd_sk == 'PP.01.2'  || $kd_sk == 'PP.05.3' || $kd_sk == 'KM.002'){
                            return 'ST000279';
                     }
                else if($kd_sk == 'KM.02.02'){
                            return 'ST000281';
                }
                else{
                    return 'ST000217';
                }
                break;
            case 07:
                if ($kd_sk == 'PP.08.1' || $kd_sk == 'PN.01.01' || 
                    $kd_sk == 'PP.01.2'  || $kd_sk == 'PP.05.3' || $kd_sk == 'KM.002'){
                            return 'ST000442';
                     }
                else if($kd_sk == 'KM.02.02' ){
                            return 'ST000444';
                }
                else{
                            return 'ST000224';
                }
                break;

          
            default:
                if ($kd_sk == 'PP.08.1' || $kd_sk == 'PN.01.01' || 
                    $kd_sk == 'PP.01.2'  || $kd_sk == 'PP.05.3' || $kd_sk == 'KM.002'){
                            return 'ST000438';
                     }
                else if($kd_sk == 'KM.02.02'){
                            return 'ST000440';
                }
                else{
                            return 'ST000231';
                }
           
                break;
        }
    }

   
 
}
?>
