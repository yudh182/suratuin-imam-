<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api_model extends CI_Model
{

    function __construct()
    {
        
        parent::__construct();    
        $this->load->library('Curl');
        $this->load->database();
        $this->load->helper('url');
        $this->load->library('S00_lib_api');
    }

     public function get_makul_smt($nim=null){
        $CI =& get_instance();
        $ta = $CI->s00_lib_api->get_api_json(URL_API_SIA.'sia_krs/data_procedure', 'POST', array('api_kode'=>50000, 'api_subkode'=>2));
        $cek=$CI->s00_lib_api->get_api_json(URL_API_SIA.'sia_krs/data_search', 'POST', array('api_kode'=>64000, 'api_subkode' => 22, 'api_search' => array($ta['data'][':hasil1'],$ta['data'][':hasil3'],$nim)));
         $response = array();
		$posts = array();
		for($i=0; $i<count($cek); $i++){
			$posts[] = array(
            		"nama_makul"            =>  $cek[$i]['NM_MK'],
            		"kode_makul"                  =>  $cek[$i]['KD_MK'],
            		"kode_kelas"			=> $cek[$i]['KD_KELAS']
        	);
		}
		$return = 0;
		$response['makul'] = $posts;
		$hasil = json_decode(json_encode($response), true);
		for($i=0;$i<count($hasil);$i++){
			if($hasil['makul'][$i]['kode_makul'] == 'TIF404033'){
				$return = 'succes';
				break;
			}
		}

		return $return;
		//print_r($hasil);
        
    }
    public function get_pejabat($id_jab=null){
        $CI = & get_instance();
        $unit_id = 'UN02006';  //kode unit   
        $status_jabatan = 1;
        $parr = array('api_kode' => 1121, 'api_subkode' => 8, 'api_search' => array(date('d/m/Y'), $unit_id, 1));
        $ttd = $CI->s00_lib_api->get_api_json(URL_API_SIMPEG1.'simpeg_mix/data_view', 'POST', $parr);
        $x=0;

        foreach ($ttd as $key) {
            $x++;
            if ($ttd[$x]['RR_STR_ID'] == $id_jab &&  $ttd[$x]['RR_STATUS'] == '1'){
              $x = $x + 0;
            break;
            //continue;
            }
        }
        //pemanggilan nama pegawai
        $parameter = array('api_kode' => 2001, 'api_subkode' => 2, 'api_search' => array($ttd[$x]['RR_KD_PGW']));
        return $pegawai = $this->s00_lib_api->get_api_json(URL_API_SIMPEG1.'simpeg_mix/data_search', 'POST', $parameter);
        //$pegawai[0]['NM_PGW_F'];
                
    }

 
    

}
    
