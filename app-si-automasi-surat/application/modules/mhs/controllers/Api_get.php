<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Apiget extends My_Controller
{

	function __construct()
	{
        header('Content-type: application/json');
        parent::__construct();    
        $this->load->library('Curl');
        $this->load->database();
        $this->load->helper('url');
        $this->load->model('Login_model');
        $this->load->model('Surat_model');
        $this->load->library('s00_lib_api');
    }  
	 
}
	
