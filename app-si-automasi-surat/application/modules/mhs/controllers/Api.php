<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends My_Controller
{

	function __construct()
	{
        header('Content-type: application/json');
        parent::__construct();    
        

        $this->load->library('Curl');
        $this->load->database();
        $this->load->helper('url');
        $this->load->model('Login_model');
        $this->load->model('Surat_model');
        $this->load->library('s00_lib_api');

    }
    public function get_makuldol($tahun=null, $semester=null){
        $CI =& get_instance();
        $nim = $_GET['nim'];

        if (empty($tahun) && empty($semester)){
        	$ta = $CI->s00_lib_api->get_api_json(URL_API_SIA.'sia_krs/data_procedure', 'POST', array('api_kode'=>50000, 'api_subkode'=>2));
        	$tahun = $ta['data'][':hasil1'];
        	$semester = $ta['data'][':hasil3'];        	
        }

	//print_r($ta);
	//echo $ta['data'][':hasil1'].'|';
	//echo $ta['data'][':hasil3'];
        $cek=$CI->s00_lib_api->get_api_json(URL_API_SIA.'sia_krs/data_search', 'POST', array('api_kode'=>64000, 'api_subkode' => 22, 'api_search' => array($tahun,$semester,$nim)));
        // print_r($cek);
        echo json_encode($cek);
        
    }
    public function tesjson_exp(){
        $conn = file_get_contents('http://exp.uin-suka.ac.id/surat/mhs/api/login.php?auth=8f304662ebfee3932f2e810aa8fb628735&username=15650051&password=dxpram354');
        $mahasiswa = json_decode($conn, true);
        echo $mahasiswa[0]['NamaPengguna'];

    }
    public function tesjson_learning(){
         $conn = file_get_contents('http://mobile.learning.uin-suka.ac.id/tmp900/login?auth=8f304662ebfee3932f2e810aa8fb628736&username=15650051&password=dxpram354');
        //$mahasiswa = json_decode($conn, true);
        //print_r($mahasiswa);
       // echo $mahasiswa['data'][0]['nim'];
       // echo "<hr>";
        $conn2 = file_get_contents('http://mobile.learning.uin-suka.ac.id/tmp900/mkl2');
        $makul = json_decode($conn2, true);
        print_r($makul);
        

    }
    public function get_matkull(){
        $CI =& get_instance();
        $ta = $CI->s00_lib_api->get_api_json(URL_API_SIA.'sia_krs/data_procedure', 'POST', array('api_kode'=>50000, 'api_subkode'=>2));
        $cek=$CI->s00_lib_api->get_api_json(URL_API_SIA.'sia_krs/data_search', 'POST', array('api_kode'=>64000, 'api_subkode' => 22, 'api_search' => array($ta['data'][':hasil1'],$ta['data'][':hasil3'],$this->session->userdata('username'))));
        $response = array();
        $posts = array();
        for($i=0; $i<count($cek); $i++){
            $posts[] = array(
                    "nama_makul"            =>  $cek[$i]['NM_MK'],
                    "kode_makul"            =>  $cek[$i]['KD_MK'],
                    "kode_kelas"            => $cek[$i]['KD_KELAS']
            );
        }     
        $response['makul'] = $posts;
        $hasil = json_decode(json_encode($response), true);
        for($i=0;$i<count($hasil);$i++){
            if($hasil['makul'][$i]['kode_makul'] == 'TIF404045'){
                $return = 'succes_kp';
                break;
            }
        }
        echo $return;
        //print_r($hasil);
        
    }
    public function get_jabatanmaster(){
        $CI = & get_instance();
        $jab = $CI->s00_lib_api->get_api_json(URL_API_SIMPEG1.'simpeg_mix/data_view', 'POST', array('api_kode'=>1101, 'api_subkode'=>1));
        echo json_encode($jab);
    }
    public function get_ttd(){
        $CI = & get_instance();
        //$parameter = array('api_kode' => 1121, 'api_subkode' => 14, 'api_search' => array(date('d/m/Y'), $str_id, $unit_id));
        $str_id = 'ST000279'; //kode jabatan wakil dekan bisang akademik
        $unit_id = 'UN02006';  //kode unit
        $parr= array('api_kode'=>1121, 'api_subkode'=>14, 'api_search' => array(date('d/m/Y'), $str_id, $unit_id));
        $ttd = $CI->s00_lib_api->get_api_json(URL_API_SIMPEG1.'simpeg_mix/data_view', 'POST', $parr);
        echo json_encode($ttd);
    }
    
     public function get_pejabat(){
        $CI = & get_instance();
        $unit_id = 'UN02006';  //kode unit   
        $status_jabatan = 1;
        $parr = array('api_kode' => 1121, 'api_subkode' => 8, 'api_search' => array(date('d/m/Y'), $unit_id, 1));
        $ttd = $CI->s00_lib_api->get_api_json(URL_API_SIMPEG1.'simpeg_mix/data_view', 'POST', $parr);
        $x=0;

        foreach ($ttd as $key) {
            $x++;
            if ($ttd[$x]['RR_STR_ID'] == 'ST000279' &&  $ttd[$x]['RR_STATUS'] == '1'){
              $x = $x + 0;
            break;
           // continue;
            }
        }
        //pemanggilan nama pegawai
        $parameter = array('api_kode' => 2001, 'api_subkode' => 2, 'api_search' => array($ttd[$x]['RR_KD_PGW']));
        $pegawai = $this->s00_lib_api->get_api_json(URL_API_SIMPEG1.'simpeg_mix/data_search', 'POST', $parameter);
        return $pegawai[0]['NM_PGW_F'];
        /////echo json_encode($pegawai,JSON_PRETTY_PRINT);       
    }
    public function get_namapegawai(){
        $parameter = array('api_kode' => 2001, 'api_subkode' => 2, 'api_search' => array('199205200000001201'));
        $pegawai = $this->s00_lib_api->get_api_json(URL_API_SIMPEG1.'simpeg_mix/data_search', 'POST', $parameter);
        echo json_encode($pegawai,JSON_PRETTY_PRINT);
    }

    public function get_pejabat_by_unit(){
        $CI = & get_instance();
        $status_jabatan =1 ;
        $unit_id = 'UN02006';
        $parr = array('api_kode' => 1121, 'api_subkode' => 8, 'api_search' => array(date('d/m/Y'), $unit_id, $status_jabatan));
        $pej = $CI->s00_lib_api->get_api_json(URL_API_SIMPEG1.'simpeg_mix/data_view', 'POST', $parr);
        echo json_encode($pej);
    }
    public function login(){
        $this->load->library('Curl'); 
        $hasil_     = 'false';
        $postorget  = 'GET';
        $nama ='';
        $nim ='';
    //8f304662ebfee3932f2e810aa8fb628736
        $username = $this->input->get('username');
        $password = $this->input->get('password');
        
        $auth = $this->input->get('auth');
        $api_url = 'http://service.uin-suka.ac.id/servad/adlogauthgr.php?aud='.$auth.'&uss='.$username.'&pss='.$password;

        $hasil = $this->curl->simple_get($api_url, false, array(CURLOPT_USERAGENT => true)); 
        $hasil = json_decode($hasil, true);
        echo json_encode($hasil);
    }

    // public function insert(){
    //     //=========================Menmapilkan================================
    //    // $query = $this->db->query('SELECT id, name, age FROM company');

    //    //  foreach ($query->result_array() as $row)
    //    //  {
    //    //          echo $row['id'];
    //    //          echo $row['name'];
    //    //          echo $row['age'];
    //    //  }
    //      //=========================Menmapilkan================================
    //      echo $this->Surat_model->cari_ttd(2);
    //     }
     
    
    public function get_mhs(){
        header('Content-Type: application/json');
        // echo $_GET['nama'];
        // echo "<br />";
        // echo $_GET['email'];
        $nim    = $_GET['nim'];
        //$sekarang   = DATE('d-m-y');
        $par1   = array($nim);
        $datar = $this->siaapi_getmhs('sia_public/sia_mahasiswa/data_search',26000,10,'api_search',$par1);
        $datrr = json_decode($datar,true);
        echo json_encode($datrr,JSON_PRETTY_PRINT);
    }
    public function siaapi_getmhs($apiurl=null,$apikod=0,$apisub=0,$apitxt=null,$apisrc=array()){
        $aipisp = 'http://service.uin-suka.ac.id/';
        $ch = curl_init();
        $URL_API = $aipisp.'servsiasuper/'.$apiurl;
        $data = array('api_kode' => $apikod, 'api_subkode' => $apisub, $apitxt => $apisrc);
        curl_setopt($ch, CURLOPT_URL, $URL_API);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('HeaderName: '.hash('sha256','tester01')));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        return $result;
    }
    //==================================================================================================================
    public function test(){
        error_reporting(0);
        //http://localhost/hmvc_example/mhs/api/test.php?kd_unit=UN02006&kd_fak=99&kd_kla=KM.02.02
        $unit_id = $_GET['kd_unit']; //SAINTEK
        $kd_penandatangan_surat = $_GET['kd_fak']; #Kepala Bagian Tata Usaha
        $kode_klasifikasi = $_GET['kd_kla']; #'LAPORAN STATUS MAHASISWA', untuk status aktif studi
        #$kode_klasifikasi = 'KS.02'; #'KETATAUSAHAAN', #penggantian ktm

        $kode_jenis_surat = '11'; #'SURAT KETERANGAN'
        
        $data_sk['UNIT_ID']                 = $unit_id;
        $data_sk['KD_STATUS_SIMPAN']        = 2;
        $data_sk['ID_PSD']                  = $kd_penandatangan_surat;
        $data_sk['ID_KLASIFIKASI_SURAT']    = $kode_klasifikasi;
        $data_sk['KD_JENIS_SURAT']          = $kode_jenis_surat;
        $data_sk['KD_KEAMANAN_SURAT']       = 'B';
        $data_sk['TGL_SURAT']               = date('d/m/Y');
        
        
        $api_url = 'http://service2.uin-suka.ac.id/servtnde/tnde_public/tnde_surat_keluar/penomoran/json';
        
        $parameter  = array('api_kode'      => 90002, 
                        'api_subkode'   => 3, 
                        'api_search'    => array($data_sk));

        $cekdoang   = $this->api_curl($api_url, $parameter, "POST");
        echo json_encode($cekdoang);
    }
     function api_curl($url,$post,$method){
        $username="12792860";
        $password="4908773573895678";
        //////////////
        if(strtoupper($method)=='POST'){
            $postdata = http_build_query($post);
            $opts = array('http' =>
                array(
                    'method'  => 'POST',
                    'header' => 'Content-type: application/x-www-form-urlencoded' . "\r\n"
                        .'Content-Length: ' . strlen($postdata) . "\r\n",
                    'content' => $postdata
                )
            );
            if($username && $password)
            {
                $opts['http']['header'] = ("Authorization: Basic " . base64_encode("$username:$password"));
            }
 
            $context = stream_context_create($opts);
            $hasil=file_get_contents($url, false, $context);
            return $hasil;
        }else{
            foreach($post as $key=>$value){
                $isi=$isi."/".$key."/$value/";
            }
            $url=$url.$isi;
            $context = stream_context_create(array(
                'http' => array(
                    'header'  => "Authorization: Basic " . base64_encode("$username:$password")
                )
            ));
            #echo "<p>$url</p>";
            $hasil=file_get_contents($url, false, $context);
            return $hasil;
        }
    }
    function testing_api_cek()
        {
            $CI =& get_instance();
            $data = $CI->s00_lib_api->get_api_json(
                 'http://service2.uin-suka.ac.id/servsimpeg/simpeg_public/simpeg_mix/data_search',
                 'POST',
                 array(
                     'api_kode'      => 1001,
                     'api_subkode'   => 4,
                     'api_search'    => array('06')
                 )
            );
            echo json_encode($data);
    //         // $nip = '197701032005011003';
    //         // //$url = tf_encode('FOTOTTD#'.$nip.'#QL:100#WM:1#SZ:300');
    //         // //$tanggal = date('d-m-Y');
    //         // $logo      = tg_encode('UK000002#'.$tanggal.'#QL:50#WM:0#SZ:150');
    //         // //$data = '<img src="'.LOGO_UNIT_980.$url.'.jpg">';

    //         // //return $data;
       }
        function tes_api_pejabat()
        {
            $CI =& get_instance();
            $data = $CI->s00_lib_api->get_api_json(
                 'http://service2.uin-suka.ac.id/servsimpeg/simpeg_public/simpeg_mix/data_search',
                 'POST',
                 array(
                     'api_kode'      => 1121,
                     'api_subkode'   => 14,
                     'api_search'    => array(date('d/m/Y'),'')
                 )
            );
            echo json_encode($data);
    //         // $nip = '197701032005011003';
    //         // //$url = tf_encode('FOTOTTD#'.$nip.'#QL:100#WM:1#SZ:300');
    //         // //$tanggal = date('d-m-Y');
    //         // $logo      = tg_encode('UK000002#'.$tanggal.'#QL:50#WM:0#SZ:150');
    //         // //$data = '<img src="'.LOGO_UNIT_980.$url.'.jpg">';

    //         // //return $data;
       }

// dibawah ini adalah api untuk melihat semua daftar unit di uin
       public function checkdaftarunit(){
        header('Content-Type: application/json');
        $datar = $this->siaapi_getdata('simpeg_public/simpeg_mix/data_view',1001,3, null ,null);
        echo json_encode($datar);
        // $datrr = json_decode($datar,true);
        // $result = array();
        // foreach ($datrr as $value){
        //     array_push($result, array(
        //         'unit_id'   => $value['UNIT_ID'],
        //         'unit_nama' => $value['UNIT_NAMA']
        //     ));
        // }
        // echo json_encode($result, JSON_PRETTY_PRINT);
    }
function siaapi_getdata($apiurl=null,$apikod=0,$apisub=0,$apitxt=null,$apisrc=array()){
        $aipisp = 'http://service2.uin-suka.ac.id/';
        $ch = curl_init();
        $URL_API = $aipisp.'servsimpeg/'.$apiurl;
        $data = array('api_kode' => $apikod, 'api_subkode' => $apisub, $apitxt => $apisrc);
        curl_setopt($ch, CURLOPT_URL, $URL_API);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('HeaderName: '.hash('sha256','tester01')));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        return $result;
    }
public function checkanggotaunit(){
        header('Content-Type: application/json');
        $unitid = $this->input->get("unitid");
        $parameter  = array($unitid);
//        $parameter  = array("UN01028");
        $datar = $this->siaapi_getdata('simpeg_public/simpeg_mix/data_search',1121,15,'api_search',$parameter);
        $datrr = json_decode($datar,true);

        $result = array();
        foreach ($datrr as $value){
            array_push($result, array(
                'nip'   => $value['NIP'],
                'nama'  => $value['NM_PGW_F']
            ));
        }
        echo json_encode($result, JSON_PRETTY_PRINT);
    }
//======kusus foto
 function photo(){
        
        $nim =  $_GET['nim'];
        
        $nim        = preg_replace("/[^0-9]/", "", $nim);
        $api_url    = URL_APISIA.'foto/ns_mahasiswa_auto/'.$nim;
        
                header("Content-type: image/jpeg");
                
                $bn=file_get_contents($api_url);
                echo  $bn;
                // if($bn==''){
                
                //     $url = $this->tf_encode('FOTOMASUK#'.$nim.'#QL:100#WM:1#SZ:300');
                //     echo file_get_contents(APP_STATIC.'foto/pgw/990/'.$url.'.jpg');
                
                // }else{
                    
                //     $url = $this->tf_encode('FOTOAUTO#'.$nim.'#QL:100#WM:1#SZ:300');
                //     $ur=APP_STATIC."foto/mhs/990/$url.jpg";
                

                //     $opts['http']['header'] .= "Referer: http://exp.uin-suka.ac.id/\r\n";
                //     $context = stream_context_create($opts);
                //     echo file_get_contents($ur, false, $context); 
                
                // } 
            
        
    }


   public function tf_encode($kd_kelas){ $hasil = ''; #return $kd_kelas;
        $str    = 'sng3bAdac5UEmQzv2YBTH8CVh7jXpRo0etfOK4MINSlwFZ6iL9kPD1JWyuqGxr#-.:/';
        $arr_e = array();  $arr_e1 = array(); $arr_r = array(); $arr_r1 = array();
        for($j = 0; $j < strlen($str); $j++){
            $j_ = $j; if ($j_ < 10) { $j_ = '0'.$j_; }
            $arr_e1[$j] = substr($str,$j,1);
            $arr_e[$j_] = substr($str,$j,1);
            $arr_r1[substr($str,$j,1)] = $j;
            $arr_r[substr($str,$j,1)] = $j_;
        }
        
        $total = 0;
        for($i = 0; $i < strlen($kd_kelas); $i++){
            $total = (int)substr($kd_kelas,$i,1) + $total; 
        } $u = fmod($total,10);
        
        $kd_enc = $arr_e1[$u];
        for($i = 0; $i < strlen($kd_kelas); $i++){
            $k = ($arr_r1[substr($kd_kelas,$i,1)]+$u); if($k < 10) { $k = '0'.$k; }
            $kd_enc .= ''.$k.rand(0,9); 
        } return $kd_enc;
    }
    public function tf_encode2($kd_kelas){ $hasil = ''; #return $kd_kelas;
        $str    = 'sng3bAdac5UEmQzv2YBTH8CVh7jXpRo0etfOK4MINSlwFZ6iL9kPD1JWyuqGxr#-.:/';
        $arr_e = array();  $arr_e1 = array(); $arr_r = array(); $arr_r1 = array();
        for($j = 0; $j < strlen($str); $j++){
            $j_ = $j; if ($j_ < 10) { $j_ = '0'.$j_; }
            $arr_e1[$j] = substr($str,$j,1);
            $arr_e[$j_] = substr($str,$j,1);
            $arr_r1[substr($str,$j,1)] = $j;
            $arr_r[substr($str,$j,1)] = $j_;
        }
        
        $total = 0;
        for($i = 0; $i < strlen($kd_kelas); $i++){
            $total = (int)substr($kd_kelas,$i,1) + $total; 
        } $u = fmod($total,10);
        
        $kd_enc = $arr_e1[$u];
        for($i = 0; $i < strlen($kd_kelas); $i++){
            $k = ($arr_r1[substr($kd_kelas,$i,1)]+$u); if($k < 10) { $k = '0'.$k; }
            $kd_enc .= ''.$k.rand(0,9); 
        } return $kd_enc;
    }
	 
}
	
