<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tmp900 extends My_Controller {

	public function __construct(){
		header('Content-type: application/json');


		
		parent::__construct();
		$this->load->library('Curl');
		$this->user=$this->session->userdata('id_user');
		$this->ta=$this->session->userdata('kd_ta');
		$this->pass=$this->session->userdata('pass');
		// $this->ta='2012';
		$this->smt=$this->session->userdata('kd_smt');
		$this->soal_count=15;
		$this->api 		= $this->s00_lib_api;
		$this->user_kelas;

		
		
		// if($this->uri->segment(2)){
		// 	$this->ta=$this->uri->segment(2);
		// }
		// if($this->uri->segment(3)){
		// 	$this->smt=$this->uri->segment(3);
		// }
		
				
		// $this->_0422_match_tasmt();
	}
	  
	public function index(){ echo 'TMP900'; }


		

	public function ta(){
		$this->load->library('Curl');
		$user = $this->session->userdata('id_user');
		$pass = $this->session->userdata('pass'); 
		$auth_ad_id = '8f304662ebfee3932f2e810aa8fb628736';
		$api_url 	= URL_API_AD.'adlogauthgr.php?aud='.$auth_ad_id.'&uss='.$user.'&pss='.$pass;
		$hasil 		= $this->curl->simple_get($api_url);
		$hasil = json_decode($hasil, true);
		$ta = $this->s00_lib_api->get_api_json(URL_API_SIA.'sia_krs/data_procedure', 'POST', array('api_kode'=>50000, 'api_subkode'=>2));
		// print_r($ta);
		// print_r($hasil);
		$arr_session = array(	'id_user'		=> $user,
									'status'		=> $this->parse_ad_status($hasil[0]['AnggotaDari']),
									'jabatan'		=> $this->v000_parse_ad_jabatan($hasil),
									'akses_prodi'	=> $this->parse_ad_akses_prodi($hasil),
									#'status'		=> 'mhs',
									#'jabatan'		=> 'MHS',
									#'akses_prodi'	=> '',
									
									#'user_agent'	=> $_SERVER['HTTP_USER_AGENT'],
									#'ip_address'	=> $_SERVER['REMOTE_ADDR'],
									#'time_login'	=> fulltoday(),
									'time_login'	=> time(),
									'kd_ta' 		=> $ta['data'][':hasil1'],
									'ta' 			=> $ta['data'][':hasil2'],
									'kd_smt' 		=> $ta['data'][':hasil3'],
									'nm_smt' 		=> $ta['data'][':hasil4'],
									'logged_in' 	=> TRUE,
									'is_admin'		=> $admin
									);
		
		$this->session->set_userdata($arr_session);	
		$this->ta=$this->session->userdata('kd_ta');
		$this->smt=$this->session->userdata('kd_smt');
		$cek2=$this->s00_lib_api->get_api_json(URL_API_SIA.'sia_master/data_search', 'POST', array('api_kode'=>16001, 'api_subkode' => 100, 'api_search' => array($this->ta,$this->smt)));
		
		$this->ta=$this->session->userdata('kd_ta');
		$this->smt=$this->session->userdata('kd_smt');
		$this->user=$this->session->userdata('id_user');
		$cek=$this->s00_lib_api->get_api_json(URL_API_SIA.'sia_krs/data_search', 'POST', array('api_kode'=>64000, 'api_subkode' => 22, 'api_search' => array($this->ta,$this->smt,$this->user)));
		$this->session->set_userdata('dpa_mhs',$cek);
		$this->mhs();
		$this->set_kelas_user();
		// print_r($cek);

	}

	public function mhs(){
		$mhs=$this->s00_lib_api->get_api_json(URL_API_SIA.'sia_mahasiswa/data_search', 'POST', array('api_kode'=>26000, 'api_subkode' => 10, 'api_search' => array($this->session->userdata('id_user'))));
		$this->session->set_userdata('data_kd',array($mhs[0]['KD_PRODI'],$mhs[0]['KD_JURUSAN'],$mhs[0]['KD_FAK']));
		
	}
	public function ta2(){
		$this->load->library('Curl');
		$user = $this->session->userdata('id_user');
		$pass = $this->session->userdata('pass'); 
		$auth_ad_id = '8f304662ebfee3932f2e810aa8fb628736';
		$api_url 	= URL_API_AD.'adlogauthgr.php?aud='.$auth_ad_id.'&uss='.$user.'&pss='.$pass;
		$hasil 		= $this->curl->simple_get($api_url);
		$hasil = json_decode($hasil, true);
		$ta = $this->s00_lib_api->get_api_json(URL_API_SIA.'sia_krs/data_procedure', 'POST', array('api_kode'=>50000, 'api_subkode'=>2));
		//echo json_encode($ta);

		// $cek2=$this->s00_lib_api->get_api_json(URL_API_SIA.'sia_master/data_search', 'POST', array('api_kode'=>16001, 'api_subkode' => 100, 'api_search' => array($ta['data'][':hasil1'],$ta['data'][':hasil3'])));
		//echo json_encode($cek2);

		$cek=$this->s00_lib_api->get_api_json(URL_API_SIA.'sia_krs/data_search', 'POST', array('api_kode'=>64000, 'api_subkode' => 22, 'api_search' => array($ta['data'][':hasil1'],$ta['data'][':hasil3'],$this->user)));
		echo json_encode($cek);

		// print_r($hasil);
		
		// print_r($cek);

	}

	public function makul(){
		header('Content-Type: application/json');
		$this->ta=$this->session->userdata('kd_ta');
		$this->smt=$this->session->userdata('kd_smt');
		$this->user=$this->session->userdata('id_user');
		$cek=$this->s00_lib_api->get_api_json(URL_API_SIA.'sia_krs/data_search', 'POST', array('api_kode'=>64000, 'api_subkode' => 22, 'api_search' => array($this->ta,$this->smt,$this->user)));
		// print_r($cek);
		$response = array();
		$posts = array();
		for($i=0; $i<count($cek); $i++){
			$posts[] = array(
            		"nama_makul"            =>  $cek[$i]['NM_MK'],
            		"kode_makul"                  =>  $cek[$i]['KD_MK'],
            		"kode_kelas"			=> $cek[$i]['KD_KELAS']
        	);
		}
		
		$response['makul'] = $posts;
		$hasil = json_decode($response, true);
		echo json_encode($hasil);
	}

	public function post($kode_mk, $kode_kelas){
		header('Content-Type: application/json');
		$this->load->library('Curl'); 
		$api=APP_SERVICE.'api/posts/kd_makul/'.$kode_mk.'/kd_kelas/'.$kode_kelas.'/kd_ta/2017';
		$this->curl->create($api);
		$result = json_decode($this->curl->execute(), TRUE);
		$response = array();
		$posts = array();
		for($i=0; $i<count($result); $i++){
			$file = array();
			$attach = $this->get_attachments($result['data'][$i]['ID_POST'],1);
			for($j=0; $j<count($attach);$j++){
            	$file[] = array(
            		"nama_file"            =>  $attach[$j]->NAMA_FILE,
            		"path_file"                  =>  base_url().'attc/'.$attach[$j]->ID_FILE,
            	);
            }
			$posts[] = array(
            		"id_user"            =>  $result['data'][$i]['ID_USER'],
            		"konten"                  =>  $result['data'][$i]['CONTENT'],
            		"file"				=> $file
            		
            		
            		
        	);
		}
		
		$response['post'] = $posts;
		echo json_encode($response);
	}

	public function logout(){
		$this->session->sess_destroy();
		$this->session->set_userdata('log', 'out');
	}

	public function login(){
		$this->load->library('Curl'); 
		$hasil_ 	= 'false';
		$postorget 	= 'GET';
		$nama ='';
		$nim ='';
		$username = $this->input->get('username');
		$password = $this->input->get('password');
		$auth = $this->input->get('auth');
		$api_url = 'http://service.uin-suka.ac.id/servad/adlogauthgr.php?aud='.$auth.'&uss='.$username.'&pss='.$password;

		$hasil = $this->curl->simple_get($api_url, false, array(CURLOPT_USERAGENT => true)); 
		$hasil = json_decode($hasil, true);
		$nama = $hasil[0]['NamaDepan'].' '.$hasil[0]['NamaBelakang'];
		$nim = $hasil[0]['NamaPengguna'];
		if(is_array($hasil)){
			$auth_type = array("MhsGroup","StaffGroup","KartuGroup","OrtuGroup");
			foreach($auth_type as $auth_type_){
				for($j1 = 0; $j1 < count($hasil[0]['AnggotaDari'][0]); $j1++){
					if($hasil[0]['AnggotaDari'][0][$j1] == $auth_type_){
						$hasil_ = 'true'; break;
						
					}
				}
			}
		}
		//print_r($hasil);
		$posts[] = array(
            		"nim"                 =>  $nim,
            		"nama"                  =>  $nama,
			"success"		=> $hasil_
        	);
		$response['data'] = $posts;
		$this->session->set_userdata('id_user',$username);
		$this->session->set_userdata('pass',$password);
		$this->ta();
		$this->session->set_userdata('log', 'in');
    		echo json_encode($response,TRUE);
	}
	function parse_ad_status($id){
		/* 
		//VERIFIKASI AD VERSI LAWAS
		switch($id){
			case 'CN=StaffGroup,CN=Users,DC=ad,DC=uin-suka,DC=ac,DC=id': 	$hasil = 'staff'; break;
			case 'CN=MhsGroup,CN=Users,DC=ad,DC=uin-suka,DC=ac,DC=id': 		$hasil = 'mhs'; break;
			case 'CN=KartuGroup,CN=Users,DC=ad,DC=uin-suka,DC=ac,DC=id': 	$hasil = 'kartu'; break;
			case 'CN=WaliGroup,CN=Users,DC=ad,DC=uin-suka,DC=ac,DC=id': 	$hasil = 'wali'; break;
			default:														$hasil = ''; break;
		} return $hasil; */
		
		foreach($id[0] as $idx){
			switch ($idx){
				case 'StaffGroup': 		$hasil = 'staff'; break;
				case 'MhsGroup': 		$hasil = 'mhs'; break;
				case 'KartuGroup': 		$hasil = 'kartu'; break;
				case 'OrtuGroup': 		$hasil = 'wali'; break;
				default:				$hasil = ''; break;
			}
		} return $hasil;		
	}

	function v000_parse_ad_jabatan($data){
		$CI =& get_instance();
		$status = $this->v000_parse_ad_status($data[0]['AnggotaDari']);
				
		switch($status){
			case 'mhs': case 'wali':	return ''; break;
			case 'staff': case 'kartu':
				$jabatan = $this->v000_get_ad_jabatan($data);
				return implode('#',$jabatan[3]);
			break;
		}
	}
	function v000_parse_ad_status($id){
		foreach($id[0] as $idx){
			switch ($idx){
				case 'StaffGroup': 		$hasil = 'staff'; break;
				case 'MhsGroup': 		$hasil = 'mhs'; break;
				case 'KartuGroup': 		$hasil = 'kartu'; break;
				case 'OrtuGroup': 		$hasil = 'wali'; break;
				default:				$hasil = ''; break;
			}
		} return $hasil;		
	}
	function v000_get_ad_jabatan($data){
		
		
		$arr_prodi 	= array();
		$arr_fak 	= array();
		$arr_nfak 	= array();
		$arr_jabat 	= array();
		$arr_sjb 	= array();
		
		//cek dosen apa bukan
		
				
		$kd_pgw = trim(strtoupper($data[0]['NamaPengguna']));
				
		//simpeg, JABATAN SISTEM melekat ke JABATAN STRUKTURAL 
		$date1 = date('d/m/Y');
		$parameter = array('api_kode' => 1121, 'api_subkode' => 3, 'api_search' => array($date1,$kd_pgw,'1'));
		$db_jbstr = $this->s00_lib_api->get_api_json(URL_API_SIMPEG1.'simpeg_mix/data_search', 'POST', $parameter);
		foreach($db_jbstr as $x1){
			$parameter = array('api_kode' => 1181, 'api_subkode' => 2, 'api_search' => array($x1['STR_ID']));
			$db_autoj = $CI->s00_lib_api->get_api_json(URL_API_SIMPEG1.'simpeg_mix/data_search', 'POST', $parameter);
			if(!empty($db_autoj)){ foreach($db_autoj as $x2){
				if(!in_array($x2['R2S_SIS_ID'], $arr_sjb, true)){ array_push($arr_sjb, $x2['R2S_SIS_ID']); }
			}}
		}
		
		//simpeg, JABATAN SISTEM manual PER ORANG
		$parameter = array('api_kode' => 1124, 'api_subkode' => 2, 'api_search' => array($kd_pgw));
		$db_jabat = $CI->s00_lib_api->get_api_json(URL_API_SIMPEG1.'simpeg_mix/data_search', 'POST', $parameter);
		foreach($db_jabat as $x1){
			if(!in_array($x1['SIS_ID'], $arr_sjb, true)){ array_push($arr_sjb, $x1['SIS_ID']); }
		}
				
		return array(array(), array(), array(), $arr_sjb);
	}
	
	function parse_ad_akses_prodi($data){
		$CI =& get_instance();
		$status = $this->parse_ad_status($data[0]['AnggotaDari']);
				
		switch($status){
			case 'mhs':	case 'wali':	return ''; break;
			case 'staff':
			case 'kartu':
				$jabatan = $this->get_ad_jabatan($data);
				return implode('#',$jabatan[0]);
			break;
		}
	}

	function get_ad_jabatan($data){
		
		
		$arr_prodi 	= array();
		$arr_fak 	= array();
		$arr_nfak 	= array();
		$arr_jabat 	= array();
		
		//comment when DB SIA is activated again
		#return array(array(),array(),array(),array('KKNADM'));
		
		//cek dosen apa bukan
		#if($this->is_dosen($data[0][427])){ $arr_jabat[] = 'DSN'; }
		// if($this->is_dosen($data[0]['NamaPengguna'])){ $arr_jabat[] = 'DSN'; }
		
		// //cek dosen wali apa bukan
		// #if($this->is_dosen_wali($data[0][427])){ $arr_jabat[] = 'DPA'; }
		// if($this->is_dosen_wali($data[0]['NamaPengguna'])){ $arr_jabat[] = 'DPA'; }
		
		//data dari *simpeg 
		$parameter = array('api_kode' => 90003, 'api_subkode' => 1, 'api_search' => array($data[0]['NamaPengguna']));
		#$parameter = array('api_kode' => 90003, 'api_subkode' => 1, 'api_search' => array($data[0][427]));
		
		$db_jabat = $this->s00_lib_api->get_api_json(URL_API_SIMPEG.'data_search', 'POST', $parameter);
		if(isset($db_jabat)){
			if(is_array($db_jabat)){
				foreach($db_jabat as $d){ 
					$arr_jabat[] = $d['KD_JABATAN']; 
					
					$parameter 	= array('api_kode' => 90004, 'api_subkode' => 1, 'api_search' => array($d['KD_JABATAN']));
					$akses_ 	= $this->s00_lib_api->get_api_json(URL_API_SIMPEG.'data_search', 'POST', $parameter);
					
					if (subsub2($d['KD_JABATAN'], $arr_jabat)) { $arr_jabat[] = $d['KD_JABATAN']; }
					if(!empty($akses_)){
						foreach ($akses_ as $akses__){
							if (subsub2($akses__['KD_PRODI'], $arr_prodi)) { $arr_prodi[] = $akses__['KD_PRODI']; }
							if (subsub2($akses__['KD_FAK'], $arr_fak)) { $arr_fak[] = $akses__['KD_FAK']; }
							if (subsub2($akses__['NM_FAK'], $arr_nfak)) { $arr_nfak[] = $akses__['NM_FAK']; }
						}
					}
				}
			}		
		}
		return array($arr_prodi, $arr_fak, $arr_nfak, $arr_jabat);
	}

	public function beranda($item=NULL){
		
		$type=1;
		 $kd_group='1';
		$type_limit=1;
		$offset=0;
		$count=10;
		if (isset($item)){
			$count = $item;
		};
		$kategori=$this->input->post('kd_kategori');
		$statususer=$this->input->post('status_user');
		//$count=20;
		//get info---
		if($this->session->userdata('status')=='mhs'){
		$cek=$this->daftarkelas();
		}else if($this->session->userdata('status')=='staff'){
			$cek=$this->daftarkelas();
		}
		$ids=array();
		$idms=array();
		foreach($cek as $d){

			array_push($ids,$d['KD_KELAS']);
			array_push($idms,$d['KD_MK']);
		}
		if($this->session->userdata('status')=='mhs'){
		array_push($ids,"DPA".$this->dpa_mhs[0]['NIP']);
		}else if($this->session->userdata('status')=='staff'){
		array_push($ids,"DPA".$this->user);
		}

		array_push($ids,-1);
		//----------
		

	
					$ta=$this->ta;
				  $username = 'admin';
				$password = '1234';
				
				
				$api=APP_SERVICE.'api/posts/kd_makul/'.$kd_group.'/kd_ta/'.$ta;
				if($type==1){
					$api=APP_SERVICE.'api/posts/kd_makul/'.$kd_group.'/kd_ta/'.$ta;
				}else{
					$api=APP_SERVICE.'api/postssubgroup/kd_group/'.$kd_group.'/kd_ta/'.$ta;
				}
				if(isset($count))
				$api.="/count/".$count;
				
				//$api.='/type_limit/'.$type_limit.'/offset/'.$offset."/kd_kelas/22607K10366138";
				$api.='/type_limit/'.$type_limit.'/offset/'.$offset."/kd_kelas_in/".urlencode(serialize($ids));
				//previous ta load matakuliah tanpa kelas
				if($this->session->userdata('status')=='staff'){
					$api.='/kd_makul_in/'.urlencode(serialize($idms));
					$api.='/id_user/'.$this->user;
				}

				
				if($kategori){
					$api.='/kd_kategori/'.$kategori;
				}

				if($statususer){
					$api.='/status_user/'.$statususer;
				}

				if($this->session->userdata('ta_w1') && $this->session->userdata('ta_w2') && (int)$this->ta>=2014){
					$api.='/limit_t1/'.$this->session->userdata('ta_w1');
					$api.='/limit_t2/'.$this->session->userdata('ta_w2');
				}

				if($this->session->userdata('data_kd')){
					$data_kd=$this->session->userdata('data_kd');
					$api.='/kd_prodi/'.$data_kd[0];
					$api.='/kd_jur/'.$data_kd[1];
					$api.='/kd_fak/'.$data_kd[2];
					$api.='/kd_makul/'.implode("delimiter", $idms);
				}

				$this->curl->create($api);
				
				// Optional, delete this line if your API is open
				$this->curl->http_login($username, $password);

				
				$result = json_decode($this->curl->execute());
				

				if(isset($result))
				{
					
					$data=$result->data;
					

					// print_r($data);
					// for($i=0;$i<count($data);$i++){
					// 	$dt=$data[$i];
					// 	$this->get_attachments($dt->ID_POST,1);
					// }
					// echo $this->format_posts_retrieved($result).'<br>'.$this->get_time().'<br>'.$this->format_posts($result);
					// if($this->session->userdata('betatesting'))
					// 	print_r($result);
					$response = array();
					$posts = array();
					for($i=0; $i<count($data); $i++){
						$res = $data[$i];
						$ruang;
						if($data[$i]->ID_PRIVACY==1){
							$ruang='Kelas';
						}else if($data[$i]->ID_PRIVACY==2){
							$ruang='Universitas';;
						}else if($data[$i]->ID_PRIVACY==3){
							$ruang='Mata Kuliah';
						}else if($data[$i]->ID_PRIVACY==4){
							$ruang='Prodi';
						}else if($data[$i]->ID_PRIVACY==5){
							$ruang='Jurusan';
						}else if($data[$i]->ID_PRIVACY==6){
							$ruang='Fakultas';
						}
						$aksi = $this->s00_lib_api->get_api_json(URL_API_SIA.'sia_penawaran/data_search', 'POST', array('api_kode'=>58000, 'api_subkode' => 6, 'api_search' => array($data[$i]->KD_KELAS)));
						if(empty($aksi)){
							$aksi = $this->s00_lib_api->get_api_json(URL_API_SIA.'sia_kurikulum/data_search', 'POST', array('api_kode'=>40000, 'api_subkode' => 19, 'api_search' => array($data[$i]->KD_PRODI,$res->KD_KUR,$res->KD_MAKUL)));
						}
						// print_r($aksi);
						$nama;
						if($data[$i]->STATUS_USER=="mhs"){
							$cekusr=$this->api->get_api_json(URL_API_SIA.'sia_mahasiswa/data_search', 'POST', array('api_kode'=>26000, 'api_subkode' => 10, 'api_search' => array($data[$i]->ID_USER)));
							$nama=ucwords(strtolower($cekusr[0]['NAMA']));
						}else{
							$cekusr=$this->api->get_api_json(URL_API_SIA.'sia_dosen/data_search', 'POST', array('api_kode'=>20000, 'api_subkode' => 3, 'api_search' => array($data[$i]->ID_USER)));
							$nama=$cekusr[0]['NM_DOSEN_F'];
						}	
						
						$file = array();
						$attach = $this->get_attachments($data[$i]->ID_POST,1);
						for($j=0; $j<count($attach);$j++){
			            	$file[] = array(
			            		"nama_file"            =>  $attach[$j]->NAMA_FILE,
			            		"path_file"                  =>  base_url().'tmp900/file/'.$attach[$j]->ID_FILE,
			            		"type_file"				=> substr(pathinfo($attach[$j]->NAMA_FILE, PATHINFO_EXTENSION),0,3)
			            	);
			            };
			            $ppc=base_url().'tmp900/foto_mahasiswa/'.$data[$i]->ID_USER;
						if($data[$i]->STATUS_USER=='staff'){
							$ppc=base_url().'tmp900/foto_staff/'.$data[$i]->ID_USER;
						};
						$posts[] = array(
			            		"id_post"            =>  $data[$i]->ID_POST,
			            		"konten"                  =>  $data[$i]->CONTENT,
			            		"prodi"				=> $aksi[0]['NM_PEND'].' - '.$aksi[0]['NM_PRODI'],
			            		"kelas"				=> $aksi[0]['NM_MK'].' '.$aksi[0]['KELAS_PARAREL'],
			            		"waktu"                  =>  $data[$i]->WAKTU_POST,
			            		"setuju"                  =>  $data[$i]->LIKES,
			            		"ruang"                  =>  $ruang,
			            		"dosen"				=> $nama,
			            		"foto_user"			=> $ppc,
			            		"file"				=> $file
			            		
			            		
			            		
			        	);
					}
					$response['beranda'] = $posts;
    				echo json_encode($response,TRUE);
				}
				
				else
				{
					echo 'err_k';
				}
	}

	function daftarkelas(){
		$this->ta=$this->session->userdata('kd_ta');
		$this->smt=$this->session->userdata('kd_smt');
		$this->user=$this->session->userdata('id_user');
		
		if(!$this->session->userdata('daftarkelas')){
					
						$cek=$this->s00_lib_api->get_api_json(URL_API_SIA.'sia_krs/data_search', 'POST', array('api_kode'=>64000, 'api_subkode' => 22, 'api_search' => array($this->ta,$this->smt,$this->user)));
						if(is_array($cek)){
							if(count($cek)>0){
								$this->session->set_userdata('daftarkelas',$cek);
								return $cek;
							}else{
								return $cek;
							}
						}
					
				}else{
					return $this->session->userdata('daftarkelas');
				}
	}

	

	function get_attachments($id_post,$post_type){
					$this->load->library('Curl');
					$api=APP_SERVICE.'api/files/id_post/'.$id_post.'/post_type/'.$post_type;
					$this->curl->create($api);
					
					$exc=json_decode($this->curl->execute());
					return $exc;
	}
	function foto_mahasiswa($id_user=null){
		if ($this->session->userdata('log')=='in') {
		$nim = $this->user;
		if (isset($id_user)) {
			$nim = $id_user;
		}
		$nim 		= preg_replace("/[^0-9]/", "", $nim);
		$api_url 	= URL_APISIA.'foto/ns_mahasiswa_auto/'.$nim;
		
		// $builder = http_build_query(array('g3mb0k' => '&&&&&&&**********'),'','&');
		// $opts = array('http' =>
		// 		array(
		// 		'method'  => 'POST',
		// 		'header'  => 'Content-type: application/x-www-form-urlencoded',
		// 		'content' => $builder
		// 		)
		// 	);
		
		
			
			
				header("Content-type: image/jpeg");
				
				$bn=file_get_contents($api_url);
				if($bn==''){
					//echo $bn;
					// echo file_get_contents(base_url().'asset/default_avatar.jpg');
					// $url = $this->tf_encode('FOTOAUTO#'.$nim.'#QL:100#WM:0#SZ:300');
					// echo '<img src="http://static.uin-suka.ac.id/foto/pgw/990/'.$url.'.jpg">';
					// echo file_get_contents('http://static.uin-suka.ac.id/foto/pgw/990/'.$url.'.jpg');
					$url = $this->tf_encode('FOTOMASUK#'.$nim.'#QL:100#WM:1#SZ:300');
					echo file_get_contents(APP_STATIC.'foto/pgw/990/'.$url.'.jpg');
					// echo '<img src="http://static.uin-suka.ac.id/foto/pgw/990/'.$url.'.jpg">';
				}else{
					// echo $bn;
					$url = $this->tf_encode('FOTOAUTO#'.$nim.'#QL:100#WM:1#SZ:300');
					$ur=APP_STATIC."foto/mhs/990/$url.jpg";
				

					$opts['http']['header'] .= "Referer: http://learning.uin-suka.ac.id/\r\n";
					$context = stream_context_create($opts);
					echo file_get_contents($ur, false, $context); 
				
				}	// echo file_get_contents($api_url, false, stream_context_create($opts));
			
		}
	}

	public function foto_staff($id){
		header("Content-type: image/jpeg");
		$url = $this->tf_encode2('FOTOMASUK#'.$id.'#QL:100#WM:1#SZ:300');
		echo file_get_contents(APP_STATIC.'foto/pgw/990/'.$url.'.jpg');
	}
	public function tf_encode($kd_kelas){ $hasil = ''; #return $kd_kelas;
		$str 	= 'sng3bAdac5UEmQzv2YBTH8CVh7jXpRo0etfOK4MINSlwFZ6iL9kPD1JWyuqGxr#-.:/';
		$arr_e = array();  $arr_e1 = array(); $arr_r = array(); $arr_r1 = array();
		for($j = 0; $j < strlen($str); $j++){
			$j_ = $j; if ($j_ < 10) { $j_ = '0'.$j_; }
			$arr_e1[$j] = substr($str,$j,1);
			$arr_e[$j_] = substr($str,$j,1);
			$arr_r1[substr($str,$j,1)] = $j;
			$arr_r[substr($str,$j,1)] = $j_;
		}
		
		$total = 0;
		for($i = 0; $i < strlen($kd_kelas); $i++){
			$total = (int)substr($kd_kelas,$i,1) + $total; 
		} $u = fmod($total,10);
		
		$kd_enc = $arr_e1[$u];
		for($i = 0; $i < strlen($kd_kelas); $i++){
			$k = ($arr_r1[substr($kd_kelas,$i,1)]+$u); if($k < 10) { $k = '0'.$k; }
			$kd_enc .= ''.$k.rand(0,9); 
		} return $kd_enc;
	}
	public function tf_encode2($kd_kelas){ $hasil = ''; #return $kd_kelas;
		$str 	= 'sng3bAdac5UEmQzv2YBTH8CVh7jXpRo0etfOK4MINSlwFZ6iL9kPD1JWyuqGxr#-.:/';
		$arr_e = array();  $arr_e1 = array(); $arr_r = array(); $arr_r1 = array();
		for($j = 0; $j < strlen($str); $j++){
			$j_ = $j; if ($j_ < 10) { $j_ = '0'.$j_; }
			$arr_e1[$j] = substr($str,$j,1);
			$arr_e[$j_] = substr($str,$j,1);
			$arr_r1[substr($str,$j,1)] = $j;
			$arr_r[substr($str,$j,1)] = $j_;
		}
		
		$total = 0;
		for($i = 0; $i < strlen($kd_kelas); $i++){
			$total = (int)substr($kd_kelas,$i,1) + $total; 
		} $u = fmod($total,10);
		
		$kd_enc = $arr_e1[$u];
		for($i = 0; $i < strlen($kd_kelas); $i++){
			$k = ($arr_r1[substr($kd_kelas,$i,1)]+$u); if($k < 10) { $k = '0'.$k; }
			$kd_enc .= ''.$k.rand(0,9); 
		} return $kd_enc;
	}
	public function set_kelas_user(){
			
		
			$cek=$this->daftarkelas();

			$cek2=$this->api->get_api_json(URL_API_SIA.'sia_mahasiswa/data_search', 'POST', array('api_kode'=>28000, 'api_subkode' => 7, 'api_search' => array($this->session->userdata('id_user'))));
			if(isset($cek2)){
				$this->dpa_mhs=$cek2;
			}
		
		
					if(isset($cek)){
						 foreach($cek as $d){
						 	array_push($this->user_kelas,$d['KD_KELAS']);
						 }
						  
						  	array_push($this->user_kelas,'DPA'.$this->dpa_mhs[0]['NIP']);
						  			$this->block_kelas=array();
						  			$this->block_kelas_data=array();
									for($i=0;$i<count($this->user_kelas);$i++){
										if($this->is_blocked($this->user,$this->user_kelas[$i])){
											array_push($this->block_kelas,$this->user_kelas[$i]);
											$data=$this->get_blocked_data($this->user,$this->user_kelas[$i]);
											if(isset($data)){
												$this->block_kelas_data[$this->user_kelas[$i]][0]=$data[0]->ID_DOSEN;
												$this->block_kelas_data[$this->user_kelas[$i]][1]=$data[0]->WAKTU_BLOCK;
											}

										}
									}
								
						  
					}

		//path
					if(!$this->session->userdata('path1')){
						if($status == 'mhs')
						{

							$mhs=$this->s00_lib_api->get_api_json(URL_API_SIA.'sia_mahasiswa/data_search', 'POST', array('api_kode'=>26000, 'api_subkode' => 10, 'api_search' => array($this->session->userdata('id_user'))));
							$this->session->set_userdata('path1',$mhs[0]['KD_FAK'].'/'.$mhs[0]['KD_PRODI']);
							
						}else if($status=='staff'){
							$st=$this->s00_lib_api->get_api_json(URL_API_SIA.'sia_dosen/data_search', 'POST', array('api_kode'=>20000, 'api_subkode' => 3, 'api_search' => array($this->session->userdata('id_user'))));
							$this->session->set_userdata('path1',$st[0]['KD_FAK'].'/'.$st[0]['KD_PRODI']);
							
						}
					}

					if(!$this->session->userdata('data_kd')){
						if($this->session->userdata('status')=='staff'){
							$st=$this->s00_lib_api->get_api_json(URL_API_SIA.'sia_dosen/data_search', 'POST', array('api_kode'=>20000, 'api_subkode' => 3, 'api_search' => array($this->session->userdata('id_user'))));
							if(isset($st))
							$this->session->set_userdata('data_kd',array($st[0]['KD_PRODI'],$st[0]['KD_JURUSAN'],$st[0]['KD_FAK']));
						}else if($this->session->userdata('status')=='mhs'){
							$mhs=$this->s00_lib_api->get_api_json(URL_API_SIA.'sia_mahasiswa/data_search', 'POST', array('api_kode'=>26000, 'api_subkode' => 10, 'api_search' => array($this->session->userdata('id_user'))));
							if(isset($mhs))
							$this->session->set_userdata('data_kd',array($mhs[0]['KD_PRODI'],$mhs[0]['KD_JURUSAN'],$mhs[0]['KD_FAK']));
						}
		
					}

		}
	function file($id=0){
		echo $this->session->userdata('log') ;
		if ($this->session->userdata('log')=='in') {
			  $username = 'admin';
				$password = '1234';
				
				$this->load->library('curl');
				// Optional, delete this line if your API is open
				$this->curl->http_login($username, $password);
				
				
				$api=APP_SERVICE.'api/file/id/'.$id;
				
				$this->curl->create($api);
				$exc=json_decode($this->curl->execute());
				//echo __DIR__;
				//----cek hak akses------------------------	
				$api=APP_SERVICE.'api/post/id_post/'.$exc->ID_POST;
				
				$this->curl->create($api);
				$post=json_decode($this->curl->execute());
				
				//------------------------------

		if($id!=0){
			$file='./'.$exc->PATH;
			if (file_exists($file)) {
				if(preg_match('([^.]+(\.(?i)(jpeg|jpg|png|gif|bmp))$)',$exc->NAMA_FILE)!=1){

						//counter--
							$api=APP_SERVICE.'api/file_counter/id/'.$id.'/id_user/'.$this->user.'/waktu/'.$this->get_time();
					
							$this->curl->create($api);
							$this->curl->execute();
						//---------
				    header('Content-Description: File Transfer');
				    header('Content-Type: application/octet-stream');
				    header('Content-Disposition: attachment; filename='.basename($file));
				    header('Expires: 0');
				    header('Cache-Control: must-revalidate');
				    header('Pragma: public');
				    header('Content-Length: ' . filesize($file));
				     ob_clean();
	            	flush();
				    readfile($file);
				    exit;
				}else{

				    header('Content-Type: image/jpeg');
				   
				    readfile($file);
				    exit;
				}
			}else{
				//cek blob
				$api=APP_SERVICE.'api_blob/cek_blob/tb/post_file_blob/id/'.$id;
				$this->curl->create($api);
				$ckb=$this->curl->execute();
				if($ckb=="ok"){
					//counter--
							$api=APP_SERVICE.'api/file_counter/id/'.$id.'/id_user/'.$this->user.'/waktu/'.$this->get_time();
					
							$this->curl->create($api);
							$this->curl->execute();
						//---------
					
					$api=APP_SERVICE.'api_blob/file_blob/table/post_file_blob/id/'.$id;
					$this->curl->create($api);
					if(preg_match('([^.]+(\.(?i)(jpeg|jpg|png|gif|bmp))$)',$exc->NAMA_FILE)!=1){
						

						//file biasa
						header('Content-Description: File Transfer');
			    		header('Content-Type: '.$exc->TYPE_FILE);
			    		header('Content-Disposition: attachment; filename='.$exc->NAMA_FILE);
			    		echo $this->curl->execute();

					}else{
						//file gambar
						header('Content-Type: image/jpeg');
						echo $this->curl->execute();
					}
				}else{
					echo "File tidak ditemukan";
				}
			}
		}
		}


				
	}
	public function is_blocked($user,$kelas){
		 $username = 'admin';
				$password = '1234';
				
				$this->load->library('curl');
				// Optional, delete this line if your API is open
				$this->curl->http_login($username, $password);
				$api=APP_SERVICE.'api/blocked/id_user/'.$user.'/id_kelas/'.$kelas;				
						$this->curl->create($api);
						$res=json_decode($this->curl->execute());
						if(isset($res)){
							return true;
						}else{
							return false;
						}
	}

	public function get_blocked_data($user,$kelas){
		 $username = 'admin';
				$password = '1234';
				
				$this->load->library('curl');
				// Optional, delete this line if your API is open
				$this->curl->http_login($username, $password);
				$api=APP_SERVICE.'api/blocked/id_user/'.$user.'/id_kelas/'.$kelas;				
						$this->curl->create($api);
						$res=json_decode($this->curl->execute());
						if(isset($res)){
							return $res;
						}else{
							return false;
						}
	} 
	function get_time(){
		$api=APP_SERVICE.'api/waktu';
		$this->curl->create($api);
		$res = json_decode($this->curl->execute());
		if(isset($res))
			return $res->tm;
		else
			return null;
	}
}

/* WIHIKAN MAWI WIJNA 05-11-2012 */
