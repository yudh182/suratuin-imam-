<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*
*/
class Mhs extends My_Controller
{
    private $api_mhs = '/mhs/api/get_mhs.php?nim=';
    private $apinomor_unit  = '/mhs/api/test.php?kd_unit=';
    private $api_kdfak ='&kd_fak=';
    private $api_kdkla  ='&kd_kla=';
	function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('log') != 'in'){
           redirect(base_url("login"));
       }	
       $this->load->model('Api_model');
        $this->load->model('Convert_model');
        $this->load->model('Surat_model');
        $this->load->library('pdf_surat');
        $this->load->database();
        $this->load->helper('url');
       
    }
     function cobacoy(){
        echo $this->Api_model->get_makul_smt($this->session->userdata('username'));
    }
    function sks(){
        $jsonku = file_get_contents(base_url().$this->api_mhs.$this->session->userdata('username'));
        $mahasiswa = json_decode($jsonku,true);
        return $mahasiswa[0]['SKS_KUM'];
    }
	function view($halaman = 'home'){

	$foto = null;
        $foto = $this->session->userdata('username');
        $data['foto'] = $foto;
        $data['onkp']= $this->Api_model->get_makul_smt($this->session->userdata('username'));
        $data['luluskah'] = $this->session->userdata('status');
		$data['nama']     = $this->session->userdata('nama');
        $data['nim']     = $this->session->userdata('username');
        $data['title1'] = "Cetak Surat";
        $crumb['title1'] = "Cetak Surat";
        $crumb['submenu'] = $this->Convert_model->submenu($halaman);
		$data['submenu1'] = "Surat Keterangan Masih Kuliah";
		$data['submenu2'] = "Surat Keterangan Tidak Menerima Beasiswa";
		$data['submenu3'] = "Surat Keterangan Berkelakuan Baik";
		$data['submenu4'] = "Surat Keterangan Bebas Teori";
		$data['submenu5'] = "Surat Keterangan Telah Munaqosah";
		$data['submenu6'] = "Surat Keterangan Lulus";
		$data['submenu7'] = "Surat Keterangan Pindah Studi";
		$data['submenu8'] = "Surat Keterangan Reviewer Penelitian";
		$data['submenu9'] = "Surat Ijin Penelitian";
		$data['submenu10'] = "Surat Studi Pendahuluan";
		$data['submenu11'] = "Surat Ijin Validasi Instrumen";
		$data['submenu12'] = "Surat Ijin Observasi";
		$data['submenu13'] = "Surat Ijin Wawancara";
		$data['submenu14'] = "Surat Permohonan Kerja Praktik";
		$data['submenu15'] = "Surat Permohonan Beasiswa Kuliah";
		$data['submenu16'] = "Surat Permohonan Dispensasi Kuliah";
        $data['submenu17'] = "Surat Permohonan Data(Pengambilan Data)";
        
        $data['cuti'] = $this->session->userdata('status');
        $data['sks'] = $this->sks();
        $this->load->view('pages/header');
        
        if ($halaman=='home'){
             $this->load->view('pages/sidebar',$data);
             
        }
        else if ($halaman=='cetak_surat'){
            $this->load->view('pages/sub_sidebar',$data);
           
        }
        else{
            $this->load->view('pages/sub_sidebar',$data);
            $this->load->view('pages/crumbs',$crumb);
         }
        
        if($this->session->userdata('status') == 'Cuti' && $halaman !='home' && $halaman != 'cetak_surat'){
            $this->load->view('pages/cuti',$data);          
        }
        else if($this->session->userdata('status') == 'Lulus' && $halaman !='home' && $halaman != 'cetak_surat' && $halaman != 'ket_lulus'){
            $this->load->view('pages/cuti',$data);          
        }
        
        else{
            $this->load->view('pages/'.$halaman,$data);
            
        }

       
        $this->load->view('pages/footer');
	}
    function print_kelbaik(){
        $jsonku = file_get_contents(base_url().$this->api_mhs.$this->session->userdata('username'));
        $mahasiswa = json_decode($jsonku,true);
        $kd_fak = $mahasiswa[0]['KD_FAK'];
        $kd_kla  = 'KM.02.2';
    
        //$id_ttd = $this->Convert_model->get_idttd($kd_fak, $kd_kla);
        $id_jab = $this->Convert_model->get_idjabatan($kd_fak,$kd_kla);
        $pejabat = $this->Api_model->get_pejabat($id_jab);
        $data['ttd']     = $pejabat[0]['NM_PGW_F'];
        $data['kop_tlp'] = $this->Convert_model->tlp($kd_fak);
        $data['kop_web'] = $this->Convert_model->web($kd_fak);
        $data['kop_fkl'] = $mahasiswa[0]['NM_FAK_J'];
        $data['kd_psd']  = $this->Convert_model->kd_psd($kd_fak,$kd_kla);
        $kd_unit= $this->Convert_model->kd_unit($kd_fak);

        $data['nama']= $mahasiswa[0]['NAMA'];
        $data['nim']= $mahasiswa[0]['NIM'];
        $data['smt']= $mahasiswa[0]['JUM_SMT'];
        $data['prodi']= $mahasiswa[0]['NM_PRODI'];
        $data['fkl']= $mahasiswa[0]['NM_FAK'];
        $data['thn_akademik']= date("Y");
        $data['jenis_surat'] = 'SURAT KETERANGAN KELAKUAN BAIK';
    $jsondata = file_get_contents(base_url().$this->apinomor_unit.$kd_unit.$this->api_kdfak.$data['kd_psd'].$this->api_kdkla.$kd_kla);
        $m = json_decode($jsondata);
        $y = json_decode($m,true);
        
        $no_urut = $y['NO_URUT'];
        $a=explode("/", $y['NO_SURAT']);
        $un_no= $a[1];
        $psd = $a[2];
        $data['dd'] = date("d");
        $mm = $a[4];
        $data['bulan'] = $this->Convert_model->ubah_bulan($a[4]);
        $data['yy'] = $a[5];
        $data['nomor'] = 'B-'.$no_urut.'/'.$un_no.'/'.$psd.'/'.$kd_kla.'/'.$mm.'/'.$data['yy'];
      
        $this->load->view('report/v_kelbaik',$data);
    }
   
    public function print_aktifkuliah(){

        $jsonku = file_get_contents(base_url().$this->api_mhs.$this->session->userdata('username'));
        $mahasiswa = json_decode($jsonku,true);
        $kd_kla  = 'KM.00.4';
        
        $kd_fak = $mahasiswa[0]['KD_FAK'];
        
       
        $id_jab = $this->Convert_model->get_idjabatan($kd_fak,$kd_kla);
        $pejabat = $this->Api_model->get_pejabat($id_jab);
        $data['ttd']     = $pejabat[0]['NM_PGW_F'];
        $data['kop_tlp'] = $this->Convert_model->tlp($kd_fak);
        $data['kop_web'] = $this->Convert_model->web($kd_fak);
        $data['kop_fkl'] = $mahasiswa[0]['NM_FAK_J'];
        $data['kd_psd']  = $this->Convert_model->kd_psd($kd_fak,$kd_kla);
        $kd_unit= $this->Convert_model->kd_unit($kd_fak);

        
        $data['nama']= $mahasiswa[0]['NAMA'];
        $data['nim']= $mahasiswa[0]['NIM'];
        $data['tmp_lahir']= $mahasiswa[0]['TMP_LAHIR'];
        $data['tgl_lhr']= $mahasiswa[0]['TGL_LAHIR'];
        $data['smt']= $mahasiswa[0]['JUM_SMT'];
        $data['prodi']= $mahasiswa[0]['NM_PRODI'];
        $data['fkl']= $mahasiswa[0]['NM_FAK'];
        $data['thn_akademik']= date("Y");
        $data['keperluan'] = $this->input->post('perlu');
        $data['jenis_surat'] = 'SURAT KETERANGAN MASIH KULIAH';

        
        $jsondata = file_get_contents(base_url().$this->apinomor_unit.$kd_unit.$this->api_kdfak.$data['kd_psd'].$this->api_kdkla.$kd_kla);
        $m = json_decode($jsondata);
        $y = json_decode($m,true);
        
        $no_urut = $y['NO_URUT'];
        $a=explode("/", $y['NO_SURAT']);
        $un_no= $a[1];
        $psd = $a[2];
        $data['dd'] = date("d");
        $mm = $a[4];
        $data['bulan'] = $this->Convert_model->ubah_bulan($a[4]);
        $data['yy'] = $a[5];
        $data['nomor'] = 'B-'.$no_urut.'/'.$un_no.'/'.$psd.'/'.$kd_kla.'/'.$mm.'/'.$data['yy'];
        $this->load->view('report/v_masihkuliah',$data);      
    }
    
     
     function print_lulus(){
        $jsonku = file_get_contents(base_url().$this->api_mhs.$this->session->userdata('username'));
        $mahasiswa = json_decode($jsonku,true);

        $kd_fak = $mahasiswa[0]['KD_FAK'];
        $kd_kla  = 'PP.01.2';
        $id_jab = $this->Convert_model->get_idjabatan($kd_fak,$kd_kla);
        $pejabat = $this->Api_model->get_pejabat($id_jab);
        $data['ttd']     = $pejabat[0]['NM_PGW_F'];
        $data['kop_tlp'] = $this->Convert_model->tlp($kd_fak);
        $data['kop_web'] = $this->Convert_model->web($kd_fak);
        $data['kop_fkl'] = $mahasiswa[0]['NM_FAK_J'];
        $data['kd_psd']  = $this->Convert_model->kd_psd($kd_fak,$kd_kla);
        $kd_unit= $this->Convert_model->kd_unit($kd_fak);
        

        $data['nama']= $mahasiswa[0]['NAMA'];
        $data['nim']= $mahasiswa[0]['NIM'];
        $data['prodi']= $mahasiswa[0]['NM_PRODI'];
        $data['thn_akademik']= date("Y");
        $data['ipk']= $mahasiswa[0]['IP_KUM'];
        $data['fkl']= $mahasiswa[0]['NM_FAK'];
        $data['jenis_surat'] = 'SURAT KETERANGAN LULUS';

        $jsondata = file_get_contents(base_url().$this->apinomor_unit.$kd_unit.$this->api_kdfak.$data['kd_psd'].$this->api_kdkla.$kd_kla);
        $m = json_decode($jsondata);
        $y = json_decode($m,true);
        $no_urut = $y['NO_URUT'];
        $a=explode("/", $y['NO_SURAT']);
        $un_no= $a[1];
        $psd = $a[2];
        $data['dd'] = date("d");
        $mm = $a[4];
        $data['bulan'] = $this->Convert_model->ubah_bulan($a[4]);
        $data['yy'] = $a[5];
        $data['nomor'] = 'B-'.$no_urut.'/'.$un_no.'/'.$psd.'/'.$kd_kla.'/'.$mm.'/'.$data['yy'];
        $this->load->view('report/v_lulus',$data);
    }
    function print_studi_pen(){
        $jsonku = file_get_contents(base_url().$this->api_mhs.$this->session->userdata('username'));
        $mahasiswa = json_decode($jsonku,true);

        $kd_fak = $mahasiswa[0]['KD_FAK'];
        $kd_kla  = 'PN.01.01';
        $id_jab = $this->Convert_model->get_idjabatan($kd_fak,$kd_kla);
        $pejabat = $this->Api_model->get_pejabat($id_jab);
        $data['ttd']     = $pejabat[0]['NM_PGW_F'];

        $data['kop_tlp'] = $this->Convert_model->tlp($kd_fak);
        $data['kop_web'] = $this->Convert_model->web($kd_fak);
        $data['kop_fkl'] = $mahasiswa[0]['NM_FAK_J'];
        $data['kd_psd']  = $this->Convert_model->kd_psd($kd_fak,$kd_kla);
        $kd_unit= $this->Convert_model->kd_unit($kd_fak);

        $data['nama']= $mahasiswa[0]['NAMA'];
        $data['nim']= $mahasiswa[0]['NIM'];
        $data['smt']= $mahasiswa[0]['JUM_SMT'];
        $data['prodi']= $mahasiswa[0]['NM_PRODI'];
        $data['thn_akademik']= date("Y");
        $data['fkl']= $mahasiswa[0]['NM_FAK'];
        $data['jabatan'] = $this->input->post('jabatan');
        $data['inst'] = $this->input->post('instansi');
        $data['tmpt'] = $this->input->post('tempat');
        $data['tgl'] = $this->input->post('tgl');
        $data['judul'] = $this->input->post('judul');
        $data['jenis_surat']='SURAT PERMOHONAN STUDI PENDAHULUAN';
        

        $jsondata = file_get_contents(base_url().$this->apinomor_unit.$kd_unit.$this->api_kdfak.$data['kd_psd'].$this->api_kdkla.$kd_kla);
        $m = json_decode($jsondata);
        $y = json_decode($m,true);
        $no_urut = $y['NO_URUT'];
        $a=explode("/", $y['NO_SURAT']);
        $un_no= $a[1];
        $psd = $a[2];
        $data['dd'] = date("d");
        $mm = $a[4];
        $data['bulan'] = $this->Convert_model->ubah_bulan($a[4]);
        $data['yy'] = $a[5];
        $data['nomor'] = 'B-'.$no_urut.'/'.$un_no.'/'.$psd.'/'.$kd_kla.'/'.$mm.'/'.$data['yy'];

        $this->load->view('report/v_studi_pen',$data);
    }
    function print_tdk_bea(){
        $jsonku = file_get_contents(base_url().$this->api_mhs.$this->session->userdata('username'));
        $mahasiswa = json_decode($jsonku,true);

        $kd_fak = $mahasiswa[0]['KD_FAK'];
        $kd_kla  = 'KM.02.02';
        $id_jab = $this->Convert_model->get_idjabatan($kd_fak,$kd_kla);
        $pejabat = $this->Api_model->get_pejabat($id_jab);
        $data['ttd']     = $pejabat[0]['NM_PGW_F'];

        $data['kop_tlp'] = $this->Convert_model->tlp($kd_fak);
        $data['kop_web'] = $this->Convert_model->web($kd_fak);
        $data['kop_fkl'] = $mahasiswa[0]['NM_FAK_J'];
        $data['kd_psd']  = $this->Convert_model->kd_psd($kd_fak,$kd_kla);
        $kd_unit= $this->Convert_model->kd_unit($kd_fak);

        $data['nama']= $mahasiswa[0]['NAMA'];
        $data['nim']= $mahasiswa[0]['NIM'];
        $data['smt']= $mahasiswa[0]['JUM_SMT'];
        $data['prodi']= $mahasiswa[0]['NM_PRODI'];
        $data['thn_akademik']= date("Y");
        $data['fkl']= $mahasiswa[0]['NM_FAK'];
        $data['isi'] = $this->input->post('bea');
        $data['jenis_surat']='SURAT KETERANGAN TIDAK SEDANG MENERIMA BEASISWA';
       
        $jsondata = file_get_contents(base_url().$this->apinomor_unit.$kd_unit.$this->api_kdfak.$data['kd_psd'].$this->api_kdkla.$kd_kla);
        $m = json_decode($jsondata);
        $y = json_decode($m,true);
        $no_urut = $y['NO_URUT'];
        $a=explode("/", $y['NO_SURAT']);
        $un_no= $a[1];
        $psd = $a[2];
        $data['dd'] = date("d");
        $mm = $a[4];
        $data['bulan'] = $this->Convert_model->ubah_bulan($a[4]);
        $data['yy'] = $a[5];
        $data['nomor'] = 'B-'.$no_urut.'/'.$un_no.'/'.$psd.'/'.$kd_kla.'/'.$mm.'/'.$data['yy'];

        $this->load->view('report/v_tdk_bea',$data);
    }
    function print_kp(){
        $jsonku = file_get_contents(base_url().$this->api_mhs.$this->session->userdata('username'));
        $mahasiswa = json_decode($jsonku,true);

        $kd_fak = $mahasiswa[0]['KD_FAK'];
        $kd_kla  = 'PP.08.1';



        $id_jab = $this->Convert_model->get_idjabatan($kd_fak,$kd_kla);
        $pejabat = $this->Api_model->get_pejabat($id_jab);
        $data['ttd']     = $pejabat[0]['NM_PGW_F'];

        $data['kop_tlp'] = $this->Convert_model->tlp($kd_fak);
        $data['kop_web'] = $this->Convert_model->web($kd_fak);
        $data['kop_fkl'] = $mahasiswa[0]['NM_FAK_J'];
        $data['kd_psd']  = $this->Convert_model->kd_psd($kd_fak,$kd_kla);
        $kd_unit= $this->Convert_model->kd_unit($kd_fak);

        $data['nama']= $mahasiswa[0]['NAMA'];
        $data['nim']= $mahasiswa[0]['NIM'];
        $data['prodi']= $mahasiswa[0]['NM_PRODI'];
        $data['fkl']= $mahasiswa[0]['NM_FAK'];
        $data['jabatan'] = $this->input->post('jabatan');
        $data['inst'] = $this->input->post('instansi');
        $data['tmpt'] = $this->input->post('tempat');
        $data['minat'] = $this->input->post('minat'); 
        $data['jenis_surat']='SURAT PERMOHONAN KERJA PRAKTEK';   

        $jsondata = file_get_contents(base_url().$this->apinomor_unit.$kd_unit.$this->api_kdfak.$data['kd_psd'].$this->api_kdkla.$kd_kla);
        $m = json_decode($jsondata);
        $y = json_decode($m,true);
        $no_urut = $y['NO_URUT'];
        $a=explode("/", $y['NO_SURAT']);
        $un_no= $a[1];
        $psd = $a[2];
        $data['dd'] = date("d");
        $mm = $a[4];
        $data['bulan'] = $this->Convert_model->ubah_bulan($a[4]);
        $data['yy'] = $a[5];
        $data['nomor'] = 'B-'.$no_urut.'/'.$un_no.'/'.$psd.'/'.$kd_kla.'/'.$mm.'/'.$data['yy'];

        $this->load->view('report/v_kp',$data);

    }
    function print_obs(){
        $jsonku = file_get_contents(base_url().$this->api_mhs.$this->session->userdata('username'));
        $mahasiswa = json_decode($jsonku,true);

        $kd_fak = $mahasiswa[0]['KD_FAK'];
        $kd_kla  = 'PP.05.3';
        $id_jab = $this->Convert_model->get_idjabatan($kd_fak,$kd_kla);
        $pejabat = $this->Api_model->get_pejabat($id_jab);
        $data['ttd']     = $pejabat[0]['NM_PGW_F'];

        $data['kop_tlp'] = $this->Convert_model->tlp($kd_fak);
        $data['kop_web'] = $this->Convert_model->web($kd_fak);
        $data['kop_fkl'] = $mahasiswa[0]['NM_FAK_J'];
        $data['kd_psd']  = $this->Convert_model->kd_psd($kd_fak,$kd_kla);
        $kd_unit= $this->Convert_model->kd_unit($kd_fak);

        $data['nama']= $mahasiswa[0]['NAMA'];
        $data['nim']= $mahasiswa[0]['NIM'];
        $data['smt']= $mahasiswa[0]['JUM_SMT'];
        $data['prodi']= $mahasiswa[0]['NM_PRODI'];
        $data['thn_akademik']= date("Y");
        $data['fkl']= $mahasiswa[0]['NM_FAK'];
        $data['kepada'] = $this->input->post('jabatan');
        $data['instansi'] = $this->input->post('inst');
        $data['tempat'] = $this->input->post('tempat');
        $data['mtkuliah'] = $this->input->post('mtkuliah');
        $data['tgl'] = $this->input->post('tgl');
        $data['jenis_surat']='SURAT IJIN OBSERVASI';



        $jsondata = file_get_contents(base_url().$this->apinomor_unit.$kd_unit.$this->api_kdfak.$data['kd_psd'].$this->api_kdkla.$kd_kla);
        $m = json_decode($jsondata);
        $y = json_decode($m,true);
        $no_urut = $y['NO_URUT'];
        $a=explode("/", $y['NO_SURAT']);
        $un_no= $a[1];
        $psd = $a[2];
        $data['dd'] = date("d");
        $mm = $a[4];
        $data['bulan'] =$this->Convert_model->ubah_bulan($a[4]);
        $data['yy'] = $a[5];
        $data['nomor'] = 'B-'.$no_urut.'/'.$un_no.'/'.$psd.'/'.$kd_kla.'/'.$mm.'/'.$data['yy'];

        
        $data['nimA']=$this->input->post('mhsa');
        $jsonka = file_get_contents(base_url().$this->api_mhs.$data['nimA']);
        $mahasiswaA = json_decode($jsonka,true);
        $data['namaA']= $mahasiswaA[0]['NAMA'];

        $data['nimB']=$this->input->post('mhsb');
        $jsonkb = file_get_contents(base_url().$this->api_mhs.$data['nimB']);
        $mahasiswaB = json_decode($jsonkb,true);
        $data['namaB']= $mahasiswaB[0]['NAMA'];
        $this->load->view('report/v_obs',$data);

    }
    function print_pindah(){
        $jsonku = file_get_contents(base_url().$this->api_mhs.$this->session->userdata('username'));
        $mahasiswa = json_decode($jsonku,true);

        $kd_fak = $mahasiswa[0]['KD_FAK'];
        $kd_kla  = 'KM.002';
        $id_jab = $this->Convert_model->get_idjabatan($kd_fak,$kd_kla);
        $pejabat = $this->Api_model->get_pejabat($id_jab);
        $data['ttd']     = $pejabat[0]['NM_PGW_F'];

        $data['kop_tlp'] = $this->Convert_model->tlp($kd_fak);
        $data['kop_web'] = $this->Convert_model->web($kd_fak);
        $data['kop_fkl'] = $mahasiswa[0]['NM_FAK_J'];
        $data['kd_psd']  = $this->Convert_model->kd_psd($kd_fak,$kd_kla);
        $kd_unit= $this->Convert_model->kd_unit($kd_fak);
        $data['nama']= $mahasiswa[0]['NAMA'];
        $data['nim']= $mahasiswa[0]['NIM'];
        $data['smt']= $mahasiswa[0]['JUM_SMT'];
        $data['prodi']= $mahasiswa[0]['NM_PRODI'];
        $data['thn_akademik']= date("Y");
        $data['fkl']= $mahasiswa[0]['NM_FAK'];
        $data['isi'] = $this->input->post('content');
        $data['masuk']= $mahasiswa[0]['TGL_MASUK'];

        $jsondata = file_get_contents(base_url().$this->apinomor_unit.$kd_unit.$this->api_kdfak.$data['kd_psd'].$this->api_kdkla.$kd_kla);
        $m = json_decode($jsondata);
        $y = json_decode($m,true);
        $no_urut = $y['NO_URUT'];
        $a=explode("/", $y['NO_SURAT']);
        $un_no= $a[1];
        $psd = $a[2];
        $data['dd'] = date("d");
        $mm = $a[4];
        $data['bulan'] = $this->Convert_model->ubah_bulan($a[4]);
        $data['yy'] = $a[5];
        $data['nomor'] = 'B-'.$no_urut.'/'.$un_no.'/'.$psd.'/'.$kd_kla.'/'.$mm.'/'.$data['yy'];
        $data['jenis_surat']='SURAT KETERANGAN PINDAH STUDI';
        $this->load->view('report/v_pindah',$data);
    }
    function print_hbs_teori(){
        $jsonku = file_get_contents(base_url().$this->api_mhs.$this->session->userdata('username'));
        
        $mahasiswa = json_decode($jsonku,true);
         
        $kd_fak = $mahasiswa[0]['KD_FAK'];
        $kd_kla  = 'PP.01.1';
        $id_jab = $this->Convert_model->get_idjabatan($kd_fak,$kd_kla);
        $pejabat = $this->Api_model->get_pejabat($id_jab);
        $data['ttd']     = $pejabat[0]['NM_PGW_F'];

        $data['kop_tlp'] = $this->Convert_model->tlp($kd_fak);
        $data['kop_web'] = $this->Convert_model->web($kd_fak);
        $data['kop_fkl'] = $mahasiswa[0]['NM_FAK_J'];
        $data['kd_psd']  = $this->Convert_model->kd_psd($kd_fak,$kd_kla);
        $kd_unit= $this->Convert_model->kd_unit($kd_fak);

        $data['nama']= $mahasiswa[0]['NAMA'];
        $data['nim']= $mahasiswa[0]['NIM'];
        $data['smt']= $mahasiswa[0]['JUM_SMT'];
        $data['fkl']= $mahasiswa[0]['NM_FAK'];
        $data['prodi']= $mahasiswa[0]['NM_PRODI'];
        $data['alamat']= $mahasiswa[0]['MAHASISWA'];
        $data['telp']= $mahasiswa[0]['HP_MHS'];
        $data['ip']= $mahasiswa[0]['IP_KUM'];

        $jsondata = file_get_contents(base_url().$this->apinomor_unit.$kd_unit.$this->api_kdfak.$data['kd_psd'].$this->api_kdkla.$kd_kla);
        $m = json_decode($jsondata);
        $y = json_decode($m,true);
        
        $no_urut = $y['NO_URUT'];
        $a=explode("/", $y['NO_SURAT']);
        $un_no= $a[1];
        $psd = $a[2];
        $data['dd'] = date("d");
        $mm = $a[4];
        $data['bulan'] = $this->Convert_model->ubah_bulan($a[4]);
        $data['yy'] = $a[5];
        $data['nomor'] = 'B-'.$no_urut.'/'.$un_no.'/'.$psd.'/'.$kd_kla.'/'.$mm.'/'.$data['yy'];
        $data['jenis_surat']= 'SURAT KETERANGAN BEBAS TEORI';
        $data['pengecek'] = $this->input->post('pengecek');
        $this->load->view('report/v_hbs_teori',$data);

    }
    function print_penelitian(){
        $jsonku = file_get_contents(base_url().$this->api_mhs.$this->session->userdata('username'));
        $mahasiswa = json_decode($jsonku,true);
        $kd_fak = $mahasiswa[0]['KD_FAK'];
        $kd_kla  = 'PN.01.01';
        $id_jab = $this->Convert_model->get_idjabatan($kd_fak,$kd_kla);
        $pejabat = $this->Api_model->get_pejabat($id_jab);
        $data['ttd']     = $pejabat[0]['NM_PGW_F'];
        
        $data['kop_tlp'] = $this->Convert_model->tlp($kd_fak);
        $data['kop_web'] = $this->Convert_model->web($kd_fak);
        $data['kop_fkl'] = $mahasiswa[0]['NM_FAK_J'];
        $data['kd_psd']  = $this->Convert_model->kd_psd($kd_fak,$kd_kla);
        $kd_unit= $this->Convert_model->kd_unit($kd_fak);

        $data['nama']= $mahasiswa[0]['NAMA'];
        $data['nim']= $mahasiswa[0]['NIM'];
        $data['prodi']= $mahasiswa[0]['NM_PRODI'];
        $data['smt']= $mahasiswa[0]['JUM_SMT'];
        $data['thn_akademik']= date("Y");
        $data['fkl']= $mahasiswa[0]['NM_FAK'];
        $data['kepada'] = $this->input->post('kepada');
        $data['tempat'] = $this->input->post('tempat');
        $data['instansi'] = $this->input->post('instansi');
        $data['judul'] = $this->input->post('judul');
        $data['metode'] = $this->input->post('metode');
        $data['tmpt_penelitian'] = $this->input->post('tmpt_penelitian');
        $data['tgl_mulai'] = date("d-m-Y", strtotime($this->input->post('tgl_mulai')));
        $data['tgl_berakhir'] = date("d-m-Y", strtotime($this->input->post('tgl_berakhir')));
        $data['jenis_surat'] = 'SURAT IJIN PENELITIAN';

        $jsondata = file_get_contents(base_url().$this->apinomor_unit.$kd_unit.$this->api_kdfak.$data['kd_psd'].$this->api_kdkla.$kd_kla);
        $m = json_decode($jsondata);
        $y = json_decode($m,true);
        
        $no_urut = $y['NO_URUT'];
        $a=explode("/", $y['NO_SURAT']);
        $un_no= $a[1];
        $psd = $a[2];
        $data['dd'] = date("d");
        $mm = $a[4];
        $data['bulan'] =$this->Convert_model->ubah_bulan($a[4]);
        $data['yy'] = $a[5];
        $data['nomor'] = 'B-'.$no_urut.'/'.$un_no.'/'.$psd.'/'.$kd_kla.'/'.$mm.'/'.$data['yy'];
       
        $this->load->view('report/v_penelitian',$data);
    }
    
//======================================================================================================

    public function get_nomor(){
         $jsondata = file_get_contents(base_url().'/surat/mahasiswa/test');
     
        $mahasiswa = json_decode($jsondata);
      
        $y = json_decode($mahasiswa,true);
        $data['no_urut'] = $y['NO_URUT'];
        $data['un_no'] = $a[1];
        $data['tst'] = $a[2];
        $data['dd'] = date("d");
        $data['mm'] = $a[4];
        $data['yy'] = $a[5];

      //  var_dump($y);
      //  echo "Berhasillll<br>";
      //  echo $y['NO_URUT'].'<br>';
      //  echo $y['NO_SELA'].'<br>';
      //  $a=explode("/", $y['NO_SURAT']);
      //  echo $a[0].'<br>';
      //  echo $a[1].'<br>';
      //  echo $a[2].'<br>';
      // // echo $a[3].'<br>';
      //  echo $a[4].'<br>';
      //  echo $a[5].'<br>';
    }
    
}
	
