<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*
*/
class Login extends My_Controller
{

  private $api_mhs = '/mhs/api/get_mhs.php?nim=';
	function __construct()
	{
		parent::__construct();    
        $this->load->database();
        $this->load->helper('url');
           $this->load->model('Login_model');
           $this->load->model('Surat_model');
	}
	function index()
	{
		$this->load->view('v_login');
	}

  // public function aksi_login(){
  //     $username = $this->input->post('username');
  //     $password = $this->input->post('password');
  //      $jsonku = file_get_contents(base_url().$this->api_mhs.$username);
  //      $mahasiswa = json_decode($jsonku,true);
  //       if ($username==$mahasiswa[0]['NIM']) {
  //           $data_session = array(
                
  //               'username' => $username,
  //               'nama'     => $mahasiswa[0]['NAMA'],
  //               'status'   => $mahasiswa[0]['NM_STATUS'],
  //               'log'   => 'in'
  //           );
  //           $this->session->set_userdata($data_session);
           
  //           redirect(base_url('mhs'));
  //       }
  //       else{
  //          redirect(base_url('login'));
  //       }
  //   }

   public function aksi_login(){
       $this->load->library('Curl'); 
          $jsonku = file_get_contents(base_url().$this->api_mhs.$username);
          $mahasiswa = json_decode($jsonku,true);
    	  $username = $this->input->post('username');
    	  $password = $this->input->post('password');

        $auth = '8f304662ebfee3932f2e810aa8fb628735';
        $api_url = 'http://service.uin-suka.ac.id/servad/adlogauthgr.php?aud='.$auth.'&uss='.$username.'&pss='.$password;
        $hasil = $this->curl->simple_get($api_url, false, array(CURLOPT_USERAGENT => true)); 
        $hasil = json_decode($hasil, true);
        $nama = $hasil[0]['NamaDepan'].' '.$hasil[0]['NamaBelakang'];
        $nim = $hasil[0]['NamaPengguna'];
              
              if(is_array($hasil)){
                  $auth_type = array("MhsGroup","StaffGroup","KartuGroup","OrtuGroup");
                  foreach($auth_type as $auth_type_){
                  for($j1 = 0; $j1 < count($hasil[0]['AnggotaDari'][0]); $j1++){
                    if($hasil[0]['AnggotaDari'][0][$j1] == $auth_type_){
                      $hasil_ = 'succes'; 
                      break;
                    }
                  }
                }
              }

        if ($hasil_ == 'succes') {
            $data_session = array(                
                'username' => $username,
                'pass'     => $password,
                'nama'     => $nama,
                'grup'     => $hasil[0]['AnggotaDari'][0],
                'status'   => $mahasiswa[0]['NM_STATUS'],
                   'sks'      => $mahasiswa[0]['SKS_KUM'],
                   'log'   => 'in'
            );
            $this->session->set_userdata($data_session);
           
            redirect(base_url('mhs'));
        }
        else{
           redirect(base_url('login'));
        }
    }

    function logout(){
    	$this->session->sess_destroy();
    	redirect(base_url('login'));
    }


}
	
