<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* Repo api-api surat pada webservice  tnde (production 2018)
 */
 
 class Api_client_tnde extends MX_Controller {
	
	public function __construct()
	{
		parent::__construct();		
		$this->load->model('common/Mdl_tnde', 'apiconn');
	}

	public function test()
	{
		return "ok go";
	}
	
	/**
	 * Dapatkan data surat keluar tunggal berdasar ID
	 * @param ID_SURAT_KELUAR
	 * @param Mode output
	 * @return default: array. opsi lain: json,xml.
	 */
	public function detail_surat_keluar($id=null, $mode='arr_return')
	{
		if (empty($id))
			return ['error'=> [
				'code' => 400,
				'message' => 'data parameter tidak lengkap'
			]];

		$api_get = $this->apiconn->api_tnde_v3('tnde_surat_keluar/detail_surat_keluar', 'json', 'POST', array('api_kode' => 90001, 'api_subkode' => 2, 'api_search' => array($id)));
		return $api_get;
	}
	
	/**
	* Surat yang dimintakan penandatanganan via email sudah bernomor (status simpan=2)
	*/
	public function psd_email($id)
	{
		$api_post = $this->apiconn->api_tnde_v3('tnde_surat_keluar/email_surat', 'json', 'POST', array('api_kode' => 90000, 'api_subkode' => 1, 'api_search' => array($id)));
		return $api_post;
	}
	

	/**
	* Dapatkan Daftar Penerima Surat Keluar
	* @param ID_SURAT_KEUAR
	*/
	public function get_penerima_sk($id_surat_keluar=null, $status_distribusi='PS')
	{
		if (!empty($status_distribusi)){
			if ($status_distribusi == 'all-group'){
				$kepada = $this->apiconn->api_tnde_v3('tnde_surat_keluar/get_penerima_sk', 'json', 'POST', array('api_kode' => 90001, 'api_subkode' => 1, 'api_search' => array($id_surat_keluar, 'PS')));

	        	$data['penerima']['PS'] = $kepada;
		        $ts = $this->apiconn->api_tnde_v3('tnde_surat_keluar/get_penerima_sk', 'json', 'POST', array('api_kode' => 90001, 'api_subkode' => 1, 'api_search' => array($id_surat_keluar, 'TS')));
		        $data['penerima']['TS'] = $ts;	    	
			} else {
				// $penerima = $this->apiconn->api_tnde_v3('tnde_surat_keluar/get_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($id_surat_keluar, $status_distribusi)));
				$data['surat_keluar']['penerima'] = $api_get = $this->apiconn->api_tnde_v3('tnde_general/get_data', 'json', 'POST', array('api_kode' => 1001, 'api_subkode' => 3, 'api_search' => array('D_PENERIMA_SK', 'ID_SURAT_KELUAR', $id_surat_keluar)));
				$penerima_by_grup = [];
				foreach ($data['surat_keluar']['penerima'] as $k=>$v) {
					if ($v['KD_STATUS_DISTRIBUSI'] == 'PS' && $v['KD_GRUP_TUJUAN'] == 'MHS01'){
						$penerima_by_grup['kepada_mahasiswa'][] = $v;
					} elseif ($v['KD_STATUS_DISTRIBUSI'] == 'PS' && $v['KD_GRUP_TUJUAN'] == 'PJB01'){
						$penerima_by_grup['kepada_pejabat'][] = $v;
					} elseif ($v['KD_STATUS_DISTRIBUSI'] == 'PS' && $v['KD_GRUP_TUJUAN'] == 'PGW01'){
						$penerima_by_grup['kepada_pegawai'][] = $v;
					} elseif ($v['KD_STATUS_DISTRIBUSI'] == 'TS' && $v['KD_GRUP_TUJUAN'] == 'PJB01'){
						$penerima_by_grup['tembusan_internal'][] = $v;
					} elseif ($v['KD_STATUS_DISTRIBUSI'] == 'TS' && $v['KD_GRUP_TUJUAN'] == 'EKS01'){
						$penerima_by_grup['tembusan_eksternal'][] = $v;
					}
				}
				$data['surat_keluar']['penerima_by_grup'] = $penerima_by_grup;	        	
			}

			
	    } else {
	    	$penerima = $this->apiconn->api_tnde_v3('tnde_surat_keluar/get_penerima_sk', 'json', 'POST', array('api_kode' => 90008, 'api_subkode' => 1, 'api_search' => array($id_surat_keluar)));
	    	$data['penerima_sk_for_sm'] = $penerima;

	    }
        return $data;
	}

	public function get_penerima_sk_by_no_surat($no_surat, $status_distribusi='PS', $mode='arr_return')
	{
		$penerima = $this->apiconn->api_tnde_v3('tnde_surat_keluar/get_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 2, 'api_search' => array($no_surat, $status_distribusi)));
		$data['penerima'][] = $penerima;
 				
		return $data; 	
	}

	public function get_surat_keluar($unit_list=null, $jenis_surat_list=null, $status_simpan=null, $per_page=20, $offset=0)
	{
		$unit_tmp = ['UN02006'];
		$unit_list 	= implode_to_string($unit_tmp);

		// $surat_list 	= (!empty($mod['KD_JENIS_SURAT']) ? $mod['KD_JENIS_SURAT'] : array());
		// $not								= array(1, 23);
		// $list									= array_diff($surat_list, $not);
		$jenis_surat_list			= implode(",", ['6','11']);
		$config['base_url'] 		= site_url()."administrasi/arsip";
		$config['per_page'] 		= 20;
		$config['uri_segment'] 	= 4;
		$status_simpan			= "0,1,2,3";

		// $parameter	= array('api_kode' => 90000,'api_subkode' => 1,'api_search' => array($unit_list, $jenis_surat_list, $status_simpan));
		// $tot 				= $this->mdl_tnde->get_api('tnde_surat_keluar/get_surat_keluar','json','POST', $parameter);
		
		// $config['total_rows'] = $tot['TOTAL'];
		
		// $this->pagination->initialize($config);
		// $page = $this->uri->segment(4);
		// if(empty($page)){
		// 	$subkode = 1;
		// }else{
		// 	$subkode = 2;
		// }
		// $data['links'] = $this->pagination->create_links();
		$subkode = 2;
		$parameter = array('api_kode' => 90006, 'api_subkode' => $subkode, 'api_search' => array($unit_list, $jenis_surat_list, $status_simpan, $config['per_page'], $page));
		$data['surat_keluar'] 	= $this->apiconn->api_tnde_v3('tnde_surat_keluar/get_surat_keluar', 'json', 'POST', $parameter);

		return $data;

	}

	public function perbarui_status_simpan($kd_pgw=null, $from_status=null, $to_status=null)
	{

	}
	
	
}