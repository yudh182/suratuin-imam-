<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');

class Api_foto extends MY_Controller {
	
	public function __construct()
	{
		parent::__construct();

		// if ($this->session->userdata('log') !== 'in')
		// 	redirect(site_url('pgw/login?from='.current_url()));

        define('LOGO_UNIT_980',     'http://static.uin-suka.ac.id/foto/unit/980/');
        define('LOGO_UNIT_990',     'http://static.uin-suka.ac.id/foto/unit/990/');
        define('FOTO_PGW_980',      'http://static.uin-suka.ac.id/foto/pgw/980/');
        define('FOTO_MHS_980',      'http://static.uin-suka.ac.id/foto/mhs/980/');  
        define('FOTO_MHS_990',      'http://static.uin-suka.ac.id/foto/mhs/990/');	
        $this->load->helper('common/Tnde_general');			
    }

    // public function foto_mhs($nim)
    // {
    //     $url = 'http://mobile.learning.uin-suka.ac.id/auto_surat/foto_mahasiswa?nim='.$nim;
    //     //header("Content-type: image/jpeg");            
    //     $fot=file_get_contents($url);
    //     //echo $fot;
    //     $type = "jpeg";
    //     $base64 = 'data:image/' . $type . ';base64,' . base64_encode($fot);
    //     //echo '<img src="'.$base64.'" />'; 
    //     return $base64;
    // }

     public function foto_mhs($nim, $ukuran='extra-small', $mode='arr_return')
    {
        $this->load->helper('common/uin_kripto');
        $ukuran = (!empty($ukuran) ? $ukuran : 'extra-small');
        $vid = $nim;
        $size = $ukuran;

        switch ($size) {
            case 'extra-small':
                $size="120";
                break;
            case 'small':
                $size="220";
                break;
            case 'medium':
                $size="360";
                break;
            case 'large':
                $size="560";
                break;            
            default: #null
                $size="120";
                break;
        }
        $urlfoto = FOTO_MHS_980.tg_encode("FOTOAUTO#".$vid."#QL:80#WM:1#SZ:".$size).'.jpg';

        $fot=file_get_contents($urlfoto);

        if ($mode == 'arr_return'){
       //echo $fot;
        $type = "jpeg";
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($fot);
        return $base64;
       } elseif ($mode == 'show_image'){
       	$this->output
        ->set_content_type('jpeg') // You could also use ".jpeg" which will have the full stop removed before looking in config/mimes.php
        ->set_output($fot);
       }
    }

    public function foto_pgw($kd_pegawai, $ukuran='extra-small', $mode='arr_return')
    {
        $ukuran = (!empty($ukuran) ? $ukuran : 'extra-small');
        $this->load->helper('common/uin_kripto');
        $vid = $kd_pegawai;
        $size = $ukuran;

        switch ($size) {
            case 'extra-small':
                $size="120";
                break;
            case 'small':
                $size="220";
                break;
            case 'medium':
                $size="360";
                break;
            case 'large':
                $size="560";
                break;            
            default: #null
                $size="120";
                break;
        }
        $urlfoto = FOTO_PGW_980.tg_encode("FOTOAUTO#".$vid."#QL:80#WM:1#SZ:".$size).'.jpg';

        $fot=file_get_contents($urlfoto);
        //echo $fot;
        $type = "jpeg";
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($fot);
        
        return $base64;        
        
    }


    public function foto_ttd($nip, $wm=1, $size=150)
    {        
        //pemanggilan url tanda tangan pegawai
        $url = tf_encode('FOTOTTD#' . $nip . '#QL:100#WM:'.$wm.'#SZ:'.$size); //default SZ:150
        echo '<img src="'. FOTO_PGW_980.$url.'.jpg">';
    }

    public function logo_unit($unit_id='', $tgl=null)
    {
        $tgl = date('d/m/Y');
        $logo_unit  = tf_encode($unit_id.'#'.$tgl.'#QL:100#WM:0#SZ:240'); //default QL:50#WM:0#SZ:150

        echo '<img src="'. LOGO_UNIT_980.$logo_unit.'.jpg">';
    }
}