
<?php
/* Repo api-api surat pada webservice  tnde (pra produksi 2018)
 */
 
 class Api_client_tnde_staging extends MX_Controller {
	
	public function __construct()
	{
		parent::__construct();		
		$this->load->model('common/Mdl_tnde', 'apiconn');
	}

	public function test()
	{
		return "ok go";
	}
	
	/**
	 * Dapatkan data surat keluar tunggal berdasar ID
	 * @param ID_SURAT_KELUAR
	 * @param Mode output
	 * @return default: array. opsi lain: json,xml.
	 */
	public function detail_surat_keluar($id=null, $mode='arr_return')
	{
		if (empty($id))
			return ['error'=> [
				'code' => 400,
				'message' => 'data parameter tidak lengkap'
			]];

		$api_get = $this->apiconn->api_tnde('tnde_surat_keluar/detail_surat_keluar', 'json', 'POST', array('api_kode' => 90001, 'api_subkode' => 2, 'api_search' => array($id)));
		return $api_get;
	}
	
	/**
	* Surat yang dimintakan penandatanganan via email sudah bernomor (status simpan=2)
	*/
	public function psd_email($id)
	{
		$api_post = $this->apiconn->api_tnde_staging('tnde_surat_keluar/email_surat', 'json', 'POST', array('api_kode' => 90000, 'api_subkode' => 1, 'api_search' => array($id)));
		return $api_post;
	}
	

	/**
	* Dapatkan Daftar Penerima Surat Keluar
	* @param ID_SURAT_KEUAR
	*/
	public function get_penerima_sk($id_surat_keluar=null, $status_distribusi='PS')
	{
		if (!empty($status_distribusi)){
			if ($status_distribusi == 'all-group'){
				$kepada = $this->apiconn->api_tnde_staging('tnde_surat_keluar/get_penerima_sk', 'json', 'POST', array('api_kode' => 90001, 'api_subkode' => 1, 'api_search' => array($id_surat_keluar, 'PS')));

	        	$data['penerima']['PS'] = $kepada;
		        $ts = $this->apiconn->api_tnde_staging('tnde_surat_keluar/get_penerima_sk', 'json', 'POST', array('api_kode' => 90001, 'api_subkode' => 1, 'api_search' => array($id_surat_keluar, 'TS')));
		        $data['penerima']['TS'] = $ts;	    	
			}			
	    } else {
	    	$penerima = $this->apiconn->api_tnde_staging('tnde_surat_keluar/get_penerima_sk', 'json', 'POST', array('api_kode' => 90008, 'api_subkode' => 1, 'api_search' => array($id_surat_keluar)));
	    	$data['penerima_sk_for_sm'] = $penerima;

	    }
        return $data;
	}

	public function get_penerima_sk_by_no_surat($no_surat, $status_distribusi='PS', $mode='arr_return')
	{
		$penerima = $this->apiconn->api_tnde_v3('tnde_surat_keluar/get_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 2, 'api_search' => array($no_surat, $status_distribusi)));
		$data['penerima'][] = $penerima;
 				
		return $data; 	
	}
	
	
}