<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_mhs_surat_keluar extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('common/Api_simpeg');
    	$this->load->model('common/Api_sia');
    	// $this->load->model('Repo_Automasi_Surat');

    	$this->load->model('mhs_surat_keluar/Mdl_sesi_pembuatan');
	}

	 /**
    * Get Surat keluar (Data Umum , Penerima) dari SISURAT
    */
    public function get_surat_keluar($id_surat_keluar)
    {
        //parameter pencarian : id_surat_keluar
        #SISURAT SURAT KELUAR
        $data['umum'] = $this->apiconn->api_tnde('tnde_surat_keluar/detail_surat_keluar', 'json', 'POST', array('api_kode' => 90001, 'api_subkode' => 2, 'api_search' => array($id_surat_keluar)));
        $kepada = $this->apiconn->api_tnde('tnde_surat_keluar/get_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($id_surat_keluar, 'PS')));
        $data['penerima']['PS'] = $kepada;
        $ts = $this->apiconn->api_tnde('tnde_surat_keluar/get_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($id_surat_keluar, 'TS')));
        $data['penerima']['TS'] = $ts;
        
        return $data;
    }

    public function get_detail_skr_automasi($id_surat_keluar)
    {
        $parameter = array('api_kode' => 14001, 'api_subkode' => 1, 'api_search' => array($id_surat_keluar));
        $api_get = $this->apiconn->api_tnde_sakad('Tnde_surat_akademik/get_detail_skr_automasi','json','POST', $parameter) ;
        return $api_get;
    }

	public function detail_surat()
	{
	}


	public function gen_signature_publik()
	{}


	public function simpan_penerbitan_surat($paramdata=array()) 
    {   
    	$response = null;	    	
    	$opsi = $paramdata['skema']; //preview (tampung variabel keluaran JSON), bernomor, tte (tandatangan elektronik)
    	$data['skema_terbit'] = $opsi;

    	$data['errors'] = []; //tampung proses error
    	$data['success'] = [];  //tampung proses sukses

    	if (!empty($paramdata)){    	
		#1. Mengambil data umum dan penerima surat dari sesi
			$get_sesi = $this->Mdl_sesi_pembuatan->sesi_by_id($paramdata['sesi']['ID_SESI']);
			if (!empty($get_sesi)){
				// exit(json_encode($get_sesi));
				$data_sk = $get_sesi; //assign semua kolom data_sesi ke array $data_sk
			} else {		
				return ['error'=> 'mohon maaf pembuatan surat tidak dapat dilanjutkan karena data sesi tidak ditemukan'];
			}
    			
			#2. Modif kolom-kolom untuk proses data
			$data_sk['ID_SURAT_KELUAR'] = uniqid().'p';
			$data_sk['ID_PSD'] = $get_sesi['PSD_ID'];
			$data_sk['ATAS_NAMA'] = $get_sesi['PSD_AN'];
			$data_sk['UNTUK_BELIAU'] = $get_sesi['PSD_UB'];
			$data_sk['UNTUK_UNIT'] = $get_sesi['UNIT_ID'];
			$data_sk['STATUS_SK'] = 0;
			$data_sk['WAKTU_SIMPAN'] = date("d/m/Y H:i:s");			
			$data_sk['TGL_SURAT'] = DateTime::createFromFormat('Y-m-d', $data_sk['TGL_SURAT'])->format('d/m/Y');

			if ($opsi == 'preview'){
				$data_sk['KD_STATUS_SIMPAN'] =  '2';

				if($data_sk['KD_STATUS_SIMPAN'] == '2' || $data_sk['KD_STATUS_SIMPAN'] == 2){ #2 BERNOMOR
					$data_sk['NO_URUT']		= null;
					$data_sk['NO_SELA']		= null;
					$data_sk['NO_SURAT']	= 'belum diset';

				} else { #1 (draf)
					$data_sk['NO_URUT']		= '';
					$data_sk['NO_SELA']		= '';
					$data_sk['NO_SURAT']	= 'belum diset';
				}
			}

			if ($opsi == 'bernomor' || $opsi == 'tte') {
				$data_sk['KD_STATUS_SIMPAN'] = '2';
				
				#SET PENOMORAN
				$parameter	= array('api_kode' => 90002, 'api_subkode' => 3, 'api_search' => array($data_sk));
				$req_no 	= $this->apiconn->api_tnde_v3('tnde_surat_keluar/penomoran', 'json', 'POST', $parameter);

				if ($data_sk['KD_STATUS_SIMPAN'] == '2' || $data_sk['KD_STATUS_SIMPAN'] == 2) {
					#beri nomor tapi jangan set no_urut dan nomor_sela
					if(empty($data['errors'])){							

						$array_penomoran_surat_aktif = [1,2,3,4,5,6,7,8,9,10,11,12]; #produksi
						$kd_jenis_sakad_int = (int)$get_sesi['KD_JENIS_SAKAD'];
						if (in_array($kd_jenis_sakad_int, $array_penomoran_surat_aktif)){

							if(empty($req_no['ERRORS'])){
								$data_sk['NO_URUT']	= (!empty($req_no['NO_URUT']) ? $req_no['NO_URUT'] : 0);
								$data_sk['NO_SELA']		= (!empty($req_no['NO_SELA']) ? $req_no['NO_SELA'] : 0);
								$data_sk['NO_SURAT']	= (!empty($req_no['NO_SURAT']) ? $req_no['NO_SURAT'] : 0);
							}else{
								$save_sk = FALSE;
								$data['errors'] = $req_no['ERRORS'];
							}
							
							$data_sk['KD_STATUS_SIMPAN'] = '2';
						} else {
							if(empty($req_no['ERRORS'])){
								$data_sk['NO_URUT']	= '';
								$data_sk['NO_SELA']		= '';
								$data_sk['NO_SURAT']	= (!empty($req_no['NO_SURAT']) ? $req_no['NO_SURAT'] : '');
							}else{
								$save_sk = FALSE;
								$data['errors'] = $req_no['ERRORS'];
							}
							#ubah lagi KD_STATUS_SIMPAN KE 1
							$data_sk['KD_STATUS_SIMPAN'] = '1';
						}
						
					}
				} else { #1 (draf)
					if(empty($req_no['ERRORS'])){
						$data_sk['NO_URUT']	= ''; #DIKOSONGKAN SELAMA TESTING OR DEV
						$data_sk['NO_SELA']		= ''; #DIKOSONGKAN SELAMA TESTING OR DEV
						$data_sk['NO_SURAT']	= (!empty($req_no['NO_SURAT']) ? $req_no['NO_SURAT'] : 0);
						// $data_sk['NO_SURAT']	= '';
					}else{
						$save_sk = FALSE;
						$data['errors'] = $req_no['ERRORS'];
					}												
				}
				
				#ubah lagi KD_STATUS_SIMPAN KE 1 (SETELAH DAPAT NOMOR, HEHEHE)
				// $data_sk['KD_STATUS_SIMPAN'] = '1';
			}

			#SELEKSI KOLOM YANG MERUPAKAN ELEMEN UMUM SURAT	KELUAR
			$key_diikutkan = ['ID_SURAT_KELUAR', 'KD_STATUS_SIMPAN', 'KD_JENIS_SURAT', 'ID_PSD', 'ATAS_NAMA', 'UNTUK_BELIAU', 'ID_KLASIFIKASI_SURAT', 'TGL_SURAT', 'STATUS_SK', 'PEMBUAT_SURAT', 'JABATAN_PEMBUAT', 'KD_JENIS_ORANG','UNIT_ID', 'UNTUK_UNIT', 'PERIHAL', 'TEMPAT_DIBUAT', 'TEMPAT_TUJUAN', 'KOTA_TUJUAN', 'ALAMAT_TUJUAN', 'LAMPIRAN', 'KD_SIFAT_SURAT', 'KD_KEAMANAN_SURAT', 'WAKTU_SIMPAN', 'KD_SISTEM', 'NO_URUT', 'NO_SELA', 'ISI_SURAT', 'KD_PRODI', 'NO_SURAT'];			
			$data_sk = array_intersect_key($data_sk, array_flip($key_diikutkan));

		#2. Melakukan API Insert ke DB SISURAT	(surat_keluar)
			if ($opsi == 'preview'){
				$data['surat_keluar']['umum'] = $data_sk; //assign data ke variabel lain
				$data['success'][] = "Berhasil menyiapkan data umum surat keluar yang akan diterbitkan #".$data_sk["ID_SURAT_KELUAR"];
				$save_sk = TRUE;
			} elseif ($opsi == 'bernomor' || $opsi == 'tte') {					
				$save_sk = TRUE;
				if($save_sk == TRUE){
					$parameter	= array('api_kode' => 90001, 'api_subkode' => 1, 'api_search' => array($data_sk));
					$save_sk 		= $this->apiconn->api_tnde('tnde_surat_keluar/insert_surat_kaluar', 'json', 'POST', $parameter);

					//exit(var_dump($data_sk));
					if($save_sk	==	FALSE || $save_sk == NULL){
						$data['errors'][] = "Gagal menyimpan surat. #log: SKR001";
						$data['surat_keluar']['umum'] = FALSE;
					}else{
						$save_sk	=	TRUE;
						$data['success'][] = "Berhasil menyimpan surat (data umum) dengan ID = ".$data_sk["ID_SURAT_KELUAR"];
						$data['surat_keluar']['umum'] = $data_sk;
					}

				}
			}
		#**********************  TITIK PENGECEKAN ERROR TAHAP 1 ************************			
			if (!empty($data['errors'])){
				log_message('error', 'Terjadi kegagalan saat penambahan data ke DB TNDE melalui REST API.');
				return [
					'error' => [
						'message' => $data['errors'],
						'debug_data' => $data
					]
				];
			}
		#**********************  ./TITIK PENGECEKAN ERROR TAHAP 1 ************************


		#3. Simpan penerima surat
			$kpd_mahasiswa = (!empty($get_sesi['D_PENERIMA_MHS']) ? explode('<$>', $get_sesi['D_PENERIMA_MHS']) : null);
			$kpd_pejabat = (!empty($get_sesi['D_PENERIMA_PJB']) ? explode('<$>', $get_sesi['D_PENERIMA_PJB']) : null);
			$kpd_pegawai = (!empty($get_sesi['D_PENERIMA_PGW']) ? explode('<$>', $get_sesi['D_PENERIMA_PGW']) : null);			
			$kpd_eks = (!empty($get_sesi['D_PENERIMA_EKS']) ? explode('<$>', $get_sesi['D_PENERIMA_EKS']) : null);

			$ts_pejabat = (!empty($get_sesi['D_TEMBUSAN_PJB']) ? explode('<$>', $get_sesi['D_TEMBUSAN_PJB']) : null);
			$ts_eks = (!empty($get_sesi['D_TEMBUSAN_EKS']) ? explode('<$>', $get_sesi['D_TEMBUSAN_EKS']) : null);
			$data['hasil_insert_penerima'] = [];

			if (!empty($kpd_mahasiswa)){
				foreach ($kpd_mahasiswa as $k=>$v) {
					$cols = explode('#', $v);
					$data['hasil_insert_penerima'][] = $this->simpan_penerima_surat($data_sk['ID_SURAT_KELUAR'], 
						['PENERIMA_SURAT' => $cols[0], 'KD_GRUP_TUJUAN' => 'MHS01', 'KD_STATUS_DISTRIBUSI' => 'PS', 'KD_JENIS_KEPADA'=>$cols[1],'NO_URUT' => $k + 1], 
						$paramdata['skema']
					);							
				}
			}			

			if (!empty($kpd_pejabat)){
				foreach ($kpd_pejabat as $k=>$v) {
					$cols = explode('#', $v);
					$data['hasil_insert_penerima'][] = $this->simpan_penerima_surat($data_sk['ID_SURAT_KELUAR'], 
						[
							'KD_GRUP_TUJUAN' => 'PJB01', 'JABATAN_PENERIMA'=>$cols[0], 'KD_STATUS_DISTRIBUSI' => 'PS', 'KD_JENIS_KEPADA'=>$cols[1], 'NO_URUT' => $k + 1
						],
						$paramdata['skema']
					);
				}
			}

			if (!empty($kpd_eks)){
				foreach ($kpd_eks as $k=>$v) {
					$cols = explode('#', $v);
					$data['hasil_insert_penerima'][] = $this->simpan_penerima_surat($data_sk['ID_SURAT_KELUAR'], 
						[
							'KD_GRUP_TUJUAN' => 'EKS01', 'PENERIMA_SURAT'=>$cols[0], 'KD_STATUS_DISTRIBUSI' => 'PS', 'KD_JENIS_KEPADA'=>$cols[1], 'NO_URUT' => $k + 1
						],
						$paramdata['skema']
					);
				}
			}

			if (!empty($ts_pejabat)){
				foreach ($ts_pejabat as $k=>$v) {
					$cols = explode('#', $v);

					$data['hasil_insert_penerima'][] = $this->simpan_penerima_surat($data_sk['ID_SURAT_KELUAR'], 
						[
							'KD_GRUP_TUJUAN' => 'PJB01', 'JABATAN_PENERIMA'=>$cols[0], 'KD_STATUS_DISTRIBUSI' => 'TS', 'KD_JENIS_TEMBUSAN'=>$cols[1], 'NO_URUT' => $k + 1
						],
						$paramdata['skema']
					);
				}
			}

			if (!empty($ts_eks)){
				foreach ($ts_eks as $k=>$v) {
					$cols = explode('#', $v);
					$data['hasil_insert_penerima'][] = $this->simpan_penerima_surat($data_sk['ID_SURAT_KELUAR'], 
						[
							'KD_GRUP_TUJUAN' => 'EKS01', 'PENERIMA_SURAT'=>$cols[0], 'KD_STATUS_DISTRIBUSI' => 'TS', 'KD_JENIS_TEMBUSAN'=>$cols[1], 'NO_URUT' => $k + 1
						],
						$paramdata['skema']
					);

				}
			}

			

		#**********************  TITIK PENGECEKAN ERROR TAHAP 2 (penerima surat) ************************
			$data['surat_keluar']['penerima']['PS'] = [];
			$data['surat_keluar']['penerima']['TS'] = [];
			$data['surat_keluar']['mhs_dalam_surat'] = [];
			
			foreach ($data['hasil_insert_penerima'] as $k=>$v) {
				if (!empty($v['errors'])){ #jika ada API INSERT penerima yg gagal
					$data['errors'][] = $v['errors'][0];
				} else {
					$data['success'][] = (!empty($v['success']) ? $v['success'][0] : 'preview_purpose - berhasil menyiapkan data penerima surat yang akan disimpan - '.$v['data_penerima']['PENERIMA_SURAT'].'#'.$v['data_penerima']['JABATAN_PENERIMA']);

					if ($v['data_penerima']['KD_STATUS_DISTRIBUSI'] == 'PS'){
						$data['surat_keluar']['penerima']['PS'][] = $v['data_penerima'];

						if ($v['data_penerima']['KD_GRUP_TUJUAN'] == 'MHS01'){
							$data['surat_keluar']['mhs_dalam_surat'][] = $v['data_penerima'];
						}
					} elseif($v['data_penerima']['KD_STATUS_DISTRIBUSI'] == 'TS'){
						$data['surat_keluar']['penerima']['TS'][] = $v['data_penerima'];
					}
				}
			}			

		#**********************  ./TITIK PENGECEKAN ERROR TAHAP 2 (penerima surat) ************************			
			

		#4. simpan detail surat_keluar_mhs (DB Autosurat)	
			$detail_surat_sesi = unserialize($get_sesi['DETAIL_SURAT']);
			$obj_tgl_surat = DateTime::createFromFormat('d/m/Y', $data_sk['TGL_SURAT']); //transform string tanggal jadi object
			$batas_masa_berlaku = isset($detail_surat_sesi['CONFIG_MASA_BERLAKU']) ? $obj_tgl_surat->add(new DateInterval($detail_surat_sesi['CONFIG_MASA_BERLAKU'])) : null; //object datetime

			$detail_skr_automasi = $detail_surat_sesi;
			$detail_skr_automasi = array_diff_key($detail_skr_automasi, array_flip(['CONFIG_MASA_BERLAKU'])); //hapus kolom tidak dibutuhkan			
			$detail_skr_automasi['BATAS_MASA_BERLAKU'] = isset($batas_masa_berlaku) ?  $batas_masa_berlaku->format('Y-m-d') : null;

			$obj_tgl_surat = DateTime::createFromFormat('d/m/Y', $data_sk['TGL_SURAT']); //transform string tanggal jadi object
			$header_skr_automasi = [
				'ID_SURAT_KELUAR' => $data['surat_keluar']['umum']['ID_SURAT_KELUAR'],
				'NO_SURAT' => $data['surat_keluar']['umum']['NO_SURAT'],				
				'TGL_SURAT' => $obj_tgl_surat->format('Y-m-d'), #FORMAT PADA POSTGRESQL,
				'PEMBUAT_SURAT' => $data['surat_keluar']['umum']['PEMBUAT_SURAT'], 
				'KD_JENIS_SAKAD' => $detail_skr_automasi['KD_JENIS_SAKAD']
			];
			$data_skr_automasi = array_merge($header_skr_automasi, $detail_skr_automasi);

			if ($opsi == 'preview'){
				$data['detail_surat_automasi'] = $data_skr_automasi;
				$data['success'][] = 'preview purpose - berhasil menyiapkan data detail surat yang akan disimpan ke sistem automasi';
			}elseif ($opsi == 'bernomor' || $opsi == 'tte'){
				if (empty($data['errors'])){
					#API INSERT DETAIL SURAT (WEBSERVICE AUTOMASI)
					$parameter	= array('api_kode'=>14004, 'api_subkode' => 1, 'api_search' => array($data_skr_automasi));
					$insert_detail_skr_automasi = $this->apiconn->api_tnde_sakad('tnde_surat_akademik/insert_detail_skr_automasi', 'json', 'POST', $parameter);

					if ($insert_detail_skr_automasi['status'] == TRUE || $insert_detail_skr_automasi['status'] == 1){
						$data['success'][] = 'Berhasil menambahkan data detail surat ke sistem automasi';
						$data['detail_surat_automasi'] = $insert_detail_skr_automasi['data'];
					} else {
						$data['detail_surat_automasi'] = FALSE;
						$data['errors'][] = 'Gagal menambahkan data detail surat ke sistem automasi';
					}
				}	
			}

			#============= FINAL CHECK (LOG jika ada error, lakukan CRON JOB untuk membersihkan data nanti)================

			#============= ./FINAL CHECK ==============

		} else {
			return ['error'=>'parameter data kosong. tidak ada yang dapat diproses'];
		}				

		return $data;
	}
	
	public function simpan_penerima_surat($id_surat_keluar=null, $paramdata=array(), $mode='preview', $kat_input='kd_user_only')
	{
		if (empty($id_surat_keluar))
			return ['error'=>'parameter id surat keluar belum diisi'];

		if (empty($paramdata))
			return ['error'=>'tidak ada data masukan yang dapat diproses'];

		#1.cek array masukan
		$penerima_surat = isset( $paramdata['PENERIMA_SURAT']) ? $paramdata['PENERIMA_SURAT'] : null;
		$grup_tujuan = isset( $paramdata['KD_GRUP_TUJUAN']) ? $paramdata['KD_GRUP_TUJUAN'] : null;
		$status_distribusi = isset( $paramdata['KD_STATUS_DISTRIBUSI']) ? $paramdata['KD_STATUS_DISTRIBUSI'] : null;

		if ($status_distribusi == 'PS'){
			// $paramdata['KD_JENIS_KEPADA'] = 0; //0=biasa, 1=untuk perhatian
			$paramdata['KD_JENIS_KEPADA'] =  isset( $paramdata['KD_JENIS_KEPADA']) ? $paramdata['KD_JENIS_KEPADA'] : null;
			$paramdata['KD_JENIS_TEMBUSAN'] = NULL;
		} elseif($status_distribusi == 'TS'){
			$paramdata['KD_JENIS_KEPADA'] = NULL; //0=biasa, 1=untuk perhatian				
			$paramdata['KD_JENIS_TEMBUSAN'] =  isset( $paramdata['KD_JENIS_TEMBUSAN']) ? $paramdata['KD_JENIS_TEMBUSAN'] : null;
		}

		if ($grup_tujuan == 'MHS01'){
			$paramdata['JABATAN_PENERIMA'] = NULL;
		} elseif ($grup_tujuan == 'PJB01' || $grup_tujuan == 'PGW01') {
			//Kondisi 1: penerima surat tidak diset, inputan tersedia hanya STR_ID
			if (!isset($paramdata['PENERIMA_SURAT']) && isset($paramdata['JABATAN_PENERIMA'])){
				$detail_pegawai = $this->Api_simpeg->get_pejabat_aktif(date('d/m/Y'), $paramdata['JABATAN_PENERIMA']);
				// exit(print_r($detail_pegawai));
				// $paramdata['PENERIMA_SURAT'] = $detail_pegawai[0]['KD_PGW']; //KODE PEGAWAI
				$penerima_surat = $detail_pegawai[0]['KD_PGW']; //KODE PEGAWAI
				$nama_penerima = $detail_pegawai[0]['NM_PGW_F'].' ('.$detail_pegawai[0]['STR_NAMA'].')'; //Nama Lengkap (beserta gelar) PEGAWAI
			} elseif (isset($paramdata['PENERIMA_SURAT']) && !isset($paramdata['JABATAN_PENERIMA'])) { //Kondisi 2: penerima surat diset, jabatan penerima (STR_ID) tidak diset
				$detail_jabatan = $this->Api_simpeg->get_jabatan_struktural($paramdata['PENERIMA_SURAT']);
				
				
				if (count($detail_jabatan) > 1){ //jika jabatan struktural > 1, pilih yang resmi saja
					$detail_jabatan_resmi = [];
					$cek_rr_jenis2 = array_column($detail_jabatan, 'RR_JENIS2');
					foreach ($cek_rr_jenis2 as $k=>$v) {
						if ($v == 1){
							$detail_jabatan_resmi[] = $detail_jabatan[$k];
						} else {							
							continue;
						}
					}
					$paramdata['JABATAN_PENERIMA'] = $detail_jabatan_resmi[0]['STR_ID'];
					$nama_penerima = $detail_jabatan_resmi[0]['NM_PGW_F'].' ('.$detail_jabatan_resmi[0]['STR_NAMA'].')';
				} else {
					$paramdata['JABATAN_PENERIMA'] = $detail_jabatan[0]['STR_ID'];
					$nama_penerima = $detail_jabatan[0]['NM_PGW_F'].' ('.$detail_jabatan_resmi[0]['STR_NAMA'].')';
				}				
			} else {
				exit(json_encode(['debug_errors' => $paramdata]));
			}
		} else { #EKS01
			$paramdata['JABATAN_PENERIMA'] = NULL;
		}


		#assign ke variabel penerima
		$penerima['ID_SURAT_KELUAR']	= $id_surat_keluar;
		$penerima['PENERIMA_SURAT'] = $penerima_surat;
		$penerima['JABATAN_PENERIMA'] = $paramdata['JABATAN_PENERIMA'];
		$penerima['KD_GRUP_TUJUAN'] = $grup_tujuan;
		$penerima['KD_STATUS_DISTRIBUSI'] = $status_distribusi;
		$penerima['ID_STATUS_SM'] = 1;
		$penerima['KD_JENIS_TEMBUSAN'] = $paramdata['KD_JENIS_TEMBUSAN'];
		$penerima['NO_URUT'] = $paramdata['NO_URUT'];
		$penerima['KD_JENIS_KEPADA'] = $paramdata['KD_JENIS_KEPADA'];
					

		if ($mode == 'preview'){
			// $insert_penerima[] = $penerima;
			// $daftar_penerima[] = $penerima; //jamak
			$data_penerima = $penerima; //tunggal

		} elseif ($mode == 'bernomor' || $mode == 'tte') {
			$insert_penerima = $this->apiconn->api_tnde('tnde_surat_keluar/insert_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($penerima)));

			if($insert_penerima == FALSE || $insert_penerima == NULL){ #gagal simpan penerima

				$data['errors'][] = "Gagal menambahkan ".(isset($nama_penerima) ? $nama_penerima : $penerima_surat)." ke daftar penerima surat";
				$data_penerima[] = FALSE;
			}elseif ($insert_penerima == TRUE) {#berhasil simpan penerima
				// $daftar_penerima[] = $penerima;
				$data_penerima = $penerima;
				$data['success'][] = "Berhasil menambahkan ".(isset($nama_penerima) ? $nama_penerima : $penerima_surat)." ke daftar penerima surat (id:".$id_surat_keluar.")";
				
			} else {
				$data['errors'][] = "Error tidak dikenali, data gagal disimpan";
			}
		}
				
		// $data['data_penerima'] = $daftar_penerima; //jamak: tidak jadi begini, fungsi ini hanya melakukan satu proses insert
		$data['data_penerima'] = $data_penerima;
		
		// $data['surat_keluar']['penerima']['PS'] = array_merge($data['surat_keluar']['penerima']['PS'], $save_mhs);
		// $getmhs = $this->Api_sia->get_profil_mhs($save_mhs[0]['PENERIMA_SURAT'],'kumulatif');			
		// $data['prodi'] = $getmhs[0]['NM_PRODI'];

		return $data;

	}
	

	public function hapus_draf_surat($id_surat_keluar=null)
	{
		$parameter	= array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('MD_SURAT_KELUAR2', 'ID_SURAT_KELUAR', $id_surat_keluar));
        $cek_surat	= $this->apiconn->api_tnde('tnde_general/get_data', 'json', 'POST', $parameter);

        if(empty($cek_skr['ID_SURAT_KELUAR']))
        	return ['error'=>'surat yang akan dihapus tidak ditemukan pada basis data'];

        if ($cek_surat['KD_SISTEM'] == 'AUTOMASI')
			return ['error'=>'surat tidak dapat dihapus karena bukan berasal dari sistem automasi surat'];

		//Periksa bahwa surat masih berstatus sebagai draf (1)
		if($cek_surat['KD_STATUS_SIMPAN'] == 1){
            #2.1 HAPUS DULU DISTRIBUSI TUJUAN/PENERIMA SURAT
            $parameter              = array('api_kode' => 1001, 'api_subkode' => 3, 'api_search' => array('D_PENERIMA_SK', 'ID_SURAT_KELUAR', $id_surat_keluar));
            $cek_penerima_skr   =   $this->apiconn->api_tnde('tnde_general/get_data', 'json', 'POST', $parameter);
            if(count($cek_penerima_skr) > 0){
                $del_ps['ID_SURAT_KELUAR']  =   $id_surat_keluar;
                $parameter              = array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('D_PENERIMA_SK', $del_ps));
                $del_penerima_skr   =   $this->apiconn->api_tnde('tnde_general/delete_data', 'json', 'POST', $parameter);
                if($del_penerima_skr == FALSE){
                    $data['errors'][]   = "Gagal menghapus penerima surat.";
                } else {
                	$data['success'][]  =   "Berhasil menghapus penerima surat.";
                }
            }
            #2.2 set act_delete TRUE / FALSE setelah hapus distribusi
            if(ISSET($data['errors'])){
                $act_delete =   FALSE;
            }else{
                $act_delete = TRUE;
            }

            #3 HAPUS ENTRY UMUM         
            if($act_delete == TRUE){
                $d_surat['ID_SURAT_KELUAR'] =   $id_surat_keluar;
                $parameter  = array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('MD_SURAT_KELUAR2', $d_surat));
                $del_surat      =   $this->apiconn->api_tnde('tnde_general/delete_data', 'json', 'POST', $parameter);
                if($del_surat == TRUE){
                    $data['success'][]  =   "Berhasil menghapus surat.";
                }else{
                    $data['errors'][]   = "Gagal menghapus surat.";
                }
            }


            #4. HAPUS DATA-DATA DI SI/APLIKASI AUTOMASI (sekali request)
            if($act_delete == TRUE){
                $del_detail = $this->delete_detail_surat_keluar_automasi($id_surat_keluar);

                if($del_detail == TRUE){
                    $data['success'][]  =   "Berhasil menghapus detail surat mahasiswa (automasi).";
                }else{
                    $data['errors'][]   = "Gagal menghapus detail surat mahasiswa (automasi).";
                }
            }
        } else {
            $data['errors'][] = "Surat yang telah memiliki nomor, tidak dapat dihapus.";
        }	
		return $data;        
	}

	 /**
    *  Pastikan surat yang dihapus , adalah surat ujicoba dan kolom KD_SISTEM = AUTOMASI
    */
    public function delete_surat_keluar_automasi($id_surat_keluar=null)
    {
        //$id_surat_keluar  =   $this->input->post('id_surat_keluar');
        if ($this->session->userdata('username') == '11650021' || $this->session->userdata('username') == '15650051'){
            $parameter          = array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('MD_SURAT_KELUAR2', 'ID_SURAT_KELUAR', $id_surat_keluar));
            $cek_skr                    = $this->apiconn->api_tnde('tnde_general/get_data', 'json', 'POST', $parameter);
            if(ISSET($cek_skr['ID_SURAT_KELUAR']) && $cek_skr['KD_SISTEM'] == 'AUTOMASI'){
                //exit(print_r($cek_skr));
                #1. CEK STATUS SIMPAN BERNILAI 1
                if($cek_skr['KD_STATUS_SIMPAN'] == 1){
                    #2.1 HAPUS DULU DISTRIBUSI TUJUAN/PENERIMA SURAT
                    $parameter              = array('api_kode' => 1001, 'api_subkode' => 3, 'api_search' => array('D_PENERIMA_SK', 'ID_SURAT_KELUAR', $id_surat_keluar));
                    $cek_penerima_skr   =   $this->apiconn->api_tnde('tnde_general/get_data', 'json', 'POST', $parameter);
                    if(count($cek_penerima_skr) > 0){
                        $del_ps['ID_SURAT_KELUAR']  =   $id_surat_keluar;
                        $parameter              = array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('D_PENERIMA_SK', $del_ps));
                        $del_penerima_skr   =   $this->apiconn->api_tnde('tnde_general/delete_data', 'json', 'POST', $parameter);
                        if($del_penerima_skr == FALSE){
                            $data['errors'][]   = "Gagal membatalkan pengiriman surat.";
                        }
                    }
                    #2.2 set act_delete TRUE / FALSE setelah hapus distribusi
                    if(ISSET($data['errors'])){
                        $act_delete =   FALSE;
                    }else{
                        $act_delete = TRUE;
                    }

                    #3 HAPUS ENTRY UMUM         
                    if($act_delete == TRUE){
                        $d_surat['ID_SURAT_KELUAR'] =   $id_surat_keluar;
                        $parameter  = array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('MD_SURAT_KELUAR2', $d_surat));
                        $del_surat      =   $this->apiconn->api_tnde('tnde_general/delete_data', 'json', 'POST', $parameter);
                        if($del_surat == TRUE){
                            $data['success'][]  =   "Berhasil menghapus surat.";
                        }else{
                            $data['errors'][]   = "Gagal menghapus surat.";
                        }
                    }


                    #4. HAPUS DATA-DATA DI SI/APLIKASI AUTOMASI (sekali request)
                    if($act_delete == TRUE){
                        $del_detail = $this->delete_detail_surat_keluar_automasi($id_surat_keluar);

                        if($del_detail == TRUE){
                            $data['success'][]  =   "Berhasil menghapus detail surat mahasiswa (automasi).";
                        }else{
                            $data['errors'][]   = "Gagal menghapus detail surat mahasiswa (automasi).";
                        }
                    }
                } else {
                    $data['errors'][] = "Surat yang telah memiliki nomor, tidak dapat dihapus.";
                }       
               
            } else {
                $data['errors'][]   = "Data tidak ditemukan dan atau surat bukan dihasilkan melalui sistem/aplikasi automasi";   
            }
            
        } else {
            $data['errors'][]   = "Akses Ditolak";  
        }

        #final response
        if(!empty($data['errors'])){
            $this->session->set_flashdata('errors', $data['errors']);            
            //echo print_r($data['errors']);
        }
        if(!empty($data['success'])){
            $this->session->set_flashdata('success', $data['success']);
            //echo print_r($data['success']);
        }
        return $data;
    }

    /**
    * API Delete Detail SKR (Surat Keluar)
     * entitas yang dihapus : HEADER_SURAT_KELUAR, DETAIL_SAKAD. SIGNATURE belum include        
     * @return boolean
     */ 
    protected function delete_detail_surat_keluar_automasi($id_surat_keluar=null){
        $parameter              = array('api_kode' => 14007, 'api_subkode' => 1, 'api_search' => array($id_surat_keluar));
        $api_del   =   $this->apiconn->api_tnde_sakad('Tnde_surat_akademik/delete_detail_surat_automasi', 'json', 'POST', $parameter);
        return $api_del;
    }

}