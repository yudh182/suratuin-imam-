<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Mdl_sesi_pembuatan extends CI_Model
{
    public function __construct()
    {
        parent::__construct(); 
        #note : $apiconn dikenali lewat controller yang memuat model ini
    }

    public function get_config_automasi($kd_jenis_sakad=null, $unit_id='')
    {
        $parameter = array(
            'api_kode' => 15003, 'api_subkode' => 1, 
            'api_search' => array(
                'CONFIG_AUTOMASI_SAKAD',
                array('KD_JENIS_SAKAD'=>(int)$kd_jenis_sakad,'UNIT_ID'=>$unit_id),
                'ID ASC'
            )
        );      
        $api_get = $this->apiconn->api_tnde_sakad('surat_general/get_data', 'json', 'POST', $parameter);

        return $api_get;
    }   

    public function simpan_sesi_automasi_buat($data=array())
    {           
        $parameter = array('api_kode' => 13006, 'api_subkode' => 3, 'api_search' => array($data));
        $api_insert = $this->apiconn->api_tnde_sakad('Tnde_surat_akademik/insert_entry_sesi_sakad','json','POST', $parameter) ;

        return $api_insert;
    }


    public function simpan_sesi_automasi_buat_v2($data=array())
    {           
        $parameter = array('api_kode' => 14006, 'api_subkode' => 3, 'api_search' => array($data));
        $api_insert = $this->apiconn->api_tnde_sakad('Tnde_surat_akademik/insert_entry_sesi_sakad_v2','json','POST', $parameter) ;

        return $api_insert; // array
    }

    public function perbarui_sesi_automasi_buat($id_surat_keluar='', $data=array())
    {           
        $parameter = array('api_kode' => 14006, 'api_subkode' => 1, 'api_search' => array($id_surat_keluar, $data));
        $api_update = $this->apiconn->api_tnde_sakad('Tnde_surat_akademik/update_entry_sesi_sakad','json','POST', $parameter) ;

        return $api_update; // array
    }

    public function sesi_by_id($id_sesi=null)
    {
        $parameter = array('api_kode' => 14006, 'api_subkode' => 1, 'api_search' => array($id_sesi));
        $api_get = $this->apiconn->api_tnde_sakad('Tnde_surat_akademik/get_sesi_sakad','json','POST', $parameter) ;

        return $api_get;
    }

    public function sesi_by_kd_user($kd_user=null) //NIM atau NIP
    {
        $parameter = array('api_kode' => 14006, 'api_subkode' => 2, 'api_search' => array($kd_user));
        $api_get = $this->apiconn->api_tnde_sakad('Tnde_surat_akademik/get_sesi_sakad','json','POST', $parameter) ;

        return $api_get;
    }

    public function hapus_sesi_automasi_buat($data=null) // By ID_SESI
    {
        $parameter = array('api_kode' => 14007, 'api_subkode' => 1, 'api_search' => array($data));
        $api_delete = $this->apiconn->api_tnde_sakad('Tnde_surat_akademik/delete_sesi_sakad','json','POST', $parameter) ;

        return $api_delete;   
    }


}