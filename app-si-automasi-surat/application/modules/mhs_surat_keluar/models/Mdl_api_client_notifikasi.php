 <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 class Mdl_api_client_notifikasi extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();		
		$this->load->model('common/Mdl_tnde', 'apiconn');
	}

	public function get_monitor_tte_by_pembuat($nim=null)
	{
		if (empty($nim))
			return ['error'=>'parameter tidak lengkap'];

		$api_get = $this->apiconn->api_autosurat_v2('notifikasi/monitor_event_surat', 'json', 'GET', 
			array(
				'api_kode' => 16001, 'api_subkode' => 1, 
				'api_search' => array(
					'kategori_monitor' => 'TTE',
					'pemonitor' => $nim
				)
			)
		);
		return $api_get;
	}

	public function get_monitor_event_surat()
	{		
		$api_get = $this->apiconn->api_autosurat_v2('notifikasi/monitor_event_surat', 'json', 'GET', array('api_kode' => 16000, 'api_subkode' => 1, 'api_search' => array()));
		return $api_get;
	}

	public function insert_monitor_event_surat($data)
	{
		/*
		$data = [
			"ID_SURAT" => "id-surat-keluar",
			"DIMONITOR_OLEH" => "nim-pemohon",
			"KD_KAT_MONITOR" => "kd-monitor" #TTE, TTB
		];
		*/
		$api_insert = $this->apiconn->api_autosurat_v2('notifikasi/insert_monitor_event_surat', 'json', 'POST', 
			[
				'api_kode' => 16000, 'api_subkode' => 1, 
				'api_search' => array($data)
			]
		);
		return $api_insert;		
	}


}