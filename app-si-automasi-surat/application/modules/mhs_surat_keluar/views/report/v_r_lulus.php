<?php
/**
* ============================================================+
* File name   : v_r_lulus.php
* @since       : 2018-04-21
* Last Update : 2018-03-25
*/
// Description : Template tcpdf untuk surat keterangan lulus

// create new PDF document
$pdf = new Pdf_creator(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->set_document_properties(
    array(
        'pdf_title'=>'Surat Keterangan Lulus',
                'pdf_margin' => ['20','40','20', true], #LEFT, TOP, RIGHT (PAGE MARGINS)
        'pdf_template_margin' => ['6','12'], #MARGIN HEADER, MARGIN FOOTER
        'pdf_page_break' => [true,'25'] #aktif/tdk aktif, MARGIN BOTTOM (DEFAULT : isikan INITIAL)
    )
);
$pdf->setData($template);

// add a page
$pdf->AddPage('P', 'A4');
//set watermark (khusus preview)
if (isset($watermark_img)) :
    $base64_decoded = base64_decode($watermark_img);
    $pdf->Image('@'.$base64_decoded);    
endif;

// set some text for example
$nama_surat ='
<b>'.$judul_surat.'</b><br>
Nomor : '.$nomor.'
';
$txt = '<p>Dekan '.ucwords(strtolower($template["kopsurat"]["fkl"])).' menerangkan bahwa : </p>        
    <table>
         <tr>
            <td width="120pt">Nama</td>
            <td width="360pt">:  '.ucwords(strtolower($isi_mhs["NAMA"])).'</td>
        </tr>
        <tr>
            <td>NIM</td>
            <td>:  '.$isi_mhs["NIM"].'</td>
        </tr>        
        <tr>
           <td>Program Studi</td>
           <td>:  '.$isi_mhs["NM_PRODI"].'</td>
        </tr>
         <tr>
            <td>Tahun Akademik</td>
            <td>:  '.$isi_surat["DATA_TAMBAHAN"]["TA"].'</td>
        </tr>
        <tr>
           <td>Fakultas</td>
           <td>:  '.$isi_mhs["NM_FAK"].'</td>
        </tr>

    </table>    
<p>berdasar pada keputusan Rapat Yudisium Pimpinan '.ucwords(strtolower($template["kopsurat"]["fkl"])).' '.ucwords(strtolower($template["kopsurat"]["univ"])).' pada tanggal '.display_tanggal_indo(transform_datetime_format($isi_surat["DATA_TAMBAHAN"]["TGL_YUDISIUM"], "d-m-Y h:i:s", "Y-m-d"), "Y-m-d").', yang bersangkutan dinyatakan <b>LULUS</b> program '.$isi_surat["DATA_TAMBAHAN"]["JENJANG"].' dengan IPK '.$isi_surat["DATA_TAMBAHAN"]["IPK"].' predikat <i>'.ucwords(strtolower($isi_surat["DATA_TAMBAHAN"]["PREDIKAT"])).'</i>.
</p><br>
<p>Surat keterangan ini berlaku sampai dengan tanggal '.display_tanggal_indo($isi_surat["BATAS_MASA_BERLAKU"], "Y-m-d").'</p>
<p>Demikian surat keterangan ini dibuat untuk dipergunakan sebagaimana mestinya.</p>
';

$txt1 = '<table cellpadding="0">    
    <tr>
        <td colspan="3"></td>
        <td align="R"></td>
        <td align="L" colspan="2"><span>Yogyakarta, '.$tgl_surat.'</span><br></td>
    </tr>    
    <tr>
        <td colspan="3"></td>
        <td align="L"></td>
        <td align="L"  colspan="2">'.$psd["label_psd"].',<br><br><br><br>'.$psd["nm_pgw_psd"].'<br>NIP. '.trans_nip_pgw($psd["nip_pgw_psd"]).'
        </td>
    </tr>
</table>';

// set font
$pdf->SetFont('times', '', 12);
// print a blox of text using writeHTMLCell()
$pdf->WriteHTMLCell(0,0,'','',$nama_surat, 0, 1, 0, true, 'C', true);
$pdf->WriteHTMLCell(0,0,'','',$txt, 0, 1, 0, true, 'L', true);
$pdf->WriteHTMLCell(0,0,'','',$txt1, 0, 1, 0, true, 'R', true);
//$pdf->WriteHTMLCell(0,0,'','',$txt2, 0, 1, 0, true, 'L', true);

$style = array(
    'border' => 0,
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255)
    'module_width' => 0.1, // width of a single module in points
    'module_height' => 0.1 // height of a single module in points
);

//$pdf->Ln();
// QRCODE,L : QR-CODE Low error correction
$pdf->write2DBarcode($qrcode_otentikasi, 'QRCODE,L', '', '', 20, 20, $style, 'N');

// move pointer to last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
ob_clean(); //untuk menghapus output buffer
$pdf->Output('surat_keterangan_lulus-'.$isi_mhs["NIM"].'_'.time().'.pdf', 'I'); #setting inline  (I) agar didownload browser

//============================================================+
// END OF FILE
//============================================================+
