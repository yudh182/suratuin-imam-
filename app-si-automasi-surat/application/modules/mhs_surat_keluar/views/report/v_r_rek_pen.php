<?php
//============================================================+
// template report   : Surat Ijin Observasi
// Last Update : 2018-03-23


$pdf = new Pdf_creator(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->set_document_properties(
    array(
        'pdf_title'=>'Surat Permohonan Rekomendasi Penelitian',
        'pdf_margin' => ['20','35','20', true], #LEFT, TOP, RIGHT (PAGE MARGINS)
        'pdf_template_margin' => ['6','12'], #MARGIN HEADER, MARGIN FOOTER
        'pdf_page_break' => [true,'12'] #aktif/tdk aktif, MARGIN BOTTOM (DEFAULT : isikan INITIAL)
    )
);
$pdf->setData($template);

// add a page
// $resolution= array(175, 266);
// $pdf->AddPage('P', $resolution);
$pdf->AddPage('P', 'A4');

// The '@' character is used to indicate that follows an image data stream and not an image file name
if (isset($watermark_img)) :
    $base64_decoded = base64_decode($watermark_img);
    $pdf->Image('@'.$base64_decoded);
endif;

// set some text for example
$nama_surat ='<table>
    <tr>
        <td width="50">Nomor</td>
        <td width="240">: '.$nomor.'</td>
        <td align="R" width="250px">'.$tgl_surat.'</td>
    </tr>
    <tr>
        <td  width="50px">Lamp.</td>
        <td>: 3</td>
        <td></td>
    </tr>
    <tr>
        <td  width="50px">Hal</td>
        <td>: Permohonan Rekomendasi Penelitian</td>

        <td></td>
    </tr>
</table>';

$txt1='Kepada:<br>
Yth. '.$tujuan["kepada"].' '.$tujuan["instansi"].'<br>
di '.$tujuan["alamat_tempat"].'
';

$txt2 = '<p>
<i>Assalamualaikum Wr.Wb</i><br><br>
Diberitahukan dengan hormat, berkenaan dengan '.$isi_surat["KEPERLUAN"].' dengan judul : <br>
<b><i>"'.$isi_surat["JUDUL_DLM_KEGIATAN"].'"</i></b><br>, mahasiswa kami perlu melakukan '.$isi_surat["NM_KEGIATAN"].' guna pengumpulan data yang akurat. Oleh karena itu, kami mohon bantuan dan kerjasama untuk memberikan rekomendasi izin penelitian bagi mahasiswa berikut,</p>
<table>
         <tr>
            <td width="120pt">Nama</td>
            <td width="360pt">:  '.ucwords(strtolower($isi_mhs["NAMA"])).'</td>
        </tr>
        <tr>
            <td>NIM</td>
            <td>:  '.$isi_mhs["NIM"].'</td>
        </tr>        
        <tr>
           <td>Semester</td>
           <td>:  '.$isi_mhs["JUM_SMT"].'</td>
        </tr>         
        <tr>
           <td>Fakultas</td>
           <td>:  '.$isi_mhs["NM_FAK"].'</td>
        </tr>
        <tr>
           <td>Program Studi</td>
           <td>:  '.$isi_mhs["NM_PRODI"].'</td>
        </tr>


    </table>
<p>untuk dapat mengadakan '.$isi_surat["NM_KEGIATAN"].' di '.$isi_surat["TEMPAT_KEGIATAN"].' dengan metode pengumpulan data meliputi: '.$isi_surat["METODE_DLM_KEGIATAN"].'. Adapun waktunya pelaksanaannya antara tanggal <b>'.display_tanggal_indo($isi_surat["TGL_MULAI"], 'Y-m-d').'</b> sd. <b>'.display_tanggal_indo($isi_surat["TGL_BERAKHIR"], 'Y-m-d').'</b>. </p>
<p>Demikian surat ini kami sampaikan, atas perhatian dan kerjasama Bapak/Ibu kami ucapkan terimakasih.</p><br>
<i>Wassalamualaikum Wr.Wb</i>';


// $txt4 = '<table><tr>
// <td></td>
// <td></td>
// <td width="200px" align="L">Yogyakarta '.$tgl_surat.' <br>
//          a.n. Dekan<br>
//                 Wakil Dekan Bidang Akademik
//                 <br><br><br><br>




//        '.$nm_pgw_psd.'<br></td>
//         </tr>
//         </table>
// ';
// $txt4 = '<table cellpadding="0">
//     <tr>
//         <td colspan="2"></td>
//         <td align="R">a.n.&nbsp;&nbsp;&nbsp;&nbsp;</td>
//         <td align="L" colspan="2">'.$label_psd_an.'</td>
//     </tr>
//     <tr>
//         <td colspan="2"></td>
//         <td align="L"></td>
//         <td align="L"  colspan="2">'.$label_psd.',
//         <br><br><br><br>
//         '.$nm_pgw_psd.'
//         </td>
//     </tr>
// </table>';
$txt4 = '<table cellpadding="0">
    <tr>
        <td colspan="3"></td>
        <td align="R"></td>
        <td colspan="2"><span>'.$tgl_surat.'</span><br></td>
    </tr>
    <tr>
        <td colspan="3"></td>
        <td align="R">a.n.&nbsp;&nbsp;</td>
        <td align="L" colspan="2">'.$psd["label_psd_an"].'</td>
    </tr>
    <tr>
        <td colspan="3"></td>
        <td align="L"></td>
        <td align="L"  colspan="2">'.$psd["label_psd"].',
        <br><br><br><br>
        '.$psd["nm_pgw_psd"].'
        </td>
    </tr>
</table>';

// $txt5='<p><u>Tembusan</u><br>
// Dekan (Sebagai laporan)';
$txt5 = $daftar_tembusan_htmlstring;

// set font
$pdf->SetFont('times', '', 11);
// print a blox of text using multicell()
$pdf->WriteHTMLCell(0,0,'','',$nama_surat, 0, 1, 0, true, 'L', true);
$pdf->WriteHTMLCell(0,0,'','',$txt1, 0, 1, 0, true, 'J', true);
$pdf->WriteHTMLCell(0,0,'','',$txt2, 0, 1, 0, true, 'J', true);
//$pdf->WriteHTMLCell(0,0,'','',$txt3, 0, 1, 0, true, 'J', true);
$pdf->WriteHTMLCell(0,0,'','',$txt4, 0, 1, 0, true, 'L', true);
$pdf->WriteHTMLCell(0,0,'','',$txt5, 0, 1, 0, true, 'L', true);

$style = array(
    'border' => 0,
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255)
    'module_width' => 0.1, // width of a single module in points
    'module_height' => 0.1 // height of a single module in points
);
// QRCODE,L : QR-CODE Low error correction
$pdf->write2DBarcode($qrcode_otentikasi, 'QRCODE,L', '', '', 20, 20, $style, 'N');

// move pointer to last page
$pdf->lastPage();

// ---------------------------------------------------------
//ob_end_clean();

//Close and output PDF document
ob_clean(); //untuk menghapus output buffer
$pdf->Output('surat_permohonan_rekomendasi_penelitian_'.time().'.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+