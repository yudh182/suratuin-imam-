<?php
//============================================================+
// template report   : Surat Ijin Studi Pendahuluan
// Last Update : 2018-03-23


$pdf = new Pdf_creator(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->set_document_properties(
    array(
        'pdf_title'=>'Surat Ijin Penelitian',
        'pdf_margin' => ['15','35','15', true], #LEFT, TOP, RIGHT (PAGE MARGINS)
        'pdf_template_margin' => ['6','9'], #MARGIN HEADER, MARGIN FOOTER
        'pdf_page_break' => [true,'20'] #aktif/tdk aktif, MARGIN BOTTOM (DEFAULT : isikan INITIAL)
    )
);
$pdf->setData($template);

// add a page
$resolution= array(175, 266);
$pdf->AddPage('P', $resolution);
$pdf->setListIndentWidth(3);

// MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)

// set some text for example
$nama_surat ='<table>
    <tr>
        <td width="50px">Nomor</td>
        <td width="210px">: '.$nomor.'</td>
        <td align="R" width="240px">'.$tgl_surat.'</td>
    </tr>
    <tr>
        <td  width="50px">Lamp.</td>
        <td>:</td>
        <td></td>
    </tr>
    <tr>
        <td width="50px">Hal</td>
        <td width="200px">: Permohonan Izin Studi Pendahuluan</td>

        <td></td>
    </tr>
</table>';
$txt='Kepada:<br>
Yth. '.$tujuan["kepada"].' '.$tujuan["instansi"].'<br>
di '.$tujuan["alamat_tempat"].'
<p>
<i>Assalamualaikum Wr.Wb</i><br><br>
Kami beritahukan bahwa untuk '.$isi_surat["keperluan"].' dengan tema : <br>
"<b>'.$isi_surat["tema"].'</b>" diperlukan '.$isi_surat["nama_kegiatan"].'.<br><br>
Oleh karena itu, kami mengharap kiranya Bapak/Ibu '.$tujuan["kepada"].'  berkenan memberi izin kepada mahasiswa kami : <p><br><br>
<table>
    <tr>
        <td style="width: 130px;">Nama</td>
        <td>:  '.$isi_mhs["nama"].'</td>
    </tr>
    <tr>
        <td>NIM</td>
        <td>:  '.$isi_mhs["nim"].'</td>
    </tr>
    <tr>
        <td>Semester</td>
        <td>:  '.$isi_mhs["smt"].'</td>
    </tr>
    <tr>
        <td>Program Studi</td>
        <td>:  '.$isi_mhs["prodi"].'</td>
    </tr>
</table><br><br>
untuk melakukan wawancara di '.$isi_surat["tempat_penelitian"].' yang Bapak/Ibu Pimpin pada tanggal '.$isi_surat["tgl_mulai"].'<br><br>

Demikian Surat permohonan ini disampaikan, atas diperkenankannya diucapkan terimakasih<br><br>
<i>Wassalamualaikum Wr.Wb</i>';

$txt2 = '<table cellpadding="0">
    <tr>
        <td colspan="2"></td>
        <td align="R">a.n.&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td align="L" colspan="2">'.$psd["label_psd_an"].'</td>
    </tr>
    <tr>
        <td colspan="2"></td>
        <td align="L"></td>
        <td align="L"  colspan="2">'.$psd["label_psd"].',
        <br><br><br><br>
        '.$psd["nm_pgw_psd"].'
        </td>
    </tr>
</table>';
$txt3=$daftar_tembusan_htmlstring;

// set font
$pdf->SetFont('times', '', 10);
// print a blox of text using multicell()
$pdf->WriteHTMLCell(0,0,'','',$nama_surat, 0, 1, 0, true, 'L', true);
$pdf->WriteHTMLCell(0,0,'','',$txt, 0, 1, 0, true, 'L', true);
$pdf->WriteHTMLCell(0,0,'','',$txt2, 0, 1, 0, true, 'R', true);
$pdf->WriteHTMLCell(0,0,'','',$txt3, 0, 1, 0, true, 'L', true);

$style = array(
    'border' => 0,
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255)
    'module_width' => 0.1, // width of a single module in points
    'module_height' => 0.1 // height of a single module in points
);

// QRCODE,L : QR-CODE Low error correction
$pdf->write2DBarcode($qrcode_otentikasi, 'QRCODE,L', 18, '', 20, 20, $style, 'N');

// move pointer to last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
ob_clean(); //untuk menghapus output buffer
$pdf->Output('surat_studi_pendahuluan_'.time().'.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
