<?php
/**
* ============================================================+
* File name   : v_r_lulus.php
* @since       : 2018-04-21
* Last Update : 2018-03-25
*/
// Description : Template tcpdf untuk surat keterangan lulus

// create new PDF document
$pdf = new Pdf_creator(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->set_document_properties(
    array(
        'pdf_title'=>'Surat Keterangan Habis Teori',
        'pdf_margin' => ['20','40','20', true], #LEFT, TOP, RIGHT (PAGE MARGINS)
        'pdf_template_margin' => ['6','12'], #MARGIN HEADER, MARGIN FOOTER
        'pdf_page_break' => [true,'20'] #aktif/tdk aktif, MARGIN BOTTOM (DEFAULT : isikan INITIAL)
    )
);
$pdf->setData($template);

// add a page
// $resolution= array(175, 266);
// $pdf->AddPage('P', $resolution);
$pdf->AddPage('P', 'A4');

// set some text for example
$nama_surat ='
<b>SURAT KETERANGAN HABIS TEORI</b><br>
Nomor : '.$nomor.'
';
$txt = '<p>Yang bertanda tangan di bawah ini: <p>
<table cellpadding="0" cellspacing="0">
            <tr>
                <td width="120pt">Nama</td>
                <td width="360pt">: '.$pejabat_dalam_surat["NM_PGW_F"].'</td>
            </tr>
            <tr><td>NIP</td><td>: '.trans_nip_pgw($pejabat_dalam_surat["NIP"]).'</td></tr>
            <tr><td>Pangkat / Gol. Ruang</td><td>: '.$pejabat_dalam_surat["HIE_NAMA"].' / '.$pejabat_dalam_surat["HIE_GOLRU"].'</td></tr>
            <tr><td>Jabatan</td><td>: '.$pejabat_dalam_surat["STR_NAMA"].'</td></tr>
        </table>        
        <p>Dengan ini menerangkan bahwa:</p>
<table>
    <tr>
        <td width="120pt">Nama</td>
        <td width="360pt">:  '.ucwords(strtolower($isi_mhs["NAMA"])).'</td>
    </tr>
    <tr>
        <td>NIM</td>
        <td>:  '.$isi_mhs["NIM"].'</td>
    </tr>
     <tr>
        <td>Program Studi</td>
        <td>:  '.$isi_mhs["NM_PRODI"].'</td>
    </tr>
    <tr>
        <td>Semester</td>
        <td>:  '.$isi_mhs["JUM_SMT"].'</td>
    </tr>
    <tr>
        <td>Fakultas</td>
        <td>:  '.$isi_mhs["NM_FAK"].'</td>
    </tr>     
</table>
<p>telah menyelesaikan semua beban mata kuliah teori, Kerja Praktek, dan KKN dengan nilai terendah C berjumlah '.$isi_surat["bebas_teori"]["jum_nl_cmin"].', IP Kumulatif '.$isi_surat["bebas_teori"]["ipk"].', dan telah memenuhi persyaratan untuk mengikuti sidang Munaqasyah.</p>
<p>Demikian surat ini dibuat dengan sebenarnya untuk dapat dipergunakan sebagaimana mestinya.</p><br><br>
';


$txt1 = '<table cellpadding="0">
    <tr>
        <td colspan="3"></td>
        <td align="R"></td>
        <td colspan="2"><span>'.$tgl_surat.'</span><br></td>
    </tr>
    <tr>
        <td colspan="3"></td>
        <td align="R">a.n.&nbsp;&nbsp;</td>
        <td align="L" colspan="2">Dekan'.$psd["label_psd_an"].'</td>
    </tr>
    <tr>
        <td colspan="3"></td>
        <td align="L"></td>
        <td align="L"  colspan="2">'.$psd["label_psd"].' Bagian Tata Usaha,
        <br><br><br><br>'.$psd["nm_pgw_psd"].'
        </td>
    </tr>
</table>';

$txt2=$daftar_tembusan_htmlstring;


// set font
$pdf->SetFont('times', '', 12);
// print a blox of text using writeHTMLcell()
$pdf->WriteHTMLCell(0,0,'','',$nama_surat, 0, 1, 0, true, 'C', true);
$pdf->WriteHTMLCell(0,0,'','',$txt, 0, 1, 0, true, 'L', true);
$pdf->WriteHTMLCell(0,0,'','',$txt1, 0, 1, 0, true, 'L', true);
$pdf->WriteHTMLCell(0,0,'','',$txt2, 0, 1, 0, true, 'L', true);

$style = array(
    'border' => 0,
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255)
    'module_width' => 0.1, // width of a single module in points
    'module_height' => 0.1 // height of a single module in points
);

// QRCODE,L : QR-CODE Low error correction
$pdf->write2DBarcode($qrcode_otentikasi, 'QRCODE,L', '', '', 20, 20, $style, 'N');

// move pointer to last page
$pdf->lastPage();

//Close and output PDF document
ob_clean(); //untuk menghapus output buffer
$pdf->Output('surat_keterangan_habis_teori_'.time().'.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
