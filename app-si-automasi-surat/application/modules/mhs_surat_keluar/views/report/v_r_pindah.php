<?php
//============================================================+
// File name   : v_r_pindah.php
// Begin       : 2018-03-22
// Last Update : 2018-03-25
//
// Description : Template tcpdf untuk surat keterangan pindah studi

// create new PDF document
$pdf = new Pdf_creator(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->set_document_properties(
    array(
        'pdf_title'=>'Surat Keterangan Pindah Studi',
        'pdf_margin' => ['20','40','20', true], #LEFT, TOP, RIGHT (PAGE MARGINS)
        'pdf_template_margin' => ['6','12'], #MARGIN HEADER, MARGIN FOOTER
        'pdf_page_break' => [true,'20'] #aktif/tdk aktif, MARGIN BOTTOM (DEFAULT : isikan INITIAL)
    )
);
$pdf->setData($template);

// add a page
$pdf->AddPage('P', 'A4');

//set watermark (khusus preview)
if (isset($watermark_img)) :
    $base64_decoded = base64_decode($watermark_img);
    $pdf->Image('@'.$base64_decoded);    
endif;

// set some text for example
$nama_surat ='
<b>'.$judul_surat.'</b><br>
Nomor : '.$nomor.'
<br>';
$txt = '<p>Yang bertanda tangan di bawah ini : </p>
<table cellpadding="0" cellspacing="0">
            <tr>
                <td width="120pt">Nama</td>
                <td width="360pt">: '.$pejabat_dalam_surat["NM_PGW_F"].'</td>
            </tr>
            <tr><td>NIP</td><td>: '.trans_nip_pgw($pejabat_dalam_surat["NIP"]).'</td></tr>
            <tr><td>Pangkat / Gol. Ruang</td><td>: '.$pejabat_dalam_surat["HIE_NAMA"].' / '.$pejabat_dalam_surat["HIE_GOLRU"].'</td></tr>
            <tr><td>Jabatan</td><td>: '.$pejabat_dalam_surat["STR_NAMA"].'</td></tr>
        </table>
        <p>Dengan ini menerangkan bahwa:</p>
<table>
     <tr>
        <td width="120pt">Nama</td>
        <td width="360pt">:  '.ucwords(strtolower($isi_mhs["nama"])).'</td>
    </tr>
    <tr>
        <td>NIM</td>
        <td>:  '.$isi_mhs["nim"].'</td>
    </tr>
    <tr>
       <td>Semester</td>
       <td>:  '.trans_angka_romawi($isi_mhs["smt"]).'</td>
    </tr>
     <tr>
        <td>Tahun Akademik</td>
        <td>:  '.$thn_akademik["tahun_ajaran"].'</td>
    </tr>
</table>
<p>adalah mahasiswa Fakultas '.$isi_mhs["fkl"].' UIN Sunan Kalijaga Yogyakarta, mahasiswa tersebut: Tercatat pada Semester I sampai dengan Semester '.trans_angka_romawi($isi_mhs["smt"]).' Tahun Akademik '.$thn_akademik["tahun_ajaran"].' pada Program Studi '.$isi_mhs["prodi"].'.</p>

<p>Surat keterangan diterbitkan sebagai salah satu syarat untuk '.$isi_surat["keperluan"].'.</p>
<p>Demikian agar dapat dipergunakan sebagaimana mestinya.</p>';

$txt2 = '<table cellpadding="0">
    <tr>
        <td colspan="3"></td>
        <td align="R"></td>
        <td colspan="2" width="220"><span>'.$tgl_surat.'</span><br></td>
    </tr>
    <tr>
        <td colspan="3"></td>
        <td align="R">a.n.&nbsp;&nbsp;</td>
        <td align="L" colspan="2">Dekan'.$psd["label_psd_an"].'</td>
    </tr>
    <tr>
        <td colspan="3"></td>
        <td align="L"></td>
        <td align="L"  colspan="2">'.$psd["label_psd"].',
        <br><br><br><br>'.$psd["nm_pgw_psd"].'
        </td>
    </tr>
</table>';

// $txt3='<p>Tembusan :<br>
// 1. Dekan (Sebagai laporan);<br>
// 2. Kepala Biro AAAK;<br>
// 3. Mahasiswa yang bersangkutan.</p>
// ';
$txt3=$daftar_tembusan_htmlstring;


// set font
$pdf->SetFont('times', '', 12);
// print a blox of text using multicell()
$pdf->WriteHTMLCell(0,0,'','',$nama_surat, 0, 1, 0, true, 'C', true);
$pdf->WriteHTMLCell(0,0,'','',$txt, 0, 1, 0, true, 'J', true);
$pdf->WriteHTMLCell(0,0,'','',$txt2, 0, 1, 0, true, 'L', true);
$pdf->WriteHTMLCell(0,0,'','',$txt3, 0, 1, 0, true, 'L', true);

$style = array(
    'border' => 0,
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255)
    'module_width' => 0.1, // width of a single module in points
    'module_height' => 0.1 // height of a single module in points
);


// QRCODE,L : QR-CODE Low error correction
$pdf->write2DBarcode($qrcode_otentikasi, 'QRCODE,L', '', '', 20, 20, $style, 'N');

// move pointer to last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
ob_clean(); //untuk menghapus output buffer
$pdf->Output('surat_keterangan_pindah_studi_'.time().'.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
