<?php
//============================================================+
// File name   : v_r_kel_baik.php
// Begin       : 2018-03-22
// Last Update : 2018-03-25
//
// Description : Template tcpdf untuk surat keterangan berkelakuan baik

// create new PDF document
$pdf = new Pdf_creator(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->set_document_properties(
    array(
        'pdf_title'=>'Surat Keterangan Berkelakuan Baik',
        'pdf_margin' => ['20','40','20', true], #LEFT, TOP, RIGHT (PAGE MARGINS)
        'pdf_template_margin' => ['6','12'], #MARGIN HEADER, MARGIN FOOTER
        'pdf_page_break' => [true,'25'] #aktif/tdk aktif, MARGIN BOTTOM (DEFAULT : isikan INITIAL)
    )
);
$pdf->setData($template);

// add a page (dalam mm)
// $resolution= array(210, 297); 
// $pdf->AddPage('P', $resolution);
$pdf->AddPage('P');

//set watermark (khusus preview)
if (isset($watermark_img)) :
    $base64_decoded = base64_decode($watermark_img);
    $pdf->Image('@'.$base64_decoded);    
endif;

// set some text for example
$nama_surat = '<b>'.$judul_surat.'</b><br>Nomor : '.$nomor;
$txt = '<p>Yang bertanda tangan di bawah ini : </p>
<table cellpadding="0" cellspacing="0">
            <tr>
                <td width="120pt">Nama</td>
                <td width="360pt">: '.$pejabat_dalam_surat["NM_PGW_F"].'</td>
            </tr>
            <tr><td>NIP</td><td>: '.$pejabat_dalam_surat["NIP"].'</td></tr>
            <tr><td>Pangkat / Gol. Ruang</td><td>: '.$pejabat_dalam_surat["HIE_NAMA"].' / '.$pejabat_dalam_surat["HIE_GOLRU"].'</td></tr>
            <tr><td>Jabatan</td><td>: '.$pejabat_dalam_surat["STR_NAMA"].'</td></tr>
        </table>
        <p>Dengan ini menerangkan bahwa:</p>
<table cellpadding="0" cellspacing="0">
     <tr>
        <td width="120pt">Nama</td>
        <td width="360pt">:  '.$isi_mhs["nama"].'</td>
    </tr>
    <tr>
        <td>NIM</td>
        <td>:  '.$isi_mhs["nim"].'</td>
    </tr>
    <tr>
       <td>Semester</td>
       <td>:  '.$isi_mhs["smt"].'</td>
    </tr>
    <tr>
       <td>Progam Studi</td>
       <td>:  '.$isi_mhs["prodi"].'</td>
    </tr>
     <tr>
        <td>Tahun Akademik</td>
        <td>:  '.$thn_akademik["tahun_ajaran"].'</td>
    </tr>
</table><br><br>
adalah benar-benar mahasiswa Fakultas '.ucwords(strtolower($kop["fkl"])).' Universitas Islam Negeri Sunan Kalijaga Yogyakarta, dan sepanjang pengetahuan kami mahasiswa tersebut di atas berkelakuan baik dan belum pernah terlibat masalah Narkoba atau penyimpangan perilaku.<br><br>
Demikian surat keterangan ini dibuat untuk dipergunakan sebagaimana mestinya.
';
// $txt1 = '<table>
// <tr>
//     <td align="R"><br><br>
//         a.n. Dekan<br>
//         '.$psd["label_psd"].' Bagian Tata Usaha<br>
//         <br><br>
//         <br><br>
//         <b>'.$psd["nm_pgw_psd"].'</b><br>
//     </td>
// </tr>
// </table>';
$txt1 = '<table cellpadding="0">
    <tr>
        <td colspan="3"></td>
        <td align="R">a.n.&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td align="L" colspan="2">Dekan</td>
    </tr>
    <tr>
        <td colspan="3"></td>
        <td align="L"></td>
        <td align="L"  colspan="2">'.$psd["label_psd"].' Bagian Tata Usaha,
        <br><br><br><br>'.$psd["nm_pgw_psd"].'
        </td>
    </tr>
</table>';
$txt2=$daftar_tembusan_htmlstring;
// $txt2 = '
// <u>Tembusan:</u><br>
// 1. Dekan (sebagai laporan);<br>
// 2. Yang bersangkutan';

// set font
$pdf->SetFont('times', '', 12);
// print a blox of text using writeHTMLCell()
$pdf->WriteHTMLCell(0,0,'','',$nama_surat, 0, 1, 0, true, 'C', true);
$pdf->WriteHTMLCell(0,0,'','',$txt, 0, 1, 0, true, 'J', true);
$pdf->WriteHTMLCell(0,0,'','',$txt1, 0, 1, 0, true, 'R', true);
$pdf->WriteHTMLCell(0,0,'','',$txt2, 0, 1, 0, true, 'L', true);

$style = array(
    'border' => 0,
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255)
    'module_width' => 0.1, // width of a single module in points
    'module_height' => 0.1 // height of a single module in points
);

//$pdf->Ln();
// QRCODE,L : QR-CODE Low error correction
$pdf->write2DBarcode($qrcode_otentikasi, 'QRCODE,L', '', '', 20, 20, $style, 'N');

// move pointer to last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
ob_clean(); //untuk menghapus output buffer
$pdf->Output('surat_keterangan_berkelakuan_baik'.time().'.pdf', 'I'); #setting inline  (I) agar didownload browser

//============================================================+
// END OF FILE
//============================================================+
