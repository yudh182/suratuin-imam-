<?php
//============================================================+
// File name   : v_r_kp.php
// Begin       : 2018-03-22
// Last Update : 2018-03-25
//
// Description : Template tcpdf untuk surat ijin kerja praktek

// create new PDF document
$pdf = new Pdf_creator(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->set_document_properties(
    array(
        'pdf_title'=>'Surat Permohonan Izin Kerja Praktek',
        'pdf_margin' => ['30','35','20', true], #LEFT, TOP, RIGHT (PAGE MARGINS)
        'pdf_template_margin' => ['6','12'], #MARGIN HEADER, MARGIN FOOTER
        'pdf_page_break' => [true,'20'] #aktif/tdk aktif, MARGIN BOTTOM (DEFAULT : isikan INITIAL)
    )
);
$pdf->setData($template);


// add a page
$pdf->AddPage('P', 'A4');
//set watermark (khusus preview)
if (isset($watermark_img)) :
    $base64_decoded = base64_decode($watermark_img);
    $pdf->Image('@'.$base64_decoded);    
endif;

// set some text for example
$nama_surat ='<table>
    <tr>
        <td width="50px">Nomor</td>
        <td width="210px">: '.$nomor.' </td>
        <td align="R" width="250px">'.$tgl_surat.'</td>
    </tr>
    <tr>
        <td>Lamp.</td>
        <td>: 1(satu) proposal</td>
        <td></td>
    </tr>
    <tr>
        <td>Hal</td>
        <td>: Permohonan '.$isi_surat["nama_kegiatan"].'</td>
        
        <td></td>
    </tr>
</table>';

$txt='Kepada:<br>
Yth. '.$tujuan["pimpinan_instansi"].'<br>
di '.$tujuan["alamat"].'.
<p>
<i>Assalamualaikum Wr.Wb</i><br><br>
Dengan hormat, mohon perkenan Bapak/Ibu '.$tujuan["pimpinan_instansi"].' mengizinkan mahasiswa kami di bawah ini 
: <br>
<table>
    <tr>
        <td  style="width: 130px;">Nama</td>
        <td>:  '.$isi_mhs["nama"].'</td>
    </tr>
    <tr>
        <td>NIM</td>
        <td>:  '.$isi_mhs["nim"].'</td>
    </tr>
    <tr>
        <td>Semester</td>
        <td>:  '.trans_angka_romawi($isi_mhs["smt"]).'</td>
    </tr>
    <tr>
        <td>Bidang Minat</td>
        <td>:  '.$isi_surat["minat"].'</td>
    </tr>
</table>        
<br><br>

untuk melaksanakan '.$isi_surat["nama_makul"].' di Instansi yang Bapak/Ibu pimpin dalam rangka program pengalaman lapangan pada Program Studi '.$isi_mhs["prodi"].'<p><br><br>
Demikian atas perhatian dan perkenannya diucapkan terima kasih.<br><br>

<i>Wassalamualaikum Wr.Wb</i>';

/*$txt2 = '<table><tr>
<td>a.n</td><td>'.$label_psd_an.'</td></tr>
<tr><td></td><td>'.$label_psd.'</td></tr>
        <tr><td colspan="2">
        <br><br><br><br>
        </td></tr>
        </tr>
        <tr><td></td><td><b>'.$nm_pgw_psd.'</b></td>
        </tr>  
</table>';*/
$txt2 = '<table cellpadding="0">    
    <tr>
        <td colspan="2"></td>
        <td align="R">a.n.&nbsp;&nbsp;</td>
        <td align="L" colspan="2">Dekan'.$psd["label_psd_an"].'</td>
    </tr>
    <tr>
        <td colspan="2"></td>
        <td align="L"></td>
        <td align="L"  colspan="2">'.$psd["label_psd"].',
        <br><br><br><br>'.$psd["nm_pgw_psd"].'<br>NIP. '.trans_nip_pgw($psd["nip_pgw_psd"]).'
        </td>
    </tr>
</table>';

$txt3='<p>Tembusan :<br>
Dekan (sebagai laporan);<br>';

// set font
$pdf->SetFont('times', '', 12);
// print a blox of text using multicell()
$pdf->WriteHTMLCell(0,0,'','',$nama_surat, 0, 1, 0, true, 'L', true);
$pdf->WriteHTMLCell(0,0,'','',$txt, 0, 1, 0, true, 'L', true);
$pdf->WriteHTMLCell(0,0,'','',$txt2, 0, 1, 0, true, 'R', true);
$pdf->WriteHTMLCell(0,0,'','',$txt3, 0, 1, 0, true, 'L', true);

$style = array(
    'border' => 0,
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255)
    'module_width' => 0.1, // width of a single module in points
    'module_height' => 0.1 // height of a single module in points
);

// QRCODE,L : QR-CODE Low error correction
$pdf->write2DBarcode($qrcode_otentikasi, 'QRCODE,L', '', '', 20, 20, $style, 'N');
// move pointer to last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
ob_clean(); //untuk menghapus output buffer
$pdf->Output('surat__permohonan_kerja_Praktek'.time().'.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
