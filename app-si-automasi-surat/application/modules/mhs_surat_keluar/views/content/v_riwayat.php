<div class="col-med-9">
	<div class="content-space">
	
	<h2 class="h-border">Riwayat Surat Saya</h2>

	<div class="bs-callout bs-callout-info">
		Klik tombol <a class="btn btn-default btn-small" href="#">lihat</a> untuk dapat melihat status surat hingga mencetak ulang pdf
	</div>

	<?php if (!empty($riwayat)) : ?>
	<table class="table table-bordered table-hover">
	  <thead>
		  <tr>
			  <th width="20px"><center>No</center></th>
			  <th width="140px"><center>Nomor Surat</center></th>
			  <th width="72px"><center>Tanggal Surat</center></th>
			  <th width="100px"><center>Jenis Surat</center></th>
			  <th><center>Keperluan</center></th>
			  <th width="100px"><center>Aksi</center></th>
		  </tr>
		</thead>
		 <tbody id="content-check">
		<?php foreach ($riwayat as $key => $val) : 
			$no = $key + 1;
		
		?>
		<tr>
			<td><?=  $no; ?></td>
			<td><?= (!empty($val['NO_SURAT']) ? $val['NO_SURAT'] : '<span class="badge">belum bernomor</span>');?></td>
			<td><?= display_tanggal_indo($val['TGL_SURAT'], 'Y-m-d'); ?></td>
			<td><?= $val['NM_JENIS_SAKAD']; ?></td>
			<td><?php echo (!empty($val['KEPERLUAN']) ? $val['KEPERLUAN'] : ''); ?></td>
			<td>
				<button class="btn btn-default btn-small" data-id_surat="<?php echo $val['ID_SURAT_KELUAR'];?>" onclick="detail($(this));" title="Lihat & Edit Data" data-rel="tooltip">
					Lihat
				</button>
				<br>
				<form method="POST" action="<?php echo site_url('mhs_surat_keluar/cetak_surat_tersimpan/') . $val['ID_SURAT_KELUAR']; ?>" target="_blank">
					<input type="submit" class="btn-uin btn btn-inverse btn-small" value="Cetak" />
				</form>
				<br>
				<!-- <button type="button" class="btn btn-default btn-small openDialogHapus" data-toggle="modal" data-target="#modal-hapus-draf" data-id_surat="<?php echo $val['ID_SURAT_KELUAR']; ?>">hapus (draf) <i class="fa fa-trash"></i></button>
				<br> -->
				<!-- <button type="button" class="btn btn-success" data-id_surat="<?php echo $val['ID_SURAT_KELUAR']; ?>" onclick="tampilModalTengok($(this))">
					tengok <i class="fa fa-eye"></i>
				</button> -->
			</td>
		</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
	<?php endif; ?>


	<!-- detail surat (load dengan ajax)-->
	<div id="content_result">
		<div id="result">&nbsp;</div>
	</div>


	</div>
</div>


<!-- mulai konten sembunyi -->
<!-- Modal -->
<div id="modal-hapus-draf" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Hapus draf surat</h4>
      </div>
      <div class="modal-body">
        	<p>
				Apakah anda yakin benar-benar akan menghapus draf surat ini ?
			</p>
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">tidak</button>

        <form action="<?= base_url('mhs_surat_keluar/kelola/hapus_surat_keluar'); ?>">
        	<button type="submit" class="btn btn-danger confirm-delete">ya</button>
        </form>
        
      </div>
    </div>

  </div>
</div>

<div class="modal fade" id="modal-hapus-sk" role="dialog" aria-labelledby="modalLabelHapus" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><i class="icon-remove"></i></button>
				<div class="modalLabelHapus">
					<h3>Konfirmasi</h3>
				</div>
			</div>
			<div class="modal-body">
				<p><!--<img class="modal-icon" src="img/trash.png" />--> Apakah Anda yakin ingin menghapus data ini ?</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn btn-default rec" data-dismiss="modal">Tidak</a>
				<form action="<?php echo base_url('pegawai/act_surat_keluar/del_sk');?>" method="POST" >					
					<input type="text" id="id_surat_keluar" name="id_surat_keluar" value="" />
					<input type="text" name="del-sk" value="Delete Surat Keluar" />
					<input type="submit" class="btn btn-danger" value="Ya" />
				</form>
			</div>
		</div>
	</div>
</div>

<div id="modal-tengok" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Info detail surat</h4>
      </div>
      <div class="modal-body">
        	<p>
				tengok tengok tengok
			</p>
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">tidak</button>
        <button type="button" class="btn btn-default">Tandai Pin <span class="fa fa-pin"></span></button>
      </div>
    </div>

  </div>
</div>
<!-- akhir konten sembunyi -->

<script type="text/javascript">
	// function detail(el){
	// 	var id_surat = el.data('id_surat');

	// 	alert(id_surat);
	// }
	$(document).on("click", ".openDialogHapus", function () {
	     var dis = $(this).data('id_surat');
	     $(".modal-footer #id_surat_keluar").val( dis );
	    //$('#addBookDialog').modal('show');
	});
	$('#modal-hapus-draf').on('show.bs.modal', function(e) {
        var $modal = $(this),
            esseyId = e.relatedTarget.class;
        console.log(esseyId);

        // $.ajax({
        //     cache: false,
        //     type: 'POST',
        //     url: 'backend.php',
        //     data: 'EID=' + essayId,
        //     success: function(data) {
        //         $modal.find('.edit-content').html(data);
        //     }
        // });
    });

	function tampilModalTengok(el){
		var id_surat_keluar = el.data('id_surat');
		//document.getElementById('id_surat_keluar').value = id_surat_keluar;
		console.log(id_surat_keluar);
		$('#modal-tengok').modal('show');
	}

	$('#modal-hapus-draf').on('show.bs.modal', function (e) {
	    console.log(e.target);
	});

	function detail(el){
		var id_surat = el.data('id_surat');
		//var kd_jenis = el.data('kd_jenis');
		$.ajax({
				url : "<?php echo base_url('mhs_surat_keluar/cetak_surat/') ?>"+id_surat+"/json",
				type: "GET",
				beforeSend: function(){
					$("#result").html(        '<div id="separate"></div>'+'<center><img src="http://akademik.uin-suka.ac.id/asset/img/loading.gif"></center>'+'<div id="separate"></div>'+'<center><font size="2px">Harap menunggu</font></center>');
					$("html, body").animate({ scrollTop: $("#result").offset().top }, 1000);
				},
				// data    : {id_surat: id_surat, kd_jenis: kd_jenis},                
				success: function(html){                       
						$("#result").html(html);
				}
		});
	}

</script>