<div class="col-med-9">
	<div class="content-space">
		<ul id="crumbs">
			<li>
				<a title="Automasi Surat Mahasiswa" href="">Surat Keluar</a>
			</li>
			<li>
				<a title="Automasi Surat Mahasiswa" href="">Monitor</a>
			</li>
		</ul><br/>
		<h2 class="h-border">Monitor Pengajuan Surat</h2>
		<div class="row">
			<div class="col-md-12">
			<!-- 3. mulai div Cek Terbitan Surat TTE -->			
			<div class="box">
				<div class="box-header">
					<div class="box-title">Pengajuan Surat Bertandatangan Elektronik</div>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse">
							<i class="fa fa-minus"></i>
						</button>
					</div>
				</div>
				<div class="box-body">
					<?php
					if (empty($detail_surats_termonitor)) {
						echo '<h4>TIDAK ADA SURAT YANG SEDANG DIMONITOR</h4>';
					} else {
						foreach ($detail_surats_termonitor as $k=>$v) {							
							$htm = '<div class="bs-callout">
							 <div class="alert alert-warning">
							 <h4>Selamat! Permohonan anda telah disetujui penandatangan. <br>Klik tombol cetak untuk menerbitkan surat</h4>
							 </div>
							 <table class="table table-striped"><tbody>							 
							';
							$htm .= '<tr><td>Perihal</td><td>: '.$v["PERIHAL"].'</td></tr>';
							$htm .= '<tr><td>Penandatangan</td><td>: '.$v["NM_PSD"].'</td></tr>';
							$htm .= '<tr><td>Waktu Pembuatan</td><td>: '.$v["WAKTU_SIMPAN"].'</td></tr>';
							$htm .= '</tbody></table>
							<table><tr><td align="R"><button class="btn btn-inverse btn-medium btn-uin">Cetak</button></td></tr></table>
							</div>';
							echo $htm;
						}
					}
					?>
				</div>
			</div>
			<?php
			// if (!empty($_view_notifikasi_tte)){
			// 	echo $_view_notifikasi_tte;
			// }

			// echo file_get_contents(
			// 	site_url('mhs_surat_keluar/monitoring/monitor_penandatanganan/5a9f676e96248p')
			// );
			?>
			<!-- /3. akhiri div Cek Terbitan Surat TTE -->
			</div>
		</div>

	</div>
</div>	