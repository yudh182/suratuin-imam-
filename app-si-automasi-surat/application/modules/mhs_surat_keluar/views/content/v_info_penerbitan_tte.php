<div id="result-mohon-terbitkan-tte" class="widget-container">
	<div class="bs-callout">
		<div class="inner">				         
			<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">x</button>
				<h3>Terkirim</h3>
				<a data-toggle="collapse" data-target="#demo">lihat detail penerbitan</a>
				<div id="demo" class="collapse">
					<?php
					$dp = '<ul>';
					foreach ($success as $k=>$v) {
						$dp .= '<li>'.$v.'</li>';
					}
					$dp .= '</ul>';
					echo $dp;
					?>
				</div>
				<p>
					Berhasil mengirim permintaan penandatanganan elektronik via email ke <?php echo $notif_email['penandatangan']['alamat_email']; ?> Permohonan anda telah dimasukkan ke daftar monitor, mohon tunggu sampai penandatangan memberikan tindakan (menyetujui memberi tanda tangan atau menolak).
				</p>
			</div>          	
			<table class="table table-striped">
				<tbody>					
					<tr>
						<td>Waktu Pembuatan</td>
						<td><?php echo $surat_keluar['umum']['WAKTU_SIMPAN']; ?></td>
					</tr>
					<tr>
						<td>Penandatangan Surat</td>
						<td>
							<?php echo $notif_email['penandatangan']['jabatan'] . " <br>[" . $notif_email['penandatangan']['nama'] . " (".$notif_email['penandatangan']['nip'] .")]"; ?>
						</td>
					</tr>
					<tr>
						<td>Jenis Surat</td>
						<td>
							<?php echo $detail_surat_automasi['NM_JENIS_SAKAD']; ?>
						</td>
					</tr>
				</tbody>
			</table>

		</div>			                    			
	</div>	
		<div class="bs-callout bs-callout-warning" style="margin-bottom:5px">
			<h5><b>Langkah berikutnya</b></h5>
			<ul>
				<li><b>Monitor</b> : Pantau status penandatanganan surat pada halaman utama sistem, sistem akan menampilkan pemberitahuan saat surat diberi tindakan (disetujui atau ditolak)
				</li>
				<li><b>Terbit</b> : Apabila permohonan penandatanganan disetujui, maka sistem akan menerbitkan surat, sehingga tombol cetak akan tersedia nuntuk anda</li>
				<li><b>Cetak Surat :</b> <i>Anda juga dapat memintakan cap dengan datang ke loket TU fakultas apabila membutuhkan (opsional)</i></li>
				<li>Pergunakan surat sebagaimana mestinya dan bertanggung jawab</li>
			</ul>				
		</div>
		<a name="btn_save_skr_bernomor" id="btn_save_skr_bernomor" style="margin: 10px 0px" class="btn btn-inverse btn-uin" href="<?php echo site_url('mhs_surat_keluar/monitoring'); ?>">Buka halaman monitor pengajuan surat</a>
	</div>
	<script type="text/javascript">

	</script>