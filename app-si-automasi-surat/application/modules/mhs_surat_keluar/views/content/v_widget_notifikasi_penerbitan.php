<?php 
/**
* View Widget Cek Penerbitan Surat Bertandatangan Elektronik
*/
$terbit_belum = true;
if ($terbit_belum) :?>
	<div class="row">
		<div class="col-md-12">
			<div class="box box-border-left box-border-right">
				<div class="box-header">
					<h3 class="box-title">Monitor Permohonanan Surat TTD Elektronik</h3>
					<div class="box-tools pull-right">
						<i class="fa fa-bell"></i>
						 <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					</div>
				</div>
				<div class="box-body" style="overflow: hidden; display: block;">
				    <!-- <div id="alur-cetak-surat">	
						<ol id="tracker-srtakd" class="progtrckr" data-progtrckr-steps="4">
							<li class="progtrckr-done number1">Simpan Sesi Form</li><li class="progtrckr-done number2">Permohonan PSD Digital</li><li class="progtrckr-onprogress number3">Menunggu Persetujuan</li><li class="progtrckr-todo number4">Cetak</li>
						</ol>
					</div>
					<table>
						<tbody>
							<tr>
								<td>Surat Keterangan Habis Teori</td>
							</tr>
							<tr>
								<td>
									<time>Dicetak pada dd/mm/YYYY h:i:s</time>			
								</td>
							</tr>
						</tbody>
					</table> -->
					<?php  if (!empty($monitor_ttde)) : 						
	                    $status_simpan = $monitor_ttde['KD_STATUS_SIMPAN'];
	                    if ($status_simpan == 3 && !empty($monitor_ttde['TGL_PERSETUJUAN'])){
	                    	$label_status = '<span class="label label-success">Telah Ditandatangani</span>';
	                    	
	                    	$form_aksi = form_open('cetak/terbitkan_surat_ttde', 'class="form-cetak" id="form-cetak-ttd"');
							$form_aksi .= '<button class="btn btn-default">Cetak</button>';
							$form_aksi .= form_close();
	                    } elseif ($status_simpan == 0 && !empty($monitor_ttde['TGL_DITOLAK'])){
	                    	$label_status = '<span class="label label-danger">Ditolak</span>';

	                    	$form_aksi = form_open('cetak/berhenti_monitor', 'class="form-cetak" id="form-cetak-ttd"');
							$form_aksi .= '<button class="btn btn-default">Lihat Histori dan Tutup</button>';
							$form_aksi .= form_close();
	                    } else {
	                    	$label_status = '<span class="label label-default">Menunggu tindakan...</span>';

	                    	$form_aksi = form_open('cetak/berhenti_monitor', 'class="form-cetak" id="form-cetak-ttd"');
							$form_aksi .= '<button class="btn btn-default">Kirim email ulang <span class="fa fa-send"></fa></button>';
							$form_aksi .= form_close();
	                    }				        
						?>
						<div class="table-responsive">
                			<table class="table no-margin">
                				<thead>
                					<tr>
					                    <th>Nomor Surat</th>
					                    <th>Perihal Surat</th>
					                    <th>Tanggal Surat</th>
					                    <th>Penandatangan</th>
					                    <th>Status</th>
					                </tr>
                  				</thead>
                  				<tbody>
				                <tr>
				                    <td><?= $monitor_ttde['NO_SURAT']; ?></td>
				                    <td><?= $monitor_ttde['PERIHAL']; ?></td>
				                    <td><?= $monitor_ttde['TGL_SURAT']; ?></td>
				                    <td><?= $monitor_ttde['NM_PSD']; ?></td></td>
				                    <td><?= $label_status; ?></td>
								</tr>
								</tbody>
							</table>
						</div>

						
					
					<?php 
					echo $form_aksi;
					endif; ?>

				</div>
			</div>
		</div>
	</div>
<?php endif; ?>