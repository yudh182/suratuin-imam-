<?php
/**
* Laman Buat Surat meliputi : 1. pilih surat 2. verifikasi 3. isi form 4. cetak
*/
?>

<div class="col-md-9">
<div class="content-space">
	<ul id="crumbs">
			<li>
				<a title="Halaman Utama" href="#">Automasi Cetak Surat</a>
			</li>
			<li>
				<a title="Halaman Utama" href="#"><?= ucwords(strtolower($jenis_sakad['NM_JENIS_SAKAD'])); ?></a>
			</li>			
	</ul>
	<h2 class="h-border">Buat <?= $jenis_sakad['NM_JENIS_SAKAD']; ?></h2>

<?php 
if (isset($_widget_sesi)) {
		echo $_widget_sesi; 		
} else { #tampilkan hasil verifikasi dan form?>

	<!-- 1. tampilkan hasil verifikasi -->
	<div class="box border-left border-right">
		<div class="box-header">
			<h3 class="box-title">Verifikasi</h3>
			<div class="box-tools pull-right">
				<?php if (isset($hasil_verifikasi['hasil_cek_mhs']['num_errors'])) {
					if ($hasil_verifikasi['hasil_cek_mhs']['num_errors'] > 0 ){
						echo '<span class="label label-danger">Tidak Lolos Verifikasi</span>';
					} else {
						echo '<span class="label label-success">Lolos Verifikasi</span>';
					}
					?>				
				<?php } else {
					echo '<span class="label label-default">Belum Diverifikasi</span>';
				} ?>
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
		</div>

		<div class="box-body">			
			<?php if ($hasil_verifikasi['hasil_cek_mhs']['num_errors'] > 0) : ?>
			<div class='bs-callout bs-callout-error'>
				Mohon maaf <b>Sdr. <?php echo $this->session->userdata('nama'); ?></b>, 
				Anda tidak dapat membuat surat ini karena tidak memenuhi semua persyaratan berikut ini  :
			</div>
			<?php endif; ?>

			<?php 
			//echo print_r($hasil_verifikasi);
			generate_tabel_hasil_cek($hasil_verifikasi['hasil_cek_mhs']['output']);
			generate_keterangan();
			?>	
		</div>
		<div class="overlay" style="visibility: visible;">
		  <i class="fa fa-refresh fa-spin"></i>
		</div>
	</div>

<?php if (isset($_view_form)) : ?>
	<!-- 2. tampilkan form -->
	<div class="box border-left border-right">
		<div class="box-header">
			<h3>Isi Form dan Cetak</h3>
			<div class="box-tools pull-right">
				<span class="label label-warning" id="label-state-isi-cetak" style="display: none;">sedang mengisi form</span>
                <!-- Trigger the modal with a button -->
				<button type="button" class="btn btn-box-tool" data-toggle="modal" data-target="#modal-debug-verifikasi"><i class="fa fa-bug"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
		</div>
		<div class="box-body">			
			<?php echo $_view_form; ?>
			<div id="result-aksi"></div>
			<div id="next-content"></div>
			<div id="loading1" style="display: none;">
				<center><img src="http://surat.uin-suka.ac.id/asset/img/ajax-loading.gif"><br />Harap menunggu<br>Sedang menyimpan sesi surat</center>
			</div>								
		</div>
		<!--<div class="box-footer">
			<span class="pull-left"></span>
			<span class="pull-right">
				<button class="btn btn-inverse btn-small btn-uin">Simpan dan Lanjutkan</button>
			</span>
		</div>-->
		<div class="overlay" style="visibility: visible;">
		  <i class="fa fa-refresh fa-spin"></i>
		</div>
	</div>
<?php endif; ?> 
<?php } ?>
</div>
</div>


<!-- mulai konten sembunyi -->
<!-- Modal -->
<div id="modal-debug-verifikasi" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">debug data verifikasi</h4>
      </div>
      <div class="modal-body">
        <div class="row">
			<div class="well">
				<pre>
					<?php 				
						echo json_encode($this->session->userdata('pre_isi_mhs'));				
						echo json_encode($this->session->userdata('pre_isi_surat'));
					?>
				</pre>
			</div>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">tutup</button>
      </div>
    </div>

  </div>
</div>
<!-- akhir konten sembunyi -->
<script type="text/javascript">
	$("form" ).on( "submit", function( event ) {
		event.preventDefault();
	});

	$("form" ).on( "focusin", function( e ) {
		$('#label-state-isi-cetak').removeClass('badge-success').addClass('badge-warning');		
		$("#label-state-isi-cetak").show();		
		$("#label-state-isi-cetak").text('pengisian form..');
	});


	$(document).ready(function() {

		$('form#form_isi_surat').submit(function(e){
			var me = $(this);
			//alert('Action URL : '+me.attr('action')+', DATA Serialize :'+me.serialize());
			
			var request = $.ajax({
				url: me.attr('action'),
				type: 'POST',
				cache: false,
				data: me.serialize(),
				//dataType: 'html',//dataType: 'json', //format data balikan			
				beforeSend: function() {
				   $('#btn_simpan_sesi').text('proses menyimpan...');	
				   $('#loading1').show();
				   setTimeout(function () {
				        $("#loading1").hide();
				        //form.submit();
				   }, 3000); // in milliseconds
				 }
			});

			request.done(function( msg, textStatus, jqXHR) {
				//var parsed_html =  $.parseHTML(msg);					
				$('[name="id_sesi_buat"]').val(jqXHR.getResponseHeader('X-ID-SESI'));
				$('#next-content').html(msg);
				$('#btn_simpan_sesi').text('perbarui');

				$('.form-group').removeClass('has-error')
								.removeClass('has-success');
				$('.text-danger').remove();
				
				$("#label-state-isi-cetak").text('sesi form tersimpan')
				$('#label-state-isi-cetak').removeClass('badge-warning').addClass('badge-success');	
				$('#loading1').hide();
			});

			request.fail(function( jqXHR, textStatus ) {
				console.log(jqXHR);				
				$('#loading1').hide();


				if (jqXHR.status==422) { //unprocessable (form input belum diisi dengan benar)					
					// if ($('[name=id_sesi_buat]') !== 'new'){
					// 	alert("tak boleh kosong");
					// } 
					console.log($('[name="id_sesi_buat"]').val());
					//$('#result-aksi').html(jqXHR.responseText);					
					 				
					$.each(jqXHR.responseJSON.messages, function(key, value) {
						var element = $('#' + key);
						
						element.closest('div.form-group')
							.removeClass('has-error')
							.addClass(value.length > 0 ? 'has-error' : 'has-success')
							.find('.text-danger').remove();
						
						element.after(value);						
					});
					$('#btn_simpan_sesi').text('simpan');

				} else if (jqXHR.status==501){ //Not Implemented (internal server error)
					$('#result-aksi').html(jqXHR.responseText);
				} else {
					$('#next-content').html(jqXHR.responseText);
				}
			});
		});

		$('#btn_cek_passing_data').click(function(e){
			var me = $('form#form_isi_surat');
			//alert('Action URL : '+me.attr('action')+', DATA Serialize :'+me.serialize());
			
			var request = $.ajax({
				url: "<?php echo base_url('pgw/test_automasi/cek_passing_data'); ?>",
				type: 'POST',
				cache: false,
				data: me.serialize(),
				dataType: 'html',
				//dataType: 'json', //format data balikan
				beforeSend: function() {
				   $('#btn_simpan_sesi').text('proses menyimpan...');	
				   $('#loading1').show();
				   setTimeout(function () {
				        $("#loading1").hide();
				        //form.submit();
				   }, 3000); // in milliseconds
				 }
			});

			request.done(function( msg, textStatus, jqXHR) {				
				$('#next-content').html(msg);				
				$('#loading1').hide();
			});

			request.fail(function( jqXHR, textStatus ) {
				console.log(jqXHR);				
				$('#loading1').hide();


				if (jqXHR.status==422) { //unprocessable
					$('#next-content').html(jqXHR.responseText);
					$('#btn_simpan_sesi').text('simpan');

				} else if (jqXHR.status==501){ //Not Implemented (internal server error)
					$('#result-aksi').html(jqXHR.responseText);
				}
			});
		});
	});

	//seluruh laman berhasil dimuat
	$(window).load(function(){
		$('.overlay').attr("style", "visibility: hidden");
	});
</script>

<script type="text/javascript">
		$("form" ).on( "submit", function( event ) {
			event.preventDefault();
		});
		$(document).ready(function() {
			var current_url = "<?= current_url(); ?>";
			$('#btn_hapus_sesi').click(function(e){					
				var attr_id = $(this).data("id");
				var txt = $(this).text();
				//alert(txt);
				var r = confirm("Press a button!");
				if (r == true) {
				    var request = $.ajax({
					url: "<?= site_url('pgw/automasi_surat_mhs/aksi_hapus_sesi'); ?>",
					type: 'POST',
					cache: false,
					data: { id_sesi: attr_id},
					dataType: 'html', //format data balikan
					beforeSend: function() {
					   $(e.target).text('proses menghapus...');
					   //$('#loading1').show();
					 }
				});
				request.done(function( msg ) {
					//$(e.target).remove();
					$(e.target).closest('tr').remove();

					if ($( "table.sesi-eksis" ).has( "tr" ).length){
						alert('masih ada sesi lainnya');
					} else {
						location.reload(true);
					}

					console.log(msg);		
					alert(msg);
					//$('#loading1').hide();
					//$('#btn_simpan_sesi').text('simpan atau perbarui');

				});

				request.fail(function( jqXHR, textStatus ) {
					console.log(jqXHR);				
					//$('#loading1').hide();

					if (jqXHR.status==422) { //unprocessable
						//$('#result-aksi').html(jqXHR.responseText);
						//$('#btn_simpan_sesi').text('simpan atau perbarui');
						alert(jqXHR.responseText);
					} else if (jqXHR.status==501){ //Not Implemented (internal server error)
						//$('#result-aksi').html(jqXHR.responseText);
						alert(jqXHR.responseText);
					} else {
						alert(jqXHR.responseText);
					}
					//$('#result-aksi').html(jqXHR.text);
				 	//alert( "Request failed: " + textStatus );
				});			
				} else {
				    alert("Batal menghapus!");
				}								
			});					
		});
</script>	