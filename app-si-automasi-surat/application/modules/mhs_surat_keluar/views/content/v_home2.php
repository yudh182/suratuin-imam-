<div class="col-med-9">
	<div class="content-space">
		<ul id="crumbs">
			<li>
				<a title="Automasi Surat Mahasiswa" href="">Automasi Surat Keluar</a>
			</li>
		</ul><br/>

		<div class="home-container" style="margin: 10px 0 150px 0;">
			<h2 class="h-border">Modul Automasi Surat Keluar</h2>

			<div class="row">
				<div class="col-md-12">
					<div class="bs-callout bs-callout-info">
						<h3>Buat surat-surat yang anda perlukan untuk keperluan internal di kampus ataupun eksternal disini</h2>
					</div>
				</div>
			</div>			

			<!-- 2. mulai div Cek Sesi -->
			<div class="row">
				<?php 
				$cek_sesi = $this->session->userdata('sesi_sakad');
				if (!empty($cek_sesi)) : ?>
				<div class="col-md-12">
					<div class="box box-warning">
						<div class="box-header">
							<h3 class="box-title">Sesi Tersimpan <i class="fa fa-clipboard-list"></i></h3>
						</div>
						<div class="box-body">
							<table class="table no-margin">
								<?php 
								$no = 1;
								foreach ($cek_sesi as $key => $val) : ?>
								<tr>
									<td><?= $no++ ?></td>
									<td><?= $val['PERIHAL']; ?></td>
									<td><?= $val['WAKTU_SIMPAN_AWAL']; ?></td>
									<td>
										<button class="btn btn-small btn-default btn_lanjut_sesi" data-id="<?= $val['ID_SESI'] ?>">lanjutkan</button>
										<button class="btn btn-small btn-danger btn_hapus_sesi" data-id="<?= $val['ID_SESI']; ?>" type="button">hapus <span class="fa fa-remove"></span></button>
									</td>											
								</tr>
								<?php endforeach;?>
							</table>

						</div>
				</div>
				<?php endif; ?>
			</div>
			<!-- /2. akhiri div Cek Sesi -->

			<!-- 3. mulai div Cek Terbitan Surat TTE -->
			<?php
			if (!empty($_view_notifikasi_tte)){
				echo $_view_notifikasi_tte;
			}

			echo file_get_contents(
				site_url('mhs_surat_keluar/monitoring/monitor_penandatanganan/5a9f676e96248p')
			);
			?>

			<!-- /3. akhiri div Cek Terbitan Surat TTE -->



				
		</div>

			<!-- <hr>
			<div class="row">
				<div class="col-md-4">
					<div class="small-box bg-emas">
			            <div class="inner">
			            	<h3>0</h3>
			            	<h4>Riwayat Cetak Surat Bernomor</h4>
			              	<p>Surat Keterangan Fakultas</p>
			            </div>			            
			            <a href="<?= site_url('modul_surat_akademik/admin_master_surat_akademik/sub_jenis_surat'); ?>" class="small-box-footer">
			              lihat <i class="fa fa-archive"></i>
			            </a>
			        </div>
				</div>
				<div class="col-md-4">
					<div class="small-box bg-emas">
			            <div class="inner">
			            	<h3>0</h3>
			            	<h4>Riwayat Cetak Surat Bertandatangan Digital</h4>			              
			            </div>
			            <div class="icon">
			              <i class="ion ion-pie-graph"></i>
			            </div>
			            <a href="#" class="small-box-footer">
			              lihat <i class="fa fa-archive"></i>
			            </a>
			        </div>
				</div>
				<div class="col-md-4">
					<div class="small-box bg-emas">
			            <div class="inner">
			            	<h3>0</h3>
			             	<h4>Surat Dibatalkan</h4>              
			            </div>
			            <div class="icon">
			              <i class="ion ion-pie-graph"></i>
			            </div>
			            <a href="#" class="small-box-footer">
			              buka laman <i class="fa fa-archive"></i>
			            </a>
			        </div>
				</div>							
			</div> -->
		</div>

	</div>
</div>	