<?php
/*
* Section preview dan cetak surat
*/

$curr_jenis_sakad = $this->session->userdata('pre_isi_surat');
$kd_jenis_sakad = (int)$curr_jenis_sakad['KD_JENIS_SAKAD'];

$arr_cetak_v1 = [];
$arr_cetak_v2 = [];
$arr_cetak_v3 = [
	1, 2, 3, 4, 5, 6, 9, 12,
	7, #izin penelitian
	8, #izin observasi	
	10, #studi pendahuluan
	11 #izin penelitian mata kuliah
];

if (in_array($kd_jenis_sakad, $arr_cetak_v1)){
	$submit_link = base_url().'pgw/automasi_surat_mhs/aksi_simpan_cetak_surat';
} 
elseif (in_array($kd_jenis_sakad, $arr_cetak_v2)) {
	$submit_link = base_url().'pgw/act_surat_keluar_mhs/aksi_simpan_cetak_surat_baru';
}
elseif (in_array($kd_jenis_sakad, $arr_cetak_v3)) { #MODUL MHS_SURAT_KELUAR
	$submit_link = base_url().'mhs_surat_keluar/terbitkan_surat';
}
?>

<?php echo (isset($pesan) ? '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">x</button>'.$pesan.'</div>' : ''); ?>

<div class="container-preview-cetak">	
	
	<div class="col-md-12">
		<div class="pull-left">
			<button id="btn_hapus_sesi" class="btn btn-small btn-danger" data-id="<?= $sesi_cetak['ID_SESI']; ?>" type="button">batalkan <span class="fa fa-remove"></span></button>
		</div>
		<div class="pull-right">					
			<form method="POST" action="<?= $submit_link; ?>" target="_blank" id="form_cetak_surat">		
				<input type="hidden" name="id_sesi_buat_cetak" value="<?php echo $sesi_cetak['ID_SESI']; ?>" />
				<input type="hidden" name="save_final_cetak" value="ok" />
				<input type="submit" name="btn_preview_skr_bernomor" id="btn_preview_skr_bernomor" style="margin: 10px 0px" class="btn btn-inverse btn-small btn-uin" value="Preview Surat" />	
			</form>
		</div>

	</div>
	
	<br>	

	<h3 class="h-border">Terbitkan Surat (cetak)</h3>

	<div class="col-md-12">	
		<ul class="nav nav-tabs">
		  <li class="active"><a data-toggle="tab" href="#skema1">Skema Cetak Bernomor Biasa</a></li>
		  <li><a data-toggle="tab" href="#skema2">Skema Cetak Bertandatangan Elektronik</a></li>		  
		</ul>

		<div class="tab-content">
		  <div id="skema1" class="tab-pane fade in active">
		    <div class="bs-callout bs-callout-warning" style="margin-bottom:5px">			
				<h5><b>Tentang Penerbitan Surat</b></h5>
				<ul>
					<li><span class="fa fa-save"></span> Surat terlebih dahulu akan <b>disimpan</b> dan diberi nomor</li>
					<li><span class="fa fa-send"></span> didistribusikan ke penerima (pengelola dan tembusan). 
						<!-- Berikut daftar distribusi surat ini : 
						<ol>
							<li>Mahasiswa yang bersangkutan</li>
							<li>Instansi Tujuan</li>
							<li>Dekan (Tembusan sebgaia Laporan)</li>
						</ol>		 -->
					</li>
					<li>
						<span class="fa fa-print"></span> Kemudian, secara otomatis anda akan menerima dialog untuk <b>mengunduh file surat</b>
					</li>
				</ul>
				
				</p>Print file surat dan segera ajukan ke TU terkait, untuk mendapatkan tandatangan dan cap pengesahan surat.</p>								
			</div>
			<!-- aksi terbitkan -->
			<div class="inline-buttons action-buttons">				
				<form method="POST" action="<?= $submit_link; ?>" target="_blank" id="form_cetak_surat">
				
					<input type="hidden" name="id_sesi_buat_cetak" value="<?php echo $sesi_cetak['ID_SESI']; ?>" />
					<input type="hidden" name="save_final_cetak" value="ok" />

					<input type="submit" name="btn_save_skr_bernomor" id="btn_save_skr_bernomor" style="margin: 10px 0px" class="btn-uin btn btn-inverse btn btn-medium" value="Terbitkan Surat Bernomor" />

					<!--<input type="submit" name="btn_save_skr_draf" id="btn_save_skr_draf" class="btn btn-inverse btn-medium btn-uin" value="Ajukan surat sebagai draf" title="menyimpan sbg draf dan diajukan dengan menunjukkan qrcode kepada petugas TU">-->
					<!-- <input type="button" name="btn_trigger_ttd" id="btn_trigger_ttd" style="margin: 10px 0px" class="btn-uin btn btn-inverse btn btn-medium" value="Ajukan Cetak Surat dengan TTD" title="menyimpan sbg permohonan tanda tangan digital dan hanya bisa dicetak setelah disetujui pejabat penandatangan" />
					<div class="triggered-form" style="display: none;">
						

						<textarea class="form-control" placeholder="tambahkan alasan kenapa anda memerlukan tanda tangan digital"></textarea>
						<br>
						<button type="button" name="btn_save_skr_ttd" id="btn_save_skr_ttd" class="btn btn-inverse btn-uin"> Simpan dan Kirim Permohonan TTD <i class="fa fa-save"></i></button>
					</div> -->
				</form>		
			</div>
		  </div>
		  <div id="skema2" class="tab-pane fade">
		    <div class="bs-callout bs-callout-warning">
				<h5><b>Skema Cetak Bertandatangan Elektronik</b></h5>
				<p>Inputkan alasan kenapa anda membutuhkan surat bertandatangan elektronik (<i>misal: karena akan penelitian di daerah asal, pejabat penandatangan sedang melaksanakan perjalanan dinas, dsb</i>), </p>
				<p>Sistem akan mengirimkan permohonan penandatanganan surat dinas (psd) elektronik kepada pejabat penandatangan surat ini. Apabila permohonan disetujui oleh pejabat , anda akan mendapat pemberitahuan bahwa surat sudah bisa dicetak dan langsung digunakan tanpa harus diajukan ke TU.
				<p> <i><b>Tips</b> : Pantau terus laman dashboard atau email anda untuk mengetahui perkembangan status permohonan</i></p>
			</div>
			<div class="form-group">
				<textarea name="alasan_pilih_tte" class="form-control" rows="2" placeholder="inputkan alasan pilih skema ini"></textarea>
			</div>
			<button type="button" name="request_tte" id="request_tte" style="margin: 10px 0px" class="btn btn-medium btn-inverse btn-uin" title="ajukan penandatanganan sekaligus penerbitan surat kepada pejabat">Terbitkan Surat Bertandatangan Elektronik</button>
		  </div>		  
		</div>	
		<div class="clearfix"></div>

				
	</div>
</div>

<script type="text/javascript">
	// $("form" ).on( "submit", function( event ) {
	// 	event.preventDefault();
	// });
	
	$(document).ready(function() {

		// $('#btn_trigger_ttd').click(function(event){
		// 	event.preventDefault();
		// 	$('.triggered-form').show();
		// });

		$('#request_tte').click(function(e){
			e.preventDefault();
			var me = $('form#form-cetak-surat');
			var val_id_sesi = $("input[name=id_sesi_buat_cetak]").val();
			var val_alasan = $("textarea[name=alasan_pilih_tte]").val();
			var opsi_skema = 'tte';
			
			var request = $.ajax({
				url: "<?php echo site_url('mhs_surat_keluar/ajukan_penerbitan_tte'); ?>",
				type: 'POST',
				cache: false,
				// data: me.serialize(),
				data: {skema: 'tte', id_sesi: val_id_sesi, ket: val_alasan},
				dataType: 'html',
				//dataType: 'json', //format data balikan
				beforeSend: function() {
				  // $('#btn_simpan_sesi').text('proses menyimpan...');	
				   $('#loading1').show();
				   setTimeout(function () {
				        $("#loading1").hide();
				        //form.submit();
				   }, 3000); // in milliseconds
				 }
			});

			request.done(function( msg ) {				
				//$('#btn_simpan_sesi').text('perbarui');
				$('#loading1').hide();

				$('.container-preview-cetak').html(msg);
				// $('#next-content').html(
				// 	'<iframe src="'+msg+'"></iframe>'
				// );
			});

			request.fail(function( jqXHR, textStatus ) {
				console.log(jqXHR);				
				$('#loading1').hide();


				if (jqXHR.status==422) { //unprocessable
					$('#result-aksi').html(jqXHR.responseText);
					//$('#btn_simpan_sesi').text('simpan');

				} else if (jqXHR.status==501){ //Not Implemented (internal server error)
					$('#result-aksi').html(jqXHR.responseText);
				}
			});
		});

		$('#btn_hapus_sesi').click(function(e){					
				var attr_id = $(this).data("id");
				var txt = $(this).text();
				//alert(txt);
				var r = confirm("Press a button!");
				if (r == true) {
				    var request = $.ajax({
					url: "<?= site_url('pgw/automasi_surat_mhs/aksi_hapus_sesi'); ?>",
					type: 'POST',
					cache: false,
					data: { id_sesi: attr_id},
					dataType: 'html', //format data balikan
					beforeSend: function() {
					   $(e.target).text('proses menghapus...');
					   //$('#loading1').show();
					 }
				});
				request.done(function( msg ) {
					//$(e.target).remove();
					$(e.target).closest('tr').remove();

					if ($( "table.sesi-eksis" ).has( "tr" ).length){
						alert('masih ada sesi lainnya');
					} else {
						location.reload(true);
					}

					console.log(msg);		
					alert(msg);
					//$('#loading1').hide();
					//$('#btn_simpan_sesi').text('simpan atau perbarui');

				});

				request.fail(function( jqXHR, textStatus ) {
					console.log(jqXHR);				
					//$('#loading1').hide();

					if (jqXHR.status==422) { //unprocessable
						//$('#result-aksi').html(jqXHR.responseText);
						//$('#btn_simpan_sesi').text('simpan atau perbarui');
						alert(jqXHR.responseText);
					} else if (jqXHR.status==501){ //Not Implemented (internal server error)
						//$('#result-aksi').html(jqXHR.responseText);
						alert(jqXHR.responseText);
					} else {
						alert(jqXHR.responseText);
					}
					//$('#result-aksi').html(jqXHR.text);
				 	//alert( "Request failed: " + textStatus );
				});			
				} else {
				    alert("Batal menghapus!");
				}								
			});					
	});
	/*
	
	$(document).ready(function() {
		$('form#form_cetak_surat').submit(function(e){
			var me = $(this);
			//alert('Action URL : '+me.attr('action')+', DATA Serialize :'+me.serialize());
			
			var request = $.ajax({
				url: me.attr('action'), //response sukses berupa url file pdf
				type: 'POST',
				cache: false,
				data: me.serialize(),
				dataType: 'html',
				//dataType: 'json', //format data balikan
				beforeSend: function() {
				   $('#btn_simpan_sesi').text('proses menyimpan...');	
				   $('#loading1').show();
				   setTimeout(function () {
				        $("#loading1").hide();
				        //form.submit();
				   }, 3000); // in milliseconds
				 }
			});

			request.done(function( msg ) {				
				$('#btn_simpan_sesi').text('perbarui');
				$('#loading1').hide();
				$('#next-content').html(
					'<iframe src="'+msg+'"></iframe>'
				);
			});

			request.fail(function( jqXHR, textStatus ) {
				console.log(jqXHR);				
				$('#loading1').hide();


				if (jqXHR.status==422) { //unprocessable
					$('#result-aksi').html(jqXHR.responseText);
					$('#btn_simpan_sesi').text('simpan');

				} else if (jqXHR.status==501){ //Not Implemented (internal server error)
					$('#result-aksi').html(jqXHR.responseText);
				}
			});
		});
	});
	*/
</script>