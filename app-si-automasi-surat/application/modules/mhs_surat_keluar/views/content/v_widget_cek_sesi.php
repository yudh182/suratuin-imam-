<?php 
/**
* View Widget Cek Sesi Pembuatan Surat
*/
if (!empty($cek_sesi)) : ?>
	<h3>Pengecekan sesi pembuatan</h3>
	<div class="bs-callout bs-callout-warning">
		<p>Anda masih memiliki sesi pembuatan surat untuk diselesaikan, yaitu:</p>			
		<table class="sesi-eksis table">
			<?php 
			$no = 1;
			foreach ($cek_sesi as $key => $val) : ?>
				<tr>
					<td><?= $no++ ?></td>
					<td><?= $val['PERIHAL'] ?> </td>
					<td><?= $val['WAKTU_SIMPAN_AWAL']; ?></td>
					<td>
						<span><button class="btn btn-small btn-default" data-id="<?= $val['ID_SESI'] ?>">Lanjutkan <span class="fa fa-edit"></button></span>
						<span><button id="btn_hapus_sesi" class="btn btn-small btn-danger" data-id="<?= $val['ID_SESI']; ?>" type="button">hapus <span class="fa fa-trash"></span></button></span>
						</td>
				</tr>
			<?php endforeach;?>
		</table>				
		<p>Oleh sebab itu, agar dapat membuat surat baru selesaikan sesi surat yang ada.</p>
	</div>
<?php endif; ?>