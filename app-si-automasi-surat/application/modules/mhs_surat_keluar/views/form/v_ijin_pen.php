<?php
/*
* Section Isi Form Surat Ijin Penelitian
*/
$log_mhs = [
	'nim' => $this->session->userdata('username'),
	'kd_grup_tujuan' => 'MHS01',
	'nm_grup_tujuan' => 'MAHASISWA',
	'nama_lengkap' => $this->session->userdata('nama'),
];

//Select2 form data
$mhs_sl2_id= $log_mhs["nim"].'#0'.$log_mhs["kd_grup_tujuan"].'#'.$log_mhs["nm_grup_tujuan"].'#'.$log_mhs["nama_lengkap"];
$mhs_sl2_text = $log_mhs["nim"].' - '.$log_mhs["nama_lengkap"];
?>
<div class="container-isi-form col-md-12">
	<div class="alert alert-info">
		<h4>Perhatian <span class="fa fa-info-circle"></span></h4>
		<p>	  		
	  		Surat izin penelitian yang dimaksud disini adalah permohonan izin penelitian yang ditujukan langsung ke instansi tempat penelitian. Apabila diperlukan rekomendasi dari badan tingkat provinsi silakan membuat di <a href="<?= base_url('mhs_surat_keluar/buat/rek_pen'); ?>">laman</a> ini.
  		</p>
	</div>
	<form id="form_isi_surat" method="POST" action="<?php echo base_url(); ?>mhs_surat_keluar/simpan_sesi" role="form" class="form-horizontal">
			<!-- <div class="form-group">
				<label for="lingkup_penelitian" class="col-md-3 col-sm-4 control-label">Kebutuhan surat</label>
				<div class="col-md-9 col-sm-8">
					<select class=" form-control" id="lingkup_penelitian" name="opsi_ijin_penelitian" />
						<option value="" data-yth="" data-almt="">tentukan kebutuhan surat</option>
						<option value="1" data-yth="" data-almt=""><span class="fa fa-file"></span> 1. Hanya Ijin ke instansi tempat penelitian</option>
						<option value="2" data-yth="" data-almt=""><span class="fa fa-file"></span> 2. Hanya pengantar rekomendasi penelitian dalam propinsi (BAKESBANGPOL DIY)</option>
						<option value="2" data-yth="" data-almt=""><span class="fa fa-file"></span> 3. Hanya pengantar rekomendasi penelitian luar propinsi</option>
						<option value="2" data-yth="Gubernur Daerah Istimewa Yogyakarta\r\n c.q Kepala BAKESBANGLINMAS DIY" data-almt="Jl. Jenderal Sudirman No. 5 Yogyakarta, 55231"><span class="fa fa-clone"></span> 4. Ijin ke instansi tempat penelitian + disertai pengantar rekomendasi penelitian dalam propinsi</option>
						<option value="3" data-yth="Gubernur Daerah Istimewa Yogyakarta\r\n c.q Kepala Biro Administrasi Pembangunan" data-almt="Komplek Kepatihan, Danurejan, Yogyakarta - 55213"><span class="fa fa-clone"></span> 5. Ijin ke instansi tempat penelitian + Disertai pengantar rekomendasi penelitian luar propinsi</option>
						<option value="3" data-yth="Kepala Biro AAKK\r\n UIN Sunan Kalijaga" data-almt="Jln. Marsa Adisucipto Yogyakarta 55281">4. Pengantar untuk penelitian di internal universitas</option>
					</select>
				</div>
			</div>

			<hr> -->

			<!-- <div class="form-group ds-rek-pen">
				<label class="col-md-3 col-sm-4 control-label">Kepada Yth.</label>
				<div class="col-md-9 col-sm-8">
					<input type="text" name="kepada_penerima_eks" id="kepada_penerima_eks" placeholder="Masukkan Penerima di Instansi Tujuan Surat" value="" class="form-control populated" required/>
				</div>
			</div> -->
				
			<!-- <div class="form-group ds-rek-pen">
				<label class="col-md-3 col-sm-4 control-label">Alamat Tujuan</label>
				<div class="col-md-9 col-sm-8">
					<input type="text" name="alamat_tujuan" id="alamat_tujuan" placeholder="Masukkan alamat (nama jalan,daerah, kodepos)" value="" class=" form-control populated" required/>
					<br>
					<select name="kota_tujuan" id="ajax-select-kota-tujuan" class="sl2ajax"></select>
				</div>
			</div> -->
									
			<!-- <hr style="width: 100%;"> -->			
			<!-- <div class="form-group">
				<label class="col-md-3 col-sm-4 control-label">Kepada Yth. (instansi lokasi penelitian)</label>
				<div class="col-md-9 col-sm-8">
					<input type="text" name="kepada_penerima_eks2" id="kepada_penerima_eks2" placeholder="Jabatan penerima/pimpinan di tempat penelitian" value="" class="form-control populated" required/>
				</div>
			</div> -->
			<div class="form-group">
				<label class="col-md-3 col-sm-4 control-label">Instansi Tempat Penelitian</label>		
				<div class="col-md-9 col-sm-12">
					<input type="text" name="kepada_penerima_eks" id="kepada_penerima_eks" placeholder="Masukkan jabatan semisal Kepala,Direktur,CEO,dsb" value="" class="form-control populated" required/>
					<br>
					<!-- <input type="text" name="tmpt_penelitian" id="tmpt_penelitian" value="" class=" form-control" placeholder="Masukkan nama instansi/lembaga/badan usaha" required /> -->					 
					<input type="text" name="instansi_tujuan" id="instansi_tujuan" placeholder="Masukkan Nama Instansi Tujuan" value="" class="form-control" required/>
					<br>
					<input type="text" name="tempat_tujuan" id="tempat_tujuan" placeholder="Masukkan alamat (nama jalan,daerah, kodepos)" value="" class=" form-control populated" required/>

				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 col-sm-4 control-label">Judul Penelitian</label>		
				<div class="col-md-9 col-sm-8">
					<!--<input type="text" name="judul_penelitian" id="judul_penelitian" placeholder="Masukkan Judul Penelitian Anda" value="" class=" form-control" />-->
					<textarea name="judul_penelitian" id="judul_penelitian" class="form-control"></textarea>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 col-sm-4 control-label">Metode Pengumpulan Data</label>
				<div class="col-md-9 col-sm-8">
					<input type="text" name="mtd_penelitian" id="mtd_penelitian" placeholder="contoh: wawancara, kuesioner, observasi, data sampling, dsb" value="" class=" form-control" />
				</div>
			</div>			

			<div class="form-group">
				<label class="col-md-3 col-sm-4 control-label">Tanggal Mulai</label>
				<div class="col-md-9 col-sm-8">
					<input type="date" name="tgl_mulai" id="tgl_mulai" value="" class="form-control" required />
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 col-sm-4 control-label">Tanggal Berakhir</label>
				<div class="col-md-9 col-sm-8">
					<input type="date" name="tgl_berakhir" id="tgl_berakhir" value="" class="form-control" required />
				</div>			
			</div>

			<!-- <div class="form-group">
				<label class="col-md-3 col-sm-4 control-label">Daftar Mahasiswa Mengikuti</label>
				<div class="col-md-9 col-sm-8">					
	  				<select name="dftr-mhs[]" id="daftar-mhs" class="form-control" multiple>	  					
	    			</select>			
				</div>			
			</div> -->
				
			<div class="form-group">
				<div class="col-md-3 col-sm-4">
					<input type="hidden" name="id_sesi_buat" value="new" />
					<input type="hidden" name="save_sesi" value="ok" />	
				</div>
				<div class="col-md-9 col-sm-8">
					<input type="submit" name="btn_submit_sesi" style="margin: 10px 0px" class="btn btn-inverse btn-medium btn-uin" id="btn_submit_sesi" value="Simpan form" />
				</div>
			</div>
		</form>	
</div>
<div class="clearfix"></div>

<script type="text/javascript">
	$("form" ).on( "submit", function( event ) {
		event.preventDefault();
	});
	$(document).ready(function() {		
		// $('.ds-rek-pen').hide();
		// $('#lingkup_penelitian').change(function(){
		// 	if ($(this).val() == "1") {
  //               //$(".show-later").show();
  //               $(".ds-rek-pen").hide();
  //           }else{
  //               $(".ds-rek-pen").show();
  //           } 

		// 	//$("form").find('.populated').val('');			
		// 	$('#kepada_penerima_eks').val($('#lingkup_penelitian').find(':selected').data('yth'));
		// 	$('#alamat_tujuan').val($('#lingkup_penelitian').find(':selected').data('almt'));			
		// });	

		// $('#ajax-select-kota-tujuan').select2({
		// 	placeholder: 'Cari kota/kabupaten tujuan surat',
		// 	minimumInputLength: 3,
		// 	ajax: {
		// 	    //url: "http://exp.uin-suka.ac.id",
		// 	    delay: 250, // wait 250 milliseconds before triggering the request
		// 	    dataType: 'json',
		// 	    //minimumInputLength: 3,		    
		// 	    url: "<?php //echo base_url('pgw/external/akademik/auto_kabupaten_sl2');?>",		   
		// 	    data: function (params) {
		// 	      var query = {
		// 	        search: params.term
		// 	        //type: 'public'
		// 	      }
		// 	      // Query parameters will be ?search=[term]&type=public
		// 	      return query;
		// 	    },    
		// 	 //    processResults: function (data) {
		// 		//     return {
		// 		//       results: data.items
		// 		//     };
		// 		// },
		// 		cache: true	    		   
		//   }
		// });				
		// set_default_value_select2('{"id":"34043", "text": "KAB. SLEMAN"}', '#ajax-select-kota-tujuan');		

		// $('#daftar-mhs').select2({			
		// 	tags: true, // enable tagging
	 //        //tokenSeparators: ['<$>',' '],
		// 	minimumInputLength: 3,
		// 	maximumSelectionLength: 10,
	 //        allowClear: true,
	 //        placeholder: 'ketikkan peserta mahasiswa lainnya',
	 //        ajax: {
	 //          url: "<?php echo base_url('pgw/external/akademik/auto_mahasiswa_sl2'); ?>",
	 //          dataType: 'json',
	 //          delay: 250,
	 //          data: function (params) {
	 //                return {q: params.term}
	 //          },
	 //          processResults: function (data) {
	 //            return {
	 //              results: data
	 //            };
	 //          },
	 //          cache: true
	 //        }
	 //    });	
	 //    set_tags_select2(
		// 	//'{"11650021#0#MHS01#MAHASISWA#MUKHLAS IMAM M" : "11650021 - MUKHLAS IMAM M"}', 
		// 	'{"<?= $mhs_sl2_id; ?>" : "<?= $mhs_sl2_text; ?>"}',
		// 	'#daftar-mhs'
		// );


		
	});
</script>	
			