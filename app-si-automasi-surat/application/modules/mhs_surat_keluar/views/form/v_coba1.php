<?php
/*
* Section Form Coba
*/

$log_mhs = [
	'nim' => $this->session->userdata('username'),
	'kd_grup_tujuan' => 'MHS01',
	'nm_grup_tujuan' => 'MAHASISWA',
	'nama_lengkap' => $this->session->userdata('nama'),
];

//Select2 form data
$mhs_sl2_id= $log_mhs["nim"].'#0'.$log_mhs["kd_grup_tujuan"].'#'.$log_mhs["nm_grup_tujuan"].'#'.$log_mhs["nama_lengkap"];
$mhs_sl2_text = $log_mhs["nim"].' - '.$log_mhs["nama_lengkap"];
?>
<div class="col-md-9 col-sm-12">
<div class="content-space row">
	<ul id="crumbs">
		<li>
			<a title="Halaman Utama" href="#">Coba</a>
		</li>
		<li>
			<a title="Halaman Utama" href="#"><?= $title; ?></a>
		</li>			
	</ul>
	
	<h3><?= $title; ?></h3>

	<div class="container-isi-form col-md-12">
		<div class="alert alert-info">
	  		<strong>Info : </strong> Pembuatan surat ini akan menghasilkan 2 lembar surat yang terdiri :
	  		<ol>
	  			<li>Pengantar ke Instansi berwenang di tingkat kabupaten/Kota/Provinsi untuk mendapatkan surat rekomendasi penelitian</li>
	  			<li>permohonan izin penelitian ke instansi tujuan yang menyediakan obyek bagi penelitian</li>
	  		</ol>  	
		</div>
		<form id="form_isi_surat_coba" method="POST" action="<?php echo base_url(); ?>mhs_surat_keluar/coba/action" role="form" class="form-horizontal">
			<div class="form-group">
				<label for="lingkup_penelitian" class="col-md-3 col-sm-4 control-label">Kebutuhan surat</label>
				<div class="col-md-9 col-sm-8">
					<select class=" form-control" id="lingkup_penelitian" />
						<option value="" data-yth="" data-almt="">--tentukan kebutuhan surat--</option>
						<option value="4" data-yth="" data-almt="">1. Hanya Pengantar ke instansi obyek penelitian</option>
						<option value="1" data-yth="Gubernur Daerah Istimewa Yogyakarta\r\n c.q Kepala BAKESBANGLINMAS DIY" data-almt="Jl. Jenderal Sudirman No. 5 Yogyakarta, 55231">2. Disertai pengantar rekomendasi penelitian dalam propinsi</option>
						<option value="2" data-yth="Gubernur Daerah Istimewa Yogyakarta\r\n c.q Kepala Biro Administrasi Pembangunan" data-almt="Komplek Kepatihan, Danurejan, Yogyakarta - 55213">3. Disertai pengantar rekomendasi penelitian luar propinsi</option>
						<!-- <option value="3" data-yth="Kepala Biro AAKK\r\n UIN Sunan Kalijaga" data-almt="Jln. Marsa Adisucipto Yogyakarta 55281">4. Pengantar untuk penelitian di internal universitas</option> -->
					</select>
				</div>
			</div>

			<hr>

			<div class="form-group show-later">
				<label class="col-md-3 col-sm-4 control-label">Kepada Yth. (pemberi rekomendasi penelitian)</label>
				<div class="col-md-9 col-sm-8">
					<input type="text" name="kepada_penerima_eks" id="kepada_penerima_eks" placeholder="Masukkan Penerima di Instansi Tujuan Surat" value="" class="form-control populated" required/>
				</div>
			</div>			
				
			<div class="form-group show-later">
				<label class="col-md-3 col-sm-4 control-label">Alamat Tujuan</label>
				<div class="col-md-9 col-sm-8">
					<input type="text" name="alamat_tujuan" id="alamat_tujuan" placeholder="Masukkan alamat (nama jalan,daerah, kodepos)" value="" class=" form-control populated" required/>
					<br>
					<select name="kota_tujuan" id="ajax-select-kota-tujuan" class="sl2ajax"></select>
				</div>
			</div>
						
			<div class="row col-md-12">
				<hr>
			</div>
			<div class="form-group show-later">
				<label class="col-md-3 col-sm-12 control-label">Kepada Yth. (instansi lokasi penelitian)</label>
				<div class="col-md-9 col-sm-12">
					<input type="text" name="kepada_penerima_eks2" id="kepada_penerima_eks2" placeholder="Jabatan penerima/pimpinan di tempat penelitian" value="" class="form-control populated" required/>
				</div>
			</div>
			<div class="form-group show-later">
				<label class="col-md-3 col-sm-12 control-label">Instansi Tempat Penelitian</label>		
				<div class="col-md-9 col-sm-12">
					<input type="text" name="tmpt_penelitian" id="tmpt_penelitian" value="" class=" form-control" placeholder="Masukkan nama instansi/lembaga/badan usaha" required /
					<input type="text" name="alamat_tujuan" id="alamat_tujuan" placeholder="Masukkan alamat (nama jalan,daerah, kodepos)" value="" class=" form-control populated" required/>

				</div>
			</div>

			<div class="form-group show-later">
				<label class="col-md-3 col-sm-12 control-label">Judul Penelitian</label>		
				<div class="col-md-9 col-sm-12">
					<!--<input type="text" name="judul_penelitian" id="judul_penelitian" placeholder="Masukkan Judul Penelitian Anda" value="" class=" form-control" />-->
					<textarea name="judul_penelitian" id="judul_penelitian" class="form-control"></textarea>
				</div>
			</div>

			<div class="form-group show-later">
				<label class="col-md-3 col-sm-4 control-label">Metode Pengumpulan Data</label>
				<div class="col-md-9 col-sm-8">
					<input type="text" name="mtd_penelitian" id="mtd_penelitian" placeholder="contoh: wawancara, kuesioner, observasi, data sampling, dsb" value="" class=" form-control" />
				</div>
			</div>			

			<div class="form-group show-later">
				<label class="col-md-3 col-sm-4 control-label">Tanggal Mulai</label>
				<div class="col-md-9 col-sm-8">
					<input type="date" name="tgl_mulai" id="tgl_mulai" value="<?= date('Y-m-d'); ?>" class="form-control" required />
				</div>
			</div>

			<div class="form-group show-later">
				<label class="col-md-3 col-sm-4 control-label">Tanggal Berakhir</label>
				<div class="col-md-9 col-sm-8">
					<input type="date" name="tgl_berakhir" id="tgl_berakhir" value="" class="form-control" required />
				</div>			
			</div>

			<div class="form-group show-later">
				<label class="col-md-3 col-sm-4 control-label">Daftar Mahasiswa Mengikuti</label>
				<div class="col-md-9 col-sm-8">					
	  				<select name="dftr-mhs[]" id="daftar-mhs" class="form-control" multiple>	  					
	    			</select>			
				</div>			
			</div>
				
			<div class="form-group">
				<div class="col-md-3 col-sm-4">
					<input type="hidden" name="id_sesi_buat" value="new" />
					<input type="hidden" name="save_sesi" value="ok" />	
				</div>
				<div class="col-md-9 col-sm-8">
					<input type="submit" name="btn_submit_sesi" style="margin: 10px 0px" class="btn btn-inverse btn-medium btn-uin" id="btn_submit_sesi" value="Simpan form" />
				</div>
			</div>
		</form>	
	</div>
	<div class="clearfix"></div>

	<div class="tampung"></div>

</div>
</div>

<script type="text/javascript">
	// $("form" ).on( "submit", function( event ) {
	// 	event.preventDefault();
	// });
	$(document).ready(function() {
		//$('.show-later').hide();
		$('#lingkup_penelitian').change(function(){	
			if ($(this).val() !== "") {
                //$(".show-later").show();
            }else{
                //$(".show-later").hide();
            } 
			$("form").find('.populated').val('');	
			$('#kepada_penerima_eks').val($('#lingkup_penelitian').find(':selected').data('yth'));
			$('#alamat_tujuan').val($('#lingkup_penelitian').find(':selected').data('almt'));			
		});	

		$('#ajax-select-kota-tujuan').select2({
			placeholder: 'Cari kota/kabupaten tujuan surat',
			minimumInputLength: 3,
			ajax: {
			    //url: "http://exp.uin-suka.ac.id",
			    delay: 250, // wait 250 milliseconds before triggering the request
			    dataType: 'json',
			    //minimumInputLength: 3,		    
			    url: "<?php echo base_url('pgw/external/akademik/auto_kabupaten_sl2');?>",		   
			    data: function (params) {
			      var query = {
			        search: params.term
			        //type: 'public'
			      }
			      // Query parameters will be ?search=[term]&type=public
			      return query;
			    },    
			 //    processResults: function (data) {
				//     return {
				//       results: data.items
				//     };
				// },
				cache: true	    		   
		  }
		});		
		//default value kota tujuan
		set_default_value_select2('{"id":"34043", "text": "KAB. SLEMAN"}', '#ajax-select-kota-tujuan');		

		$('#daftar-mhs').select2({			
			tags: true, // enable tagging
	        //tokenSeparators: ['<$>',' '],
			minimumInputLength: 3,
	        allowClear: true,
	        placeholder: 'ketikkan peserta mahasiswa lainnya',
	        language: "id",
	        ajax: {
	          url: "<?php echo base_url('pgw/external/akademik/auto_mahasiswa_sl2'); ?>",
	          dataType: 'json',
	          delay: 250,
	          data: function (params) {
	                return {q: params.term}
	          },
	          processResults: function (data) {
	            return {
	              results: data
	            };
	          },
	          cache: true
	        }
	    });	
	 //    set_tags_select2(
		// 	'{"<?= $mhs_sl2_id; ?>" : "<?= $mhs_sl2_text; ?>"}',
		// );
		set_tags_select2(
			//'{"11650021#0#MHS01#MAHASISWA#MUKHLAS IMAM M" : "11650021 - MUKHLAS IMAM M"}', 
			'{"<?= $mhs_sl2_id; ?>" : "<?= $mhs_sl2_text; ?>"}',
			'#daftar-mhs'
		);
	    //set_default_value_select2('{"id":"11650021#0#MHS01#MAHASISWA#MUKHLAS IMAM M", "text": "11650021 - MUKHLAS IMAM M"}', '#daftar-mhs');	    					 

	  //   $('#form_isi_surat_coba').submit(function(e){
	  //   	e.preventDefault();
	  //   	var me = $(this);
			// var request = $.ajax({
			// 	url: me.attr('action'),
			// 	type: 'POST',
			// 	cache: false,
			// 	data: me.serialize(),
			// 	//dataType: 'html',
			// 	//dataType: 'json', //format data balikan
			// 	beforeSend: function() {
			// 	   $('#btn_simpan_sesi').text('proses menyimpan...');	
			// 	   // $('#loading1').show();
			// 	   // setTimeout(function () {
			// 	   //      $("#loading1").hide();
			// 	   //      //form.submit();
			// 	   // }, 3000); // in milliseconds
			// 	 }
			// });

			// request.done(function( msg, textStatus, jqXHR) {				
			// 	$('#tampung').html(msg);
			// 	$('#btn_simpan_sesi').text('Simpan form');
			// 	// $('#loading1').hide();
			// });

			// request.fail(function( jqXHR, textStatus ) {
			// 	console.log(jqXHR);				
			// 	// $('#loading1').hide();


			// 	if (jqXHR.status==422) { //unprocessable
			// 		$('#tampung').html(jqXHR.responseText);
			// 		$('#btn_simpan_sesi').text('simpan form');

			// 	} else if (jqXHR.status==501){ //Not Implemented (internal server error)
			// 		$('#tampung').html(jqXHR.responseText);
			// 	}
			// });
	  //   });
		
		/*$(".tempat_tujuan").tokenInput("<?php echo base_url();?>pgw/external/akademik/auto_kabupaten", {
			<?php
			/*if(!empty($kota_tujuan)){ ?>
				prePopulate: [{id:'<?php echo $kota_tujuan[0]['KD_KAB'];?>', name:'<?php echo ucwords(strtolower($kota_tujuan[0]['NM_KAB1'])).". ".$kota_tujuan[0]['NM_KAB2'];?>'}],<?php
			}*/?>			
			prePopulate: [{id: 34712, name: "KODYA. YOGYAKARTA"}],
			tokenLimit:1
		});*/				
	});

	//SET NILAI DEFAULT KE SELECT2		
	function set_default_value_select2(data, element_selected)
	{
		// Fetch the preselected item, and add to the control
		var data = JSON.parse(data);
		var newOption = new Option(data.text, data.id, false, false);
		$(element_selected).append(newOption).trigger('change');

	    // manually trigger the `select2:select` event
	    $(element_selected).trigger({
	        type: 'select2:select',
	        params: {
	            data: data
	        }
	    });
	}	


	function set_tags_select2(options_data, element_selected)
	{
		var select = $(element_selected);
		var parseddata = JSON.parse(options_data);
		// var newOptions = {
  //               'red' : 'Red',
  //               'blue' : 'Blue',
  //               'green' : 'Green',
  //               'yellow' : 'Yellow'
  //           };
         var newOptions = parseddata;

		$('option', select).remove();
		$.each(newOptions, function(text, key) {
		    var option = new Option(key, text, true, true);
		    select.append($(option));
		});
	}	

	
</script>	
			