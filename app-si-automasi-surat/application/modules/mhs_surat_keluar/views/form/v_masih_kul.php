<div class="container-isi-form">
	<form method="POST" id="form_isi_surat" action="<?php echo base_url(); ?>mhs_surat_keluar/simpan_sesi" role="form" class="form-horizontal">

		<div class="form-group">
			<label for="keperluan" class="col-md-3 col-sm-4 control-label">Keperluan</label>
			<div class="col-md-9 col-sm-8">
				<input type="text" name="keperluan" id="keperluan" placeholder="contoh: permohonan tunjangan anak PNS" value="" class=" form-control" />
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-3">
				<input type="hidden" name="id_sesi_buat" value="new" />
				<input type="hidden" name="save_sesi" value="ok" />	
			</div>
			<div class="col-md-9">
				<button type="submit" style="margin: 10px 0px" class="btn btn-inverse btn-medium btn-uin" name="btn-sess" id="btn_simpan_sesi">Simpan sesi</button>				
			</div>
			
		</div>

	</form>
</div>
<div class="clearfix"></div>