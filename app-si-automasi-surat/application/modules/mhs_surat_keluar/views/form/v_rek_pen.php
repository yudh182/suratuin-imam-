<?php
/*
* Section Isi Formulir Surat Permohonan Rekomendasi Penelitian
*/
// $log_mhs = [
// 	'nim' => $this->session->userdata('username'),
// 	'kd_grup_tujuan' => 'MHS01',
// 	'nm_grup_tujuan' => 'MAHASISWA',
// 	'nama_lengkap' => $this->session->userdata('nama'),
// ];

// //Select2 form data
// $mhs_sl2_id= $log_mhs["nim"].'#0'.$log_mhs["kd_grup_tujuan"].'#'.$log_mhs["nm_grup_tujuan"].'#'.$log_mhs["nama_lengkap"];
// $mhs_sl2_text = $log_mhs["nim"].' - '.$log_mhs["nama_lengkap"];
?>
<div class="container-isi-form col-md-12">
	<div class="alert alert-info">
		<h4>Tips</h4>
		<p>
			Pastikan tujuan surat yang anda pilih dalam form sesuai dengan ketentuan prosedur permohonan rekomendasi penelitian yang saat ini sedang berlaku.
		</p>		
	</div>
	<form id="form_isi_surat" method="POST" action="<?php echo base_url(); ?>mhs_surat_keluar/simpan_sesi" role="form" class="form-horizontal">
		<h4 class="h_border">Tujuan Surat</h4>
		<div class="form-group">
			<div class="col-md-3"></div>
			<div class="col-md-9">
				<select class="select2control form-control" id="ajax-select-lembaga-tujuan">
				  <option></option>
				</select>
			</div>
		</div>
		<div class="form-group show-later ds-rek-pen">
			<label class="col-md-3 col-sm-4 control-label">Penerima Surat</label>
			<div class="col-md-9 col-sm-8">
				<input type="text" name="pejabat_tujuan_surat" id="pejabat_tujuan_surat" placeholder="Pejabat tujuan surat" value="" class="form-control populated" required/>
				<br>
				<!-- <input type="text" name="pimpinan_lembaga_tujuan_surat" id="pimpinan_lembaga_tujuan_surat" placeholder="Pimpinan" value="" class="form-control populated" required/>
				<br> -->
				<input type="text" name="pejabat_tujuan_surat_spesifik" id="pejabat_tujuan_surat_spesifik" value="" class=" form-control" placeholder="Pimpinan lembaga penerbit rekomendasi penelitian" required />
				<br>
				<input type="text" name="alamat_lembaga_tujuan_surat" id="alamat_lembaga_tujuan_surat" placeholder="Masukkan alamat (nama jalan,daerah, kodepos)" value="" class=" form-control populated" required/>
			</div>
		</div>		
			
		<!-- <div class="form-group show-later ds-rek-pen">
			<label class="col-md-3 col-sm-4 control-label">Alamat Tujuan</label>
			<div class="col-md-9 col-sm-8">
				<input type="text" name="alamat_tujuan" id="alamat_tujuan" placeholder="Masukkan alamat (nama jalan,daerah, kodepos)" value="" class=" form-control populated" required/>
				<br>
				<select name="kota_tujuan" id="ajax-select-kota-tujuan" class="sl2ajax"></select>
			</div>
		</div> -->

		<h4 class="h_border">Badan Surat</h4>
		<div class="form-group show-later">
			<label class="col-md-3 col-sm-4 control-label">Instansi Tempat Penelitian</label>		
			<div class="col-md-9 col-sm-8">
				<input type="text" name="tmpt_penelitian" id="tmpt_penelitian" value="" class=" form-control" placeholder="Masukkan nama instansi/lembaga/badan usaha" required />
				<br>
				<input type="text" name="alamat_tujuan" id="alamat_tujuan" placeholder="Masukkan alamat (nama jalan,daerah, kodepos)" value="" class=" form-control populated" required/>

			</div>
		</div>

		<div class="form-group show-later">
			<label class="col-md-3 col-sm-4 control-label">Judul Penelitian</label>		
			<div class="col-md-9 col-sm-8">
				<!--<input type="text" name="judul_penelitian" id="judul_penelitian" placeholder="Masukkan Judul Penelitian Anda" value="" class=" form-control" />-->
				<textarea name="judul_penelitian" id="judul_penelitian" class="form-control"></textarea>
			</div>
		</div>

		<div class="form-group show-later">
			<label class="col-md-3 col-sm-4 control-label">Metode Pengumpulan Data</label>
			<div class="col-md-9 col-sm-8">
				<input type="text" name="mtd_penelitian" id="mtd_penelitian" placeholder="contoh: wawancara, kuesioner, observasi, data sampling, dsb" value="" class=" form-control" />
			</div>
		</div>			

		<div class="form-group show-later">
			<label class="col-md-3 col-sm-4 control-label">Tanggal Mulai</label>
			<div class="col-md-9 col-sm-8">
				<input type="date" name="tgl_mulai" id="tgl_mulai" value="" class="form-control" required />
			</div>
		</div>

		<div class="form-group show-later">
			<label class="col-md-3 col-sm-4 control-label">Tanggal Berakhir</label>
			<div class="col-md-9 col-sm-8">
				<input type="date" name="tgl_berakhir" id="tgl_berakhir" value="" class="form-control" required />
			</div>			
		</div>

		<div class="form-group">
			<div class="col-md-3 col-sm-4">
				<input type="hidden" name="id_sesi_buat" value="new" />
				<input type="hidden" name="save_sesi" value="ok" />	
			</div>
			<div class="col-md-9 col-sm-8">
				<input type="submit" name="btn_submit_sesi" style="margin: 10px 0px" class="btn btn-inverse btn-medium btn-uin" id="btn_submit_sesi" value="Simpan form" />
			</div>
		</div>
	</form>	
</div>
<div class="clearfix"></div>

<script type="text/javascript">
	$("form" ).on( "submit", function( event ) {
		event.preventDefault();
	});
	$(document).ready(function() {	
		$(".js-example-placeholder-single").select2({
		    placeholder: "Tentukan tujuan surat",
		    allowClear: false
		});
		$('#ajax-select-lembaga-tujuan').select2({
			placeholder: 'Pilih Lembaga Tujuan (penerbit surat rekomendasi)',
			allowClear: true,
			width: "element",
			//minimumInputLength: 0,
			ajax: {			    
			    delay: 250, // wait 250 milliseconds before triggering the request
			    dataType: 'json',			    
			    url: "<?php echo base_url('service_sakad/perizinan/instansi_perizinan');?>",		   
			    data: function (params) {
			      var query = {
			        search: params.term
			        //type: 'public'
			      }
			      // Query parameters menjadi ?search=[term]&type=public
			      return query;
			    },    
			    processResults: function (data) {
				    return {
				      results: data.results,

				    };
				},

				// processResults: function (data) {
				//     return {
				//         results: $.map(data, function(obj) {
				//             return { id: obj.ID, text: obj.NM_INSTANSI };
				//         })
				//     };
				// },

				cache: true	    		   
			}
		});
		$('#ajax-select-lembaga-tujuan').on('select2:select', function (e) {
		    var data = e.params.data;
		    console.log(data);

		    $('#pejabat_tujuan_surat').val(data.arah_tj);
		    $('#pejabat_tujuan_surat_spesifik').val('Cq. '+data.pimpinan+' '+data.nama_instansi);
		    $('#alamat_lembaga_tujuan_surat').val(data.alamat);
		});
		$('#ajax-select-lembaga-tujuan').on('select2:unselect', function (e) {		    
		    $('#pejabat_tujuan_surat').val('');
		    $('#pejabat_tujuan_surat_spesifik').val('');
		    $('#alamat_lembaga_tujuan_surat').val('');
		});





		$('#ajax-select-kota-tujuan').select2({
			placeholder: 'Cari kota/kabupaten tujuan surat',
			minimumInputLength: 3,
			ajax: {			    
			    delay: 250, // wait 250 milliseconds before triggering the request
			    dataType: 'json',			    
			    url: "<?php echo base_url('pgw/external/akademik/auto_kabupaten_sl2');?>",		   
			    data: function (params) {
			      var query = {
			        search: params.term
			        //type: 'public'
			      }
			      // Query parameters menjadi ?search=[term]&type=public
			      return query;
			    },    
			 //    processResults: function (data) {
				//     return {
				//       results: data.items
				//     };
				// },
				cache: true
		  }
		});
	});
</script>	

