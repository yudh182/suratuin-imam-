<script type="text/javascript">
	var bpd = 1; //index beasiswa yg pernah diikuti
	$(function(){
		$("#addBPD").click(function(){
			row = '<tr>'+
			'<td><textarea style="width: 100%;" rows="3" name="nm_beasiswa_pd['+bpd+']" class="inp-bpd"></textarea></td>'+
			'<td><textarea style="width: 100%;" rows="3" name="ket_beasiswa_pd['+bpd+']" class="inp-bpd"></textarea></td>'+
			'<td><a class="btn btn-small btn-danger btn-act remove-btn_sp" style="" title="Hapus"> x </a></td>'+
			'</tr>';			
			$(row).insertBefore("#last_bpd");
			bpd++;
		});
	});
	$(".remove-btn_sp").on('click', function(){
		// $(this).parent().parent().remove();
		$(this).parent().remove();
	});
</script>

<div class="container-isi-form col-md-12">

	<form method="POST" id="form_isi_surat" action="<?php echo base_url(); ?>mhs_surat_keluar/simpan_sesi" role="form" class="form-horizontal">
			<div class="form-group">
				<label class="col-md-3 control-label">Keperluan</label>
				<div class="col-md-9 col-sm-12">
					<input type="text" name="keperluan" id="keperluan" placeholder="isikan keperluan atau tujuan penggunaan surat" value="" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">
					Beasiswa yang pernah diikuti (Opsional)
					<br>
					<span><i>bukan termasuk output surat namun wajib diisi untuk kepentingan administrasi</i></span>
				</label>
				<div class="col-md-9 col-sm-12">
					<!--<input type="text" name="bea" id="bea" value="" class=" form-control" />-->
					<table class="table table-bordered" width="99%">
						<tr>
							<td width="250px">Nama beasiswa</td><td width="280px">Keterangan</td><td width="30px">Aksi</td>
						</tr>
						<tr>
							<td><textarea style="width: 100%;" rows="3" name="nm_beasiswa_pd[0]" class="inp-bpd"></textarea>
							<td><textarea style="width: 100%;" rows="3" name="ket_beasiswa_pd[0]" placeholder="ex: periode beasiswa, ket. khusus, dsb" class="inp-bpd"></textarea></td>
							<td></td>
						</tr>
						<tr id="last_bpd"></tr>
						<tr><td colspan="4"><a class="btn btn-default btn-small" id="addBPD" style="margin:0 0 5px 0">Tambah</a></td></tr>
					</table>
				</div>			
			<div class="form-group">
				<div class="col-md-3 col-sm-4">
					<input type="hidden" name="id_sesi_buat" value="new" />
					<input type="hidden" name="save_sesi" value="ok" />	
				</div>
				<div class="col-md-9 col-sm-8">
					<!-- <input type="submit" name="btn_submit_sesi" style="margin: 10px 0px" class="btn btn-inverse btn-medium btn-uin" id="btn_submit_sesi" value="Simpan form" /> -->
					<button type="submit" id="btn_simpan_sesi" class="btn btn-inverse btn-small btn-uin" style="margin: 10px 0px">Simpan Sesi</button>
					<!-- <button type="button" id="btn_cek_passing_data" class="btn btn-default" style="margin: 10px 0px">Cek Formdata</button> -->
				</div>
			</div>
			
		</form>	
</div>		
<div class="clearfix"></div>

<script type="text/javascript">
	$("form" ).on( "submit", function( event ) {
		event.preventDefault();
	});	
</script>	