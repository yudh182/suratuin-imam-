<div class="container-isi-form col-md-12">
	<div class="alert alert-info">
		<p>
	  		<strong>Info <span class="fa fa-info-circle"></span></strong><br>
	  		Kelompok Kerja Praktek | PL | PKL untuk satu instansi maksimal adalah 2 orang mahasiswa, dan masing-masing wajib memilih bidang minat yang berbeda
  		</p>
	</div>

	<form id="form_isi_surat" method="POST" action="<?php echo base_url(); ?>mhs_surat_keluar/simpan_sesi" role="form" class="form-horizontal">

		<div class="form-group">
			<label class="col-md-3 control-label">Kepada Yth.</label>
			<div class="col-md-9 col-sm-12">
				<input type="text" name="kepada_penerima_eks" id="kepada_penerima_eks" placeholder="Jabatan penerima/pimpinan di tempat tujuan" value="" class="form-control" required/>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label">Nama Instansi</label>
			<div class="col-md-9 col-sm-12">
				<input type="text" name="instansi_tujuan" id="instansi_tujuan" placeholder="Masukkan Nama Instansi Tujuan" value="" class="form-control" required/>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label">Tempat Tujuan</label>
			<div class="col-md-9 col-sm-12">
				<input type="text" name="tempat_tujuan" id="tempat_tujuan" placeholder="alamat jalan kota kodepos" value="" class=" form-control" />
				<!-- <input type="text" name="kota_tujuan" id="kota_tujuan" placeholder="Masukkan nama kota/kabupaten instansi tujuan berada" value="" class=" form-control populated" /> -->
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label">Bidang Minat</label>
			<div class="col-md-9 col-sm-12">
				<input type="text" name="minat" id="minat" placeholder="Masukkan Bidang Minat" value="" class="form-control" />
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-md-3 control-label">Tanggal Mulai</label>
			<div class="col-md-9 col-sm-12">
				<input type="date" name="tgl_mulai" id="tgl_mulai" value="" class="form-control" required />
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label">Tanggal Berakhir</label>
			<div class="col-md-9 col-sm-12">
				<input type="date" name="tgl_berakhir" id="tgl_berakhir" value="" class="form-control" required />
			</div>
		</div>
		
		<div class="form-group">
			<div class="col-md-3 col-sm-4">
				<input type="hidden" name="id_sesi_buat" value="new" />
				<input type="hidden" name="save_sesi" value="ok" />	
			</div>
			<div class="col-md-9 col-sm-8">
				<input type="submit" name="btn_submit_sesi" style="margin: 10px 0px" class="btn btn-inverse btn-medium btn-uin" id="btn_submit_sesi" value="Simpan form" />
			</div>
		</div>
	</form>	
</div>		
<div class="clearfix"></div>

<script type="text/javascript">
	$("form" ).on( "submit", function( event ) {
		event.preventDefault();
	});	
</script>	