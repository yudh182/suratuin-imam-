<?php
$daftar_makul = $this->Api_sia->get_makul_smt($this->session->userdata('username'));
?>


<!-- <script type="text/javascript" src="<?php echo base_url();?>assets/jsplugin/jquery-tokeninput/jquery.tokeninput_editbaru.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>assets/jsplugin/jquery-tokeninput/token-input.css" type="text/css" /> -->

<!--<script type="text/javascript" src="<?php echo base_url();?>assets/jsplugin/jquery-tokeninput/src/jquery.tokeninput.js"></script>-->
<!--<link rel="stylesheet" href="<?php echo base_url();?>assets/jsplugin/jquery-tokeninput/token-input.css" type="text/css" />-->
<!--<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>-->

<div class="container-isi-form">
	<form method="POST" id="form_isi_surat" action="<?php echo base_url(); ?>mhs_surat_keluar/simpan_sesi" role="form" class="form-horizontal">
		<div class="form-group">
			<label for="pilih_makul" class="col-md-3 col-sm-4 control-label">Mata Kuliah</label>
			<div class="col-md-9 col-sm-8">
				<select name="pilih_makul" id="pilih_makul" class="form-control">
					<option value="">--pilih mata kuliah--</option>
					<?php
					//$daftar_makul = $this->Repo_SIA->get_makul_smt($this->session->userdata('username'));
					foreach ($daftar_makul as $key => $val) {
					 	echo '<option value="'.$val["KD_MK"].'#'.$val["NM_MK"].'">'.$val["NM_MK"].' (Dosen : '.$val["NM_DOSEN_F"].')</option>';
					}
					?>
				</select>
			</div>
		</div>

		<!-- <div class="form-group">
			<label for="pilih_makul" class="col-md-3 col-sm-4 control-label">Ajax Mata Kuliah</label>
			<div class="col-md-9 col-sm-8">
				<select name="ajax_makul_saya" id="ajax_makul_saya" class="form-control">					
				</select>
			</div>
		</div> -->

		<div class="form-group">
			<label for="kepada_penerima_eks" class="col-md-3 col-sm-4 control-label">Kepada Yth.</label>
			<div class="col-md-9 col-sm-8">
				<input type="text" name="kepada_penerima_eks" id="kepada_penerima_eks" placeholder="" value="" class=" form-control" />
			</div>
		</div>		
		<div class="form-group">				
			<label for="instansi_tujuan" class="col-md-3 col-sm-4 control-label">Nama Instansi</label>
			<div class="col-md-9 col-sm-8">
				<input type="text" name="instansi_tujuan" id="instansi_tujuan" placeholder="" value="" class=" form-control" />
			</div>
		</div>	
		<div class="form-group">
			<label for="alamat_tujuan" class="col-md-3 col-sm-4 control-label">Alamat Tujuan</label>
			<div class="col-md-9 col-sm-8">
				<input type="text" name="alamat_tujuan" id="alamat_tujuan" placeholder="" value="" class=" form-control" />
			</div>
		</div>	
		<div class="form-group">
			<label for="topik_observasi" class="col-md-3 col-sm-4 control-label">Topik Observasi</label>
			<div class="col-md-9 col-sm-8">
				<input type="text" name="topik_observasi" id="topik_observasi" placeholder="" value="" class=" form-control" />
			</div>
		</div>	
		<div class="form-group">
			<label for="tgl_mulai" class="col-md-3 col-sm-4 control-label">Tanggal Pelaksanaan</label>
			<div class="col-md-9 col-sm-8">
				<input type="date" name="tgl_mulai" id="tgl_mulai" value="" class="form-control" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 col-sm-4 control-label">Daftar Mahasiswa Yang Ikut</label>	
			<div class="col-md-9 col-sm-8">					
				<div class="tujuan-surat">
					<div class="auto-surat grup" id="kpd_mahasiswa_grup">
						<div class="label-input">	
							Mahasiswa
						</div>
						<input type="text" name="kpd_mahasiswa" id="kpd_mahasiswa" />
					</div>
				</div>
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-3">
				<input type="hidden" name="id_sesi_buat" value="new" />
				<input type="hidden" name="save_sesi" value="ok" />	
			</div>
			<div class="col-md-9">
				<button type="submit" style="margin: 10px 0px" class="btn btn-inverse btn-medium btn-uin" name="btn-sess" id="btn_simpan_sesi">Simpan sesi</button>				
			</div>
			
		</div>				
	</form>				
</div>
<div class="clearfix"></div>

<script type="text/javascript">
	var no = 1;
	$(document).ready(function() {
		$("#kpd_mahasiswa").tokenInput("<?php echo base_url();?>pgw/external/akademik/auto_mahasiswa", {	
			prePopulate: [ //default current user
				<?php
				if ($this->session->userdata('grup') == 'mhs'){
					echo "{id: '".$this->session->userdata('username')."#0#MHS01#MAHASISWA#".str_replace("'", "", $this->session->userdata('nama'))."', name: '".str_replace("'", "",$this->session->userdata('username')." - ".str_replace("'", "", $this->session->userdata('nama')))."', readonly:true},";
				}
				?>
			],
			tokenLimit: 5,
			minChars: 2,
			tokenFormatter: function(item) { return "<li><p class='left'>" + item[this.propertyToSearch] + 
				"</p><div class='clear'></div><hr style='width:100%; border-color:#A4884A;margin-bottom:0px'/><p style='padding-left:0px;float:left;padding-top:4px;margin-bottom:-8px'>"+
				"<input type='number' name='kpd_urut["+item.nim+"]' style='width:70px;border-radius:0px' placeholder='No. Urut' min='0' /></li>" }
		});

		var data = JSON.parse('[{"id": "0","text": "enhancement"},{"id": "1","text": "bug"},{"id": "2","text": "duplicate"}]');

		// $( "#ajax_makul_saya" ).select2({
		// 	data: data
		// });
		// $( "#ajax_makul_saya2" ).select2({
		//  	placeholder: 'pilih matakuliah yang menugaskan observasi',
		// 	allowClear: true,
		// 	width: "element",
		// 	ajax: {
  //   			url: "<?php echo base_url();?>pgw/external/akademik/auto_makul_smt",
  //   			dataType: 'json',
  //   			data: function (params) {
		// 	      var query = {
		// 	        search: params.term
		// 	        //type: 'public'
		// 	      }
		// 	      // Query parameters menjadi ?search=[term]&type=public
		// 	      return query;
		// 	    },
  //   			processResults: function (data) {
		// 		    return {
		// 		      results: data,

		// 		    };
		// 		},			    				
		// 	    cache: true
		// 	}			
		// });		

		/*$( "#kepada_penerima_eks" ).autocomplete({
		 source: "<?php echo base_url();?>pgw/external/akademik/auto_pejabat_eks", 
		   minLength:2,
		});*/
		// $("#kota_tujuan").tokenInput("<?php echo base_url();?>pgw/external/akademik/auto_kabupaten", {
			<?php
			/*
			if(!empty($default['tempat_tujuan'][0])){ ?>
				prePopulate: [{id: '<?php echo $default['tempat_dibuat'][0]['KD_KAB'];?>', name: '<?php echo ucwords(strtolower($default['tempat_dibuat'][0]['NM_KAB1'])).". ".$default['tempat_dibuat'][0]['NM_KAB2'];?>'}],<?php
			}
			*/
			?>
		// 	tokenLimit:1,
		// 	tokenFormatter: function(item) { return "<li><p class='left'>" + item[this.propertyToSearch] + 
		// 		"</p><div class='clear'></div><hr style='width:100%; border-color:#A4884A;margin-bottom:0px'/><p style='padding-left:0px;float:left;padding-top:4px;margin-bottom:-8px'>"
		// 	}


		// });

	});
</script>