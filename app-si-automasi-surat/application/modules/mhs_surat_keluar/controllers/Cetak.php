<?php if(!defined('BASEPATH'))exit('No direct script access allowed');
/**
* Endpoint untuk menciptakan pdf surat dalam berbagai situasi
* meliputi :
* - preview surat sebelum diterbitkan
* - setelah pengguna menerbitkan surat
* - cetak ulang pada halaman riwayat/arsip
* - surat (watermark) laman cek keasian surat
* diamankan dengan penambahan signature pada url
*/
class Cetak extends MX_Controller {

	public function __construct()
	{
		parent::__construct();	
		
		$this->load->model('common/Api_sia');
		$this->load->model('common/Api_simpeg');					
		$this->load->model('common/Api_automasi_surat','autosurat');

		$this->load->helper('common/Trans_general');
		$this->load->helper('pgw/Tnde_sakad');

		$this->load->module('repo/Api_client_tnde','api_client_tnde'); //produksi
		$this->load->module('repo/Api_client_tnde_staging','api_client_tnde_staging'); //pra-produksi
    } 

    /**
     * Mempersiapkan data-data pembangun cetakan surat
     * dengan melakukan API Requests ke service-service
     */
    public function siapkan_surat($id_surat_keluar=null, $mode='full') #preview, direct (after successfull insert), cetak dari data tersimpan
    {
    	// $this->load->model('common/Api_simpeg');
    	// $this->load->model('common/Api_sia');
    	// $this->load->model('common/Api_automasi_surat');
    	// $this->load->model('pgw/Verifikasi_model');
    	// $this->load->helper('pgw/Tnde_sakad');

    	## 1. API GET SURAT KELUAR (UMUM + PENERIMA)
    	$data['surat_keluar'] = [];
		$data['surat_keluar']['umum'] = $this->api_client_tnde_staging->detail_surat_keluar($id_surat_keluar);
		
		$data['surat_keluar']['raw_penerima'] = $this->api_client_tnde_staging->get_penerima_sk($id_surat_keluar);
		exit(json_encode($data));

		// $data['surat_keluar']['raw_penerima'] = $api_get = $this->apiconn->api_tnde_v3('tnde_general/get_data', 'json', 'POST', array('api_kode' => 1001, 'api_subkode' => 3, 'api_search' => array('D_PENERIMA_SK', 'ID_SURAT_KELUAR', $id_surat_keluar)));

		// $data['surat_keluar']['penerima'] = [];
		// foreach ($data['surat_keluar']['raw_penerima'] as $k=>$v) {
		// 	if ($v['KD_STATUS_DISTRIBUSI'] == 'PS'){
		// 		$data['surat_keluar']['penerima']['PS'][] = $v;
		// 	} elseif ($v['KD_STATUS_DISTRIBUSI'] == 'TS'){
		// 		$data['surat_keluar']['penerima']['TS'][] = $v;
		// 	}
		// }

		// $penerima_by_grup = [];
		// foreach ($data['surat_keluar']['penerima'] as $k=>$v) {
		// 	if ($v['KD_STATUS_DISTRIBUSI'] == 'PS' && $v['KD_GRUP_TUJUAN'] == 'MHS01'){
		// 		$penerima_by_grup['kepada_mahasiswa'][] = $v;
		// 	} elseif ($v['KD_STATUS_DISTRIBUSI'] == 'PS' && $v['KD_GRUP_TUJUAN'] == 'PJB01'){
		// 		$penerima_by_grup['kepada_pejabat'][] = $v;
		// 	} elseif ($v['KD_STATUS_DISTRIBUSI'] == 'PS' && $v['KD_GRUP_TUJUAN'] == 'PGW01'){
		// 		$penerima_by_grup['kepada_pegawai'][] = $v;
		// 	} elseif ($v['KD_STATUS_DISTRIBUSI'] == 'TS' && $v['KD_GRUP_TUJUAN'] == 'PJB01'){
		// 		$penerima_by_grup['tembusan_internal'][] = $v;
		// 	} elseif ($v['KD_STATUS_DISTRIBUSI'] == 'TS' && $v['KD_GRUP_TUJUAN'] == 'EKS01'){
		// 		$penerima_by_grup['tembusan_eksternal'][] = $v;
		// 	}
		// }
		// $data['surat_keluar']['penerima_by_grup'] = $penerima_by_grup;

		//exit(json_encode($data));
		
		$skr_umum = $data['surat_keluar']['umum'];
		$skr_penerima_ps = $data['surat_keluar']['penerima']['PS'];
		$skr_penerima_ts = $data['surat_keluar']['penerima']['TS'];

		## 2. API GET DETAIL SURAT (AUTOMASI)
		$data['detail_surat_automasi'] = $this->autosurat->get_detail_skr_automasi($id_surat_keluar);
		// exit(json_encode($data));

		#seleksi hanya penerima eksternal
		foreach ($skr_penerima_ps as $k=>$v) {
			if ($v['KD_GRUP_TUJUAN'] !== 'EKS01'){
				continue;
			} else {
				$ketemu = $v['PENERIMA_SURAT'];
				break;
			}
		}
		//$data['tujuan_surat']['pimpinan_instansi'] = $save_eks[0]['PENERIMA_SURAT'];
		$data['tujuan_surat']['pimpinan_instansi'] = isset($ketemu) ? $ketemu : 'undefined - kesalahan data';
		$data['tujuan_surat']['tempat_tujuan'] = $data['surat_keluar']['umum']['TEMPAT_TUJUAN'];

		## 3. UNIT
		### 3.1 Unit penerbit surat
		$parameter = array('api_kode' => 1901, 'api_subkode' => 2, 'api_search' => array($data['surat_keluar']['umum']['TGL_SURAT'], $data['surat_keluar']['umum']['UNIT_ID']));
		$data['unit_penerbit'] = $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);

		## PEGAWAI/PEJABAT PENANDATANGANAN disebut dalam surat
		if (!empty($skr_umum['ID_PSD'])){
			$pgw_psd = $this->Api_simpeg->get_pejabat_aktif($skr_umum['TGL_SURAT'], $skr_umum['KD_JABATAN_2']);
			$nip_pgw = $pgw_psd[0]['NIP'];
			$parameter = array('api_kode' => 1123, 'api_subkode' => 3, 'api_search' => array($skr_umum['TGL_SURAT'], $nip_pgw));
			$pang_gol = $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
			$data['psd_detail']['pangkat'] = (!empty($pang_gol) ? $pang_gol[0] : []);
			$data['psd_detail']['pegawai'] = $pgw_psd;
		}
		
		if (!empty($skr_umum['ATAS_NAMA'])){
			$an = $this->apiconn->api_tnde_v3('tnde_general/get_data', 'json', 'POST', array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('MD_PSD', 'ID_PSD', $skr_umum['ATAS_NAMA'])));
			if(!empty($an['KD_JABATAN'])){
				$parameter = array('api_kode' => 1121, 'api_subkode' => 9, 'api_search' => array($skr_umum['TGL_SURAT'], $an['KD_JABATAN'], 3));
				$data['psd_detail']['atas_nama'] = $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
			}			
			$data['psd_ringkas']['label_psd_an'] = $data['psd_detail']['atas_nama'][0]['STR_NAMA_S1']; //$data['label_psd_an'] = $pgw_psd_an[0]['STR_NAMA_A212'];
		}		
		$data['psd_ringkas']['label_psd'] = $pgw_psd[0]['STR_NAMA_S1'];
		//$data['label_psd'] = $pgw_psd[0]['STR_NAMA_A212'];
		$data['psd_ringkas']['nm_pgw_psd'] = $pgw_psd[0]['NM_PGW_F'];
		$data['psd_ringkas']['nip_pgw_psd'] = $pgw_psd[0]['NIP'];

		## PEJABAT disebut dalam surat
		$psd_profil_pegawai = array_intersect_key($pgw_psd[0], array_flip(['NIP','NM_PGW_F','UNIT_NAMA','STR_NAMA','STR_NAMA_A212']));
		if (!empty($data['psd_detail']['pangkat'])){
			$psd_pangkat_pegawai = array_intersect_key($data['psd_detail']['pangkat'], array_flip(['HIE_GOLRU','HIE_NAMA']));	
		} else {
			$psd_pangkat_pegawai = [];
		}
		
		$data['pejabat_dalam_surat'] = array_merge($psd_profil_pegawai, $psd_pangkat_pegawai);

		## MAHASISWA disebut dalam surat
		$kepada = $skr_penerima_ps;		
		$kd_jenis_sakad_int = (int)$data['detail_surat_automasi']['KD_JENIS_SAKAD'];

		$arr_jenis_sakad = $this->Api_automasi_surat->get_jenis_surat_mahasiswa('kd', $kd_jenis_sakad_int);
		$data['raw_jenis_sakad'] = $arr_jenis_sakad;
		//exit(var_dump($data['raw_jenis_sakad']));

		foreach ($kepada as $k=>$v) {
			if($v['KD_GRUP_TUJUAN'] == 'MHS01'){
				$multi_mhs = [8,11]; #observasi, penelitian matkul, rekomendasi penelitian
				if (in_array($kd_jenis_sakad_int, $multi_mhs)){
					$tmp_mhs = $this->Api_sia->get_profil_mhs($v['PENERIMA_SURAT'],'kumulatif');
					$data['mhs_dalam_surat'][$k]['NIM']  = $tmp_mhs[0]['NIM']; 
					$data['mhs_dalam_surat'][$k]['NAMA']  = $tmp_mhs[0]['NAMA_F'];
				} else {
					$data['mhs_dalam_surat'][] = $this->Verifikasi_model->prepare_isi_surat($arr_jenis_sakad, $v['PENERIMA_SURAT'])['tmp_verified_mhs'];
				}
				
			} else {
				continue;
			}
		}

		##ambil data prodi berdasarkan data mahasiswa disebut dlm surat
		$getmhs = $this->Api_sia->get_profil_mhs($skr_umum['PEMBUAT_SURAT'],'kumulatif');	
		$data['prodi'] = $getmhs[0]['NM_PRODI']; 


		// foreach ($kepada as $k=>$v) {
		// 	if($v['KD_GRUP_TUJUAN'] == 'MHS01'){
		// 		#cek apakah terdaftar untuk request sia_mhs_kum
		// 		if (in_array($kd_jenis_sakad_int, $config_api_by_jenis_surat['sia_mhs_kum'])){
		// 			$get_mhs = $this->Api_sia->get_profil_mhs($v['PENERIMA_SURAT'],'kumulatif');
		// 			$data['detail_penerima'][$k]['mahasiswa']['kum'] = (!empty($get_mhs[0]) ? $get_mhs[0] : '');

		// 		}

		// 		#cek apakah terdaftar untuk request sia_mhs_dpm
		// 		if (in_array($kd_jenis_sakad_int, $config_api_by_jenis_surat['sia_mhs_dpm'])){
		// 			$get_mhs = $this->Api_sia->get_profil_mhs($v['PENERIMA_SURAT'],'dpm');
		// 			$data['detail_penerima'][$k]['mahasiswa']['dpm'] = (!empty($get_mhs[0]) ? $get_mhs[0] : '');
		// 		}

		// 		#cek apakah terdaftar untuk request sia_mhs_makul
		// 		if (in_array($kd_jenis_sakad_int, $config_api_by_jenis_surat['sia_mhs_makul'])){
					
		// 		}
				
		// 	} else {
		// 		continue;
		// 	}
		// }

		// ## TEMBUSAN INTERNAL (PEJABAT)
		// $tembusan = $skr_penerima_ts; //assign
		// if(!empty($tembusan)){
		// 	foreach($tembusan as $key => $val){
		// 		switch($val['KD_GRUP_TUJUAN']){
		// 			case 'PJB01':
		// 				$parameter 	= array('api_kode' => 1101, 'api_subkode' => 1, 'api_search' => array($val['JABATAN_PENERIMA']));
		// 				$jab_tembusan = $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
		// 				break;
		// 			case 'MHS01':
		// 				$parameter	= array('api_kode'=>26000, 'api_subkode' => 12, 'api_search' => array($val['PENERIMA_SURAT']));
		// 				$get_mhs 		= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);
		// 				$data['penerima_tembusan'][$key]['mahasiswa'] = (!empty($get_mhs[0]) ? $get_mhs[0] : '');
		// 				break;
		// 			case 'EKS01':

		// 				break;
		// 		break;

		// 	}
		// }
		

		if ($mode === 'full'){
			$this->generate_pdf_surat_v2($data);	
		} elseif ($mode === 'json') {
			$this->output->set_status_header(200)
			 	->set_content_type('application/json')
			 	->set_output(json_encode($data));
		}		
    }

     /**
    * Menghasilkan PDF Surat 
    * perbaikan dari versi sebelumnya : 
    	- mengurangi additional request ke API (sampai hanya butuh 1 request saja)
    	- lebih semantik    
    */
    public function generate_pdf_surat_v2($paramdata=null)
    {
    	//$this->load->model('Repo_SIA');
    	$this->load->helper('pgw/Tnde_sakad');
    	$this->load->helper('common/Trans_general');
    	$this->load->model('common/Konversi_model','konversi_model');
		$this->load->library('common/Pdf_creator');


    	$elemen_pdf_surat = ['meta', 'kop', 'frasa_buka', 'isi_pejabat_psd', 'isi_mhs', 'isi_surat', 'frasa_penutup', 'psd', 'tembusan'];

    	$raw_skr_umum = $paramdata['surat_keluar']['umum'];
    	$raw_tembusan = $paramdata['surat_keluar']['penerima']['TS'];
    	$raw_pengelola = $paramdata['surat_keluar']['penerima']['PS'];

    	$raw_data_mhs = $paramdata['mhs_dalam_surat'];
    	if (count($raw_data_mhs) > 1){
			$raw_data_mhs = $raw_data_mhs;
		} else {
			$raw_data_mhs = (isset($raw_data_mhs[0]) ? $raw_data_mhs[0] : $raw_data_mhs);
		}

    	$raw_isi_surat = $paramdata['detail_surat_automasi'];
    	$raw_unit_penerbit = $paramdata['unit_penerbit'][0];
    	$raw_psd = $paramdata['psd_detail'];

    	#siapkan variabel2 pembentuk pdf
    	if (!empty($raw_unit_penerbit)){
    		$data['template']['kopsurat'] = [
				'kementrian' => 'KEMENTERIAN AGAMA REPUBLIK INDONESIA',
				'univ' => 'UNIVERSITAS ISLAM NEGERI SUNAN KALIJAGA YOGYAKARTA',
				'fkl' => (!empty($raw_unit_penerbit['UNIT_NAMA']) ? strtoupper($raw_unit_penerbit['UNIT_NAMA']) : ''),
				'almt' => (!empty($raw_unit_penerbit['UNIT_ALAMAT']) ? $raw_unit_penerbit['UNIT_ALAMAT'] : '').' '.(!empty($raw_unit_penerbit['UNIT_KDPOS']) ? $raw_unit_penerbit['UNIT_KDPOS'] : ''),
				'tlp' => (!empty($raw_unit_penerbit['UNIT_TELP1']) ? $raw_unit_penerbit['UNIT_TELP1'] : ''),
				'fax' => (!empty($raw_unit_penerbit['UNIT_FAX1']) ? $raw_unit_penerbit['UNIT_FAX1'] : ''),
				'web' => $this->konversi_model->website_unit($paramdata['surat_keluar']['umum']['UNIT_ID']),
				'kabkota' => (!empty($raw_unit_penerbit['UNIT_NMKAB2']) ? $raw_unit_penerbit['UNIT_NMKAB2'] : '')
			];
    	}

    	if (!empty($paramdata['psd_ringkas'])){
    		$data['psd'] = $paramdata['psd_ringkas'];
    	}

    	if (!empty($paramdata['pejabat_dalam_surat'])){
    		$data['pejabat_dalam_surat'] = $paramdata['pejabat_dalam_surat'];	
    	}

    	$data['judul_surat'] = strtoupper($paramdata['raw_jenis_sakad']['NM_JENIS_SAKAD']);
		$data['nomor'] = $paramdata['surat_keluar']['umum']['NO_SURAT'];
		$data['thn_akademik'] = $this->Api_sia->get_tahun_ajaran('info_ta_smt');
		$data['tgl_surat'] = tanggal_indo($paramdata['surat_keluar']['umum']['TGL_SURAT'], 'd/m/Y');
		$data['qrcode_otentikasi'] = base_url('publik/cek_surat/validasi_qrcode/').$paramdata['surat_keluar']['umum']['ID_SURAT_KELUAR'];

		##SET UP DATA PEMBENTUK PDF
		$watermark_img = file_get_contents(FCPATH.'/assets/images/watermark-preview.png');
		$opsi = (isset($paramdata['opsi']) ? $paramdata['opsi'] : null);
		if (!empty($opsi)){
			if ($opsi == 'preview'){
				$data['watermark_img'] = base64_encode($watermark_img);
			}	
		}
		
		//daftar tembusan
		if (!empty($paramdata['daftar_tembusan'])){
			$ts = 'Tembusan :
			<ol style="margin: 0; text-indent:0;">';
			foreach ($paramdata['daftar_tembusan'] as $k=>$v) {
				$ts .= '<li>';
				$ts .= $v['penerima_tembusan'];
				if ($v['status_tembusan'] == '1'){
					$ts .= ' (sebagai laporan)';
				}
				$ts .= '</li>';						
			}
			$ts .= '</ol>';
			$data['daftar_tembusan_htmlstring'] = $ts;
		}


		## CONDITIONAL CHECK CUSTOM DATA TIAP JENIS SURAT
		$kd_jenis_sakad = $paramdata['raw_jenis_sakad']['KD_JENIS_SAKAD'];
		$path = $paramdata['raw_jenis_sakad']['URLPATH'];
		switch ($path) {
			case 'masih_kul':	#1. PDF Surat Keterangan Masih Kuliah
				$data['isi_pgw'] = '';

				$raw_data_mhs = isset($raw_data_mhs[0]) ? $raw_data_mhs[0] : $raw_data_mhs;		
				$data['isi_mhs'] = [
					'nama' => $raw_data_mhs['NAMA'],
					'nim' => $raw_data_mhs['NIM'],
					'tmp_lahir' => $raw_data_mhs['TMP_LAHIR'],		
					'tgl_lahir' => display_tanggal_indo($raw_data_mhs['TGL_LAHIR'], 'd-m-Y'),
					'smt' => $raw_data_mhs['JUM_SMT'],
					'prodi' => $raw_data_mhs['NM_PRODI'],
					'fkl' => $raw_data_mhs['NM_FAK'],
				];
				$buka_frasa_isi = 'Surat keterangan ini dibuat untuk melengkapi salah satu syarat ';
				$data['isi_surat'] = $buka_frasa_isi.$raw_isi_surat['KEPERLUAN'];
				$view = 'report/v_r_masih_kul';
			break;
			case 'kel_baik': #2. PDF Surat Keterangan Berkelakuan Baik
				$raw_data_mhs = isset($raw_data_mhs[0]) ? $raw_data_mhs[0] : $raw_data_mhs;
				$data['isi_mhs'] = [
					'nama' => $raw_data_mhs['NAMA'],
					'nim' => $raw_data_mhs['NIM'],
					'tmp_lahir' => $raw_data_mhs['TMP_LAHIR'],
					'tgl_lahir' => display_tanggal_indo($raw_data_mhs['TGL_LAHIR'], 'd-m-Y'),
					'smt' => $raw_data_mhs['JUM_SMT'],
					'prodi' => $raw_data_mhs['NM_PRODI'],
					'fkl' => $raw_data_mhs['NM_FAK']
				];
				$data['isi_surat'] = [					
					'keperluan' => $raw_isi_surat["KEPERLUAN"]
				];
				$view = 'report/v_r_kel_baik';
			break;
			case 'ijin_pen_makul':
				$data['tujuan'] = [					
					'kepada' => str_replace(['\r\n', '\r', '\n'], '<br/>', $paramdata['tujuan_surat']['pimpinan_instansi']),
					'instansi' => '',
					'alamat_tempat' => $paramdata['tujuan_surat']['tempat_tujuan']
				];
				// $raw_data_mhs = isset($raw_data_mhs[0]) ? $raw_data_mhs[0] : $raw_data_mhs;
				//exit(json_encode($raw_data_mhs));
				$data['isi_mhs'] = $raw_data_mhs;	

				$makul = explode('#', $raw_isi_surat['MATA_KULIAH_TERKAIT']);
				$data['prodi'] = $paramdata['prodi'];
				$data['isi_surat'] = [
					'nama_kegiatan' => (!empty($raw_isi_surat["NAMA_KEGIATAN"]) ? $raw_isi_surat["NAMA_KEGIATAN"] : ''),
					'kode_makul' => $makul[0],
					'nama_makul' => $makul[1],
					'topik_penelitian_makul' => $raw_isi_surat["TEMA_DLM_KEGIATAN"],
					'tempat' => $raw_isi_surat["TEMPAT_KEGIATAN"],				
					'tgl_mulai' => display_tanggal_indo($raw_isi_surat["TGL_MULAI"], 'Y-m-d'),
					'tgl_berakhir'=> null,
				];
				$data['daftar_tembusan'][] = isset($pgw_psd_an) ? $pgw_psd_an[0]['STR_NAMA_S1'].' (sebagai laporan)' : '';
				$view = 'report/v_r_pen_makul';	
			break;
			case 'rek_pen': #2. PDF Surat Permohonan Rekomendasi Penelitian
				$data['tujuan'] = [					
					'kepada' => str_replace(['\r\n', '\r', '\n'], '<br/>', $paramdata['tujuan_surat']['pimpinan_instansi']),
					'instansi' => '',
					'alamat_tempat' => $paramdata['tujuan_surat']['tempat_tujuan']
				];

				$data['isi_mhs'] = (isset($raw_data_mhs[0]) ? $raw_data_mhs[0] : $raw_data_mhs);
				$data['isi_surat'] = $raw_isi_surat;
				$view = 'report/v_r_rek_pen';
				//exit(json_encode($data, JSON_PRETTY_PRINT));
			break;
			case 'ket_lulus':
				$raw_data_mhs = isset($raw_data_mhs[0]) ? $raw_data_mhs[0] : $raw_data_mhs;
				$data['isi_mhs'] = $raw_data_mhs;
				$data['isi_surat'] = $raw_isi_surat;
				if( is_array($data['isi_surat']['DATA_TAMBAHAN']) ){
					$data['isi_surat'] = $raw_isi_surat;
				} else { //masih dalam string php serialized
					$data['isi_surat']['DATA_TAMBAHAN'] = unserialize($data['isi_surat']['DATA_TAMBAHAN']);
				}
				//exit(var_dump($data));
				$view = 'report/v_r_lulus';
				break;
			case 'hbs_teori':
				$raw_data_mhs = isset($raw_data_mhs[0]) ? $raw_data_mhs[0] : $raw_data_mhs;
				$data['isi_mhs'] = $raw_data_mhs;
				$data['isi_surat'] = $raw_isi_surat;
				//exit(var_dump($data));
				$view = 'report/v_r_hbs_teori';
				break;
			case 'perm_kp':
				$data['tujuan'] = [
					// 'kepada' => str_replace(['\r\n', '\r', '\n'], '<br/>', $paramdata['tujuan_surat']['pimpinan_instansi']),
					'pimpinan_instansi' => str_replace(['\r\n', '\r', '\n'], '<br/>', $paramdata['tujuan_surat']['pimpinan_instansi']),
					'alamat' => $paramdata['tujuan_surat']['tempat_tujuan']
				];

				$makul = explode('#', $raw_isi_surat['MATA_KULIAH_TERKAIT']);

				$raw_data_mhs = isset($raw_data_mhs[0]) ? $raw_data_mhs[0] : $raw_data_mhs;
				$data['isi_mhs'] = [
					'nama' => $raw_data_mhs['NAMA'],
					'nim' => $raw_data_mhs['NIM'],
					'tmp_lahir' => $raw_data_mhs['TMP_LAHIR'],
					'tgl_lahir' => display_tanggal_indo($raw_data_mhs['TGL_LAHIR'], 'd-m-Y'),
					'smt' => $raw_data_mhs['JUM_SMT'],
					'prodi' => $raw_data_mhs['NM_PRODI'],
					'fkl' => $raw_data_mhs['NM_FAK']
				];
				$data['isi_surat'] = [
					'nama_kegiatan' => $makul[1],//$raw_isi_surat["NAMA_KEGIATAN"],
					'kode_makul' => $makul[0],
					'nama_makul' => $makul[1],
					'tempat' => $raw_isi_surat["TEMPAT_KEGIATAN"],
					'minat' => $raw_isi_surat["TEMA_DLM_KEGIATAN"],
					'tgl_mulai' => display_tanggal_indo($raw_isi_surat["TGL_MULAI"], 'Y-m-d'),
					'tgl_berakhir'=> display_tanggal_indo($raw_isi_surat["TGL_BERAKHIR"], 'Y-m-d'),
				];
				$view = 'report/v_r_kp';
			break;
			
			case 'tdk_men_bea': #4. PDF Surat Keterangan Tidak Sedang menerima beasiswa
				$raw_data_mhs = isset($raw_data_mhs[0]) ? $raw_data_mhs[0] : $raw_data_mhs;
				$data['isi_mhs'] = [
					'nama' => $raw_data_mhs['NAMA'],
					'nim' => $raw_data_mhs['NIM'],
					'tmp_lahir' => $raw_data_mhs['TMP_LAHIR'],
					'tgl_lahir' => display_tanggal_indo($raw_data_mhs['TGL_LAHIR'], 'd-m-Y'),
					'smt' => $raw_data_mhs['JUM_SMT'],
					'prodi' => $raw_data_mhs['NM_PRODI'],
					'fkl' => $raw_data_mhs['NM_FAK']
				];

				if (!empty($raw_isi_surat["DATA_TAMBAHAN"])){
					$bea = '<ul>';
					foreach ($raw_isi_surat["DATA_TAMBAHAN"] as $k=>$v) {
						$bea .= '<li>'.$v['NAMA_BEASISWA'].'</li>';
					}
					$bea .= '</ul>';
				} else {
					$bea = '';
				}
				$data['isi_surat'] = [
					'daftar_beasiswa_pernah_diikuti' => $bea,
				];
				$view = 'report/v_r_tdk_men_bea';
			break;

			case 'pndh_studi': #5. PDF Surat Keterangan Pindah Studi
				$raw_data_mhs = isset($raw_data_mhs[0]) ? $raw_data_mhs[0] : $raw_data_mhs;
				$data['isi_mhs'] = [
					'nama' => $raw_data_mhs['NAMA'],
					'nim' => $raw_data_mhs['NIM'],
					'tmp_lahir' => $raw_data_mhs['TMP_LAHIR'],
					'tgl_lahir' => display_tanggal_indo($raw_data_mhs['TGL_LAHIR'], 'd-m-Y'),
					'smt' => $raw_data_mhs['JUM_SMT'],
					'prodi' => $raw_data_mhs['NM_PRODI'],
					'fkl' => $raw_data_mhs['NM_FAK']
				];
				$data['isi_surat'] = [
					'keperluan' => $raw_isi_surat["KEPERLUAN"]
				];
				$view = 'report/v_r_pindah';
			break;

			case 'ijin_pen':
				// $data['tujuan'] = [
				// 	//'kepada' => nl2br($paramdata['distribusi_ps']['penerima_eksternal'][0]['PENERIMA_SURAT']),
				// 	'kepada' => str_replace(['\r\n', '\r', '\n'], '<br/>', $paramdata['distribusi_ps']['penerima_eksternal'][0]['PENERIMA_SURAT']),
				// 	'instansi' => '',
				// 	'alamat' => $paramdata['insert_surat_keluar']['TEMPAT_TUJUAN']
				// ];

				$data['tujuan'] = [					
					'kepada' => str_replace(['\r\n', '\r', '\n'], '<br/>', $paramdata['tujuan_surat']['pimpinan_instansi']),
					'instansi' => '',
					'alamat_tempat' => $paramdata['tujuan_surat']['tempat_tujuan']
				];

				$data['isi_mhs'] = [
					'nama' => $raw_data_mhs['NAMA'],
					'nim' => $raw_data_mhs['NIM'],
					'tmp_lahir' => $raw_data_mhs['TMP_LAHIR'],
					'tgl_lahir' => display_tanggal_indo($raw_data_mhs['TGL_LAHIR'], 'd-m-Y'),
					'smt' => $raw_data_mhs['JUM_SMT'],
					'prodi' => $raw_data_mhs['NM_PRODI'],
					'fkl' => $raw_data_mhs['NM_FAK'],
					'hp' => $raw_data_mhs['NO_HP'],
					'alamat' => $raw_data_mhs['ALAMAT']
				];

				$data['isi_surat'] = [
					'judul'	=> $raw_isi_surat["JUDUL_DLM_KEGIATAN"],
					'tempat_penelitian' => $raw_isi_surat["TEMPAT_KEGIATAN"],
					'metode' => $raw_isi_surat["METODE_DLM_KEGIATAN"],
					'tgl_mulai' => display_tanggal_indo($raw_isi_surat["TGL_MULAI"], 'Y-m-d'),
					'tgl_berakhir'=> display_tanggal_indo($raw_isi_surat["TGL_BERAKHIR"], 'Y-m-d'),
				];

				$data['daftar_tembusan'][] = isset($pgw_psd_an) ? $pgw_psd_an[0]['STR_NAMA_S1'].' (sebagai laporan)' : '';
				$view = 'report/v_r_penelitian';
			break;

			case 'ijin_studi_pen':
				$data['tujuan'] = [					
					'kepada' => str_replace(['\r\n', '\r', '\n'], '<br/>', $paramdata['tujuan_surat']['pimpinan_instansi']),
					'instansi' => '',
					'alamat_tempat' => $paramdata['tujuan_surat']['tempat_tujuan']
				];

				$raw_data_mhs = (isset($raw_data_mhs[0]) ? $raw_data_mhs[0] : $raw_data_mhs);
				$data['isi_mhs'] = [
					'nama' => $raw_data_mhs['NAMA'],
					'nim' => $raw_data_mhs['NIM'],
					'smt' => trans_angka_romawi($raw_data_mhs['JUM_SMT']),
					'prodi' => $raw_data_mhs['NM_PRODI'],
					'fkl' => $raw_data_mhs['NM_FAK']
				];
				$data['isi_surat'] = [
					'keperluan' => $raw_isi_surat["KEPERLUAN"],
					'nama_kegiatan' => $raw_isi_surat["NM_KEGIATAN"],
					'tema' => $raw_isi_surat["TEMA_DLM_KEGIATAN"],
					'tempat_penelitian' => $raw_isi_surat["TEMPAT_KEGIATAN"],
					'tgl_mulai' => display_tanggal_indo($raw_isi_surat["TGL_MULAI"], 'Y-m-d'),
					'tgl_berakhir'=> display_tanggal_indo($raw_isi_surat["TGL_BERAKHIR"], 'Y-m-d'),
				];					
				
				$view = 'report/v_r_studi_pen';
			break;

			case 'ijin_obs':
				$data['tujuan'] = [					
					'kepada' => str_replace(['\r\n', '\r', '\n'], '<br/>', $paramdata['tujuan_surat']['pimpinan_instansi']),
					'instansi' => '',
					'alamat_tempat' => $paramdata['tujuan_surat']['tempat_tujuan']
				];						
				
				
				$data['isi_mhs'] = $raw_data_mhs;
				//exit(var_dump($data['isi_mhs']));

				$makul = explode('#', $raw_isi_surat['MATA_KULIAH_TERKAIT']);
				//sementara
				// $data['prodi'] = $paramdata['bagian_surat_lain']['nm_unit_prodi'];
				$data['isi_surat'] = [
					'nama_kegiatan' => 'Observasi',//$raw_isi_surat["NAMA_KEGIATAN"],
					'kode_makul' => $makul[0],
					'nama_makul' => $makul[1],
					'topik_observasi' => $raw_isi_surat["TEMA_DLM_KEGIATAN"],
					'tempat' => $raw_isi_surat["TEMPAT_KEGIATAN"],
					'tgl_mulai' => display_tanggal_indo($raw_isi_surat["TGL_MULAI"], 'Y-m-d'),
					'tgl_berakhir'=> display_tanggal_indo($raw_isi_surat["TGL_BERAKHIR"], 'Y-m-d'),
				];				
				$view = 'report/v_r_observasi';
			break;

			case 'ijin_pen_makul':
				$data['tujuan'] = [					
					'kepada' => str_replace(['\r\n', '\r', '\n'], '<br/>', $paramdata['tujuan_surat']['pimpinan_instansi']),
					'instansi' => '',
					'alamat_tempat' => $paramdata['tujuan_surat']['tempat_tujuan']
				];				
				$data['isi_mhs'] = $raw_data_mhs;				

				$makul = explode('#', $raw_isi_surat['MATA_KULIAH_TERKAIT']);
				$data['prodi'] = $paramdata['prodi'];
				$data['isi_surat'] = [
					'nama_kegiatan' => (!empty($raw_isi_surat["NAMA_KEGIATAN"]) ? $raw_isi_surat["NAMA_KEGIATAN"] : ''),
					'kode_makul' => $makul[0],
					'nama_makul' => $makul[1],
					'topik_penelitian_makul' => $raw_isi_surat["TEMA_DLM_KEGIATAN"],
					'tempat' => $raw_isi_surat["TEMPAT_KEGIATAN"],				
					'tgl_mulai' => display_tanggal_indo($raw_isi_surat["TGL_MULAI"], 'Y-m-d'),
					'tgl_berakhir'=> null,
				];
				// $data['daftar_tembusan'][] = isset($pgw_psd_an) ? $pgw_psd_an[0]['STR_NAMA_S1'].' (sebagai laporan)' : '';
				$view = 'report/v_r_pen_makul';	
			break;	

			default:				
				echo "<h1>Template report belum ada</h1><br><br>";
				echo '<pre>'.json_encode($data).'</pre>';

				break;
		}
		$this->load->view($view,$data);
    }

}