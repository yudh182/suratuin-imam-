<?php if(!defined('BASEPATH'))exit('No direct script access allowed');

class Mhs_surat_keluar extends MY_Controller {

	public function __construct()
	{
		parent::__construct();	
		
		// if ($this->session->userdata('log') !== 'in')
		// 	redirect(site_url('auth/login?from='.current_url()));

		$this->load->module('pgw/api/Api_foto','api_foto');
		$this->load->module('pgw/external/Akademik','akademik');
		$this->load->model('pgw/Automasi_model');
		$this->load->model('common/Api_automasi_surat','autosurat');

    }    

    protected function is_auth() #is authorized ?
    {
    	if ($this->session->userdata('log') !== 'in' && $this->session->userdata('grup') !== 'mhs')
			redirect(site_url('auth/login?from='.current_url()));		
    }

    public function index()
    {
    	$this->load->helper('common/MY_array');

    	$this->is_auth();


    	$data['_view_notifikasi_tte'] = $this->load->view('content/v_widget_notifikasi_penerbitan', 
    		[
    			'status_penerbitan'=>TRUE,
    			'response_data' => [
    				'TGL_PENYELESAIAN' => '2018-05-12 12:00:00'
    			]
    		], true);
    	
    	#1. list surat
    	$data['list_jenis_sakad'] = $this->autosurat->get_jenis_surat('aktif');
		$list_page = array_column($data['list_jenis_sakad'], 'URLPATH'); #daftar page berdasarkan urlpath

		#2. bagi jenis-jenis surat sesuai grup
		$jsg = groupArray($data['list_jenis_sakad'], "GRUP_JENIS_SAKAD", true, false);
		$data['jenis_surat_per_grup'] = $jsg;
		#2. cek hak akses
			#code here....

		#3. Render view akhir
		$this->load->view('common/templates/v_header');
		$this->load->view('common/templates/v_sidebar', $data);
		$this->load->view('content/v_home_skr',$data);
		$this->load->view('common/templates/v_footer');

    }       

    //LAMAN PEMBUATAN SURAT
    public function buat($page='')
	{
		$this->is_auth();
		#1. FIRST LOAD
		// $this->load->model('pgw/Automasi_model');
		$this->load->model('pgw/Verifikasi_model');		
		$this->load->helper('pgw/Tnde_sakad');
				
		#2. Daftar jenis surat				
		$data['list_jenis_sakad'] = $this->autosurat->get_jenis_surat('aktif');
		$list_page = array_column($data['list_jenis_sakad'], 'URLPATH'); #daftar page berdasarkan urlpath		
		$page = $this->uri->segment(3); //current page

		$data['cek_sesi'] = $this->Automasi_model->sesi_by_kd_user($this->session->userdata('username'));		
		if (!empty($data['cek_sesi']))
			$data['_widget_sesi'] = $this->load->view('content/v_widget_cek_sesi', $data, true);		

		$kel_surat_v1 = [];
		$kel_surat_v2 = [];
		$kel_surat_v3 = ['masih_kul', 'kel_baik', 'tdk_men_bea', 'ket_lulus', 'hbs_teori', 'pndh_studi', 'ijin_studi_pen', 'ijin_obs', 'ijin_pen', 'ijin_pen_makul','perm_kp','rek_pen']; #form support ajax validations, perbaikan template pdf, bisa validasi keaslian qr code
				
		if (in_array($page, $list_page)){ #current/visited page
			#3. cek data jenis surat saat ini
			// $data['cek_sesi'] = $this->Automasi_model->sesi_by_kd_user($this->session->userdata('username'));
			$idx_curr_page = array_search($page, $list_page); $data['jenis_sakad'] = $data['list_jenis_sakad'][$idx_curr_page];
			$param_jenis_sakad = $data['jenis_sakad'];
		    $param_nim = (!empty($this->session->userdata('username')) ? $this->session->userdata('username') : $this->input->get('nim'));//$this->session->userdata('username');			

		    if (empty($data['cek_sesi'])){
		    	#4. cek hasil verifikasi dan kondisional template view sesuai kelompok versi 
				if (in_array($page, $kel_surat_v2)){	#VERSI 2
			    	$pre_data_surat = $this->Verifikasi_model->verifikasi_persyaratan($param_jenis_sakad, $param_nim);
				    $data['pre_data_surat'] = $pre_data_surat;
				    exit(print_r($data));
				} elseif (in_array($page, $kel_surat_v3)) {		#VERSI 3		
				    $crosscheck_syarat = $this->Verifikasi_model->verifikasi_persyaratan($param_jenis_sakad, $param_nim);
				    $data['hasil_verifikasi'] = $crosscheck_syarat;

				    #Tampilkan form jika lolos verifikasi (htmlstring buffer)
				    if ($data['hasil_verifikasi']['hasil_cek_mhs']['num_errors'] == 0)
				    {
			    		if (!empty($data['hasil_verifikasi']['tmp_verified_mhs'])){
			    			$this->session->set_userdata('pre_isi_mhs',$data['hasil_verifikasi']['tmp_verified_mhs']);
			    		}

			    		if (!empty($data['hasil_verifikasi']['tmp_isi_detail'])){
			    			$this->session->set_userdata('pre_isi_surat',$data['hasil_verifikasi']['tmp_isi_detail']);
			    		}

			    		// //kondisional formdata
			    		// if (in_array($page, ['ijin_obs','ijin_pen_makul'])){
			    		// 	$data['json_makul'] = modules::run(
			    		// 		'akademik/auto_makul_smt_powerfull',
			    		// 		$this->session->userdata("username")
			    		// 	);
			    		// }
			    		
						$view_form = 'form/v_'.$page;
						$data['_view_form'] = $this->load->view($view_form, $data, true);
					}

				}
		    }
								
			$view = 'content/v_buat_terverifikasi';
		} else {
			$view = 'buat_404';
		}
		
		#5. Render view akhir
		$this->load->view('common/templates/v_header');
		$this->load->view('common/templates/v_sidebar', $data);
		$this->load->view($view,$data);
		$this->load->view('common/templates/v_footer');
	}

	//LAMAN RIWAYAT SURAT
	public function riwayat()
	{
		#0. Mengmabil data daftar jenis surat				
		$data['list_jenis_sakad'] = $this->autosurat->get_jenis_surat('aktif');
		
		#1. Mengambil data riwayat
		$nim = $this->session->userdata('username');
		$parameter	= array('search'=>$nim);
		// $data['riwayat'] = $this->apiconn->api_autosurat('arsip_surat_keluar/data_search_by/nim', 'json', 'GET', $parameter);
		$data['riwayat'] = $this->apiconn->api_autosurat('arsip_surat_keluar/data_search_by/nim?search=' . $nim, 'json', 'GET', array());				

		#3. Render view akhir
		$this->load->view('common/templates/v_header');
		$this->load->view('common/templates/v_sidebar', $data);
		$this->load->view('content/v_riwayat',$data);
		$this->load->view('common/templates/v_footer');
	}	

	//ACTION
	public function simpan_sesi()
	{
		$this->load->library('form_validation');

		#1. TANGKAP DATA FORM dan SESSION
		$sess_detail = $this->session->userdata('pre_isi_surat');
    	$sess_mhs = $this->session->userdata('pre_isi_mhs');
    	$kd_jenis_surat_int = (int)$sess_detail['KD_JENIS_SAKAD'];

    	if ($this->session->userdata('log') == 'in' && $this->input->post('save_sesi') == 'ok'){
    		# 2. FORM VALIDATION
    		switch ($kd_jenis_surat_int) {
    			case 1: #Masih Kuliah
					$config = array(array('field' => 'keperluan','label' => 'Keperluan','rules' => 'required'));					
					$form_ke_grup = [
						'KEPERLUAN' => $this->input->post('keperluan'),
						'CONFIG_MASA_BERLAKU' => 'P5M' //5 BULAN
					];
				break;
				case 2: #Berkelakuan baik
					$config = array(array('field' => 'keperluan','label' => 'Keperluan','rules' => 'required'));

					$form_ke_grup = [
						'KEPERLUAN' => $this->input->post('keperluan'),
						'CONFIG_MASA_BERLAKU' => 'P5M' //5 BULAN
					];
				break;

    			case 3: #6. Keterangan Habis Teori
					$config = array(array('field' => 'keperluan','label' => 'Keperluan','rules' => 'required'));
					$form_ke_grup = [
						'KEPERLUAN' => $this->input->post('keperluan'),
						'CONFIG_MASA_BERLAKU' => 'P3M' //3 BULAN
					];
					break;
				case 4: #tidak sedang menerima beasiswa
					$config = array(
						array('field' => 'keperluan','label' => 'Keperluan','rules' => 'required'),
					);
					$form_ke_umum = [
						//'PENERIMA_MHS' => $this->session->userdata('username').'#0',
						'ISI_SURAT' => $this->input->post('keperluan')
					];

					$bpd_nama               = $this->input->post('nm_beasiswa_pd');
			        $bpd_keterangan         = $this->input->post('ket_beasiswa_pd');
			        $tot = count($bpd_nama);
			        $no = 0;
			        if(!empty($bpd_nama)){
			            $bea_pd = [];
			            foreach($bpd_nama as $n => $val){
			                $no++;

			                $item_bpd_nama = $bpd_nama[$n];
			                if(!empty($item_bpd_nama)){
			                    $bea_pd[$n]['NAMA_BEASISWA'] = $bpd_nama[$n];
			                    $bea_pd[$n]['KETERANGAN_BEASISWA'] = $bpd_keterangan[$n];
			                    $bea_pd[$n]['NO_URUT']               = $no;
			                }
			            }
			        }
			        $beasiswa_pernah_diikuti = array_values($bea_pd); //re-order index key
					$form_ke_grup = [
						'KEPERLUAN' => $this->input->post('keperluan'),
						'DATA_TAMBAHAN' => $beasiswa_pernah_diikuti, //data asal dalam array --> serialized (meta, data)
						'CONFIG_MASA_BERLAKU' => 'P2M' //1 BULAN
					];
				break;

				case 5: #Pindah Studi
					$config = array(
						array('field' => 'opsi_pindah','label' => 'Opsi Pindah','rules' => 'required'),
						array('field' => 'catatan','label' => 'Catatan','rules' => 'required')
					);
					$opsi_pindah = $this->input->post('opsi_pindah');
					$catatan = $this->input->post('catatan');
					if  ($opsi_pindah == '1'){
						$keperluan = "pengajuan pindah studi ke Program Studi ".$catatan;
					} elseif ($opsi_pindah == '2') {
						$keperluan = "meneruskan studi di Perguruan Tinggi lain";
					} else {
						$keperluan = 'kosong';
					}
					$form_ke_grup = [
						'KEPERLUAN' =>$keperluan,
						'CONFIG_MASA_BERLAKU' => 'P1M' //1 BULAN
					];
				break;

				case 6: #6. Keterangan Lulus
					$config = array(array('field' => 'keperluan','label' => 'Keperluan','rules' => 'required'));
										
					$form_ke_grup = [
						'KEPERLUAN' => $this->input->post('keperluan'),
						//'DATA_TAMBAHAN' => $data_tambahan,
						'CONFIG_MASA_BERLAKU' => 'P3M' //3 BULAN
					];
					break;
				case 7: #izin penelitian (skripsi/tugas akhir)
					$config = array(
						array('field' => 'judul_penelitian','label' => 'Judul Penelitian','rules' => 'required'),
						array('field' => 'instansi_tujuan','label' => 'Instansi Tujuan','rules' => 'required'),
				        array('field' => 'mtd_penelitian','label' => 'Metode Penelitian','rules' => 'required')
					);			
					#update 2 mei 2018 - comment switch	kembali ke style form lama	
					// $opsi_ijin = (int)$this->input->post('opsi_ijin_penelitian');
					// switch ($opsi_ijin) {
					// 	case 1: #hanya ijin ke instansi tempat penelitian
							
					// 		break;
					// 	case 2: #hanya pengantar rekomendasi penelitian dalam propinsi
					// 		$form_ke_umum = [
					// 			'PERIHAL' => 'Permohonan Surat Rekomendasi Penelitian',
					// 			'PENERIMA_EKS' => ['Gubernur Daerah Istimewa Yogyakarta\r\n c.q Kepala BAKESBANGLINMAS DIY#0']
					// 		];

					// 		break;
					// 	case 3: #hanya pengantar rekomendasi penelitian luar propinsi
								
					// 		break;
					// 	case 4: #ijin ke instansi tempat penelitin + disertai pengantar rekomendasi penelitian dalam propinsi
					// 		$form_ke_umum = [
					// 			//'ISI_SURAT' => $this->input->post('keperluan'),
					// 			'PENERIMA_EKS' => [
					// 				$this->input->post('kepada_penerima_eks').'#0', 
					// 				$this->input->post('tmpt_penelitian').'#0'
					// 			],								
					// 			'TEMPAT_TUJUAN' => $this->input->post('alamat_tujuan')
					// 		];

					// 		break;
					// 	case 5: #ijin ke instansi tempat penelitin + disertai pengantar rekomendasi penelitian luar propinsi
								
					// 		break;						
						
					// 	default:
					// 		$data['errors'][] = 'opsi ijin penelitian tidak dikenali';
					// 		break;
					// }					
					#memetakan/assign variabel-variabel $_POST hasil inputan form ke bentuk array-nya grup_automasi

					$form_ke_umum = [
						'ISI_SURAT' => $this->input->post('keperluan'),
						'PENERIMA_EKS' => [
							$this->input->post('kepada_penerima_eks').' '.$this->input->post('instansi_tujuan').'#0'
						],
						//'INSTANSI_TUJUAN' => $this->input->post('intansi_tujuan'),
						'TEMPAT_TUJUAN' => $this->input->post('tempat_tujuan')
					];
					$form_ke_grup = [
						'KEPERLUAN' => $sess_detail['KEPERLUAN'],
						'JUDUL_DLM_KEGIATAN' => $this->input->post('judul_penelitian'),						
						'METODE_DLM_KEGIATAN' => $this->input->post('mtd_penelitian'),
						'TEMPAT_KEGIATAN' => $this->input->post('instansi_tujuan'),
						'TGL_MULAI' => $this->input->post('tgl_mulai'),
						'TGL_BERAKHIR' => $this->input->post('tgl_berakhir'),
						'CONFIG_MASA_BERLAKU' => 'P1M', //1 BULAN
						'MATA_KULIAH_TERKAIT' => $sess_detail['MATA_KULIAH_TERKAIT']
					];

				break;

				case 8: #ijin observasi
					$kpd_mahasiswa 	= explode("<$>", $this->input->post('kpd_mahasiswa'));
					$config = array(
						array('field' => 'pilih_makul','label' => 'Pilih Mata Kuliah','rules' => 'required'),
						array('field' => 'kepada_penerima_eks','label' => 'Kepada Yth.','rules' => 'required'),
				        array('field' => 'instansi_tujuan','label' => 'Instansi Tujuan','rules' => 'required')
				    );

					// $arr_kpd_mhs = explode("<$>", $this->input->post('kpd_mahasiswa'));
					// $kpd_mhs = [];
					// foreach ($arr_kpd_mhs as $key => $val) {
					// 	$val = explode('#', $val);
					// 	$kpd_mhs[$key] = $val[0].'#'.$val[1];
					// }
					// $kpd_mhs = implode('<$>', $kpd_mhs);
					//exit(var_dump($kpd_mhs));

					$form_ke_umum = [
						'ISI_SURAT' => $sess_detail['KEPERLUAN'].' ke '.$this->input->post('instansi_tujuan').' terkait '.$this->input->post('topik_observasi'),
						'PENERIMA_EKS' => [
							$this->input->post('kepada_penerima_eks').' '.$this->input->post('instansi_tujuan').'#0'
						],
						'PENERIMA_MHS' => $this->input->post('kpd_mahasiswa'),//$kpd_mhs
						'TEMPAT_TUJUAN' => $this->input->post('alamat_tujuan')
					];
				    $form_ke_grup = [
				    	'KEPERLUAN' => $sess_detail['KEPERLUAN'],
						'MATA_KULIAH_TERKAIT' => $this->input->post('pilih_makul'),
						'TEMPAT_KEGIATAN' => $this->input->post('instansi_tujuan'),
						'TEMA_DLM_KEGIATAN' => $this->input->post('topik_observasi'),
						'TGL_MULAI' => $this->input->post('tgl_mulai'),
						'TGL_BERAKHIR' => $this->input->post('tgl_mulai'),
						'CONFIG_MASA_BERLAKU' => 'P1M' //1 BULAN
						//'MATA_KULIAH_TERKAIT' => null
					];

				break;

				case 9: #permohonan KP
					$config = array(
						array('field' => 'kepada_penerima_eks','label' => 'Kepada Yth.','rules' => 'required'),
				        array('field' => 'instansi_tujuan','label' => 'Instansi Tujuan','rules' => 'required'),
				         array('field' => 'minat','label' => 'Bidang Minat','rules' => 'required'),
				        array('field' => 'tgl_mulai','label' => 'Tanggal Mulai','rules' => 'required'),
				        array('field' => 'tgl_berakhir','label' => 'Tanggal Berakhir','rules' => 'required'),
				    );
					$form_ke_umum = [

						'ISI_SURAT' => $sess_detail['KEPERLUAN'],
						'PENERIMA_EKS' => [
							$this->input->post('kepada_penerima_eks').' '.$this->input->post('instansi_tujuan').'#0'
						],
						// 'PENERIMA_MHS' => $this->input->post('kpd_mahasiswa'),//$kpd_mhs
						'TEMPAT_TUJUAN' => $this->input->post('tempat_tujuan')
					];

				    $form_ke_grup = [
				    	'KEPERLUAN' => $sess_detail['KEPERLUAN'],
						'MATA_KULIAH_TERKAIT' => $sess_detail['MATA_KULIAH_TERKAIT'],
						'TEMPAT_KEGIATAN' => $this->input->post('instansi_tujuan'),
						'TEMA_DLM_KEGIATAN' => $this->input->post('minat'),
						'TGL_MULAI' => $this->input->post('tgl_mulai'),
						'TGL_BERAKHIR' => $this->input->post('tgl_mulai'),
						'CONFIG_MASA_BERLAKU' => 'P3M' //1 BULAN
					];
				break;

				case 10: #studi pendahuluan
					$config = array(
						array('field' => 'tema','label' => 'Tema','rules' => 'required'),
				        array('field' => 'instansi_tujuan','label' => 'Instansi Tujuan','rules' => 'required'),
				        array('field' => 'kepada_penerima_eks','label' => 'Kepada Yth.','rules' => 'required')
					);
					$form_ke_umum = [
						'ISI_SURAT' => $this->input->post('keperluan'),
						'PENERIMA_EKS' => [
							$this->input->post('kepada_penerima_eks').' '.$this->input->post('instansi_tujuan').'#0'
						],
						'TEMPAT_TUJUAN' => $this->input->post('tempat_tujuan')
					];

					#memetakan/assign variabel-variabel $_POST hasil inputan form ke bentuk array-nya grup_automasi
					$form_ke_grup = [
						'KEPERLUAN' => $sess_detail['KEPERLUAN'],
						'TEMA_DLM_KEGIATAN' => $this->input->post('tema'),
						'TEMPAT_KEGIATAN' => $this->input->post('instansi_tujuan'),
						'TGL_MULAI' => $this->input->post('tgl_mulai'),
						'TGL_BERAKHIR' => $this->input->post('tgl_mulai'),
						'CONFIG_MASA_BERLAKU' => 'P1M' //1 BULAN
						//'MATA_KULIAH_TERKAIT' => null
					];

				break;

				case 11: #ijin penelitian makul
					$kpd_mahasiswa 	= explode("<$>", $this->input->post('kpd_mahasiswa'));
					$config = array(
						array('field' => 'pilih_makul','label' => 'Pilih Mata Kuliah','rules' => 'required'),
						array('field' => 'kepada_penerima_eks','label' => 'Kepada Yth.','rules' => 'required'),
				        array('field' => 'instansi_tujuan','label' => 'Instansi Tujuan','rules' => 'required'),
				        array('field' => 'tgl_mulai','label' => 'Tanggal mulai','rules' => 'required'),
				    );

					$form_ke_umum = [
						//'ISI_SURAT' => $sess_detail['KEPERLUAN'].' ke '.$this->input->post('instansi_tujuan').' terkait '.$this->input->post('topik_penelitian_makul'),
						'ISI_SURAT' =>	'DISIMPAN DI SISTEM AUTOMASI SURAT',
						'PENERIMA_EKS' => [
							$this->input->post('kepada_penerima_eks').' '.$this->input->post('instansi_tujuan').'#0'
						],
						'PENERIMA_MHS' => $this->input->post('kpd_mahasiswa'),//$kpd_mhs
						'TEMPAT_TUJUAN' => $this->input->post('alamat_tujuan')
					];
				    $form_ke_grup = [
				    	//'KEPERLUAN' => $sess_detail['KEPERLUAN'],
						'MATA_KULIAH_TERKAIT' => $this->input->post('pilih_makul'),
						'TEMPAT_KEGIATAN' => $this->input->post('instansi_tujuan'),
						'TEMA_DLM_KEGIATAN' => $this->input->post('topik_penelitian_makul'),
						'TGL_MULAI' => $this->input->post('tgl_mulai'),
						'TGL_BERAKHIR' => null,
						'CONFIG_MASA_BERLAKU' => 'P1M' //1 BULAN
						//'MATA_KULIAH_TERKAIT' => null
					];
				break;

				case 12: #permohonan rekomendasi penelitian
				 	$config = array(
				 	// 	array('field' => 'lembaga_tujuan_surat','label' => 'Nama lembaga','rules' => 'required'), 
						array('field' => 'judul_penelitian','label' => 'Judul Penelitian','rules' => 'required'), 
				  //       array('field' => 'mtd_penelitian','label' => 'Metode Penelitian','rules' => 'required'),				        
				  //       array('field' => 'mtd_penelitian','label' => 'Metode Penelitian','rules' => 'required'),
				  //       array('field' => 'tgl_mulai','label' => 'Tanggal mulai penelitian','rules' => 'required'),
				  //       array('field' => 'tgl_berakhir','label' => 'Tanggal berakhir penelitian','rules' => 'required')
					);

					$form_ke_umum = [
						'PERIHAL' => 'Permohonan Surat Rekomendasi Penelitian',
						// 'PENERIMA_EKS' => $this->input->post('pimpinan_lembaga_tujuan_surat').'c.q'.$this->input->post('lembaga_tujuan_surat'),
						'PENERIMA_EKS' => [
							$this->input->post('pejabat_tujuan_surat').'\r\n'.$this->input->post('pejabat_tujuan_surat_spesifik').'#0'
						],
						'TEMPAT_TUJUAN' => $this->input->post('alamat_lembaga_tujuan_surat')
					];
					$form_ke_grup = [
						'KEPERLUAN' => 'kelengkapan penyusunan skripsi',
						'NM_KEGIATAN' => 'Penelitian/Riset',
						'JUDUL_DLM_KEGIATAN' => $this->input->post('judul_penelitian'),						
						'METODE_DLM_KEGIATAN' => $this->input->post('mtd_penelitian'),
						'TEMPAT_KEGIATAN' => $this->input->post('tmpt_penelitian'),
						'TGL_MULAI' => $this->input->post('tgl_mulai'),
						'TGL_BERAKHIR' => $this->input->post('tgl_berakhir'),
						'CONFIG_MASA_BERLAKU' => 'P1M', //1 BULAN
						//'MATA_KULIAH_TERKAIT' => $sess_detail['MATA_KULIAH_TERKAIT']
					];
				 break;
				default:
					//balikkan error
					$data['errors'][] = 'jenis surat yang diinputkan tidak dikenali';
					break;
			}
			$this->form_validation->set_message('required', 'kolom {field} wajib diisi');
			$this->form_validation->set_rules($config);
			$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
			if ($this->form_validation->run()){#pastikan form terisi semua
				#3. SET DATA SESI
				$dt = new DateTime();				
				$data_umum = array(
					'ID_SESI' => null, //sementara di-null
	                'KD_JENIS_SAKAD' => (int)$kd_jenis_surat_int,
	                //'KD_GRUP_JENIS_SAKAD' => $grup_jenis_sakad,
	                'KD_JENIS_SURAT' => (int)$sess_detail['KD_JENIS_SURAT'], #jenis surat induk
	                'PEMBUAT_SURAT'	=> $this->session->userdata('username'),
	                'KD_JENIS_ORANG'		=> "M", //mahasiswa
	                // 'TGL_SURAT' => $this->datetime_koreksi('Y-m-d'),
	                'TGL_SURAT' => date('Y-m-d'),
	                'TEMPAT_DIBUAT'		=> "34712", //kodya yogyakarta
	                'KD_STATUS_SIMPAN'	=> "1", //DRAF TANPA NOMOR
	                'KD_SISTEM'			=> "AUTOMASI",
	                'WAKTU_SIMPAN_AWAL' => $dt->format('Y-m-d H:i:s')
		       	);
		       	$data_automasi = $this->Automasi_model->get_config_automasi($kd_jenis_surat_int, $sess_mhs['UNIT_ID']);
		       	$data['conf_automasi'] = $data_automasi[0];
		       	$data['detail_skr_automasi'] = array_merge($sess_detail, $form_ke_grup);
		       	$data_sesi = array_merge($data_umum, $data_automasi[0]);

		       	//modifikasi kolom yg tidak dibutuhkan tabel SESI_BUAT_SAKAD
		       	$data_sesi['PERIHAL'] = isset($data_sesi['PERIHAL']) ? ucwords(strtolower($data_sesi['PERIHAL'])).' '.$sess_mhs['NAMA'] : NULL;
				$data_sesi =  array_diff_key($data_sesi, array_flip(['ID', 'FRASA_BUKA', 'FRASA_TUTUP', 'WAKTU_SIMPAN']));
				$data_sesi['DETAIL_SURAT'] = isset($data['detail_skr_automasi']) ? serialize($data['detail_skr_automasi']) : null;

				#3.2 SET DATA SESI - PENERIMA SURAT
				if (!empty($form_ke_umum['PENERIMA_MHS'])){ #cek adakah mahasiswa diinputkan lewat form
					$arr_mhs = explode("<$>", $form_ke_umum['PENERIMA_MHS']);

					$data_sesi['DETAIL_MHS'] = [];
					$data_sesi['D_PENERIMA_MHS'] = [];
					foreach ($arr_mhs as $key => $val) {
						$val = explode('#', $val);

						$data_sesi['D_PENERIMA_MHS'][$key] = $val[0].'#'.$val[1];

						$data_sesi['DETAIL_MHS'][$key]['NIM'] = $val[0];
						$data_sesi['DETAIL_MHS'][$key]['NAMA'] = $val[4];
					}
					$data_sesi['D_PENERIMA_MHS'] = implode('<$>', $data_sesi['D_PENERIMA_MHS']);
					$data_sesi['DETAIL_MHS'] = serialize($data_sesi['DETAIL_MHS']);
				} else {
					$data_sesi['D_PENERIMA_MHS'] = $sess_mhs['NIM'].'#0';
					$data_sesi['DETAIL_MHS'] = isset($sess_mhs) ? serialize($sess_mhs) : null;
				}


				if (isset($form_ke_umum['PENERIMA_EKS'])){
					if (is_array($form_ke_umum['PENERIMA_EKS'])){#lebih dari satu
						$data_sesi['D_PENERIMA_EKS'] =  implode('<$>', $form_ke_umum['PENERIMA_EKS']);
					} else {
						$data_sesi['D_PENERIMA_EKS'] = $form_ke_umum['PENERIMA_EKS'];
					}
				}
				

				#3.3 SET DATA SESI - UMUM SURAT
				$data_sesi['TEMPAT_TUJUAN'] = isset($form_ke_umum['TEMPAT_TUJUAN']) ? $form_ke_umum['TEMPAT_TUJUAN'] : null;
				$data_sesi['ISI_SURAT'] = isset($form_ke_umum['ISI_SURAT']) ? $form_ke_umum['ISI_SURAT'] : 'isi surat tersimpan di sistem automasi';


				#4. ASSIGN DAN KIRIM KE API INSERT
				$data['insert_surat_keluar'] = $data_sesi;
				$id_sesi = $this->input->post('id_sesi_buat');
				if ($id_sesi == 'new' OR $id_sesi == '' OR $id_sesi == null){ #INSERT
					$data['insert_surat_keluar']['ID_SESI'] = 'SB'.uniqid();
					$save_sesi = $this->Automasi_model->simpan_sesi_automasi_buat_v2($data['insert_surat_keluar']);
					if ($save_sesi['status'] == TRUE || $save_sesi['status'] == 1 || $save_sesi['status'] == '1'){
						$data['pesan'] = $save_sesi['success_message'];
						$data['sesi_cetak'] = $save_sesi['data'];
						$response = $this->load->view('content/v_cetak',$data, true);
						$this->output
							->set_status_header(200)
							->set_header('X-ID-SESI: '.$data['sesi_cetak']['ID_SESI'])
							->set_content_type('text/html')
							->set_output($response);
					} else { #JIKA INSERT GAGAL KARENA KESALAHAN DI SERVICE
						$this->output
							->set_status_header(501) //gagal simpan karena kesalahan program di server
							->set_content_type('text/html')
							->set_output(print_r($save_sesi));
					}
				}	elseif (!empty($id_sesi)){ #UPDATE. asumsi : memiliki id sesi yang benar
					$update_sesi = $this->Automasi_model->perbarui_sesi_automasi_buat($id_sesi, $data['insert_surat_keluar']);

					if ($update_sesi['status'] == TRUE || $update_sesi['status'] == 1 || $update_sesi['status'] == '1'){ #RESPONSE HTML JIKA INSERT BERHASIL
						$data['pesan'] = $update_sesi['success_message'];
			       		$data['sesi_cetak'] = $update_sesi['data'];

			       		$response = $this->load->view('content/v_cetak',$data, true);
			       		$this->output
			       			->set_status_header(200)
			       			->set_header('X-ID-SESI: '.$data['sesi_cetak']['ID_SESI'])
			       			->set_content_type('text/html')
			       			->set_output($response);
			       	} else { #JIKA UPDATE GAGAL KARENA KESALAHAN DI SERVICE
			       		$this->output
			       			->set_status_header(501) //gagal simpan karena kesalahan program di server
			       			->set_content_type('text/html')
			       			->set_output(print_r($update_sesi));
			       	}
				}
				##************ END SIMPAN SESI ******************##
			} else {
				$data['form_validation'] = [];
			 	$data['form_validation']['success'] = false;
			 	$data['form_validation']['messages'] = [];
			 	foreach ($_POST as $key => $value) {
			 		$data['form_validation']['messages'][$key] = form_error($key);
			 	}
			 	$this->output->set_status_header(422)
			 		->set_content_type('application/json')
			 		->set_output(json_encode($data['form_validation']));
			}
    	} else {
    		$this->output->set_content_type('application/json')
	       			->set_output(json_encode(array('status' => false,'message'=>'Unauthorized')));
    	}
	}
       

    //ACTION
    /**
    * Penerbitan Surat
    * request data : $_POST['id_sesi_buat_cetak'], $_POST['save_final_cetak'], $_POST['btn_preview_skr_bernomor'] atau $_POST['btn_save_skr_bernomor']
    * 1) Simpan dan penomoran 2) Kirim ke penerima-penerima 3) generate pdf (atau debug)
    * @return filestream PDF|html
    */
    public function terbitkan_surat($mode='pdf') # a) preview (tanpa simpan langsung cetak), b) bernomor (simpan bernomor langsung cetak) c) simpan_saja d)kirim_perm_ttd
    {
    	//$this->load->model('Api_simpeg');
    	//$this->load->model('Repo_SIA');
    	$this->load->model('common/Api_simpeg');
    	$this->load->model('common/Api_sia');
    	$this->load->model('Repo_Automasi_Surat');    	

    	# SKEMA CETAK BERNOMOR BIASA (TANDA TANGAN BASAH MELALUI PENGAJUAN MANUAL)
    	if (isset($_POST['btn_save_skr_bernomor']) || isset($_POST['btn_preview_skr_bernomor'])){ #Sementara disimpan sbagai draf (testing)
    		$id_sesi_buat = $this->input->post('id_sesi_buat_cetak'); 
    		$save_final = $this->input->post('save_final_cetak');
    		if (isset($_POST['btn_preview_skr_bernomor'])){
	    		$opsi = 'preview';
	    		$data['opsi'] = $opsi;
	    	} elseif (isset($_POST['btn_save_skr_bernomor'])) {
	    		$opsi = 'bernomor';
	    		$data['opsi'] = $opsi;
	    	}
	    	
    		if (!empty($id_sesi_buat) && $save_final == 'ok'){
    			#1. Mengambil data umum dan penerima surat dari sesi
    			$get_sesi = $this->Automasi_model->sesi_by_id($id_sesi_buat); #mapping ke semua API INSERT
    			;
    			$data['isi_mhs'] = ''; //assign data ke variabel lain
    			if ($get_sesi == FALSE || $get_sesi == NULL){
    				exit('Mohon maaf pembuatan surat tidak dapat diselesaikan karena data sesi tidak ditemukan');
    			} else {
    				//assign semua kolom data_sesi ke array $data_sk
					$data_sk = $get_sesi;	
    			}
    			
				#2. Modif kolom-kolom untuk proses data
				$data_sk['ID_SURAT_KELUAR'] = uniqid().'p';
				$data_sk['ID_PSD'] = $get_sesi['PSD_ID'];
				$data_sk['ATAS_NAMA'] = $get_sesi['PSD_AN'];
				$data_sk['UNTUK_BELIAU'] = $get_sesi['PSD_UB'];
				$data_sk['STATUS_SK'] = 0;
				$data_sk['WAKTU_SIMPAN'] = date("d/m/Y H:i:s");
				// $data_sk['WAKTU_SIMPAN'] = $this->datetime_koreksi('d/m/Y H:i:s');
				$data_sk['TGL_SURAT'] = DateTime::createFromFormat('Y-m-d', $data_sk['TGL_SURAT'])->format('d/m/Y'); //settingan waktu servernya keliru
				// $data_sk['TGL_SURAT'] = $this->datetime_koreksi('d/m/Y');


				if ($opsi == 'preview'){
					$data_sk['KD_STATUS_SIMPAN'] =  '2';

					if($data_sk['KD_STATUS_SIMPAN'] == '2' || $data_sk['KD_STATUS_SIMPAN'] == 2){ #2 BERNOMOR						
						$data_sk['NO_URUT']		= null;
						$data_sk['NO_SELA']		= null;
						$data_sk['NO_SURAT']	= 'belum diset';

					} else { #1 (draf)
						$data_sk['NO_URUT']		= '';
						$data_sk['NO_SELA']		= '';
						$data_sk['NO_SURAT']	= 'belum diset';
					}
				}

				if ($opsi == 'bernomor') {
					$data_sk['KD_STATUS_SIMPAN'] = '2';
					
					#SET PENOMORAN
					$parameter	= array('api_kode' => 90002, 'api_subkode' => 3, 'api_search' => array($data_sk));
					$req_no 	= $this->apiconn->api_tnde_v3('tnde_surat_keluar/penomoran', 'json', 'POST', $parameter);

					if ($data_sk['KD_STATUS_SIMPAN'] == '2' || $data_sk['KD_STATUS_SIMPAN'] == 2) {
						#beri nomor tapi jangan set no_urut dan nomor_sela
						if(empty($data['errors'])){							

							$array_penomoran_surat_aktif = [11]; #produksi
							$kd_jenis_sakad_int = (int)$get_sesi['KD_JENIS_SAKAD'];
							if (in_array($kd_jenis_sakad_int, $array_penomoran_surat_aktif)){

								if(empty($req_no['ERRORS'])){
									$data_sk['NO_URUT']	= (!empty($req_no['NO_URUT']) ? $req_no['NO_URUT'] : 0);
									$data_sk['NO_SELA']		= (!empty($req_no['NO_SELA']) ? $req_no['NO_SELA'] : 0);
									$data_sk['NO_SURAT']	= (!empty($req_no['NO_SURAT']) ? $req_no['NO_SURAT'] : 0);
								}else{
									$save_sk = FALSE;
									$data['errors'] = $req_no['ERRORS'];
								}
								
								$data_sk['KD_STATUS_SIMPAN'] = '2';
							} else {
								if(empty($req_no['ERRORS'])){
									$data_sk['NO_URUT']	= '';
									$data_sk['NO_SELA']		= '';
									$data_sk['NO_SURAT']	= (!empty($req_no['NO_SURAT']) ? $req_no['NO_SURAT'] : '');
								}else{
									$save_sk = FALSE;
									$data['errors'] = $req_no['ERRORS'];
								}
								#ubah lagi KD_STATUS_SIMPAN KE 1
								$data_sk['KD_STATUS_SIMPAN'] = '1';
							}
							
						}
					} else { #1 (draf)
						if(empty($req_no['ERRORS'])){
							$data_sk['NO_URUT']	= ''; #DIKOSONGKAN SELAMA TESTING OR DEV
							$data_sk['NO_SELA']		= ''; #DIKOSONGKAN SELAMA TESTING OR DEV
							$data_sk['NO_SURAT']	= (!empty($req_no['NO_SURAT']) ? $req_no['NO_SURAT'] : 0);
							// $data_sk['NO_SURAT']	= '';
						}else{
							$save_sk = FALSE;
							$data['errors'] = $req_no['ERRORS'];
						}						
						
					}
					
					#ubah lagi KD_STATUS_SIMPAN KE 1 (SETELAH DAPAT NOMOR, HEHEHE)
					#NONAKTIF DULU // $data_sk['KD_STATUS_SIMPAN'] = '1';
				}

				#SELEKSI KOLOM YANG MERUPAKAN ELEMEN UMUM SURAT	KELUAR
				$key_diikutkan = ['ID_SURAT_KELUAR', 'KD_STATUS_SIMPAN', 'KD_JENIS_SURAT', 'ID_PSD', 'ATAS_NAMA', 'UNTUK_BELIAU', 'ID_KLASIFIKASI_SURAT', 'TGL_SURAT', 'STATUS_SK', 'PEMBUAT_SURAT', 'JABATAN_PEMBUAT', 'KD_JENIS_ORANG','UNIT_ID', 'UNTUK_UNIT', 'PERIHAL', 'TEMPAT_DIBUAT', 'TEMPAT_TUJUAN', 'KOTA_TUJUAN', 'ALAMAT_TUJUAN', 'LAMPIRAN', 'KD_SIFAT_SURAT', 'KD_KEAMANAN_SURAT', 'WAKTU_SIMPAN', 'KD_SISTEM', 'NO_URUT', 'NO_SELA', 'ISI_SURAT', 'KD_PRODI', 'NO_SURAT'];			
				$data_sk = array_intersect_key($data_sk, array_flip($key_diikutkan));


				#2. Melakukan API Insert ke SISURAT	(surat_keluar)
				if ($opsi == 'preview'){
					$data['surat_keluar']['umum'] = $data_sk; //assign data ke variabel lain
					$save_sk = TRUE;
				} elseif ($opsi == 'bernomor') {					
					$save_sk = TRUE;

					if($save_sk == TRUE){
						$parameter	= array('api_kode' => 90001, 'api_subkode' => 1, 'api_search' => array($data_sk));
						$save_sk 		= $this->apiconn->api_tnde('tnde_surat_keluar/insert_surat_kaluar', 'json', 'POST', $parameter);

						//exit(var_dump($data_sk));
						if($save_sk	==	FALSE || $save_sk == NULL){
							$data['errors'][] = "Gagal menyimpan surat. #log: SKR001";
							$data['surat_keluar']['umum'] = FALSE;
						}else{
							$save_sk	=	TRUE;
							$data['surat_keluar']['umum'] = $data_sk;
						}
					}
				}

				if (isset($data['errors'])){
					exit(json_encode($data));
				}



			
    			#3. ======== Melakukan API INSERT PENERIMA_SK KE SISURAT ========
    			$kpd_mahasiswa = (!empty($get_sesi['D_PENERIMA_MHS']) ? explode('<$>', $get_sesi['D_PENERIMA_MHS']) : null);
    			$kpd_pejabat = (!empty($get_sesi['D_PENERIMA_PJB']) ? explode('<$>', $get_sesi['D_PENERIMA_PJB']) : null);
    			$kpd_pegawai = (!empty($get_sesi['D_PENERIMA_PGW']) ? explode('<$>', $get_sesi['D_PENERIMA_PGW']) : null);
    			$ts_pejabat = (!empty($get_sesi['D_TEMBUSAN_PJB']) ? explode('<$>', $get_sesi['D_TEMBUSAN_PJB']) : null);
    			$ts_eks = (!empty($get_sesi['D_TEMBUSAN_EKS']) ? explode('<$>', $get_sesi['D_TEMBUSAN_EKS']) : null);
    			# 2. API SISURAT - INSERT PENERIMA SURAT KELUAR			
				if ($save_sk == TRUE){
	    		#============== 3.1 Pengelola Surat (PS)==============
	    			
	    			# 3.1 KIRIM SURAT KE PEJABAT (Yang menerangkan, menandatangani, dsb)
	    			if(!empty($kpd_pejabat[0])){
	    				$save_pjb = [];
						foreach($kpd_pejabat as $key=> $val){
							$exp = explode("#", $val);
							$get_pjb = $this->Api_simpeg->get_pejabat_aktif($data['surat_keluar']['umum']['TGL_SURAT'], $exp[0]);	
							if(count($exp) == 2){
								$pjb['ID_SURAT_KELUAR']				= $data_sk['ID_SURAT_KELUAR'];
								$pjb['PENERIMA_SURAT'] = $get_pjb[0]['NIP']; //NIP PEGAWAI
								$pjb['JABATAN_PENERIMA'] = $exp[0];
								$pjb['KD_GRUP_TUJUAN'] = 'PJB01';
								$pjb['KD_STATUS_DISTRIBUSI'] = "PS";
								$pjb['ID_STATUS_SM'] = 1;
								$pjb['KD_JENIS_TEMBUSAN'] = 0;
								$pjb['NO_URUT'] = $key + 1;
								$pjb['KD_JENIS_KEPADA']				= $exp[1];

								if ($opsi == 'preview'){
									$save_pjb[] = $pjb;	
								} elseif ($opsi == 'bernomor') {
									$savepjb = $this->apiconn->api_tnde('tnde_surat_keluar/insert_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($pjb)));
									if($savepjb == FALSE){
										$data['errors'][] = "Gagal mengirim ke tujuan surat ke ".$pjb['PENERIMA_SURAT'];
										$save_pjb[] = FALSE;	
									}elseif ($savepjb == TRUE) {
										$save_pjb[] = $pjb;	
									}
								}
							#....							
							}
						}

						$data['surat_keluar']['penerima']['PS'] = $save_pjb;
	    			}

	    			#CONTOH.....
	    			$kpd_entitas_penerima = null;
	    			if (!empty($kpd_entitas_penerima)){
	    				$this->kirim_surat(
	    					'pjb', 'testing',
	    					$data['surat_keluar']['umum'],
	    					$kpd_entitas_penerima
	    				);
	    			}

	    			# 3.2 KIRIM SURAT KE MAHASISWA
	    			if(!empty($kpd_mahasiswa[0])){
	    				$save_mhs = [];
						foreach($kpd_mahasiswa as $key=> $val){
							$exp = explode("#", $val);
							if(count($exp) == 2){
								$mhs['ID_SURAT_KELUAR']				= $data_sk['ID_SURAT_KELUAR'];
								$mhs['PENERIMA_SURAT'] = $exp[0];
								$mhs['JABATAN_PENERIMA'] = '0';
								$mhs['KD_GRUP_TUJUAN'] = 'MHS01';
								$mhs['KD_STATUS_DISTRIBUSI'] = "PS";
								$mhs['ID_STATUS_SM'] = 1;
								$mhs['KD_JENIS_TEMBUSAN'] = 0;
								$mhs['NO_URUT'] = $key + 1;
								$mhs['KD_JENIS_KEPADA']				= $exp[1];
								

								if ($opsi == 'preview'){
									$save_mhs[] = $mhs;
									//array_push($data['surat_keluar']['penerima']['PS'], $mhs);
								} elseif ($opsi == 'bernomor') {
									$savemhs = $this->apiconn->api_tnde('tnde_surat_keluar/insert_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($mhs)));

									if($savemhs == FALSE || $savemhs == NULL){
										$data['errors'][] = "Gagal mengirim ke tujuan surat ke ".$exp[0];
										$save_mhs[] = FALSE;
									}elseif ($savemhs == TRUE) {
										$save_mhs[] = $mhs;
										$data['success'][] = "Berhasil mengirim ke tujuan surat ke ".$exp[0];
									}
								}
								/*$save_mhs = $this->apiconn->get_api('tnde_surat_keluar/insert_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($mhs)));
								if($save_mhs == FALSE){
									$data['errors'][] = "Gagal mengirim ke tujuan surat ke ".$exp[0]; // Mahasiswa
								}*/
							}
						}

						// $data['surat_keluar']['penerima']['PS'] = $save_mhs;		
						$data['surat_keluar']['penerima']['PS'] = array_merge($data['surat_keluar']['penerima']['PS'], $save_mhs);
						$getmhs = $this->Api_sia->get_profil_mhs($save_mhs[0]['PENERIMA_SURAT'],'kumulatif');
						//exit(var_dump($getmhs));
						$data['prodi'] = $getmhs[0]['NM_PRODI'];
					}

					# 3.3 KIRIM SURAT KE EKSTERNAL
					$kpd_eks = (!empty($get_sesi['D_PENERIMA_EKS']) ? explode('<$>', $get_sesi['D_PENERIMA_EKS']) : null);
					if(!empty($kpd_eks[0])){
						$save_eks = [];					
						foreach($kpd_eks as $key => $val){
							$exp = explode("#", $val);

							if(count($exp) == 2){
								$eks['ID_SURAT_KELUAR']				= $data_sk['ID_SURAT_KELUAR'];
								$eks['PENERIMA_SURAT']				= $exp[0];
								$eks['JABATAN_PENERIMA']			= $exp[0]; //misal: kepala, kabag, sekda, dsb
								$eks['KD_GRUP_TUJUAN']				= "EKS01";
								$eks['KD_STATUS_DISTRIBUSI']		= "PS";	
								$eks['ID_STATUS_SM'] = 1;
								$eks['KD_JENIS_TEMBUSAN'] = 0;
								$eks['NO_URUT'] = $key + 1;
								$eks['KD_JENIS_KEPADA']				= $exp[1]; //1 = untuk perhatian , 0
								//$save_eks[] = $eks;

								if ($opsi == 'preview'){
									$save_eks[] = $eks;
								} elseif ($opsi == 'bernomor') {
									$saveeks = $this->apiconn->api_tnde('tnde_surat_keluar/insert_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($eks)));
									if($saveeks == FALSE || $saveeks == NULL){
										$data['errors'][] = "Gagal mengirim ke tujuan surat ke ".$exp[0];
										$save_eks[] = FALSE;
									}elseif ($saveeks == TRUE) {
										$save_eks[] = $eks;	
									}
								}								
							}
						}

						// $data['distribusi_ps']['penerima_eksternal'] = $save_eks;
						$data['surat_keluar']['penerima']['PS'] = array_merge($data['surat_keluar']['penerima']['PS'], $save_eks);
						$data['tujuan_surat']['pimpinan_instansi'] = $save_eks[0]['PENERIMA_SURAT'];
						$data['tujuan_surat']['tempat_tujuan'] = $data['surat_keluar']['umum']['TEMPAT_TUJUAN'];
						//$data['tujuan_surat']['pimpinan_instansi'] = $save_eks[0]['PENERIMA_SURAT'];
					}

	    			#============== 3.2 Tembusan Surat (TS)============== 
	    			$data['surat_keluar']['penerima']['TS'] = [];   			
					if(!empty($ts_pejabat)){
						$ts_urut = 0;
						foreach($ts_pejabat as $key => $val){
							$exp = explode("#", $val);
							$get_pjb = $this->Api_simpeg->get_pejabat_aktif($data['surat_keluar']['umum']['TGL_SURAT'], $exp[0]);						
							if(count($exp) == 2 ){
								$ts_pjb['ID_SURAT_KELUAR'] 				= $data['surat_keluar']['umum']['ID_SURAT_KELUAR'];
								$ts_pjb['PENERIMA_SURAT'] 				= (!empty($get_pjb[0]['NIP']) ? $get_pjb[0]['NIP'] : $get_pjb[0]['KD_PGW']); //NIP PEGAWAI
								$ts_pjb['JABATAN_PENERIMA'] 			= $exp[0];
								$ts_pjb['KD_GRUP_TUJUAN'] 				= 'PJB01';
								$ts_pjb['KD_STATUS_DISTRIBUSI'] 	= "TS";
								$ts_pjb['ID_STATUS_SM'] 						= "1";
								$ts_pjb['NO_URUT']									= $ts_urut + 1;
								$ts_pjb['KD_JENIS_TEMBUSAN'] 		= (string)$exp[1]; #1 = sebagai laporan, 0 = biasa
								$ts_pjb['KD_JENIS_KEPADA'] 				= '0';					
								//$save_ts_pjb = $ts_pjb;

								if ($opsi == 'preview'){
									$save_ts_pjb[] = $ts_pjb;
									$data['daftar_tembusan'][] = [
											'penerima_tembusan' => $get_pjb[0]['STR_NAMA_A222'],
											'status_tembusan' => (string)$exp[1]
										];
								} elseif ($opsi == 'bernomor') {
									$savetspjb = $this->apiconn->api_tnde('tnde_surat_keluar/insert_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($ts_pjb)));
									
									if($savetspjb == FALSE || $savetspjb == NULL){
										$data['errors'][] = "Gagal mengirim tembusan ke surat ke ".$exp[0];
										exit(print_r($get_pjb));
										$save_ts_pjb[] = FALSE;

									}elseif ($savetspjb == TRUE || $savetspjb == '1') {
										$save_ts_pjb[] = $ts_pjb;
										$data['daftar_tembusan'][] = [
											'penerima_tembusan' => $get_pjb[0]['STR_NAMA_A222'],
											'status_tembusan' => (string)$exp[1]
										];
									}

								}								
							}
						}

						// $data['distribusi_ts']['tembusan_pejabat'] = $save_ts_pjb;
						$data['surat_keluar']['penerima']['TS'] = array_merge($data['surat_keluar']['penerima']['TS'], $save_ts_pjb);

					}
				}

			## 4. Melakukan API Insert ke AUTOMASI  				
				#======= 4.1 SIMPAN DATA BERDASARKAN JENIS SURATNYA ====
				$detail_surat_sesi = unserialize($get_sesi['DETAIL_SURAT']);
				
				$tgl_surat_obj = DateTime::createFromFormat('d/m/Y', $data_sk['TGL_SURAT']);
				 //transform string tanggal jadi object tanggal php

				$detail_skr_automasi = $detail_surat_sesi;				
				$tgl_bmb = isset($detail_surat_sesi['CONFIG_MASA_BERLAKU']) ? $tgl_surat_obj->add(new DateInterval($detail_surat_sesi['CONFIG_MASA_BERLAKU'])) : null;
				$detail_skr_automasi['BATAS_MASA_BERLAKU'] = isset($tgl_bmb) ?  $tgl_bmb->format('Y-m-d') : null;

				$detail_skr_automasi = array_diff_key($detail_skr_automasi, array_flip(['CONFIG_MASA_BERLAKU'])); //hapus kolom tidak dibutuhkan

				$tgl_surat_obj = DateTime::createFromFormat('d/m/Y', $data_sk['TGL_SURAT']);
				$header_skr_automasi = [
					'ID_SURAT_KELUAR' => $data['surat_keluar']['umum']['ID_SURAT_KELUAR'],
					'NO_SURAT' => $data['surat_keluar']['umum']['NO_SURAT'],
					// 'TGL_SURAT' => $this->datetime_koreksi('Y-m-d'),
    				'TGL_SURAT' => $tgl_surat_obj->format('Y-m-d'), #FORMAT PADA POSTGRESQL,
    				'PEMBUAT_SURAT' => $data['surat_keluar']['umum']['PEMBUAT_SURAT'], 
    				'KD_JENIS_SAKAD' => $detail_skr_automasi['KD_JENIS_SAKAD']
				];
				$data_skr_automasi = array_merge($header_skr_automasi, $detail_skr_automasi);

							
				if ($opsi == 'preview'){
					$data['detail_surat_automasi'] = $data_skr_automasi;
				}elseif ($opsi == 'bernomor'){
					if (empty($data['errors'])){
						#API INSERT DETAIL SURAT (WEBSERVICE AUTOMASI)
						$parameter	= array('api_kode'=>14004, 'api_subkode' => 1, 'api_search' => array($data_skr_automasi));
						$insert_detail_skr_automasi = $this->apiconn->api_tnde_sakad('tnde_surat_akademik/insert_detail_skr_automasi', 'json', 'POST', $parameter);

						if ($insert_detail_skr_automasi['status'] == TRUE || $insert_detail_skr_automasi['status'] == 1){
							$data['success'][] = 'Berhasil menambahkan data detail surat ke sistem automasi';
							$data['detail_surat_automasi'] = $insert_detail_skr_automasi['data'];
						} else {
							$data['detail_surat_automasi'] = FALSE;
							$data['errors'][] = 'Gagal menambahkan data detail surat ke sistem automasi';
						}
					}	
				}

				$kd_jenis_sakad_int = (int)$data['detail_surat_automasi']['KD_JENIS_SAKAD'];
				$arr_jenis_sakad = $this->Repo_Automasi_Surat->get_jenis_surat_mahasiswa('kd', $kd_jenis_sakad_int);
				$data['raw_jenis_sakad'] = $arr_jenis_sakad;
								
				## 3. UNIT
				### 3.1 Unit penerbit surat
				$parameter = array('api_kode' => 1901, 'api_subkode' => 2, 'api_search' => array($data['surat_keluar']['umum']['TGL_SURAT'], $data['surat_keluar']['umum']['UNIT_ID']));
				$data['unit_penerbit'] = $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);

				## PEGAWAI/PEJABAT PENANDATANGANAN disebut dalam surat
				$pjb_ps = explode('#', $get_sesi['D_PENERIMA_PJB']);
				$pjb_ts = explode('#', $get_sesi['D_TEMBUSAN_PJB']);

				$skr_umum = $data['surat_keluar']['umum'];
				if (!empty($skr_umum['ID_PSD'])){					
					$pgw_psd = $this->Api_simpeg->get_pejabat_aktif($skr_umum['TGL_SURAT'], $pjb_ps[0]);
					$nip_pgw = $pgw_psd[0]['NIP'];

					$parameter = array('api_kode' => 1123, 'api_subkode' => 3, 'api_search' => array($skr_umum['TGL_SURAT'], $nip_pgw));
					$pang_gol = $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
					
					$data['psd_detail']['pangkat'] = (!empty($pang_gol) ? $pang_gol[0] : []);
					$data['psd_detail']['pegawai'] = $pgw_psd;
				}
				if (!empty($skr_umum['ATAS_NAMA'])){
					$an = $this->apiconn->api_tnde('tnde_general/get_data', 'json', 'POST', array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('MD_PSD', 'ID_PSD', $skr_umum['ATAS_NAMA'])));
					if(!empty($an['KD_JABATAN'])){
						$parameter = array('api_kode' => 1121, 'api_subkode' => 9, 'api_search' => array($skr_umum['TGL_SURAT'], $an['KD_JABATAN'], 3));
						$data['psd_detail']['atas_nama'] = $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
					}			
					$data['psd_ringkas']['label_psd_an'] = $data['psd_detail']['atas_nama'][0]['STR_NAMA_S1']; //$data['label_psd_an'] = $pgw_psd_an[0]['STR_NAMA_A212'];
				}
				$data['psd_ringkas']['label_psd'] = $pgw_psd[0]['STR_NAMA_S1'];
				$data['psd_ringkas']['nm_pgw_psd'] = $pgw_psd[0]['NM_PGW_F'];
				$data['psd_ringkas']['nip_pgw_psd'] = $pgw_psd[0]['NIP'];

				## PEJABAT disebut dalam surat
				$psd_profil_pegawai = array_intersect_key($pgw_psd[0], array_flip(['NIP','NM_PGW_F','UNIT_NAMA','STR_NAMA','STR_NAMA_A212']));
				$psd_pangkat_pegawai = array_intersect_key($data['psd_detail']['pangkat'], array_flip(['HIE_GOLRU','HIE_NAMA']));
				$data['pejabat_dalam_surat'] = array_merge($psd_profil_pegawai, $psd_pangkat_pegawai);

				## MAHASISWA disebut dalam surat				
				$data['mhs_dalam_surat'] = unserialize($get_sesi['DETAIL_MHS']);
				
			## 5. GENERATE PDF				
				if (empty($data['errors'])){					
					
					if ($mode === 'pdf'){
						$this->generate_pdf_surat_v2($data);
					} elseif ($mode === 'debug') {
						exit(var_dump($data));	
					}
					//LAST, hapus sesi
					if ($opsi == 'bernomor'){
						$del_sesi = $this->Automasi_model->hapus_sesi_automasi_buat($get_sesi['ID_SESI']);
						$data['hapus_sesi_sebelumnya'] = $del_sesi;	
					}						
				} else {
					$this->output
		       			->set_status_header(200)
		       			->set_content_type('text/html')		       			
		       			->set_output(json_encode($data['errors']));
				}

								
    		} else {
    			exit("Maaf , penyimpanan surat gagal karena masalah teknis, silakan merefresh halaman untuk mengulang pembuatan surat");
    		}
    	} 
    	
    	#cetak ulang (sumber data dari remote API , bukan lagi parameter dalam bentuk arrays)
    	if (isset($_POST['print_surat_bernomor_tersimpan'])) {} #SURAT BARU SAJA DISIMPAN DAN BELUM DIAJUKAN KE TU

    	if (isset($_POST['print_surat_ttd'])) {} #CETAK SURAT YANG TELAH DITANDATANGANI SCR DIGITAL
    }

    public function ajukan_penerbitan_tte($mode='debug')
    {
    	$this->load->model('Mdl_mhs_surat_keluar');
    	$this->load->model('Mdl_sesi_pembuatan');
    	$this->load->model('Mdl_api_client_notifikasi', 'api_notif');

    	if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}
		//cek incoming request
		// exit(print_r($_POST)); //data yang diperlukan meliputi: ID_SESI dan skema
		$id_sesi = $this->input->post('id_sesi');
		$skema = $this->input->post('skema');
		$keterangan_tte = $this->input->post('ket');

		if (!empty($id_sesi) && $skema == 'tte'){
			$proses_terbitkan = $this->Mdl_mhs_surat_keluar->simpan_penerbitan_surat(
				[
					'sesi' => [
						'ID_SESI' => $id_sesi,
						'PEMBUAT' => $this->session->userdata('username')
					],
					'skema' => 'tte',
				]
			);

			if (!empty($proses_terbitkan)){
				// exit(json_encode($proses_terbitkan)); #ada error ?
				$data = $proses_terbitkan;

				//LAST, hapus sesi
				if ($data['skema_terbit'] == 'bernomor' || $data['skema_terbit'] == 'tte'){
					$del_sesi = $this->Mdl_sesi_pembuatan->hapus_sesi_automasi_buat($id_sesi);
					$data['hapus_sesi_sebelumnya'] = $del_sesi;	
				}

				#api cek detail pegawai penandatangan surat
				$psd = $this->apiconn->api_tnde('tnde_general/get_data', 'json', 'POST', array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('MD_PSD', 'ID_PSD', $data['surat_keluar']['umum']['ID_PSD'])));
				if(!empty($psd['KD_JABATAN'])){
					$parameter = array('api_kode' => 1121, 'api_subkode' => 9, 'api_search' => array($data['surat_keluar']['umum']['TGL_SURAT'], $psd['KD_JABATAN'], 3));
					$data['pegawai_psd'] = $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
				}
				$data['notif_email']['surat'] = array_merge($data['surat_keluar']['umum'], $data['detail_surat_automasi']);
				$data['notif_email']['pemohon'] = $data['surat_keluar']['mhs_dalam_surat'];
				$data['notif_email']['penandatangan']['nip']	= $data['pegawai_psd'][0]['NIP'];
				$data['notif_email']['penandatangan']['alamat_email']	= $data['pegawai_psd'][0]['KD_PGW'] . '@uin-suka.ac.id';
				$data['notif_email']['penandatangan']['nama']	= $data['pegawai_psd'][0]['NM_PGW_F'];
				$data['notif_email']['penandatangan']['jabatan']	= $data['pegawai_psd'][0]['STR_NAMA'];

				if (!empty($data['errors']))
					$tambah_ke_monitor = $this->api_notif->insert_monitor_event_surat(
						[
							"ID_SURAT" => $data['surat_keluar']['umum']['ID_SURAT_KELUAR'],
							"DIMONITOR_OLEH" => $data['surat_keluar']['umum']['PEMBUAT_SURAT'],
							"KD_KAT_MONITOR" => "TTE",
							"KETERANGAN" => $keterangan_tte
						]
					);
				$data['success']['hasil_tambah_ke_monitor'] = $tambah_ke_monitor;


				// $insert_monitor = $this->api_client_autosurat->tambah_monitor('tte',$data['monitor_surat']);				
				$response = $this->load->view('content/v_info_penerbitan_tte', $data, true);
				// $response = print_r($data['pegawai_psd']);
				// exit();
			}
			
			

		} else {
			$this->output
   			->set_status_header(200)   			
   			->set_content_type('text/html')
   			->set_output(json_encode(['error'=>'Tidak dapat memroses ke penerbitan akibat parameter form tidak lengkap']));
		}
		
		//render view akhir
		$this->output
   			->set_status_header(200)   			
   			->set_content_type('text/html')
   			->set_output($response);
    }

    public function simpan_via_model()
    {
    	$this->load->model('Mdl_mhs_surat_keluar');
    	
    	//perintahkan method untuk mengambil data sesi lalu langsung memroses penerbitan surat, controller hanya menunggu hasilnya
    	$proses_terbitkan = $this->Mdl_mhs_surat_keluar->simpan_penerbitan_surat(
    		[
	    		'sesi' => [
	    			'ID_SESI' => 'SB5b376e8bcd7f4',
	    			'PEMBUAT' => '11650021'
	    		],
    			'skema' => 'preview',
    		]
    	);
    	echo json_encode($proses_terbitkan);

    	//jika berhasil menerbitkan, ambil kolom-kolom yg diperlukan untuk tambah mmonitor surat


    }

    public function test_monitor_tte($page='index')
    {
    	$this->load->model('Mdl_api_client_notifikasi', 'api_notif');

    	if ($page == 'index'){
    		$monitor = $this->api_notif->get_monitor_event_surat(); //seharunya NULL atau [array]
    	} elseif ($page == 'tambah') {
    		$monitor = $this->api_notif->insert_monitor_event_surat(
    			[
    				"ID_SURAT" => uniqid(),
    				"DIMONITOR_OLEH" => "11650021",
    				"KD_KAT_MONITOR" => "TTE"
    			]
    		);
    	}
    	echo json_encode($monitor);


    }

    public function test_tambah_penerima()
    {
    	$this->load->model('Mdl_mhs_surat_keluar');
    	$id_surat_keluar = '5b37854cbb1fcp.XXX';

    	// $tambah_11650021 = $this->Mdl_mhs_surat_keluar->simpan_penerima_surat(
    	// 	$id_surat_keluar,
    	// 	[
    	// 		'PENERIMA_SURAT' => '11650023',
    	// 		'KD_GRUP_TUJUAN' => 'MHS01',
    	// 		'KD_STATUS_DISTRIBUSI' => 'PS',
    	// 		'NO_URUT' => '1',
    	// 	],
    	// 	'tte'
    	// );

    	$tambah_pgw_ST00217 = $this->Mdl_mhs_surat_keluar->simpan_penerima_surat(
    		$id_surat_keluar,
    		[    			
    			'KD_GRUP_TUJUAN' => 'PJB01',
    			'PENERIMA_SURAT' => '196912122000031001',
    			// 'JABATAN_PENERIMA' => 'ST000278', //dekaN saintek
    			'KD_STATUS_DISTRIBUSI' => 'TS',
    			'KD_JENIS_TEMBUSAN' => '1', //1: SEBAGAI LAPORAN
    			'NO_URUT' => '1',
    		],
    		'tte'
    	);

    	echo var_dump($tambah_pgw_ST00217);
    }

    /**
     * Menghasilkan PDF Surat 
     * perbaikan dari versi sebelumnya : 
    	- mengurangi additional request ke API (sampai hanya butuh 1 request saja)
    	- lebih semantik    
     */
    public function generate_pdf_surat_v2($paramdata=null)
    {
    	//$this->load->model('Repo_SIA');
    	$this->load->helper('pgw/Tnde_sakad');
    	$this->load->helper('common/Trans_general');
    	$this->load->model('common/Konversi_model','konversi_model');
		$this->load->library('common/Pdf_creator');


    	$elemen_pdf_surat = ['meta', 'kop', 'frasa_buka', 'isi_pejabat_psd', 'isi_mhs', 'isi_surat', 'frasa_penutup', 'psd', 'tembusan'];

    	$raw_skr_umum = $paramdata['surat_keluar']['umum'];
    	$raw_tembusan = $paramdata['surat_keluar']['penerima']['TS'];
    	$raw_pengelola = $paramdata['surat_keluar']['penerima']['PS'];

    	$raw_data_mhs = $paramdata['mhs_dalam_surat'];
    	if (count($raw_data_mhs) > 1){
			$raw_data_mhs = $raw_data_mhs;
		} else {
			$raw_data_mhs = (isset($raw_data_mhs[0]) ? $raw_data_mhs[0] : $raw_data_mhs);
		}

    	$raw_isi_surat = $paramdata['detail_surat_automasi'];
    	$raw_unit_penerbit = $paramdata['unit_penerbit'][0];
    	$raw_psd = $paramdata['psd_detail'];

    	#siapkan variabel2 pembentuk pdf
    	if (!empty($raw_unit_penerbit)){
    		$data['template']['kopsurat'] = [
				'kementrian' => 'KEMENTERIAN AGAMA REPUBLIK INDONESIA',
				'univ' => 'UNIVERSITAS ISLAM NEGERI SUNAN KALIJAGA YOGYAKARTA',
				'fkl' => (!empty($raw_unit_penerbit['UNIT_NAMA']) ? strtoupper($raw_unit_penerbit['UNIT_NAMA']) : ''),
				'almt' => (!empty($raw_unit_penerbit['UNIT_ALAMAT']) ? $raw_unit_penerbit['UNIT_ALAMAT'] : '').' '.(!empty($raw_unit_penerbit['UNIT_KDPOS']) ? $raw_unit_penerbit['UNIT_KDPOS'] : ''),
				'tlp' => (!empty($raw_unit_penerbit['UNIT_TELP1']) ? $raw_unit_penerbit['UNIT_TELP1'] : ''),
				'fax' => (!empty($raw_unit_penerbit['UNIT_FAX1']) ? $raw_unit_penerbit['UNIT_FAX1'] : ''),
				'web' => $this->konversi_model->website_unit($paramdata['surat_keluar']['umum']['UNIT_ID']),
				'kabkota' => (!empty($raw_unit_penerbit['UNIT_NMKAB2']) ? $raw_unit_penerbit['UNIT_NMKAB2'] : '')
			];
    	}

    	if (!empty($paramdata['psd_ringkas'])){
    		$data['psd'] = $paramdata['psd_ringkas'];
    	}

    	if (!empty($paramdata['pejabat_dalam_surat'])){
    		$data['pejabat_dalam_surat'] = $paramdata['pejabat_dalam_surat'];	
    	}

    	$data['judul_surat'] = strtoupper($paramdata['raw_jenis_sakad']['NM_JENIS_SAKAD']);
		$data['nomor'] = $paramdata['surat_keluar']['umum']['NO_SURAT'];
		$data['thn_akademik'] = $this->Api_sia->get_tahun_ajaran('info_ta_smt');
		$data['tgl_surat'] = tanggal_indo($paramdata['surat_keluar']['umum']['TGL_SURAT'], 'd/m/Y');
		$data['qrcode_otentikasi'] = base_url('publik/cek_surat/validasi_qrcode/').$paramdata['surat_keluar']['umum']['ID_SURAT_KELUAR'];

		##SET UP DATA PEMBENTUK PDF
		$watermark_img = file_get_contents(FCPATH.'/assets/images/watermark-preview.png');
		$opsi = (isset($paramdata['opsi']) ? $paramdata['opsi'] : null);
		if (!empty($opsi)){
			if ($opsi == 'preview'){
				$data['watermark_img'] = base64_encode($watermark_img);
			}	
		}
		
		//daftar tembusan
		if (!empty($paramdata['daftar_tembusan'])){
			$ts = 'Tembusan :
			<ol style="margin: 0; text-indent:0;">';
			foreach ($paramdata['daftar_tembusan'] as $k=>$v) {
				$ts .= '<li>';
				$ts .= $v['penerima_tembusan'];
				if ($v['status_tembusan'] == '1'){
					$ts .= ' (sebagai laporan)';
				}
				$ts .= '</li>';						
			}
			$ts .= '</ol>';
			$data['daftar_tembusan_htmlstring'] = $ts;
		}


		## CONDITIONAL CHECK CUSTOM DATA TIAP JENIS SURAT
		$kd_jenis_sakad = $paramdata['raw_jenis_sakad']['KD_JENIS_SAKAD'];
		$path = $paramdata['raw_jenis_sakad']['URLPATH'];
		switch ($path) {
			case 'masih_kul':	#1. PDF Surat Keterangan Masih Kuliah
				$data['isi_pgw'] = '';

				$raw_data_mhs = isset($raw_data_mhs[0]) ? $raw_data_mhs[0] : $raw_data_mhs;		
				$data['isi_mhs'] = [
					'nama' => $raw_data_mhs['NAMA'],
					'nim' => $raw_data_mhs['NIM'],
					'tmp_lahir' => $raw_data_mhs['TMP_LAHIR'],		
					'tgl_lahir' => display_tanggal_indo($raw_data_mhs['TGL_LAHIR'], 'd-m-Y'),
					'smt' => $raw_data_mhs['JUM_SMT'],
					'prodi' => $raw_data_mhs['NM_PRODI'],
					'fkl' => $raw_data_mhs['NM_FAK'],
				];
				$buka_frasa_isi = 'Surat keterangan ini dibuat untuk melengkapi salah satu syarat ';
				$data['isi_surat'] = $buka_frasa_isi.$raw_isi_surat['KEPERLUAN'];
				$view = 'report/v_r_masih_kul';
			break;
			case 'kel_baik': #2. PDF Surat Keterangan Berkelakuan Baik
				$raw_data_mhs = isset($raw_data_mhs[0]) ? $raw_data_mhs[0] : $raw_data_mhs;
				$data['isi_mhs'] = [
					'nama' => $raw_data_mhs['NAMA'],
					'nim' => $raw_data_mhs['NIM'],
					'tmp_lahir' => $raw_data_mhs['TMP_LAHIR'],
					'tgl_lahir' => display_tanggal_indo($raw_data_mhs['TGL_LAHIR'], 'd-m-Y'),
					'smt' => $raw_data_mhs['JUM_SMT'],
					'prodi' => $raw_data_mhs['NM_PRODI'],
					'fkl' => $raw_data_mhs['NM_FAK']
				];
				$data['isi_surat'] = [					
					'keperluan' => $raw_isi_surat["KEPERLUAN"]
				];
				$view = 'report/v_r_kel_baik';
			break;
			case 'ijin_pen_makul':
				$data['tujuan'] = [					
					'kepada' => str_replace(['\r\n', '\r', '\n'], '<br/>', $paramdata['tujuan_surat']['pimpinan_instansi']),
					'instansi' => '',
					'alamat_tempat' => $paramdata['tujuan_surat']['tempat_tujuan']
				];
				// $raw_data_mhs = isset($raw_data_mhs[0]) ? $raw_data_mhs[0] : $raw_data_mhs;
				//exit(json_encode($raw_data_mhs));
				$data['isi_mhs'] = $raw_data_mhs;	

				$makul = explode('#', $raw_isi_surat['MATA_KULIAH_TERKAIT']);
				$data['prodi'] = $paramdata['prodi'];
				$data['isi_surat'] = [
					'nama_kegiatan' => (!empty($raw_isi_surat["NAMA_KEGIATAN"]) ? $raw_isi_surat["NAMA_KEGIATAN"] : ''),
					'kode_makul' => $makul[0],
					'nama_makul' => $makul[1],
					'topik_penelitian_makul' => $raw_isi_surat["TEMA_DLM_KEGIATAN"],
					'tempat' => $raw_isi_surat["TEMPAT_KEGIATAN"],				
					'tgl_mulai' => display_tanggal_indo($raw_isi_surat["TGL_MULAI"], 'Y-m-d'),
					'tgl_berakhir'=> null,
				];
				$data['daftar_tembusan'][] = isset($pgw_psd_an) ? $pgw_psd_an[0]['STR_NAMA_S1'].' (sebagai laporan)' : '';
				$view = 'report/v_r_pen_makul';	
			break;
			case 'rek_pen': #2. PDF Surat Permohonan Rekomendasi Penelitian
				$data['tujuan'] = [					
					'kepada' => str_replace(['\r\n', '\r', '\n'], '<br/>', $paramdata['tujuan_surat']['pimpinan_instansi']),
					'instansi' => '',
					'alamat_tempat' => $paramdata['tujuan_surat']['tempat_tujuan']
				];

				$data['isi_mhs'] = (isset($raw_data_mhs[0]) ? $raw_data_mhs[0] : $raw_data_mhs);
				$data['isi_surat'] = $raw_isi_surat;
				$view = 'report/v_r_rek_pen';
				//exit(json_encode($data, JSON_PRETTY_PRINT));
			break;
			case 'ket_lulus':
				$raw_data_mhs = isset($raw_data_mhs[0]) ? $raw_data_mhs[0] : $raw_data_mhs;
				$data['isi_mhs'] = $raw_data_mhs;
				$data['isi_surat'] = $raw_isi_surat;
				if( is_array($data['isi_surat']['DATA_TAMBAHAN']) ){
					$data['isi_surat'] = $raw_isi_surat;
				} else { //masih dalam string php serialized
					$data['isi_surat']['DATA_TAMBAHAN'] = unserialize($data['isi_surat']['DATA_TAMBAHAN']);
				}
				//exit(var_dump($data));
				$view = 'report/v_r_lulus';
				break;
			case 'hbs_teori':
				$raw_data_mhs = isset($raw_data_mhs[0]) ? $raw_data_mhs[0] : $raw_data_mhs;
				$data['isi_mhs'] = $raw_data_mhs;
				$data['isi_surat'] = $raw_isi_surat;
				//exit(var_dump($data));
				$view = 'report/v_r_hbs_teori';
				break;
			case 'perm_kp':
				$data['tujuan'] = [
					// 'kepada' => str_replace(['\r\n', '\r', '\n'], '<br/>', $paramdata['tujuan_surat']['pimpinan_instansi']),
					'pimpinan_instansi' => str_replace(['\r\n', '\r', '\n'], '<br/>', $paramdata['tujuan_surat']['pimpinan_instansi']),
					'alamat' => $paramdata['tujuan_surat']['tempat_tujuan']
				];

				$makul = explode('#', $raw_isi_surat['MATA_KULIAH_TERKAIT']);

				$raw_data_mhs = isset($raw_data_mhs[0]) ? $raw_data_mhs[0] : $raw_data_mhs;
				$data['isi_mhs'] = [
					'nama' => $raw_data_mhs['NAMA'],
					'nim' => $raw_data_mhs['NIM'],
					'tmp_lahir' => $raw_data_mhs['TMP_LAHIR'],
					'tgl_lahir' => display_tanggal_indo($raw_data_mhs['TGL_LAHIR'], 'd-m-Y'),
					'smt' => $raw_data_mhs['JUM_SMT'],
					'prodi' => $raw_data_mhs['NM_PRODI'],
					'fkl' => $raw_data_mhs['NM_FAK']
				];
				$data['isi_surat'] = [
					'nama_kegiatan' => $makul[1],//$raw_isi_surat["NAMA_KEGIATAN"],
					'kode_makul' => $makul[0],
					'nama_makul' => $makul[1],
					'tempat' => $raw_isi_surat["TEMPAT_KEGIATAN"],
					'minat' => $raw_isi_surat["TEMA_DLM_KEGIATAN"],
					'tgl_mulai' => display_tanggal_indo($raw_isi_surat["TGL_MULAI"], 'Y-m-d'),
					'tgl_berakhir'=> display_tanggal_indo($raw_isi_surat["TGL_BERAKHIR"], 'Y-m-d'),
				];
				$view = 'report/v_r_kp';
			break;
			
			case 'tdk_men_bea': #4. PDF Surat Keterangan Tidak Sedang menerima beasiswa
				$raw_data_mhs = isset($raw_data_mhs[0]) ? $raw_data_mhs[0] : $raw_data_mhs;
				$data['isi_mhs'] = [
					'nama' => $raw_data_mhs['NAMA'],
					'nim' => $raw_data_mhs['NIM'],
					'tmp_lahir' => $raw_data_mhs['TMP_LAHIR'],
					'tgl_lahir' => display_tanggal_indo($raw_data_mhs['TGL_LAHIR'], 'd-m-Y'),
					'smt' => $raw_data_mhs['JUM_SMT'],
					'prodi' => $raw_data_mhs['NM_PRODI'],
					'fkl' => $raw_data_mhs['NM_FAK']
				];

				if (!empty($raw_isi_surat["DATA_TAMBAHAN"])){
					$bea = '<ul>';
					foreach ($raw_isi_surat["DATA_TAMBAHAN"] as $k=>$v) {
						$bea .= '<li>'.$v['NAMA_BEASISWA'].'</li>';
					}
					$bea .= '</ul>';
				} else {
					$bea = '';
				}
				$data['isi_surat'] = [
					'daftar_beasiswa_pernah_diikuti' => $bea,
				];
				$view = 'report/v_r_tdk_men_bea';
			break;

			case 'pndh_studi': #5. PDF Surat Keterangan Pindah Studi
				$raw_data_mhs = isset($raw_data_mhs[0]) ? $raw_data_mhs[0] : $raw_data_mhs;
				$data['isi_mhs'] = [
					'nama' => $raw_data_mhs['NAMA'],
					'nim' => $raw_data_mhs['NIM'],
					'tmp_lahir' => $raw_data_mhs['TMP_LAHIR'],
					'tgl_lahir' => display_tanggal_indo($raw_data_mhs['TGL_LAHIR'], 'd-m-Y'),
					'smt' => $raw_data_mhs['JUM_SMT'],
					'prodi' => $raw_data_mhs['NM_PRODI'],
					'fkl' => $raw_data_mhs['NM_FAK']
				];
				$data['isi_surat'] = [
					'keperluan' => $raw_isi_surat["KEPERLUAN"]
				];
				$view = 'report/v_r_pindah';
			break;

			case 'ijin_pen':
				// $data['tujuan'] = [
				// 	//'kepada' => nl2br($paramdata['distribusi_ps']['penerima_eksternal'][0]['PENERIMA_SURAT']),
				// 	'kepada' => str_replace(['\r\n', '\r', '\n'], '<br/>', $paramdata['distribusi_ps']['penerima_eksternal'][0]['PENERIMA_SURAT']),
				// 	'instansi' => '',
				// 	'alamat' => $paramdata['insert_surat_keluar']['TEMPAT_TUJUAN']
				// ];

				$data['tujuan'] = [					
					'kepada' => str_replace(['\r\n', '\r', '\n'], '<br/>', $paramdata['tujuan_surat']['pimpinan_instansi']),
					'instansi' => '',
					'alamat_tempat' => $paramdata['tujuan_surat']['tempat_tujuan']
				];

				$data['isi_mhs'] = [
					'nama' => $raw_data_mhs['NAMA'],
					'nim' => $raw_data_mhs['NIM'],
					'tmp_lahir' => $raw_data_mhs['TMP_LAHIR'],
					'tgl_lahir' => display_tanggal_indo($raw_data_mhs['TGL_LAHIR'], 'd-m-Y'),
					'smt' => $raw_data_mhs['JUM_SMT'],
					'prodi' => $raw_data_mhs['NM_PRODI'],
					'fkl' => $raw_data_mhs['NM_FAK'],
					'hp' => $raw_data_mhs['NO_HP'],
					'alamat' => $raw_data_mhs['ALAMAT']
				];

				$data['isi_surat'] = [
					'judul'	=> $raw_isi_surat["JUDUL_DLM_KEGIATAN"],
					'tempat_penelitian' => $raw_isi_surat["TEMPAT_KEGIATAN"],
					'metode' => $raw_isi_surat["METODE_DLM_KEGIATAN"],
					'tgl_mulai' => display_tanggal_indo($raw_isi_surat["TGL_MULAI"], 'Y-m-d'),
					'tgl_berakhir'=> display_tanggal_indo($raw_isi_surat["TGL_BERAKHIR"], 'Y-m-d'),
				];

				$data['daftar_tembusan'][] = isset($pgw_psd_an) ? $pgw_psd_an[0]['STR_NAMA_S1'].' (sebagai laporan)' : '';
				$view = 'report/v_r_penelitian';
			break;

			case 'ijin_studi_pen':
				$data['tujuan'] = [					
					'kepada' => str_replace(['\r\n', '\r', '\n'], '<br/>', $paramdata['tujuan_surat']['pimpinan_instansi']),
					'instansi' => '',
					'alamat_tempat' => $paramdata['tujuan_surat']['tempat_tujuan']
				];

				$raw_data_mhs = (isset($raw_data_mhs[0]) ? $raw_data_mhs[0] : $raw_data_mhs);
				$data['isi_mhs'] = [
					'nama' => $raw_data_mhs['NAMA'],
					'nim' => $raw_data_mhs['NIM'],
					'smt' => trans_angka_romawi($raw_data_mhs['JUM_SMT']),
					'prodi' => $raw_data_mhs['NM_PRODI'],
					'fkl' => $raw_data_mhs['NM_FAK']
				];
				$data['isi_surat'] = [
					'keperluan' => $raw_isi_surat["KEPERLUAN"],
					'nama_kegiatan' => $raw_isi_surat["NM_KEGIATAN"],
					'tema' => $raw_isi_surat["TEMA_DLM_KEGIATAN"],
					'tempat_penelitian' => $raw_isi_surat["TEMPAT_KEGIATAN"],
					'tgl_mulai' => display_tanggal_indo($raw_isi_surat["TGL_MULAI"], 'Y-m-d'),
					'tgl_berakhir'=> display_tanggal_indo($raw_isi_surat["TGL_BERAKHIR"], 'Y-m-d'),
				];					
				
				$view = 'report/v_r_studi_pen';
			break;

			case 'ijin_obs':
				$data['tujuan'] = [					
					'kepada' => str_replace(['\r\n', '\r', '\n'], '<br/>', $paramdata['tujuan_surat']['pimpinan_instansi']),
					'instansi' => '',
					'alamat_tempat' => $paramdata['tujuan_surat']['tempat_tujuan']
				];						
				
				
				$data['isi_mhs'] = $raw_data_mhs;
				//exit(var_dump($data['isi_mhs']));

				$makul = explode('#', $raw_isi_surat['MATA_KULIAH_TERKAIT']);
				//sementara
				// $data['prodi'] = $paramdata['bagian_surat_lain']['nm_unit_prodi'];
				$data['isi_surat'] = [
					'nama_kegiatan' => 'Observasi',//$raw_isi_surat["NAMA_KEGIATAN"],
					'kode_makul' => $makul[0],
					'nama_makul' => $makul[1],
					'topik_observasi' => $raw_isi_surat["TEMA_DLM_KEGIATAN"],
					'tempat' => $raw_isi_surat["TEMPAT_KEGIATAN"],
					'tgl_mulai' => display_tanggal_indo($raw_isi_surat["TGL_MULAI"], 'Y-m-d'),
					'tgl_berakhir'=> display_tanggal_indo($raw_isi_surat["TGL_BERAKHIR"], 'Y-m-d'),
				];				
				$view = 'report/v_r_observasi';
			break;

			case 'ijin_pen_makul':
				$data['tujuan'] = [					
					'kepada' => str_replace(['\r\n', '\r', '\n'], '<br/>', $paramdata['tujuan_surat']['pimpinan_instansi']),
					'instansi' => '',
					'alamat_tempat' => $paramdata['tujuan_surat']['tempat_tujuan']
				];				
				$data['isi_mhs'] = $raw_data_mhs;				

				$makul = explode('#', $raw_isi_surat['MATA_KULIAH_TERKAIT']);
				$data['prodi'] = $paramdata['prodi'];
				$data['isi_surat'] = [
					'nama_kegiatan' => (!empty($raw_isi_surat["NAMA_KEGIATAN"]) ? $raw_isi_surat["NAMA_KEGIATAN"] : ''),
					'kode_makul' => $makul[0],
					'nama_makul' => $makul[1],
					'topik_penelitian_makul' => $raw_isi_surat["TEMA_DLM_KEGIATAN"],
					'tempat' => $raw_isi_surat["TEMPAT_KEGIATAN"],				
					'tgl_mulai' => display_tanggal_indo($raw_isi_surat["TGL_MULAI"], 'Y-m-d'),
					'tgl_berakhir'=> null,
				];
				// $data['daftar_tembusan'][] = isset($pgw_psd_an) ? $pgw_psd_an[0]['STR_NAMA_S1'].' (sebagai laporan)' : '';
				$view = 'report/v_r_pen_makul';	
			break;	

			default:				
				echo "<h1>Template report belum ada</h1><br><br>";
				echo '<pre>'.json_encode($data).'</pre>';

				break;
		}
		$this->load->view($view,$data);
    }


    protected function set_nomor($data_sk, $mode='testing')
    {
    	$parameter	= array('api_kode' => 90002, 'api_subkode' => 3, 'api_search' => array($data_sk));
		$req_no 		= $this->apiconn->api_tnde_v3('tnde_surat_keluar/penomoran', 'json', 'POST', $parameter);
				
		if(empty($req_no['ERRORS'])){
			#pada mode testing, no. urut dan no. sela dikosongka agar tak ganggu penomoran resmi
			if ($mode == 'testing'){
				$data_sk['NO_URUT'] = '';
				$data_sk['NO_SELA']	= '';
			} elseif ($mode == 'production') {
				$data_sk['NO_URUT']	= (!empty($req_no['NO_URUT']) ? $req_no['NO_URUT'] : 0);
				$data_sk['NO_SELA']		= (!empty($req_no['NO_SELA']) ? $req_no['NO_SELA'] : 0);	
			}
			
			$data_sk['NO_SURAT']	= (!empty($req_no['NO_SURAT']) ? $req_no['NO_SURAT'] : 0);
			$data['save_sk'] = TRUE;
		} else{
			$data['save_sk'] = FALSE;
			$data['errors'] = $req_no['ERRORS'];
		}					
    }

    /**
    * HELPER METHOD SIMPAN PENERIMA SURAT
    * @return void
    */
    protected function simpan_penerima_surat($kategori=null, $mode='preview', $data_sk=null, $data_penerima=null)
    {       	
    	switch ($kategori) {
    		case 'pjb':#Pejabat
    			$kpd_pejabat = $data_penerima;
    			if(!empty($kpd_pejabat[0])){
					$save_pjb = [];
					foreach($kpd_pejabat as $key=> $val){
						$exp = explode("#", $val);
						$get_pjb = $this->Repo_Simpeg->get_pejabat_aktif($data['surat_keluar']['umum']['TGL_SURAT'], $exp[0]);	
						if(count($exp) == 2){
							$pjb['ID_SURAT_KELUAR']	= $data_sk['ID_SURAT_KELUAR'];
							$pjb['PENERIMA_SURAT'] = $get_pjb[0]['NIP']; //NIP PEGAWAI
							$pjb['JABATAN_PENERIMA'] = $exp[0];
							$pjb['KD_GRUP_TUJUAN'] = 'PJB01';
							$pjb['KD_STATUS_DISTRIBUSI'] = "PS";
							$pjb['ID_STATUS_SM'] = 1;
							$pjb['KD_JENIS_TEMBUSAN'] = 0;
							$pjb['NO_URUT'] = $key + 1;
							$pjb['KD_JENIS_KEPADA']				= $exp[1];

							if ($mode == 'preview'){
								$save_pjb[] = $pjb;	#langsung ditampung
							} elseif ($mode == 'bernomor') {
								$savepjb = $this->apiconn->get_api('tnde_surat_keluar/insert_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($pjb)));
								if($savepjb == FALSE){
									$data['errors'][] = "Gagal mengirim ke tujuan surat ke ".$pjb['PENERIMA_SURAT'];
									$save_pjb[] = FALSE;	
								}elseif ($savepjb == TRUE) {
									$save_pjb[] = $pjb;	
								}
							}
						
						}
					}

					$data['surat_keluar']['penerima']['PS'] = $save_pjb;
				}
    			break;
    		case 'pgw':#Pegawai
    			# code...
    			break;
    		case 'mhs':#Mahasiswa
    			# code...
    			break;
    		case 'eks':#Eksternal
    			# code...
    			break;
    		case 'dos':#Dosen
    			# code...
    			break;
    		
    		default:
    			# code...
    			break;
    	}    	
    }

    protected function kirim_tembusan_surat()
    {    	    	
   //  	if(!empty($ts_pejabat)){
			// $ts_urut = 0;
			// foreach($ts_pejabat as $key => $val){
				$exp = explode("#", $val);
				// $get_pjb = $this->Repo_Simpeg->get_pejabat_aktif($data['surat_keluar']['umum']['TGL_SURAT'], $exp[0]);
				switch ($kategori) {
		    		case 'internal_pejabat':
		    			$kd_grup_tujuan = "PJB01";
		    			$api_get_pjb =  $this->Repo_Simpeg->get_pejabat_aktif($data['surat_keluar']['umum']['TGL_SURAT'], $exp[0]);
		    			$penerima_surat = $api_get_pjb[0]['NIP'];
		    			$kd_grup_tujuan = "PJB01";
		    			   			
		    			break;
		    		case 'eksternal':
		    			# code...
		    			break;
		    		
		    		default:
		    			# code...
		    			break;
		    	}
				if(count($exp) == 2 ){
					$ts_pjb['ID_SURAT_KELUAR'] 				= $data['surat_keluar']['umum']['ID_SURAT_KELUAR'];
					$ts_pjb['PENERIMA_SURAT'] 				= $get_pjb[0]['NIP']; //NIP PEGAWAI
					$ts_pjb['JABATAN_PENERIMA'] 			= $exp[0];
					$ts_pjb['KD_GRUP_TUJUAN'] 				= 'PJB01';
					$ts_pjb['KD_STATUS_DISTRIBUSI'] 	= "TS";
					$ts_pjb['ID_STATUS_SM'] 						= "1";
					$ts_pjb['NO_URUT']									= $ts_urut + 1;
					$ts_pjb['KD_JENIS_TEMBUSAN'] 		= (string)$exp[1]; #1 = sebagai laporan, 0 = biasa
					$ts_pjb['KD_JENIS_KEPADA'] 				= '0';					
					//$save_ts_pjb = $ts_pjb;

					if ($opsi == 'preview'){
						$save_ts_pjb[] = $ts_pjb;
					} elseif ($opsi == 'bernomor') {
						$savetspjb = $this->apiconn->get_api('tnde_surat_keluar/insert_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($ts_pjb)));
						if($savetspjb == FALSE || $savetspjb == NULL){
							$data['errors'][] = "Gagal mengirim ke tujuan surat ke ".$exp[0];
							$save_ts_pjb[] = FALSE;
						}elseif ($savetspjb == TRUE) {
							$save_ts_pjb[] = $ts_pjb;	
						}
					}					
				}
			// }

			// $data['distribusi_ts']['tembusan_pejabat'] = $save_ts_pjb;
			$data['surat_keluar']['penerima']['TS'] = array_merge($data['surat_keluar']['penerima']['TS'], $save_ts_pjb);
		// }

    }

    /**
    * Menyimpan data umum surat, distribusi, dan detail [programmatically]
    * mode tersedia : debug, testing (surat bernomor, tapi status simpan tetap sbg draf, supaya masih bisa dihapus), production    
    * @param array data sesi (db + php session)
    * @return array (success message + data atau error message)
    */
    protected function insert_surat_keluar($paramdata=array(), $mode='testing')
    {    	
    }

    //mode: pdf biasa, pdf watermark, debug
    public function cetak_surat($id_surat_keluar=null, $mode='full') #preview, direct (after successfull insert), cetak dari data tersimpan
    {
    	$this->load->model('common/Api_simpeg');
    	$this->load->model('common/Api_sia');
    	$this->load->model('common/Api_automasi_surat');
    	$this->load->model('pgw/Verifikasi_model');
    	$this->load->helper('pgw/Tnde_sakad');

    	## 1. API GET SURAT KELUAR (UMUM + PENERIMA)
		$data['surat_keluar'] = $this->Automasi_model->get_surat_keluar($id_surat_keluar);
		$skr_umum = $data['surat_keluar']['umum'];
		$skr_penerima_ps = $data['surat_keluar']['penerima']['PS'];
		$skr_penerima_ts = $data['surat_keluar']['penerima']['TS'];

		## 2. API GET DETAIL SURAT (AUTOMASI)
		$data['detail_surat_automasi'] = $this->Automasi_model->get_detail_skr_automasi($id_surat_keluar);

		#seleksi hanya penerima eksternal
		foreach ($skr_penerima_ps as $k=>$v) {
			if ($v['KD_GRUP_TUJUAN'] !== 'EKS01'){
				continue;
			} else {
				$ketemu = $v['PENERIMA_SURAT'];
				break;
			}
		}
		//$data['tujuan_surat']['pimpinan_instansi'] = $save_eks[0]['PENERIMA_SURAT'];
		$data['tujuan_surat']['pimpinan_instansi'] = isset($ketemu) ? $ketemu : 'undefined - kesalahan data';
		$data['tujuan_surat']['tempat_tujuan'] = $data['surat_keluar']['umum']['TEMPAT_TUJUAN'];

		## 3. UNIT
		### 3.1 Unit penerbit surat
		$parameter = array('api_kode' => 1901, 'api_subkode' => 2, 'api_search' => array($data['surat_keluar']['umum']['TGL_SURAT'], $data['surat_keluar']['umum']['UNIT_ID']));
		$data['unit_penerbit'] = $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);

		## PEGAWAI/PEJABAT PENANDATANGANAN disebut dalam surat
		if (!empty($skr_umum['ID_PSD'])){
			$pgw_psd = $this->Api_simpeg->get_pejabat_aktif($skr_umum['TGL_SURAT'], $skr_umum['KD_JABATAN_2']);
			$nip_pgw = $pgw_psd[0]['NIP'];
			$parameter = array('api_kode' => 1123, 'api_subkode' => 3, 'api_search' => array($skr_umum['TGL_SURAT'], $nip_pgw));
			$pang_gol = $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
			$data['psd_detail']['pangkat'] = (!empty($pang_gol) ? $pang_gol[0] : []);
			$data['psd_detail']['pegawai'] = $pgw_psd;
		}
		
		if (!empty($skr_umum['ATAS_NAMA'])){
			$an = $this->apiconn->api_tnde('tnde_general/get_data', 'json', 'POST', array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('MD_PSD', 'ID_PSD', $skr_umum['ATAS_NAMA'])));
			if(!empty($an['KD_JABATAN'])){
				$parameter = array('api_kode' => 1121, 'api_subkode' => 9, 'api_search' => array($skr_umum['TGL_SURAT'], $an['KD_JABATAN'], 3));
				$data['psd_detail']['atas_nama'] = $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
			}			
			$data['psd_ringkas']['label_psd_an'] = isset($data['psd_detail']['atas_nama']) ? $data['psd_detail']['atas_nama'][0]['STR_NAMA_S1'] : ''; //$data['label_psd_an'] = $pgw_psd_an[0]['STR_NAMA_A212'];
		}		
		$data['psd_ringkas']['label_psd'] = $pgw_psd[0]['STR_NAMA_S1'];
		//$data['label_psd'] = $pgw_psd[0]['STR_NAMA_A212'];
		$data['psd_ringkas']['nm_pgw_psd'] = $pgw_psd[0]['NM_PGW_F'];
		$data['psd_ringkas']['nip_pgw_psd'] = $pgw_psd[0]['NIP'];

		## PEJABAT disebut dalam surat
		$psd_profil_pegawai = array_intersect_key($pgw_psd[0], array_flip(['NIP','NM_PGW_F','UNIT_NAMA','STR_NAMA','STR_NAMA_A212']));
		if (!empty($data['psd_detail']['pangkat'])){
			$psd_pangkat_pegawai = array_intersect_key($data['psd_detail']['pangkat'], array_flip(['HIE_GOLRU','HIE_NAMA']));	
		} else {
			$psd_pangkat_pegawai = [];
		}
		
		$data['pejabat_dalam_surat'] = array_merge($psd_profil_pegawai, $psd_pangkat_pegawai);

		## MAHASISWA disebut dalam surat
		$kepada = $skr_penerima_ps;		
		$kd_jenis_sakad_int = (int)$data['detail_surat_automasi']['KD_JENIS_SAKAD'];

		$arr_jenis_sakad = $this->Api_automasi_surat->get_jenis_surat_mahasiswa('kd', $kd_jenis_sakad_int);
		$data['raw_jenis_sakad'] = $arr_jenis_sakad;
		//exit(var_dump($data['raw_jenis_sakad']));

		foreach ($kepada as $k=>$v) {
			if($v['KD_GRUP_TUJUAN'] == 'MHS01'){
				$multi_mhs = [8,11]; #observasi, penelitian matkul, rekomendasi penelitian
				if (in_array($kd_jenis_sakad_int, $multi_mhs)){
					$tmp_mhs = $this->Api_sia->get_profil_mhs($v['PENERIMA_SURAT'],'kumulatif');
					$data['mhs_dalam_surat'][$k]['NIM']  = $tmp_mhs[0]['NIM']; 
					$data['mhs_dalam_surat'][$k]['NAMA']  = $tmp_mhs[0]['NAMA_F'];
				} else {
					$data['mhs_dalam_surat'][] = $this->Verifikasi_model->prepare_isi_surat($arr_jenis_sakad, $v['PENERIMA_SURAT'])['tmp_verified_mhs'];
				}
				
			} else {
				continue;
			}
		}

		##ambil data prodi berdasarkan data mahasiswa disebut dlm surat
		$getmhs = $this->Api_sia->get_profil_mhs($skr_umum['PEMBUAT_SURAT'],'kumulatif');	
		$data['prodi'] = $getmhs[0]['NM_PRODI']; 


		// foreach ($kepada as $k=>$v) {
		// 	if($v['KD_GRUP_TUJUAN'] == 'MHS01'){
		// 		#cek apakah terdaftar untuk request sia_mhs_kum
		// 		if (in_array($kd_jenis_sakad_int, $config_api_by_jenis_surat['sia_mhs_kum'])){
		// 			$get_mhs = $this->Api_sia->get_profil_mhs($v['PENERIMA_SURAT'],'kumulatif');
		// 			$data['detail_penerima'][$k]['mahasiswa']['kum'] = (!empty($get_mhs[0]) ? $get_mhs[0] : '');

		// 		}

		// 		#cek apakah terdaftar untuk request sia_mhs_dpm
		// 		if (in_array($kd_jenis_sakad_int, $config_api_by_jenis_surat['sia_mhs_dpm'])){
		// 			$get_mhs = $this->Api_sia->get_profil_mhs($v['PENERIMA_SURAT'],'dpm');
		// 			$data['detail_penerima'][$k]['mahasiswa']['dpm'] = (!empty($get_mhs[0]) ? $get_mhs[0] : '');
		// 		}

		// 		#cek apakah terdaftar untuk request sia_mhs_makul
		// 		if (in_array($kd_jenis_sakad_int, $config_api_by_jenis_surat['sia_mhs_makul'])){
					
		// 		}
				
		// 	} else {
		// 		continue;
		// 	}
		// }

		// ## TEMBUSAN INTERNAL (PEJABAT)
		// $tembusan = $skr_penerima_ts; //assign
		// if(!empty($tembusan)){
		// 	foreach($tembusan as $key => $val){
		// 		switch($val['KD_GRUP_TUJUAN']){
		// 			case 'PJB01':
		// 				$parameter 	= array('api_kode' => 1101, 'api_subkode' => 1, 'api_search' => array($val['JABATAN_PENERIMA']));
		// 				$jab_tembusan = $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
		// 				break;
		// 			case 'MHS01':
		// 				$parameter	= array('api_kode'=>26000, 'api_subkode' => 12, 'api_search' => array($val['PENERIMA_SURAT']));
		// 				$get_mhs 		= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);
		// 				$data['penerima_tembusan'][$key]['mahasiswa'] = (!empty($get_mhs[0]) ? $get_mhs[0] : '');
		// 				break;
		// 			case 'EKS01':

		// 				break;
		// 		break;

		// 	}
		// }
		

		if ($mode === 'full'){
			$this->generate_pdf_surat_v2($data);	
		} elseif ($mode === 'json') {
			$this->output->set_status_header(200)
			 	->set_content_type('application/json')
			 	->set_output(json_encode($data));
		}		
    }

    //url ini diakses oleh googledoc viewer, sehingga tanpa pengamanan auth
    public function cetak_surat_tersimpan($id_surat_keluar, $mode='full')
    {
    	$this->cetak_surat($id_surat_keluar, $mode);
    }

## ------------------ TESTING PURPOSE --------------------- ##
    public function testing_pejabat($kd_jabatan)
    {
    	$this->load->model('common/Api_simpeg');
    	//$this->load->model('pgw/Repo_Simpeg', 'Repo_Simpeg');

    	echo print_r( $this->Api_simpeg->get_pejabat_aktif('03/05/2018', $kd_jabatan) );
    }
    public function testing_mhs($nim)
	{
		$parameter	= array('api_kode'=>26000, 'api_subkode' => 10, 'api_search' => array($nim));
		$api_get_kum	= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);
		echo var_dump($api_get_kum[0]);
	}

	public function testing_bebas_teori()
	{
		$this->load->model('pgw/Verifikasi_model');

		$nim = $this->input->get('nim');
		echo json_encode($this->Verifikasi_model->complex_logic_bebas_teori($nim));		
	}

	#TESTING PURPOSE
    public function testing_api_sia($apa='')
    {    
    	$this->load->model('common/Api_sia'); 

    	$nim = $this->input->get('nim');
    	$kd_prodi = $this->input->get('prodi');
    	if ($apa == 'lulus'){    		
    		$get = $this->Api_sia->cekTIYM($nim); 
    	} elseif ($apa == 'khs') {
    		$get = $this->Api_sia->cek_khs_kumul($nim);
    		$jml_result_makul = count($get);
    		$dat = [
    			'meta' => [
    				'count' => $jml_result_makul,
    			],
    			'data' => $get
    		];    		
    	} elseif ($apa == 'bea') {
    		$get = $this->Api_sia->get_beasiswa_diikuti($nim);
    	} elseif ($apa == 'riw_bea') {
    		$get = $this->Api_sia->cekRiwayatBeasiswa($nim);
    	} elseif ($apa == 'bts03') { #cek matkul akhir
    		$get = $this->Api_sia->cek_matkul_akhir($nim);
    	} elseif ($apa == 'bts02') { #cek kurikulum aktif
    		$get = $this->Api_sia->cek_kur_aktif($kd_prodi);
    	} elseif ($apa == 'bts05') {
    		$get0 = $this->Api_sia->cek_kur_aktif($kd_prodi);    		
    		
    		//-------- Cek Detil KUR Aktif ------------
			$get = $this->Api_sia->cek_detil_kur($kd_prodi, $get0[0]["KD_KUR"]);
			//$minsksawal = $ckr[0]['MIN_SKS_EVALUASI_AWAL'];			
    	} elseif ($apa == 'bts06') {
    		$get = $this->Api_sia->cek_bebas_teori_mhs($nim);
    		$jml_result_makul = count($get['result']['cek_matakuliah']);
    		$dat = [
    			'jumlah_makul_bebas_teori' => $jml_result_makul
    		];
    	}

    	echo json_encode($dat);
    }

    public function testing_001()
    {
    	$this->load->model('Verifikasi_model');

    	$nim = $this->input->get('nim');
    	$paramdata_cf = [
    		[
    			'NIM' => $nim,
    			'KD_PRODI' => '22607'
    		]
    	];
    	echo var_dump($this->Verifikasi_model->testing_callback($paramdata_cf));
    }
	public function coba($mode='page', $where='coba1')
    {
    	if ($mode == 'page'){
    		switch ($where) {
	    		case 'coba2':
	    			break;
	    		
	    		default:
	    			$data['title'] = 'Form surat ijin penelitian';
	    			$view = 'form/v_coba1';
	    			break;
	    	}
	    	#5. Render view akhir
			$this->load->view('common/templates/v_header_responsive');
			$this->load->view('common/templates/v_sidebar_responsive', $data);
			$this->load->view($view,$data);
			$this->load->view('common/templates/v_footer_minimal');	
    	} elseif ($mode == 'action') {
    		echo var_dump($_POST);
    	}
    	
    }


    public function waktu()
    {
    	$d1=new DateTime(); 
		$d2=new DateTime("2018-04-26 07:30:00.889342"); 
		$diff=$d2->diff($d1);

		echo print_r($diff);
		// DateInterval Object ( [y] => 0 [m] => 3 [d] => 4 [h] => 23 [i] => 3 [s] => 40 [weekday] => 0 [weekday_behavior] => 0 [first_last_day_of] => 0 [invert] => 1 [days] => 94 [special_type] => 0 [special_amount] => 0 [have_weekday_relative] => 0 [have_special_relative] => 0 ) 1
    }

    /**
    * Fungsi darurat untuk adaptasi current datetime akibat kesalahan waktu di server exp
    */
    protected function adaptasi_waktu_exp($datetime_string, $format='Y-m-d h:i:s')
    {
    	$serv_exp_time = new DateTime($datetime_string);

    	$correct_time =$serv_exp_time->add(new DateInterval());
    	return $correct_time;
    }

    
    protected function datetime_koreksi($format=null)
    {
    	$wrong_exp_time = new DateTime();
    	$correct_datetime = $wrong_exp_time->add(new DateInterval('P3M4DT22H50M12S'));

    	if (!empty($format)){
    		return $correct_datetime->format($format);
    	} else {
    		return $correct_datetime; //object
    	}
    	
    }


    public function waktu_server()
    {
    	echo '<h1>Waktu Server Sekarang</h1><hr>';
    	$dt = new DateTime();
    	echo $dt->format('Y-m-d h:i:s');

    	echo '<h1>Waktu Server Seharusnya</h1><hr>';
    	$cdt = $this->datetime_koreksi();
		//echo $cdt->format('Y-m-d h:i:s');
		echo $this->datetime_koreksi('d/m/Y');
    }
}