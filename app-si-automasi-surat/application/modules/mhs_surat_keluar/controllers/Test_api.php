<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');

class Test_api extends MY_Controller {
    
    public function __construct()
    {
        parent::__construct();         
    }

    /**
     * Versi 2 - Service2
     */
    public function detail_surat($id_surat_keluar)
    {
        $this->load->model('pgw/Automasi_model');

        echo json_encode( $this->Automasi_model->get_surat_keluar($id_surat_keluar) );

    }

    /**
     * Detail Surat Keluar (TNDE SERVICE6 - surat2018)
     * @param string id surat keluar
     * @return mixed json dari beberapa endpoint
     */
    public function detail($id=null)
    {
        if (empty($id))
            exit('gunakan parameter yang benar!');

        $data['surat_keluar']  = $this->apiconn->api_tnde_v3('tnde_surat_keluar/detail_surat_keluar', 'json', 'POST', array('api_kode' => 90001, 'api_subkode' => 2, 'api_search' => array($id)));
        if (!empty($data['surat_keluar'])){
            $status_code = 200;
            $response = $data['surat_keluar'];
        } else {
            $status_code = 404;
            $response = [
                'has_errors' => true,
                'errors' => [
                    'surat keluar dengan id '.$id.' tidak ditemukan di database'
                ]
            ];
        }

        $this->output->set_status_header($status_code)
            ->set_content_type('application/json')
            ->set_output(json_encode($response));
    }

    public function master($kind=null)
    {
    	if ($kind == 'jenis_surat'){
    		// $data['jenis_surat']  = $this->apiconn->api_tnde2018('tnde_master/get_jenis_surat', 'json', 'POST', array('api_kode' => 11001, 'api_subkode' => 3, 'api_search' => array(date('d/m/Y'))));

    		// echo print_r($data);    		
    		$curl_handle=curl_init();
			curl_setopt($curl_handle, CURLOPT_URL,'http://service.uin-suka.ac.id/servsiasuper/index.php/tnde_public/');
			curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
			curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl_handle, CURLOPT_USERAGENT, 'SISURAT');
			$query = curl_exec($curl_handle);
			curl_close($curl_handle);
			echo $query;


    	}

    }

    public function distribusi()
    {

    }

    public function waktu()
    {
    	echo date('d/m/Y h:i:s');
    }

}