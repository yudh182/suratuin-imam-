<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');

class Monitoring extends MY_Controller {

	public function __construct()
	{
		parent::__construct();		
		$this->load->helper('form');
		$this->load->helper('common/Trans_general');
		$this->load->helper('pgw/Tnde_sakad');

        $this->load->module('repo/Api_foto','api_foto'); //produksi
		// $this->load->module('repo/Api_client_tnde','api_client_tnde'); //produksi
        $this->load->module('repo/Api_client_tnde_staging', 'api_client_tnde_staging'); //staging
        $this->load->model('common/Api_automasi_surat','autosurat');
    }

    public function test()
    {
        // $api_get = $this->apiconn->api_autosurat_v2('notifikasi/cek_querystring', 'json', 'GET', 
        //     array(
        //         'api_kode' => 16001, 'api_subkode' => 1, 
        //         'api_search' => array(
        //             'kategori_monitor' => 'TTE',
        //             'pemonitor' => '15650051'
        //         )
        //     )
        // );

        // $api_get = $this->apiconn->api_autosurat_v2('notifikasi/monitor_event_surat', 'json', 'GET', 
        //     array(
        //         'api_kode' => 16001, 'api_subkode' => 1, 
        //         'api_search' => array(
        //             'kategori_monitor' => 'TTE',
        //             'pemonitor' => '11650021'
        //         )
        //     )
        // );
       
        // var_dump($api_get);
    }

    public function index()
    {
        $this->load->model('Mdl_api_client_notifikasi', 'api_notif');

        $data['list_jenis_sakad'] = $this->autosurat->get_jenis_surat('aktif');
        
        #cek monitor surat
        $monitor = $this->api_notif->get_monitor_tte_by_pembuat($this->session->userdata('username'));            	
    	$data['monitor_tte'] = $monitor;

        if (!empty($monitor)){
            $data['detail_surats_termonitor'] = [];
            foreach ($monitor as $k=>$v) {
                $data['detail_surats_termonitor'][] = $this->apiconn->api_tnde('tnde_surat_keluar/detail_surat_keluar', 'json', 'POST', array('api_kode' => 90001, 'api_subkode' => 2, 'api_search' => array($v['ID_SURAT'])));
            }
            // exit(json_encode($detail_surats));
        }
        // exit(json_encode($data));
         

        #monitor pengajuan penerbitan tandatangan elektronik
        #loop API Detail surat keluar untuk melihat apakah surat sudah diberi tindakan?
        // $surat_keluar_dimonitor = [];
        // foreach ($data['monitor_tte'] as $k=>$v) {
        //    $surat_keluar_dimonitor[] = $this->api_client_tnde_staging->detail_surat_keluar($v['ID_SURAT']);
        // }        
              
    	#3. Render view akhir
		$this->load->view('common/templates/v_header');
		$this->load->view('common/templates/v_sidebar', $data);
		$this->load->view('content/v_monitor_surat',$data);
		$this->load->view('common/templates/v_footer');
    }

    public function monitor_penandatanganan_by_nim() #By Mahasiswa PEMOHON
    { 
    	#1. cek surat-surat apa saja yang di monitor oleh mahasiswa bersangkutan
    	$nim = $this->session->userdata('username');
    	$daftar_monitor = $this->api_client_autosurat->get_daftar_monitor($nim);
    	#2. loop cek tiap surat
    	$status_ttd = [];
    	foreach ($daftar_monitor as $k=>$v) {
    		$status_ttd[$k] = $this->monitor_penandatanganan($v['ID_SURAT']);
    	}
    }

    /**
	 * Tampilkan kepada pengguna apakah surat sudah ditandatangani atau belum atau justru ditolak
	 * @return 
	 */
    public function monitor_penandatanganan($id_surat='')
    {
    	$surat_keluar = $this->api_client_tnde->detail_surat_keluar($id_surat);
    	$data['monitor_ttde'] = $surat_keluar;

    	// if (!empty($surat_keluar))
    	// 	echo json_encode($surat_keluar);    	

    	$this->load->view('content/v_widget_notifikasi_penerbitan', $data);
    }


    public function monitor_distribusi($id_surat)
    {}

    protected function do_auth()
    {
    	// if ($this->session->userdata('log') !== 'in')
		// 	redirect(site_url('auth/login?from='.current_url()));
    }
}