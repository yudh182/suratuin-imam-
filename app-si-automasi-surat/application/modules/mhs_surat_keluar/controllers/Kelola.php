<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');

//use \Firebase\JWT\JWT;
class Kelola extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		// if ($this->session->userdata('log') !== 'in')
		// 	redirect(site_url('auth/login?from='.current_url()));

		$this->load->model('common/Mdl_tnde', 'apiconn'); #autoload
		// $this->load->helper('form');
		// $this->load->helper('Tnde_sakad');				
		$this->load->model('pgw/Verifikasi_model');
		$this->load->model('pgw/Automasi_model');		
		
    }

    public function index()
    {
    	phpinfo();
    }


    public function set_final()
    {
    	$id_surat_keluar = $this->input->post('id_surat_keluar');
    	$wr['ID_SURAT_KELUAR'] = $id_surat_keluar;
    	$surat['KD_STATUS_SIMPAN'] = 3; // final
    	$parameter = array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('MD_SURAT_KELUAR2', $surat, $wr));
    	$kesshou = $this->apiconn->api_tnde('tnde_general/update_data', 'json', 'POST', $parameter);
    	if($kesshou){
    		$parameter = array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('MD_SURAT_KELUAR2','ID_SURAT_KELUAR', $wr['ID_SURAT_KELUAR']));
    		$data['surat_keluar'] = $this->apiconn->api_tnde('tnde_general/get_data', 'json', 'POST', $parameter);

			$data['success'][] = "Berhasil memperbaharui (mengesahkan) surat keluar";
		}else{
			$data['errors'][] = "Gagal memperbaharui (mengesahkan) surat keluar";
		}

		$view_hasil = $this->load->view('administrasi/v_proses_pengajuan_sukses', $data, TRUE);

		$this->output
		    ->set_status_header(200)
		    ->set_content_type('text/html')		       			
		    ->set_output($view_hasil);
    }

    public function set_batal()
    {
    	$id_surat_keluar = $this->input->post('id_surat_keluar');
    	$wr['ID_SURAT_KELUAR'] = $id_surat_keluar;
    	$surat['KD_STATUS_SIMPAN'] = 0; // final
    	$parameter = array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('MD_SURAT_KELUAR2', $surat, $wr));
    	$kesshou = $this->apiconn->api_tnde('tnde_general/update_data', 'json', 'POST', $parameter);
    	if($kesshou){
    		$parameter = array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('MD_SURAT_KELUAR2','ID_SURAT_KELUAR', $wr['ID_SURAT_KELUAR']));
    		$data['surat_keluar'] = $this->apiconn->api_tnde('tnde_general/get_data', 'json', 'POST', $parameter);

			$data['success'][] = "Berhasil memperbaharui surat keluar (membatalkan surat)";
		}else{
			$data['errors'][] = "Gagal memperbaharui surat keluar (membatalkan surat)";
		}

		$view_hasil = $this->load->view('administrasi/v_proses_pengajuan_sukses', $data, TRUE);

		$this->output
		    ->set_status_header(200)
		    ->set_content_type('text/html')		       			
		    ->set_output($view_hasil);
    }

    public function reaktif()
    {
    	$id_surat_keluar = $this->input->post('id_surat_keluar');
    	$wr['ID_SURAT_KELUAR'] = $id_surat_keluar;
    	$surat['KD_STATUS_SIMPAN'] = 2; // final
    	$parameter = array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('MD_SURAT_KELUAR2', $surat, $wr));
    	$kesshou = $this->apiconn->api_tnde('tnde_general/update_data', 'json', 'POST', $parameter);
    	if($kesshou){
    		$parameter = array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('MD_SURAT_KELUAR2','ID_SURAT_KELUAR', $wr['ID_SURAT_KELUAR']));
    		$data['surat_keluar'] = $this->apiconn->api_tnde('tnde_general/get_data', 'json', 'POST', $parameter);

			$data['success'][] = "Berhasil mengaktifkan kembali surat keluar";
		}else{
			$data['errors'][] = "Gagal mengaktifkan kembali surat keluar";
		}

		$view_hasil = $this->load->view('administrasi/v_proses_pengajuan_sukses', $data, TRUE);

		$this->output
		    ->set_status_header(200)
		    ->set_content_type('text/html')		       			
		    ->set_output($view_hasil);
    }


    public function trigger_notifikasi_email()
    {
    	// $this->load->library('mhs_surat_keluar/Mailer_surat');

    	// $kirim = $this->mailer_surat->testing_email();    	
    	$this->load->library('email');
						

			$config = array();
			$config['protocol']		='smtp';
			// $config['smtp_host']	='ssl://smtp.gmail.com';
			// $config['smtp_port']	='465';
			$config['smtp_crypto'] = 'ssl';
			$config['smtp_host']	='smtp.gmail.com';  
			$config['smtp_port']	= '465';
			$config['smtp_user']	= 'surat-no-replay@uin-suka.ac.id';
			$config['smtp_pass']	= 'surat123456';
			$config['charset']    = 'utf-8';
            // $config['newline']    = "\r\n";
			$config['mailtype']		='text'; //atau html			
			$config['charset'] = 'iso-8859-1';
    		$config['wordwrap'] = TRUE;
			$config['validation'] = FALSE;

			// $config['protocol'] = 'sendmail';
			// $config['mailpath'] = '/usr/sbin/sendmail';
			// $config['charset'] = 'iso-8859-1';
			// $config['wordwrap'] = TRUE;
			
			$this->email->initialize($config);
			
			//siapkan data
			$penerima = '11650021@student.uin-suka.ac.id';
			$judul = 'Testing Email Automasi Surat';
			$pesan = '<p>Jika anda membaca pesan ini, percobaan 1 sudah berhasil!</p>';

			$this->email->set_newline("\r\n");
			$this->email->from($config['smtp_user'], 'Sistem Informasi Surat');
			// $this->email->from('surat-no-reply@exp.uin-suka.ac.id', 'Sistem Informasi Surat');
			$this->email->to($penerima);
			$this->email->subject($judul);
			$this->email->message($pesan);

			$kirim = $this->email->send();

			if ($kirim){
				echo 'Berhasil Dikirim';
			} else {
				//var_dump($this->email);
				 $error = $this->email->print_debugger(array('headers'));
                echo json_encode($error);
			}

    	//cek berhasil ngirim enggak?
    	// if($kirim){
     //        //$this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
     //        echo 'Selamat, email seharusnya berhasil dikirim. cek email anda <<11650021@student.uin-suka.ac.id>> kalau tidak percaya';
     //    } else{
     //        echo 'Maaf, email gagal dikirim akibat kesalahan teknis';
     //    }        
    }

#HAPUS SURAT DRAF (DENGAN NOMOR) YANG DIBUAT SELAMA TESTING
	public function hapus_surat_keluar($id_surat, $mode='full', $kode_akses_unik='test')
	{
		$del = $id_surat;

		if (!empty($del)){
			$api_del = $this->Automasi_model->delete_surat_keluar_automasi($del, $mode);

			echo json_encode($api_del, JSON_PRETTY_PRINT);
		} else {
			echo json_encode(['error_message'=>'perintah tidak dikenali']);
		}
	}
	
	//hanya hapus data di TNDE SURAT
	public function hapus_skr_sisurat($id_surat, $kode_akses_unik='test')
	{
		$del = $id_surat;

		if (!empty($del)){
			$api_del = $this->Automasi_model->delete_surat_keluar_automasi($del, 'partial_sisurat'); //data di SI Automasi tidak turut dihapus

			echo json_encode($api_del, JSON_PRETTY_PRINT);
		} else {
			echo json_encode(['error_message'=>'perintah tidak dikenali']);
		}
	}

	//hanya hapus data di AUTOMASI SURAT
	public function hapus_surat_automasi($id_surat, $kode_akses_unik='test')
	{
		$del = $id_surat;

		if (!empty($del)){
			$api_del = $this->Automasi_model->delete_detail_skr_automasi($del);

			echo json_encode($api_del, JSON_PRETTY_PRINT);
		} else {
			echo json_encode(['error_message'=>'perintah tidak dikenali']);
		}
	}



}