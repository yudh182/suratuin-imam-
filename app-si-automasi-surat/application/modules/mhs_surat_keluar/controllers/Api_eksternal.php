<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');
Class Api_eksternal extends MY_Controller{
	function __construct(){
		parent::__construct();
		$this->load->library('common/Curl');
		$this->load->model('common/Mdl_tnde','apiconn');
	}

	function auto_pejabat_eks_sl2(){
		if(isset($_GET['term'])){
			$return_arr = array();
			$q = strtoupper($_GET['term']);
			$parameter	= array('api_kode' => 90001, 'api_subkode' => 1, 'api_search' => array($q));
			$data 				= $this->apiconn->api_tnde('tnde_surat_keluar/get_tujuan_eks', 'json','POST', $parameter);
			$hasil = array();
			foreach($data as $val){
				//$hasil[] = htmlentities($val['PENERIMA_SURAT']);
				//$hasil[] = implode('#', $val);								
				$hasil['results'][] = array('id' => '', 'text' => $val['PENERIMA_SURAT']);
			}
			header("Content-type:application/json");
			echo json_encode($hasil);
		}
	}


	#SELECT2 AJAX
	function auto_kabupaten_sl2(){
		if(ISSET($_GET['search'])){
			// $referer = (!empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '');
			// $auth = host_allowed($referer);
			// if($auth){
				$q = strtoupper(str_replace("'", "''", $_GET['search']));
				$tot = count(str_split($q));
				if($tot > 2){
					$data = $this->apiconn->api_sia('sia_master/data_search', 'json', 'POST', array('api_kode' => 12000, 'api_subkode' => 3,'api_search' => array($q)));
					if(count($data) > 0){
						foreach($data as $val){
							$nm_kab = (!empty($val['NM_KAB1']) ? $val['NM_KAB1'].". ".$val['NM_KAB2'] : $val['NM_KAB2']);
							$return_arr['results'][] = array('id' => $val['KD_KAB'], 'text' => strtoupper($nm_kab));
						}
						$joss = json_encode($return_arr);
						header("Content-type:application/json");
						echo $joss;
					}else{
						echo 0;
					}
				}
			//}
		}

	}

}
