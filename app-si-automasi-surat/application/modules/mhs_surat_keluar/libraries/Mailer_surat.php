<?php defined('BASEPATH') OR exit('No direct script access allowed');
Class Mailer_surat{
	protected $CI;
	protected $config = [];

	public function __construct()
	{
        $this->CI =& get_instance(); // Assign the CodeIgniter super-object
		$this->config = [
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.gmail.com',
			'smtp_port' => '465',
			'smtp_user' => 'surat-no-replay@uin-suka.ac.id',
			'smtp_pass' => 'surat123456',
			'mailtype' => 'html'
		];		
	}

	public function kirim_email_payload()
	{
	}

	public function notifikasi_pasif()
	{}


	public function kirim_email_penandatanganan($id='')
	{
		// set_time_limit(900);
		$ci =& get_instance();
		$ci->load->model('mdl_tnde');
		$data['surat_keluar']  = $ci->mdl_tnde->get_api('tnde_surat_keluar/detail_surat_keluar', 'json', 'POST', array('api_kode' => 90001, 'api_subkode' => 2, 'api_search' => array($id)));

		$parameter = array('api_kode' => 2001, 'api_subkode' => 2, 'api_search' => array($data['surat_keluar']['PEMBUAT_SURAT']));
		$data['detail_pembuat'] = $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
		
		$psd = $ci->mdl_tnde->get_api('tnde_general/get_data', 'json', 'POST', array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('MD_PSD', 'ID_PSD', $data['surat_keluar']['ID_PSD'])));
		if(!empty($psd['KD_JABATAN'])){
			$parameter = array('api_kode' => 1121, 'api_subkode' => 9, 'api_search' => array($data['surat_keluar']['TGL_SURAT'], $psd['KD_JABATAN'], 3));
			$data['psd'] = $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);

			$parameter = array('api_kode' => 1121, 'api_subkode' => 3, 'api_search' => array(tgl($data['surat_keluar']['WAKTU_SIMPAN']), $data['psd'][0]['KD_PGW'], 1)); #tgl, kd_pgw, status aktif/tidak aktif
			$data['psd_detail']['jabatan'] = $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
		
			// $data['url'] = base_url() . 'verifikasi/psd_email?uid=' . $id . '&req=email&authid=' . $data['psd'][0] ['KD_PGW'] ;
			
			$ci->load->library('s00_lib_siaenc');
			$param = array('uid'=> $id, 'req'=>'email', 'authid'=> $data['psd'][0] ['KD_PGW']);
			
			$data_enc = $ci->s00_lib_siaenc->encrypt(json_encode($param));
		
			$data['url'] = base_url() . 'verifikasi/psd_email?req=' . $data_enc;
			
			$pesan		= $ci->load->view('mail_content', $data, true);
			$penerima	= $data['psd'][0] ['KD_PGW'] . '@uin-suka.ac.id';
			$judul 		= 'Pembuatan ' . $data['surat_keluar']['PERIHAL'];
			
			$config['protocol']		='smtp';
			$config['smtp_host']	='ssl://smtp.gmail.com';  
			$config['smtp_port']	='465';
			$config['smtp_user']	= 'surat-no-replay@uin-suka.ac.id';  
			$config['smtp_pass']	= 'surat123456';
			$config['mailtype']		='html';
			
			$ci->load->library('email', $config);
			$ci->email->set_newline("\r\n");
			$ci->email->from($config['smtp_user'], 'Sistem Informasi Surat');
			$ci->email->to($penerima);
			$ci->email->subject($judul);
			$ci->email->message($pesan);
			
			return $ci->email->send();
			/* tambahan error */
		} else {
			return false;
		}
	}

	public function testing_email()
	{
			$config = $this->config;
			$this->CI->load->library('email', $config);

			//siapkan data
			$penerima = '11650021@student.uin-suka.ac.id';
			$judul = 'Testing Email Automasi Surat';
			$pesan = 'Jika anda membaca pesan ini, percobaan 1 sudah berhasil!';

			$this->CI->email->set_newline("\r\n");
			$this->CI->email->from($config['smtp_user'], 'Sistem Informasi Surat');
			$this->CI->email->to($penerima);
			$this->CI->email->subject($judul);
			$this->CI->email->message($pesan);
			
			return $this->CI->email->send();
	}

}