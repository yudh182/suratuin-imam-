<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');

class Informasi extends MX_Controller {
	public function __construct()
	{
		parent::__construct();		
		$this->load->helper('form');
		$this->load->helper('common/Trans_general');
		$this->load->helper('pgw/Tnde_sakad');

		$this->load->model('common/Mdl_tnde','apiconn');

        $this->load->module('repo/Api_foto','api_foto'); //produksi
		// // $this->load->module('repo/Api_client_tnde','api_client_tnde'); //produksi
  //       $this->load->module('repo/Api_client_tnde_staging', 'api_client_tnde_staging'); //staging
        $this->load->model('common/Api_automasi_surat','autosurat');
    }

     public function index()
    {        
        $data['list_jenis_sakad'] = $this->autosurat->get_jenis_surat('aktif');

        #3. Render view akhir
		$this->load->view('common/templates/v_header');
		$this->load->view('common/templates/v_sidebar', $data);
		$this->load->view('v_home_informasi',$data);
		$this->load->view('common/templates/v_footer');
	}
}