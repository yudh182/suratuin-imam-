<div class="col-med-9">
	<div class="content-space">
		<ul id="crumbs">
			<li>
				<a title="#" href="">Informasi</a>
			</li>
			<li>
				<a title="#" href="">Cara Penggunaan Sistem</a>
			</li>
		</ul><br/>

		<h2 class="h-border">Informasi</h2>

		<div class="row">
			<div class="col-md-6">
				<div class="box">
					<div class="box-header">
						<div class="box-title">Penggunaan Sistem</div>
							<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-minus"></i>
							</button>
						</div>
					</div>
					<div class="box-body">
						<article class="bs-callout">
							<h1>Buat surat dalam 4 langkah saja</h1>
							<ol>
								<li>Pilih jenis surat yang akan dibuat</li>
								<li>isi sedikit form</li>
								<li>klik preview untuk mengintip print out surat</li>
								<li>klik terbitkan untuk menerbitkan sekaligus mencetak surat</li>
							</ol>
							<a href="#" class="pull-right">selengkapnya</a>
						</article>

						<article class="bs-callout">
							<h1>Buat surat izin penelitian saat pulang kampung ?</h1>
							<p class="text-success"><b>Bisa!</b> begini caranya :</p>
							<a href="#" class="pull-right">selengkapnya</a>
						</article>

					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="box">
					<div class="box-header">
						<div class="box-title">Pemberitahuan</div>
							<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-minus"></i>
							</button>
						</div>
					</div>
					<div class="box-body">
						<div class="alert alert-danger">
							<p><b>Perhatian</b>, Sistem masih dalam tahap testing, mohon berkonsultasi dengan tata usaha di fakultas anda</p>
						</div>
					</div>
				</div>
			</div>
	</div>
</div>