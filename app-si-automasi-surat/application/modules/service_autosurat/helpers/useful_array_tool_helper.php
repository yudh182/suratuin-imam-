<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');
class Useful_array_tool {
	/**
	* @since 29 mei 2018
	* @source http://thinkofdev.com/php-isset-and-multi-dimentional-array/
	*/
	public static function elementExists($key, $array){
		if (is_array($key)) {
			$curArray = $array;
			$lastKey = array_pop($key);
			foreach($key as $oneKey) {
				if (!elementExists($oneKey, $curArray)) 
					return false;
				$curArray = $curArray[$oneKey];
			}
			return is_array($curArray) && elementExists($lastKey, $curArray);
		} else {
			return isset($array[$key]) || array_key_exists($key, $array);
		}
	}

	public static function array_keys_exists(array $keys, array $arr) {
	   return !array_diff_key(array_flip($keys), $arr);
	}

	
}