<div class="col-med-9">
	<div class="content-space">
		
		<ul id="crumbs">
			<li>
				<a title="Surat Keluar" href="<?php echo site_url();?>">Beranda</a>
			</li>
			<li>
				<a title="Tambah Surat Keluar" href="<?php echo site_url('publik/cek_surat/keaslian');?>#">Cek Keaslian Surat</a>
			</li>
		</ul><br/>

		<h2 class="h-border">Cek Keaslian Surat</h2>

		<!-- <div class="overviewhead row">
    		<div class="pull-left"><button id="mark-pengajuan-diurus" class="btn btn-inverse btn-uin">Tandai sedang diurus</button></div>
    		<div class="pull-right"><button id="mark-pengajuan-ubah-tte" class="btn btn-inverse btn-uin">Ubah ke penandatanganan elektronik <i class="fa fa-edit"></i></button></div>
    	</div> -->
		<div class="container-cek-surat">
			<!-- jika tidak asli -->
			<!--<div class="bs-callout bs-callout-error">
				<center><h2>Sistem tidak mengenali surat dengan qrcode keaslian ini</h1></center>
			</div>
			<div class="bs-callout bs-callout-error">
				<center><h2>Surat ini asli tetapi telah kadaluarsa</h1></center>
				<center><h3>Nomor Surat : B-XXX/YY/ZZZ/XXX</h2></center>
			</div>-->
			<?php
			if ($status_otentikasi === 'valid'){
				$alert_class = 'alert alert-success';
				$icon_class = 'fa fa-check-circle';
			} elseif ($status_otentikasi === 'invalid') {
				$alert_class = 'alert alert-danger';
				$icon_class = 'fa fa-info-circle';
			}
			?>
			<div id="hasil_otentikasi" class="<?= $alert_class; ?>">
			    <center style="margin-bottom: 10px;">
			    	<span class="<?= $icon_class; ?> fa-3x"></span>		    
				</center>
				<center>
					<h4><b><?= $hasil_otentikasi; ?></b></h4>
					<h3>Kode Otentikasi : <?= $kode_otentikasi; ?></h3>
				</center>
			</div>				
			<div class="alert alert-info col-md-12">
				<div class="col-md-1"><span class="fa fa-info-circle fa-3x"></span></div>
				<div class="col-md-10">
					<h4>perhatian:</h4>
        			<p>Mohon <b>bandingkan</b> juga dokumen cetak yang anda terima dengan rangkuman isi surat untuk menghindari kemungkinan penggunaan valid qrcode dari dokumen lain.</p>
				</div>        		
	      	</div>
	      	<div class="clearfix"></div>

		 <?php if ($status_otentikasi === 'valid') : ?>
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Tampilan Surat</h3>
				</div>
				<div class="box-body">				
				 <?php if (isset($link_pdf_gviewer)) : 	?>

					<iframe src="<?= $link_pdf; ?>" width="100%" height="630px">						
					</iframe>
					<!-- <iframe src="http://docs.google.com/viewer?url=<?php echo $link_pdf;?>&embedded=true" width="99%" height="630" style="border: none;"></iframe> -->
				 <?php endif; ?>
					<div class="col-md-6">
						<div class="small-box">
				            <div class="inner">
				            	<!--<h3>0</h3>-->
				            	<h4><b>Status Administrasi</b></h4>
				            	<?php
				            	$kds = $surat_keluar['umum']['KD_STATUS_SIMPAN'];
				            	if ($kds == '0'){ #dibatalkan
				            		echo '<h3 class="text-danger">Dibatalkan</h3>';
				            	} elseif ($kds == '1') {
				            		echo '<h5 class="text-warning">Draf (belum bernomor)</h5>';
				            	} elseif ($kds == '2') {
				            		echo '<h3 class="">Bernomor</h3>';
				            	} elseif ($kds == '3') {
				            		echo '<h3 class="text-success">Telah ditandatangani dan disahkan</h3>';
				            	}
				            	?>				              	
				            </div>			                    
				            </a>
				        </div>
					</div>
					<div class="col-md-6">
						<div class="small-box">
				            <div class="inner">				         
				            	<h4>Dibuat pada :</h4>
				            	<?php echo (!empty($surat_keluar['umum']['WAKTU_SIMPAN']) ? '<h5>'.$surat_keluar['umum']['WAKTU_SIMPAN'].'</h5>' : "");?>
				            	<!-- <hr><br>
				            	<h4>Diajukan pada :</h4>
				            	<h5 class="text-danger">belum diajukan</h5>
				            	<br><br>
				            	<hr>
				            	<h4>Ditandatangani dan disahkan pada :</h4>
				            	<h5 class="text-danger">belum disahkan</h5>
				            	<br>
				            	<hr> -->
				            </div>			                    
				            </a>
				        </div>
					</div>
					
				</div>
			</div>
			
			
	     <?php endif; ?>
			

			
			
		</div>

		
	</div><!-- ./content-space -->
</div><!-- ./col-md-9 -->