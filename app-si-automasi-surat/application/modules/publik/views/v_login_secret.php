
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" />
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sistem Informasi Surat UIN Sunan Kalijaga</title>
        
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">		
		<link href="http://static.uin-suka.ac.id/images/favicon.png" type="image/x-icon" rel="shortcut icon">
		<link href="http://static.uin-suka.ac.id/plugins/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
		<link href="http://static.uin-suka.ac.id/css/style_global.css" rel="stylesheet" type="text/css"/>
		<link href="http://static.uin-suka.ac.id/css/style_layout.css" rel="stylesheet" type="text/css"/>
		<link href="http://static.uin-suka.ac.id/css/docs.css" rel="stylesheet" type="text/css"/>
		<link href="http://surat.uin-suka.ac.id/asset/css/tnde.css" rel="stylesheet" type="text/css"/>
		<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro">
		<!--<link href="http://akademik.uin-suka.ac.id/asset/css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css"/>-->
		
		<link href="http://static.uin-suka.ac.id/css/breadcrumb.css" rel="stylesheet" type="text/css"/>
		<script>!window.jQuery && document.write(unescape('%3Cscript src="http://surat.uin-suka.ac.id/asset/js/jquery-1.9.1.min.js"%3E%3C/script%3E'))</script>
		<!-- custom scrollbars plugin -->
		<script src="http://surat.uin-suka.ac.id/asset/js/jquery.mCustomScrollbar.concat.min.js"></script>
    </head>
    <body>	
	<div class="app_header-top"></div>
	<div class="app_main">
		<div class="app_header">
			<div class="center">
				<div class="app_uin_id">
					<a href="http://surat.uin-suka.ac.id/" ></a>
				</div>
				<div class="app_header_right">
									<style type="text/css">
.searchform {
	display: inline-block;
	zoom: 1; /* ie7 hack for display:inline-block */
	*display: inline;
	padding: 0px 0 0px 3px;
}
.searchform input {
	font: normal 12px/100% Arial, Helvetica, sans-serif;
}
.searchform .searchfield {
	background: #fff;
	padding: 4px 6px 4px 8px;
	width: 202px;
	border: solid 1px #bcbbbb;
	outline: none;
	margin-top:8px;
	height:30px;
	-webkit-border-radius: .3em;
	-moz-border-radius: .3em;
	border-radius: .3em;

	-moz-box-shadow: inset 0 1px 2px rgba(0,0,0,.2);
	-webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,.2);
	box-shadow: inset 0 1px 2px rgba(0,0,0,.2);
}
.searchform .searchbutton {
	color: #fff;
	border: solid 1px #494949;
	font-size: 11px;
	height: 30px;
	width: 30px;
	text-shadow: 0 1px 1px rgba(0,0,0,.6);
	margin-top:-8px;
	cursor:pointer;	
	-webkit-border-radius: .3em;
	-moz-border-radius: .3em;
	border-radius: .3em;

	background: #5f5f5f;
	background: -webkit-gradient(linear, left top, left bottom, from(#8fc800), to(#438c00));
	background: -moz-linear-gradient(top,  #8fc800,  #438c00);
	filter:  progid:DXImageTransform.Microsoft.gradient(startColorstr='#8fc800', endColorstr='#438c00'); /* ie7 */
	-ms-filter:  progid:DXImageTransform.Microsoft.gradient(startColorstr='#8fc800', endColorstr='#438c00'); /* ie8 */
}
					</style>
					<div style="text-align:right; margin-top:-15px;">
						<div class="app_system_id">Sistem Automasi Persuratan Mahasiswa</div>
						<div class="app_subsystem_id"></div>
						<div>
							<div class="clear5"></div>
							<form class="searchform" action="http://surat.uin-suka.ac.id/page/page/search" method="post">
								<input class="searchfield" type="text" name="cari" value="Kata kunci..." onfocus="if (this.value == 'Kata kunci...') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Kata kunci...';}" />
								<button class="searchbutton">Cari</button>
							</form>
						</div>	
					</div>
								</div>
			<div class="clear"></div>
			</div>
		</div>
		<div id="app_content">
			<div class="app-row">
				<div class="col-med-3">
					<h2>Login</h2>
					<br>
					<div class="login-form">
						<form method="post" action="<?php echo site_url('publik/login_portal/aksi_login'); ?>">		
							<div class="form-group">
								<input type="text" name="username" id="username" class="form-control" placeholder="Username" autofocus >
							</div>
							<div class="form-group">
								<input type="password" name="password" id="password" class="form-control" placeholder="Password" >
							</div>
							<button type="submit" class="btn-uin btn btn-inverse btn btn-small" style="float:right;">Login</button><br>
						</form>
					</div>
					<br>
					<?php
					$sess_errors = $this->session->flashdata('errors');

					if(!empty($sess_errors)){
						echo '<div class="bs-callout bs-callout-error" style="margin-bottom:5px">';
						if(is_array($sess_errors)){
							foreach($sess_errors as $value){
								echo "- ".$value."<br/>";
							}
						}else{
							echo $sess_errors;
						}
						echo '</div>';
					}

					?>
					<div class="login-links">
						<div class="login-link">
						</div>
						<div class="login-link">
							<a class="link-icon monitoring-surat" href="http://surat.uin-suka.ac.id/surat/cek_surat" title="">
							Cek Surat
							</a>
						</div>
					</div>  
				</div>
				<div class="col-med-9">
					
				</div>		
			</div>
			<div class="clear20"></div>			
		</div>

		<div class="clear5"></div>
		
	</div>	
	<div style="clear:both;"></div>
</div>
<div class="copyright">&copy; 2014 - Pusat Teknologi Informasi dan Pangkalan Data, UIN Sunan Kalijaga, Yogyakarta</div>	
</div>
</body>
</html>