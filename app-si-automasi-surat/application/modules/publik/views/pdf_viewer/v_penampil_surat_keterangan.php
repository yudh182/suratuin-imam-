<?php
/**
//============================================================+
// File name   : v_penampil_surat_keterangan.php
// Begin       : 2018-03-22
// Last Update : 2018-03-23
*/
$pdf = new Pdf_creator(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);#P, mm, A4, , UTF-8 encoding, .
$pdf->set_document_properties(
    array(
        'pdf_title'=> $judul_surat,
        'pdf_margin' => ['15','35','15', true], #LEFT, TOP, RIGHT (PAGE MARGINS)
        'pdf_template_margin' => ['6','6'] #MARGIN HEADER, MARGIN FOOTER
    )
);
$pdf->setData($template); //data untuk header dan footer

// set font
$pdf->SetFont('times', '', 10);

// add a page
$resolution= array(175, 266);
$pdf->AddPage('P', $resolution);


### *** MULAI HTML STRING *** ##

### *** STOP HTML STRING *** ###

### Render dengan method writeHTMLCell()
//writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)


### Render QR Code
$style = array(
    'border' => 0,
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255)
    'module_width' => 0.1, // width of a single module in points
    'module_height' => 0.1 // height of a single module in points
);
$pdf->write2DBarcode($qrcode_otentikasi, 'QRCODE,L', 20, '', 20, 20, $style, 'N'); // QRCODE,L : QR-CODE Low error correction

// move pointer to last page
$pdf->lastPage();

// ---------------------------------------------------------
// ob_end_clean();
ob_clean(); //untuk menghapus output buffer


//Close and output PDF document
$pdf->Output('surat_'.time().'.pdf', 'I');
//$pdf->Output(FCPATH.'uploads/tmp_pdf_generated/surat_keterangan_masih_kuliah'.time().'.pdf', 'F');


//============================================================+
// END OF FILE
//============================================================+