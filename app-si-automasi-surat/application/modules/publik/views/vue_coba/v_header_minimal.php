<!DOCTYPE html>
<html>
<head>
	<title><?= $title; ?> - Sistem Informasi Surat</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script src="<?= base_url('assets/js/vue.js'); ?>"></script>
	<link href="http://static.uin-suka.ac.id/plugins/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
	<link href="http://static.uin-suka.ac.id/css/style_global.css" rel="stylesheet" type="text/css"/>
	<link href="http://static.uin-suka.ac.id/css/style_layout.css" rel="stylesheet" type="text/css"/>
	<link href="http://surat.uin-suka.ac.id/asset/css/tnde.css" rel="stylesheet" type="text/css"/>
	<link href="<?= base_url('assets/css/style_tnde_sakad.css'); ?>" rel="stylesheet" type="text/css"/>
	<link href="http://static.uin-suka.ac.id/css/breadcrumb.css" rel="stylesheet" type="text/css"/>

	<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script type="text/javascript" src="<?= base_url('assets/js/adminlte.min.js'); ?>"></script>
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
</head>
<body>
	<div class="app_header-top"></div>
	
	<div class="app_main"><!-- wrapper app buka -->
		<div class="app_header">
			<div class="center">
				<div class="app_uin_id">
					<a href="http://surat.uin-suka.ac.id/" ></a>
				</div>
				<div class="app_header_right">
					<div class="app_system_id">Sistem Informasi Surat</div>
					<div class="app_univ_id">UIN Sunan Kalijaga</div>
					<div class="app_subsystem_id badge">Automasi Persuratan Mahasiswa</div>
				</div>
			<div class="clear"></div>
			</div>
		</div>

		<div id="app_content">
			<div class="app-row">

			<!-- next codes: view sidebar, crumb, content, footer -->