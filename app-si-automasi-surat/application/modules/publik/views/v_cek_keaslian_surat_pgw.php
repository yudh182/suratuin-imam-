<div class="col-med-3">
	<h2>Login</h2>
	<br>
	<div class="login-form">
		<form method="post" action="<?php echo base_url(); ?>pgw/login/aksi_login">		
			<div class="form-group">
				<input type="text" name="username" id="username" class="form-control" placeholder="Username" autofocus >
			</div>
			<div class="form-group">
				<input type="password" name="password" id="password" class="form-control" placeholder="Password" >
			</div>
			<button type="submit" class="btn-uin btn btn-inverse btn btn-small" style="float:right;">Login</button><br>
		</form>
	</div>
	<br>
	<?php
	$sess_errors = $this->session->flashdata('errors');

	if(!empty($sess_errors)){
		echo '<div class="bs-callout bs-callout-error" style="margin-bottom:5px">';
		if(is_array($sess_errors)){
			foreach($sess_errors as $value){
				echo "- ".$value."<br/>";
			}
		}else{
			echo $sess_errors;
		}
		echo '</div>';
	}

	?>
	<div class="login-links">
		<div class="login-link">
		</div>
		<div class="login-link">
			<a class="link-icon monitoring-surat" href="http://surat.uin-suka.ac.id/surat/cek_surat" title="">
				Cek Surat
			</a>
		</div>
	</div>  
</div>
<div class="col-med-9">
	<h2>Cek Keaslian Surat</h2>

	<div class="container-cek-surat">
		<div class="row">
			<div class="col-md-8" id="display-surat"></div>
			<div class="col-md-4" id="result-keaslian">
				<h3>Status Keaslian</h3>

				<h3>Waktu Pembuatan</h3>

				<h3>Status Administrasi</h3>
			</div>
		</div> 
	</div>
</div>