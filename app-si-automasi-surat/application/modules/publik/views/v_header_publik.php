<!DOCTYPE html>
<html>
<head>
	<title><?php isset($title) ? ucwords($title).' | ' : 'Halaman utama | '; ?>Sistem Informasi Surat</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script src="<?= base_url('assets/js/vue.js'); ?>"></script>
	<link href="http://static.uin-suka.ac.id/plugins/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
	<link href="http://static.uin-suka.ac.id/css/style_global.css" rel="stylesheet" type="text/css"/>
	<link href="http://static.uin-suka.ac.id/css/style_layout.css" rel="stylesheet" type="text/css"/>
	<link href="http://surat.uin-suka.ac.id/asset/css/tnde.css" rel="stylesheet" type="text/css"/>
	<link href="<?= base_url('assets/css/style_tnde_sakad.css'); ?>" rel="stylesheet" type="text/css"/>
	<link href="http://static.uin-suka.ac.id/css/breadcrumb.css" rel="stylesheet" type="text/css"/>

	<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script type="text/javascript" src="<?= base_url('assets/js/adminlte.min.js'); ?>"></script>
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<style type="text/css">
		.searchform {
			display: inline-block;
			zoom: 1; /* ie7 hack for display:inline-block */
			*display: inline;
			padding: 0px 0 0px 3px;
		}
		.searchform input {
			font: normal 12px/100% Arial, Helvetica, sans-serif;
		}
		.searchform .searchfield {
			background: #fff;
			padding: 4px 6px 4px 8px;
			width: 202px;
			border: solid 1px #bcbbbb;
			outline: none;
			margin-top:8px;
			height:30px;
			-webkit-border-radius: .3em;
			-moz-border-radius: .3em;
			border-radius: .3em;

			-moz-box-shadow: inset 0 1px 2px rgba(0,0,0,.2);
			-webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,.2);
			box-shadow: inset 0 1px 2px rgba(0,0,0,.2);
		}
		.searchform .searchbutton {
			color: #fff;
			border: solid 1px #494949;
			font-size: 11px;
			height: 30px;
			width: 30px;
			text-shadow: 0 1px 1px rgba(0,0,0,.6);
			margin-top:-8px;
			cursor:pointer;	
			-webkit-border-radius: .3em;
			-moz-border-radius: .3em;
			border-radius: .3em;

			background: #5f5f5f;
			background: -webkit-gradient(linear, left top, left bottom, from(#8fc800), to(#438c00));
			background: -moz-linear-gradient(top,  #8fc800,  #438c00);
			filter:  progid:DXImageTransform.Microsoft.gradient(startColorstr='#8fc800', endColorstr='#438c00'); /* ie7 */
			-ms-filter:  progid:DXImageTransform.Microsoft.gradient(startColorstr='#8fc800', endColorstr='#438c00'); /* ie8 */
		}
	</style>
</head>
<body>
	<div class="app_header-top"></div>
	
	<div class="app_main"><!-- wrapper app buka -->
		<div class="app_header">
			<div class="center">
				<div class="app_uin_id">
					<a href="http://surat.uin-suka.ac.id/" ></a>
				</div>
				<div class="app_header_right">
					<div class="app_system_id">Sistem Informasi Surat</div>
					<div class="app_univ_id">UIN Sunan Kalijaga</div>
					<div class="app_subsystem_id badge">Automasi Persuratan Mahasiswa</div>
				</div>
			<div class="clear"></div>
			</div>
		</div>

		<div id="app_content">
			<!-- <div class="app-row"> -->

			<!-- next codes: view sidebar, crumb, content, footer -->