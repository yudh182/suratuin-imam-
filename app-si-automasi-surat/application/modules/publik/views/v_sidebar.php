<div class="col-med-3">
<?php if ($this->session->userdata('log') !== 'in') : ?>
	<h2>Login</h2>
	<br>
	<div class="login-form">
		<form method="post" action="<?php echo base_url(); ?>mhs2/login/aksi_login">		
			<div class="form-group">
				<input type="text" name="username" id="username" class="form-control" placeholder="Username" autofocus >
			</div>
			<div class="form-group">
				<input type="password" name="password" id="password" class="form-control" placeholder="Password" >
			</div>
			<button type="submit" class="btn-uin btn btn-inverse btn btn-small" style="float:right;">Login</button><br>
		</form>
	</div>
	<br>
	<?php
	$sess_errors = $this->session->flashdata('errors');

	if(!empty($sess_errors)){
		echo '<div class="bs-callout bs-callout-error" style="margin-bottom:5px">';
		if(is_array($sess_errors)){
			foreach($sess_errors as $value){
				echo "- ".$value."<br/>";
			}
		}else{
			echo $sess_errors;
		}
		echo '</div>';
	}

	?>
	<div class="login-links">
		<div class="login-link">
		</div>
		<div class="login-link">
			<a class="link-icon monitoring-surat" href="http://surat.uin-suka.ac.id/surat/cek_surat" title="">
				Cek Surat
			</a>
		</div>
	</div>  
<?php endif; ?>


<?php if ($this->session->userdata('log') == 'in' && $this->session->userdata('grup') === 'mhs') : ?>
	<div id="sidebar-sia">		
		<nav class="accordion">
			<ol>
				<li>
					<div class="sia-profile" style="margin-bottom:10px;width:223px">						
						<div class="alert alert-warning">
							<p>Saat ini anda berada di laman publik</p>
						</div>
						<h2><?php  echo $this->session->userdata('nama'); ?></h2>		
						<p style="text-align:center; font-weight:bold;"><?php echo $this->session->userdata('username'); ?></p>
						<hr>
						<?php if ($this->session->userdata('grup') == 'mhs') : ?> 
						<table>
							<tbody>
								<tr>
									<td style="width:100px;height:25px;"><p class="fakultas-profil"><b>Fakultas</b> </p></td><td> <?= $this->session->userdata('nm_fakultas'); ?><p></p></td>
								</tr>
								<tr>
									<td style="width:100px;height:25px;"><p class="prodi-profil"><b>Program Studi</b> </p></td><td> <?= $this->session->userdata('nm_prodi'); ?><p></p></td>
								</tr>
							</tbody>
						</table>
						<?php endif; ?>

						<b><?php echo (!empty($this->session->userdata('str_nama')) ? $this->session->userdata('str_nama') : ''); ?></b>

					</div>
					<div id="separate"></div>
				</li>
				<li>
					<a class="item full" href="<?php echo base_url('pgw'); ?>">
						<span>
							Masuk ke Sistem
							<span>
								<div class="underline-menu"></div>
							</span>
						</span>
					</a>
				</li>
			</ol>
		</nav>
	</div>
<?php endif; ?>
</div>