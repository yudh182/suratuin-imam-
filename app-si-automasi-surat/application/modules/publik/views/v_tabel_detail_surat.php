<fieldset class="fs-detail-surat">
						<legend>Umum</legend>
						<table class="table table-nama detail-surat">
							<tr>
								<td class="tdlabel">Unit/Bagian/Fakultas</td>
								<td>&nbsp;: <?php echo (!empty($unit[0]['UNIT_NAMA']) ? $unit[0]['UNIT_NAMA'] : '');?></td>
							</tr>
							<tr>
								<td>Nomor Surat</td><td>
								 &nbsp;: <?php echo (!empty($surat_keluar['umum']['NO_SURAT']) ? $surat_keluar['umum']['NO_SURAT'] : "");?></td>
							</tr>
							<tr>
								<td>Jenis Surat</td>
								<td> &nbsp;: <?php echo (!empty($surat_keluar['umum']['NM_JENIS_SURAT']) ?strtoupper($surat_keluar['umum']['NM_JENIS_SURAT']) : "");?>
								</td>
							</tr>
							<tr>
								<td>Klasifikasi Surat</td>
								<td> &nbsp;: <?php echo (!empty($surat_keluar['umum']['KD_KLASIFIKASI_SURAT']) ? $surat_keluar['umum']['KD_KLASIFIKASI_SURAT'].' - '.strtoupper($surat_keluar['umum']['NM_KLASIFIKASI_SURAT']) : '');?>
								</td>
							</tr>
							<tr>
								<td>Sifat Surat / Tingkat Keamanan</td>
								<td> &nbsp;: <?php 
									echo (!empty($surat_keluar['umum']['NM_SIFAT_SURAT']) ?strtoupper($surat_keluar['umum']['NM_SIFAT_SURAT']) : '');
									echo (!empty($surat_keluar['umum']['NM_KEAMANAN_SURAT']) ? ' / '.strtoupper($surat_keluar['umum']['NM_KEAMANAN_SURAT']) : '');
									?>
								</td>
							</tr>
							
						</table>
					</fieldset>

					<fieldset class="fs-detail-surat">
						<legend>Detail Surat (Automasi)</legend>

					<?php if (isset($detail_surat_automasi) && $detail_surat_automasi['GRUP_JENIS_SAKAD'] == '1'): #GRUP KETERANGAN FAKULTAS ?>
						<table class="table table-nama detail-surat">
							<tr>
								<td class="tdlabel">Jenis Surat Mahasiswa</td>
								<td> :  <?php echo (!empty($detail_surat_automasi['NM_JENIS_SAKAD']) ? $detail_surat_automasi['NM_JENIS_SAKAD'] : "");?></td>
							</tr>
							<tr>
								<td>Grup Surat</td><td>
								 &nbsp;: <?php echo (!empty($detail_surat_automasi['GRUP_JENIS_SAKAD']) ? $detail_surat_automasi['GRUP_JENIS_SAKAD'] : "");?></td>
							</tr>
							<tr>
								<td>Keperluan</td>
								<td> &nbsp;: <?php echo (!empty($detail_surat_automasi['KEPERLUAN']) ? $detail_surat_automasi['KEPERLUAN'] : "");?>
								</td>
							</tr>
							<tr>
								<td>Batas Masa Berlaku</td>
								<td> &nbsp;: <?php echo (!empty($detail_surat_automasi['BATAS_MASA_BERLAKU']) ? display_tanggal_indo($detail_surat_automasi['BATAS_MASA_BERLAKU'], 'Y-m-d') : "");?>
								</td>
							</tr>				
						</table>
					<?php endif; ?>

					<?php if (isset($detail_surat_automasi) && $detail_surat_automasi['GRUP_JENIS_SAKAD'] == '2'): #GRUP PERMOHONAN IJIN FAKULTAS ?>
						<table class="table table-nama detail-surat">
							<tr>
								<td class="tdlabel">Jenis Surat Mahasiswa</td>
								<td> :  <?php echo (!empty($detail_surat_automasi['NM_JENIS_SAKAD']) ? $detail_surat_automasi['NM_JENIS_SAKAD'] : "");?></td>
							</tr>
							<tr>
								<td>Grup Surat</td><td>
								 &nbsp;: <?php echo (!empty($detail_surat_automasi['GRUP_JENIS_SAKAD']) ? $detail_surat_automasi['GRUP_JENIS_SAKAD'] : "");?></td>
							</tr>
							<tr>
								<td>Keperluan</td>
								<td> &nbsp;: <?php echo (!empty($detail_surat_automasi['KEPERLUAN']) ? $detail_surat_automasi['KEPERLUAN'] : "");?>
								</td>
							</tr>
							<tr>
								<td>Batas Masa Berlaku</td>
								<td> &nbsp;: <?php 					
								echo (!empty($detail_surat_automasi['BATAS_MASA_BERLAKU']) ? display_tanggal_indo(
										date_format(date_create_from_format('Y-m-d h:i:s', $detail_surat_automasi['BATAS_MASA_BERLAKU']), 'Y-m-d')) : "");?>
								</td>
							</tr>
							<tr>
								<td>Nama Kegiatan</td>
								<td> &nbsp;: <?php echo (!empty($detail_surat_automasi['NM_KEGIATAN']) ? $detail_surat_automasi['NM_KEGIATAN'] : "");?>
								</td>
							</tr>
							<tr>
								<td>Tema dalam Kegiatan</td>
								<td> &nbsp;: <?php echo (!empty($detail_surat_automasi['TEMA_DLM_KEGIATAN']) ? $detail_surat_automasi['TEMA_DLM_KEGIATAN'] : "");?>
								</td>
							</tr>
							<tr>
								<td>Tempat Kegiatan</td>
								<td> &nbsp;: <?php echo (!empty($detail_surat_automasi['TEMPAT_KEGIATAN']) ? $detail_surat_automasi['TEMPAT_KEGIATAN'] : "");?>
								</td>
							</tr>
							<tr>
								<td>Judul terkait Kegiatan</td>
								<td> &nbsp;: <?php echo (!empty($detail_surat_automasi['JUDUL_DLM_KEGIATAN']) ? $detail_surat_automasi['JUDUL_DLM_KEGIATAN'] : "");?>
								</td>
							</tr>
							<tr>
								<td>Metode dalam Kegiatan</td>
								<td> &nbsp;: <?php echo (!empty($detail_surat_automasi['METODE_DLM_KEGIATAN	']) ? $detail_surat_automasi['METODE_DLM_KEGIATAN	'] : "");?>
								</td>
							</tr>
							<tr>
								<td>Subyek dalam Kegiatan</td>
								<td> &nbsp;: <?php echo (!empty($detail_surat_automasi['SUBYEK_DLM_KEGIATAN']) ? $detail_surat_automasi['SUBYEK_DLM_KEGIATAN'] : "");?>
								</td>
							</tr>
							<tr>
								<td>Obyek dalam Kegiatan</td>
								<td> &nbsp;: <?php echo (!empty($detail_surat_automasi['OBYEK_DLM_KEGIATAN']) ? $detail_surat_automasi['OBYEK_DLM_KEGIATAN'] : "");?>
								</td>
							</tr>
							<tr>
								<td>Tanggal Mulai</td>
								<td> &nbsp;: <?php echo (!empty($detail_surat_automasi['TGL_MULAI']) ? display_tanggal_indo($detail_surat_automasi['TGL_MULAI'], 'Y-m-d') : "")?>
								</td>
							</tr>
							<tr>
								<td>Tanggal Berakhir</td>
								<td> &nbsp;: <?php echo (!empty($detail_surat_automasi['TGL_BERAKHIR']) ? display_tanggal_indo($detail_surat_automasi['TGL_BERAKHIR'], 'Y-m-d') : "")?>
								</td>
							</tr>
						</table>
					<?php endif; ?>
					</fieldset>