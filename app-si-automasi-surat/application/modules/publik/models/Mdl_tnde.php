<?php
class Mdl_tnde extends CI_Model {
	
	//fungsi di model untuk memanggil API berjenis REST
	function get_api($url, $output='json', $postorget='GET', $parameter){	
		$api_url = 'http://service2.uin-suka.ac.id/servtnde/tnde_public/'.$url.'/'.$output;
		// $api_url = 'http://service.uin-suka.ac.id/servsiasuper/index.php/tnde_public/'.$url.'/'.$output;
		$hasil = null;
		if ($postorget == 'POST'){
			$hasil = $this->curl->simple_post($api_url, $parameter);
		} else {
			$hasil = $this->curl->simple_get($api_url);
		}
		return json_decode($hasil, TRUE);
	}
	
	function api_simar($url, $output='json', $postorget='GET', $parameter){	
		$api_url = 'http://service2.uin-suka.ac.id/servaset/simar_public/'.$url.'/'.$output;
		// $api_url = 'http://service.uin-suka.ac.id/servsiasuper/index.php/tnde_public/'.$url.'/'.$output;
		$hasil = null;
		if ($postorget == 'POST'){
			$hasil = $this->curl->simple_post($api_url, $parameter);
		} else {
			$hasil = $this->curl->simple_get($api_url);
		}
		return json_decode($hasil, TRUE);
	}
	
	function api_service($url, $output='json', $postorget='GET', $parameter){
		$api_url = 'http://service.uin-suka.ac.id/servsiasuper/index.php/'.$url.'/'.$output;
		$hasil = null;
		if ($postorget == 'POST'){
			$hasil = $this->curl->simple_post($api_url, $parameter);
		} else {
			$hasil = $this->curl->simple_get($api_url);
		}
		return json_decode($hasil, TRUE);
	}

	function api_simpeg($url, $output='json', $postorget='GET', $parameter){	
		$api_url = 'http://service2.uin-suka.ac.id/servsimpeg/simpeg_public/'.$url.'/'.$output;
		// $api_url = 'http://service.uin-suka.ac.id/servsiasuper/simpeg_public/'.$url.'/'.$output;
		$hasil = null;
		
		$this->curl->option('HTTPHEADER', array('HeaderName: '.$this->encrypt001('dedy5u__4t')));
						
		if ($postorget == 'POST'){
			$hasil = $this->curl->simple_post($api_url, $parameter);
		} else {
			$hasil = $this->curl->simple_get($api_url);
		}
		return json_decode($hasil, TRUE);
	}	
	
	function api_ict($url, $output='json', $postorget='GET', $parameter){	
		$api_url = 'http://service.uin-suka.ac.id/servsiasuper/ict_public/'.$url.'/'.$output;
		$hasil = null;
		
		$this->curl->option('HTTPHEADER', array('HeaderName: '.$this->encrypt001('dedy5u__4t')));
						
		if ($postorget == 'POST'){
			$hasil = $this->curl->simple_post($api_url, $parameter);
		} else {
			$hasil = $this->curl->simple_get($api_url);
		}
		return json_decode($hasil, TRUE);
	}		
	
	function api_pbba($url, $output='json', $postorget='GET', $parameter){	
		$api_url = 'http://service2.uin-suka.ac.id/servtoec/pbba_public/'.$url.'/'.$output;
		$hasil = null;
		
		$this->curl->option('HTTPHEADER', array('HeaderName: '.$this->encrypt001('dedy5u__4t')));
						
		if ($postorget == 'POST'){
			$hasil = $this->curl->simple_post($api_url, $parameter);
		} else {
			$hasil = $this->curl->simple_get($api_url);
		}
		return json_decode($hasil, TRUE);
	}	
	
	function api_skripsi($url, $output='json', $postorget='GET', $parameter){	
		$api_url = 'http://service2.uin-suka.ac.id/servtugasakhir/sia_skripsi_public/'.$url.'/'.$output;
		$hasil = null;
		
		$this->curl->option('HTTPHEADER', array('HeaderName: '.$this->encrypt001('dedy5u__4t')));
						
		if ($postorget == 'POST'){
			$hasil = $this->curl->simple_post($api_url, $parameter);
		} else {
			$hasil = $this->curl->simple_get($api_url);
		}
		return json_decode($hasil, TRUE);
	}
	
	function api_ikd($url, $output='json', $postorget='GET', $parameter){
		$api_url = 'http://service.uin-suka.ac.id/servikd/index.php/ikd_public/'.$url.'/'.$output;
		$hasil = null;
		
		$this->curl->option('HTTPHEADER', array('HeaderName: '.$this->encrypt001('dedy5u__4t')));
		
		if ($postorget == 'POST'){
			$hasil = $this->curl->simple_post($api_url, $parameter);
		} else {
			$hasil = $this->curl->simple_get($api_url);
		}
		return json_decode($hasil, TRUE);
	}
	
	function api_link($url, $output='json', $postorget='GET', $parameter){	
		$api_url = $url.'/'.$output;
		$hasil = null;
		
		$this->curl->option('HTTPHEADER', array('HeaderName: '.$this->encrypt001('dedy5u__4t')));
		
		if ($postorget == 'POST'){
			$hasil = $this->curl->simple_post($api_url, $parameter);
		} else {
			$hasil = $this->curl->simple_get($api_url);
		}
		return json_decode($hasil, TRUE);
	}
	
	function simple_api($url, $output='json', $postorget='GET', $parameter){
		$api_url = $url.'/'.$output;
		$hasil = null;
		if ($postorget == 'POST'){
			$hasil = $this->curl->simple_post($api_url, $parameter);
		} else {
			$hasil = $this->curl->simple_get($api_url);
		}
		return json_decode($hasil, TRUE);
	}
	
	function auth_ad($username, $password){
		$api_url = "http://service.uin-suka.ac.id/servad/adlogauthgr.php?aud=8f304662ebfee3932f2e810aa8fb628723&uss=".$username."&pss=".$password;
		$hasil = null;
		$hasil = $this->curl->simple_get($api_url);
		return json_decode($hasil, TRUE);
	}
	
	function api_sia($url, $output='json', $postorget='GET', $parameter){	
		$api_url = 'http://service.uin-suka.ac.id/servsiasuper/index.php/sia_public/'.$url.'/'.$output;
		$hasil = null;
		
		$this->curl->option('HTTPHEADER', array('HeaderName: '.$this->encrypt001('dedy5u__4t')));
						
		if ($postorget == 'POST'){
			$hasil = $this->curl->simple_post($api_url, $parameter);
		} else {
			$hasil = $this->curl->simple_get($api_url);
		}
		return json_decode($hasil, TRUE);
	}
	
	function encrypt001($kata = ''){
		$this->load->library('s00_lib_siaenc');
		return $this->s00_lib_siaenc->encrypt($kata);
	}
	
	function api_foto($url){
		$api_url = "http://service.uin-suka.ac.id/foto/".$url;
		$hasil = null;
		$hasil = $this->curl->simple_get($api_url);
		return json_decode($hasil, TRUE);
	}
	
}
?>
