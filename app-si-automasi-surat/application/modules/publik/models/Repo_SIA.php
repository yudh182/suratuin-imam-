<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Repo_SIA extends CI_Model
{
	public function __construct()
	{
		parent::__construct();           
        $this->load->library('common/S00_lib_api');
	}

	public function get_makul_smt($nim=null)
	{
        $CI =& get_instance();
        $ta = $CI->s00_lib_api->get_api_json(URL_API_SIA.'sia_krs/data_procedure', 'POST', array('api_kode'=>50000, 'api_subkode'=>2));
        $cek=$CI->s00_lib_api->get_api_json(URL_API_SIA.'sia_krs/data_search', 'POST', array('api_kode'=>64000, 'api_subkode' => 22, 'api_search' => array($ta['data'][':hasil1'],$ta['data'][':hasil3'],$nim)));
         $response = array();
		$posts = array();
		for($i=0; $i<count($cek); $i++){
			$posts[] = array(
            		"nama_makul"            =>  $cek[$i]['NM_MK'],
            		"kode_makul"                  =>  $cek[$i]['KD_MK'],
            		"kode_kelas"			=> $cek[$i]['KD_KELAS']
        	);
		}
		$return = 0;
		$response['makul'] = $posts;
		$hasil = json_decode(json_encode($response), true);
		for($i=0;$i<count($hasil);$i++){
			if($hasil['makul'][$i]['kode_makul'] == 'TIF404033'){
				$return = 'succes';
				break;
			}
		}

		return $return;
		//print_r($hasil);        
    }


}