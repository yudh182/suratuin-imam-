<?php
/**
* Halaman SPA dengan VueJS
*/
class Vue_app extends MY_Controller
{
	
	function __construct()
	{
		$this->load->model('common/Mdl_tnde', 'apiconn');
	}

	/**
	* Mengimplementasikan SPA dengan vuejs
	* @return html
	*/
	public function index($state=null)
	{
		#Persiapkan initial data 
		$data['title'] = 'Panduan Penggunaan Aplikasi Web Automasi Surat';

		#Render view html dan vuejs
		$this->load->view('vue_coba/v_header_minimal', $data);		
		$this->load->view('vue_coba/v_panduan', $data);
		$this->load->view('vue_coba/v_footer_minimal');
	}

	/**
	* Search data melalui API Webservice SI UINSuka
	* @return json (terdiri atas meta dan results)
	*/
	public function search()
	{

	}

	/**
	* Insert data baru melalui API Webservice SI UINSuka
	* @return json (body) + custom header
	*/
	public function save()
	{

	}

	/**
	* Update data baru melalui API Webservice SI UINSuka
	* @return json (body) + custom header
	*/
	public function update()
	{

	}

}