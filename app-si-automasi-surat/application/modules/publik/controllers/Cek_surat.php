<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');
use \Firebase\JWT\JWT;
class Cek_surat extends MY_Controller {
	
	public function __construct()
	{
		parent::__construct();	
		$this->load->library('common/Curl');
		$this->load->model('common/Mdl_tnde', 'apiconn');
		$this->load->model('pgw/Automasi_model');
		$this->load->helper('common/MY_date');
		$this->load->helper('common/Kripto');
		//$this->load->model('Token_validator_model', 'token_validator');
		        
    }

	public function index()
	{		
	}

	/**
	* Cek Keaslian Surat menggunakan Json Web Token
	* decode lanjut cek ke webservice
	* @return PDF dan Ringkasan Status Surat
	*/
	public function keaslian($token=null)
	{
		$secretKey = 'Your-Secret-Key';

		#1. Baca Token dari uri segment lalu lakukan decode untuk ekstrak informasi
		$token_str = $this->uri->segment(4);
		//$decode_token_keaslian = $this->token_decoder->extract_informasi($token_str);
		
		try {
            $decoded = JWT::decode($token_str, $secretKey, array('HS256'));
            //$waktu_diterbitkan = date("Y-m-d H:i:s", $decoded->iat);
            //$waktu_kadaluarsa = date("Y-m-d H:i:s", $decoded->exp);
            $status_keaslian = TRUE;
            $status_berlaku = TRUE;
            //exit("surat asli dan masih berlaku");
        } catch (Firebase\JWT\ExpiredException $e ) {        	
        	$status_keaslian = TRUE;
            $status_berlaku = FALSE;
            //exit("surat asli tetapi sudah habis masa berlakunya");
        } catch (Firebase\JWT\SignatureInvalidException $e) {
            //echo "{'status' : 'fail' ,'msg':'Unauthorized - Signature verification failed'}";
            $status_keaslian = FALSE;
            $status_berlaku = FALSE; 
             exit("surat tidak dikenali");
        }

        if ($status_keaslian==true && $status_berlaku==true){
        	$data = (array)$decoded->data;

        	$this->load->view('pgw/templates/v_header');
			$this->load->view('v_cek_keaslian_surat_publik', $data);
			$this->load->view('pgw/templates/v_footer');

        } elseif ($status_keaslian==true && $status_berlaku==false) {
        	# code...
        } else { //semua false

        }

        /*
		if (!empty($this->session->userdata('log_nip')) && $this->session->userdata('log_str_id') == 'ST0007'){

			
			$this->load->view('pgw/templates/v_header');			
			$this->load->view('v_cek_keaslian_surat_pgw');
			$this->load->view('pgw/templates/v_footer');			
		} else { //publik
			$this->load->view('pgw/templates/v_header');
			$this->load->view('v_cek_keaslian_surat_publik');
			$this->load->view('pgw/templates/v_footer');
		}
		*/		
	}

	#VERSI SURAT YANG DIBUAT DARI LAMAN AUTOMASI_SURAT_MHS
	public function validasi_qrcode($id_surat_keluar=null, $signature='')
	{
		$this->load->model('mhs_surat_keluar/Mdl_mhs_surat_keluar');
		$data['title'] = 'Cek Keaslian Surat';

		if (empty($id_surat_keluar)){
			$encoded_id_surat = $this->input->get('search');
			$id_surat_keluar = Kripto::urlsafeB64Decode($encoded_id_surat);
		}
		
		if (!empty($id_surat_keluar)){
			## 1. API GET SURAT KELUAR + PENERIMA
			// $data['surat_keluar'] = $this->Automasi_model->get_surat_keluar($id_surat_keluar);
			$data['surat_keluar'] = $this->Mdl_mhs_surat_keluar->get_surat_keluar($id_surat_keluar);
			// exit(json_encode($data));

			## 2. SET VARIABEL OTENTIKASI
			if (empty($data['surat_keluar']) || empty($data['surat_keluar']['umum'])){#404 (surat tidak ditemukan di sistem)
				$data['status_otentikasi'] = 'invalid';
				$data['hasil_otentikasi'] = "Dokumen surat tidak ditemukan <br> ada kemungkinan surat ini tidak asli";
				$data['kode_otentikasi'] = $this->uri->segment(4);

				$is_otentik = FALSE;
			} else {
				$data['status_otentikasi'] = 'valid';
				$data['hasil_otentikasi'] = "Dokumen teridentifikasi asli";
				// $data['kode_otentikasi'] = $this->uri->segment(4);
				$data['kode_otentikasi'] = $id_surat_keluar;

				$is_otentik = TRUE;
			}

			## 3. SET VARIABEL OTENTIKASI
			if ($is_otentik === false){

			} elseif ($is_otentik === true) {
				## 2. API GET DETAIL SURAT (AUTOMASI)
				//$data['detail_surat_automasi'] = $this->Automasi_model->get_detail_skr_automasi($id_surat_keluar);
				// $data['link_pdf'] = base_url('pgw/act_surat_keluar_mhs/cetak_surat_tersimpan/'.$data["surat_keluar"]["umum"]["ID_SURAT_KELUAR"]);
				$data['link_pdf'] = base_url('mhs_surat_keluar/cetak_surat_tersimpan/'.$data["surat_keluar"]["umum"]["ID_SURAT_KELUAR"]);
				$data['link_pdf_gviewer'] = 'https://docs.google.com/viewer?url='.$data["link_pdf"];




				## 3. API GET ke SIMPEG untuk mengambil data pelengkap
				#3.1 unit simpeg
				// $parameter = array('api_kode' => 1901, 'api_subkode' => 2, 'api_search' => array($data['surat_keluar']['umum']['TGL_SURAT'], $data['surat_keluar']['umum']['UNIT_ID'])); 
				// $data['unit'] = $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);

				#3.2 penandatanganan tnde
				// $parameter							= array('api_kode' => 4004, 'api_subkode' => 1, 'api_search' => array($data['surat_keluar']['umum']['KD_JABATAN_2']));
				// $data['penandatangan_surat']	= $this->apiconn->api_tnde('tnde_pegawai/pegawai_by_kd_jabatan', 'json', 'POST', $parameter);
				//$psd = $this->apiconn->api_tnde('tnde_general/get_data', 'json', 'POST', array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('MD_PSD', 'ID_PSD', $data['surat_keluar']['umum']['ID_PSD'])));				
			}
			
		} else {
			$data['pesan'] = "Scan qrcode yang tertera pada surat untuk mengidentifikasi keaslian surat";
		}
		
		$this->load->view('pgw/templates/v_header');
		$this->load->view('v_sidebar');
		$this->load->view('v_cek_keaslian_surat_publik', $data);
		$this->load->view('pgw/templates/v_footer');
	}


	public function test_penyamaran($str_asli='')
	{		
		echo Kripto::urlsafeB64Encode($str_asli);
	}

	#VERSI SURAT YANG DIBUAT DARI LAMAN MHS_SURAT_KELUAR
	public function validasi_keaslian($id_surat_keluar=null, $signature='')
	{		
		$data['title'] = 'Validasi Keaslian Surat';
		
		if (!empty($id_surat_keluar)){
			## 1. API GET SURAT KELUAR + PENERIMA
			$data['surat_keluar'] = $this->Automasi_model->get_surat_keluar($id_surat_keluar);	

			## 2. SET VARIABEL OTENTIKASI
			if (empty($data['surat_keluar']) || empty($data['surat_keluar']['umum'])){#404 (surat tidak ditemukan di sistem)
				$data['status_otentikasi'] = 'invalid';
				$data['hasil_otentikasi'] = "Dokumen surat tidak ditemukan <br> ada kemungkinan surat ini tidak asli";
				$data['kode_otentikasi'] = $this->uri->segment(4);

				$is_otentik = FALSE;
			} else {
				$data['status_otentikasi'] = 'valid';
				$data['hasil_otentikasi'] = "Dokumen teridentifikasi asli";
				$data['kode_otentikasi'] = $this->uri->segment(4);

				$is_otentik = TRUE;
			}

			## 3. SET VARIABEL OTENTIKASI
			if ($is_otentik === false){

			} elseif ($is_otentik === true) {				
				$data['link_pdf'] = base_url('mhs_surat_keluar/tampilkan_surat/'.$data["surat_keluar"]["umum"]["ID_SURAT_KELUAR"]);
				$data['link_pdf_gviewer'] = 'https://docs.google.com/viewer?url='.$data["link_pdf"];						
			}
			
		} else {
			$data['pesan'] = "Scan qrcode yang tertera pada surat untuk mengidentifikasi keaslian surat";
		}
		
		$this->load->view('pgw/templates/v_header');
		$this->load->view('v_sidebar');
		$this->load->view('v_cek_keaslian_surat_publik', $data);
		$this->load->view('pgw/templates/v_footer');
	}

	public function umum()
	{		
		$id_surat_keluar = $this->input->get('by_id');

		$parameter          = array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('MD_SURAT_KELUAR2', 'ID_SURAT_KELUAR', $id_surat_keluar));
		$cek_skr = $this->apiconn->api_tnde('tnde_general/get_data', 'json', 'POST', $parameter);

		if(ISSET($cek_skr['ID_SURAT_KELUAR']) && $cek_skr['KD_SISTEM'] == 'AUTOMASI'){
			exit("Surat ini benar diterbitkan melalui Sistem Informasi Automasi Surat UIN Sunan Kalijaga");
		} else {
			exit("Surat yang anda cari tidak ditemukan dalam basis data Sistem Informasi Automasi Surat UIN Sunan Kalijaga");
		}
	}
}

/* End of file Cek_surat.php */
/* Location: ./application/modules/publik/controllers/Cek_surat.php */