<div class="app-row">
	<div class="col-med-3">
		<h2>Login</h2>
		<br>
		<div class="login-form">
			<form method="post" action="<?php echo base_url(); ?>auth/login/aksi_login">
				<div class="form-group">
					<input type="text" name="username" id="username" class="form-control" placeholder="Username" autofocus >
				</div>
				<div class="form-group">
					<input type="password" name="password" id="password" class="form-control" placeholder="Password" >
				</div>
				<button type="submit" class="btn-uin btn btn-inverse btn btn-small" style="float:right;">Login</button><br>
			</form>
		</div>
		<br>
		<?php
		$sess_errors = $this->session->flashdata('errors');

		if(!empty($sess_errors)){
			echo '<div class="bs-callout bs-callout-error" style="margin-bottom:5px">';
			if(is_array($sess_errors)){
				foreach($sess_errors as $value){
					echo "- ".$value."<br/>";
				}
			}else{
				echo $sess_errors;
			}
			echo '</div>';
		}

		?>
		<div class="login-links">
			<div class="login-link">
			</div>
			<div class="login-link">
				<a class="link-icon monitoring-surat" href="http://surat.uin-suka.ac.id/surat/cek_surat" title="">
				Cek Surat
				</a>
			</div>
		</div>  
	</div>
	<div class="col-med-9">
		<div class="app-blog">
			<!-- <h3 class="judul-02" style="margin:0;"><b>
			<a href="http://surat.uin-suka.ac.id/page/liputan/detail/1/tahun-baru-hijriyah" title="Tahun Baru Hijriyah">Tahun Baru Hijriyah</a>
			</b></h3>
			<span class="tgl-post">Selasa, 5 September 2017 06:15:48 WIB <span class="page_counter">Dilihat :  77 kali</span></span>
			<div class="clear5"></div>
				<div style="float:left; width: 320px; margin-top:7px;  margin-right:20px;">
					<img src="http://surat.uin-suka.ac.id/page/liputan/picture/1"  style="width: 320px;">
				</div>
				<div>
					<p>
					Lorem Ipsum Dolor sit Amet
						<a href="http://surat.uin-suka.ac.id/page/liputan/detail/1/tahun-baru-hijriyah"><b>(Selengkapnya)</b></a>
					</p>
					<div style="clear:both"></div><br>
				</div> -->
		</div>
		<!-- <a class="btn-uin btn btn-inverse btn btn-small" style="float:right" href="http://surat.uin-suka.ac.id/page/liputan"><i class="btn-uin"></i>Lainnya >></a>  -->
	</div>
</div>
<div class="clear20"></div>