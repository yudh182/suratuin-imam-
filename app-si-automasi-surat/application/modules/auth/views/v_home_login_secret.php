<?php
/**
* Konten utama halaman beranda dengan form login
*/
?>
<div class="container">
	<div class="row">
		<div class="col-md-6 col-sm-12 col-xs-12 col-md-offset-3">
			<div class="box featured-box">
				<div class="box-body">
					<h4 class="heading-primary align-center text-uppercase mb-md">Login</h4>
					<?php
					$sess_errors = $this->session->flashdata('errors');

					if(!empty($sess_errors)){
						echo '<div class="bs-callout bs-callout-error" style="margin-bottom:5px">';
						if(is_array($sess_errors)){
							foreach($sess_errors as $value){
								echo "- ".$value."<br/>";
							}
						}else{
							echo $sess_errors;
						}
						echo '</div>';
					}

					?>
					<div class="login-form">
						<form method="post" action="<?php echo site_url('auth/login_portal/aksi_login'); ?>" class="form-horizontal">
							<div class="form-group">
								<label class="col-md-3 col-sm-12">Username</label>
								<div class="col-md-9 col-sm-12">
									<input type="text" name="username" id="username" class="form-control" placeholder="Username" autofocus >	
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 col-sm-12">Username</label>
								<div class="col-md-9 col-sm-12">
									<input type="password" name="password" id="password" class="form-control" placeholder="Password" >
								</div>
							</div>
							<button type="submit" class="btn-uin btn btn-inverse btn btn-small" style="float:right;">Login</button><br>
						</form>
					</div>
					<br>			
					<div class="login-links">
						<div class="login-link">
						</div>
						<div class="login-link">
							<a class="link-icon monitoring-surat" href="<?= site_url('publik/cek_keaslian_surat'); ?>" title="Cek Keaslian Surat (Scan Kode QR)">
							Cek Surat
							</a>
						</div>
					</div> 
				</div>
			</div>

		</div><!-- /.col -->	
	</div><!-- /.row -->
</div><!-- /.container -->
<div class="clearfix"></div>