<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Login functionality
*/
class Login extends MY_Controller
{

  //private $api_mhs = '/mhs/api/get_mhs.php?nim=';
	function __construct()
	{
		parent::__construct();   				
	}

	public function index()
	{
		//exit('Tepat sekali, login dulu!!');
		if ($this->session->userdata('log') !== 'in'){
			$this->load->view('common/templates/v_header_resp_publik');
			$this->load->view('v_home_login');
			$this->load->view('publik/v_footer_publik');
		} else {						
			$redirect_backto = $this->input->get('from');
			
		  	if (!empty($redirect_backto) && !empty($this->session->userdata('username'))){ #ARAHKAN KEMBALI KE URL REFERER
				redirect($redirect_backto);
			}
			redirect('mhs_surat_keluar');
		}
	}  

	public function aksi_login(){
		$this->load->library('common/Curl');
		$this->load->model('common/Api_sia');
		$this->load->model('common/Api_simpeg');

		//dapatkan querystring url referer ke halaman login
		if (empty($_POST))
			exit('Unauthorized access!!');

		$ur = $_SERVER['HTTP_REFERER'];
		$ur_qs = parse_str( parse_url($ur, PHP_URL_QUERY),$uqfrom );
		$redirect_backto = isset ($uqfrom['from']) ? $uqfrom['from'] : null;

		
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$auth = '8f304662ebfee3932f2e810aa8fb628735';
		$api_url = 'http://service.uin-suka.ac.id/servad/adlogauthgr.php?aud='.$auth.'&uss='.$username.'&pss='.$password;      
		$hasil = $this->curl->simple_get($api_url, false, array(CURLOPT_USERAGENT => true)); 
		$hasil = json_decode($hasil, true);		

		$nama = $hasil[0]['NamaDepan'].' '.$hasil[0]['NamaBelakang'];
		$nim = $hasil[0]['NamaPengguna']; //username || nip || nim
		

		if (is_array($hasil)){ //jika autentikasi user pass berhasil
			$grup = $hasil[0]['AnggotaDari'][0];
			$grup_dibolehkan = array("MhsGroup","StaffGroup","KartuGroup","OrtuGroup"); 

			//if (in_array($grup, $grup_dibolehkan)){ $is_auth_allowed = TRUE;}
			if (in_array('MhsGroup', $grup)){
				$is_auth_sukses = TRUE;
				//get profil mhs
				$mhs = $this->Api_sia->get_profil_mhs($username);
				$data_session = array(
			  		'username'	=> $username,			  		
			  		// 'nama'		=> $nama,
			  		'grup'		=> 'mhs',			  		
			  		'log'		=> 'in'
			  	);
			  	if(!empty($mhs[0])){
				  	$data_session_tambahan = array(
				  		'nama'		=> $mhs[0]['NAMA_F'],
				  		'status'	=> $mhs[0]['NM_STATUS'],
				  		'kd_fakultas' => $mhs[0]['KD_FAK'], 
				  		'nm_fakultas'=> $mhs[0]['NM_FAK_J'],
				  		'kd_prodi'	=> $mhs[0]['KD_PRODI'],
						'nm_prodi'	=> $mhs[0]['NM_PRODI']
				  	);
				}
			} elseif (in_array('StaffGroup', $grup)) { # Pegawai (pejabat, pegawai biasa, dosen)
				$is_auth_sukses = TRUE;

	            # 1. ambil data profil simpeg
	            $pegawai = $this->Api_simpeg->get_profil_pegawai($username,'dpm');
	            
	            if ($username == '199205200000001201'){ #akun pengembang
	         		$hak_akses = array('MTR39','ASKRM39'); #hak akses komplit
	        	}
            	
            	$data_session = array(
			  		'username'	=> $username,
			  		'nama'		=> $nama,
			  		'grup'		=> 'pgw',			  		
			  		'log'		=> 'in'
			  	);			  	

            	# 2. ambil data jabatan (cek apakah termasuk pejabat struktural , atau pegawai pelaksana tata usaha)
            	$jab_str = $this->Api_simpeg->get_jabatan_struktural($username);
            	//$jab_fung = $this->Api_simpeg->get_jabatan_fungsional($username);

            	$struktural = jabatan_struktural($jab_str);	

            	if(!empty($struktural['PEJABAT'])){
            		#code
            	} else { # PEGAWAI PELAKSANA
            		$sess_nip = (!empty($simpeg_pgw[0]['NIP']) ? $simpeg_pgw[0]['NIP'] : '-');
            		$ses_str_id 				= (!empty($struktural['PELAKSANA'][0]['STR_ID']) ? $struktural['PELAKSANA'][0]['STR_ID'] : '');
            		$ses_str_nama 		= (!empty($struktural['PELAKSANA'][0]['STR_NAMA']) ? $struktural['PELAKSANA'][0]['STR_NAMA'] : '');					
					$ses_unit_id	 			= (!empty($struktural['PELAKSANA'][0]['UNIT_ID']) ? $struktural['PELAKSANA'][0]['UNIT_ID'] : '');
					$ses_unit_nama		= (!empty($struktural['PELAKSANA'][0]['UNIT_NAMA']) ? $struktural['PELAKSANA'][0]['UNIT_NAMA'] : '');					
					
					$data_session_tambahan = array(
						'nip'	   => $sess_nip,
            			'str_id'	=> $ses_str_id,
						'str_nama'	=> $ses_str_nama,
						'unit_id'	=> $ses_unit_id,
						'unit_nama'	=> $ses_unit_nama,
						'status_jabatan'	=> 'PELAKSANA'
            		);
            	}            	
	      	} elseif (in_array('KartuGroup', $grup)) {
	      		$is_auth_sukses = FALSE;
	            $error = [
	      			'kd_error' => '2',
	      			'error_message'=> 'Maaf, login kategori pengguna ini masih dalam perbaikan'
	      		];
	      	} else {
	      		$is_auth_sukses = FALSE;
	      		$error = [
	      			'kd_error' => '3',
	      			'error_message'=> 'Maaf, anda tidak masuk kategori pengguna yang dapat memasuki sistem ini'
	      		];
	      	}
  		} else { //
  			$is_auth_sukses = FALSE;
  			switch($hasil){
				case 1:										
			  		$this->session->set_flashdata('errors','Akses Ditolak'); /*$this->output->set_content_type('application/json', 'utf-8')
			  			->set_output(json_encode(["error_type"=>1, "error_msg"=>"Akses Ditolak"])); */
			  		redirect('auth/login?error=1');				        
					break;
				case 2:					
					$this->session->set_flashdata('errors','Akses Ditolak');
					redirect('auth/login?error=2');
					break;
				case 3:					
					$this->session->set_flashdata('errors','Gagal terhubung dengan server');
					redirect('auth/login?error=3');
					break;
				case 4:					
					$this->session->set_flashdata('errors','Username atau password salah');
					redirect('auth/login?error=4');
					break;
				case 5:					
					$this->session->set_flashdata('errors','Gagal mengambil data');
					redirect('auth/login?error=5');			
					break;
				case 6:					
					$this->session->set_flashdata('errors','Maaf Anda belum terdaftar di database pegawai. Silahkan hubungi PTIPD');
					redirect('auth/login?error=6');			
					break;
				default:
					redirect('auth?error=1');
			}
  		}


		if ($is_auth_sukses === TRUE) {		  	
		  	$this->login_sukses($redirect_backto, $data_session['grup'], $data_session, $data_session_tambahan);
	  		
  		} else{
  			exit(json_encode($error));
  			redirect(base_url('auth/login'));
  		}
	}

	protected function login_sukses($url_sebelumnya=null, $grup_user=null, $initial_session, $additional_session)
	{
			$this->session->set_userdata($initial_session);
		  	$this->session->set_userdata($additional_session);

		  	if (!empty($url_sebelumnya)){ #ARAHKAN KEMBALI KE URL REFERER
				redirect($url_sebelumnya);
			} else {
				if ($grup_user == 'mhs') {			  		
			  		redirect(base_url('mhs_surat_keluar'));
			  	} elseif ($grup_user == 'pgw') {
			  		redirect(base_url('administrasi_surat'));
			  	} else {
			  		redirect(base_url('pgw'));
			  	}
			}

	}


	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('pgw/login'));
	}

}