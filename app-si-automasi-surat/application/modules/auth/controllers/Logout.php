<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');
class Logout extends MY_Controller{
	function __construct(){
		parent::__construct();
		
		$this->session->sess_destroy();
		session_destroy();
		
	}

	public function index()
	{
		redirect(base_url('auth/login'));
	}
}
?>