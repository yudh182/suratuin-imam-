<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Login functionality
*/
class Autorisasi extends MY_Controller
{

  //private $api_mhs = '/mhs/api/get_mhs.php?nim=';
	function __construct()
	{
		parent::__construct();   				
	}

	/** 
	 * Cek akses menu yang dimiliki user tertentu
	 * @return array
	 */
	public function cek_akses_menu($kd_user='', $grup='')
	{
		$this->load->model('common/Api_automasi_surat','autosurat');

		#2. Daftar jenis surat				
		$list_subjenis_surat = $this->autosurat->get_jenis_surat('aktif');		
		//$menu_surat = [];
		
		$grup = 'MhsGroup';

		if ($grup == 'MhsGroup'){
			$sidebar_menu = [
				[ #menu ke-1
					'title' => 'Surat Keluar Mahasiswa',
					'link' => site_url('mhs_surat_keluar'),
					'hr_after' => true,
					'submenus' => []
				],
				[ #menu ke-2
					'title' => 'Log Out',
					'link' => site_url('auth/logout'),
				] 
			];

			foreach ($list_subjenis_surat as $k=>$v) {
				// $menu_surat[$k]['title'] = $v['NM_JENIS_SAKAD'];
				// $menu_surat[$k]['link'] = site_url('mhs_surat_keluar/buat') . $v['URLPATH'];
				//push
				$sidebar_menu[0]['submenus'][$k]['title'] = $v['NM_JENIS_SAKAD'];
				$sidebar_menu[0]['submenus'][$k]['link'] = site_url('mhs_surat_keluar/buat/') . $v['URLPATH'];
			}

			$queue = [
				'title' => 'Buat Surat',							
				'link' => null
			];

			array_unshift($sidebar_menu[0]['submenus'], $queue);

			$sidebar_menu[0]['submenus'][] = [
				'title' => 'Riwayat Persuratan',
				'link' => site_url('mhs_surat_keluar/riwayat')
			];
			$sidebar_menu[0]['submenus'][] = [						
				'title' => 'Monitor Pengajuan Surat',
				'link' => site_url('mhs_surat_keluar/monitoring')				
			];
			
			echo json_encode($sidebar_menu);

		} elseif ($grup == 'StaffGroup') {
			# code...
		}		
	}
		

	/** 
	 * @return html
	 */
	public function gen_sidebar_menu($arr=array())
	{

	}

	public function unset_sidebar()
	{

	}


}