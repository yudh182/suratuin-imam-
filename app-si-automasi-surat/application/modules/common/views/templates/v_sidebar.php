<div class="col-med-3">
	<div id="sidebar-sia">
		<?php
		$modul					= $this->uri->segment(2);
		$auto_surat_mhs   = ($modul == 'automasi_surat_mhs' ? TRUE : FALSE);
		$mhs_surat_keluar   = ($this->uri->segment(1) == 'mhs_surat_keluar' ? TRUE : FALSE);
		$psd_digital   = ($modul == 'psd_digital' ? TRUE : FALSE);
		$master_auto_surat_mhs   = ($modul == 'master_auto_surat_mhs' ? TRUE : FALSE);

		$log_username = $this->session->userdata('username');
		if ($log_username == '11650021XX' OR $log_username == '15650051XX'){
			$hak_akses = ['SKR0001', 'SKR1001', 'PSD0001', 'MTR0001','SET0001'];
		} else {
			$hak_akses = ['SKR1001'];
		}
		
		?>
		<nav class="accordion">
			<ol>				
				<li>
					<div class="sia-profile" style="margin-bottom:10px;width:223px">
						<?php if ($this->session->userdata('grup') == 'mhs') : ?>
						<img class="sia-profile-image" alt="foto pengguna" src="<?= $this->api_foto->foto_mhs($this->session->userdata('username')); ?>">
						<?php endif; ?>

						<?php if ($this->session->userdata('grup') == 'pgw')
						echo '<img class="sia-profile-image" alt="foto pengguna" src="'.$this->api_foto->foto_pgw($this->session->userdata("username")).'" />';
						?>

						<h2><?php  echo $this->session->userdata('nama'); ?></h2>		
						<p style="text-align:center; font-weight:bold;"><?php echo $this->session->userdata('username'); ?></p>

						<hr>
						<?php if ($this->session->userdata('grup') == 'mhs') : ?> 
						<table>
							<tbody>
								<tr>
									<td style="width:100px;height:25px;">
										<p class="fakultas-profil"><b>Fakultas</b></p>
									</td>
									<td> <?= $this->session->userdata('nm_fakultas'); ?></td>
								</tr>
								<tr>
									<td style="width:100px;height:25px;">
										<p class="prodi-profil"><b>Program Studi</b> </p>
									</td>
									<td> <?= $this->session->userdata('nm_prodi'); ?></td>
								</tr>
							</tbody>
						</table>
						<?php endif; ?>



						<b><?php echo (!empty($this->session->userdata('str_nama')) ? $this->session->userdata('str_nama') : ''); ?></b>

					</div>
					<div id="separate"></div>
				</li>			

				<?php if (in_array('SKR1001', $hak_akses)) : ?>
				<!-- Testing accordion menu -->
				<li>
					<!-- versi normal link-->
					<a class="item full" href="<?php echo base_url('mhs_surat_keluar'); ?>">
						<span>
							Surat Keluar Mahasiswa
							<!-- <span class="pull-right-container"><i class="fa fa-angle-down pull-right"></i></span> -->
							<span>
								<div class="underline-menu"></div>
							</span>
						</span>
					</a>

					<!-- <a class="item full" data-toggle="collapse" data-parent=".accordion" href="#collapse-menu-mhs-surat-keluar">
						<span>
							Surat Keluar Mahasiswa
							<span class="pull-right-container"><i class="fa fa-angle-down pull-right"></i></span>
							<span>
								<div class="underline-menu"></div>
							</span>
						</span>
					</a> -->
					

					<?php if ($mhs_surat_keluar == TRUE): ?>
					<ol id="collapse-menu-mhs-surat-keluar" class="submenus-container" style="display: block;">
						<li class="submenu">
							<span>
								<b>Buat Surat</b>
							</span>
						</li>
						<li class="submenu">							
							<?php if (isset($list_jenis_sakad)): ?>
							<ul class="container-childmenu">
								<?php foreach ($list_jenis_sakad as $key => $val) : ?>
									<li class="submenu">
										<?php $menu_link = base_url('mhs_surat_keluar/buat/').$val["URLPATH"]; ?>
										<a href="<?= $menu_link; ?>">
											<?= $val['NM_JENIS_SAKAD']; ?>
										</a>
									</li>
								<?php endforeach; ?>
							</ul>
						<?php  endif; ?>
						</li>				


						<li class="submenu">
							<hr>
						</li>
						<li class="submenu">
							<a href="<?php echo base_url('mhs_surat_keluar/riwayat'); ?>">
								<span>Riwayat</span>
								<div class="underline-menu"></div>
							</a>
						</li>																		
						<li class="submenu">
							<a href="<?php echo base_url('mhs_surat_keluar/monitoring'); ?>">							
								<span>Monitor Pengajuan Surat <span class="notif-active">1</span></span>
								<div class="underline-menu"></div>
							</a>
						</li>
					</ol>
					<?php endif; ?>				
				</li>
				<?php endif; ?>	

				<!-- informasi -->
				<li>
					<a class="item full" href="<?php echo site_url('informasi'); ?>">
						<span>
							Informasi
							<span>
								<div class="underline-menu"></div>
							</span>
						</span>
					</a>
					<?php if ($this->uri->segment(1) == 'informasi') : ?>
					<ol class="" style="display: block;">
						<li class="submenu">
							<a href="<?php echo site_url('informasi'); ?>">
								Cara Penggunaan Sistem
							</a>
						</li>
						<li class="submenu">
							<a href="<?php echo site_url('informasi'); ?>">
								Info Persuratan
							</a>
						</li>
					</ol>
				<?php endif; ?>
				</li>
				<!-- ./informasi -->

					
				<?php if (in_array('SKR0001', $hak_akses)) : ?>
				<li>
					<a class="item full" href="<?php echo base_url('pgw/automasi_surat_mhs'); ?>">
						<span>
							Automasi Surat Mahasiswa (<b><i>v1 deprecated</i></b>)
							<span>
								<div class="underline-menu"></div>
							</span>
						</span>
					</a>

					<?php if ($auto_surat_mhs == TRUE): ?>
					<ol class="" style="display: block;">
						<li class="submenu">
							<a href="<?php echo site_url('pgw/automasi_surat_mhs/buat'); ?>" class="submenu-title">
								<b>Buat</b>
							</a>
							<?php if (isset($list_jenis_sakad)): ?>
							<ul class="container-childmenu">
								<?php foreach ($list_jenis_sakad as $key => $val) : ?>
									<li class="submenu">
										<a href="<?php echo base_url('pgw/automasi_surat_mhs/buat/'); echo $val['URLPATH']; ?>">
											<?= $val['NM_JENIS_SAKAD']; ?>
										</a>
									</li>
								<?php endforeach; ?>
							</ul>
						<?php  endif; ?>
						</li>				


						<li class="submenu">
							<hr>
						</li>
						<li class="submenu">
							<a href="<?php echo base_url('pgw/automasi_surat_mhs/riwayat'); ?>">
								Riwayat
							</a>
						</li>



						<li class="submenu">
							<hr>
						</li>	
						<li class="submenu">
							<b>Administrasi</b>							
						</li>

						<li class="submenu">
							<a href="<?php site_url('pgw/automasi_surat_mhs/laman_pengurusan'); ?>">
								Laman Pengurusan
							</a>
						</li>
						<li class="submenu">
							<a href="<?php site_url('pgw/automasi_surat_mhs/arsip'); ?>">
								Arsip
						</li>
						
						<li class="submenu">
							<hr>
						</li>
					</ol>
					<?php endif; ?>				
				</li>
				<?php endif; ?>
				

				<?php if (in_array('PSD0001', $hak_akses)) : ?>
				<li>
					<a class="item full" href="<?php echo base_url('pgw/psd_digital'); ?>">
						<span>
							Penandatanganan Digital
							<span>
								<div class="underline-menu"></div>
							</span>
						</span>
					</a>

					<?php if ($psd_digital == TRUE): ?>
					<ol>
						<li class="submenu">
							<a href="<?= site_url('pgw/psd_digital/inbox'); ?>">
								Inbox
							</a>
						</li>
						<li class="submenu">
							<a href="<?= site_url('pgw/psd_digital/arsip/1'); ?>">
								Arsip Ditandatangani
							</a>
						</li>
						<li class="submenu">
							<a href="<?= site_url('pgw/psd_digital/arsip/0'); ?>">
								Arsip Ditolak
							</a>
						</li>
						<li class="submenu">
							<a href="<?= site_url('pgw/psd_digital/pengaturan'); ?>">
								Pengaturan
							</a>
						</li>
						<li class="submenu">
						<hr>
						</li>																		
					</ol>
					<?php endif; ?>
				</li>
				<?php endif; ?>
				
				<?php if (in_array('MTR0001', $hak_akses)) : ?>
				<li>
					<a class="item full" href="<?php echo site_url('pgw/master_auto_surat_mhs/jenis');?>">
						<!-- <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span> -->
						<span>
							Master Data Automasi Surat
							<span>
								<div class="underline-menu"></div>
							</span>
						</span>
					</a>

					<?php if ($master_auto_surat_mhs == TRUE): ?>
					<ol id="menu-master-automasi" class="submenus-container" style="display: block;">
						<li class="submenu">
							<a href="<?= site_url('pgw/master_auto_surat_mhs/jenis'); ?>">
								Jenis Surat Mahasiswa
							</a>
						</li>
						<li class="submenu">
							<a href="<?= site_url('pgw/master_auto_surat_mhs/repository_persyaratan'); ?>">
								Master Persyaratan Verifikasi
							</a>
						</li>
						<li class="submenu">
							<a href="<?= site_url('pgw/master_auto_surat_mhs/config_automasi'); ?>">
								Pengaturan Automasi Data
							</a>
						</li>
						<li class="submenu">
							<hr>
						</li>																		
					</ol>										
					<?php endif; ?>	
				</li>
				<?php endif; ?>

				<li>
					<a class="item full" href="<?php echo base_url(); ?>auth/logout">
						<span>Logout</span>
						<div class="underline-menu"></div>
					</a>
				</li>
			</ol>
		</nav>
	</div>
</div>	