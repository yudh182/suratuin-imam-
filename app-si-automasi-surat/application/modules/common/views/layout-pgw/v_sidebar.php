<div class="col-med-3">
	<div id="sidebar-sia">
		<?php
		$modul = $this->uri->segment(1);
		$administrasi   = ($modul == 'administrasi' ? TRUE : FALSE);
		$admin_master   = ($modul == 'master_settings' ? TRUE : FALSE);
		$psd_elektronik   = ($modul == 'psd_elektronik' ? TRUE : FALSE);

		$log_username = $this->session->userdata('username');		
		//sementara pegawai juga sebagai admin		
		?>
		<nav class="accordion">
			<ol>
				<!-- start user profile -->				
				<li>
					<div class="sia-profile" style="margin-bottom:10px;width:223px">						
						<?php if ($this->session->userdata('grup') == 'pgw')
						echo '<img class="sia-profile-image" alt="foto pengguna"  src="'.$this->api_foto->foto_pgw($this->session->userdata("username")).'" />';
						?>

						<h2><?php  echo $this->session->userdata('nama'); ?></h2>		
						<p style="text-align:center; font-weight:bold;"><?php echo trans_nip_pgw($this->session->userdata('username')); ?></p>
						<hr>						

						<b><?php echo (!empty($this->session->userdata('str_nama')) ? $this->session->userdata('str_nama') : ''); ?></b>

					</div>
					<div id="separate"></div>
				</li>
				<!-- end user profie -->

				<!-- start app menu -->
				<li>
					<a class="item full" href="<?php echo base_url('pgw/psd_digital'); ?>">
						<span>
							Administrasi Surat
							<span>
								<div class="underline-menu"></div>
							</span>
						</span>
					</a>

					<?php if ($administrasi == TRUE): ?>
					<ol>						
						<li class="submenu">
							<a href="<?php echo site_url('administrasi/terima_pengajuan'); ?>">
								Terima Pengajuan Surat
							</a>
						</li>
						<li class="submenu">
							<a href="<?php echo site_url('administrasi/arsip_unit'); ?>">
								Arsip Surat
						</li>
						<li class="submenu">
							<a href="<?php echo site_url('administrasi/psd_elektronik'); ?>">
								Arsip Penandatanganan Elektronik
							</a>
						</li>												
						<li class="submenu">
							<hr>
						</li>
					</ol>					
				</li>				
								
				<li>
					<a class="item full" href="<?php echo base_url('informasi'); ?>">
						<span>
							Informasi
							<span>
								<div class="underline-menu"></div>
							</span>
						</span>
					</a>
					
					<ol style="display: none;">
						<li class="submenu">
							<a href="<?= site_url('informasi'); ?>">
								Lihat Informasi
							</a>
						</li>
						<li class="submenu">
							<a href="<?= site_url('informasi/kelola'); ?>">
								Kelola Informasi
							</a>
						</li>																								
					</ol>
					
				</li>
				<?php endif; ?>
				
				<?php if ($this->session->userdata('username') == '1650021') : ?>
				<li>
					<a class="item full" href="<?php echo site_url('pgw/master_auto_surat_mhs/jenis');?>">
						<!-- <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span> -->
						<span>
							Master Pengaturan (Admin)
							<span>
								<div class="underline-menu"></div>
							</span>
						</span>
					</a>

					<?php if ($admin_master == TRUE): ?>
					<ol id="menu-master-automasi" class="submenus-container" style="display: block;">
						<li class="submenu">
							<a href="<?= site_url('pgw/master_auto_surat_mhs/jenis'); ?>">
								Jenis Surat Mahasiswa
							</a>
						</li>
						<li class="submenu">
							<a href="<?= site_url('pgw/master_auto_surat_mhs/repository_persyaratan'); ?>">
								Master Persyaratan Verifikasi
							</a>
						</li>
						<li class="submenu">
							<a href="<?= site_url('pgw/master_auto_surat_mhs/config_automasi'); ?>">
								Pengaturan Automasi Data
							</a>
						</li>
						<li class="submenu">
							<hr>
						</li>																		
					</ol>										
					<?php endif; ?>	
				</li>
				<?php endif; ?>


				<li>
					<a class="item full" href="<?php echo base_url(); ?>auth/logout">
						<span>Logout</span>
						<div class="underline-menu"></div>
					</a>
				</li>
			</ol>
		</nav>
	</div>
</div>	