<?php 
defined('BASEPATH') OR exit('No direct script acces allowed');

/**
* 
*/
require_once dirname(__FILE__).'/tcpdf/tcpdf.php';
require_once(dirname(__FILE__).'/tcpdf/tcpdf_barcodes_2d.php');

class Pdf_creator extends TCPDF
{	
	public $template; #array
     
    public function setData($template){
    	$this->template = $template;
    }

	public function qrcode_format()
	{
		$barcodeobj = new TCPDF2DBarcode('http://www.tcpdf.org', 'QRCODE,H');

		// export as SVG image
		//$barcodeobj->getBarcodeSVG(3, 3, 'black');

		// export as PNG image
		//$barcodeobj->getBarcodePNG(3, 3, array(0,128,0));

		// export as HTML code
		echo $barcodeobj->getBarcodeHTML(3, 3, 'black');
	}

	public function set_document_properties($data)
	{
		$this->SetCreator(PDF_CREATOR);				
		$this->SetAuthor('Sistem Informasi Automasi Persuratan UIN Sunan Kalijaga');
		$this->SetKeywords('Laporan, Sistem Informasi Surat, Sistem Automasi Persuratan, UIN Sunan Kalijaga');
		$this->SetTitle($data['pdf_title']);
		//$this->SetSubject($data['pdf_title']);

		# SET UI/Layout
		$this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		$this->setPrintHeader(true);
		$this->setPrintFooter(true);

		// set margins of document
		if(isset($data['pdf_margin'])){ 
			$this->SetMargins($data['pdf_margin'][0], $data['pdf_margin'][1], $data['pdf_margin'][2], true); 
			// $this->setBottomMargin('')
		} else { 
			//$this->SetMargins(20, 10, 20, true); 
			$this->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT); #15, 35, 15			
		}

		if(isset($data['pdf_template_margin'])){ 
			$this->SetHeaderMargin($data['pdf_template_margin'][0]); 
			$this->SetFooterMargin($data['pdf_template_margin'][1]);
		} else { 
			$this->SetHeaderMargin(PDF_MARGIN_HEADER); #5
			$this->SetFooterMargin(PDF_MARGIN_FOOTER); #10
		}
		

		// set auto page breaks
		if (isset($data['pdf_page_break'])){
			$this->SetAutoPageBreak($data['pdf_page_break'][0], $data['pdf_page_break'][1]);
		} else {
			$this->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); #25	
		}
		
		// set image scale factor
		$this->setImageScale(PDF_IMAGE_SCALE_RATIO);	

		// set cell padding
		$this->setCellPaddings(0.5, 0.5, 0.5, 0.5); #setting sebelumnya : $pdf->setCellPaddings(1, 1, 1, 1);

		// set cell margins
		$this->setCellMargins(0, 1, 0, 1); #setting sebelumnya : $pdf->setCellMargins(2, 3, 2, 3);		

		// set color for background
		$this->SetFillColor(255, 255, 127);	

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}	

		// set header and footer fonts
		$this->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$this->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));	
	}

	public function set_page_portrait1()
	{		
	}

	public function set_page_landscape()
	{

	}

	## OVERRIDE DEFAULT PAGE HEADER
	public function Header() {		 				        
        // Title
        //$this->Cell(0, 15, '<< TCPDF Example 003 >>', 0, false, 'C', 0, '', 0, false, 'M', 'M');				
        $kop_data = $this->template['kopsurat'];
        $this->Image(FCPATH.'/assets/images/logouin.jpg', 0, PDF_MARGIN_HEADER, 21, 26, '','','',true,300,'L');

		//$this->SetFont('helvetica', '', 10);						
        $kop_html = '<style type="text/css">        
		h1 { font-weight: normal; font-size: 14pt;}
		h2 { font-weight: normal; font-size: 12pt;}
		h3 { font-weight: normal; font-size: 9pt;}
		hr { height: 1.5px;}
		</style>
		<table>
		<tr>
        	<td><h1><b>'.$kop_data["kementrian"].'</b></h1></td>
        </tr>        
        <tr>        	
        	<td><h2>'.$kop_data["univ"].'</h2></td>
        </tr>';
        $kop_html .= '<tr><td><h2>'.strtoupper($kop_data["fkl"] ).'</h2></td></tr>';
		$kop_html .='<tr><td><h3>'.$kop_data["almt"].'</h3></td></tr>';
		$kop_html .='<tr><td><h3>Telepon '.$kop_data["tlp"].', Faksimili '.$kop_data["fax"].'</h3></td></tr>';
		$kop_html .='<tr><td><h3>Website : '.$kop_data["web"].'</h3></td></tr></table>
		<hr>';
		
        //$this->WriteHTMLCell(0,0,'','',$kop1, 0, 1, 0, true, 'C', true);
        $this->WriteHTMLCell(0,0,'','',$kop_html, 0, 1, 0, true, 'C', true);
	}


	## OVERRIDE DEFAULT PAGE FOOTER
	public function Footer() {		
		//$this->SetY(-10);
		$this->SetFont('helvetica', '', 10);

		//$this->writeHTMLCell(0, 0, '', '', '<p>surat ini dicetak melalui sistem informasi surat pada '.date("d/m/Y h:i:s").'</p>', 0, 0, false, "C", true);
		$html_footer = '<hr>
		<table>
			<tr>
				<td align="L" colspan="3">Diterbitkan melalui Sistem Informasi Surat UIN Sunan Kalijaga Yogyakarta</td>
				<td align="R">'.$this->getAliasNumPage().'/'.$this->getAliasNbPages().'</td>
			</tr>
			<tr>
				<td align="L" colspan="3">Untuk membuktikan keaslian surat silakan scan kode QR-Code diatas</td>
				<td></td>
			</tr>
		</table>';		
		$this->writeHTMLCell(0, 0, '', '', $html_footer, 0, 1, 0, true, "L", true);
		//$this->writeHTMLCell(0, 0, '', '', $html_footer2, 0, 1, 0, true, "R", true);
		//$this->writeHTMLCell(0, 0, '', '', $html_footer2, 0, 1, 0, true, "R", true);
		//$this->Cell(0, 0, 'surat ini dibuat melalui sistem automasi surat pada '.date('d/m/Y h:i:s'), 0, false, 'L', 0, '', 0, false, 'T', 'M');		
		// Page number		        
		//$this->Cell(0, 120, ''.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
	}

}