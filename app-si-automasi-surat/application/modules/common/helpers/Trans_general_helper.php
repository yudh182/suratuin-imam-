<?php
if(! defined('BASEPATH'))exit('No direct script access allowed');
/**
====================================
* Transformasi antar format satuan
* angka, tanggal, hingga format NIP
====================================
*/

/**
* Transformasi antar format kepegawaian
*/
function trans_nip_pgw($nip=""){
    $nip = str_replace(" ","",trim($nip));
    return substr($nip,0,8)." ".substr($nip,8,6)." ".substr($nip,14,1)." ".substr($nip,15,3);
}




/**
*Transformasi antar format angka
*/
function trans_angka_romawi_v1($number, $upcase = true){ 
    $table = array('M'=>1000, 'CM'=>900, 'D'=>500, 'CD'=>400, 'C'=>100, 'XC'=>90, 'L'=>50, 'XL'=>40, 'X'=>10, 'IX'=>9, 'V'=>5, 'IV'=>4, 'I'=>1); 
    $return = ''; 
    while($number > 0){ 
        foreach($table as $rom=>$arb){ 
            if($number >= $arb){ 
                $number -= $arb; 
                $return .= $rom; 
                break; 
            } 
        } 
    } 

    return $return; 
}

function trans_angka_romawi($num){
     // Make sure that we only use the integer portion of the value
     $n = intval($num);
     $result = '';
 
     // Declare a lookup array that we will use to traverse the number:
     $lookup = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400,
     'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40,
     'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
 
     foreach ($lookup as $roman => $value) 
     {
         // Determine the number of matches
         $matches = intval($n / $value);
 
         // Store that many characters
         $result .= str_repeat($roman, $matches);
 
         // Substract that from the number
         $n = $n % $value;
     }
 
     // The Roman numeral should be built, return it
     return $result;
}