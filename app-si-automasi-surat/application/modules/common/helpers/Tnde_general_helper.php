<?php
if(! defined('BASEPATH'))exit('No direct script access allowed');

function kd_prodi_sia_by_kd_jabatan($kd_jabatan){
	switch($kd_jabatan){
		case 113: $hasil = '22508'; break; 
		case 154: $hasil = '22508'; break; 
		case 114: $hasil = '22513'; break; 
		case 155: $hasil = '22513'; break; 
		case 115: $hasil = '22510'; break; 
		case 156: $hasil = '22510'; break; 
		case 116: $hasil = 'DIP03'; break; 
		case 157: $hasil = 'DIP03'; break; 
		case 117: $hasil = '15'; break; 
		case 158: $hasil = '15'; break; 
		case 118: $hasil = '22407'; break; 
		case 159: $hasil = '22407'; break; 
		case 119: $hasil = '22403'; break; 
		case 160: $hasil = '22403'; break; 
		case 120: $hasil = '22411'; break; 
		case 161: $hasil = '22411'; break; 
		case 121: $hasil = '22408'; break; 
		case 162: $hasil = '22408'; break; 
		case 122: $hasil = '25'; break; 
		case 163: $hasil = '25'; break;
		case 123: $hasil = '22201'; break;
		case 164: $hasil = '22201'; break;
		case 124: $hasil = '22207'; break;
		case 165: $hasil = '22207'; break;
		case 125: $hasil = '22216'; break;
		case 166: $hasil = '22216'; break;
		case 126: $hasil = '22206'; break;
		case 167: $hasil = '22206'; break;
		case 127: $hasil = '22221'; break;
		case 168: $hasil = '22221'; break;
		case 128: $hasil = '40'; break;
		case 169: $hasil = '40'; break;
		case 129: $hasil = '22124'; break;
		case 170: $hasil = '22124'; break;
		case 130: $hasil = '22127'; break;
		case 171: $hasil = '22127'; break;
		case 131: $hasil = '22128'; break;
		case 172: $hasil = '22128'; break;
		case 132: $hasil = '22122'; break;
		case 173: $hasil = '22122'; break;
		case 133: $hasil = '22120'; break;
		case 174: $hasil = '22120'; break;
		case 134: $hasil = '22129'; break;
		case 175: $hasil = '22129'; break;
		case 135: $hasil = '22302'; break;
		case 176: $hasil = '22302'; break;
		case 136: $hasil = '22310'; break;
		case 177: $hasil = '22310'; break;
		case 137: $hasil = '22312'; break;
		case 178: $hasil = '22312'; break;
		case 138: $hasil = '22311'; break;
		case 179: $hasil = '22311'; break;
		case 139: $hasil = '22625'; break;
		case 180: $hasil = '22625'; break;
		case 140: $hasil = '22603'; break;
		case 181: $hasil = '22603'; break;
		case 141: $hasil = '22604'; break;
		case 182: $hasil = '22604'; break;
		case 142: $hasil = '22602'; break;
		case 183: $hasil = '22602'; break;
		case 143: $hasil = '22607'; break;
		case 184: $hasil = '22607'; break;
		case 144: $hasil = '22608'; break;
		case 185: $hasil = '22608'; break;
		case 145: $hasil = '22123'; break;
		case 186: $hasil = '22123'; break;
		case 146: $hasil = '22121'; break;
		case 187: $hasil = '22121'; break;
		case 147: $hasil = '22116'; break;
		case 188: $hasil = '22116'; break;
		case 148: $hasil = '22117'; break;
		case 189: $hasil = '22117'; break;
		case 149: $hasil = '22620'; break;
		case 190: $hasil = '22620'; break;
		case 150: $hasil = '22412'; break;
		case 191: $hasil = '22412'; break;
		case 151: $hasil = '22406'; break;
		case 192: $hasil = '22406'; break;
		case 152: $hasil = '81'; break;
		case 153: $hasil = '82'; break;
	}
	return $hasil;
}

function kd_prodi_sia_by_kd_bagian($kd_bagian){
	switch($kd_bagian){
		case 85: $hasil = '22607'; break;
		case 86: $hasil = '22608'; break;
		case 87: $hasil = '22625'; break;
		case 88: $hasil = '22603'; break;
		case 89: $hasil = '22604'; break;
		case 90: $hasil = '22602'; break;
		case 91: $hasil = '22123'; break;
		case 92: $hasil = '22121'; break;
		case 93: $hasil = '22116'; break;
		case 94: $hasil = '22117'; break;
		case 101: $hasil = '22508'; break;
		case 102: $hasil = '22513'; break;
		case 103: $hasil = '22510'; break;
		case 104: $hasil = 'DIP03'; break;
		case 105: $hasil = '15'; break;
		case 106: $hasil = '22407'; break;
		case 107: $hasil = '22403'; break;
		case 108: $hasil = '22411'; break;
		case 109: $hasil = '22408'; break;
		case 110: $hasil = '25'; break;
		case 111: $hasil = '22201'; break;
		case 112: $hasil = '22207'; break;
		case 113: $hasil = '22216'; break;
		case 114: $hasil = '22206'; break;
		case 115: $hasil = '22221'; break;
		case 116: $hasil = '40'; break;
		case 117: $hasil = '22124'; break;
		case 118: $hasil = '22127'; break;
		case 119: $hasil = '22128'; break;
		case 120: $hasil = '22122'; break;
		case 121: $hasil = '22120'; break;
		case 122: $hasil = '0261'; break;
		case 123: $hasil = '22302'; break;
		case 124: $hasil = '22310'; break;
		case 125: $hasil = '22312'; break;
		case 126: $hasil = '22311'; break;
		case 127: $hasil = '22620'; break;
		case 128: $hasil = '22412'; break;
		case 129: $hasil = '22406'; break;
		case 130: $hasil = '81'; break;
		case 131: $hasil = '82'; break;
	}
	return $hasil;
}

function fixed_cari($data){
	$hasil = str_replace("/", "-", str_replace('"','', str_replace("'", "", str_replace(" ", "_", $data))));
	return $hasil;
}

function back_cari($data){
	$hasil = str_replace("-", "/", str_replace("_", " ", $data));
	return $hasil;
}

function fixed_isi_surat($data){
	$dd = str_replace("<body>", "", str_replace("<title>", "", str_replace("<head>", "", str_replace("<html>", "",$data))));
	$hasil = str_replace("</body>", "", str_replace("</title>", "", str_replace("</head>", "", str_replace("</html>", "", $dd))));
	$hasil = str_replace('<body dir="undefined">', '', $hasil);
	return $hasil;
}

function conv_to_int($data){
	$hasil = (int)preg_replace("/[^0-9]/", "", $data);
	return $hasil;
}

function jab_surat($data=''){
	if(count($data) > 0){
		$hasil = array();
		foreach($data as $val){
			$jab = substr($val['SIS_ID'], 0, 3);
			if($jab == 'SRT'){
				$hasil[] = array('SIS_ID' => $val['SIS_ID'], 'SIS_NAMA' => $val['SIS_NAMA']);
			}
		}
		return $hasil;
	}else{
		return NULL;
	}
}

function akses_sisurat($data=''){
	$jab_surat	= jab_surat($data);
	if(count($jab_surat) > 0){
		$module_list = array();
		foreach($jab_surat as $val){
			$kd_module 		= substr($val['SIS_ID'], 3, 3);
			if(! in_array($kd_module, $module_list)){
				$module_list[] = $kd_module;
			}
		}
		foreach($module_list as $val){
			$sub_modul[$val] = array();
			$hasil[$val]['KD_TU_BAGIAN'] = array();
		}
		foreach($jab_surat as $val){
			$kd_module 			= substr($val['SIS_ID'], 3, 3);
			$kd_sub_modul	= substr($val['SIS_ID'], 6, 2);
			$kd_tu_bagian 		= (Int)substr($val['SIS_ID'], 8, 2);
			switch($kd_module){
				case 'MTR': //MASTER
					if(! in_array($kd_sub_modul, $sub_modul[$kd_module])){
						$hasil[$kd_module]['KD_MASTER'][] = (Int)$kd_sub_modul;
						$sub_modul[$kd_module][] = (Int)$kd_sub_modul;
					}
					break;
				case 'AVT':
					$kd_jabatan = substr($val['SIS_ID'], 6);
					if(! in_array($kd_jabatan, $sub_modul[$kd_module])){
						$hasil[$kd_module]['SIS_ID'][] = (Int)$kd_jabatan;
						$sub_modul[$kd_module][] = (Int)$kd_jabatan;
					}
					break;
				default:
					if(! in_array($kd_sub_modul, $sub_modul[$kd_module])){
						$hasil[$kd_module]['KD_JENIS_SURAT'][] = (Int)$kd_sub_modul;
						$sub_modul[$kd_module][] = (Int)$kd_sub_modul;
					}
					break;
			}
			if(! in_array($kd_tu_bagian, $hasil[$kd_module]['KD_TU_BAGIAN'])){
				$hasil[$kd_module]['KD_TU_BAGIAN'][] = $kd_tu_bagian;
			}
		}
		return $hasil;
	}
}

function number_to_words($number) {
    
    $hyphen      = '-';
    $conjunction = ' dan ';
    $separator   = ', ';
    $negative    = 'negatif ';
    $decimal     = ' titik ';
    $dictionary  = array(
        0                   => 'nol',
        1                   => 'Satu',
        2                   => 'Dua',
        3                   => 'Tiga',
        4                   => 'Empat',
        5                   => 'Lima',
        6                   => 'Enam',
        7                   => 'Tujuh',
        8                   => 'Delapan',
        9                   => 'Sembilan',
        10                  => 'Sepuluh',
        11                  => 'Sebelas',
        12                  => 'Dua belas',
        13                  => 'Tiga belas',
        14                  => 'Empat belas',
        15                  => 'Lima belas',
        16                  => 'Enam belas',
        17                  => 'Tujuh belas',
        18                  => 'Delapan belas',
        19                  => 'Sembilan belas',
        20                  => 'Dua puluh',
        30                  => 'Tiga puluh',
        40                  => 'Empat puluh',
        50                  => 'Lima puluh',
        60                  => 'Enam puluh',
        70                  => 'Tujuh puluh',
        80                  => 'Delapan puluh',
        90                  => 'Sembilan puluh',
        100                 => 'Seratus',
        1000                => 'Seribu',
        1000000             => 'Seratus ribu',
        1000000000          => 'Satu triliyun',
        1000000000000       => 'Satu biliyun',
        1000000000000000    => 'quadrillion',
        1000000000000000000 => 'quintillion'
    );
    
    if (!is_numeric($number)) {
        return false;
    }
    
    if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
        // overflow
        trigger_error(
            'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
            E_USER_WARNING
        );
        return false;
    }

    if ($number < 0) {
        return $negative . convert_number_to_words(abs($number));
    }
    
    $string = $fraction = null;
    
    if (strpos($number, '.') !== false) {
        list($number, $fraction) = explode('.', $number);
    }
    
    switch (true) {
        case $number < 21:
            $string = $dictionary[$number];
            break;
        case $number < 100:
            $tens   = ((int) ($number / 10)) * 10;
            $units  = $number % 10;
            $string = $dictionary[$tens];
            if ($units) {
                $string .= $hyphen . $dictionary[$units];
            }
            break;
        case $number < 1000:
            $hundreds  = $number / 100;
            $remainder = $number % 100;
            $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
            if ($remainder) {
                $string .= $conjunction . convert_number_to_words($remainder);
            }
            break;
        default:
            $baseUnit = pow(1000, floor(log($number, 1000)));
            $numBaseUnits = (int) ($number / $baseUnit);
            $remainder = $number % $baseUnit;
            $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
            if ($remainder) {
                $string .= $remainder < 100 ? $conjunction : $separator;
                $string .= convert_number_to_words($remainder);
            }
            break;
    }
    
    if (null !== $fraction && is_numeric($fraction)) {
        $string .= $decimal;
        $words = array();
        foreach (str_split((string) $fraction) as $number) {
            $words[] = $dictionary[$number];
        }
        $string .= implode(' ', $words);
    }
    
    return $string;
}

function romanic_number($number, $upcase = true){ 
    $table = array('M'=>1000, 'CM'=>900, 'D'=>500, 'CD'=>400, 'C'=>100, 'XC'=>90, 'L'=>50, 'XL'=>40, 'X'=>10, 'IX'=>9, 'V'=>5, 'IV'=>4, 'I'=>1); 
    $return = ''; 
    while($number > 0){ 
        foreach($table as $rom=>$arb){ 
            if($number >= $arb){ 
                $number -= $arb; 
                $return .= $rom; 
                break; 
            } 
        } 
    } 

    return $return; 
}

// function host_allowed($referer=''){
	// if(!empty($referer)){
		// $data = parse_url($referer);
		// $host = $data['host'];
		// $allowed[] = "surat.uin-suka.ac.id";
		// if(in_array($host, $allowed)){
			// return TRUE;
		// }else{
			// return FALSE;
		// }
	// }else{
		// return FALSE;
	// }
// }

function host_allowed($referer=''){
	if(!empty($referer)){
		$host = domain($referer);
		$allowed[] = domain(base_url());
		if(in_array($host, $allowed)){
			return TRUE;
		}else{
			return FALSE;
		}
	}else{
		return FALSE;
	}
}

function domain($url=''){
  $pieces = parse_url($url);
  $domain = isset($pieces['host']) ? $pieces['host'] : '';
  if(preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)){
    return $regs['domain'];
  }
  return false;
}

function jabatan_struktural($data=''){
	$hasil['PEJABAT'] = array();
	$hasil['PELAKSANA'] = array();
	if(!empty($data)){
		foreach($data as $val){
			$kel_jab_str = array();
			$exp = explode("#", $val['GRP_C']);
			if(!empty($exp)){
				foreach($exp as $d){
					if(!empty($d)){
						$exp2 = explode("@", $d);
						$kel_jab_str[] = $exp2[0];
					}
				}
			}
			if(in_array('GA000007', $kel_jab_str)){ #kelompok pejabat
				$hasil['PEJABAT'][] = array(
					'KD_PGW' => $val['KD_PGW'],
					'NIP' => $val['NIP'],
					'NM_PGW_F' => $val['NM_PGW_F'],
					'NM_PGW_P' => $val['NM_PGW_P'],
					'STR_ID' => $val['STR_ID'],
					'STR_NAMA' => $val['STR_NAMA'],
					'STR_NAMA_S1' => $val['STR_NAMA_S1'],
					'STR_NAMA_S2' => $val['STR_NAMA_S2'],
					'UNIT_ID' => $val['UNIT_ID'],
					'UNIT_NAMA' => $val['UNIT_NAMA'],
					'UNIT_NAMA_S1' => $val['UNIT_NAMA_S1'],
					'UNIT_NAMA_S2' => $val['UNIT_NAMA_S2']
				);
			}else{
				$hasil['PELAKSANA'][] = array(
					'KD_PGW' => $val['KD_PGW'],
					'NIP' => $val['NIP'],
					'NM_PGW_F' => $val['NM_PGW_F'],
					'NM_PGW_P' => $val['NM_PGW_P'],
					'STR_ID' => $val['STR_ID'],
					'STR_NAMA' => $val['STR_NAMA'],
					'STR_NAMA_S1' => $val['STR_NAMA_S1'],
					'STR_NAMA_S2' => $val['STR_NAMA_S2'],
					'UNIT_ID' => $val['UNIT_ID'],
					'UNIT_NAMA' => $val['UNIT_NAMA'],
					'UNIT_NAMA_S1' => $val['UNIT_NAMA_S1'],
					'UNIT_NAMA_S2' => $val['UNIT_NAMA_S2']
				);
			}
		}
	}
	return $hasil;
}

function split_content($data="", $limit=160){
	$rm_html	= strip_tags($data);
	$tot_char		= count(str_split($rm_html));
	if($tot_char > $limit){
		$hasil		= substr($rm_html, 0, $limit);
		return $hasil;
	}else{
		return $rm_html;
	}
}

function implode_to_string($data){
	$hasil = '';
	if(!empty($data)){
		foreach($data as $val){
			$tmp[] = "'".$val."'";
		}
		$hasil = implode(",", $tmp);
	}
	return $hasil;
}

function status_jabatan($data){
	$hasil = 'PELAKSANA';
	if(!empty($data['GRP_C'])){
		$kel_jab_str = array();
		$exp = explode("#", $data['GRP_C']);
		if(!empty($exp)){
			foreach($exp as $d){
				if(!empty($d)){
					$exp2 = explode("@", $d);
					$kel_jab_str[] = $exp2[0];
				}
			}
		}
		
		if(in_array('GA000007', $kel_jab_str)){ #kelompok pejabat
			$hasil = 'PEJABAT';
		}else{
			$hasil = 'PELAKSANA';
		}
	}
	return $hasil;
}

function nama($data=''){
	$exp = explode("^", $data);
	return $exp[2];
}

function urlencodeku($data=''){
	$urlencode = urlencode($data);
	$plus			= str_replace("+", "_", $urlencode);
	return $plus;
}

function urldecodeku($data=''){
	$plus				= str_replace("_", "+", $data);
	$urldecode	= urldecode($plus);
	return $urldecode;
}

function tf_encode($kd_kelas, $enckey = ''){ $hasil = ''; #return $kd_kelas;
	if ($enckey == ''){ $str 	= 'sng3bAdac5UEmQzv2YBTH8CVh7jXpRo0etfOK4MINSlwFZ6iL9kPD1JWyuqGxr#-.:/'; } else { $str = $enckey; }
	
	$arr_e = array();  $arr_e1 = array(); $arr_r = array(); $arr_r1 = array();
	for($j = 0; $j < strlen($str); $j++){
		$j_ = $j; if ($j_ < 10) { $j_ = '0'.$j_; }
		$arr_e1[$j] = substr($str,$j,1);
		$arr_e[$j_] = substr($str,$j,1);
		$arr_r1[substr($str,$j,1)] = $j;
		$arr_r[substr($str,$j,1)] = $j_;
	}
	
	$total = 0;
	for($i = 0; $i < strlen($kd_kelas); $i++){
		$total = (int)substr($kd_kelas,$i,1) + $total; 
	} $u = fmod($total,10);
	
	$kd_enc = $arr_e1[$u];
	for($i = 0; $i < strlen($kd_kelas); $i++){
		$k = ($arr_r1[substr($kd_kelas,$i,1)]+$u); if($k < 10) { $k = '0'.$k; }
		$kd_enc .= ''.$k.rand(0,9); 
	} return $kd_enc;
}

function tf_encode2($kd_kelas, $enckey = ''){ $hasil = ''; #return $kd_kelas;
	if ($enckey == ''){ $str 	= 'XpRo0etfOK4MINSlwFZsng3bAdac5_UEmQzv2YBTH8CVh7j6iL9kPD1JWyuqGxr#-.:/ *='; } else { $str = $enckey; }
	
	$arr_e = array();  $arr_e1 = array(); $arr_r = array(); $arr_r1 = array();
	for($j = 0; $j < strlen($str); $j++){
		$j_ = $j; if ($j_ < 10) { $j_ = '0'.$j_; }
		$arr_e1[$j] = substr($str,$j,1);
		$arr_e[$j_] = substr($str,$j,1);
		$arr_r1[substr($str,$j,1)] = $j;
		$arr_r[substr($str,$j,1)] = $j_;
	}
	
	$total = 0;
	for($i = 0; $i < strlen($kd_kelas); $i++){
		$total = (int)substr($kd_kelas,$i,1) + $total; 
	} $u = fmod($total,10);
	
	$kd_enc = $arr_e1[$u];
	for($i = 0; $i < strlen($kd_kelas); $i++){
		$k = ($arr_r1[substr($kd_kelas,$i,1)]+$u); if($k < 10) { $k = '0'.$k; }
		$kd_enc .= ''.$k.rand(0,9); 
	} return $kd_enc;
}

function nip_pgw($nip=""){
	$nip = str_replace(" ","",trim($nip));
	return substr($nip,0,8)." ".substr($nip,8,6)." ".substr($nip,14,1)." ".substr($nip,15,3);
}

function general_place($data=''){
	$up 		= strtoupper($data);
	$satu 	= str_replace("LAINNYA PROP. ", "", $up);
	$dua		= str_replace("LAINNYA KAB. ", "", $satu);
	$tiga		= str_replace("KAB. LAINNYA PROP. ", "", $satu);
	return ucwords(strtolower($tiga));
}

function huruf_kapital($data=''){
	$hasil = ucwords(strtolower($data));
	return $hasil;
}

function uniqidku(){
	return uniqid()."p";
}

function loading_circle($width=20, $color=''){
	$width	= (is_numeric($width) ? $width.'px' : $width);
	$img	= ($color == 'white' ? LOADING_CIRCLE_WHITE : LOADING_CIRCLE);
	return '<img src="'.base_url($img).'" style="width:'.$width.'" />';
}

function notifications($messages='', $status, $prop=''){
	$hasil 	= '';
	$id 		= '';
	$class	= '';
	$exp	= explode(" ", $prop);
	if(!empty($exp[0])){
		foreach($exp as $val){
			$str_arr	= str_split($val);
			if(!empty($str_arr[0])){
				switch($str_arr[0]){
					case '.':
						$tmp_class[]	= str_replace(".", "", $val);
						break;
					case '#':
						$tmp_id[]			= str_replace("#", "", $val);
						break;
				}
			}
		}
		$class	= (!empty($tmp_class) ? implode(" ", $tmp_class) : "");
		$id		= (!empty($tmp_id) ? implode(" ", $tmp_id) : "");
	}
	if(!empty($messages)&&!empty($status)){
		switch($status){
			case 'info':
				$class_status	= "bs-callout bs-callout-info";
				break;
			case 'errors':
				$class_status	= "bs-callout bs-callout-error";
				break;
			case 'success':
				$class_status	= "bs-callout bs-callout-success";
				break;
			default:
				$class_status	= "bs-callout bs-callout-info";
				break;
		}
		if(is_array($messages)){
			$pesan = '<div id="'.$id.'" class="'.$class_status.' '.$class.'">';
			foreach($messages as $val){
				$pesan = $pesan.'<p>-'.$val.'</p>';
			}
			$pesan 	= $pesan.'</div>';
			$hasil 		= $pesan;
		}else{
			$hasil = '<div  id="'.$id.'"class="'.$class_status.' '.$class.'">'.$messages.'</div>';
		}
	}
	return $hasil;
}

function br2nl($data=''){
    $breaks = array("<br />","<br>","<br/>");  
    $text = str_ireplace($breaks, "\r\n", $data);  
	return $text;
}

function pre($data=''){
	echo '<pre>';
	print_r($data);
	echo '</pre>';
}

?>