<?php

	/**
	* Memuatkan source 3rd party package ke class yang butuh
	*/
	function load_package_random_compat($version='')
	{
		require_once APPPATH.'modules/common/packages/random_compat/ci-random-compat.php';
	}

	function load_package_firebase_jwt($version='')
	{
		$dir_jwt_pack = APPPATH.'modules/common/packages/firebase-php-jwt/';
		require_once $dir_jwt_pack.'BeforeValidException.php';
		require_once $dir_jwt_pack.'ExpiredException.php';
		require_once $dir_jwt_pack.'SignatureInvalidException.php';
		require_once $dir_jwt_pack.'JWT.php';		
	}