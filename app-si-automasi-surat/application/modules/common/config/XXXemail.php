<?php defined('BASEPATH') OR exit('No direct script access allowed');

// $config = array(
//     'protocol' => 'smtp', 
//     'smtp_host' => 'your_host',
//     'smtp_port' => your_port,
//     'smtp_user' => 'your_email',
//     'smtp_pass' => 'your_password',
//     'smtp_crypto' => 'security', //can be 'ssl' or 'tls' for example
//     'mailtype' => 'html', //plaintext 'text' mails or 'html'
//     'smtp_timeout' => '4', //in seconds
//     'charset' => 'iso-8859-1',
//     'wordwrap' => TRUE
// );

//SMTP & mail configuration
$config = array(
    'protocol'  => 'smtp', // 'mail', 'sendmail', or 'smtp'
    'smtp_host' => 'ssl://smtp.gmail.com',
    'smtp_port' => 465,    
    'smtp_user' => 'surat-no-replay@uin-suka.ac.id',
    'smtp_pass' => 'surat123456',
    // 'smtp_crypto' => 'security', //can be 'ssl' or 'tls' for example
    'mailtype'  => 'html',
    'smtp_timeout' => '4', //in seconds
    'charset' => 'iso-8859-1',
    'wordwrap' => TRUE
);