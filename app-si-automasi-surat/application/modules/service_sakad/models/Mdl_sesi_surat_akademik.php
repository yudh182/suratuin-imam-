<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Standar format return untuk INSERT dan UPDATE berupa array dengan key-value sebagai berikut :
* 'status'	=> TRUE / FALSE
* 'code'	=> 201 (new resurce added) / 422 (unprocessable entity) /  400 (bad syntax) / 501 (internal sever error)
* 'data'	=> sertakan kembali data yang diinput jika berhasil
* 'error_message' => tampilkan pesan error jika proses insert/update gagal
*/
class Mdl_sesi_surat_akademik extends CI_Model {

	//public $data = array();
	private $table_name = 'SESI_BUAT_SAKAD';

	public function __construct()
	{
		parent::__construct();
		$this->tnde = $this->load->database('tnde',TRUE);	
	}

	/**
	* Dapatkan data sesi sakad by id_sesi
	*/
	public function get_one_sesi_sakad($data)
	{		
		$this->tnde->select('*');
	    $this->tnde->from('SESI_BUAT_SAKAD'); 
	    $this->tnde->where('ID_SESI',$data);	         
	    $query = $this->tnde->get(); 
	    if($query->num_rows() != 0)
	    {
	        return $query->row_array();
	    }
	    else
	    {
	        return FALSE;
	    }
	}

	public function get_sesi_sakad_by_user($data)
	{
		$this->tnde->select('*');
	    $this->tnde->from('SESI_BUAT_SAKAD'); 
	    //$this->tnde->join('MD_SUB_JENIS_SURAT', 'MD_SUB_JENIS_SURAT.KD_SUB_JENIS_SURAT=SESI_BUAT_KONSEP_SURAT.KD_SUB_JENIS_SURAT', 'left');
	    //$this->tnde->join('MD_JENIS_SURAT', 'MD_JENIS_SURAT.KD_JENIS_SURAT=SESI_BUAT_KONSEP_SURAT.KD_JENIS_SURAT', 'left');
 		$this->tnde->where('SESI_BUAT_SAKAD.PEMBUAT_SURAT',$data);
	    $this->tnde->order_by('SESI_BUAT_SAKAD.WAKTU_SIMPAN_AWAL','asc');
	    $query = $this->tnde->get(); 
	    if($query->num_rows() > 0)
	    {
	        return $query->result_array();
	    }
	    else
	    {
	        return FALSE;
	    }
	}

	public function insert_entry_minimal($data)
	{					
		//$kd_status_simpan = (!empty($data['KD_STATUS_SIMPAN']) ? (int)$data['KD_STATUS_SIMPAN'] : 1);
		$id_sesi = (!empty($data['ID_SESI_TEMP']) ? $data['ID_SESI_TEMP'] : '');
		$kd_jenis_surat = (!empty($data['KD_JENIS_SURAT']) ? (int)$data['KD_JENIS_SURAT'] : '');
		$kd_sub_jenis_surat = (!empty($data['KD_SUB_JENIS_SURAT']) ? (int)$data['KD_SUB_JENIS_SURAT'] : '');
		$pembuat_surat = (!empty($data['PEMBUAT_SURAT']) ? $data['PEMBUAT_SURAT'] : '');
		$penerima_mhs = (!empty($data['D_PENERIMA_MHS']) ? $data['D_PENERIMA_MHS'] : '');
		$sql = "INSERT INTO SESI_BUAT_KONSEP_SURAT(ID_SESI_TEMP, KD_JENIS_SURAT, KD_SUB_JENIS_SURAT, PEMBUAT_SURAT, TMP_D_PENERIMA_MHS, WAKTU_SIMPAN_AWAL) VALUES ('".$id_sesi."', '".$kd_jenis_surat."','".$kd_sub_jenis_surat."', '".$pembuat_surat."', '".$penerima_mhs."', CURRENT_TIMESTAMP(6) )";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}		
	}

	public function insert_entry_lengkap($data=array())
	{
		//cek value data umum yang perlu di-casting ke integer, cek kolom yang kemungkinan bisa kosong
		$id_sesi = (!empty($data['ID_SESI_TEMP']) ? $data['ID_SESI_TEMP'] : '');
		$kd_jenis_surat = (!empty($data['KD_JENIS_SURAT']) ? (int)$data['KD_JENIS_SURAT'] : '');
		$kd_sub_jenis_surat = (!empty($data['KD_SUB_JENIS_SURAT']) ? (int)$data['KD_SUB_JENIS_SURAT'] : '');
		$pembuat_surat = (!empty($data['PEMBUAT_SURAT']) ? $data['PEMBUAT_SURAT'] : '');
		$jabatan_pembuat = (!empty($data['JABATAN_PEMBUAT']) ? $data['JABATAN_PEMBUAT'] : '');
		$kd_jenis_orang = (!empty($data['KD_JENIS_ORANG']) ? $data['KD_JENIS_ORANG'] : '');
		$unit_id = (!empty($data['UNIT_ID']) ? $data['UNIT_ID'] : '');
		$untuk_unit = (!empty($data['UNTUK_UNIT']) ? $data['UNTUK_UNIT'] : '');
		$kd_prodi = (!empty($data['KD_PRODI']) ? $data['KD_PRODI'] : '');
		$psd_id = (!empty($data['PSD_ID']) ? $data['PSD_ID'] : '');
		$psd_an = (!empty($data['PSD_AN']) ? $data['PSD_AN'] : '');
		$psd_ub = (!empty($data['PSD_UB']) ? $data['PSD_UB'] : '');
		$perihal = (!empty($data['PERIHAL']) ? $data['PERIHAL'] : '');
		$lampiran = (!empty($data['LAMPIRAN']) ? $data['LAMPIRAN'] : '');
		$klasifikasi = (!empty($data['ID_KLASIFIKASI_SURAT']) ? $data['ID_KLASIFIKASI_SURAT'] : '');
		$keamanan = (!empty($data['KD_KEAMANAN_SURAT']) ? $data['KD_KEAMANAN_SURAT'] : '');
		$sifat = (!empty($data['KD_SIFAT_SURAT']) ? $data['KD_SIFAT_SURAT'] : '');
		$tempat_dibuat = (!empty($data['TEMPAT_DIBUAT']) ? $data['TEMPAT_DIBUAT'] : '');
		$tempat_tujuan = (!empty($data['TEMPAT_TUJUAN']) ? $data['TEMPAT_TUJUAN'] : '');
		$kota_tujuan = (!empty($data['KOTA_TUJUAN']) ? $data['KOTA_TUJUAN'] : '');

		//DISTRIBUSI
		$penerima_mhs = (!empty($data['D_PENERIMA_MHS']) ? $data['D_PENERIMA_MHS'] : '');

		$sql = "INSERT INTO SESI_BUAT_KONSEP_SURAT(ID_SESI, KD_JENIS_SURAT, KD_SUB_JENIS_SURAT, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_JENIS_ORANG, UNIT_ID, UNTUK_UNIT, KD_PRODI, PSD_ID, PSD_AN, PSD_UB, PERIHAL, LAMPIRAN, ID_KLASIFIKASI_SURAT, KD_KEAMANAN_SURAT, KD_SIFAT_SURAT, TEMPAT_DIBUAT, TEMPAT_TUJUAN, KOTA_TUJUAN, TMP_D_PENERIMA_MHS, WAKTU_SIMPAN_AWAL) VALUES ('".$id_sesi."', '".$kd_jenis_surat."','".$kd_sub_jenis_surat."', '".$pembuat_surat."', '".$jabatan_pembuat."', '".$kd_jenis_orang."', '".$unit_id."','".$untuk_unit."', '".$kd_prodi."', '".$psd_id."', '".$psd_an."', '".$psd_ub."','".$perihal."', '".$lampiran."', '".$klasifikasi."', '".$keamanan."', '".$sifat."','".$tempat_dibuat."', '".$tempat_tujuan."', '".$kota_tujuan."', '".$penerima_mhs."', CURRENT_TIMESTAMP() )";

		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}		
	}

	function insert_entry_data($data){
		$this->tnde->insert('SESI_BUAT_SAKAD', $data);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	function insert_entry_data_v2($data){
		$this->tnde->insert('SESI_BUAT_SAKAD', $data);
		if($this->tnde->affected_rows() > 0){
			//return TRUE;			
			return [
				'status' => (bool)TRUE,
				'code'	=> 201,
				//'data'	=> $this->tnde->result_array()
				'success_message' => 'Berhasil menyimpan sesi baru',
				'data'	=> $data //tampilkan kembali data input sebagai bagian response
			];
		}else{
			//return FALSE;			
			return [
				'status' => (bool)FALSE,
				'code'	=> 422,
				'errror_message' => 'Gagal menambahkan entry sesi' 
			];
		}									
	}

	function update_entry_sesi_v1($id_sesi=null, $data){		

		/*$data = array(
        	'ISI_SURAT' => $data['ISI_SURAT'],
        	//'DETAIL_LAINNYA' => serialize($data['DETAIL_LAINNYA']),
        	'WAKTU_SIMPAN_AWAL' => date('Y-m-d h:i:s')
		);*/
		$data['WAKTU_SIMPAN_AWAL'] = date('Y-m-d h:i:s');
		
		//$this->tnde->set('ISI_SURAT', 'JJKSJKJSJKJKJSJSKJKJKSJJSKJSJKSJ');		
		//$this->tnde->where('ID_SESI', 'BSM5a80747301842', fsl);
		//$this->tnde->update('SESI_BUAT_SAKAD', ['ISI_SURAT'=>"BAJINGAN KOK ERROR YA.."]);
		$this->tnde->update('SESI_BUAT_SAKAD', $data, array('ID_SESI' => $id_sesi));

		//exit(print_r($this->tnde));

		//$this->tnde->insert('SESI_BUAT_SAKAD', $data);
		$this->tnde->error();
		if($this->tnde->affected_rows() > 0){
			//return TRUE;
			$data['ID_SESI'] = $id_sesi;
			return [
				'status' => (bool)TRUE,
				'code'	=> 200,
				//'data'	=> $this->tnde->result_array()
				'success_message' => 'Berhasil memperbarui data sesi',
				'data'	=> $data //tampilkan kembali data input sebagai bagian response
			];
		}else{
			//return FALSE;			
			return [
				'status' => (bool)FALSE,
				'code'	=> 422,
				'errror_message' => 'Gagal memperbarui data sesi' 
			];
		}									
	}

	public function delete_entry_sesi_sakad($data)
	{		
		$this->tnde->delete('SESI_BUAT_SAKAD', array('ID_SESI' => $data));
	    if($this->tnde->affected_rows() != 0)
	    {
	        return TRUE;
	    }
	    else
	    {
	        return FALSE;
	    }
	}



	public function update_entry_by_id($data=array()) #PEMBARUAN ISIAN FORM
	{
		$isi_surat = (!empty($data['ISI_SURAT']) ? $data['ISI_SURAT'] : '');
	}

	public function update_entry_sesi_sakad($data=array())
	{
		$harus_diupdate = array('');
		//$berangkat_lain	= (!empty($data['TEMPAT_BERANGKAT_LAIN']) ? $data['TEMPAT_BERANGKAT_LAIN'] : '');
		//$tujuan_lain 		= (!empty($data['TEMPAT_TUJUAN_LAIN']) ? $data['TEMPAT_TUJUAN_LAIN'] : '');
		//$surat_tugas 		= (!empty($data['SURAT_TUGAS']) ? $data['SURAT_TUGAS'] : '');
		$sql = "UPDATE SESI_BUAT_KONSEP_SURAT SET TRANSPORTASI='".$data['TRANSPORTASI']."', TEMPAT_BERANGKAT='".$data['TEMPAT_BERANGKAT']."', TEMPAT_TUJUAN='".$data['TEMPAT_TUJUAN']."', 
					TGL_BERANGKAT=TO_DATE('".$data['TGL_BERANGKAT']."', 'DD/MM/YYYY HH24:MI:SS'), TGL_TIBA=TO_DATE('".$data['TGL_TIBA']."', 'DD/MM/YYYY HH24:MI:SS'), 
					TGL_KEMBALI=TO_DATE('".$data['TGL_KEMBALI']."', 'DD/MM/YYYY HH24:MI:SS'), ANGGARAN_INSTANSI='".$data['ANGGARAN_INSTANSI']."', ANGGARAN_AKUN='".$data['ANGGARAN_AKUN']."',
					KETERANGAN='".$data['KETERANGAN']."', TEMPAT_BERANGKAT_LAIN='".$berangkat_lain."', TEMPAT_TUJUAN_LAIN='".$tujuan_lain."', SURAT_TUGAS='".$surat_tugas."' WHERE ID_SURAT_KELUAR='".$id_surat_keluar."' ";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return $sql;
		}else{
			return FALSE;
		}
	}	

	function insert_detail_spd($data){
		$berangkat_lain	= (!empty($data['TEMPAT_BERANGKAT_LAIN']) ? $data['TEMPAT_BERANGKAT_LAIN'] : '');
		$tujuan_lain			= (!empty($data['TEMPAT_TUJUAN_LAIN']) ? $data['TEMPAT_TUJUAN_LAIN'] : '');
		$surat_tugas		= (!empty($data['SURAT_TUGAS']) ? $data['SURAT_TUGAS'] : '');
		
		$sql = "INSERT INTO D_DETAIL_SPD(ID_SURAT_KELUAR, TRANSPORTASI, TEMPAT_BERANGKAT, TEMPAT_TUJUAN, TGL_BERANGKAT, TGL_TIBA, TGL_KEMBALI, ANGGARAN_INSTANSI, ANGGARAN_AKUN, KETERANGAN, TEMPAT_BERANGKAT_LAIN, TEMPAT_TUJUAN_LAIN, SURAT_TUGAS)
		VALUES('".$data['ID_SURAT_KELUAR']."', '".$data['TRANSPORTASI']."', '".$data['TEMPAT_BERANGKAT']."', '".$data['TEMPAT_TUJUAN']."', TO_DATE('".$data['TGL_BERANGKAT']."', 'DD/MM/YYYY HH24:MI:SS'), 
		TO_DATE('".$data['TGL_TIBA']."', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('".$data['TGL_KEMBALI']."', 'DD/MM/YYYY HH24:MI:SS'), '".$data['ANGGARAN_INSTANSI']."', '".$data['ANGGARAN_AKUN']."', '".$data['KETERANGAN']."', '".$berangkat_lain."', '".$tujuan_lain."', '".$surat_tugas."')";
		
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}

}

/* End of file  */
/* Location: ./application/models/ */