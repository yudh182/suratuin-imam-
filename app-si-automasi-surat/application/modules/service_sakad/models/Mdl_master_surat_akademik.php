<?php
if(! defined('BASEPATH'))exit('No direct script access allowed');
class Mdl_master_surat_akademik extends CI_Model{
	function __construct(){
		parent::__construct();		
		$this->tnde = $this->load->database('tnde',TRUE);
	}

	function get_all_md_jenis_sakad()
	{
		//$query = $this->tnde->get('MD_JENIS_SAKAD');
		$this->tnde->select('*');
		$this->tnde->from('MD_JENIS_SAKAD');
		$this->tnde->join('GRUP_JENIS_SAKAD', 'GRUP_JENIS_SAKAD.ID = MD_JENIS_SAKAD.GRUP_JENIS_SAKAD');
		$query = $this->tnde->get();
		if($query->num_rows() != 0) {
	    	return $query->result_array();
	    } else {
	    	return FALSE;
	    }
	}

	function get_md_jenis_sakad($is_aktif=TRUE){
		$this->tnde->select('*');
		$this->tnde->from('MD_JENIS_SAKAD');
		$this->tnde->where('IS_AKTIF', $is_aktif);
		$this->tnde->order_by('NO_URUT', 'ASC');

		$query	= $this->tnde->get();
		if($query->num_rows() != 0)
	    {
	    	return $query->result_array();
	        /*return [
	        	'meta'	=> [
	        		'DATASET' => 'Data master semua jenis surat akademik',
	        		'TOTAL_DATA' => $query->num_rows()
	        	],
	        	'data'	=> $query->result_array()
	        ];*/	        	
	    }
	    else
	    {
	        return FALSE;
	    }			
	}	

	function get_jenis_sakad_by_kd($kd_jenis_sakad=null)
	{		
		$this->tnde->select('*');
		$this->tnde->from('MD_JENIS_SAKAD');
		$this->tnde->where('KD_JENIS_SAKAD',(int)$kd_jenis_sakad);
		$query	= $this->tnde->get();
		if($query->num_rows() != 0){
	    	return $query->result_array();
	    } else {
	    	return FALSE;
	    }
	}

	function get_jenis_sakad_by_path($path='')
	{		
		$this->tnde->select('*');
		$this->tnde->from('MD_JENIS_SAKAD');
		$this->tnde->where('URLPATH',$path);
		$query	= $this->tnde->get();
		if($query->num_rows() != 0){
	    	return $query->result_array();
	    } else {
	    	return FALSE;
	    }
	}

	function get_jenis_sakad_by_kd_v2($kd_jenis_sakad=null)
	{		
		$this->tnde->select('*');
		$this->tnde->from('MD_JENIS_SAKAD');
		$this->tnde->where('KD_JENIS_SAKAD',(int)$kd_jenis_sakad);
		$query	= $this->tnde->get();
		if($query->num_rows() != 0){
	    	return $query->row_array();
	    } else {
	    	return FALSE;
	    }
	}

	function get_jenis_sakad_by_path_v2($path='')
	{		
		$this->tnde->select('*');
		$this->tnde->from('MD_JENIS_SAKAD');
		$this->tnde->where('URLPATH',$path);
		$query	= $this->tnde->get();
		if($query->num_rows() != 0){
	    	return $query->row_array();
	    } else {
	    	return FALSE;
	    }
	}
	
	function get_all_persyaratan()
	{
		$this->tnde->select('*');
		$this->tnde->from('MD_PERSYARATAN_SAKAD');
		$query = $this->tnde->get();
		if($query->num_rows() != 0)
	    {
	        return [
	        	'meta'	=> [
	        		'TOTAL_DATA' => $query->num_rows()
	        	],
	        	'data'	=> $query->result_array()
	        ];
	        	
	    }
	    else
	    {
	        return FALSE;
	    }		
	}
	function cari_persyaratan($kds=array()){
		$this->tnde->select('*');
		$this->tnde->from('MD_PERSYARATAN_SAKAD');
		$this->tnde->where_in('KD_PERSYARATAN', $kds );
		$data	= $this->tnde->get();
		return $data->result_array();		
	}

	function update_persyaratan_sub_jenis_surat($kd_sub_jenis_surat='',$data_syarat='')
	{
		/*
		$data = array(
        	'TEST_CEK_SYARAT' => $data_syarat,
        	'TERAKHIR_DIUPDATE' => '2018-01-15 17:43:22.000000'
		);
		//$this->tnde->set('TEST_CEK_SYARAT', $data_syarat);
		//$this->tnde->set('TERAKHIR_DIUPDATE', '2018-01-15 17:43:22.000000');
		$this->tnde->where('KD_SUB_JENIS_SURAT', (int)$kd_sub_jenis_surat);
		//$this->tnde->update('MD_SUB_JENIS_SURAT'); 
		$this->tnde->update('MD_SUB_JENIS_SURAT',$data); 
		*/
		#PLAIN SQL
		$kd_sub_jenis_surat = (int)$kd_sub_jenis_surat;
		$dt = new DateTime();
		$terakhir_diupdate =  $dt->format('Y-m-d H:i:s');
		
		$sql = "UPDATE MD_SUB_JENIS_SURAT
					SET  TEST_CEK_SYARAT= '{$data_syarat}', TERAKHIR_DIUPDATE=TO_DATE('{$terakhir_diupdate}','YYYY-MM-DD HH24:MI:SS')
					WHERE KD_SUB_JENIS_SURAT = {$kd_sub_jenis_surat}
				";
		$this->tnde->query($sql);

		if($this->tnde->affected_rows() > 0){
			return TRUE; //update success
		}else{
			return FALSE; //update failed
		}
	}

	public function get_all_config_automasi_sakad()
	{
		$this->tnde->select('*');
		$this->tnde->from('CONFIG_AUTOMASI_SAKAD');
		$query = $this->tnde->get();
		if($query->num_rows() != 0)
	    {
	        return [
	        	'meta'	=> [
	        		'TOTAL_DATA' => $query->num_rows()
	        	],
	        	'data'	=> $query->result_array()
	        ];
	        	
	    }
	    else
	    {
	        return FALSE;
	    }
	}

	public function get_config_automasi_sakad_by_kd_sjs($kd_sub_jenis_surat)
	{
		$this->tnde->select('*');
		$this->tnde->from('CONFIG_AUTOMASI_SAKAD');
		$this->tnde->where('KD_SUB_JENIS_SURAT', $kd_sub_jenis_surat);		
		$query = $this->tnde->get();

		if($query->num_rows() != 0)
	    {
	        return $query->result_array();	        	
	    }
	    else
	    {
	        return FALSE;
	    }
	}

	public function get_config_automasi_sakad_by_unit_id($unit_id)
	{
		$this->tnde->select('*');
		$this->tnde->from('CONFIG_AUTOMASI_SAKAD');
		$this->tnde->where('UNIT_ID', $unit_id);		
		$query = $this->tnde->get();

		if($query->num_rows() != 0)
	    {
	        return $query->result_array();	        
	    }
	    else
	    {
	        return FALSE;
	    }
	}


	public function get_config_automasi_sakad_by_par($kd_sub_jenis_surat, $unit_id)#SEHARUSNYA HANYA 1 RESULT
	{
		$this->tnde->select('*');
		$this->tnde->from('CONFIG_AUTOMASI_SAKAD');
		$this->tnde->where('KD_SUB_JENIS_SURAT', $kd_sub_jenis_surat);
		$this->tnde->where('UNIT_ID', $unit_id);
		$query = $this->tnde->get();

		if($query->num_rows() != 0)
	    {
	        return $query->row_array();	        	
	    }
	    else
	    {
	        return FALSE;
	    }
	}
}