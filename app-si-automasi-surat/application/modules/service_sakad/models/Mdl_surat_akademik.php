<?php
if(! defined('BASEPATH'))exit('No direct script access allowed');
/**
* Semua Transactions harus menggunakan instance driver PDO
* lainnya bisa pakai instance driver Postgre seperti sebelumnya
*/ 
class Mdl_surat_akademik extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->tnde = $this->load->database('tnde',TRUE);
		$this->dbpdo = $this->load->database('tnde_pdo',TRUE);

	}

	public function test_pdo_header_skr($data=null)
	{
		$query = $this->tnde_pdo->get('D_HEADER_SURAT_KELUAR');

		if($query->num_rows() != 0){
			return $query->result_array();
		} else {
			return FALSE;
		}
	}

	public function test_pdo_insert_header_skr($data=null)
	{	
		$this->dbpdo->trans_begin();
		$this->dbpdo->insert('D_HEADER_SURAT_KELUAR', $data);
		//percobaan query kedua dibikin error
		$this->dbpdo->insert('D_DETAIL_SAKAD_IJIN', 
			array(
				'ID_SURAT_KELUAR' => $data['ID_SURAT_KELUAR'],
				'KEPERLUAN' => 'TEST TRANSACTION INSERT DENGAN DRIVER PDO PGSQL'
			)
		); 
		$this->dbpdo->trans_complete(); #complete transaction

		if ($this->dbpdo->trans_status() === true){
			$this->dbpdo->trans_commit();
			return $this->dbpdo;
		} else { //bisa false, error, null
			$this->dbpdo->trans_rollback();
			return $this->dbpdo;
			
			//return [
			//	'trans_status' => $this->tnde_pdo->trans_status(),
			//	'affected_rows' => $this->tnde_pdo->affected_rows() 
			//];
		}
		
	
		/* 
		Jika Tanpa Transaksi , insert pertama sukses kedua gagal
		$this->tnde_pdo->insert('D_HEADER_SURAT_KELUAR', $data);
		$this->tnde_pdo->insert('D_DETAIL_SAKAD_IJIN', 
			array(
				'ID_SURAT_KELUAR' => $data['ID_SURAT_KELUAR'].'xxxxxxxxxxxxxxxxxxxxxx',
				'KEPERLUAN' => 'TEST TRANSACTION INSERT DENGAN DRIVER PDO PGSQL'
			)
		); */
	}	

###	versi 1 (deprecated)
	/**
	* Dapatkan surat-surat keluar yang dihasilkan melalui sistem automasi
	* dibentuk dari gabungan tabel D_HEADER_SURAT_KELUAR, D_DETAIL_SAKAD_{GRUP}, D_TIKET_ADMINISTRASI
	*/
	public function get_d_surat_keluar_automasi_v1($data)
	{		
		$this->tnde->select('*');
		$this->tnde->from('D_HEADER_SURAT_KELUAR');
		$this->tnde->join('D_TIKET_ADMINISTRASI', 'D_TIKET_ADMINISTRASI.ID_SURAT_KELUAR = D_HEADER_SURAT_KELUAR.ID_SURAT_KELUAR');
		$this->tnde->join('D_DETAIL_SAKAD_KET', 'D_DETAIL_SAKAD_KET.ID_SURAT_KELUAR = D_HEADER_SURAT_KELUAR.ID_SURAT_KELUAR');
		$this->tnde->where('D_HEADER_SURAT_KELUAR.ID_SURAT_KELUAR', $data);
		$query = $this->tnde->get();

		if($query->num_rows() != 0){
			return $query->row_array();
		} else {
			return FALSE;
		}
	}

	public function get_d_header_surat_keluar_by_id($data)
	{
		$query = $this->tnde->get_where('D_HEADER_SURAT_KELUAR', array('ID_SURAT_KELUAR' => $data));

		if($query->num_rows() != 0){
			return $query->row_array();
		} else {
			return FALSE;
		}
	}

	/**
	* Kolom-kolom : ID (SERIAL Sequence), ID_SURAT_KELUAR, TGL_SURAT, NO_SURAT, NIM, WAKTU_SIMPAN, KD_JENIS_SAKAD
	*/
	public function insert_header_surat_keluar_v1($data=null)
	{
		$this->tnde->insert('D_HEADER_SURAT_KELUAR', $data);
		if($this->tnde->affected_rows() > 0){
			//return TRUE;			
			return [
				'status' => (bool)TRUE,
				'code'	=> 201,				
				'success_message' => 'Berhasil menambahkan item header surat keluar',
				'data'	=> $data //tampilkan kembali data input sebagai bagian response
			];
		}else{
			//return FALSE;			
			return [
				'status' => (bool)FALSE,
				'code'	=> 422,
				'errror_message' => 'Gagal menambahkan item header surat keluar' 
			];
		}
		
		//antisipasi DB ERROR, misal akibat kekeliruan pada array pembangun insert	    
	}


	public function insert_detail_sakad_keterangan($data=null)
	{
		//format tanggal penyimpanan di postgresql
		//$data['MASA_BERLAKU_SAMPAI'] =  date( "Y-m-d", strtotime($data['MASA_BERLAKU_SAMPAI']));
		$data['KD_JENIS_SAKAD'] = (int)$data['KD_JENIS_SAKAD'];
	
		$insert = $this->tnde->insert('D_DETAIL_SAKAD_KET', $data);
		//$error = $this->tnde->error();
		if($this->tnde->affected_rows() > 0){
			//return TRUE;			
			return [
				'status' => (bool)TRUE,
				'code'	=> 201,				
				'success_message' => 'Berhasil menambahkan detail surat keterangan mahasiswa (automasi))',
				'data'	=> $data //tampilkan kembali data input sebagai bagian response
			];
		} else{												
			//return FALSE;			
			return [
				'status' => (bool)FALSE,
				'code'	=> 422,
				'errror_message' => 'Gagal menambahkan detail surat keterangan mahasiswa (automasi).'
			];
		}		
	}

	public function insert_detail_sakad_ijin()
	{

	}

	public function insert_detail_sakad_permohonan_lain()
	{

	}


###	versi 2 (digunakan saat ini)

	public function get_detail_surat_keluar_automasi($id_surat_keluar, $grup_detail)
	{		
		// $id_surat_keluar = $data['ID_SURAT_KELUAR'];
		// $grup_detail = $data['GRUP_JENIS_SAKAD'];
		$grup_detail = (int)$grup_detail;
		switch ($grup_detail) {
			case 1: #GRUP KETERANGAN KEMAHASISWAAN fakultas				
				$tbl_detail = 'D_DETAIL_SAKAD_KET';				
				break;
			case 2:
				$tbl_detail = 'D_DETAIL_SAKAD_IJIN';				
				break;
			default:
				exit("GRUP TIDAK DIKENALI");
				break;
		}

		$this->tnde->select('*');
		$this->tnde->from($tbl_detail);
		$this->tnde->where('ID_SURAT_KELUAR', $id_surat_keluar);
		$query = $this->tnde->get();

		if($query->num_rows() != 0){
			return $query->row_array();
		} else {
			return FALSE;
		}
	}
	/**
	* Insert data detail surat keluar ke tabel D_HEADER_SURAT_KELUAR, 
	* D_DETAIL_SAKAD (KET, IJIN, atau PERM_LAIN) dan D_TIKET_ADMINISTRASI (opsional)
	* @param (array) ID_SURAT_KELUAR, TGL_SURAT, PEMBUAT_SURAT, NIM (opsional), KD_JENIS_SAKAD, GRUP_JENIS_SAKAD, KEPERLUAN, BATAS_MASA_BERLAKU, ++kolom kolom detail lainnya
	* @return boolean
	*/
	public function trans_insert_detail_surat_keluar_automasi($data)
	{
		$grup_detail = (int)$data['GRUP_JENIS_SAKAD'];

		//exit(print_r($data));
		$kolom_header_required = ['ID_SURAT_KELUAR', 'NO_SURAT', 'TGL_SURAT','PEMBUAT_SURAT','KD_JENIS_SAKAD'];
		switch ($grup_detail) {
			case 1: #GRUP KETERANGAN KEMAHASISWAAN fakultas
				$data['DATA_TAMBAHAN'] = (!empty($data['DATA_TAMBAHAN']) ? serialize($data['DATA_TAMBAHAN']) : NULL);
				$tbl_detail = 'D_DETAIL_SAKAD_KET';
				$kolom_required = ['ID_SURAT_KELUAR','KEPERLUAN','DATA_TAMBAHAN','KOLOM_MHS','BATAS_MASA_BERLAKU'];
				break;
			case 2:
				$tbl_detail = 'D_DETAIL_SAKAD_IJIN';
				$kolom_required = ['ID_SURAT_KELUAR','KEPERLUAN','NM_KEGIATAN', 'JUDUL_DLM_KEGIATAN', 'SUBYEK_DLM_KEGIATAN','OBYEK_DLM_KEGIATAN','TEMA_DLM_KEGIATAN', 'METODE_DLM_KEGIATAN', 'TEMPAT_KEGIATAN', 'TGL_MULAI','TGL_BERAKHIR','BATAS_MASA_BERLAKU','DATA_LAINNYA','MATA_KULIAH_TERKAIT'];
				break;
			default:
				exit("GRUP TIDAK DIKENALI");
				break;
		}
		
		$selected_data_header = array_intersect_key($data, array_flip($kolom_header_required));
		$selected_data_detail = array_intersect_key($data, array_flip($kolom_required));

		//modif kolom integer , perlu dicasting
		$selected_data_header['KD_JENIS_SAKAD'] = (int)$selected_data_header['KD_JENIS_SAKAD'];		



		//modif kolom terkait waktu dan tanggalan (format dilakukan di sisi REST CLIENT  )
		/*if (!empty($selected_data_header['TGL_SURAT'])){
			$tgl = DateTime::createFromFormat('d/m/Y', $selected_data_header['TGL_SURAT']);
			$selected_data_header['TGL_SURAT'] = $tgl->format('Y-m-d');	
		}
		
		if (!empty($selected_data_detail['BATAS_MASA_BERLAKU'])){
			$tgl = DateTime::createFromFormat('d/m/Y', $selected_data_detail['BATAS_MASA_BERLAKU']);
			$selected_data_detail['BATAS_MASA_BERLAKU'] = $tgl->format('Y-m-d');	
		}*/

		///exit(print_r($selected_data_header));
		//exit(print_r($selected_data_detail));
		/****** Data Siap , Mulai transaction *********/
        $this->dbpdo->trans_begin(); #mulai transaction //$this->db->trans_start(TRUE); // Query will be rolled back        
    	$this->dbpdo->insert('D_HEADER_SURAT_KELUAR', $selected_data_header);
    	$this->dbpdo->insert($tbl_detail, $selected_data_detail); 
        $this->dbpdo->trans_complete(); #complete transaction
        
        if ($this->dbpdo->trans_status() === true) { 
            $this->dbpdo->trans_commit();
        	//return TRUE;
        	return [
        		'status' => $this->dbpdo->trans_status(),
        		'message' => 'Berhasil menambahkan data detail surat keluar automasi',
        		//'affected_rows' => $this->dbpdo->affected_rows(),        		
        		'data' => array_merge($selected_data_header, $selected_data_detail),
        		//'debug' => $this->dbpdo,
        	];
        } else {                        
        	$this->dbpdo->trans_rollback();
        	//return FALSE;        	
			return [
				'status' => $this->dbpdo->trans_status(),
				'message' => 'Gagal menambahkan data detail surat keluar automasi'
				//'affected_rows' => $this->dbpdo->affected_rows(),
				//'debug' => $this->dbpdo
			];
        }
	}

	/**
	* Delete data detail di tabel D_HEADER_SURAT_KELUAR, D_DETAIL_SAKAD, D_TIKET_ADMINISTRASI
	* @param (string) ID_SURAT_KELUAR 
	* @return boolean
	*/
	public function trans_delete_detail_surat_automasi($id_surat_keluar, $kd_grup)
	{
		#tentukan tabel sesuai grup
		switch ($kd_grup) {
			case 1:
				$tbl_detail = 'D_DETAIL_SAKAD_KET';
				break;
			case 2:
				$tbl_detail = 'D_DETAIL_SAKAD_IJIN';
				break;
			default:
				return FALSE;
				break;
		}
		
		#mulai transaction
        $this->dbpdo->trans_begin();
        
        //1. hapus record di tabel D_HEADER_SURAT_KELUAR        
        $this->dbpdo->where('ID_SURAT_KELUAR', $id_surat_keluar);
        $this->dbpdo->delete('D_HEADER_SURAT_KELUAR');

        //1. hapus record di tabel D_DETAIL_SAKAD_KET
        $this->dbpdo->where('ID_SURAT_KELUAR', $id_surat_keluar);
        $this->dbpdo->delete($tbl_detail);

        //$this->dbpdo->where('ID_SURAT_KELUAR', $id_surat_keluar);
        //$this->dbpdo->delete('D_TIKET_ADMINISTRASI');        
        $this->dbpdo->trans_complete();        

        if ($this->dbpdo->trans_status() === TRUE) {        	
            $this->dbpdo->trans_commit(); //if everything went right, delete the data from the database

            if($this->dbpdo->affected_rows() > 0){ //data-data memang terhapus
				return TRUE; //delete berhasil				
			} else{ //data-data tidak terhapus (misal karena tidak ditemukan)
				return FALSE; //delete gagal
				 $this->dbpdo->trans_rollback();				
			}            

        } else {
            //if something went wrong, rollback everything
            $this->dbpdo->trans_rollback();
        	return FALSE;
        }		
          
	}
 


}
