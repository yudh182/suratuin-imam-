<?php
if(! defined('BASEPATH'))exit('No direct script access allowed');
/**
* Semua Transactions harus menggunakan instance driver PDO
* lainnya bisa pakai instance driver Postgre seperti sebelumnya
*/ 
class Model_arsip_surat_keluar extends CI_Model{
	
	public function __construct(){
		parent::__construct();
		$this->db = $this->load->database('tnde',TRUE);
		$this->pdo = $this->load->database('tnde_pdo',TRUE);
	}

	#without [data] envelope, failed query = FALSE
	public function find_surat_keluar_by_nim($nim, $order='DESC', $limit=20, $offset=0)
	{		
		$this->db->select('*');
		$this->db->from('D_HEADER_SURAT_KELUAR');
		$this->db->join('MD_JENIS_SAKAD', 'MD_JENIS_SAKAD.KD_JENIS_SAKAD = D_HEADER_SURAT_KELUAR.KD_JENIS_SAKAD');
		// $this->db->join('D_DETAIL_SAKAD_KET', 'D_HEADER_SURAT_KELUAR.ID_SURAT_KELUAR = D_DETAIL_SAKAD_KET.ID_SURAT_KELUAR', 'left');
		// $this->db->join('D_DETAIL_SAKAD_IJIN', 'D_HEADER_SURAT_KELUAR.ID_SURAT_KELUAR = D_DETAIL_SAKAD_IJIN.ID_SURAT_KELUAR', 'left');
		$this->db->where('D_HEADER_SURAT_KELUAR.PEMBUAT_SURAT', $nim);
		$this->db->order_by('D_HEADER_SURAT_KELUAR.TGL_SURAT', $order);
		$this->db->limit($limit, $offset);
		$query = $this->db->get();

		if($query->num_rows() != 0){
			return $query->result_array();
		} else {
			return FALSE;
		}
	
	}

	public function find_detail_surat_keluar_by_nim($grup=null, $nim, $order='DESC', $limit=20, $offset=0)
	{
		$grup = (int) $grup;
		switch ($grup) {
			case 1:
				$nm_tbl = 'D_DETAIL_SAKAD_KET';
				break;
			case 2:
				$nm_tbl = 'D_DETAIL_SAKAD_IJIN';
				break;
			
			default:
				return ['error'=>'parameter tidak dikenali'];
				break;
		}
		$this->db->select('*');
		$this->db->from($nm_tbl);
		$this->db->join('D_HEADER_SURAT_KELUAR', 'D_HEADER_SURAT_KELUAR.ID_SURAT_KELUAR = '.$nm_tbl.'.ID_SURAT_KELUAR');		
		$this->db->where($nm_tbl.'.PEMBUAT_SURAT', $nim);
		$this->db->order_by('D_HEADER_SURAT_KELUAR.TGL_SURAT', $order);
		$this->db->limit($limit, $offset);
		$query = $this->db->get();

		if($query->num_rows() != 0){
			return $query->result_array();
		} else {
			return FALSE;
		}
	
	}

}