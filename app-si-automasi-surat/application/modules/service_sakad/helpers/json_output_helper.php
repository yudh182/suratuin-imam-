<?php
defined('BASEPATH') OR exit("No direct script is allowed");

	/**
	* apakah fungsi ini syntaksnya sesuai dengan CI 2.1.3 ?
	* versi CI yang dipakai Service2
	* @type void [json]
	*/
	function json_output($statusHeader=200, $response)
	{
		$ci =$ get_instance();
		$ci->output->set_content_type('application/json');
		$ci->output->set_status_header($statusHeader);
		$ci->output->set_output(json_encode($response));
	}