<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'modules/service_sakad/libraries/REST_Controller.php';

class Perizinan extends REST_Controller {

	public function __construct() {
        parent::__construct(); 

        //$this->load->model("mdl_direktori_instansi");
        $this->tnde = $this->load->database('tnde_pdo',TRUE);
        $this->load->model("mdl_direktori_instansi");
    }

    /**
    * GET instansi pemerintah all|detail|pagination|search
    */
    public function instansi_pemerintah_get($id=null, $sub_resource=null)
    {
    	// //tampilkan resource spesifik berdasar ID
    	// if (!empty($id)){
    	// 	$resource = $this->mdl_direktori_instansi->findOne($id);
    	// }

    	// //pencarian
    	// $q = $this->input->get('search');
    	// if (!empty($q)){
    	// 	$resource = $this->mdl_direktori_instansi->findManyBySearching($q);
    	// }
    }

    public function instansi_perizinan_get()
    {
        $response = [];
        $query = $this->mdl_direktori_instansi->get_instansi_perizinan();


        if ($query){
            $response['results'] = [];
            foreach ($query as $k=>$v) {
                $response['results'][$k]['id'] = $v['ID'];
                $response['results'][$k]['text'] = $v['NM_INSTANSI'];
                $response['results'][$k]['pimpinan'] = $v['SEBUTAN_PIMPINAN'];
                $response['results'][$k]['nama_instansi'] = $v['NM_INSTANSI_F'];
                $response['results'][$k]['alamat'] = $v['ALAMAT'];
                $response['results'][$k]['arah_tj'] = $v['ARAH_TJ'];

                // if ($v['KD_INSTANSI'] == 'KESBANGPOLDIY'){
                //     $response['results'][$k]['disabled'] = TRUE;
                // }
            }

            # TANPA ADAPTASI UNTUK SELECT2
            // $response['results'] = $query;
            // $response = $query;
            
        }

        $this->response($response);
    }


}