<?php if(!defined('BASEPATH'))exit('No direct script access allowed');
require APPPATH.'modules/service_sakad/libraries/REST_Controller.php';
class Arsip_surat_keluar extends REST_Controller {	
 //    $api_kode = null;
	// $api_subkode = null;
	// $api_search = [];

	function __construct() {
        parent::__construct();                              
        $this->load->model("model_arsip_surat_keluar", "mdl_arsip"); 
    }

	function index_get($format = 'json')
	{
        $this->response( 
        	array(
        		"status"=>true, 
        		"message"=>"resource eksis di ct arsip surat keluar!")
        		, 
        	200
        ); // 200 being the HTTP response code
    }

    /**
    * Koleksi resource arsip surat keluar dalam berbagai wujud
    */
    public function data_search_post($format='json')
    {
    	$kode = isset($_POST['api_kode']) ? (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']) : NULL;
		$subkode = isset($_POST['api_subkode']) ? (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']) : NULL;
		$api_search = $this->input->post('api_search');

		if ($kode === 1001){

		} elseif ($kode === 1002) {
			
		} else {
			$query = array("status"=>false, 'error'=> [
				'error_code'=>400,
				'error_message'=>'parameter api kode tidak benar'
			]);
			$status=400;
		}
		$this->response($query, $status);
    }

    public function data_search_by_get($kondisi)
    {
        $search = $this->input->get('search');

    	switch ($kondisi) {
    		case 'nim':
    			$query = $this->mdl_arsip->find_surat_keluar_by_nim($search, 'DESC');
    			break;
    		case 'unit_spesifik':
    			break;
    		case 'periode':
    			break;    		
    		
    		default:
    			# code...
    			break;
    	}
    	$this->response($query);
    }

    public function data_update_post($format='json')
    {

    }    

}