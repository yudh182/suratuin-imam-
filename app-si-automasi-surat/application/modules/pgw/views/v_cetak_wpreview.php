<?php
/*
* Section preview dan cetak surat
*/

$curr_jenis_sakad = $this->session->userdata('tmp_verified_detail_skr');
$kd_jenis_sakad = $curr_jenis_sakad['KD_JENIS_SAKAD'];

$arr_cetak_v1 = [1,2,3,4,5,6,7,8,9,10];
$arr_cetak_v2 = [11];

if (in_array($kd_jenis_sakad, $arr_cetak_v1)){
	$submit_link = base_url().'pgw/automasi_surat_mhs/aksi_simpan_cetak_surat';
} elseif (in_array($kd_jenis_sakad, $arr_cetak_v2)) {
	$submit_link = base_url().'pgw/act_surat_keluar_mhs/aksi_simpan_cetak_surat_baru';
} else {
	$submit_link = base_url().'pgw/automasi_surat_mhs/aksi_simpan_cetak_surat';
}
?>

<?php echo (isset($pesan) ? '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">x</button>'.$pesan.'</div>' : ''); ?>

<div class="container-preview-cetak">	
	
	<div class="col-md-12">
		<div class="pull-left">
			<button id="btn_hapus_sesi" class="btn btn-small btn-danger" data-id="<?= $sesi_cetak['ID_SESI']; ?>" type="button">batalkan <span class="fa fa-remove"></span></button>
		</div>
		<div class="pull-right">		
			<!-- <form method="POST" action="<?php echo base_url(); ?>pgw/act_surat_keluar_mhs/aksi_simpan_cetak_surat_baru" target="_blank" id="form_cetak_surat">			
			<form method="POST" action="<?php echo base_url(); ?>pgw/automasi_surat_mhs/aksi_simpan_cetak_surat" target="_blank" id="form_cetak_surat">	 -->
			<form method="POST" action="<?= $submit_link; ?>" target="_blank" id="form_cetak_surat">		
				<input type="hidden" name="id_sesi_buat_cetak" value="<?php echo $sesi_cetak['ID_SESI']; ?>" />
				<input type="hidden" name="save_final_cetak" value="ok" />
				<input type="submit" name="btn_preview_skr_bernomor" id="btn_preview_skr_bernomor" style="margin: 10px 0px" class="btn btn-inverse btn-small btn-uin" value="Preview Surat" />	
			</form>
		</div>

	</div>
	
	<hr>

	<h3 class="h-border">Cetak Surat</h3>

	<div class="col-md-12">	
		<ul class="nav nav-tabs">
		  <li class="active"><a data-toggle="tab" href="#skema1">Skema Cetak Bernomor Biasa</a></li>
		  <li><a data-toggle="tab" href="#skema2">Skema Cetak Bertandatangan Elektronik</a></li>		  
		</ul>

		<div class="tab-content">
		  <div id="skema1" class="tab-pane fade in active">
		    <div class="bs-callout bs-callout-warning" style="margin-bottom:5px">			
				<h5><b>Mohon diperhatikan!</b></h5>
				<ul>
					<li><p>Surat terlebih dahulu akan <b>disimpan <span class="fa fa-save"></span></b> supaya mendapatkan nomor</li>
					<li>
						<b>didistribusikan <span class="fa fa-send"></span></b> ke penerima (pengelola dan tembusan). Berikut daftar distribusi surat ini : 
						<ol>
							<li>Mahasiswa yang bersangkutan</li>
							<li>Instansi Tujuan</li>
							<li>Dekan (Tembusan sebgaia Laporan)</li>
						</ol>		
					</li>
					<li>
						<p>Kemudian, secara otomatis anda akan menerima dialog untuk <b>mengunduh file surat <span class="fa fa-print"></span></b>
					</li>
				</ul>
				
				</p>Print file surat dan segera ajukan ke TU terkait, untuk mendapatkan tandatangan dan cap pengesahan surat.</p>								
			</div>
		  </div>
		  <div id="skema2" class="tab-pane fade">
		    <div class="bs-callout bs-callout-warning">
				<h5><b>Skema Cetak Bertandatangan Elektronik</b></h5>
				<p>Inputkan alasan kenapa anda membutuhkan surat bertandatangan elektronik (<i>misal: karena akan penelitian di daerah asal, pejabat penandatangan sedang melaksanakan perjalanan dinas, dsb</i>), </p>
				<p>Sistem akan mengirimkan permohonan penandatanganan surat dinas (psd) elektronik kepada pejabat penandatangan surat ini. Apabila permohonan disetujui oleh pejabat , anda akan mendapat pemberitahuan bahwa surat sudah bisa dicetak dan langsung digunakan tanpa harus diajukan ke TU.
				<p> <i><b>Tips</b> : Pantau terus laman dashboard atau email anda untuk mengetahui perkembangan status permohonan</i></p>
			</div>	
		  </div>		  
		</div>	

		<div class="clearfix"></div>
		<div class="inline-buttons action-buttons">			
		<!-- <form method="POST" action="<?php echo base_url(); ?>pgw/automasi_surat_mhs/aksi_simpan_cetak_surat" target="_blank" id="form_cetak_surat">	 -->
		<!-- <form method="POST" action="<?php echo base_url(); ?>pgw/act_surat_keluar_mhs/aksi_simpan_cetak_surat_baru" target="_blank" id="form_cetak_surat">	 -->
		<form method="POST" action="<?= $submit_link; ?>" target="_blank" id="form_cetak_surat">
		
			<input type="hidden" name="id_sesi_buat_cetak" value="<?php echo $sesi_cetak['ID_SESI']; ?>" />
			<input type="hidden" name="save_final_cetak" value="ok" />

			<input type="submit" name="btn_save_skr_bernomor" id="btn_save_skr_bernomor" style="margin: 10px 0px" class="btn-uin btn btn-inverse btn btn-medium" value="Cetak Surat Bernomor" />

			<!--<input type="submit" name="btn_save_skr_draf" id="btn_save_skr_draf" class="btn btn-inverse btn-medium btn-uin" value="Ajukan surat sebagai draf" title="menyimpan sbg draf dan diajukan dengan menunjukkan qrcode kepada petugas TU">-->
			<!-- <input type="button" name="btn_trigger_ttd" id="btn_trigger_ttd" style="margin: 10px 0px" class="btn-uin btn btn-inverse btn btn-medium" value="Ajukan Cetak Surat dengan TTD" title="menyimpan sbg permohonan tanda tangan digital dan hanya bisa dicetak setelah disetujui pejabat penandatangan" />
			<div class="triggered-form" style="display: none;">
				

				<textarea class="form-control" placeholder="tambahkan alasan kenapa anda memerlukan tanda tangan digital"></textarea>
				<br>
				<button type="button" name="btn_save_skr_ttd" id="btn_save_skr_ttd" class="btn btn-inverse btn-uin"> Simpan dan Kirim Permohonan TTD <i class="fa fa-save"></i></button>
			</div> -->
		</form>		

		</div>		
	</div>
</div>

<script type="text/javascript">
	/*$("form" ).on( "submit", function( event ) {
		event.preventDefault();
	});*/
	$(document).ready(function() {

		$('#btn_trigger_ttd').click(function(event){
			event.preventDefault();
			$('.triggered-form').show();
		});

		$('#btn_save_skr_ttd').click(function(e){
			//e.preventDefault();
			var me = $('form#form-cetak-surat');
			
			var request = $.ajax({
				url: "<?php echo base_url('pgw/automasi_surat_mhs/aksi_simpan_cetak_surat/kirim_perm_ttd'); ?>",
				type: 'POST',
				cache: false,
				data: me.serialize(),
				dataType: 'html',
				//dataType: 'json', //format data balikan
				beforeSend: function() {
				  // $('#btn_simpan_sesi').text('proses menyimpan...');	
				   $('#loading1').show();
				   setTimeout(function () {
				        $("#loading1").hide();
				        //form.submit();
				   }, 3000); // in milliseconds
				 }
			});

			request.done(function( msg ) {				
				//$('#btn_simpan_sesi').text('perbarui');
				$('#loading1').hide();
				$('#next-content').html(
					'<iframe src="'+msg+'"></iframe>'
				);
			});

			request.fail(function( jqXHR, textStatus ) {
				console.log(jqXHR);				
				$('#loading1').hide();


				if (jqXHR.status==422) { //unprocessable
					$('#result-aksi').html(jqXHR.responseText);
					//$('#btn_simpan_sesi').text('simpan');

				} else if (jqXHR.status==501){ //Not Implemented (internal server error)
					$('#result-aksi').html(jqXHR.responseText);
				}
			});
		});

		$('#btn_hapus_sesi').click(function(e){					
				var attr_id = $(this).data("id");
				var txt = $(this).text();
				//alert(txt);
				var r = confirm("Press a button!");
				if (r == true) {
				    var request = $.ajax({
					url: "<?= site_url('pgw/automasi_surat_mhs/aksi_hapus_sesi'); ?>",
					type: 'POST',
					cache: false,
					data: { id_sesi: attr_id},
					dataType: 'html', //format data balikan
					beforeSend: function() {
					   $(e.target).text('proses menghapus...');
					   //$('#loading1').show();
					 }
				});
				request.done(function( msg ) {
					//$(e.target).remove();
					$(e.target).closest('tr').remove();

					if ($( "table.sesi-eksis" ).has( "tr" ).length){
						alert('masih ada sesi lainnya');
					} else {
						location.reload(true);
					}

					console.log(msg);		
					alert(msg);
					//$('#loading1').hide();
					//$('#btn_simpan_sesi').text('simpan atau perbarui');

				});

				request.fail(function( jqXHR, textStatus ) {
					console.log(jqXHR);				
					//$('#loading1').hide();

					if (jqXHR.status==422) { //unprocessable
						//$('#result-aksi').html(jqXHR.responseText);
						//$('#btn_simpan_sesi').text('simpan atau perbarui');
						alert(jqXHR.responseText);
					} else if (jqXHR.status==501){ //Not Implemented (internal server error)
						//$('#result-aksi').html(jqXHR.responseText);
						alert(jqXHR.responseText);
					} else {
						alert(jqXHR.responseText);
					}
					//$('#result-aksi').html(jqXHR.text);
				 	//alert( "Request failed: " + textStatus );
				});			
				} else {
				    alert("Batal menghapus!");
				}								
			});					
	});
	/*
	
	$(document).ready(function() {
		$('form#form_cetak_surat').submit(function(e){
			var me = $(this);
			//alert('Action URL : '+me.attr('action')+', DATA Serialize :'+me.serialize());
			
			var request = $.ajax({
				url: me.attr('action'), //response sukses berupa url file pdf
				type: 'POST',
				cache: false,
				data: me.serialize(),
				dataType: 'html',
				//dataType: 'json', //format data balikan
				beforeSend: function() {
				   $('#btn_simpan_sesi').text('proses menyimpan...');	
				   $('#loading1').show();
				   setTimeout(function () {
				        $("#loading1").hide();
				        //form.submit();
				   }, 3000); // in milliseconds
				 }
			});

			request.done(function( msg ) {				
				$('#btn_simpan_sesi').text('perbarui');
				$('#loading1').hide();
				$('#next-content').html(
					'<iframe src="'+msg+'"></iframe>'
				);
			});

			request.fail(function( jqXHR, textStatus ) {
				console.log(jqXHR);				
				$('#loading1').hide();


				if (jqXHR.status==422) { //unprocessable
					$('#result-aksi').html(jqXHR.responseText);
					$('#btn_simpan_sesi').text('simpan');

				} else if (jqXHR.status==501){ //Not Implemented (internal server error)
					$('#result-aksi').html(jqXHR.responseText);
				}
			});
		});
	});
	*/
</script>