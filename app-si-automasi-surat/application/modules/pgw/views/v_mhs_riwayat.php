<div class="col-med-9">
	<div class="content-space">
	
	<h2 class="h-border">Riwayat Surat Saya</h2>

	<?php if (!empty($header_surat_keluar)) : ?>
	<table class="table table-bordered table-hover">
	  <thead>
		  <tr>
			  <th width="20px"><center>No</center></th>
			  <th width="140px"><center>Nomor Surat</center></th>
			  <th width="72px"><center>Tanggal Surat</center></th>
			  <!--<th width="16%"><center>Kepada</center></th>-->
			  <th width="100px"><center>Jenis Surat</center></th>
			  <th><center>Status</center></th>
			  <th width="100px"><center>Aksi</center></th>
		  </tr>
		</thead>
		 <tbody id="content-check">
		<?php foreach ($header_surat_keluar as $key => $val) : 
		$no = $key + 1;
		?>
		<tr class="success">
			<td><?= $no; ?></td>
			<td><?= $val['NO_SURAT']; ?></td>
			<td><?= $val['TGL_SURAT']; ?></td>
			<td>SURAT KETERANGAN</td>
			<td><span class="badge badge-success">telah disahkan</span></td>
			<td>
				<a class="btn btn-default btn-small" href="<?php echo base_url('pgw/automasi_surat_mhs/riwayat/detail/');?><?php echo $val['ID_SURAT_KELUAR'];?>" title="Lihat & Edit Data" data-rel="tooltip">
					Lihat
				</a>
			</td>
		</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
	<?php endif; ?>

	</div>
</div>