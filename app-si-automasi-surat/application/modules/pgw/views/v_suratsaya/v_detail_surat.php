<script type="text/javascript">
	$(document).ready(function(){
		/*
		$("#detail-dok").hide();	
		$("#hide-detail").hide();
		
		$("#show-detail").click(function(){
			var monitoring = $("#monitoring").val();
			if(monitoring == ""){
				$.ajax({
					url: "<?php echo base_url('internal/surat_masuk/monitoring_v2');?>",
					type: "POST",
					beforeSend: function(){
						$("#monitoring-process").show();
					},
					data: {id: "<?php //echo $surat_masuk['ID_SURAT_MASUK'];?>"},
					success:function(hasil){
						$("#distribusi").html(hasil);
						$("#monitoring-process").hide();
						$("#show-detail").hide();
						$("#hide-detail").show();
						document.getElementById("monitoring").value = 1;
					}
				});
			}else{
				$("#distribusi").show("slow");
				$("#monitoring-process").hide();
				$("#show-detail").hide();
				$("#hide-detail").show();
			}
			 $("#detail-dok").show("slow");
			 $("#hide-detail").show();
			 $("#show-detail").hide();	
		});
		
		$("#hide-detail").click(function(){
			 $("#detail-dok").hide("slow");
			 $("#show-detail").show();
			 $("#hide-detail").hide();	
		});*/
	});
</script>
<div class="col-med-9">
	<div class="content-space">
		<ul id="crumbs">
			<li>
				<a title="Surat Keluar Saya" href="#">Surat Saya</a>
			</li>			
			<li>
				<a title="Detail Surat Keluar" href="#">Detail Surat</a>
			</li>
		</ul><br/>
	<div class="container-detail-surat">
		<div class="alert alert-success">
		    <p><span class="fa fa-check-circle fa-lg"></span> Surat berhasil disimpan di database. Selama surat belum disahkan anda masih bisa mencetak ulang untuk pengajuan ke tata usaha.</p>
		</div>
		<div class="well">
			<?php echo print_r($surat_keluar); ?>
		</div>

		<div class="well">
			<?php echo print_r($detail_surat_automasi); ?>
		</div>
		<fieldset class="fs-detail-surat">
			<legend>Umum</legend>
			<table class="table table-nama detail-surat">
				<tr>
					<td class="tdlabel">Unit/Bagian/Fakultas</td>
					<td>&nbsp;: <?php echo (!empty($unit[0]['UNIT_NAMA']) ? $unit[0]['UNIT_NAMA'] : '');?></td>
				</tr>
				<tr>
					<td>Nomor Surat</td><td>
					 &nbsp;: <?php echo (!empty($surat_keluar['umum']['NO_SURAT']) ? $surat_keluar['umum']['NO_SURAT'] : "");?></td>
				</tr>
				<tr>
					<td>Jenis Surat</td>
					<td> &nbsp;: <?php echo (!empty($surat_keluar['umum']['NM_JENIS_SURAT']) ?strtoupper($surat_keluar['umum']['NM_JENIS_SURAT']) : "");?>
					</td>
				</tr>
				<tr>
					<td>Klasifikasi Surat</td>
					<td> &nbsp;: <?php echo (!empty($surat_keluar['umum']['KD_KLASIFIKASI_SURAT']) ? $surat_keluar['umum']['KD_KLASIFIKASI_SURAT'].' - '.strtoupper($surat_keluar['umum']['NM_KLASIFIKASI_SURAT']) : '');?>
					</td>
				</tr>
				<tr>
					<td>Sifat Surat / Tingkat Keamanan</td>
					<td> &nbsp;: <?php 
						echo (!empty($surat_keluar['umum']['NM_SIFAT_SURAT']) ?strtoupper($surat_keluar['umum']['NM_SIFAT_SURAT']) : '');
						echo (!empty($surat_keluar['umum']['NM_KEAMANAN_SURAT']) ? ' / '.strtoupper($surat_keluar['umum']['NM_KEAMANAN_SURAT']) : '');
						?>
					</td>
				</tr>
				
			</table>
		</fieldset>
		<br>
		<fieldset class="fs-detail-surat">
			<legend>Detail Surat (Automasi)</legend>

		<?php if ($detail_surat_automasi['GRUP_JENIS_SAKAD'] == '1'): #GRUP KETERANGAN FAKULTAS ?>
			<table class="table table-nama detail-surat">
				<tr>
					<td class="tdlabel">Jenis Surat Mahasiswa</td>
					<td> :  <?php echo (!empty($detail_surat_automasi['NM_JENIS_SAKAD']) ? $detail_surat_automasi['NM_JENIS_SAKAD'] : "");?></td>
				</tr>
				<tr>
					<td>Grup Surat</td><td>
					 &nbsp;: <?php echo (!empty($detail_surat_automasi['GRUP_JENIS_SAKAD']) ? $detail_surat_automasi['GRUP_JENIS_SAKAD'] : "");?></td>
				</tr>
				<tr>
					<td>Keperluan</td>
					<td> &nbsp;: <?php echo (!empty($detail_surat_automasi['KEPERLUAN']) ? $detail_surat_automasi['KEPERLUAN'] : "");?>
					</td>
				</tr>
				<tr>
					<td>Batas Masa Berlaku</td>
					<td> &nbsp;: <?php echo (!empty($detail_surat_automasi['BATAS_MASA_BERLAKU']) ? display_tanggal_indo($detail_surat_automasi['BATAS_MASA_BERLAKU'], 'Y-m-d') : "");?>
					</td>
				</tr>				
			</table>
		<?php endif; ?>

		<?php if ($detail_surat_automasi['GRUP_JENIS_SAKAD'] == '2'): #GRUP PERMOHONAN IJIN FAKULTAS ?>
			<table class="table table-nama detail-surat">
				<tr>
					<td class="tdlabel">Jenis Surat Mahasiswa</td>
					<td> :  <?php echo (!empty($detail_surat_automasi['NM_JENIS_SAKAD']) ? $detail_surat_automasi['NM_JENIS_SAKAD'] : "");?></td>
				</tr>
				<tr>
					<td>Grup Surat</td><td>
					 &nbsp;: <?php echo (!empty($detail_surat_automasi['GRUP_JENIS_SAKAD']) ? $detail_surat_automasi['GRUP_JENIS_SAKAD'] : "");?></td>
				</tr>
				<tr>
					<td>Keperluan</td>
					<td> &nbsp;: <?php echo (!empty($detail_surat_automasi['KEPERLUAN']) ? $detail_surat_automasi['KEPERLUAN'] : "");?>
					</td>
				</tr>
				<tr>
					<td>Batas Masa Berlaku</td>
					<td> &nbsp;: <?php 					
					echo (!empty($detail_surat_automasi['BATAS_MASA_BERLAKU']) ? display_tanggal_indo(
							date_format(date_create_from_format('Y-m-d h:i:s', $detail_surat_automasi['BATAS_MASA_BERLAKU']), 'Y-m-d')) : "");?>
					</td>
				</tr>
				<tr>
					<td>Nama Kegiatan</td>
					<td> &nbsp;: <?php echo (!empty($detail_surat_automasi['NM_KEGIATAN']) ? $detail_surat_automasi['NM_KEGIATAN'] : "");?>
					</td>
				</tr>
				<tr>
					<td>Tema dalam Kegiatan</td>
					<td> &nbsp;: <?php echo (!empty($detail_surat_automasi['TEMA_DLM_KEGIATAN']) ? $detail_surat_automasi['TEMA_DLM_KEGIATAN'] : "");?>
					</td>
				</tr>
				<tr>
					<td>Tempat Kegiatan</td>
					<td> &nbsp;: <?php echo (!empty($detail_surat_automasi['TEMPAT_KEGIATAN']) ? $detail_surat_automasi['TEMPAT_KEGIATAN'] : "");?>
					</td>
				</tr>
				<tr>
					<td>Judul terkait Kegiatan</td>
					<td> &nbsp;: <?php echo (!empty($detail_surat_automasi['JUDUL_DLM_KEGIATAN']) ? $detail_surat_automasi['JUDUL_DLM_KEGIATAN'] : "");?>
					</td>
				</tr>
				<tr>
					<td>Metode dalam Kegiatan</td>
					<td> &nbsp;: <?php echo (!empty($detail_surat_automasi['METODE_DLM_KEGIATAN	']) ? $detail_surat_automasi['METODE_DLM_KEGIATAN	'] : "");?>
					</td>
				</tr>
				<tr>
					<td>Subyek dalam Kegiatan</td>
					<td> &nbsp;: <?php echo (!empty($detail_surat_automasi['SUBYEK_DLM_KEGIATAN']) ? $detail_surat_automasi['SUBYEK_DLM_KEGIATAN'] : "");?>
					</td>
				</tr>
				<tr>
					<td>Obyek dalam Kegiatan</td>
					<td> &nbsp;: <?php echo (!empty($detail_surat_automasi['OBYEK_DLM_KEGIATAN']) ? $detail_surat_automasi['OBYEK_DLM_KEGIATAN'] : "");?>
					</td>
				</tr>
				<tr>
					<td>Tanggal Mulai</td>
					<td> &nbsp;: <?php echo (!empty($detail_surat_automasi['TGL_MULAI']) ? display_tanggal_indo($detail_surat_automasi['TGL_MULAI'], 'Y-m-d') : "")?>
					</td>
				</tr>
				<tr>
					<td>Tanggal Berakhir</td>
					<td> &nbsp;: <?php echo (!empty($detail_surat_automasi['TGL_BERAKHIR']) ? display_tanggal_indo($detail_surat_automasi['TGL_BERAKHIR'], 'Y-m-d') : "")?>
					</td>
				</tr>
			</table>
		<?php endif; ?>
		</fieldset>

		<h4 class="h-border">Distribusi Surat</h4>
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th width="4%">
						<center>No</center>
					</th>
					<th width="15%">
						<center>Waktu Proses</center>
					</th>
					<th width="18%">
						<center>Pemroses</center>
					</th>
					<th>
						<center>Grup Distribusi</center>
					</th>
					<th width="20%">
						<center>Keterangan</center>
					</th>
				</tr>
			</thead>
			<tbody id="content-check">
				<?php 
				$no = 1;
				foreach ($surat_keluar['penerima']['PS'] as $k=>$v) : ?>
				<tr>
					<td><center><?= $no++; ?></center></td>
					<td><center><?= $v['WAKTU_AKSES']; ?></center></td>
					<td>
						<center>
							<?= $v['PENERIMA_SURAT']; ?>
							<!-- <br>(MOH.TAUFIK) -->
						</center>
					</td>
					<td>
						<?= $v['NM_GRUP_TUJUAN']; ?> | <?= 'PENGELOLA SURAT (PS)'; ?></td>
					<td>
						<center>							
							<span class="label label-default"><?= $v['NM_STATUS_SM']; ?></span>
						</center>
					</td>
				</tr>
			<?php endforeach; ?>

			<?php 
				$no = 1;
				foreach ($surat_keluar['penerima']['TS'] as $k=>$v) : ?>
				<tr>
					<td><center><?= $no++; ?></center></td>
					<td><center><?= $v['WAKTU_AKSES']; ?></center></td>
					<td>
						<center>
							<?= $v['PENERIMA_SURAT']; ?>
							<!-- <br>(MOH.TAUFIK) -->
						</center>
					</td>
					<td>
						<?= $v['NM_GRUP_TUJUAN']; ?> | <?= 'TEMBUSAN SURAT (TS)'; ?></td>
					<td>
						<center>							
							<span class="label label-default"><?= $v['NM_STATUS_SM']; ?></span>
						</center>
					</td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
		<hr>		

	</div>
	</div>
</div>