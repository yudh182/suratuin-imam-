<div class="col-med-9">
<div class="content-space">
	<ul id="crumbs">
		<li>
			<a title="Verificator Surat" href="#">Penandatanganan Digital</a>
		</li>
		<li>
			<a title="Arsip Surat Masuk" href="#">Inbox</a>
		</li>
	</ul><br/>
	<div class="cari">
		<div class="right">
			<form action="<?php echo base_url();?>pegawai/pengarah_sm/cari" method="POST">
				<input type="text" name="perihal" placeholder="Cari Surat Masuk" style="width:160px" />
				<input type="hidden" name="q" value="true" />
				<button type="submit" class="btn btn-default btn-small" style="border-radius:0;padding:5px;">
					Cari
				</button>
			</form>
		</div>
	</div>
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th width="4%"><center>No</center></th>
				<th width="30%"><center>Nomor Surat / Perihal</center></th>
				<th width="14%"><center>Jenis Surat</center></th>
				<th width="8%"><center>Tanggal Masuk</center></th>
				<th width="24%"><center>Asal Surat</center></th>
				<th width="10%"><center>Status</center></th>
				<th width="10%"><center>Aksi</center></th>
			</tr>			
		</thead>
		<tbody id="content-check">

			<tr>
				<td class="center">1</td>
				<td class="center">
					B- 1295/Un.02/DU/KP.07.6/06/2017
					<hr/>
					Permohonan Ijin Penelitian a.n. Johan Pangabean
				</td>
				<td class="center">Permohonan surat ijin penelitian</td>
				<td class="center">05/06/2018</td>
				<td class="center">14650021</td>
				<td><center><span class="label label-warning" >menunggu aksi</span></center></td>
				<td>
					<center>
						<a class="btn btn-default btn-small btn-aksi-lihat" href="" title="Lihat & Edit Data" data-rel="tooltip">
							<span class="fa fa-envelope"></span> Lihat
						</a>
					</center>
				</td>
			</tr>
		</tbody>
	</table>

</div>
</div>