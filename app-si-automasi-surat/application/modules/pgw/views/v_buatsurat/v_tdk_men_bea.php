<script type="text/javascript">
	var bpd = 1; //index beasiswa yg pernah diikuti
	$(function(){
		$("#addBPD").click(function(){
			row = '<tr>'+
			'<td><textarea style="width: 100%;" rows="3" name="nm_beasiswa_pd['+bpd+']" class="inp-bpd"></textarea></td>'+
			'<td><textarea style="width: 100%;" rows="3" name="ket_beasiswa_pd['+bpd+']" class="inp-bpd"></textarea></td>'+
			'<td><a class="btn btn-small btn-danger btn-act remove-btn_sp" style="" title="Hapus"> x </a></td>'+
			'</tr>';			
			$(row).insertBefore("#last_bpd");
			bpd++;
		});
	});
	$(".remove-btn_sp").on('click', function(){
		$(this).parent().parent().remove();
	});
</script>

<div class="container-isi-form col-md-12">

	<form id="form_isi_surat" method="POST" action="<?php echo base_url(); ?>pgw/automasi_surat_mhs/aksi_simpan_perbarui_sesi">
		<table>
			<tr>
				<td class="tdlabel">Keperluan</td>			
				<td class="tdinput">
					<input type="text" name="keperluan" id="keperluan" value="" class="form-control" />					
				</td>
			</tr>
			<tr class="underline"><td></td><td><span><i>bukan termasuk output surat namun wajib diisi untuk kepentingan administrasi</i></span></td></tr>
			<tr class="underline">
				<td class="tdlabel">Beasiswa yang pernah diikuti (Opsional)</td>			
				<td class="tdinput">
					<!--<input type="text" name="bea" id="bea" value="" class=" form-control" />-->

					<table class="table table-bordered" width="99%">
						<tr>
							<td width="250px">Nama beasiswa</td><td width="280px">Keterangan</td><td width="30px">Aksi</td>
						</tr>
						<tr>
							<td><textarea style="width: 100%;" rows="3" name="nm_beasiswa_pd[0]" class="inp-bpd"></textarea>
							<td><textarea style="width: 100%;" rows="3" name="ket_beasiswa_pd[0]" placeholder="ex: periode beasiswa, ket. khusus, dsb" class="inp-bpd"></textarea></td>
							<td></td>
						</tr>
						<tr id="last_bpd"></tr>
						<tr><td colspan="4"><a class="btn btn-default btn-small" id="addBPD" style="margin:0 0 5px 0">Tambah</a></td></tr>
					</table>

				</td>
			</tr>

			<tr>
				<td>
					<input type="hidden" name="id_sesi_buat" value="new" />					
					<input type="hidden" name="save_sesi" value="ok" />
					<input type="hidden" name="phpsess" value="<?php echo session_id(); ?>">
				</td>
				<td>
					<button type="submit" id="btn_simpan_sesi" class="btn btn-inverse btn-small btn-uin" style="margin: 10px 0px">Simpan Sesi</button>
					<button type="button" id="btn_cek_passing_data" class="btn btn-default" style="margin: 10px 0px">Cek Formdata</button>
				</td>
			</tr>
		</table>
	</form>		
</div> <!-- div for content -->
<div class="clearfix"></div>