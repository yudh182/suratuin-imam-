<?php
/*
* Section Isi Form Surat Keterangan Lulus
*/
?>
<div class="container-isi-form col-md-12">
<form id="form_isi_surat" method="POST" action="<?php echo base_url(); ?>pgw/automasi_surat_mhs/aksi_simpan_perbarui_sesi">
	<table>
		<tr>
			<td class="tdlabel">Keperluan</td>			
			<td class="tdinput">
				<input type="text" name="keperluan" id="keperluan" value="<?= (isset($sesi['KEPERLUAN']) ? $sesi['KEPERLUAN']: ''); ?>" class=" form-control" />				
			</td>
		</tr>
		<tr><td></td><td><i>area ini tidak diinputkan ke surat, namun wajib diisi untuk kepentingan administrasi</i></td></tr>
		<tr>
			<td>
				<input type="hidden" name="id_sesi_buat" value="new" />				
				<input type="hidden" name="save_sesi" value="ok" />
				<input type="hidden" name="phpsess" value="<?php echo session_id(); ?>">
			</td>
			<td><button type="submit" id="btn_simpan_sesi" class="btn btn-inverse btn-small btn-uin" style="margin: 10px 0px">Simpan Sesi</button></td>
		</tr>		
	</table>
</form>
</div>
<div class="clearfix"></div>