<?php
/*
* Section Isi Form Surat Ijin Penelitian
*/
?>
<script type="text/javascript" src="<?php echo base_url();?>assets/jsplugin/jquery-tokeninput/jquery.tokeninput_editbaru.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>assets/jsplugin/jquery-tokeninput/token-input.css" type="text/css" />
<div class="container-isi-form col-md-12">	
<form id="form_isi_surat" method="POST" action="<?php echo base_url(); ?>pgw/automasi_surat_mhs/aksi_simpan_perbarui_sesi">	
	<table>				
		<tr class="underline">
			<td class="tdlabel">Kepada Yth.</td>
			<td class="tdinput"><input type="text" name="kepada_penerima_eks" id="kepada_penerima_eks" placeholder="Masukkan Penerima di Instansi Tujuan Surat" value="" class="form-control populated" required/></td>
		</tr>
		
		<tr class="underline">
			<td class="tdlabel">Alamat Tujuan</td>
			<td class="tdinput"><input type="text" name="tempat_tujuan" id="tempat_tujuan" placeholder="Masukkan alamat (nama jalan,daerah, kodepos)" value="" class=" form-control populated" required/></td>
		</tr>
		<!-- <tr class="underline">
			<td class="tdlabel">Kota/Kab Tempat Tujuan</td>
			<td class="tdinput">
				<input type="text" name="tempat_tujuan" id="tempat_tujuan"/></td>
		</tr> -->
		<tr class="underline">
			<td class="tdlabel">Judul Penelitian</td>
			<td class="tdinput">
				<!--<input type="text" name="judul_penelitian" id="judul_penelitian" placeholder="Masukkan Judul Penelitian Anda" value="" class=" form-control" />-->
				<textarea name="judul_penelitian" id="judul_penelitian" class="form-control"></textarea>
			</td>
		</tr>
		<tr class="underline">
			<td class="tdlabel">Metode Penelitian</td>
			<td class="tdinput"><input type="text" name="mtd_penelitian" id="mtd_penelitian" placeholder="Masukkan metode terkait Penelitian anda " value="" class=" form-control" /></td>
		</tr>
		<!--<tr class="underline">
			<td class="tdlabel">Nama dan Alamat Instansi</td>
			<td class="tdinput"><input type="text" name="instansi_tujuan" id="instansi_tujuan" placeholder="Masukkan Nama Instansi Tujuan Surat" value="" class=" form-control" required /></td>
		</tr>-->

		<tr class="underline">
			<td class="tdlabel">Instansi Tempat Penelitian</td>
			<td class="tdinput"><input type="text" name="tmpt_penelitian" id="tmpt_penelitian" placeholder="" value="" class=" form-control" required /></td>
		</tr>
		<tr>
			<td class="tdlabel">Tanggal Mulai</td>
			<td class="tdinput"><input type="date" name="tgl_mulai" id="tgl_mulai" value="" class="form-control" required /></td>
		</tr>
		<tr>
			<td class="tdlabel">Tanggal Berakhir</td>
			<td class="tdinput"><input type="date" name="tgl_berakhir" id="tgl_berakhir" value="" class="form-control" required /></td>
		</tr>
		<tr>
			<td></td>
			<td>
				<input type="hidden" name="id_sesi_buat" value="new" />
				<!--<input type="hidden" name="inp_kd_jenis_sakad" value="<?php //$jenis_sakad['KD_JENIS_SAKAD']; ?>" />
				<input type="hidden" name="inp_grup_jenis_sakad" value="<?php //$jenis_sakad['GRUP_JENIS_SAKAD']; ?>" />
				<input type="hidden" name="inp_kd_jenis_surat_induk" value="<?php //$jenis_sakad['KD_JENIS_SURAT_INDUK']; ?>" />-->
				<input type="hidden" name="save_sesi" value="ok" />
				<input type="submit" name="btn_submit_sesi" style="margin: 10px 0px" class="btn btn-inverse btn-medium btn-uin" id="btn_submit_sesi" value="Simpan sesi" />
				<!--<input type="submit" name="save_sesi2" value="Simpan sesi lokal">-->
			</td>
		</tr>
	</table>
</form>	
</div>
<div class="clearfix"></div>
<!--<div id="result-aksi"></div>
<div id="next-content"></div>
<div id="loading1" style="display: none;">
	<center><img src="http://surat.uin-suka.ac.id/asset/img/ajax-loading.gif"><br />Harap menunggu<br>Sedang menyimpan sesi surat</center>
</div>-->

<script type="text/javascript">
	$("form" ).on( "submit", function( event ) {
		event.preventDefault();
	});	
</script>	
			