<?php
/*
* Section Isi Form Surat Keterangan Pindah Studi
*/
?>
<div class="container-isi-form col-md-12">
	<div class="alert alert-warning">
		<span class="fa fa-info"></span> Mohon persiapkan syarat-syarat penting terkait administrasi pindah studi
	</div>
<form id="form_isi_surat" method="POST" action="<?php echo base_url(); ?>pgw/automasi_surat_mhs/aksi_simpan_perbarui_sesi" role="form" class="form-horizontal">
	
		<div class="form-group">
			<label for="pilih_makul" class="col-md-3 col-sm-4 control-label">Opsi Pindah Studi</label>
			<div class="col-md-9 col-sm-8">
				<select name="opsi_pindah" id="opsi_pindah" class="form-control">
					<option value="">--pilih opsi pindah studi (sesuai keperluan)--</option>
					<option value="1">Pindah ke program studi lain (di UIN Sunan Kalijaga)</option>
					<option value="2">Meneruskan studi di Perguruan Tinggi lain (yang setara)</option>
				</select>
			</div>
		</div>

		<div class="form-group">
			<label for="catatan" class="col-md-3 col-sm-4 control-label">Catatan</label>
			<div class="col-md-9 col-sm-8">
				<input type="text" name="catatan" id="keperluan" placeholder="" value="" class=" form-control" />
				<p>inputkan prodi (jika antar prodi) atau PT yang rencana akan dituju (PT lain)</p>
			</div>
		</div>	

	<div class="form-group">
		<input type="hidden" name="id_sesi_buat" value="new" />
		<input type="hidden" name="save_sesi" value="ok" />	
		<button type="submit" style="margin: 10px 0px" class="btn btn-inverse btn-medium btn-uin" name="btn-sess" id="btn_simpan_sesi">Simpan sesi</button>
	</div>
		
</form>
</div>
<div class="clearfix"></div>