<div class="col-med-9">
	<div class="content-space">
		<ul id="crumbs">
			<li>
				<a title="Automasi Surat Mahasiswa" href="">Automasi Persuratan</a>
			</li>			
		</ul><br/>

		<div class="home-container" style="margin: 10px 0 150px 0;">
			<h2 class="h-border">Dashboard Persuratan Mahasiswa</h2>

			<div class="row">
				<?php 
				$cek_sesi = $this->session->userdata('sesi_sakad');
				if (!empty($cek_sesi)) : ?>
				<div class="col-md-12">
					<div class="box box-warning">
						<div class="box-header">
							<h3 class="box-title">Sesi Tersimpan <i class="fa fa-clipboard-list"></i></h3>
						</div>
						<div class="box-body">
							<table class="table no-margin">
								<?php 
								$no = 1;
								foreach ($cek_sesi as $key => $val) : ?>
								<tr>
									<td><?= $no++ ?></td>
									<td><?= $val['PERIHAL']; ?></td>
									<td><?= $val['WAKTU_SIMPAN_AWAL']; ?></td>
									<td>
										<button class="btn btn-inverse btn-small btn-uin" data-id="<?= $val['ID_SESI'] ?>">lanjutkan</button>
										<button class="btn btn-small btn-danger btn_hapus_sesi" data-id="<?= $val['ID_SESI']; ?>" type="button">hapus <span class="fa fa-remove"></span></button>
									</td>											
								</tr>
								<?php endforeach;?>
							</table>

						</div>
				</div>
			</div>
			<?php endif; ?>
				<!-- <div class="col-md-7">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Pembuatan Surat Bertandatangan Digital <i class="fa fa-qrcode"></i></h3>
							<div class="box-tools pull-right">
								<i class="fa fa-bell"></i>
								 <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
							</div>
						</div>
						<div class="box-body" style="overflow: hidden; display: block;">
					    <div id="alur-cetak-surat">	
							<ol id="tracker-srtakd" class="progtrckr" data-progtrckr-steps="4">
								<li class="progtrckr-done number1">Simpan Sesi Form</li><li class="progtrckr-done number2">Permohonan PSD Digital</li><li class="progtrckr-onprogress number3">Menunggu Persetujuan</li><li class="progtrckr-todo number4">Cetak</li>
							</ol>
						</div>
							<table>
								<tbody><tr>
									<td>Surat Keterangan Habis Teori</td>
								</tr>
								<tr>
									<td>
										<time>Dicetak pada dd/mm/YYYY h:i:s</time>			
									</td>
								</tr>
							</tbody></table>
							
							

						</div>
					</div>
				</div> -->

				
			</div>

			<!-- <hr>
			<div class="row">
				<div class="col-md-4">
					<div class="small-box bg-emas">
			            <div class="inner">
			            	<h3>0</h3>
			            	<h4>Riwayat Cetak Surat Bernomor</h4>
			              	<p>Surat Keterangan Fakultas</p>
			            </div>			            
			            <a href="<?= site_url('modul_surat_akademik/admin_master_surat_akademik/sub_jenis_surat'); ?>" class="small-box-footer">
			              lihat <i class="fa fa-archive"></i>
			            </a>
			        </div>
				</div>
				<div class="col-md-4">
					<div class="small-box bg-emas">
			            <div class="inner">
			            	<h3>0</h3>
			            	<h4>Riwayat Cetak Surat Bertandatangan Digital</h4>			              
			            </div>
			            <div class="icon">
			              <i class="ion ion-pie-graph"></i>
			            </div>
			            <a href="#" class="small-box-footer">
			              lihat <i class="fa fa-archive"></i>
			            </a>
			        </div>
				</div>
				<div class="col-md-4">
					<div class="small-box bg-emas">
			            <div class="inner">
			            	<h3>0</h3>
			             	<h4>Surat Dibatalkan</h4>              
			            </div>
			            <div class="icon">
			              <i class="ion ion-pie-graph"></i>
			            </div>
			            <a href="#" class="small-box-footer">
			              buka laman <i class="fa fa-archive"></i>
			            </a>
			        </div>
				</div>							
			</div> -->
		</div>

	</div>
</div>	