<div class="col-med-9">
	<div class="content-space">
		<ul id="crumbs">
			<li>
				<a title="Surat Keluar" href="#">Surat Keluar</a>
			</li>
			<li>
				<a title="Arsip Surat Keluar" href="#">Arsip Surat Keluar</a>
			</li>
		</ul>

		<table class="table table-bordered table-hover">
		 	<thead>
				<tr>
					<th width="20px"><center>No</center></th>
					<th width="140px"><center>Nomor Surat</center></th>
					<th width="72px"><center>Tanggal Surat</center></th>
					<!--<th width="16%"><center>Kepada</center></th>-->
				 	<th width="100px"><center>Jenis Surat</center></th>
					<th><center>Perihal</center></th>
					<th width="100px"><center>Aksi</center></th>
				</tr>
		  </thead>   

		  <tbody id="content-check">
				<?php
				$url = $this->uri->segment(4);
				if(ISSET($url)){
					$i = $url;
				}else{
					$i = 0;
				}
				if(!empty($surat_keluar)){
					foreach($surat_keluar as $key => $data){
						$i = $i + 1; 
						switch($data['KD_STATUS_SIMPAN']){
							case 0:
								$status = 'danger';
								break;
							case 3:
								$status = 'success';
								break;
							default:
								$status = '';
								break;
						} ?>
						<tr class="<?php echo $status;?>">
							<td class="center"><?php echo $i;?></td>
							<td class="center"><?php echo $data['NO_SURAT'];?></td>
							<td class="center">
								<?php echo $data['TGL_SURAT'];?>
								<hr/>
								<?php echo (!empty($untuk_unit[$key]['UNIT_NAMA']) ? $untuk_unit[$key]['UNIT_NAMA'] : ''); ?>
							</td>
							<td class="center"><?php echo $data['NM_JENIS_SURAT'];?></td>
							<td class="center">
								<?php echo $data['PERIHAL'];?>
							</td>
							<td>
								<?php
								$keamanan = array('R', 'SR');
								if(in_array('SPR', $modules)){
									$aksi = TRUE;
								}else{
									if(in_array($data['KD_KEAMANAN_SURAT'], $keamanan)){
										if($data['PEMBUAT_SURAT'] == $this->session->userdata('kd_pgw')){
											$aksi = TRUE;
										}else{
											$aksi = FALSE;
										}
									}else{
											$aksi = TRUE;
									}
								}
								
								if($aksi == TRUE){ ?>
									<a class="btn btn-default btn-small" href="<?php echo base_url();?>pegawai/surat_keluar/detail/<?php echo $data['ID_SURAT_KELUAR'];?>" title="Lihat & Edit Data" data-rel="tooltip">
										Lihat
									</a>
									<?php
									switch($data['KD_STATUS_SIMPAN']){
										case 0: ?>
											<form action="<?php echo base_url();?>pegawai/surat_keluar" method="POST">
												<input type="hidden" name="id_surat_keluar" value="<?php echo $data['ID_SURAT_KELUAR'];?>" />
												<input type="hidden" name="suitchi" value="1" />
												<button class="btn btn-default btn-small" title="Aktifkan"><i class="icon-star"></i> Aktifkan</button>
											</form><?php
											break;
										case 1: 
											if($data['KD_KAT_PENOMORAN'] == 0){ #SURAT TANPA NOMOR ?>
												<a class="btn btn-default btn-small" title="Final" data-rel="tooltip" data-id_surat_keluar="<?php echo $data['ID_SURAT_KELUAR'];?>" onclick="kesshou($(this));">
													Final
												</a><?php
											}else{ ?>
												<a class="btn btn-default btn-small" title="Beri Nomor" data-rel="tooltip" data-id_surat_keluar="<?php echo (string)$data['ID_SURAT_KELUAR'];?>" onClick="berinomor($(this));">
													Beri Nomor
												</a><?php
											}
											?>
											<a class="btn btn-default btn-small" title="Hapus Data" data-rel="tooltip" data-id_surat_keluar="<?php echo $data['ID_SURAT_KELUAR'];?>" onClick="daleteSuratKeluar($(this));">
												Hapus
											</a><?php
											break;
										case 2: ?>
											<a class="btn btn-default btn-small" title="Final" data-rel="tooltip" data-id_surat_keluar="<?php echo $data['ID_SURAT_KELUAR'];?>" onclick="kesshou($(this));">
												Final
											</a>
											<a class="btn btn-default btn-small" title="Hapus Data" data-rel="tooltip" data-id_surat_keluar="<?php echo $data['ID_SURAT_KELUAR'];?>" onclick="kyanseru($(this));">
												Batalkan
											</a><?php
											break;
									}
								}
								?>
							</td>
						</tr><?php
					}
				}else{ ?>
					<tr class="error centered"><td colspan="6">Belum ada surat keluar yang disimpan</td></tr><?php
				}
				?>			
		  </tbody>
	 </table>
	 <br/>
	<?php
	if(isset($links)){
		echo $links;
	}
	?>

	</div>
</div>