<div class="col-med-9">
	<div class="content-space">
		<ul id="crumbs">
			<li>
				<a title="Automasi Surat Mahasiswa" href="">Penandatanganan Digital</a>
			</li>			
		</ul><br>

		<div class="home-container" style="margin: 10px 0 150px 0;">
			<h2 class="h-border">Penandatanganan Digital</h2>
			<div class="row">
				
				<div class="col-md-3">
					<div class="small-box bg-emas">
			            <div class="inner">
			            	<h3>7</h3>
			            	<h4>Permohonan TTD Terbaru</h4>			              
			            </div>
			            <div class="icon">
			              <i class="ion ion-pie-graph"></i>
			            </div>
			            <a href="#" class="small-box-footer">Beri Tindakan <i class="fa fa-certificate"></i>
			            </a>
			        </div>
				</div>
				<div class="col-md-3">
					<div class="small-box bg-emas">
			            <div class="inner">
			            	<h3>45</h3>
			             	<h4>Permohonan Disetujui</h4>              
			            </div>
			            <div class="icon">
			              <i class="ion ion-pie-graph"></i>
			            </div>
			            <a href="#" class="small-box-footer">
			              buka laman <i class="fa fa-certificate"></i>
			            </a>
			        </div>
				</div>
				<div class="col-md-3">
					<div class="small-box bg-emas">
			            <div class="inner">
			            	<h3>2</h3>
			            	<h4>Permohonan Ditolak</h4>	
			              
			            </div>
			            <div class="icon">
			              <i class="ion ion-pie-graph"></i>
			            </div>
			            <a href="#" class="small-box-footer">
			              buka laman <i class="fa fa-certificate"></i>
			            </a>
			        </div>
				</div>				
			</div>
		</div>

	</div>
</div>