<div class="col-med-3">
	<div id="sidebar-sia">
		<?php
		$modul					= $this->uri->segment(2);
		$auto_surat_mhs   = ($modul == 'automasi_surat_mhs' ? TRUE : FALSE);
		$psd_digital   = ($modul == 'psd_digital' ? TRUE : FALSE);
		$master_auto_surat_mhs   = ($modul == 'master_auto_surat_mhs' ? TRUE : FALSE);						
		?>
		<nav class="accordion">
			<ol>
				<li>
					<div class="sia-profile" style="margin-bottom:10px;width:223px">						
						<img class="sia-profile-image" alt="" src="<?= $this->api_foto->foto_mhs($this->session->userdata('username')); ?>">
						<h2><?php  echo $this->session->userdata('nama'); ?></h2>		
						<p style="text-align:center; font-weight:bold;"><?php echo $this->session->userdata('username'); ?></p>
						<hr>
						<?php if ($this->session->userdata('grup') == 'mhs') : ?> 
						<table>
							<tbody>
								<tr>
									<td style="width:100px;height:25px;"><p class="fakultas-profil"><b>Fakultas</b> </p></td><td> <?= $this->session->userdata('nm_fakultas'); ?><p></p></td>
								</tr>
								<tr>
									<td style="width:100px;height:25px;"><p class="prodi-profil"><b>Program Studi</b> </p></td><td> <?= $this->session->userdata('nm_prodi'); ?><p></p></td>
								</tr>
							</tbody>
						</table>
						<?php endif; ?>

						<b><?php echo (!empty($this->session->userdata('str_nama')) ? $this->session->userdata('str_nama') : ''); ?></b>

					</div>
					<div id="separate"></div>
				</li>
					

				<li>
					<a class="item full" href="<?php echo base_url('pgw/automasi_surat_mhs'); ?>">
						<span>
							Automasi Surat Mahasiswa
							<span>
								<div class="underline-menu"></div>
							</span>
						</span>
					</a>

					<?php if ($auto_surat_mhs == TRUE): ?>
					<ol class="" style="display: block;">
						<li class="submenu">
							<a href="<?php echo site_url('pgw/automasi_surat_mhs/buat'); ?>" class="submenu-title">
								<b>Buat</b>
							</a>
							<?php if (isset($list_jenis_sakad)): ?>
							<ul class="container-childmenu">
								<?php foreach ($list_jenis_sakad as $key => $val) : ?>
									<li class="submenu">
										<a href="<?php echo base_url('pgw/automasi_surat_mhs/buat/'); echo $val['URLPATH']; ?>">
											<?= $val['NM_JENIS_SAKAD']; ?>
										</a>
									</li>
								<?php endforeach; ?>
							</ul>
						<?php  endif; ?>
						</li>				


						<li class="submenu">
							<hr>
						</li>
						<li class="submenu">
							<a href="<?php echo base_url('pgw/automasi_surat_mhs/riwayat'); ?>">
								Riwayat
							</a>
						</li>


						<li class="submenu">
							<hr>
						</li>	
						<li class="submenu">
							<b>Administrasi</b>							
						</li>

						<li class="submenu">
							<a href="<?php site_url('pgw/automasi_surat_mhs/laman_pengurusan'); ?>">
								Laman Pengurusan
							</a>
						</li>
						<li class="submenu">
							<a href="<?php site_url('pgw/automasi_surat_mhs/arsip'); ?>">
								Arsip
						</li>
						
						<li class="submenu">
							<hr>
						</li>
					</ol>
					<?php endif; ?>				
				</li>

				<li>
					<a class="item full" href="<?php echo base_url('pgw/psd_digital'); ?>">
						<span>
							Penandatanganan Digital
							<span>
								<div class="underline-menu"></div>
							</span>
						</span>
					</a>

					<?php if ($psd_digital == TRUE): ?>
					<ol>
						<li class="submenu">
							<a href="<?= site_url('pgw/psd_digital/inbox'); ?>">
								Inbox
							</a>
						</li>
						<li class="submenu">
							<a href="<?= site_url('pgw/psd_digital/arsip/1'); ?>">
								Arsip Ditandatangani
							</a>
						</li>
						<li class="submenu">
							<a href="<?= site_url('pgw/psd_digital/arsip/0'); ?>">
								Arsip Ditolak
							</a>
						</li>
						<li class="submenu">
							<a href="<?= site_url('pgw/psd_digital/pengaturan'); ?>">
								Pengaturan
							</a>
						</li>
						<li class="submenu">
						<hr>
						</li>																		
					</ol>
					<?php endif; ?>
				</li>
				<li>
					<a class="item full" href="<?php echo base_url('pgw/master_auto_surat_mhs/home'); ?>">
						<span>
							Master Data Automasi Surat Mahasiswa
							<span>
								<div class="underline-menu"></div>
							</span>
						</span>
					</a>

					<?php if ($master_auto_surat_mhs == TRUE): ?>
					<ol class="" style="display: block;">
						<li class="submenu">
							<a href="<?= site_url('pgw/master_auto_surat_mhs/jenis'); ?>">
								Jenis Surat Mahasiswa
							</a>
						</li>
						<li class="submenu">
							<a href="<?= site_url('pgw/master_auto_surat_mhs/repository_persyaratan'); ?>">
								Master Persyaratan Verifikasi
							</a>
						</li>
						<li class="submenu">
							<a href="<?= site_url('pgw/master_auto_surat_mhs/config_automasi'); ?>">
								Pengaturan Automasi Data
							</a>
						</li>
						<li class="submenu">
							<hr>
						</li>																		
					</ol>										
					<?php endif; ?>	
				</li>
				<li>
					<a class="item full" href="<?php echo base_url(); ?>pgw/login/logout">
						<span>Logout</span>
						<div class="underline-menu"></div>
					</a>
				</li>
			</ol>
		</nav>
	</div>
</div>	