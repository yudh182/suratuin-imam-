<div class="alert alert-success">
	<p>sistem akan mengirimkan permohonan penandatanganan surat dinas (psd) digital kepada <?php echo $nama_jabatan.' ['.$nama_pgw.' - '.$nip.']'; ?>. Apabila permohonan disetujui oleh pejabat , anda akan mendapat pemberitahuan bahwa surat sudah tersedia untuk dicetak dan dapat langsung digunakan tanpa harus diajukan ke TU.</p>
	<p> <i><b>Tips</b> : Pantau terus laman dashboard untuk mengetahui perkembangan status permohonan anda</i></p>
</div>