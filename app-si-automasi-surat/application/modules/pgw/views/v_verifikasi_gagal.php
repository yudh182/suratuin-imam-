<div class="bs-callout bs-callout-error">
	<p>
		<b> Permohonan gagal</b>. Mohon Maaf, Anda tidak dapat melanjutkan pembuatan surat ini karena ada <?= $response_verifikasi['num_errors']; ?> syarat yang belum terpenuhi:
	</p>
</div>

<?php generate_tabel_verifikasi_v2($response_verifikasi['output']); ?>
<div class="actions-wrapper clearfix"><div class="pull-right"><a id="btn_reset" class="btn btn-default" href="<?= site_url('pgw/surat_akademik/buat'); ?>">Kembali</a></div></div>