<?php
/**
* Laman Buat Surat meliputi : 1. pilih surat 2. verifikasi 3. isi form 4. cetak
*/
?>
<div class="col-md-9">
	<h2 class="h-border">Buat <?= $jenis_sakad['NM_JENIS_SAKAD']; ?></h2>

	<?php if (!empty($cek_sesi)) : ?>
	<div class="bs-callout bs-callout-on-progress">		
		<p>Anda masih memiliki sesi pembuatan surat untuk diselesaikan, yaitu:</p>			
		<table>
			<?php 
			$no = 1;
			foreach ($cek_sesi as $key => $val) : ?>
				<tr>
					<td><?= $no++ ?></td>
					<td><?= $val['PERIHAL'].' - '.$val['WAKTU_SIMPAN_AWAL']; ?></td>
					<td>
						<button class="btn btn-inverse btn-small btn-uin" data-id="<?= $val['ID_SESI'] ?>">lanjutkan</button><br>
						<button class="btn_hapus_sesi" data-id="<?= $val['ID_SESI']; ?>" type="button">hapus</td>
				</tr>
			<?php endforeach;?>
		</table>				
		<p>Oleh sebab itu, agar dapat membuat surat baru selesaikan sesi surat yang ada.</p>
	</div>
	<?php endif; ?>

	<?php if (empty($cek_sesi)) : ?>
	<div class="box">
		<div class="box-header">
			<h3 class="box-title">Verifikasi</h3>
			<div class="box-tools pull-right">
				<span class="label label-default">Belum Terverifikasi</span>
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
		</div>

		<div class="box-body">
			<div class="bs-callout bs-callout-warning">
				<p>
					<?= $resp_persyaratan['meta']['dataset_title']; ?> :
				</p>
			</div>

			<div class='bs-callout bs-callout-error'>
				Mohon maaf <b>Sdr. <?php echo $this->session->userdata('nama'); ?></b>, 
				Anda belum dapat melakukan print Surat ini karena ada persyaratan belum terpenuhi berikut ini  :
			</div>

			<?php 
			generate_tabel_syarat($resp_persyaratan['data']);
			generate_keterangan();
			?>	
		</div>
	</div>
	
	<div class="box">
		<div class="box-header">
			<h3>Isi Form dan Cetak</h3>

			<div class="box-tools pull-right">
				<span class="label label-warning" id="label-state-isi-cetak" style="display: none;">sedang mengisi form</span>
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
		</div>
		<div class="box-body">
			<?php if (isset($_view_form)) : ?>
				<?php echo $_view_form; ?>
				<div id="result-aksi"></div>
				<div id="next-content"></div>
				<div id="loading1" style="display: none;">
					<center><img src="http://surat.uin-suka.ac.id/asset/img/ajax-loading.gif"><br />Harap menunggu<br>Sedang menyimpan sesi surat</center>
				</div>			
			<?php endif; ?>			
		</div>
		<!--<div class="box-footer">
			<span class="pull-left"></span>
			<span class="pull-right">
				<button class="btn btn-inverse btn-small btn-uin">Simpan dan Lanjutkan</button>
			</span>
		</div>-->
		<div class="overlay" style="display: none;">
		  <i class="fa fa-refresh fa-spin"></i>
		</div>
	</div>  	
	<?php endif; ?>
</div>

<script type="text/javascript">
	$("form" ).on( "submit", function( event ) {
		event.preventDefault();
	});

	$("form" ).on( "focusin", function( e ) {
		$("#label-state-isi-cetak").show();
		$("#label-state-isi-cetak").text('pengisian form..');
	});


	$(document).ready(function() {
		$('form#form_isi_surat').submit(function(e){
			var me = $(this);
			//alert('Action URL : '+me.attr('action')+', DATA Serialize :'+me.serialize());
			
			var request = $.ajax({
				url: me.attr('action'),
				type: 'POST',
				cache: false,
				data: me.serialize(),
				dataType: 'html',
				//dataType: 'json', //format data balikan
				beforeSend: function() {
				   $('#btn_simpan_sesi').text('proses menyimpan...');	
				   $('#loading1').show();
				   setTimeout(function () {
				        $("#loading1").hide();
				        //form.submit();
				   }, 3000); // in milliseconds
				 }
			});

			request.done(function( msg, textStatus, jqXHR) {				
				$('[name="id_sesi_buat"]').val(jqXHR.getResponseHeader('X-ID-SESI'));
				$('#next-content').html(msg);
				$('#btn_simpan_sesi').text('simpan atau perbarui');
				$("#label-state-isi-cetak").text('sesi form tersimpan');
				$('#loading1').hide();
			});

			request.fail(function( jqXHR, textStatus ) {
				console.log(jqXHR);				
				$('#loading1').hide();


				if (jqXHR.status==422) { //unprocessable
					$('#result-aksi').html(jqXHR.responseText);
					$('#btn_simpan_sesi').text('simpan');

				} else if (jqXHR.status==501){ //Not Implemented (internal server error)
					$('#result-aksi').html(jqXHR.responseText);
				}
			});
		});
	});
</script>

<script type="text/javascript">
		$("form" ).on( "submit", function( event ) {
			event.preventDefault();
		});
		$(document).ready(function() {
			$('.btn_hapus_sesi').click(function(e){					
				var attr_id = $(this).data("id");
				var txt = $(this).text();
				//alert(txt);
				var r = confirm("Press a button!");
				if (r == true) {
				    var request = $.ajax({
					url: "<?= site_url('pgw/automasi_surat_mhs/aksi_hapus_sesi'); ?>",
					type: 'POST',
					cache: false,
					data: { id_sesi: attr_id},
					dataType: 'html', //format data balikan
					beforeSend: function() {
					   $(e.target).text('proses menghapus...');
					   //$('#loading1').show();
					 }
				});
				request.done(function( msg ) {
					//$(e.target).remove();
					$(e.target).closest('tr').remove();
					console.log(msg);		
					alert(msg);
					//$('#loading1').hide();
					//$('#btn_simpan_sesi').text('simpan atau perbarui');

				});

				request.fail(function( jqXHR, textStatus ) {
					console.log(jqXHR);				
					//$('#loading1').hide();

					if (jqXHR.status==422) { //unprocessable
						//$('#result-aksi').html(jqXHR.responseText);
						//$('#btn_simpan_sesi').text('simpan atau perbarui');
						alert(jqXHR.responseText);
					} else if (jqXHR.status==501){ //Not Implemented (internal server error)
						//$('#result-aksi').html(jqXHR.responseText);
						alert(jqXHR.responseText);
					} else {
						alert(jqXHR.responseText);
					}
					//$('#result-aksi').html(jqXHR.text);
				 	//alert( "Request failed: " + textStatus );
				});			
				} else {
				    alert("Batal menghapus!");
				}
				
				
			});			
		});
</script>	