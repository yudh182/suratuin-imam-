<div class="col-med-9">	
	<?php //echo create_breadcrumb(site_url('modul_surat_akademik')); ?>
	<h2 class="h-border">Buat Surat Akademik</h2>

	<div id="alur-cetak-surat">	
		<ol id="tracker-srtakd" class="progtrckr" data-progtrckr-steps="4">
			<li class="progtrckr-onprogress number1">Pilih Surat</li><li class="progtrckr-todo number2">Verifikasi Permohonan</li><li class="progtrckr-todo number3">Isi Konten Surat</li><li class="progtrckr-todo number4">Cetak</li>
		</ol>
	</div>

	<div class="main-prog-wrapper overviewhead">
		<h3 class="h-border">1. Pilih Surat dan Isi NIM</h3>			
		<?php echo form_open('#', array('id' => 'form_step_1', 'role' => 'form')); ?>
			<div id="the-message-box">
				<div id="pesan-aksi">
					<?php
					$sess_errors = $this->session->flashdata('errors');
					if((!empty($errors))||(!empty($sess_errors))){ ?>
						<div class="bs-callout bs-callout-error" style="margin-bottom:5px">
							<?php
							if(!empty($errors)){
								if(is_array($errors)){
									foreach($errors as $value){
										echo "- ".$value."<br/>";
									}
								}else{
									echo $errors;
								}
							}
							if(!empty($sess_errors)){
								if(is_array($sess_errors)){
									foreach($sess_errors as $value){
										echo "- ".$value."<br/>";
									}
								}else{
									echo $sess_errors;
								}
							}
							?>
						</div><?php
					}
					
					$sess_success = $this->session->flashdata('success');
					if((!empty($success))||(!empty($sess_success))){  ?>
						<div class="bs-callout bs-callout-success" style="margin-bottom:5px">
							<?php
							if(!empty($success)){
								if(is_array($success)){
									foreach($success as $value){
										echo "- ".$value."<br/>";
									}
								}else{
									echo $success;
								}
							}
							if(!empty($sess_success)){
								if(is_array($sess_success)){
									foreach($this->session->flashdata('success') as $value){
										echo "- ".$value."<br/>";
									}
								}else{
									echo $sess_success;
								}
							}
							?>
						</div><?php
					}
					?>
				</div>															
			</div>
			<div class="form-group"><!-- PILIH SUB JENIS SURAT -->			
				<label class="label-input">Pilih Jenis Surat</label>
				<select name="inp_kd_srtakd" id="inp_kd_srtakd" class="wajib padding5" autofocus>
					<option value="" selected="">--pilih jenis surat akademik--</option>
	                <?php

	                if(isset($list_jenis_sakad)){
	                    $n = 1;
	                    foreach($list_jenis_sakad as $data){ ?>
	                    <option value="<?php echo $data['KD_JENIS_SAKAD'];?>" data-induk="<?php echo $data['KD_JENIS_SURAT_INDUK'];?>">
	                        <?php echo $n.". ".$data['NM_JENIS_SAKAD'];?>
	                    </option><?php
	                    $n++;
	                    }
	                }

	                ?>
				</select>
			</div>

			<div class="form-group"><!-- INPUTKAN NIM -->
				<label class="label-input">Masukkan NIM</label>
				<input type="number" name="inp_nim" id="inp_nim" placeholder="Masukkan NIM"/>
			</div>

			<div class="actions-wrapper clearfix"><!-- SUBMIT FORM, LAKUKAN VERIFIKASI -->
		        <div class="pull-left">	           	            
		            <!--<button id="btn_submit_cek_syarat" class="btn" title="Cek Syarat Surat" type="button"><i class="glyphicon glyphicon-list"></i> Tes Cek Syarat</button>-->
		            <!--<button type="submit">tes data submit!</button>-->
		            <button id="btn_test_jumptoform" class="btn btn-inverse btn btn-small btn-uin " title="Test lompat ke laman form" type="button">Lompat ke form</button>
		        </div>
				<div class="pull-right">				
					<button id="btn_test_jumptoverifikasi" class="btn btn-inverse btn-small btn-uin" title="Test Lompat ke laman hasil verifikasi">Lompat ke verifikasi</button>
				    <!--<button id="btn_submit_step1" class="btn btn-inverse btn btn-small btn-uin " title="Lanjut ke verifikasi dengan sistem" type="button">Simpan dan Lanjutkan</button>-->
		        </div>
			</div>
		<?php echo form_close(); ?>				
	</div>

	<div class="more-content">
		<div id="box-step2" style="display: none;">
			<h2 class="h-border">2. Verifikasi Permohonan</h2>
			<div class="content"></div>
		</div>
		<div id="kontendinamis"></div>
	</div>

	<div id="loading1" style="display: none;"><br /><center><img src="http://akademik.uin-suka.ac.id/asset/img/loading.gif"><br />Harap menunggu</center><br /></div>
</div>
<script type="text/javascript">	
	$("form" ).on( "submit", function( event ) {
		event.preventDefault();
	});
	$(document).ready(function(){
		$(window).keydown(function(event){
		    if(event.keyCode == 13) {
		      event.preventDefault();  //return false;		    

		      if ($('#inp_nim').val() > 0){
		      	document.getElementById('btn_submit_step1').click();
		      } else {
		      	return false;
		      }
		      
		    }
	  	});
	  
		$('#btn_submit_step1').click(function(){
			// ------ mulai ajax ------- 
			prosesPermintaan("<?php echo site_url('modul_surat_akademik/surat_akademik_pegawai/action_verifikasi_permohonan'); ?>");			
			//$('#box-step2').show();$('#tracker-srtakd .number2').addClass('progtrckr-onprogress').removeClass('progtrckr-todo');
		});		

		$('form#form_step_1').submit(function(e){			
			var me = $(this);
			alert('Action URL : '+me.attr('action')+', DATA Serialize :'+me.serialize());
			/*
			$.ajax({
				url: me.attr('action'),
				type: 'post',
				data: me.serialize(),
				dataType: 'json',
				success: function(response){				
				}			
			}) */

		});

		$('#kontendinamis').on('click', 'button', function(){
			alert('elemen anak baru di klik');
		});
					

		$('#btn_submit_cek_syarat').click(function(){
			sendCekSyarat();
		});

		$('#btn_test_jumptoform').click(function(){
			muatIsi();
		});		
		$('#btn_test_jumptoverifikasi').click(function(){
			muatVerifikasi();
		});

	});

	function prosesPermintaan(url_service){ //ajax
		var selected_jenis_surat = $('#inp_kd_srtakd').val();
		var selected_induk_jenis = $('#inp_kd_srtakd').find(':selected').data('induk');
        var inputted_nim = $('#inp_nim').val();

        //alert('jenis='+selected_jenis_surat+',induk = '+selected_induk_jenis+' , nim = '+inputted_nim);

		var request = $.ajax({
		  url: url_service,	  
		  type: "POST",
		  cache: false, 
		  data: {
              kd_jns_srt : selected_jenis_surat,
              kd_jns_srt_induk : selected_induk_jenis,
              nim :inputted_nim
          },          
		  //dataType: "",
		  beforeSend: function() {
		    $('#tracker-srtakd .number1').addClass('progtrckr-done').removeClass('progtrckr-onprogress');
		    $('#loading1').show();
		    //$('#box-step2 .content').html('<br /><center><img src="http://surat.uin-suka.ac.id/asset/img/ajax-loading.gif"><br />Harap menunggu<br>Sedang memverifikasi permohonan surat</center></center>');

		  }
		});
		 
		request.done(function( msg ) {					  
		 	$('#box-step2').show();
			$('#box-step2 .content').html( msg );
		  	$('#tracker-srtakd .number2').addClass('progtrckr-onprogress').removeClass('progtrckr-todo'); //update tracker
            $('#btn_submit_step1').remove();
            $('#loading1').hide();
		});
		 
		request.fail(function( jqXHR, textStatus ) {
			$('#box-step2').show();
			$('#box-step2 .content').html( '<p> Request gagal, dapat disebabkan karena koneksi anda sedang bermasalah atau sistem sedang maintenance </p>' );
			$('#loading1').hide();
		 	alert( "Request failed: " + textStatus );
		});

	}	
		

	function sendCekSyarat(){ //ajax
		var request = $.ajax({
		  url: "<?= site_url('modul_surat_akademik/act_surat_akademik_pegawai/tampilkan_daftar_persyaratan'); ?>/"+$('#inp_kd_srtakd').val()+"",	  
		  type: "GET",
		  cache: false, 
		  dataType: "html",
		  beforeSend: function() {
		    $('#loading1').show();
		  }
		});
		 
		request.done(function( msg ) {
		  //$( "#log" ).html( msg );
		 	$('#box-step2').show();
			$('#box-step2 .content').html( msg );
			$('#loading1').hide();
		});
		 
		request.fail(function( jqXHR, textStatus ) {
			$('#box-step2').show();
			$('#box-step2 .content').html( '<p> Ada kesalahan teknis pada sistem atau koneksi anda </p>' );
		 	alert( "Request failed: " + textStatus );
		 	$('#loading1').hide();
		});
	}

	function muatIsi(){ //ajax
		var request = $.ajax({
		  url: "<?= base_url('pgw/surat_akademik/load_partial_isi'); ?>",	  
		  type: "GET",
		  cache: false, 
		  dataType: "html",
		  beforeSend: function() {
		    $('#loading1').show();
		  }
		});
		 
		request.done(function( msg ) {		  		 	
			$('#kontendinamis').html( msg );
			/*alert( "panggilan ajax berhasil"); */
			$('#loading1').hide();
		});
		 
		request.fail(function( jqXHR, textStatus ) {
			$('#kontendinamis').html( '<p>Maaf, halaman yang anda minta tidak berhasil dimuat</p>' );
		 	alert( "Request failed: " + textStatus );
		 	$('#loading1').hide();
		});
	}

	function muatVerifikasi(){ //ajax
		var request = $.ajax({
		  url: "<?= base_url('pgw/surat_akademik/load_partial_verifikasi'); ?>",	  
		  type: "GET",
		  cache: false, 
		  dataType: "html",
		  beforeSend: function() {
		    $('#loading1').show();
		  }
		});
		 
		request.done(function( msg ) {		  		 	
			$('#kontendinamis').html( msg );			
			$('#loading1').hide();
		});
		 
		request.fail(function( jqXHR, textStatus ) {
			$('#kontendinamis').html( '<p>Maaf, halaman yang anda minta tidak berhasil dimuat</p>' );
		 	alert( "Request failed: " + textStatus );
		 	$('#loading1').hide();
		});
	}

	function batalkan(el){
		var id_surat_masuk = el.data('id_surat_masuk');
		document.getElementById('id_surat_masuk').value = id_surat_masuk;
		$('#modalBatalkan').modal('show');
	}

	function showBKSModal(el){
		$('#modalInfoBKS').modal('show');
	}

	function updateUI(){
		alert("DOM telah diupdate");
	}
</script>	