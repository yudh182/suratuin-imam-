<?php
/*
* Implementasi Select2 dengan remote data dan AXIOS untuk ajax
*/
?>
 <link href="<?php echo base_url('assets/jsplugin/select2/css/select2.min.css'); ?>"" rel="stylesheet" />
  <script src="<?php echo base_url('assets/jsplugin/select2/js/select2.min.js'); ?>"></script>

<div class="col-md-9">
	<form id="form_isi_surat" method="POST" action="<?php echo base_url(); ?>pgw/test_baru/simpan_ajax">	
		<table>		
			<tr class="underline">
				<td class="tdlabel">Pilih Kebutuhan Surat</td>
				<td class="tdinput">
					<select class=" form-control" id="lingkup_penelitian" />
						<option value="" data-yth="" data-almt="">--tentukan kebutuhan surat--</option>
						<option value="4" data-yth="" data-almt="">Pengantar ke instansi obyek penelitian</option>
						<option value="1" data-yth="Gubernur Daerah Istimewa Yogyakarta\r\n c.q Kepala BAKESBANGLINMAS DIY" data-almt="Jl. Jenderal Sudirman No. 5 Yogyakarta, 55231">Pengantar Rekomendasi Penelitian untuk Instansi di Luar DIY</option>
						<option value="2" data-yth="Gubernur Daerah Istimewa Yogyakarta\r\n c.q Kepala Biro Administrasi Pembangunan" data-almt="Komplek Kepatihan, Danurejan, Yogyakarta - 55213">Pengantar Rekomendasi Penelitian untuk Instansi di dalam DIY</option>
						<option value="3" data-yth="Kepala Biro AAKK\r\n UIN Sunan Kalijaga" data-almt="Jln. Marsa Adisucipto Yogyakarta 55281">Pengantar untuk penelitian di internal universitas</option>
					</select>
				</td>
			</tr>		
			</tr>
			<tr class="underline">
				<td class="tdlabel">Kepada Yth.</td>
				<td class="tdinput"><input type="text" name="kepada_penerima_eks" id="kepada_penerima_eks" placeholder="Masukkan Penerima di Instansi Tujuan Surat" value="" class="form-control populated" required/></td>
			</tr>
			
			<tr class="underline">
				<td class="tdlabel">Alamat Tujuan</td>
				<td class="tdinput"><input type="text" name="alamat_tujuan" id="alamat_tujuan" placeholder="Masukkan alamat (nama jalan,daerah, kodepos)" value="" class=" form-control populated" required/></td>
			</tr>
			<tr class="underline">
				<td class="tdlabel">Kota/Kab Tempat Tujuan</td>
				<td class="tdinput">
					<select type="text" name="tempat_tujuan" id="tempat_tujuan" class="select2-input"></select>
				</td>
			</tr>
			<tr class="underline">
				<td class="tdlabel">Judul Penelitian</td>
				<td class="tdinput">
					<!--<input type="text" name="judul_penelitian" id="judul_penelitian" placeholder="Masukkan Judul Penelitian Anda" value="" class=" form-control" />-->
					<textarea name="judul_penelitian" id="judul_penelitian" class="form-control"></textarea>
				</td>
			</tr>
			<tr class="underline">
				<td class="tdlabel">Metode Penelitian</td>
				<td class="tdinput"><input type="text" name="mtd_penelitian" id="mtd_penelitian" placeholder="Masukkan metode terkait Penelitian anda " value="" class=" form-control" /></td>
			</tr>
			<tr class="underline">
				<td class="tdlabel">Daftar Mahasiswa</td>
				<td class="tdinput">
					<h2>Ajax Tagging with Jquery Select2.js</h2>
	  				<select name="dftr-mhs[]" class="form-control itemName" multiple>   				
	    			</select>			
				</td>
			</tr>

			<tr class="underline">
				<td class="tdlabel">Instansi Tempat Penelitian</td>
				<td class="tdinput"><input type="text" name="tmpt_penelitian" id="tmpt_penelitian" placeholder="" value="" class=" form-control" required /></td>
			</tr>
			<tr>
				<td class="tdlabel">Tanggal Mulai</td>
				<td class="tdinput"><input type="date" name="tgl_mulai" id="tgl_mulai" value="" class="form-control" required /></td>
			</tr>
			<tr>
				<td class="tdlabel">Tanggal Berakhir</td>
				<td class="tdinput"><input type="date" name="tgl_berakhir" id="tgl_berakhir" value="" class="form-control" required /></td>
			</tr>
			<tr>
				<td></td>
				<td>
					<input type="hidden" name="id_sesi_buat" value="new" />
					<!--<input type="hidden" name="inp_kd_jenis_sakad" value="<?php //$jenis_sakad['KD_JENIS_SAKAD']; ?>" />
					<input type="hidden" name="inp_grup_jenis_sakad" value="<?php //$jenis_sakad['GRUP_JENIS_SAKAD']; ?>" />
					<input type="hidden" name="inp_kd_jenis_surat_induk" value="<?php //$jenis_sakad['KD_JENIS_SURAT_INDUK']; ?>" />-->
					<input type="hidden" name="save_sesi" value="ok" />
					<input type="submit" name="btn_submit_sesi" style="margin: 10px 0px" class="btn btn-inverse btn-medium btn-uin" id="btn_submit_sesi" value="Simpan sesi" />
					<button type="button" id="btn_cek_passing_data" class="btn btn-default" style="margin: 10px 0px">Cek Formdata</button>
					<!--<input type="submit" name="save_sesi2" value="Simpan sesi lokal">-->
				</td>
			</tr>
		</table>
	</form>	
	<span class="clearfix"></span>
	<div id="next-content"></div>
	<div id="loading1" style="display: none;">
		<center><img src="http://surat.uin-suka.ac.id/asset/img/ajax-loading.gif"><br />Harap menunggu<br>Sedang menyimpan sesi surat</center>
	</div>	
</div>

<script type="text/javascript">
	$('#tempat_tujuan').select2({
		minimumInputLength: 3,
		placeholder: 'pilih kota/kabupaten tujuan',
        ajax: {
          url: "<?php echo base_url('pgw/external/akademik/auto_kabupaten2'); ?>",
          dataType: 'json',
          type: 'POST',
          delay: 250,
          data: function (params) {
                return {q: params.term}
          },
          processResults: function (data) {
            return {
              results: data
            };
          },
          cache: true
        }
	});	

	$('.itemName').select2({
		// enable tagging
		tags: true,
        tokenSeparators: ['<$>',' '],

		minimumInputLength: 3,
        allowClear: true,
        placeholder: 'Select an item',
        ajax: {
          url: "<?php echo base_url('pgw/external/akademik/auto_mahasiswa2'); ?>",
          dataType: 'json',
          delay: 250,
          data: function (params) {
                return {q: params.term}
          },
          processResults: function (data) {
            return {
              results: data
            };
          },
          cache: true
        }
    });	

    $('.itemName').on('select2:open', function (e) {
	    var y = $(window).scrollTop();
	    $(window).scrollTop(y + 1);
	});




    $(document).ready(function(){
    	$('#btn_cek_passing_data').click(function(e){
			var me = $('form#form_isi_surat');
			//alert('Action URL : '+me.attr('action')+', DATA Serialize :'+me.serialize());
			
			var request = $.ajax({
				url: "<?php echo base_url('pgw/test_automasi/cek_passing_data'); ?>",
				type: 'POST',
				cache: false,
				data: me.serialize(),
				dataType: 'html',
				//dataType: 'json', //format data balikan
				beforeSend: function() {				   
				   $('#loading1').show();
				   setTimeout(function () {
				        $("#loading1").hide();
				        //form.submit();
				   }, 3000); // in milliseconds
				 }
			});

			request.done(function( msg, textStatus, jqXHR) {				
				$('#next-content').html(msg);				
				$('#loading1').hide();
			});

			request.fail(function( jqXHR, textStatus ) {
				console.log(jqXHR);				
				$('#loading1').hide();


				if (jqXHR.status==422) { //unprocessable
					$('#next-content').html(jqXHR.responseText);

				} else if (jqXHR.status==501){ //Not Implemented (internal server error)
					$('#result-aksi').html(jqXHR.responseText);
				}
			});
		});
    });
</script>