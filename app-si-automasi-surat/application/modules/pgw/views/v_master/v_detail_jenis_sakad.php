<script type="text/javascript" src="<?php echo base_url();?>assets/jsplugin/jquery-tokeninput/jquery.tokeninput_edit.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>assets/jsplugin/jquery-tokeninput/token-input.css" type="text/css" />
<script type="text/javascript">
	$(function(){
		$(".multiselect").multiselect(); //inisiasi DOM multiselect
	});

	$(document).ready(function(){
		var arr_tokenpjb = [];
		$("#container-kelola-persyaratan").hide();
		$("#show_edit_syarat").click(function(){
			$("#container-info-persyaratan").hide();
			$("#container-kelola-persyaratan").show('slow');
		});
		$("#cancel_edit_syarat").click(function(){
			$("#container-kelola-persyaratan").hide();
			$("#container-info-persyaratan").show('slow');
		});

		$("#pejabat_psd").tokenInput("<?php echo base_url();?>common/ajax/auto_jabatan", {			
			tokenLimit:1,
			onAdd: function (item) {
                alert("Added " + item.id);
                //arr_tokenpjb.push(item);
            },
		});


		//var arr_tokenpjb = $("#pejabat_psd").tokenInput("get");


		$(".id_klasifikasi_surat").tokenInput("<?php echo base_url();?>common/ajax/auto_klasifikasi_surat", {
			tokenLimit:1
		});
	});
</script>
<div class="col-med-9">
	<div class="content-space">

	<ul id="crumbs">
		<li>
			<a title="Master Data" href="#">Master Data Surat Akademik</a>
		</li>			
		<li>
			<a title="Master Data Jenis Surat Akademik" href="#">Jenis Surat (administrasi akademik)</a>
		</li>
	</ul><br/>
	<div id="result">
		<?php
		$sess_errors = $this->session->flashdata('errors');
		if((!empty($errors))||(!empty($sess_errors))){ ?>
			<div class="bs-callout bs-callout-error" style="margin-bottom:5px">
				<?php
				if(!empty($errors)){
					if(is_array($errors)){
						foreach($errors as $value){
							echo "- ".$value."<br/>";
						}
					}else{
						echo $errors;
					}
				}
				if(!empty($sess_errors)){
					if(is_array($sess_errors)){
						foreach($sess_errors as $value){
							echo "- ".$value."<br/>";
						}
					}else{
						echo $sess_errors;
					}
				}
				?>
			</div><?php
		}
		
		$sess_success = $this->session->flashdata('success');
		if((!empty($success))||(!empty($sess_success))){  ?>
			<div class="bs-callout bs-callout-success" style="margin-bottom:5px">
				<?php
				if(!empty($success)){
					if(is_array($success)){
						foreach($success as $value){
							echo "- ".$value."<br/>";
						}
					}else{
						echo $success;
					}
				}
				if(!empty($sess_success)){
					if(is_array($sess_success)){
						foreach($this->session->flashdata('success') as $value){
							echo "- ".$value."<br/>";
						}
					}else{
						echo $sess_success;
					}
				}
				?>
			</div><?php
		}
		?>
	</div>		

	<h2 class="h-border">Detail/edit Jenis Surat Mahasiswa</h2>
	<div class="row">
        <div class="col-md-12">
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Setting Umum</a></li>
              <li><a href="#tab_2" data-toggle="tab">Setting Verifikasi</a></li>
              <li><a href="#tab_3" data-toggle="tab">Setting Auto data</a></li>              
              <!-- <li class="pull-right"><a class="text-muted" href="#"><i class="fa fa-gear"></i></a></li> -->
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">	                
					<h3><b>Data Umum</b></h3>
					<table class="table table-nama detail-surat">
						<tbody>
							<tr >
								<td style="width:200px" class="tdlabel-abu">NAMA SUB JENIS SURAT</td>
								<td> : <?= $jenis_sakad['NM_JENIS_SAKAD']; ?></td>
							</tr>
							<tr>
								<td class="tdlabel-abu">
								JENIS SURAT INDUK</td>
								<td> : <?php $nama_jns_srt_induk = convert_kd_jenis_surat_induk($jenis_sakad['KD_JENIS_SURAT_INDUK']); 
									echo $nama_jns_srt_induk;
								?>	
								</td>
							</tr>
							<tr>
								<td class="tdlabel-abu">GRUP</td>
								<td> : Surat Akademik Fakultas</td></tr>
							<tr>
								<td class="tdlabel-abu">DESKRIPSI</td>
								<td> : <?php echo $jenis_sakad['KETERANGAN'];?></td></tr>
							<tr>
								<td class="tdlabel-abu">NOMOR URUT</td>
								<td> : <?php echo $jenis_sakad['NO_URUT'];?></td></tr>
							<tr>
								<td class="tdlabel-abu">STATUS KEAKTIFAN</td><td><?php echo ($jenis_sakad['IS_AKTIF'] == 't') ? 'AKTIF': 'TIDAK AKTIF' ; ?></td></tr>
							<tr>
								<td class="tdlabel-abu">
									Mode Penerbitan Surat
								</td>
								<td>
									<div class="form-group">
										<div class="radio">
											<label>
												<input name="optionsRadios" id="optionsRadios1" type="radio" checked="" value="option1">
												Pengembangan / dev
											</label>
										</div>
										<div class="radio">
											<label>
												<input name="optionsRadios" id="optionsRadios2" type="radio" value="option2">
												Pengujian / test
											</label>
										</div>
										<div class="radio">
											<label>
												<input name="optionsRadios" disabled="" id="optionsRadios3" type="radio" value="option3">
												Produksi / prod
											</label>
										</div>
									</div>
								</td>
							</tr>			
						</tbody>
					</table>
              </div>
              <!-- /.tab-pane -->
            	<div class="tab-pane" id="tab_2">
            		<h3 class="box-title">Pengaturan untuk Verifikasi</h3>			
					<div id="container-info-persyaratan">
						<?php if (isset($current_persyaratan) && !empty($current_persyaratan)) {
						$html = '<table class="table table-bordered table-hover">';
						$html .= '<thead>
							<tr>
								<th width="20px">No. </th>
								<th width="190px">Nama Syarat</th>
								<th>Isi Required</th>
								<th width="130px">Aksi</th>
							</tr>
						</thead>';
						foreach ($current_persyaratan as $item) {				
							$html .= '<tr>';
							$html .= '<td>-</td>';
							$html .= '<td>'.$item["OUT_TXT_JUDUL"].'</td>';
							$html .= '<td>'.$item["ISI_REQUIRED"].'</td>';
							$html .= '<td>lihat</td>';
							$html .= '</tr>';
						}
							$html .= '</table>';
							echo $html;
						}		
						?>
						<button id="show_edit_syarat" class="btn-uin btn btn-inverse btn btn-medium">Edit</button>
					</div>
					<div id="container-kelola-persyaratan" class="bs-callout bs-callout-chating">
						<div id="aksi-callout">
							<button class="btn btn-small btn-danger btn-act right" id="cancel_edit_syarat" title="Tutup"> &nbsp;x&nbsp; </button>
						</div>						
						<table style="width:100%">
							<tbody>
								<tr><td>													
										<span class="title-surat">Atur Persyaratan</span>
										<div class="form-persyaratan form-surat">
										<?php
										$act_url = base_url('modul_surat_akademik/admin_master_surat_akademik/perbarui_persyaratan_sjs');
										$haystack_mp= array_column($current_persyaratan, 'KD_PERSYARATAN');
										//echo print_r($haystack_mp);
										?>
										<form action="<?= $act_url; ?>" method="POST">
											<select id="master_persyaratan" class="multiselect" multiple="multiple" name="update_persyaratan[]" >
												<?php 
												foreach ($master_persyaratan['data'] as $item) {
													if (in_array($item['KD_PERSYARATAN'], $haystack_mp)){
														echo '<option value="'.$item["ID"].'" selected>'.$item["NM_PERSYARATAN"].' ('.$item["KD_PERSYARATAN"].')</option>';	
													} else {
														echo '<option value="'.$item["ID"].'">'.$item["NM_PERSYARATAN"].' ('.$item["KD_PERSYARATAN"].')</option>';
													}
												}
												?>	
											</select>

											<span class="pull-right">				
												<input type="hidden" name="btn-perbarui-persyaratan" value="perbarui persyaratan">
												<input type="hidden" name="kd_sjs_syarat" value="<?= $jenis_sakad['KD_JENIS_SAKAD'];?>">
												<input type="submit" class="btn-uin btn-inverse btn btn-medium" id="btnSubmitEditSyarat" name="btnSubmitEditSyarat" value="Perbarui persyaratan">		
										</form>								
										</div>
										
								</td></tr>
							</tbody>
						</table>
					</div><!-- #/container-kelola-persyaratan -->
				</div>
              <!-- /.tab-pane -->
            	<div class="tab-pane" id="tab_3">
                	<h3 class="box-title">Pengaturan Automasi Form (template)</h3>			
					<div class="container-automasi-form">
						<fieldset>
							<legend>Tambah Config</legend>
							<table>
								<tr class="underline">
									<td style="width:152px" valign="top">Unit/Bagian/Fakultas</td>
									<td style="padding-top:10px;padding-bottom:2px" valign="top">
										<select id="kd_tu_bagian" name="unit_id" class="kd_tu_bagian">
											<option value="UN02001">-- Pilih Unit/Bagian/Fakultas --</option>
											<option value="UN02002">Fak. Adab dan Ilmu Budaya</option>
											<option value="UN02003">Fak. Dakwah dan Komunikasi</option>
											<option value="UN02004">Fak. Syari'ah dan Hukum</option>
											<option value="UN02005">Fak. Ilmu Tarbiyah dan Keguruan</option>
											<option value="UN02006">Fak. Ushuluddin dan Pemikiran Islam</option>
											<option value="UN02007">Fak. Sains dan Teknologi</option>
											<option value="UN02008">Fak. Ilmu Sosial dan Humaniora</option>
											<option value="UN02009">Fak. Ekonomi dan Bisnis Islam</option>							
										</select>
									</td>
								</tr>
								<tr class="underline">
									<td>Pilih Pejabat PSD</td>
									<td style="padding-top:10px;">
										<div id="token_input_plugin">
											<input type="text" name="pejabat_psd" id="pejabat_psd" class="" />
										</div>
									</td>
								</tr>
								<tr class="underline">
									<td>Penandatangan Surat</td>
									<td style="padding-top:10px;">
										<div id="token_input_plugin">
											<input type="text" name="id_psd" id="id_psd" class="" />
										</div>
									</td>
								</tr>
								<tr class="underline klasifikasi_surat">
									<td>Klasifikasi Surat</td>
									<td style="padding-top:10px;">
										<div id="token_input_plugin">
											<input type="text" name="id_klasifikasi_surat" class="wajib id_klasifikasi_surat" id="id_klasifikasi_surat"/>
										</div>
									</td>
								</tr>
								<tr class="underline" id="tr_keamanan_surat">
									<td>Tingkat Keamanan Surat</td>
									<td style="padding-top:10px"> 
										<select id="kd_keamanan_surat" name="kd_keamanan_surat" class="wajib">
											<option value="">--Pilih Tingkat Keamanan Surat--</option>
											<?php
											if(!empty($keamanan_surat)){
												foreach($keamanan_surat as $data){ ?>
													<option value="<?php echo $data['KD_KEAMANAN_SURAT'];?>"><?php echo $data['NM_KEAMANAN_SURAT'];?></option><?php
												}
											}
											?>
										</select>
									</td>
								</tr>
								<tr class="underline" id="tr_ts">
									<td class="tdlabel">Atur Daftar Tembusan</td>
									<td class="tdinput">
										<div class="tujuan-surat">
											<div class="auto-surat grup" id="ts_pejabat_grup">
												<div class="label-input">	
													Pejabat
												</div>
												<input type="text" name="ts_pejabat" id="ts_pejabat" />
											</div>
										</div>
									</td>
								</tr>			
							</table>
						</fieldset>

						<table class="table table-bordered table-hover">
							<tbody>
								<tr class="tebal center">
									<th width="26px">No</th>
									<th width="70px">Unit</th>
									<th width="160px">Sifat dan keamanan</th>
									<th width="100px">Klasifikasi</th>
									<th width="160px">Frasa</th>			
									<th width="70px">Penandatanganan</th>					
									<th width="80px">Aksi</th>
								</tr>
								<?php if (isset($conf_auto)) : 
								$no = 0;
								foreach ($conf_auto as $key => $val) { ?>
								<tr>
									<td><center><?= $no++; ?></center></td>
									<td>
										<b><?= $val['UNIT_ID'] ?></b>						
									</td>
									<td><b>Sifat:</b> <?= $val['KD_SIFAT_SURAT']; ?> <br/><b>Tingkat keamanan:</b> <?= $val['KD_KEAMANAN_SURAT']; ?></td>
									<td>
										<?= $val['ID_KLASIFIKASI_SURAT']; ?></td>
									<td>
										<b>Perihal :</b>
										<br />
										<p><?= $val['PERIHAL']; ?></p>
									</td>
									<td>
										<b>Penandatangan:</b> <?= $val['PSD_ID']; ?>
										<br />
										<b>Atas nama:</b> <?= $val['PSD_AN']; ?>
									</td>					
									<td>
										<center><a class="btn btn-default btn-small" href="#" title="detail item config sakad" disabled="">Detail</a></center>
									</td>
								</tr>
								<?php } 
								endif; 
								?>
							</tbody>
						</table>
					</div>
					
            	</div>
              <!-- /.tab-pane -->

            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div> <!-- /.col -->              
     </div>															
	</div>	
	</div><!-- ./content-space -->
</div>

<div class="modal fade" id="modalDeleteRiwayat">
  <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><i class="icon-remove"></i></button>
				<div class="contenttitle">
					<h3>Konfirmasi</h3>
				</div>
			</div>
			<div class="modal-body">
				<p><!--<img class="modal-icon" src="img/trash.png" />--> Apakah Anda yakin ingin menghapus data ini ?</p>
			</div>
			<div class="modal-footer">
			
				<form action="<?php echo base_url('pegawai/admin_master/del_his_jenis_surat');?>" method="POST" >
					<a href="#" class="btn btn-default rec" data-dismiss="modal">Tidak</a>
					<input type="hidden" id="id_his_jenis_surat" name="id_his_jenis_surat" value="" />
					<input type="hidden" name="kd_jenis_surat" value="<?php echo $jenis_surat['KD_JENIS_SURAT']; ?>" />
					<input type="submit" class="btn btn-danger" value="Ya" />
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">		
	function delete_riwayat(el){
		var id_his_jenis_surat = el.data('id_his_jenis_surat');
		document.getElementById('id_his_jenis_surat').value = id_his_jenis_surat;
		$('#modalDeleteRiwayat').modal('show');
	}
</script>
<script type="text/javascript">
	/*var options = {
	  'disableSubmitBtn' : false
	};
	var metrics = [
		[ '.wajib', 'presence', 'Tidak boleh kosong' ],[ '.angka', 'integer', 'Harus angka' ],[ '.mail', 'email', 'Masukkan email dengan benar' ],[ '#repass', 'same-as:#pass', 'Password harus sama, ulangi kembali' ],
		[ '.repass', 'same-as:.pass', 'Password harus sama' ],[ '.min6', 'min-length:6', 'Minimal harus 6 karakter' ]
  	];
  	$("#validate").nod( metrics,options);
  	$("#validate_active").nod( metrics);*/
</script>