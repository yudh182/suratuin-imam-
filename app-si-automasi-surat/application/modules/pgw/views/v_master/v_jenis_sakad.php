<div class="col-med-9">	
	<?php //echo create_breadcrumb(site_url('modul_surat_akademik')); ?>
	<h2 class="h-border">Master Jenis Surat Akademik</h2>
	<ul id="crumbs">
		<li>
			<a title="Master Data" href="#">Master Data Surat Akademik</a>
		</li>			
		<li>
			<a title="Master Data Jenis Surat Akademik" href="#">Jenis Surat (administrasi akademik)</a>
		</li>
	</ul><br/>
	<?php
	$sess_errors = $this->session->flashdata('errors');
	if((!empty($errors))||(!empty($sess_errors))){ ?>
		<div class="bs-callout bs-callout-error" style="margin-bottom:5px">
			<?php
			if(!empty($errors)){
				if(is_array($errors)){
					foreach($errors as $value){
						echo "- ".$value."<br/>";
					}
				}else{
					echo $errors;
				}
			}
			if(!empty($sess_errors)){
				if(is_array($sess_errors)){
					foreach($sess_errors as $value){
						echo "- ".$value."<br/>";
					}
				}else{
					echo $sess_errors;
				}
			}
			?>
		</div><?php
	}
			
	$sess_success = $this->session->flashdata('success');
	if((!empty($success))||(!empty($sess_success))){  ?>
		<div class="bs-callout bs-callout-success" style="margin-bottom:5px">
			<?php
			if(!empty($success)){
				if(is_array($success)){
					foreach($success as $value){
						echo "- ".$value."<br/>";
					}
				}else{
					echo $success;
				}
			}
			if(!empty($sess_success)){
				if(is_array($sess_success)){
					foreach($this->session->flashdata('success') as $value){
						echo "- ".$value."<br/>";
					}
				}else{
					echo $sess_success;
				}
			}
			?>
		</div><?php
	}
	?>
	<table class="table table-bordered table-hover">
		  <thead>
			  <tr>
				  <th width="22px"><center>No</center></th>
				  <th width="300px"><center>Nama Jenis Surat Akademik</center></th>
				  <th width="260px"><center>Jenis Surat Induk</center></th>
				  <th width="260px"><center>Grup</center></th>
				  <th width="260px"><center>Status</center></th>
				  <th width="100px"><center>Aksi</center></th>
			  </tr>
		  </thead>   
		  <tbody id="content-check">
			<?php
			if(!empty($jenis_sakad)){
				$n = 1;
				foreach($jenis_sakad as $val){ ?>
					<tr>
						<td><?= $n; ?></td>
						<td><?= $val['NM_JENIS_SAKAD']; ?></td>
						<td><?php echo ($val['KD_JENIS_SURAT_INDUK'] == '11' ? 'SURAT KETERANGAN' : 'SURAT NASKAH DINAS (UMUM)'); ?></td>
						<td><?php echo ($val['NM_GRUP'] !== ''? $val['NM_GRUP'] : 'Grup belum diatur' ); ?></td>
						<td><?php echo ($val['IS_AKTIF'] == 't'? 'AKTIF' : 'TIDAK AKTIF'); ?></td>
						<td class="centered">
							<a class="btn btn-small btn-default" href="<?php echo site_url('pgw/master_auto_surat_mhs/jenis/detail/'.$val['KD_JENIS_SAKAD']);?>">Lihat & Edit</sa>
						</td>
					</tr><?php
					$n++;
				}
			}else{ ?>
				<tr><td colspan="5"><center>BELUM ADA JENIS SURAT AKADEMIK YANG DITAMBAHKAN</center></td></tr><?php
			}
			?>
		  </tbody>
	 </table>
</div>