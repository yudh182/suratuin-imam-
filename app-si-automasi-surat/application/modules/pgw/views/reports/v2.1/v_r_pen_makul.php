<?php
//============================================================+
// template report   : Surat Ijin Observasi
// Last Update : 2018-03-23


$pdf = new Pdf_creator(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->set_document_properties(
    array(
        'pdf_title'=>'Surat Ijin Penelitian',
        'pdf_margin' => ['15','30','15', true], #LEFT, TOP, RIGHT (PAGE MARGINS)
        'pdf_template_margin' => ['6','6'], #MARGIN HEADER, MARGIN FOOTER
        'pdf_page_break' => [true,'5'] #aktif/tdk aktif, MARGIN BOTTOM (DEFAULT : isikan INITIAL)
    )
);
$pdf->setData($template);

// add a page
$resolution= array(175, 266);
$pdf->AddPage('P', $resolution);

// The '@' character is used to indicate that follows an image data stream and not an image file name
if (isset($watermark_img)) :
    $base64_decoded = base64_decode($watermark_img);
    $pdf->Image('@'.$base64_decoded);
endif;

// set some text for example
$nama_surat ='<table>
    <tr>
        <td width="50px">Nomor</td>
        <td width="210px">: '.$nomor.'</td>
        <td align="R" width="250px">'.$isi_surat["tgl_mulai"].'</td>
    </tr>
    <tr>
        <td  width="50px">Lamp.</td>
        <td>:</td>
        <td></td>
    </tr>
    <tr>
        <td  width="50px">Hal</td>
        <td>: Permohonan Izin Penelitian</td>

        <td></td>
    </tr>
</table>';

$txt1='Kepada:<br>
Yth. '.$tujuan["kepada"].' '.$tujuan["instansi"].'<br>
di '.$tujuan["alamat_tempat"].'
<p>
<i>Assalamualaikum Wr.Wb</i><br><br>
Dengan hormat, sehubungan dengan pelaksanaan mata kuliah <i>'.$isi_surat["nama_makul"].'</i> pada Program Studi '.$prodi.' '.ucwords(strtolower($template["kopsurat"]["fkl"])).' Universitas Islam Negeri Sunan Kalijaga pada '.strtolower($thn_akademik["semester_sekarang"]).' Tahun Akademik '.$thn_akademik["tahun_ajaran"].' , maka kami memberikan tugas kepada mahasiswa berikut :';
$txt2 = '<table border="1" cellpadding="3">';
$txt2 .=  '<tr><td align="C" width="30px">No.</td><td width="200px">Nama</td><td>NIM</td></tr>';
$no = 1;
foreach ($isi_mhs as $key => $val) {
    $txt2 .= '<tr>';
    $txt2 .= '<td>'.$no++.'</td>';
    $txt2 .= '<td>'.ucwords(strtolower($val["NAMA"])).'</td>';
    $txt2 .= '<td>'.ucwords(strtolower($val["NIM"])).'</td>';
    $txt2 .= '</tr>';
}
$txt2 .= '</table>';
$txt3 = '<p>untuk mengadakan penelitian ke '.$isi_surat["tempat"].' pada tanggal '.$isi_surat["tgl_mulai"].' berkaitan dengan <i>'.$isi_surat["topik_penelitian_makul"].'</i>. Sehubungan dengan hal tersebut, dengan ini kami memohon kesediaan Bapak/Ibu '.$tujuan["kepada"].' untuk memberikan izin dan memfasilitasi kepada mahasiswa tersebut diatas.</p>
<p>Demikian surat ini kami sampaikan, atas perhatian dan kerjasama Bapak/Ibu kami ucapkan terimakasih.</p><br>
<i>Wassalamualaikum Wr.Wb</i>';


// $txt4 = '<table><tr>
// <td></td>
// <td></td>
// <td width="200px" align="L">Yogyakarta '.$tgl_surat.' <br>
//          a.n. Dekan<br>
//                 Wakil Dekan Bidang Akademik
//                 <br><br><br><br>




//        '.$nm_pgw_psd.'<br></td>
//         </tr>
//         </table>
// ';
// $txt4 = '<table cellpadding="0">
//     <tr>
//         <td colspan="2"></td>
//         <td align="R">a.n.&nbsp;&nbsp;&nbsp;&nbsp;</td>
//         <td align="L" colspan="2">'.$label_psd_an.'</td>
//     </tr>
//     <tr>
//         <td colspan="2"></td>
//         <td align="L"></td>
//         <td align="L"  colspan="2">'.$label_psd.',
//         <br><br><br><br>
//         '.$nm_pgw_psd.'
//         </td>
//     </tr>
// </table>';
//uji
// $txt4 = '<table>
// <tr>
//     <td align="R">
//         a.n. Dekan<br>
//         '.$psd["label_psd"].'<br>
//         <br><br>
//         <br>
//         <b>'.$psd["nm_pgw_psd"].'</b><br>
//     </td>
// </tr>
// </table>';
$txt4 = '<table cellpadding="0">
    <tr>
        <td colspan="2"></td>
        <td align="R">a.n.&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td align="L" colspan="2">Dekan</td>
    </tr>
    <tr>
        <td colspan="2"></td>
        <td align="L"></td>
        <td align="L"  colspan="2">'.$psd["label_psd"].',
        <br><br><br><br>
        '.$psd["nm_pgw_psd"].'
        </td>
    </tr>
</table>';
$txt5='<p><u>Tembusan</u><br>
Dekan(Sebagai laporan)';

// set font
$pdf->SetFont('times', '', 10);
// print a blox of text using multicell()
$pdf->WriteHTMLCell(0,0,'','',$nama_surat, 0, 1, 0, true, 'L', true);
$pdf->WriteHTMLCell(0,0,'','',$txt1, 0, 1, 0, true, 'J', true);
$pdf->WriteHTMLCell(0,0,'','',$txt2, 0, 1, 0, true, 'J', true);
$pdf->WriteHTMLCell(0,0,'','',$txt3, 0, 1, 0, true, 'J', true);
$pdf->WriteHTMLCell(0,0,'','',$txt4, 0, 1, 0, true, 'R', true);
$pdf->WriteHTMLCell(0,0,'','',$txt5, 0, 1, 0, true, 'L', true);

$style = array(
    'border' => 0,
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255)
    'module_width' => 0.1, // width of a single module in points
    'module_height' => 0.1 // height of a single module in points
);
// QRCODE,L : QR-CODE Low error correction
$pdf->write2DBarcode($qrcode_otentikasi, 'QRCODE,L', $pdf->getX(), '', 20, 20, $style, 'N');

// move pointer to last page
$pdf->lastPage();

// ---------------------------------------------------------
//ob_end_clean();

//Close and output PDF document
ob_clean(); //untuk menghapus output buffer
$pdf->Output('surat_ijin_penelitian_makul'.time().'.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
