<?php
//============================================================+
// File name   : v_r_pindah.php
// Begin       : 2018-03-22
// Last Update : 2018-03-25
//
// Description : Template tcpdf untuk surat keterangan pindah studi

// create new PDF document
$pdf = new Pdf_creator(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->set_document_properties(
    array(
        'pdf_title'=>'Surat Keterangan Pindah Studi',
        'pdf_margin' => ['15','35','15', true], #LEFT, TOP, RIGHT (PAGE MARGINS)
        'pdf_template_margin' => ['6','6'], #MARGIN HEADER, MARGIN FOOTER
        'pdf_page_break' => [true,'30'] #aktif/tdk aktif, MARGIN BOTTOM (DEFAULT : isikan INITIAL)
    )
);
$pdf->setData($template);

// add a page
$resolution= array(175, 266);
$pdf->AddPage('P', $resolution);

// set some text for example
$nama_surat ='
<b><u style="border: 1px;">'.$judul_surat.'</u><b><br>
Nomor : '.$nomor.'
';
$txt = '<p>Yang bertanda tangan di bawah ini : </p>
<table cellpadding="0" cellspacing="0">
            <tr>
                <td width="120pt">Nama</td>
                <td width="360pt">: '.$pejabat_dalam_surat["NM_PGW_F"].'</td>
            </tr>
            <tr><td>NIP</td><td>: '.$pejabat_dalam_surat["NIP"].'</td></tr>
            <tr><td>Pangkat / Gol. Ruang</td><td>: '.$pejabat_dalam_surat["HIE_NAMA"].' / '.$pejabat_dalam_surat["HIE_GOLRU"].'</td></tr>
            <tr><td>Jabatan</td><td>: '.$pejabat_dalam_surat["STR_NAMA"].'</td></tr>
        </table>
        <p>Dengan ini menerangkan bahwa:</p>
<table>
     <tr>
        <td width="120pt">Nama</td>
        <td width="360pt">:  '.$isi_mhs["nama"].'</td>
    </tr>
    <tr>
        <td>Nomor Induk Mahasiswa</td>
        <td>:  '.$isi_mhs["nim"].'</td>
    </tr>
    <tr>
       <td>Semester</td>
       <td>:  '.$isi_mhs["smt"].'</td>
    </tr>
     <tr>
        <td>Tahun Akademik</td>
        <td>:  '.$thn_akademik["tahun_ajaran"].'</td>
    </tr>
</table><br><br>
adalah mahasiswa Fakultas '.$isi_mhs["fkl"].' UIN Sunan Kalijaga Yogyakarta, mahasiswa tersebut: Tercatat pada Semester 1 sampai dengan Semester '.$isi_mhs["smt"].' Tahun Akademik '.$thn_akademik["tahun_ajaran"].' pada Program Studi '.$isi_mhs["prodi"].'.<br>

Surat keterangan diterbitkan sebagai salah satu syarat untuk '.$isi_surat["keperluan"].'.<br><br>
Demikian agar dapat dipergunakan sebagaimana mestinya.';
$txt2 = '<table>
<tr>
    <td></td>
    <td></td>
    <td align="L" width="200px"><br>
		a.n. Dekan<br>
        Wakil Dekan Bidang Akademik<br><br><br><br>




        '.$nm_pgw_psd.'</td>
</tr>
</table>
';

$txt3='<p>Tembusan :<br>
1. Dekan(Sebagai laporan);<br>
2. Kepala Biro AAAK;<br>
3. Yang bersangkutan.</p>
';


// set font
$pdf->SetFont('times', '', 10);
// print a blox of text using multicell()
$pdf->WriteHTMLCell(0,0,'','',$nama_surat, 0, 1, 0, true, 'C', true);
$pdf->WriteHTMLCell(0,0,'','',$txt, 0, 1, 0, true, 'L', true);
$pdf->WriteHTMLCell(0,0,'','',$txt2, 0, 1, 0, true, 'R', true);
$pdf->WriteHTMLCell(0,0,'','',$txt3, 0, 1, 0, true, 'L', true);

$style = array(
    'border' => 0,
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255)
    'module_width' => 0.1, // width of a single module in points
    'module_height' => 0.1 // height of a single module in points
);


// QRCODE,L : QR-CODE Low error correction
$pdf->write2DBarcode($qrcode_otentikasi, 'QRCODE,L', 18, '', 20, 20, $style, 'N');

// move pointer to last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
ob_clean(); //untuk menghapus output buffer
$pdf->Output('surat_keterangan_pindah_studi-'.time().'.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
