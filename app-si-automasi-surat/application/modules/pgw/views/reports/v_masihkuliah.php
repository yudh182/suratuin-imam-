<?php
//============================================================+
// File name   : v_masihkuliah.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Template tcpdf surat keterangan masih kuliah


// create new PDF document
//$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf = new Pdf_creator(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->set_document_properties(
    array(
        'pdf_title'=>'Surat Keterangan Masih Kuliah',
        'pdf_margin' => ['15','35','15', true], #LEFT, TOP, RIGHT (PAGE MARGINS)
        'pdf_template_margin' => ['6','6'], #MARGIN HEADER, MARGIN FOOTER
        'pdf_page_break' => [true,'30'] #aktif/tdk aktif, MARGIN BOTTOM (DEFAULT : isikan INITIAL)
    )
);
$pdf->setData($template);

// add a page
$resolution= array(175, 266);
$pdf->AddPage('P', $resolution);

$base64_decoded = base64_decode('iVBORw0KGgoAAAANSUhEUgAAAZAAAAEvBAMAAACZDYpfAAAAA3NCSVQICAjb4U/gAAAAElBMVEX///8zMzMzMzMzMzMzMzMzMzPrq7PdAAAABnRSTlMAESIzRFWQJKqlAAAACXBIWXMAAAsSAAALEgHS3X78AAAAFnRFWHRDcmVhdGlvbiBUaW1lADA2LzIzLzA5bKSWcAAAABx0RVh0U29mdHdhcmUAQWRvYmUgRmlyZXdvcmtzIENTNAay06AAABKJSURBVHic3Z3LYtyoEob75r0TR3vNpL33xMNemTm9T+zW+7/KsYCCqqIQAtFuNLVI3LryAX9xF7vdGtu/Xsbx3x+Pqx5yf9ufR2PXv+4dlFW2V6Ozf+4dmBWGOTZNch7H/wTJcWT2894hKrP9hYOM/b3DVGTfAo7x/d5hKrFDyDGOw71DVWCdBHK9d6jyTVDINpPk5FPhx5/fHdX2VKJc6TFVs1xVZXOOy0n9zR4Akt93DVa+gdSvrtqrtpm3QBSDO3LYZN6CUGN3+7xFv3UKEsRVvbYlEvBZpGF42Z5I9sxlGXveXuEOFfgXcvRBSKXGreO+V9txe25LSTkLXBkk034DLcaL6Gn3FOTYfts3UvYxkK79VvwpLA0nYyBTsjVO0okSYSAm2domiVRGqNhtsjXds6JkP3skIM8bqLHIEtntvv6N+C5SmdmWWS1IlaoJpdd/gWdruZw/zGWar3+boJ/itM3YUdY6tS1I5Lgk929AIpBrZnP/FiSyqLq+BYlYkPkG1BYkYkHmI3sLElkCsgmJLAHZhESs+50N5CYksgRkExKxApjzWtuQCFQaZ0K5DYlANX4m32xDIqCAIXVB6xKBCOdNdm9WIjEV/XGbUBVYF4aTdDJYicRIL83MJArr8SfSgJ8fKflIr1ZIwrbuhUS/lUgv3z2lVyskiiXJNxLshER0ejVC0tmg2qJETx7wSZKSyNgOCYyPvGsSO/+sh7MqJZFUxeATDQZ13z9c6dcLTQEYzurlWxPp9ckmTHFyIT8ukEgzg797CcRGcjcb5Yn0+nR7FkCsO56XSCK9Pt2kaWeDPpOI8vn0uoOFKrEJkohyml6n+3vhcOKZLR6zJPLcQHnCMxeEPEsilxZKxifC4Ur5HInoyGiMpLcH8yQyNkfiApMpkUZIoHZy9Y2+XIk0QqLH2t7RMph8ibRCwqxAIrci2a/qVSuRyI1Iju9rSIokchuSblxBUiiRuXuK7TKuICmViL6pL32raDqaikmKJVKfxERTafdzuUSqk9hoKpueBFEeSVCaXoB1vg0JRFMRiQ1bLD1pejmsm5B4T1JCYsMWcaUsvTzWLUiQJykgUXCvSELTCyv/BiTYk2SToL4VicSmlx3+IcpX/s5KJMST5JLgFZYCiQ2t1CTGi2WrkLDCNpcEr3kNSKIS0ScRSY2hR1bYZleA5kjiEtG/PUmNrqKg320FCc8iNi9JEpnMkwxrCKytL2zjJHSEtAti35HkvlIwkAhqjF9zq10xEjaJQIWxb0lq9Ka6gX5Ekj0DOUJCJxGI9X1DUlEiv0kHSZ+8jZlMQicRyPV9TWLz3vc17TuUjT1J/vwFkUSUCHcmE4kFuKxo35Fs7EiyVSKSyBIJZkfs1bu7vpyEZuNvkXctsZBElkjoFvd/+bAUk7C5MGuW32KSx/DZqDITcfD6+lISNtAPIwdFVQZPcsXPZhKJk5jryzxYMHAMFZaieHGVhDf07EAiMZJVU9fCgeP5uRjz5grqAT07kEiMpEYcDsGRsnlXQNKjJ4USiZBAehaBhPFv4+VXydOAJCURmQQqffz4EhPmVrAF0efMx2qSGYn8L0ri2kUlIMLcCrbk5ZJbF55IXtCzmUQeo1Xl0xoQG00DDggBOeTX6j9I4qXIe7yq7FxegTyl+ihNkZOUmVNPfdX/iRKZHqtkEtcuKgCRlnZTjTwLmXmZyRKZ3iT3OviugwKQTogY6rUuQmZeZhGJ7DjJI71+LGr2ugeGbQjztEN4fqlRiTAsT3Jl148lnh8VtpE2xCk8v9SoRFjd1JO8sespyH7Zu3BhCyGFY4/4/QUktvbJhhNc9qeVGRoWBLJwAk6HQGDcXJEURz0suSRPOMLD+iCpzNCwoCbEwgk4aiT2Y+ed/Bt5fznJYP4WljLgygwNCwJZNgEnmNt3fVX0YbQTMrf9+zTyETfSXPOVGRYWf9XCCTjBF/6QveD3g+U23p6iEnEkgxAWPps4SdLFOUKJlJFEJQIkvRAWD/Is3haYMpf9VCGIIJEiEnt5bLXP/m8WFgay7IM+rqJFv+qpbSDvR+fLBkyTq31sWK404LNrz735jr+A5EpKkQGfL/qewyUVCzYs/1AQG5GpulfnE5KT/Cbv78n5Pp/DZdEoiQ3LnxQESWQ/o3iFchEj6fH7r/R8QUeR9+IxEvP46969UBuSyDHuu2hbhJD8JO/X2kMzZYdsEOTFZRIbljf7vz16QO/r4l6Y9Y3jOH8k7zcBPziS/LEMXEiIJPAhBgpyQjE9hS5C0qH41iTuC9G9PUCSDJHkN3zw3FSJpIM3mf9JRF59WGQSFYTq6YI5guEMR1LQp4lJBL+n4E0EBPWg2rBIJNJcmP3311c/d5QnmSMpGHMgJLFB7DcIu3k+k0iMJDEXZif1sMCgQ0kv5ByJ/1aJsllsMi6RyKstY7KwJeVGyFaFpHOZA4MEEtE/e/ZYQSLMpBG/U4q+jES5zIFBQolIJInpYjt5ht9eOLaeBHUJPvvYxRKZGckvkchu7fclIyRo9SwCwRKZIelSmUSeBOszgbYfVUg6nzkQiHmVy9kxEluQJyXCyq9nArLPrtWLJCh2OheqIBfPkyQlwkhoipzy2ycCCe41N2+d+oPCz0fNkaQlwkioRp6DFMsjMcHBC8w9SIcv8a+LkKQlQknYWvBLkGILDEWsyQ8d+vHgQExEEucfnXK3V2mJUBJ7lHYpFJOQxZjmhwH5HXH0cZIFEiFhtUdpx3QxiXkMGVg6wd/yF9ZwPlnYX4jYfVihdWUf/0zPZpOQfnLz4wggNspYYPN7PoNOyH+nxGOVRt9oLCMhPRyPCORNlMiupOcz7IS8/vHlO/xNqtnFJFYixBECSKQulN/zaVP2je3/MuKXkugpIDGqoH1XBxv+yEcI83s+FTxIIsHDi85y+7vOkkQsyPsSiVjkeUN1YxWCBBLRlj3JVpCIAzFvjUgE9XwOiZegunG4YvqK37mCxBitK5gYfJ+XyG9fnvSJp3f2julvPlGblyLrSFj3ru2hm5fIiysZkz1TCu5AtzvrzTU2et7XkbDuXZsUnRjfaHDCkmRIZBcMo4CqIXpm+0eSxkdAzE8lxjcenDA5fkg8nTYf5VLIR88qEt7kxK/iVVoyOKH7pvrE0zvyILlegKJnDYliQcb5mFdp6fjdB0lSIvQOk9F+6nL9HThI9OCPWmSSmMzug6zQo1hpx8fvDpeURPgdmuuj2vDlzy8R2NnFMAtIfJARCG/1BeN3hyHxbH6HfjhLRgaLSTL7IT9IUJARyKxEFhm/wzy8J9dwWERCL0zbXpGpDtEIEYe4Z43fYR4+kGuC2amOJH/4ZN8Hz8U521r+rODgjk7/pMoKZ6eiyR9zA34J8yBJiSQtuKMLnyDMTgWSYfJzxSRdSiLD8mcFojI9AiO+Rvzy1xkS6VD+7QAPEpFIv/xZgahO4SPk6DnbRJpqBoUkHkSWSI4Cv/EHCZ9aj8wAOJtE0jeUkTwkJJI1EHBmDwpBXJOetwvPOpFWfFXHgayXyM6QoAfZVEUz9HwG4KHVM9AfxMyxyFwNdb1EdHDIg8K5LWqMkky2AsSl9aN4PLuQOuMH7TkI6fUSSGyC5b4UBTiQSFcgEW3f0d9BC5r2eoUkFUC4RFSJRALjIN04T0KHg7IMqhVMIpU+kMibcWqcJ1HlINBpU0ki8wELvzz5l3R94eq1S1WJSAFz0SHMTqUka5bhGZLbSAQKI/gJ0RNtq5tDZdMKTBfAbSTCQVz0REgCd51pT7eSCBnlI9Ej9zrYCwrXzX3Y040kwkBw9Ii9DmTyRZF9Yb8rSYRVOUj0SL0OB/pzvVX7zO4DeQ6NHkQCojhWeq2zWhKhIePR40lepMtrWC2J0JAF0eMmdbJlX8zzvJZUho3VkgjN9GH0nNkRsRZ/LP92QL0vUZOpREL0nOkRi0qfocq/HVBNIqSrWYyeMznSCe/VgSkkqSaRnSX55UIURM8ZH5Fq8SYdy0iqSWSyiUSD2Ojh9Y9zsA6HRKBr+BWQVP5Y+weJBlFY+MjOg/tThSD2rqJorSgRbabTPD47lY9x4DRzfSMlja1IHig33TUN0TOXRS7Bi9es8E7PUC6ybkFmN1fghaokQXKnpgY9n1XMZfY4yT5QA0mQ/KmpvOezhqHmepSE7eLME+RYNimqnkS04ZkpMZID95buHn1oypwFJJUlgtb1REk4iEsQ45B15swnqSwRRiJecWTipAliM14uyff0JbmGSeZWfrFKPVxcu3hbYSkSNlIHV1/JFJ0m9iJJkDyQKHcJYseElPk1fFZgZ22epCP6YQnS2HY9syQdzjs8QRqSiLY5ElKLdwliT7YkEW0zJEYF7KMRAznZ0HY9MyQYhCdIg9v1xEnMcV35DRKESWTZ10JubZgEj7ejoMMVrgpgJYInGTZAggZ8sHjNkamSFw6aK/J7xcSCuubHRwZ/cO9VECQIayjbcZeWSHp/zFd+wwRhyzgBtB0SXL4dXKSH698FibRFgiXiKr/CV/4kiTRFMqADFsQ3Jj2lLJGWSHr0+2Qzmxtf7NmpQCLtkJAqoKnFv7sEQUUMnWTIFqD0nxjkiD2RKqAR9BskCO7Ko5MM2QKUytv1FBnpGDAg70KCsHm4bAlbEyTYaPhwgsxJpEGS+GI4KhGoP87szHBfUySS8RkqEZMFb7VdTwUjIAM6wSSi3BWNkuCsL32L5HO266lgOEFIh60sEf1DNUiCp9fRLtWIRPRNDZJgEJIgUYnouxBJ5ZGQUjvEQjS/FwkiaaSrCIH05MSMRCbzJMPnBXbOjrGYnZGINkfSfzyk+mhIvnmQnhyflYg2SzIlUrdq689KRj+062xeIuaggvue12z9Wc0MCR+JT0hEmyaZ8p5aszVMPdMkfImMKJHYdj3TxY2Q8ASRJRLZrsd+Z6AJEp4gWdv1rJ9vW83OPDbpEqmZb8qh07WH1qsYXSLVjbMkwnrBVoytIlTjLMmKNVq3NrqKMLVdTyf55jYsKhGRRGHH0JbFJSKRrFpIc1NjBbmVSHS7HnOs8sSmKtaJEolt1xPOXGvGlCSR6HY91ZdtVDNW1+181lESCZ8ntTuUfa2+vskS0RMcJZLgQwDPhd/dr24RiegfmIQsGUKt4LF0B4HaRiWS3K6Hz63XIxQtkATzNpxE9FlHAgWHIuehWGmAJHe7HvPLTYCGAaGiXR3q2uGCIjz8UAuQ9PYnA3WdyQ0MM2qSwfyd3K6HTbf1FbMWKl8TSW/+lL5lpElAIqwY8c6gicrX4RKTiLaJZLB/2ySDGx1HI5UvKJ0j3zL6IOntnx3JR2gkr3zx7y0svV0PmROJ22DDpwVyiaU/96XwBXh9dlu1yMjHeoIrBv03+W5n/ykBXGhOu1ESUoyQ2RFNdUakt+shxUh0ZPXuhpxQhAQXIyRBkH/gC/fvYLgDRSaBaVKTKQyC5n1dGqitpLbrwcUIUL/qf72jOzUx9JvYrgcXIzYfXk2T0ZeHqo1B7MSnT5WLfvBwg0kkOle1fRJfjHQ2QR5NyvRwhfnZPMkI0Q+F4U+bSFCMQK2lZPOm2jZDYoP56GsnjzaR4Ao43kStPk4Cs21dYfhmE8mVh1BItlH1ipK4YgQKwx6GFO0FsY+73ctiJA8QSuXCeyA5CeoGwx0CLVqEpNMHfruIf0FbYEwGPrmhmpdMYiL8F0oQm8kGcx6k3kDnkLNwu54d5KgXVxjuIJGstkHqDfheb8F2PTsoRnqonUyHOoQKLqAJ3+uNb9fjipEvOAM9I1TlpdOU0e16dk7LVgmm8DaBx6eb8b3eziyCjYe6XjAfCnqHpNOY4e16dnwJQz8dQuUhVL8a8r3e0HY9OzZyjXfAJluItOR7vZ1x448sETAZDpWHIPWmfK+3M/JBCnGQ3TaGna9mNeZ7vaFJUbj/ZzCHHlzqQHK15nsFI3ug2WMd6B6k3p7vDQ0vIBvsMVcentiJlg1PrSMjPVPytOx7uaFixHky+AAwQLbpe5l1HqS3h/bgqEDqjfpear4Y8buU2/TZ8xNNm3IgzsUa/f9atfH155srRryLPVpPtSHfi4sRH++2PASpD/cLXYa52RzIxRr992pDvnfnSQZ/yOj/65Z872R2NgdyseYA+OVN+F5tetoKjnct8qvvP92MTSQ43kds2/C91ugebmT6+TZ8r7NDj39gkOFOIapheBh4K75XNNytshnfK9kDAtmO7xWs8xxb8r2hof6hTfnewJTj2Jjv5eb7h4Z7B2WVoRWL9w7KOvPl4aZ9Ly4PN+17g68eb9dcimzb9+7c0O/Gfe9kT/8B32vsafu+19rT5n0v2NPWfa+z+Dzf/wNj80O6J2/c5AAAAABJRU5ErkJggg==');

// The '@' character is used to indicate that follows an image data stream and not an image file name
$pdf->Image('@'.$base64_decoded);
//$pdf->Image('@'.$imgdata);
//$pdf->Image($img_file, 0, 0, 140, 180, '', '', '', false, 300, '', false, false, 0);


$nama_surat ='
<b><u style="border: 1px;">'.$judul_surat.'<u><b><br>
NOMOR: '.$nomor.'
';
$txt = '<p>'.$frasa_pendahuluan.'<p><br>
<table cellpadding="0">
    <tr>
        <td style="width: 130px;">Nama</td>
        <td>:  '.$isi_mhs["nama"].'</td>
    </tr>
    <tr>
        <td style="width: 130px;">Tempat, Tanggal Lahir</td>
        <td>:  '.$isi_mhs["tmp_lahir"].' , '.$isi_mhs["tgl_lahir"].'</td>
    </tr>     
    <tr>
         <td style="width: 130px;">NIM</td>
        <td>:  '.$isi_mhs["nim"].'</td>
    </tr>
    <tr>
       <td style="width: 130px;">Semester / Prodi</td>
       <td>:  '.$isi_mhs["smt"].' / '.$isi_mhs["prodi"].'</td>
    </tr>
     <tr>
        <td style="width: 130px;">Tahun Akademik</td>
       <td>:  '.$thn_akademik["tahun_ajaran"].'</td>
    </tr>
</table><br>
adalah mahasiswa Fakultas '.$isi_mhs["fkl"].' UIN Sunan Kalijaga Yogyakarta yang aktif atau tidak sedang cuti akademik pada '.strtolower($thn_akademik["semester_sekarang"]).' tahun akademik '.$thn_akademik["tahun_ajaran"].'.<br>
'.$isi_surat.'.
<br>
Demikian Surat Keterangan ini dibuat untuk dapat dipergunakan sebagaimana mestinya.<br>';

//$txt2 = '<span>Yogyakarta, '.$tgl_surat.'</span>';
$txt3 = '<table cellpadding="0">    
    <tr>
        <td colspan="3"></td>
        <td align="R"></td>
        <td colspan="2"><span>Yogyakarta, '.$tgl_surat.'</span><br></td>
    </tr>
    <tr>
        <td colspan="3"></td>
        <td align="R">a.n.&nbsp;&nbsp;</td>
        <td align="L" colspan="2">'.$label_psd_an.'</td>
    </tr>
    <tr>
        <td colspan="3"></td>
        <td align="L"></td>
        <td align="L"  colspan="2">'.$label_psd.' Bagian Tata Usaha,
        <br><br><br><br>
        '.$nm_pgw_psd.'
        </td>
    </tr>
</table>';
$txt4 = '<span><u>Tembusan :</u></span>
<ol style="list-style-position: inside;">
<li>Dekan (Sebagai laporan)</li>
<li>Mahasiswa bersangkutan</li>
</ol>';

// set font
$pdf->SetFont('times', '', 10);
// print a blox of text using multicell()
$pdf->WriteHTMLCell(0,0,'','',$nama_surat, 0, 1, 0, true, 'C', true);
$pdf->WriteHTMLCell(0,0,'','',$txt, 0, 1, 0, true, 'J', true);
$pdf->WriteHTMLCell(0,0,'','',$txt2, 0, 1, 0, true, 'R', true);
$pdf->WriteHTMLCell(0,0,'','',$txt3, 0, 1, 0, true, 'L', true);
//$pdf->WriteHTMLCell(0,0,'','',$txt2, 0, 1, 0, true, 'R', true);
$pdf->WriteHTMLCell(0,0,'','',$txt4, 0, 1, 0, true, 'L', true);


$style = array(
    'border' => 0,
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255)
    'module_width' => 0.1, // width of a single module in points
    'module_height' => 0.1 // height of a single module in points
);
$pdf->write2DBarcode($qrcode_otentikasi, 'QRCODE,L', 20, '', 20, 20, $style, 'N'); // QRCODE,L : QR-CODE Low error correction

// move pointer to last page
$pdf->lastPage();

// ---------------------------------------------------------
// ob_end_clean();
// $pdf->Output($pdf_name, 'I');
//Close and output PDF document
ob_clean(); //untuk menghapus output buffer

//$pdf->Output(FCPATH.'uploads/tmp_pdf_generated/surat_keterangan_masih_kuliah'.time().'.pdf', 'F');S
$pdf->Output('surat_keterangan_masih_kuliah'.time().'.pdf', 'I');


//============================================================+
// END OF FILE
//============================================================+
