<?php
//============================================================+
// File name   : v_r_tdk_men_bea.php
// Begin       : 2018-03-22
// Last Update : 2018-03-25
//
// Description : Template tcpdf untuk surat keterangan tidak sedang menerima beasiswa

// create new PDF document
$pdf = new Pdf_creator(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->set_document_properties(
    array(
        'pdf_title'=>'Surat Keterangan Tidak Sedang Menerima Beasiswa',
        'pdf_margin' => ['15','35','15', true], #LEFT, TOP, RIGHT (PAGE MARGINS)
        'pdf_template_margin' => ['6','6'], #MARGIN HEADER, MARGIN FOOTER
        'pdf_page_break' => [true,'30'] #aktif/tdk aktif, MARGIN BOTTOM (DEFAULT : isikan INITIAL)
    )
);
$pdf->setData($template);

// add a page
$resolution= array(175, 266);
$pdf->AddPage('P', $resolution);


// set some text for example
$nama_surat ='
<b><u style="border: 1px;">'.$judul_surat.'<u><b><br>
NOMOR: '.$nomor.'
';
$txt = '<p>Yang bertanda tangan dibawah ini, Dekan '.ucwords(strtolower($kop["fkl"])).' UIN Sunan Kalijaga Yogyakarta menerangkan : <p><br>
<table>
    <tr>
        <td style="width: 130px;">Nama Lengkap</td>
        <td>:  '.$isi_mhs["nama"].'</td>
    </tr>
    <tr>
        <td>Nomor Induk Mahasiswa</td>
        <td>:  '.$isi_mhs["nim"].'</td>
    </tr>
    <tr>
        <td>Program Studi / Semester</td>
        <td>:  '.$isi_mhs["prodi"].' / '.$isi_mhs["smt"].'</td>
    </tr>
    <tr>
        <td style="width: 130px;">Tempat Tanggal Lahir</td>
        <td>:  '.$isi_mhs["tmp_lahir"].' , '.$isi_mhs["tgl_lahir"].'</td>
    </tr> 
    
</table><br><br>
adalah benar-benar mahasiswa '.ucwords(strtolower($kop["fkl"])).' UIN Sunan Kalijaga Yogyakarta, mahasiswa tersebut saat ini (periode tahun akademik '.$thn_akademik["tahun_ajaran"].') tidak sedang menerima beasiswa dari pihak manapun.<br><br><br>


<b><u style="border: 1px;">Catatan : </u></b><br>
Beasiswa yang pernah diikuti :
'.$isi_surat["daftar_beasiswa_pernah_diikuti"].'<br><br>

Demikian agar menjadi maklum dan dapat dipergunakan sebagaimana mestinya.<br>';

$txt2 = 'Yogyakarta, '.$tgl_surat.'<br>
         a.n. '.$label_psd_an.'<br>
                '.$label_psd.', <br><br><br><br><br><br>
        '.$nm_pgw_psd.'<br>
';
$txt3='<p><u>Tembusan</u><br>
1. Dekan(Sebagai laporan)<br>
2. Yang bersangkutan</p>
<br>';

// set font
$pdf->SetFont('times', '', 10);
// print a blox of text using multicell()
$pdf->WriteHTMLCell(0,0,'','',$nama_surat, 0, 1, 0, true, 'C', true);
$pdf->WriteHTMLCell(0,0,'','',$txt, 0, 1, 0, true, 'L', true);
$pdf->WriteHTMLCell(0,0,'','',$txt2, 0, 1, 0, true, 'R', true);
$pdf->WriteHTMLCell(0,0,'','',$txt3, 0, 1, 0, true, 'L', true);

$style = array(
    'border' => 0,
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255)
    'module_width' => 0.1, // width of a single module in points
    'module_height' => 0.1 // height of a single module in points
);

$pdf->Ln();
// QRCODE,L : QR-CODE Low error correction
$pdf->write2DBarcode($qrcode_otentikasi, 'QRCODE,L', 18, '', 20, 20, $style, 'N');

// move pointer to last page
$pdf->lastPage();

// ---------------------------------------------------------


//Close and output PDF document
ob_clean(); //untuk menghapus output buffer
$pdf->Output('surat_tidak_sedang_menerima_Beasiswa_'.time().'.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
