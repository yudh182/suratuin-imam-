<?php
//============================================================+
// template report   : Surat Permohonan Ijin Penelitian
// Last Update : 2018-03-23


$pdf = new Pdf_creator(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->set_document_properties(
    array(
        'pdf_title'=>'Surat Ijin Penelitian',
        'pdf_margin' => ['15','35','15', true], #LEFT, TOP, RIGHT (PAGE MARGINS)
        'pdf_template_margin' => ['6','6'], #MARGIN HEADER, MARGIN FOOTER
        'pdf_page_break' => [true,'20'] #aktif/tdk aktif, MARGIN BOTTOM (DEFAULT : isikan INITIAL)
    )
);
$pdf->setData($template);

// add a page
$resolution= array(175, 266);
$pdf->AddPage('P', $resolution);

// MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)

// set some text for example
$nama_surat ='<table>
<tr>
        <td width="50px">Nomor</td>
        <td width="200px">: '.$nomor.'</td>
        <td align="R" width="250px">Yogyakarta, '.$tgl_surat.'</td>
    </tr>
    <tr>
        <td width="50px">Sifat</td>
        <td>: Penting</td>
        <td></td>
    </tr>
    <tr>
        <td  width="50px">Lamp.</td>
        <td>: 1 bendel proposal</td>
        <td></td>
    </tr>
    <tr>
        <td  width="50px">Hal</td>
        <td>: Permohonan Ijin Penelitian</td>
        
        <td></td>
    </tr></table><br><br>
Kepada:<br>
Yth. '.$tujuan["kepada"].'<br>
di '.$tujuan["alamat"].'.
<p><i>Assalamualaikum Wr.Wb</i><br><br>
Kami beritahukan bahwa untuk memenuhi penyusunan tugas akhir/skripsi yang berjudul <b>"'.$isi_surat["judul"].'"</b> diperlukan penelitian</p><br>
<p><br>
Oleh karena itu, kami mengharap kiranya Bapak/Ibu berkenan memberi izin kepada mahasiswa kami:</p><br><br><table>
    <tr>
        <td  style="width: 130px;">Nama</td>
        <td>:  '.$isi_mhs["nama"].'</td>
    </tr>
    <tr>
        <td>Nomor Induk Mahasiswa</td>
        <td>:  '.$isi_mhs["nim"].'</td>
    </tr>
    <tr>
        <td>Semester </td>
        <td>:  '.$isi_mhs["smt"].'</td>
    </tr>
    <tr>
        <td>Program Studi</td>
        <td>:  '.$isi_mhs["prodi"].'</td>
    </tr>
     <tr>
        <td>Tahun Akademik</td>
        <td>:  '.ucwords(strtolower($thn_akademik["semester_sekarang"])).', '.$thn_akademik["tahun_ajaran"].'</td>
    </tr>
    <tr>
        <td>Alamat dan Kontak</td>
        <td>:  '.$isi_mhs["alamat"].' / '.$isi_mhs["hp"].'</td>
    </tr>    
</table><br/><br/>
untuk melakukan penelitian di '.$isi_surat["tempat_penelitian"].' dengan metode penelitian <i>'.$isi_surat["metode"].'</i> yang dijadwalkan pada tanggal '.$isi_surat["tgl_mulai"].' s/d '.$isi_surat["tgl_berakhir"].'<br><br>
Sebagai bahan pertimbangan bersama ini kami lampirkan :<br>
<table>
<tr>
<td>1.Proposal Skripsi</td>
<td>3. Fotocopy Kartu Rencana Studi (KRS) </td>
</tr>
<tr>
<td>2.Fotocopy Kartu Tanda Mahasiswa (KTM)</td>
<td></td>
</tr>
</table><br>
Demikian surat keterangan ini dibuat untuk dipergunakan sebagaimana mestinya.<br><br>
<i>Wassalamualaikum Wr.Wb</i>';
$txt1 = '<table><tr>
<td>a.n  '.$label_psd_an.'<br>
           Wakil Dekan Bidang Akademik<br>
        <br><br><br>
                
        <b>'.$nm_pgw_psd.'</b></td></tr>
</table>';
$txt3 = 'Tembusan :<br>
'.$daftar_tembusan[0].'<br>';

// set font
$pdf->SetFont('times', '', 10);
// print a blox of text using WriteHTMLCell()
$pdf->WriteHTMLCell(0,0,'','',$nama_surat, 0, 1, 0, true, 'L', true);
//$pdf->WriteHTMLCell(0,0,'','',$txt, 0, 1, 0, true, 'L', true);
$pdf->WriteHTMLCell(0,0,'','',$txt1, 0, 1, 0, true, 'R', true);
//$pdf->WriteHTMLCell(0,0,'','',$txt2, 0, 1, 0, true, 'C', true);
$pdf->WriteHTMLCell(0,0,'','',$txt3, 0, 1, 0, true, 'L', true);

$style = array(
    'border' => 0,
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255)
    'module_width' => 0.1, // width of a single module in points
    'module_height' => 0.1 // height of a single module in points
);

//$pdf->Ln();
// QRCODE,L : QR-CODE Low error correction kiri, posisi dari atas-kebawah, panjang, lebar
$pdf->write2DBarcode($qrcode_otentikasi, 'QRCODE,L', 20, '', 20, 20, $style, 'N');

// move pointer to last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
ob_clean(); //untuk menghapus output buffer
$pdf->Output('surat_ijin_penelitian_'.time().'.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
