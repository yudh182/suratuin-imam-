<?php
//============================================================+
// File name   : v_r_kel_baik.php
// Begin       : 2018-03-22
// Last Update : 2018-03-25
//
// Description : Template tcpdf untuk surat keterangan berkelakuan baik

// create new PDF document
$pdf = new Pdf_creator(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->set_document_properties(
    array(
        'pdf_title'=>'Surat Keterangan Berkelakuan Baik',
        'pdf_margin' => ['15','35','15', true], #LEFT, TOP, RIGHT (PAGE MARGINS)
        'pdf_template_margin' => ['6','6'], #MARGIN HEADER, MARGIN FOOTER
        'pdf_page_break' => [true,'30'] #aktif/tdk aktif, MARGIN BOTTOM (DEFAULT : isikan INITIAL)
    )
);
$pdf->setData($template);

// add a page
$resolution= array(175, 266);
$pdf->AddPage('P', $resolution);

// set some text for example
$nama_surat ='
<b><u style="border: 1px;">'.$judul_surat.'<u><b><br>
No : '.$nomor.'
';
$txt = '<p>Berdasarkan surat pernyataan saudara mahasiswa/i di bawah ini, Dekan '.ucwords(strtolower($kop["fkl"])).' Universitas Islam Negeri Sunan Kalijaga Yogyakarta menerangkan  : <p><br>
<table>
     <tr>
        <td style="width: 130px;">Nama</td>
        <td>:  '.$isi_mhs["nama"].'</td>
    </tr>
    <tr>
        <td style="width: 130px;">Nomor Induk Mahasiswa</td>
        <td>:  '.$isi_mhs["nim"].'</td>
    </tr>
    <tr>
       <td style="width: 130px;">Semester</td>
       <td>:  '.$isi_mhs["smt"].'</td>
    </tr>
    <tr>
       <td style="width: 130px;">Progam Studi</td>
       <td>:  '.$isi_mhs["prodi"].'</td>
    </tr>
     <tr>
        <td style="width: 130px;">Tahun Akademik</td>
       <td>:  '.$thn_akademik["tahun_ajaran"].'</td>
    </tr>
</table><br><br>
adalah benar-benar mahasiswa Fakultas '.ucwords(strtolower($kop["fkl"])).' UIN Sunan Kalijaga Yogyakarta, dan sepanjang pengetahuan kami mahasiswa tersebut di atas berkelakuan baik dan belum pernah terlibat masalah Narkoba atau penyimpangan perilaku.<br><br>
Demikian surat keterangan ini dibuat untuk dipergunakan sebagaimana mestinya.
';
$txt1 = '<table>
<td align="R">Yogyakarta, '.$tgl_surat.'<br>
 a.n. Dekan<br>
                        Kepala Bagian Tata Usaha<br>
                <br><br>
                <br><br>
                



                        <b>'.$nm_pgw_psd.'</b><br></td></tr>
</table>';

$txt2 = '<table>
<td align="L"><u>Tembusan:</u><br>
1. Dekan (sebagai laporan);<br>
2. Yang bersangkutan<br>
                <br></td></tr>
</table>';

// set font
$pdf->SetFont('times', '', 10);
// print a blox of text using writeHTMLCell()
$pdf->WriteHTMLCell(0,0,'','',$nama_surat, 0, 1, 0, true, 'C', true);
$pdf->WriteHTMLCell(0,0,'','',$txt, 0, 1, 0, true, 'L', true);
$pdf->WriteHTMLCell(0,0,'','',$txt1, 0, 1, 0, true, 'C', true);
$pdf->WriteHTMLCell(0,0,'','',$txt2, 0, 1, 0, true, 'C', true);

$style = array(
    'border' => 0,
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255)
    'module_width' => 0.1, // width of a single module in points
    'module_height' => 0.1 // height of a single module in points
);

//$pdf->Ln();
// QRCODE,L : QR-CODE Low error correction
$pdf->write2DBarcode($qrcode_otentikasi, 'QRCODE,L', 18, '', 20, 20, $style, 'N');

// move pointer to last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
ob_clean(); //untuk menghapus output buffer
$pdf->Output('surat_keterangan_berkelakuan_baik'.time().'.pdf', 'I'); #setting inline  (I) agar didownload browser

//============================================================+
// END OF FILE
//============================================================+
