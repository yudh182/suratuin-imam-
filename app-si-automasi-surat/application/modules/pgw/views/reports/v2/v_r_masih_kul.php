<?php
//============================================================+
// File name   : v_r_masih_kul.php
// Begin       : 2018-03-22
// Last Update : 2018-03-25
//
// Description : Template tcpdf untuk surat keterangan masih kuliah

// create new PDF document
$pdf = new Pdf_creator(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->set_document_properties(
    array(
        'pdf_title'=>'Surat Keterangan Masih Kuliah',
        'pdf_margin' => ['15','35','15', true], #LEFT, TOP, RIGHT (PAGE MARGINS)
        'pdf_template_margin' => ['6','6'], #MARGIN HEADER, MARGIN FOOTER
        'pdf_page_break' => [true,'30'] #aktif/tdk aktif, MARGIN BOTTOM (DEFAULT : isikan INITIAL)
    )
);
$pdf->setData($template);

// add a page
$resolution= array(175, 266);
$pdf->AddPage('P', $resolution);

// The '@' character is used to indicate that follows an image data stream and not an image file name
if (isset($watermark_img)) :
    $base64_decoded = base64_decode($watermark_img);
    $pdf->Image('@'.$base64_decoded);
    //$pdf->Image('@'.$imgdata);
    //$pdf->Image($img_file, 0, 0, 140, 180, '', '', '', false, 300, '', false, false, 0);
endif;


$nama_surat ='
<b><u style="border: 1px;">'.$judul_surat.'<u><b><br>
NOMOR: '.$nomor.'
';
$txt = '<p>'.$frasa_pendahuluan.'<p><br>
<table cellpadding="0">
    <tr>
        <td style="width: 130px;">Nama</td>
        <td>:  '.$isi_mhs["nama"].'</td>
    </tr>
    <tr>
        <td style="width: 130px;">Tempat, Tanggal Lahir</td>
        <td>:  '.$isi_mhs["tmp_lahir"].' , '.$isi_mhs["tgl_lahir"].'</td>
    </tr>     
    <tr>
         <td style="width: 130px;">NIM</td>
        <td>:  '.$isi_mhs["nim"].'</td>
    </tr>
    <tr>
       <td style="width: 130px;">Semester / Prodi</td>
       <td>:  '.$isi_mhs["smt"].' / '.$isi_mhs["prodi"].'</td>
    </tr>
     <tr>
        <td style="width: 130px;">Tahun Akademik</td>
       <td>:  '.$thn_akademik["tahun_ajaran"].'</td>
    </tr>
</table><br>
adalah mahasiswa Fakultas '.$isi_mhs["fkl"].' UIN Sunan Kalijaga Yogyakarta yang aktif atau tidak sedang cuti akademik pada '.strtolower($thn_akademik["semester_sekarang"]).' tahun akademik '.$thn_akademik["tahun_ajaran"].'.<br>
'.$isi_surat.'.
<br>
Demikian Surat Keterangan ini dibuat untuk dapat dipergunakan sebagaimana mestinya.<br>';

//$txt2 = '<span>Yogyakarta, '.$tgl_surat.'</span>';
$txt3 = '<table cellpadding="0">    
    <tr>
        <td colspan="3"></td>
        <td align="R"></td>
        <td colspan="2"><span>Yogyakarta, '.$tgl_surat.'</span><br></td>
    </tr>
    <tr>
        <td colspan="3"></td>
        <td align="R">a.n.&nbsp;&nbsp;</td>
        <td align="L" colspan="2">'.$label_psd_an.'</td>
    </tr>
    <tr>
        <td colspan="3"></td>
        <td align="L"></td>
        <td align="L"  colspan="2">'.$label_psd.' Bagian Tata Usaha,
        <br><br><br><br>
        '.$nm_pgw_psd.'
        </td>
    </tr>
</table>';
$txt4 = '<span><u>Tembusan :</u></span>
<ol style="list-style-position: inside;">
<li>Dekan (Sebagai laporan)</li>
<li>Mahasiswa bersangkutan</li>
</ol>';

// set font
$pdf->SetFont('times', '', 10);
// print a blox of text using multicell()
$pdf->WriteHTMLCell(0,0,'','',$nama_surat, 0, 1, 0, true, 'C', true);
$pdf->WriteHTMLCell(0,0,'','',$txt, 0, 1, 0, true, 'J', true);
$pdf->WriteHTMLCell(0,0,'','',$txt2, 0, 1, 0, true, 'R', true);
$pdf->WriteHTMLCell(0,0,'','',$txt3, 0, 1, 0, true, 'L', true);
//$pdf->WriteHTMLCell(0,0,'','',$txt2, 0, 1, 0, true, 'R', true);
$pdf->WriteHTMLCell(0,0,'','',$txt4, 0, 1, 0, true, 'L', true);


$style = array(
    'border' => 0,
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255)
    'module_width' => 0.1, // width of a single module in points
    'module_height' => 0.1 // height of a single module in points
);
$pdf->write2DBarcode($qrcode_otentikasi, 'QRCODE,L', 20, '', 20, 20, $style, 'N'); // QRCODE,L : QR-CODE Low error correction

// move pointer to last page
$pdf->lastPage();

// ---------------------------------------------------------
// ob_end_clean();
// $pdf->Output($pdf_name, 'I');
//Close and output PDF document
ob_clean(); //untuk menghapus output buffer

//$pdf->Output(FCPATH.'uploads/tmp_pdf_generated/surat_keterangan_masih_kuliah'.time().'.pdf', 'F');S
$pdf->Output('surat_keterangan_masih_kuliah'.time().'.pdf', 'I');


//============================================================+
// END OF FILE
//============================================================+
