<?php
//============================================================+
// File name   : v_r_hbs_teori.php
// Begin       : 2018-03-22
// Last Update : 2018-03-25
//
// Description : Template tcpdf untuk surat keterangan habis teori

// create new PDF document
$pdf = new Pdf_creator(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->set_document_properties(
    array(
        'pdf_title'=>'Surat Keterangan Habis Teori',
        'pdf_margin' => ['15','35','15', true], #LEFT, TOP, RIGHT (PAGE MARGINS)
        'pdf_template_margin' => ['6','6'], #MARGIN HEADER, MARGIN FOOTER
        'pdf_page_break' => [true,'25'] #aktif/tdk aktif, MARGIN BOTTOM (DEFAULT : isikan INITIAL)
    )
);
$pdf->setData($template);

// add a page
$resolution= array(175, 266);
$pdf->AddPage('P', $resolution);

// set some text for example
$nama_surat ='
<b><u style="border: 1px;">SURAT KETERANGAN BEBAS TEORI</u><b><br>
No : '.$nomor.'
';
$txt = '<i>Assalamu`alaikum Wr.Wb.</i><p>Yang bertanda tangan di bawah ini menerangkan bahwa Mahasiswa : <p><br>
<table>
    <tr>
        <td  style="width: 130px;">Nama</td>
        <td>:  '.$nama.'</td>
    </tr>
    <tr>
        <td>NIM</td>
        <td>:  '.$nim.'</td>
    </tr>
     <tr>
        <td>Program Studi</td>
        <td>:  '.$prodi.'</td>
    </tr>
    <tr>
        <td>Semester</td>
        <td>:  '.$smt.'</td>
    </tr>
    <tr>
        <td>Fakultas</td>
        <td>:  '.$fkl.'</td>
    </tr>
     <tr>
        <td>Telp/HP</td>
        <td>:  '.$telp.'</td>
    </tr>
</table><br><br>
telah menyelesaikan semua beban mata kuliah teori, tugas PPL I, PPL II, Kerja Praktek*), dan KKN dengan nilai terendah C, IP Kumulatif '.$ip.', dan Telah memenuhi persyaratan untuk mengikuti sidang Munaqasyah.<br>
Demikian surat ini dibuat dengan sebenarnya untuk dapat dipergunakan sebagaimana mestinya.<br><br>


<i>Wassalamu`alaikum Wr.Wb</i><br>';

$txt1 = '<table>
<tr>
<td align="L"><br><br><br>
Pengecek<br>
                <br><br><br>
                



'.$pengecek.'<br>
</td><td></td>
<td align="L">Yogyakarta, '.$dd.' '.$bulan.' '.$yy.'<br>a.n  Dekan<br>
                        Kepala Bagian Tata Usaha<br>
                <br><br><br>
                



                        '.$ttd.'<br></td></tr>
</table>';


$pdf->SetFont('times', '', 10);
$pdf->WriteHTMLCell(0,0,'','',$nama_surat, 0, 1, 0, true, 'C', true);
$pdf->WriteHTMLCell(0,0,'','',$txt, 0, 1, 0, true, 'L', true);
$pdf->WriteHTMLCell(0,0,'','',$txt1, 0, 1, 0, true, 'C', true);
$pdf->WriteHTMLCell(0,0,'','',$txt2, 0, 1, 0, true, 'C', true);

$style = array(
    'border' => 0,
    'vpadding' => 'auto',
    'hpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255)
    'module_width' => 0.1, // width of a single module in points
    'module_height' => 0.1 // height of a single module in points
);

// QRCODE,L : QR-CODE Low error correction
$pdf->write2DBarcode($nomor, 'QRCODE,L', 18, '', 20, 20, $style, 'N');

// move pointer to last page
$pdf->lastPage();

// ---------------------------------------------------------
ob_end_clean();
//Close and output PDF document
ob_clean(); //untuk menghapus output buffer
$pdf->Output('surat_keterangan_bebas_teori_'.time().'.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
