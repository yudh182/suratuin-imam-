<?php

$dir_jwt_pack = APPPATH.'modules/pgw/packages/firebase-php-jwt/';
require_once $dir_jwt_pack.'BeforeValidException.php';
require_once $dir_jwt_pack.'ExpiredException.php';
require_once $dir_jwt_pack.'SignatureInvalidException.php';
require_once $dir_jwt_pack.'JWT.php';
use \Firebase\JWT\JWT;
