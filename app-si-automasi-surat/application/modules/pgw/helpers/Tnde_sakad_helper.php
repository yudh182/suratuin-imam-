<?php 

/**
* Menyeleksi array duplikat
* digunakan pada laman buat surat akademik (action_verifikasi_permohonan)
* @return array
*/
if (!function_exists('seleksi_array')) 
{ 
    function seleksi_array_duplikat($array_complete) 
    { 
       $filtered_array = array_flip($array_complete); //value jadi key, duplikat hilang
       $filtered_array = array_keys($filtered_array);
       return $filtered_array; 
    } 
} 

/**
mencari array yang mengandung key sama dengan value pencarian
*/
function cari_index_berdasarkan_key_value($value_dicari='', $array_sumber=array(), $keyname=''){
	//temukan indexnya dulu
	$posisi = array_search($value_dicari, array_column($array_sumber, $keyname));
}

function search_array_by_keyval($array, $key, $value)
{
    $results = array();

    if (is_array($array)) {
        if (isset($array[$key]) && $array[$key] == $value) {
            $results[] = $array;
        }

        foreach ($array as $subarray) {
            $results = array_merge($results, search_array_by_keyval($subarray, $key, $value));
        }
    }

    return $results;
}

function generate_tabel_syarat($data){
    $html = '<table id="tbl-cek" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th width="30px">No.</th>
                        <th>Syarat</th>
                        <th width="180px">Isi yang diharapkan</th>
                        <th width="100px">Hubungi</th>
                        <th width="50px">Status</th>
                    </tr>
                </thead>
                <tbody>';
    foreach ($data as $key => $val) {
        $no = (int)$key + 1;
        $html .= '<tr>';
        $html .= '<td >'.$no.'</td>';
        $html .= '<td>'.$val['OUT_TXT_JUDUL'].'</td>';
        $html .= '<td>'.$val['ISI_REQUIRED'].'</td>';
        $html .= '<td>'.$val['OUT_TXT_HUBUNGI'].'</td>';
        
        $html .= '<td><center><span class="badge"><i class="icon-white icon-remove"></i></span></center></td>';
        
    }        
    $html .= '</tbody></table>';
    echo $html;
}

function generate_tabel_hasil_cek($data){
    $html = '<table id="tbl-cek" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th width="30px">No.</th>
                        <th>Syarat</th>
                        <th width="180px">Isi</th>
                        <th width="100px">Hubungi</th>
                        <th width="50px">Status</th>
                    </tr>
                </thead>
                <tbody>';
    foreach ($data as $key => $val) {        
        $html .= '<tr>';
        $html .= '<td >'.$val["NO."].'</td>';
        $html .= '<td>'.$val['OUT_TXT_JUDUL'].'</td>';
        $html .= '<td>'.$val['ISI'].'</td>';
        $html .= '<td>'.$val['OUT_TXT_HUBUNGI'].'</td>';
        if ($val['STATUS'] === 'pass'){
            $html .= '<td><center><span class="badge badge-success"><i class="icon-white icon-ok"></i></span></center></td>';
        } else {
            $html .= '<td><center><span class="badge"><i class="icon-white icon-remove"></i></span></center></td>';
        }        
    }        
    $html .= '</tbody></table>';
    echo $html;
}

function generate_tabel_verifikasi_v2($data){
    $html = '<table id="tbl-cek" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th width="30px">No.</th>
                        <th>Syarat</th>
                        <th width="180px">Isi</th>
                        <th width="100px">Hubungi</th>
                        <th width="50px">Status</th>
                    </tr>
                </thead>
                <tbody>';
    foreach ($data as $key => $val) {
        $html .= '<tr>';
        $html .= '<td >'.$val["NO."].'</td>';
        $html .= '<td>'.$val['OUT_TXT_TITLE'].'</td>';
        $html .= '<td>'.$val['ISI'].'</td>';
        $html .= '<td>'.$val['OUT_TXT_HUBUNGI'].'</td>';

        if ($val['STATUS'] === 'pass'){
            $html .= '<td><center><span class="badge badge-success"><i class="icon-white icon-ok"></i></span></center></td>';
        } else {
            $html .= '<td><center><span class="badge"><i class="icon-white icon-remove"></i></span></center></td>';
        }
    }        
    $html .= '</tbody></table>';
    echo $html;
}


function generate_keterangan(){
    echo '
    <table class="table table-nama" style="border: none; margin:1% 0 3% 0;">
        <tbody>
            <tr><td colspan="3" class=""><b>Keterangan</b></td></tr>                    
            <tr><td class=""><span style="cursor:pointer;" class="badge badge-success"><i class="icon-ok icon-white"></i></span></td><td class="tdlebar">:</td><td> Syarat permohonan surat <b>SUDAH</b> terpenuhi.</td></tr>
            <tr><td class=""><span style="cursor:pointer;" class="badge badge-important"><i class="icon-remove icon-white"></i></span></td><td class="tdlebar">:</td><td><p> Syarat permohonan surat <b>BELUM</b> terpenuhi, info lebih lanjut silahkan hubungi pihak yang bersangkutan.</p></td></tr>
        </tbody>                        
    </table>';
}

function convert_kd_jenis_surat_induk($data,$to='nama'){
    switch ($data) {
        case '6':
            $hasil = 'SURAT NASKAH DINAS (UMUM)';
            break;
        case '11':
            $hasil = 'SURAT KETERANGAN';
            break;
        
        default:
            # code...
            break;
    }
    return $hasil;
}

/**
* Filter array dengan seleksi keys tertentu 
 * ditambahkan 29 januari 2018
 */
function seleksi_array_by_keys($array_sumber, $array_keys_terpilih)
{
    return array_intersect_key($array_sumber, array_flip($array_keys_terpilih));
}


function tanggal_indo($tanggal, $from_format='Y-m-d', $cetak_hari = false)
{
    $hari = array ( 1 =>    'Senin',
                'Selasa',
                'Rabu',
                'Kamis',
                'Jumat',
                'Sabtu',
                'Minggu'
            );
            
    $bulan = array (
        1 =>   'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember'
            );
    if ($from_format == 'Y-m-d'){
        $split    = explode('-', $tanggal);
        $tgl_indo = $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];    
    } elseif ($from_format == 'd/m/Y') {
        $split    = explode('/', $tanggal);
        $tgl_indo = $split[0] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[2];    
    }
    
    
    if ($cetak_hari) {
        $num = date('N', strtotime($tanggal));
        return $hari[$num] . ', ' . $tgl_indo;
    }
    return $tgl_indo;
}

?>