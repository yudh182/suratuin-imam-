<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logic_Verifikasi extends CI_Model
{
	public function __construct()
	{
		parent::__construct();            
 		$this->load->model('Repo_SIA');
		$this->load->model('Repo_Surat');	 		
	}

	public function get_persyaratan_by_kd($kd='')
	{
		$parameter = array('api_kode' => 14001, 'api_subkode' => 1, 'api_search' => array($kd));
        $api_get = $this->apiconn->api_tnde_sakad('tnde_surat_akademik/get_persyaratan_verifikasi','json','POST', $parameter)   ;
        return $api_get;
	}

	public function get_persyaratan_by_path($path='')
	{

		$parameter = array('api_kode' => 14001, 'api_subkode' => 2, 'api_search' => array($path));
        $api_get = $this->apiconn->api_tnde_sakad('tnde_surat_akademik/get_persyaratan_verifikasi','json','POST', $parameter)   ;
        return $api_get;
	}

	/**
	* Menentukan permohonan surat lanjut atau berhenti (verifikasi / validasi)
	* data balikan digunakan untuk di-mapping ke pengisian form
	*/
	public function pengecekan_persyaratan($data_persyaratan=array(), $kd_jenis_sakad=null, $nim=null)
	{
		# 1. CEK VALUES KOLOM ASAL_API PADA ARRAY PERSYARATAN
		$asal_api_unq = seleksi_array_duplikat( array_column($data_persyaratan, 'ASAL_API') );

		return $asal_api_unq;

		# 2. LOOPING KE WEBSERVICE TERKAIT (Pencarian data mhs)    
	    /*$datas_from_webservice = [];
	    foreach ($asal_api_unq as $req) {
        	$datas_from_webservice[$req] = $this->remote_service_untuk_sakad($req, array('NIM'=>$temp_nim));
        }
		*/
	}

	/**
	* Mengarahkan panggilan API Webservice sesuai kode asal api
	* PENGGUNAAN :
	* - verifikasi permohonan buat surat akademik
	* - automasi form buat surat akademik
	*/
	public function remote_service_untuk_sakad($kd_asal_api, $params=array()){
		$arr_hasil = ['error'=>'gagal mendapatkan resource dari sumber data'];
		switch ($kd_asal_api) {
			case 'sia_mhs_dpm': #PROFIL MAHASISWA DPM
				$parameter	= array('api_kode'=>26000, 'api_subkode' => 12, 'api_search' => array($params['NIM'])); 
				$arr_hasil	= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);
				//$arr_hasil = $arr_hasil[0];
				/*$arr_hasil['meta'] = [
					'kd_asal_api' => 'SIA_MHS',
					'title'	=> 'Profil Mahasiswa Lengkap'];*/
				//$arr_hasil['meta']	= 'SIA_MHS';
				//$arr_hasil = $this->dummydata->dummy_get_mhs_sia( (int) $params['NIM'] )[0];	
					
				break;
			case 'sia_mhs_kumu': #Profil Mahasiswa + IP Kumulatif
				break;
			case 'sia_nilai': #data nilai MAHASISWA
				# MENUNGGU INFO DARI PTIPD
				/*$arr_hasil['meta'] = [
					'kd_asal_api' => 'SIA_NILAI',
					'title' => 'Rangkuman Nilai Mahasiswa'
				];*/
				//$arr_hasil['meta'] = 'SIA_NILAI';
				$arr_hasil = [
					'IPK'	=> '2.99',
					'JUMLAH_NILAI_BAWAH_C' => 3,
					'JUMLAH_NILAI_E' => 0
				];

				break;
			case 'sia_makul': #data makul semester ini
				
				break;
			case 'tnde_riwayat': #riwayat persuratan mhs
				# MENUNGGU BUTUH
				break;
			
			default:
				$arr_hasil = ['errors'=> array(
					'kode_error' => '400', #kode asal api tidak dikenali
					'pesan_error'=> 'Insufficient data'
				)];
				break;
		}

		return $arr_hasil;
	}

}