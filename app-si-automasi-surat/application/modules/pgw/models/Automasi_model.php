<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Automasi_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct(); 
        #note : $apiconn dikenali lewat controller yang memuat model ini
    }

    public function get_config_automasi($kd_jenis_sakad=null, $unit_id='')
    {
        $parameter = array(
            'api_kode' => 15003, 'api_subkode' => 1, 
            'api_search' => array(
                'CONFIG_AUTOMASI_SAKAD',
                array('KD_JENIS_SAKAD'=>(int)$kd_jenis_sakad,'UNIT_ID'=>$unit_id),
                'ID ASC'
            )
        );      
        $api_get = $this->apiconn->api_tnde_sakad('surat_general/get_data', 'json', 'POST', $parameter);

        return $api_get;
    }   

    public function simpan_sesi_automasi_buat($data=array())
    {           
        $parameter = array('api_kode' => 13006, 'api_subkode' => 3, 'api_search' => array($data));
        $api_insert = $this->apiconn->api_tnde_sakad('Tnde_surat_akademik/insert_entry_sesi_sakad','json','POST', $parameter) ;

        return $api_insert;
    }


    public function simpan_sesi_automasi_buat_v2($data=array())
    {           
        $parameter = array('api_kode' => 14006, 'api_subkode' => 3, 'api_search' => array($data));
        $api_insert = $this->apiconn->api_tnde_sakad('Tnde_surat_akademik/insert_entry_sesi_sakad_v2','json','POST', $parameter) ;

        return $api_insert; // array
    }

    public function perbarui_sesi_automasi_buat($id_surat_keluar='', $data=array())
    {           
        $parameter = array('api_kode' => 14006, 'api_subkode' => 1, 'api_search' => array($id_surat_keluar, $data));
        $api_update = $this->apiconn->api_tnde_sakad('Tnde_surat_akademik/update_entry_sesi_sakad','json','POST', $parameter) ;

        return $api_update; // array
    }

    public function sesi_by_id($id_sesi=null)
    {
        $parameter = array('api_kode' => 14006, 'api_subkode' => 1, 'api_search' => array($id_sesi));
        $api_get = $this->apiconn->api_tnde_sakad('Tnde_surat_akademik/get_sesi_sakad','json','POST', $parameter) ;

        return $api_get;
    }

    public function sesi_by_kd_user($kd_user=null) //NIM atau NIP
    {
        $parameter = array('api_kode' => 14006, 'api_subkode' => 2, 'api_search' => array($kd_user));
        $api_get = $this->apiconn->api_tnde_sakad('Tnde_surat_akademik/get_sesi_sakad','json','POST', $parameter) ;

        return $api_get;
    }

    public function hapus_sesi_automasi_buat($data=null) // By ID_SESI
    {
        $parameter = array('api_kode' => 14007, 'api_subkode' => 1, 'api_search' => array($data));
        $api_delete = $this->apiconn->api_tnde_sakad('Tnde_surat_akademik/delete_sesi_sakad','json','POST', $parameter) ;

        return $api_delete;   
    }
    
    /**
    * Token Keaslian akan digunakan untuk proses pembuktian keaslian surat saat pengajuan dan menyerahkan ke instansi tujuan
     * terinspirasi dari http://www.anillabs.com/2015/09/generate-token-key-in-the-registration-of-codeigniter-application/
    */
    public function generateTokenKeaslian($pos_token='cek_keaslian', $user_id=null, $id_surat_keluar=null, $keterangan=null){
        $static_str='AL';
        $currenttimeseconds = date("Ymd_His");
        $token_id=$static_str.$user_id.$currenttimeseconds;
        $data = array(
         'TOKEN_KEASLIAN' => md5($token_id),
         'POS_TOKEN' => $pos_token, 
         'KD_USER' => $user_id, //NIM atau NIP
         'ID_SURAT_KELUAR' => $id_surat_keluar,
         'KETERANGAN' => $keterangan,
         );        
        $parameter = array('api_kode' => 15001, 'api_subkode' => 1, 'api_search' => array('D_TIKET_ADMINISTRASI',$data));        
        $api_insert = $this->apiconn->api_tnde_sakad('surat_general/insert_data','json','POST', $parameter)   ;

        if ($api_insert == TRUE) {
            return md5($token_id);
        } elseif ($api_insert = FALSE) {
            return array('status'=>false, 'error'=>'gagal menambahkan data');
        } else {
            return array('status'=>false,'error'=>'internal error in webservice');
        }        
     }


    public function generateQRCodeKeaslian($user_id,$pos_tiket='pengajuan')
    {
        $str_token = $this->generateTokenKeaslian($user_id, $pos_tiket);
        $static_str = base_url('cek_keaslian_surat/');
        $qrcode = $static_str.$str_token;

        return $qrcode;
    }

    public function updateTiket()
    {

    }

    /**
    * Simpan Detail Surat Keluar di Automasi (Versi 1)
     * entitas : HEADER, DETAIL, TIKET_ADMINISTRASI
     * note : sementara penyimpanan melalui API Endpoint masing-masing, nanti diubah ke satu endpoint dengan transcactions
    */
    public function insert_surat_keluar_automasi($data=null)
    {
        if (!empty($data) || $data !== null){
            #1. insert header_surat_keluar
            $parameter = array('api_kode' => 14003, 'api_subkode' => 1, 'api_search' => array($data));
            $api_insert_hsk = $this->apiconn->api_tnde_sakad('Tnde_surat_akademik/insert_header_surat_keluar','json','POST', $parameter) ;            

            #2. insert detail


            #3. insert tiket_administrasi (generate token keaslian untuk qrcode)
            $tiket_adm = $this->generateTokenKeaslian($pos_token='cek_keaslian', $user_id=null, $id_surat_keluar=null, $keterangan=null);

            #4. jika ketiga insert berhasil, hapus data entry sesi (sementara juga lewat API)
            $api_del = $this->hapus_sesi_automasi_buat($data['ID_SESI']); //by ID_SESI

        }
        return (bool)FALSE;
    }

    public function insert_header_surat_keluar($data=null) #ganti ke protected
    {
        if (!empty($data)){
            $parameter = array('api_kode' => 14003, 'api_subkode' => 1, 'api_search' => array($data));
            $api_insert_hsk = $this->apiconn->api_tnde_sakad('Tnde_surat_akademik/insert_header_surat_keluar','json','POST', $parameter) ;

            return $api_insert_hsk;
        }
        return (bool)FALSE; 
    }

    /** DEPRECATED
    * Simpan Detail Surat (Automasi) setelah API INSERT SURAT KELUAR bernilai TRUE(berhasil)
    * Terdapat beberapa tabel, masing-masing mewakili grup. meliputi: KETERANGAN AKADEMIK, PERMOHONAN IJIN
    */
    public function insert_detail_sakad($kd_grup=null, $data=null) #ganti ke protected
    {
        if (!empty($data)){
            $kd_grup = (int)$kd_grup;
            switch ($kd_grup) {
                case 1: #Keterangan fakultas
                    $parameter = array('api_kode' => 14003, 'api_subkode' => 1, 'api_search' => array($data));
                    $api_insert = $this->apiconn->api_tnde_sakad('Tnde_surat_akademik/insert_detail_sakad','json','POST', $parameter) ;
                    break;
                case 2: #permohonan ijin
                    $parameter = array('api_kode' => 14003, 'api_subkode' => 2, 'api_search' => array($data));
                    $api_insert = $this->apiconn->api_tnde_sakad('Tnde_surat_akademik/insert_detail_sakad','json','POST', $parameter);
                    break;
                
                default:
                    $api_insert = ['error' => ['error_message'=>'nilai parameter tidak dikenali oleh sistem automasi']];
                    break;
            }

            return $api_insert;
        }
        return FALSE; 
    }

    /**
    * Get Surat keluar (Data Umum , Penerima) dari SISURAT
    */
    public function get_surat_keluar($id_surat_keluar)
    {
        //parameter pencarian : id_surat_keluar
        #SISURAT SURAT KELUAR
        $data['umum'] = $this->apiconn->api_tnde('tnde_surat_keluar/detail_surat_keluar', 'json', 'POST', array('api_kode' => 90001, 'api_subkode' => 2, 'api_search' => array($id_surat_keluar)));
        $kepada = $this->apiconn->api_tnde('tnde_surat_keluar/get_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($id_surat_keluar, 'PS')));
        $data['penerima']['PS'] = $kepada;
        $ts = $this->apiconn->api_tnde('tnde_surat_keluar/get_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($id_surat_keluar, 'TS')));
        $data['penerima']['TS'] = $ts;
        
        return $data;
    }

    public function get_detail_skr_automasi($id_surat_keluar)
    {
        $parameter = array('api_kode' => 14001, 'api_subkode' => 1, 'api_search' => array($id_surat_keluar));
        $api_get = $this->apiconn->api_tnde_sakad('Tnde_surat_akademik/get_detail_skr_automasi','json','POST', $parameter) ;
        return $api_get;
    }

    /**
    * 1 (Satu) API Endpoint untuk simpan detail surat keluar mahasiswa (di Webservice Automasi)
     * entitas : HEADER, DETAIL, TIKET_ADMINISTRASI
     * note 1 : query transaction untuk menyimpan HEADER + DETAIL, dimana tabel DETAIL yang terisi sesuai grup jenis sakad-nya
     * note 2 : generate token yg bersarang pada tabel D_TIKET_ADMINISTRASI di versi 2 sekarang dilakukan di Webservice bukan Web App
     */
    public function insert_surat_keluar_automasi_v2($data=null)
    { 
        $parameter = array('api_kode' => 14004, 'api_subkode' => 1, 'api_search' => array($data));
        $api_insert_trans = $this->apiconn->api_tnde_sakad('Tnde_surat_akademik/insert_detail_skr_automasi','json','POST', $parameter) ; 
        
        if ($api_insert_trans == FALSE){
            
        }
    }

    /**
    *  Pastikan surat yang dihapus , adalah surat ujicoba dan kolom KD_SISTEM = AUTOMASI
    */
    public function delete_surat_keluar_automasi($id_surat_keluar=null, $mode='full')
    {
        //$id_surat_keluar  =   $this->input->post('id_surat_keluar');
        if ($this->session->userdata('username') == '11650021' || $this->session->userdata('username') == '15650051'){
            $parameter          = array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('MD_SURAT_KELUAR2', 'ID_SURAT_KELUAR', $id_surat_keluar));
            $cek_skr                    = $this->apiconn->api_tnde('tnde_general/get_data', 'json', 'POST', $parameter);
            if(ISSET($cek_skr['ID_SURAT_KELUAR'])){
                if ($mode !== 'full_as'){ #all sistem
                    if ($cek_skr['KD_SISTEM'] !== 'AUTOMASI')
                        exit("data yang tidak diciptakan dari sistem automasi tidak dapat dimanipulasi!");
                }
                
                //exit(print_r($cek_skr));
                #1. CEK STATUS SIMPAN BERNILAI 1
                if($cek_skr['KD_STATUS_SIMPAN'] == 1){
                    #2.1 HAPUS DULU DISTRIBUSI TUJUAN/PENERIMA SURAT
                    $parameter              = array('api_kode' => 1001, 'api_subkode' => 3, 'api_search' => array('D_PENERIMA_SK', 'ID_SURAT_KELUAR', $id_surat_keluar));
                    $cek_penerima_skr   =   $this->apiconn->api_tnde('tnde_general/get_data', 'json', 'POST', $parameter);
                    if(count($cek_penerima_skr) > 0){
                        $del_ps['ID_SURAT_KELUAR']  =   $id_surat_keluar;
                        $parameter              = array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('D_PENERIMA_SK', $del_ps));
                        $del_penerima_skr   =   $this->apiconn->api_tnde('tnde_general/delete_data', 'json', 'POST', $parameter);
                        if($del_penerima_skr == FALSE){
                            $data['errors'][]   = "Gagal membatalkan pengiriman surat.";
                        }
                    }
                    #2.2 set act_delete TRUE / FALSE setelah hapus distribusi
                    if(ISSET($data['errors'])){
                        $act_delete =   FALSE;
                    }else{
                        $act_delete = TRUE;
                    }

                    #3 HAPUS ENTRY UMUM         
                    if($act_delete == TRUE){
                        $d_surat['ID_SURAT_KELUAR'] =   $id_surat_keluar;
                        $parameter  = array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('MD_SURAT_KELUAR2', $d_surat));
                        $del_surat      =   $this->apiconn->api_tnde('tnde_general/delete_data', 'json', 'POST', $parameter);
                        if($del_surat == TRUE){
                            $data['success'][]  =   "Berhasil menghapus surat.";
                        }else{
                            $data['errors'][]   = "Gagal menghapus surat.";
                        }
                    }


                    #4. HAPUS DATA-DATA DI SI/APLIKASI AUTOMASI (sekali request)
                    if ($mode !== 'partial_sisurat') #kalau mode partial sisurat, data automasi tidak ikut dihapus
                    {
                        if($act_delete == TRUE){
                            $del_detail = $this->delete_detail_surat_keluar_automasi($id_surat_keluar);

                            if($del_detail == TRUE){
                                $data['success'][]  =   "Berhasil menghapus detail surat mahasiswa (automasi).";
                            }else{
                                $data['errors'][]   = "Gagal menghapus detail surat mahasiswa (automasi).";
                            }
                        }    
                    }
                    
                } else {
                    $data['errors'][] = "Surat yang telah memiliki nomor, tidak dapat dihapus.";
                }       

                /*
                if(in_array($cek_sk['KD_JENIS_SURAT'], $srt_tanpa_no)){
                    $act_delete             = TRUE;
                    $parameter              = array('api_kode' => 1001, 'api_subkode' => 3, 'api_search' => array('D_PENERIMA_SK', 'ID_SURAT_KELUAR', $id_surat_keluar));
                    $cek_penerima_sk    =   $this->mdl_tnde->get_api('tnde_general/get_data', 'json', 'POST', $parameter);
                    if(count($cek_penerima_sk) > 0){
                        foreach($cek_penerima_sk as $val){
                            if($val['ID_STATUS_SM'] > 1){
                                $act_delete = FALSE;
                            }
                        }
                        if($act_delete == TRUE){
                            $del_ps['ID_SURAT_KELUAR']  =   $id_surat_keluar;
                            $parameter              = array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('D_PENERIMA_SK', $del_ps));
                            $del_penerima_sk    =   $this->mdl_tnde->get_api('tnde_general/delete_data', 'json', 'POST', $parameter);
                            if($del_penerima_sk == FALSE){
                                $data['errors'][]   = "Gagal membatalkan pengiriman surat. 0";
                                $act_delete =   FALSE;
                            }
                        }else{
                            $data['errors'][] = "Surat yang telah dibaca pegawai terkait, tidak dapat dihapus.";
                        }
                    }
                }else{
                    $data['errors'][] = "Surat yang telah memiliki nomor, tidak dapat dihapus.";
                }*/
            } else {
                $data['errors'][]   = "Data tidak ditemukan pada basisdata surat";   
            }
            
        } else {
            $data['errors'][]   = "Akses Ditolak";  
        }

        #final response
        if(!empty($data['errors'])){
            $this->session->set_flashdata('errors', $data['errors']);            
            //echo print_r($data['errors']);
        }
        if(!empty($data['success'])){
            $this->session->set_flashdata('success', $data['success']);
            //echo print_r($data['success']);
        }
        return $data;
    }

    /**
    * API Delete Detail SKR (Surat Keluar)
     * entitas yang dihapus : HEADER_SURAT_KELUAR, DETAIL_SAKAD. SIGNATURE belum include        
     * @return boolean
     */ 
    protected function delete_detail_surat_keluar_automasi($id_surat_keluar=null){
        $parameter              = array('api_kode' => 14007, 'api_subkode' => 1, 'api_search' => array($id_surat_keluar));
        $api_del   =   $this->apiconn->api_tnde_sakad('Tnde_surat_akademik/delete_detail_surat_automasi', 'json', 'POST', $parameter);
        return $api_del;
    }

    public function delete_detail_skr_automasi($id_surat_keluar=null){
        $parameter              = array('api_kode' => 14007, 'api_subkode' => 1, 'api_search' => array($id_surat_keluar));
        $api_del   =   $this->apiconn->api_tnde_sakad('Tnde_surat_akademik/delete_detail_surat_automasi', 'json', 'POST', $parameter);
        return $api_del;
    }


    /**
    * Klik Tombol Terima Pengajuan setelah scan qrcode mengarahkan ke laman cek keaslian surat
    * pegawai harus sudah sign-in saat melakukan scan
    */
    public function terimaPengajuan()
    {
        //update status_simpan_surat

    }

    public function get_riwayat($nim=null)
    {
        $parameter = array(
            'api_kode' => 15003, 'api_subkode' => 1, 
            'api_search' => array(
                'D_HEADER_SURAT_KELUAR',
                array('NIM'=>$nim),
                'WAKTU_SIMPAN ASC'
            )
        );   
        $api_get = $this->apiconn->api_tnde_sakad('surat_general/get_data', 'json', 'POST', $parameter);

        return $api_get;
    }
}