<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Repo_Beasiswa extends CI_Model
{
	public function __construct()
	{
		parent::__construct();        
	}

	public function get_beasiswa($nim)
	{
		$parameter  = array('api_kode' => 1000, 'api_subkode' => 3, 'api_search' => array($nim));
        $api_get         = $this->apiconn->api_beasiswa('sia_beasiswa_mhs/get_status_mhs', 'json', 'POST', $parameter);
        return $api_get;
	}

	public function get_beasiswa_diikuti($nim)
	{		
		$parameter  = array('api_kode' => 1000, 'api_subkode' => 3, 'api_search' => array($nim));
        $api_get         = $this->apiconn->api_beasiswa('sia_beasiswa_mhs/get_status_mhs', 'json', 'POST', $parameter);
        
        $equal = 'A'; 
		$result = array_filter($api_get, function ($item) use ($equal) {
		    /*if (stripos($item['name'], $like) !== false) {
		        return true;
		    }*/
		    if ($item['STATUS'] === $equal){
		    	return true;
		    }
		    return false;
		});

        return $result;
	}

	public function get_riwayat_beasiswa($nim)
	{}
	
}