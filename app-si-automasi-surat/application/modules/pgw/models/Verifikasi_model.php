<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Verifikasi_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
        //$this->load->library('common/Curl'); #diganti pakai autoload per module (sejak 20 feb 2018)
 		//$this->load->model('Mdl_tnde','apiconn'); #Mdl_tnde alias apiconn dimuatkan oleh controller atau autoload bukan model sendiri-sendiri.
 		
 		$this->load->model('common/Api_sia');//$this->load->model('Repo_Beasiswa');
 		//$this->load->model('Api_sia');
		//$this->load->model('Repo_Surat');
	}

	public function get_persyaratan_by_kd($kd='')
	{
		$parameter = array('api_kode' => 14001, 'api_subkode' => 1, 'api_search' => array($kd));
        $api_get = $this->apiconn->api_tnde_sakad('tnde_surat_akademik/get_persyaratan_verifikasi','json','POST', $parameter)   ;
        return $api_get;
	}

	public function get_persyaratan_by_path($path='')
	{

		$parameter = array('api_kode' => 14001, 'api_subkode' => 2, 'api_search' => array($path));
        $api_get = $this->apiconn->api_tnde_sakad('tnde_surat_akademik/get_persyaratan_verifikasi','json','POST', $parameter)   ;
        return $api_get;
	}

	
	public function verifikasi_persyaratan($jenis_sakad=array(), $nim=null) #void
	{
		$data = $this->prepare_isi_surat($jenis_sakad, $nim);		

		#2. GET Persyaratan Jenis Surat
		$path_jenis_sakad = $jenis_sakad['URLPATH'];
		$data['persyaratan'] = $this->get_persyaratan_by_path($path_jenis_sakad);
		$data['hasil_cek_mhs'] = null; #default dulu nanti diisikan

		#4. MATCHING Persyaratan x Data Mhs
		$ceksyarat_errors = 0;
        $tmp_final = array( 'num_errors'=>0, 'output'=>array() );
        $tot_item_persyaratan = count($data['persyaratan']['data']);

        # 4.1 Loop tiap item persyaratan, bandingkan expected value dg value data mahasiswa
        foreach ($data['persyaratan']['data'] as $key => $val) {
        	$tmp_final['output'][$key]['NO.'] = $key + 1;
        	$tmp_final['output'][$key]['OUT_TXT_JUDUL'] = $val["OUT_TXT_JUDUL"];
        	$tmp_final['output'][$key]['OUT_TXT_HUBUNGI'] = $val["OUT_TXT_HUBUNGI"];

        	if ($val["TIPE_OPERATOR"] == '==='){ #NILAI STRING HARUS SAMA
        		$field_mhs = $data[$val["ASAL_API"]][$val["FIELD"]];
        		$nilai_pembanding = $val["ISI_REQUIRED"];

        		if ( $field_mhs === $nilai_pembanding){
        			$tmp_final['output'][$key]['ISI'] = $field_mhs;
        			$tmp_final['output'][$key]['STATUS'] = 'pass';
        		} else {
        			$tmp_final['output'][$key]['ISI'] = $field_mhs;
        			$tmp_final['output'][$key]['STATUS'] = 'fail';
        			$ceksyarat_errors++;
        		}
        	}

        	if ($val["TIPE_OPERATOR"] == 'compare_float'){

        	}

        	if($val["TIPE_OPERATOR"] == 'custom')#MULTI CHECK
        	{
        		// $key_field_syarat = explode('#', $val['FIELD']);
        		// $klist = array_flip($key_field_syarat);
        		// $key_dipilih = array_intersect_key(
        		// 	array_replace(
        		// 		$klist,
        		// 		$data[$val['ASAL_API']]
        		// 	)
        		// 	, $klist
        		// );        		
        		//exit(print_r($key_dipilih));
        		//$checker = explode('#', $key_field_syarat);
        		$key_field_syarat = explode('#', $val['FIELD']);

        		if (!empty($data[$val['ASAL_API']]) && is_array($data[$val['ASAL_API']])){
        			$key_dipilih = array_intersect_key($data[$val['ASAL_API']], array_flip($key_field_syarat));

	        		$sub_error = 0;
	        		$txtstatus = 'Hasil : <br><ol>';
	        		foreach ($key_dipilih as $subfield) {
	        			if (!empty($subfield)){
	        				$txtstatus .= '<li>'.$subfield.'</li>';
	        			} else {
	        				$txtstatus .= '<li>belum diisi</li>';
	        				$sub_error++;
	        			}
	        		}
	        		$txtstatus .= '</ol>';

	        		if ($sub_error > 1){
	        			$ceksyarat_errors++;
	        			$txtstatus .= '<br><p><i>Segera lengkapi DPM di SIA<i><p>';
	        			$tmp_final['output'][$key]['STATUS'] = 'fail';
	        		} else {
	        			$tmp_final['output'][$key]['STATUS'] = 'pass';
	        		}
	        		$tmp_final['output'][$key]['ISI'] = $txtstatus;	
        		} else {
        			$tmp_final['output'][$key]['STATUS'] = 'fail';
					$tmp_final['output'][$key]['ISI'] = 'Belum memiliki nilai munaqosyah dan lulus yudisium';	
        		}
        		
        	}

        	if ($val["TIPE_OPERATOR"] == 'regex_string'){
        		$raw_makul = $data[$val['ASAL_API']];
        		//exit(print_r($raw_makul));

        		if ($val['FIELD'] == 'NM_MK'){
        			$kol_nama_makul = array_column($data[$val['ASAL_API']], 'NM_MK');
	        		$hasil_regex = ['count'=>0];
	        		$pattern_dicari = $val['ISI_REQUIRED'];
	        		foreach ($kol_nama_makul as $ky => $vl) {
						$is_found_yet = preg_match("/".$pattern_dicari."/i", $vl, $result);
						//$hasil_regex['result'][] = $result; //tampung pregmatch ke array hasil regex
						if (!empty($result)){ //jika pattern ditemukan selama loop ini
							$hasil_regex['count']++;
							$hasil_regex['result'][]['detail'] = $raw_makul[$ky];
						}
	        		}
	        		if ($hasil_regex['count'] > 0){
	        			//exit(print_r($hasil_regex['result']));
	        			if (!empty($hasil_regex['result'])){

	        				if (isset($hasil_regex['result'][0])){
	        					$rgxdet = $hasil_regex['result'][0]['detail'];
	        				}

	        			}

	        			$tmp_final['output'][$key]['ISI'] = $rgxdet["NM_MK"].' ('.$rgxdet["KD_MK"].')'.' - diampu oleh '.$rgxdet["NM_DOSEN_F"];
	        			$tmp_final['output'][$key]['STATUS'] = 'pass';
	        			$data['tmp_isi_detail']['MATA_KULIAH_TERKAIT'] = $rgxdet["KD_MK"].'#'.$rgxdet["NM_MK"];

	        		} else {
	        			$tmp_final['output'][$key]['ISI'] = $val['OUT_TXT_KET'];
	        			$tmp_final['output'][$key]['STATUS'] = 'fail';
	        			$ceksyarat_errors++;
	        		}
        		}
        	}

        	if ($val["TIPE_OPERATOR"] == 'map_string'){#map setiap item array untuk dibandingkan dengan operan kedua (isi_required)
        		if (!empty($data[$val['ASAL_API']])) { //cek apakah array berisi atau kosong
        			/*$key = isset($val["FIELD"]) ? $val["FIELD"] : null;
        			$isi_required = $val['ISI_REQUIRED'];
        			*/
        			$equal = 'A';
					$beasiswa_aktif = array_filter($data[$val['ASAL_API']], function ($item) use ($equal) {
					    if ($item['STATUS'] === $equal){
					    	return true;
					    }
					    return false;
					});


					//jika ditemukan beasiswa aktif = tidak lolos verifikasi,
					//jika yg ditemukan beasiswa tdk aktif = lolos verifikasi dan set nilai2 ke array $beasiswa_pernah_diikuti
					if (!empty($beasiswa_aktif)){
						$tmp_final['output'][$key]['ISI'] = $beasiswa_aktif[0]["NAMA_BEASISWA"];
						$tmp_final['output'][$key]['STATUS'] = 'fail';
						$ceksyarat_errors++;
					} else {
						$tmp_isi_detail['BEASISWA_PERNAH_DIIKUTI'] = '';
						$tmp_final['output'][$key]['ISI'] = 'beasiswa lalu';
						$tmp_final['output'][$key]['STATUS'] = 'pass';
					}

        		} else { //tidak memiliki riwayat beasiswa apapun
        			$tmp_final['output'][$key]['ISI'] = 'nihil';
        			$tmp_final['output'][$key]['STATUS'] = 'pass';
        		}

        	}


        	if ($val["TIPE_OPERATOR"] == 'custom_function'){#lemparkan pemrosesan ke custom function
        		#1. pilih fungsi (ex: pengecekan_bebas_teori)
	    		$fungsi_dipilih = isset($val['ASAL_API']) ? $val['ASAL_API'] : null;	    		
	    		#2. set data parameter
	    		$paramdata_cf = $data['tmp_verified_mhs'];	    		


	    		#3. jalankan fungsi (kembalian dalam array)	    			    		
	    		$fungsi_berjalan = call_user_func_array( array($this, $fungsi_dipilih) , [$paramdata_cf]);

	    		#4. cek return value fungsi diatas
	    		if (!empty($fungsi_berjalan)){	    			
	    			if ( $fungsi_berjalan['num_errors'] > 0){
	        			$tmp_final['output'][$key]['ISI'] = $fungsi_berjalan['ISI'];
	        			$tmp_final['output'][$key]['STATUS'] = 'fail';

	        			$ceksyarat_errors++;
	        		} else {
	        			$tmp_final['output'][$key]['ISI'] = $fungsi_berjalan['ISI'];
	        			$tmp_final['output'][$key]['STATUS'] = 'pass';    

	        			//exit(var_dump($data));
	        			if (!empty($fungsi_berjalan['DATASET']))
	        				$data['tmp_isi_detail'] = array_merge( $data['tmp_isi_detail'], $fungsi_berjalan['DATASET'] );

	        		}
	    		}

        	}
        }
        $tmp_final['num_errors'] = $ceksyarat_errors;
        $data['hasil_cek_mhs'] = $tmp_final;

		return $data;
	}

	public function prepare_isi_surat($jenis_sakad=array(), $nim=null)
	{
		$this->load->helper('pgw/Tnde_converter_to_simpeg');

		#1. GRAB INPUTAN USER
		$path_jenis_sakad = $jenis_sakad['URLPATH'];
		$kd_jenis_sakad = (int)$jenis_sakad['KD_JENIS_SAKAD'];

		$data['tmp_isi_detail'] = [
			'KD_JENIS_SAKAD' => $kd_jenis_sakad,
			'KD_JENIS_SURAT' => (int)$jenis_sakad['KD_JENIS_SURAT_INDUK'],
			'PATH_JENIS_SAKAD' => $path_jenis_sakad,
			'GRUP_JENIS_SAKAD' => (int)$jenis_sakad['GRUP_JENIS_SAKAD'],
			'NM_JENIS_SAKAD' => $jenis_sakad['NM_JENIS_SAKAD']
		];

		#3. AKSES KE API DATA MHS sesuai jenis surat masing2
		switch ($path_jenis_sakad) {
			case 'masih_kul':
				$parameter	= array('api_kode'=>26000, 'api_subkode' => 10, 'api_search' => array($nim));
				$api_get_kum	= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);

				#3.1 CEK KE API SIA DPM
				$parameter	= array('api_kode'=>26000, 'api_subkode' => 12, 'api_search' => array($nim));
				$api_get_dpm	= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);
								
				#3.2COCOKKAN NILAI KOLOM-KOLOM DENGAN KRITERIA PERSYARATAN S.KET.MASIH KULIAH				
				$data['sia_mhs_kum'] = (!empty($api_get_kum) ? $api_get_kum[0] : null);
				$data['sia_mhs_dpm'] = (!empty($api_get_dpm) ? $api_get_dpm[0] : null);
				$data['tmp_verified_mhs'] = [
					'UNIT_ID' => convert_kdfak_ke_unitid($api_get_kum[0]['KD_FAK']), #unit_id fakultas mhs
					'KD_PRODI' => $api_get_kum[0]['KD_PRODI'], #seharusnya juga diconvert ke versi kd simpeg
					'NAMA' => $api_get_kum[0]['NAMA'],
					'NIM' => $api_get_kum[0]['NIM'],
					'TMP_LAHIR' => $api_get_kum[0]['TMP_LAHIR'],
					'TGL_LAHIR' => date('d-m-Y', strtotime($api_get_kum[0]['TGL_LAHIR'])),
					'JUM_SMT' => $api_get_kum[0]['JUM_SMT'],
					'NM_PRODI' => $api_get_kum[0]['NM_PRODI'],
					'NM_FAK' => $api_get_kum[0]['NM_FAK']
				];

				break;
			case 'ijin_pen':
				#3.1 CEK KE API SIA KUMULATIF DAN SIA MAKUL
				//todo : api get skripsi, supaya kolom judul penelitian dan studi kasus (jika ada) bisa terisi otomatis
				$parameter	= array('api_kode'=>26000, 'api_subkode' => 12, 'api_search' => array($nim));
				$api_get_dpm	= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);
				$parameter	= array('api_kode'=>26000, 'api_subkode' => 10, 'api_search' => array($nim));
				$api_get_kum	= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);
				$api_get_makul	= $this->Api_sia->get_makul_smt($nim);

				#3.2COCOKKAN NILAI KOLOM-KOLOM DENGAN KRITERIA PERSYARATAN S.KET.MASIH KULIAH
				$data['sia_mhs_kum'] = $api_get_kum[0];
				$data['sia_mhs_dpm'] = $api_get_dpm[0];
				$data['sia_mhs_makul'] = $api_get_makul;

				$data['tmp_verified_mhs'] = [
					'UNIT_ID' => convert_kdfak_ke_unitid($api_get_kum[0]['KD_FAK']), #unit_id fakultas mhs
					'KD_PRODI' => $api_get_kum[0]['KD_PRODI'], #seharusnya juga diconvert ke versi kd simpeg
					'NAMA' => $api_get_kum[0]['NAMA'],
					'NIM' => $api_get_kum[0]['NIM'],
					'TMP_LAHIR' => $api_get_kum[0]['TMP_LAHIR'],
					'TGL_LAHIR' => date('d-m-Y', strtotime($api_get_kum[0]['TGL_LAHIR'])),
					'JUM_SMT' => $api_get_kum[0]['JUM_SMT'],
					'NM_PRODI' => $api_get_kum[0]['NM_PRODI'],
					'NM_FAK' => $api_get_kum[0]['NM_FAK'],
					'NO_HP' => $api_get_dpm[0]['HP_MHS'],
					'ALAMAT' => $api_get_dpm[0]['ALAMAT_MHS']
				];
				$data['tmp_isi_detail']['KEPERLUAN'] = 'kelengkapan penyusunan skripsi';
				$data['tmp_isi_detail']['NM_KEGIATAN'] = 'penelitian';
				break;
			case 'rek_pen':
			 	$parameter	= array('api_kode'=>26000, 'api_subkode' => 10, 'api_search' => array($nim));
				$api_get_kum	= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);
				$data['sia_mhs_kum'] = (!empty($api_get_kum) ? $api_get_kum[0] : null);
				$data['tmp_verified_mhs'] = [
					'UNIT_ID' => convert_kdfak_ke_unitid($api_get_kum[0]['KD_FAK']), #unit_id fakultas mhs
					'KD_PRODI' => $api_get_kum[0]['KD_PRODI'], #seharusnya juga diconvert ke versi kd simpeg
					'NAMA' => $api_get_kum[0]['NAMA'],
					'NIM' => $api_get_kum[0]['NIM'],
					'TMP_LAHIR' => $api_get_kum[0]['TMP_LAHIR'],
					'TGL_LAHIR' => date('d-m-Y', strtotime($api_get_kum[0]['TGL_LAHIR'])),
					'JUM_SMT' => $api_get_kum[0]['JUM_SMT'],
					'NM_PRODI' => $api_get_kum[0]['NM_PRODI'],
					'NM_FAK' => $api_get_kum[0]['NM_FAK']
				];
			 break;
			case 'ijin_pen_makul':
				// #3.1 CEK KE API SIA KUMULATIF DAN SIA MAKUL
				// //todo : api get skripsi, supaya kolom judul penelitian dan studi kasus (jika ada) bisa terisi otomatis
				// $parameter	= array('api_kode'=>26000, 'api_subkode' => 12, 'api_search' => array($nim));
				// $api_get_dpm	= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);
				// $parameter	= array('api_kode'=>26000, 'api_subkode' => 10, 'api_search' => array($nim));
				// $api_get_kum	= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);
				// $api_get_makul	= $this->Api_sia->get_makul_smt($nim);
				//
				// #3.2COCOKKAN NILAI KOLOM-KOLOM DENGAN KRITERIA PERSYARATAN S.KET.MASIH KULIAH
				// $data['sia_mhs_kum'] = $api_get_kum[0];
				// $data['sia_mhs_dpm'] = $api_get_dpm[0];
				// $data['sia_mhs_makul'] = $api_get_makul;
				//

				$parameter	= array('api_kode'=>26000, 'api_subkode' => 12, 'api_search' => array($nim));
				$api_get_dpm	= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);
				$parameter	= array('api_kode'=>26000, 'api_subkode' => 10, 'api_search' => array($nim));
				$api_get_kum	= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);

				#3.2COCOKKAN NILAI KOLOM-KOLOM DENGAN KRITERIA PERSYARATAN S.KET.MASIH KULIAH
				$data['sia_mhs_kum'] = $api_get_kum[0];
				$data['sia_mhs_dpm'] = $api_get_dpm[0];

				$data['tmp_verified_mhs'] = [
					'UNIT_ID' => convert_kdfak_ke_unitid($api_get_kum[0]['KD_FAK']), #unit_id fakultas mhs
					'KD_PRODI' => $api_get_kum[0]['KD_PRODI'], #seharusnya juga diconvert ke versi kd simpeg
					'NAMA' => $api_get_kum[0]['NAMA'],
					'NIM' => $api_get_kum[0]['NIM'],
					'TMP_LAHIR' => $api_get_kum[0]['TMP_LAHIR'],
					'TGL_LAHIR' => date('d-m-Y', strtotime($api_get_kum[0]['TGL_LAHIR'])),
					'JUM_SMT' => $api_get_kum[0]['JUM_SMT'],
					'NM_PRODI' => $api_get_kum[0]['NM_PRODI'],
					'NM_FAK' => $api_get_kum[0]['NM_FAK'],
					'NO_HP' => $api_get_dpm[0]['HP_MHS'],
					'ALAMAT' => $api_get_dpm[0]['ALAMAT_MHS']
				];

				$data['tmp_isi_detail']['KEPERLUAN'] = 'tugas mata kuliah';
				$data['tmp_isi_detail']['NM_KEGIATAN'] = 'penelitian';
				break;
			case 'ijin_studi_pen': #Studi Pendahuluan
				$parameter	= array('api_kode'=>26000, 'api_subkode' => 12, 'api_search' => array($nim));
				$api_get_dpm	= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);
				#3.1 CEK KE API SIA KUMULATIF DAN SIA MAKUL
				$parameter	= array('api_kode'=>26000, 'api_subkode' => 10, 'api_search' => array($nim));
				$api_get_kum	= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);
				$api_get_makul	= $this->Api_sia->get_makul_smt($nim);

				#3.2COCOKKAN NILAI KOLOM-KOLOM DENGAN KRITERIA PERSYARATAN S.KET.MASIH KULIAH
				$data['sia_mhs_kum'] = $api_get_kum[0];
				$data['sia_mhs_dpm'] = $api_get_dpm[0];
				$data['sia_mhs_makul'] = $api_get_makul;
				$data['tmp_verified_mhs'] = [
					'UNIT_ID' => convert_kdfak_ke_unitid($api_get_kum[0]['KD_FAK']), #unit_id fakultas mhs
					'KD_PRODI' => $api_get_kum[0]['KD_PRODI'], #seharusnya juga diconvert ke versi kd simpeg
					'NAMA' => $api_get_kum[0]['NAMA'],
					'NIM' => $api_get_kum[0]['NIM'],
					'TMP_LAHIR' => $api_get_kum[0]['TMP_LAHIR'],
					'TGL_LAHIR' => date('d-m-Y', strtotime($api_get_kum[0]['TGL_LAHIR'])),
					'JUM_SMT' => $api_get_kum[0]['JUM_SMT'],
					'NM_PRODI' => $api_get_kum[0]['NM_PRODI'],
					'NM_FAK' => $api_get_kum[0]['NM_FAK'],
					'NO_HP' => $api_get_dpm[0]['HP_MHS'],
					'ALAMAT' => $api_get_dpm[0]['ALAMAT_MHS']
				];
				$data['tmp_isi_detail']['KEPERLUAN'] = 'kelengkapan penyusunan proposal skripsi';
				$data['tmp_isi_detail']['NM_KEGIATAN'] = 'studi pendahuluan';
				break;
			case 'ijin_obs':
				$parameter	= array('api_kode'=>26000, 'api_subkode' => 10, 'api_search' => array($nim));
				$api_get_kum	= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);
				$data['sia_mhs_kum'] = $api_get_kum[0];
				// $api_get_makul	= $this->Api_sia->get_makul_smt($nim);
				// $data['sia_mhs_dpm'] = $api_get_dpm[0];
				// $data['sia_mhs_makul'] = $api_get_makul;

				$data['tmp_verified_mhs'] = [
					'UNIT_ID' => convert_kdfak_ke_unitid($api_get_kum[0]['KD_FAK']),
					'KD_PRODI' => $api_get_kum[0]['KD_PRODI'], #seharusnya juga diconvert ke versi kd simpeg
					'NAMA' => $api_get_kum[0]['NAMA'],
					'NIM' => $api_get_kum[0]['NIM'],
					'TMP_LAHIR' => $api_get_kum[0]['TMP_LAHIR'],
					'TGL_LAHIR' => date('d-m-Y', strtotime($api_get_kum[0]['TGL_LAHIR'])),
					'JUM_SMT' => $api_get_kum[0]['JUM_SMT'],
					'NM_PRODI' => $api_get_kum[0]['NM_PRODI'],
					'NM_FAK' => $api_get_kum[0]['NM_FAK']
				];
				$data['tmp_isi_detail']['KEPERLUAN'] = 'tugas mata kuliah';
				$data['tmp_isi_detail']['NM_KEGIATAN'] = 'observasi';
				break;
			case 'perm_kp':
				$parameter	= array('api_kode'=>26000, 'api_subkode' => 10, 'api_search' => array($nim));
				$api_get_kum	= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);
				$api_get_makul	= $this->Api_sia->get_makul_smt($nim);
				$data['sia_mhs_kum'] = $api_get_kum[0];
				$data['sia_mhs_makul'] = $api_get_makul;

				$data['tmp_verified_mhs'] = [
					'UNIT_ID' => convert_kdfak_ke_unitid($api_get_kum[0]['KD_FAK']), #unit_id fakultas mhs
					'KD_PRODI' => $api_get_kum[0]['KD_PRODI'], #seharusnya juga diconvert ke versi kd simpeg
					'NAMA' => $api_get_kum[0]['NAMA'],
					'NIM' => $api_get_kum[0]['NIM'],
					'TMP_LAHIR' => $api_get_kum[0]['TMP_LAHIR'],
					'TGL_LAHIR' => date('d-m-Y', strtotime($api_get_kum[0]['TGL_LAHIR'])),
					'JUM_SMT' => $api_get_kum[0]['JUM_SMT'],
					'NM_PRODI' => $api_get_kum[0]['NM_PRODI'],
					'NM_FAK' => $api_get_kum[0]['NM_FAK'],
				];
				$data['tmp_isi_detail']['KEPERLUAN'] = 'memenuhi mata kuliah kerja praktek';
				$data['tmp_isi_detail']['NM_KEGIATAN'] = 'kerja praktek';
			case 'hbs_teori':
				$parameter	= array('api_kode'=>26000, 'api_subkode' => 10, 'api_search' => array($nim));
				$api_get_kum	= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);
				$api_get_makul	= $this->Api_sia->get_makul_smt($nim);
				$data['sia_mhs_kum'] = $api_get_kum[0];
				$data['sia_mhs_makul'] = $api_get_makul;

				$data['tmp_verified_mhs'] = [
					'UNIT_ID' => convert_kdfak_ke_unitid($api_get_kum[0]['KD_FAK']), #unit_id fakultas mhs
					'KD_PRODI' => $api_get_kum[0]['KD_PRODI'], #seharusnya juga diconvert ke versi kd simpeg
					'NAMA' => $api_get_kum[0]['NAMA'],
					'NIM' => $api_get_kum[0]['NIM'],
					'TMP_LAHIR' => $api_get_kum[0]['TMP_LAHIR'],
					'TGL_LAHIR' => date('d-m-Y', strtotime($api_get_kum[0]['TGL_LAHIR'])),
					'JUM_SMT' => $api_get_kum[0]['JUM_SMT'],
					'NM_PRODI' => $api_get_kum[0]['NM_PRODI'],
					'NM_FAK' => $api_get_kum[0]['NM_FAK']
				];
				break;
			case 'kel_baik':
				$parameter	= array('api_kode'=>26000, 'api_subkode' => 10, 'api_search' => array($nim));
				$api_get_kum	= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);

				#3.2COCOKKAN NILAI KOLOM-KOLOM DENGAN KRITERIA PERSYARATAN S.KET.MASIH KULIAH
				$data['sia_mhs_kum'] = $api_get_kum[0];
				$data['tmp_verified_mhs'] = [
					'UNIT_ID' => convert_kdfak_ke_unitid($api_get_kum[0]['KD_FAK']), #unit_id fakultas mhs
					'KD_PRODI' => $api_get_kum[0]['KD_PRODI'], #seharusnya juga diconvert ke versi kd simpeg
					'NAMA' => $api_get_kum[0]['NAMA'],
					'NIM' => $api_get_kum[0]['NIM'],
					'TMP_LAHIR' => $api_get_kum[0]['TMP_LAHIR'],
					'TGL_LAHIR' => date('d-m-Y', strtotime($api_get_kum[0]['TGL_LAHIR'])),
					'JUM_SMT' => $api_get_kum[0]['JUM_SMT'],
					'NM_PRODI' => $api_get_kum[0]['NM_PRODI'],
					'NM_FAK' => $api_get_kum[0]['NM_FAK']
				];
				break;
			case 'pndh_studi':
				$parameter	= array('api_kode'=>26000, 'api_subkode' => 10, 'api_search' => array($nim));
				$api_get_kum	= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);

				#3.2COCOKKAN NILAI KOLOM-KOLOM DENGAN KRITERIA PERSYARATAN S.KET.MASIH KULIAH
				$data['sia_mhs_kum'] = $api_get_kum[0];
				$data['tmp_verified_mhs'] = [
					'UNIT_ID' => convert_kdfak_ke_unitid($api_get_kum[0]['KD_FAK']), #unit_id fakultas mhs
					'KD_PRODI' => $api_get_kum[0]['KD_PRODI'], #seharusnya juga diconvert ke versi kd simpeg
					'NAMA' => $api_get_kum[0]['NAMA'],
					'NIM' => $api_get_kum[0]['NIM'],
					'TMP_LAHIR' => $api_get_kum[0]['TMP_LAHIR'],
					'TGL_LAHIR' => date('d-m-Y', strtotime($api_get_kum[0]['TGL_LAHIR'])),
					'JUM_SMT' => $api_get_kum[0]['JUM_SMT'],
					'NM_PRODI' => $api_get_kum[0]['NM_PRODI'],
					'NM_FAK' => $api_get_kum[0]['NM_FAK']
				];
				break;
			case 'tdk_men_bea':
				$parameter	= array('api_kode'=>26000, 'api_subkode' => 10, 'api_search' => array($nim));
				$api_get_kum	= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);
				$api_get_bea = $this->Api_sia->get_beasiswa($nim); #cek status beasiswa (semua)

				$data['sia_mhs_kum'] = $api_get_kum[0];
				$data['sia_beasiswa'] = $api_get_bea;

				$data['tmp_verified_mhs'] = [
					'UNIT_ID' => convert_kdfak_ke_unitid($api_get_kum[0]['KD_FAK']), #unit_id fakultas mhs
					'KD_PRODI' => $api_get_kum[0]['KD_PRODI'], #seharusnya juga diconvert ke versi kd simpeg
					'NAMA' => $api_get_kum[0]['NAMA'],
					'NIM' => $api_get_kum[0]['NIM'],
					'TMP_LAHIR' => $api_get_kum[0]['TMP_LAHIR'],
					'TGL_LAHIR' => date('d-m-Y', strtotime($api_get_kum[0]['TGL_LAHIR'])),
					'JUM_SMT' => $api_get_kum[0]['JUM_SMT'],
					'NM_PRODI' => $api_get_kum[0]['NM_PRODI'],
					'NM_FAK' => $api_get_kum[0]['NM_FAK']
				];

				break;
			case 'ket_lulus':				
				$parameter	= array('api_kode'=>26000, 'api_subkode' => 10, 'api_search' => array($nim));
				$api_get_kum	= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);
				$api_get_lulus = $this->Api_sia->cekTIYM($nim); #data kelulusan (TA, IPK, Yudisium)
				$data['sia_mhs_kum'] = (!empty($api_get_kum) ? $api_get_kum[0] : null);
				$data['sia_mhs_lulus'] = (!empty($api_get_lulus) ? $api_get_lulus[0] : null);				

				$data['tmp_verified_mhs'] = [
					'UNIT_ID' => convert_kdfak_ke_unitid($api_get_kum[0]['KD_FAK']), #unit_id fakultas mhs
					'KD_PRODI' => $api_get_kum[0]['KD_PRODI'], #seharusnya juga diconvert ke versi kd simpeg
					'NAMA' => $api_get_kum[0]['NAMA'],
					'NIM' => $api_get_kum[0]['NIM'],
					'TMP_LAHIR' => $api_get_kum[0]['TMP_LAHIR'],
					'TGL_LAHIR' => date('d-m-Y', strtotime($api_get_kum[0]['TGL_LAHIR'])),
					//'JUM_SMT' => $api_get_kum[0]['JUM_SMT'],
					'NM_PRODI' => $api_get_kum[0]['NM_PRODI'],
					'NM_FAK' => $api_get_kum[0]['NM_FAK']
				];

				if (!empty($data['sia_mhs_lulus'])){
					$tmp_isi_detail_more = [
						'JENJANG' => $api_get_kum[0]['NM_JENJANG'],
						'TA' => $api_get_lulus[0]['TA'],
						'TGL_MUNAQOSYAH' => $api_get_lulus[0]['TGL_LULUS_F'],
						'TGL_MUNAQOSYAH_INDO' => $api_get_lulus[0]['TANGGAL'],
						'TGL_YUDISIUM' => $api_get_lulus[0]['TGL_YUDISIUM'],
						'IPK' => $api_get_lulus[0]['IPK_TRANS'],
						'PREDIKAT' => $api_get_lulus[0]['PREDIKAT'],
					];	
					// $data['tmp_isi_detail'] = array_merge($data['tmp_isi_detail'], $tmp_isi_detail_more);
					$data['tmp_isi_detail']['DATA_TAMBAHAN'] = $tmp_isi_detail_more;
				}
				
				
			 break;			
			default:
				# code...
				break;
		}

		return $data;
	}


//  ============= VERSI PERTAMA ==================
/**
	* Cek Persyaratan Versi 2 (ditambahkan pada 19 Feb 2018)
	 * Kondisional jenis surat dengan switch case biasa, bukan menggunakan kolom CEK_SYARAT pada tabel MD_JENIS_SAKAD
	 * @return array & session
	 */
	public function cek_persyaratan($jenis_sakad=array(), $nim=null)
	{
		$this->load->helper('pgw/Tnde_converter_to_simpeg');

		#1. GRAB INPUTAN USER
		$grup_user_login = $this->session->userdata('grup');
		$path_jenis_sakad = $jenis_sakad['URLPATH'];
		$kd_jenis_sakad = (int)$jenis_sakad['KD_JENIS_SAKAD'];

		$data['tmp_isi_detail'] = [
			'KD_JENIS_SAKAD' => $kd_jenis_sakad,
			'KD_JENIS_SURAT' => (int)$jenis_sakad['KD_JENIS_SURAT_INDUK'],
			'PATH_JENIS_SAKAD' => $path_jenis_sakad,
			'GRUP_JENIS_SAKAD' => (int)$jenis_sakad['GRUP_JENIS_SAKAD'],
			'NM_JENIS_SAKAD' => $jenis_sakad['NM_JENIS_SAKAD']
		];

		/*$repo_api_sia = [
			'sia_kumulatif' => [
				'endpoint' => 'sia_mahasiswa/data_search',
				'api_kode' => 26000,
				'api_subkode' => 12,
				'api_search' => array($nim)
			],
			'sia_dpm' => [
				'sia_mahasiswa/data_search',
				'api_kode'=>26000,
				'api_subkode' => 12,
				'api_search' => array($nim)
			],
			'sia_nilai' => [
			],
			'sia_beasiswa' => [
			]
		];*/

		if ($grup_user_login == 'mhs'){
			$pembuat_surat = $nim;
			#2. GET Persyaratan Jenis Surat
			$data['persyaratan'] = $this->get_persyaratan_by_path($path_jenis_sakad);

			$data['hasil_cek_mhs'] = null; #default dulu nanti diisikan
			#3. AKSES KE API DATA MHS sesuai jenis surat masing2
			switch ($path_jenis_sakad) {
				case 'masih_kul':
					#3.1 CEK KE API SIA DPM
					$parameter	= array('api_kode'=>26000, 'api_subkode' => 12, 'api_search' => array($nim));
					$api_get_dpm	= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);
					$parameter	= array('api_kode'=>26000, 'api_subkode' => 10, 'api_search' => array($nim));
					$api_get_kum	= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);

					#3.2COCOKKAN NILAI KOLOM-KOLOM DENGAN KRITERIA PERSYARATAN S.KET.MASIH KULIAH
					$data['sia_mhs_kum'] = $api_get_kum[0];
					$data['sia_mhs_dpm'] = $api_get_dpm[0];
					$data['tmp_verified_mhs'] = [
						'UNIT_ID' => convert_kdfak_ke_unitid($api_get_kum[0]['KD_FAK']), #unit_id fakultas mhs
						'KD_PRODI' => $api_get_kum[0]['KD_PRODI'], #seharusnya juga diconvert ke versi kd simpeg
						'NAMA' => $api_get_kum[0]['NAMA'],
						'NIM' => $api_get_kum[0]['NIM'],
						'TMP_LAHIR' => $api_get_kum[0]['TMP_LAHIR'],
						'TGL_LAHIR' => date('d-m-Y', strtotime($api_get_kum[0]['TGL_LAHIR'])),
						'JUM_SMT' => $api_get_kum[0]['JUM_SMT'],
						'NM_PRODI' => $api_get_kum[0]['NM_PRODI'],
						'NM_FAK' => $api_get_kum[0]['NM_FAK']
					];

					break;
				case 'ijin_pen':
					#3.1 CEK KE API SIA KUMULATIF DAN SIA MAKUL
					//todo : api get skripsi, supaya kolom judul penelitian dan studi kasus (jika ada) bisa terisi otomatis
					$parameter	= array('api_kode'=>26000, 'api_subkode' => 12, 'api_search' => array($nim));
					$api_get_dpm	= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);
					$parameter	= array('api_kode'=>26000, 'api_subkode' => 10, 'api_search' => array($nim));
					$api_get_kum	= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);
					$api_get_makul	= $this->Api_sia->get_makul_smt($nim);

					#3.2COCOKKAN NILAI KOLOM-KOLOM DENGAN KRITERIA PERSYARATAN S.KET.MASIH KULIAH
					$data['sia_mhs_kum'] = $api_get_kum[0];
					$data['sia_mhs_dpm'] = $api_get_dpm[0];
					$data['sia_mhs_makul'] = $api_get_makul;

					$data['tmp_verified_mhs'] = [
						'UNIT_ID' => convert_kdfak_ke_unitid($api_get_kum[0]['KD_FAK']), #unit_id fakultas mhs
						'KD_PRODI' => $api_get_kum[0]['KD_PRODI'], #seharusnya juga diconvert ke versi kd simpeg
						'NAMA' => $api_get_kum[0]['NAMA'],
						'NIM' => $api_get_kum[0]['NIM'],
						'TMP_LAHIR' => $api_get_kum[0]['TMP_LAHIR'],
						'TGL_LAHIR' => date('d-m-Y', strtotime($api_get_kum[0]['TGL_LAHIR'])),
						'JUM_SMT' => $api_get_kum[0]['JUM_SMT'],
						'NM_PRODI' => $api_get_kum[0]['NM_PRODI'],
						'NM_FAK' => $api_get_kum[0]['NM_FAK'],
						'NO_HP' => $api_get_dpm[0]['HP_MHS'],
						'ALAMAT' => $api_get_dpm[0]['ALAMAT_MHS']
					];
					$data['tmp_isi_detail']['KEPERLUAN'] = 'kelengkapan penyusunan skripsi';
					$data['tmp_isi_detail']['NM_KEGIATAN'] = 'penelitian';
					break;
				case 'ijin_studi_pen': #Studi Pendahuluan
					$parameter	= array('api_kode'=>26000, 'api_subkode' => 12, 'api_search' => array($nim));
					$api_get_dpm	= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);
					#3.1 CEK KE API SIA KUMULATIF DAN SIA MAKUL
					$parameter	= array('api_kode'=>26000, 'api_subkode' => 10, 'api_search' => array($nim));
					$api_get_kum	= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);
					$api_get_makul	= $this->Api_sia->get_makul_smt($nim);

					#3.2COCOKKAN NILAI KOLOM-KOLOM DENGAN KRITERIA PERSYARATAN S.KET.MASIH KULIAH
					$data['sia_mhs_kum'] = $api_get_kum[0];
					$data['sia_mhs_dpm'] = $api_get_dpm[0];
					$data['sia_mhs_makul'] = $api_get_makul;
					$data['tmp_verified_mhs'] = [
						'UNIT_ID' => convert_kdfak_ke_unitid($api_get_kum[0]['KD_FAK']), #unit_id fakultas mhs
						'KD_PRODI' => $api_get_kum[0]['KD_PRODI'], #seharusnya juga diconvert ke versi kd simpeg
						'NAMA' => $api_get_kum[0]['NAMA'],
						'NIM' => $api_get_kum[0]['NIM'],
						'TMP_LAHIR' => $api_get_kum[0]['TMP_LAHIR'],
						'TGL_LAHIR' => date('d-m-Y', strtotime($api_get_kum[0]['TGL_LAHIR'])),
						'JUM_SMT' => $api_get_kum[0]['JUM_SMT'],
						'NM_PRODI' => $api_get_kum[0]['NM_PRODI'],
						'NM_FAK' => $api_get_kum[0]['NM_FAK'],
						'NO_HP' => $api_get_dpm[0]['HP_MHS'],
						'ALAMAT' => $api_get_dpm[0]['ALAMAT_MHS']
					];
					$data['tmp_isi_detail']['KEPERLUAN'] = 'kelengkapan penyusunan proposal skripsi';
					$data['tmp_isi_detail']['NM_KEGIATAN'] = 'studi pendahuluan';
					break;
				case 'ijin_obs':
					$parameter	= array('api_kode'=>26000, 'api_subkode' => 10, 'api_search' => array($nim));
					$api_get_kum	= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);
					$data['sia_mhs_kum'] = $api_get_kum[0];
					// $api_get_makul	= $this->Api_sia->get_makul_smt($nim);
					// $data['sia_mhs_dpm'] = $api_get_dpm[0];
					// $data['sia_mhs_makul'] = $api_get_makul;

					$data['tmp_verified_mhs'] = [
						'UNIT_ID' => convert_kdfak_ke_unitid($api_get_kum[0]['KD_FAK']),
						'KD_PRODI' => $api_get_kum[0]['KD_PRODI'], #seharusnya juga diconvert ke versi kd simpeg
						'NAMA' => $api_get_kum[0]['NAMA'],
						'NIM' => $api_get_kum[0]['NIM'],
						'TMP_LAHIR' => $api_get_kum[0]['TMP_LAHIR'],
						'TGL_LAHIR' => date('d-m-Y', strtotime($api_get_kum[0]['TGL_LAHIR'])),
						'JUM_SMT' => $api_get_kum[0]['JUM_SMT'],
						'NM_PRODI' => $api_get_kum[0]['NM_PRODI'],
						'NM_FAK' => $api_get_kum[0]['NM_FAK']
					];
					$data['tmp_isi_detail']['KEPERLUAN'] = 'tugas mata kuliah';
					$data['tmp_isi_detail']['NM_KEGIATAN'] = 'observasi';
					break;
				case 'ijin_pen_makul':
						// $parameter	= array('api_kode'=>26000, 'api_subkode' => 12, 'api_search' => array($nim));
						// $api_get_dpm	= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);
						$parameter	= array('api_kode'=>26000, 'api_subkode' => 10, 'api_search' => array($nim));
						$api_get_kum	= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);

						#3.2COCOKKAN NILAI KOLOM-KOLOM DENGAN KRITERIA PERSYARATAN S.KET.MASIH KULIAH
						$data['sia_mhs_kum'] = $api_get_kum[0];
						// $data['sia_mhs_dpm'] = $api_get_dpm[0];

						$data['tmp_verified_mhs'] = [
							'UNIT_ID' => convert_kdfak_ke_unitid($api_get_kum[0]['KD_FAK']), #unit_id fakultas mhs
							'KD_PRODI' => $api_get_kum[0]['KD_PRODI'], #seharusnya juga diconvert ke versi kd simpeg
							'NAMA' => $api_get_kum[0]['NAMA'],
							'NIM' => $api_get_kum[0]['NIM'],
							'TMP_LAHIR' => $api_get_kum[0]['TMP_LAHIR'],
							'TGL_LAHIR' => date('d-m-Y', strtotime($api_get_kum[0]['TGL_LAHIR'])),
							'JUM_SMT' => $api_get_kum[0]['JUM_SMT'],
							'NM_PRODI' => $api_get_kum[0]['NM_PRODI'],
							'NM_FAK' => $api_get_kum[0]['NM_FAK'],
							// 'NO_HP' => $api_get_dpm[0]['HP_MHS'],
							// 'ALAMAT' => $api_get_dpm[0]['ALAMAT_MHS']
						];

						$data['tmp_isi_detail']['KEPERLUAN'] = 'tugas mata kuliah';
						$data['tmp_isi_detail']['NM_KEGIATAN'] = 'penelitian';
				break;
				case 'perm_KP':
					$parameter	= array('api_kode'=>26000, 'api_subkode' => 10, 'api_search' => array($nim));
					$api_get_kum	= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);
					$api_get_makul	= $this->Api_sia->get_makul_smt($nim);
					$data['sia_mhs_kum'] = $api_get_kum[0];
					$data['sia_mhs_makul'] = $api_get_makul;

					$data['tmp_verified_mhs'] = [
						'UNIT_ID' => convert_kdfak_ke_unitid($api_get_kum[0]['KD_FAK']), #unit_id fakultas mhs
						'KD_PRODI' => $api_get_kum[0]['KD_PRODI'], #seharusnya juga diconvert ke versi kd simpeg
						'NAMA' => $api_get_kum[0]['NAMA'],
						'NIM' => $api_get_kum[0]['NIM'],
						'TMP_LAHIR' => $api_get_kum[0]['TMP_LAHIR'],
						'TGL_LAHIR' => date('d-m-Y', strtotime($api_get_kum[0]['TGL_LAHIR'])),
						'JUM_SMT' => $api_get_kum[0]['JUM_SMT'],
						'NM_PRODI' => $api_get_kum[0]['NM_PRODI'],
						'NM_FAK' => $api_get_kum[0]['NM_FAK'],
					];
					$data['tmp_isi_detail']['KEPERLUAN'] = 'memenuhi mata kuliah kerja praktek';
					$data['tmp_isi_detail']['NM_KEGIATAN'] = 'kerja praktek';
				case 'hbs_teori':
					$parameter	= array('api_kode'=>26000, 'api_subkode' => 10, 'api_search' => array($nim));
					$api_get_kum	= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);
					$api_get_makul	= $this->Api_sia->get_makul_smt($nim);
					$data['sia_mhs_kum'] = $api_get_kum[0];
					$data['sia_mhs_makul'] = $api_get_makul;

					$data['tmp_verified_mhs'] = [
						'UNIT_ID' => convert_kdfak_ke_unitid($api_get_kum[0]['KD_FAK']), #unit_id fakultas mhs
						'KD_PRODI' => $api_get_kum[0]['KD_PRODI'], #seharusnya juga diconvert ke versi kd simpeg
						'NAMA' => $api_get_kum[0]['NAMA'],
						'NIM' => $api_get_kum[0]['NIM'],
						'TMP_LAHIR' => $api_get_kum[0]['TMP_LAHIR'],
						'TGL_LAHIR' => date('d-m-Y', strtotime($api_get_kum[0]['TGL_LAHIR'])),
						'JUM_SMT' => $api_get_kum[0]['JUM_SMT'],
						'NM_PRODI' => $api_get_kum[0]['NM_PRODI'],
						'NM_FAK' => $api_get_kum[0]['NM_FAK']
					];
					break;
				case 'kel_baik':
					$parameter	= array('api_kode'=>26000, 'api_subkode' => 10, 'api_search' => array($nim));
					$api_get_kum	= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);

					#3.2COCOKKAN NILAI KOLOM-KOLOM DENGAN KRITERIA PERSYARATAN S.KET.MASIH KULIAH
					$data['sia_mhs_kum'] = $api_get_kum[0];
					$data['tmp_verified_mhs'] = [
						'UNIT_ID' => convert_kdfak_ke_unitid($api_get_kum[0]['KD_FAK']), #unit_id fakultas mhs
						'KD_PRODI' => $api_get_kum[0]['KD_PRODI'], #seharusnya juga diconvert ke versi kd simpeg
						'NAMA' => $api_get_kum[0]['NAMA'],
						'NIM' => $api_get_kum[0]['NIM'],
						'TMP_LAHIR' => $api_get_kum[0]['TMP_LAHIR'],
						'TGL_LAHIR' => date('d-m-Y', strtotime($api_get_kum[0]['TGL_LAHIR'])),
						'JUM_SMT' => $api_get_kum[0]['JUM_SMT'],
						'NM_PRODI' => $api_get_kum[0]['NM_PRODI'],
						'NM_FAK' => $api_get_kum[0]['NM_FAK']
					];
					break;
				case 'pndh_studi':
					$parameter	= array('api_kode'=>26000, 'api_subkode' => 10, 'api_search' => array($nim));
					$api_get_kum	= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);

					#3.2COCOKKAN NILAI KOLOM-KOLOM DENGAN KRITERIA PERSYARATAN S.KET.MASIH KULIAH
					$data['sia_mhs_kum'] = $api_get_kum[0];
					$data['tmp_verified_mhs'] = [
						'UNIT_ID' => convert_kdfak_ke_unitid($api_get_kum[0]['KD_FAK']), #unit_id fakultas mhs
						'KD_PRODI' => $api_get_kum[0]['KD_PRODI'], #seharusnya juga diconvert ke versi kd simpeg
						'NAMA' => $api_get_kum[0]['NAMA'],
						'NIM' => $api_get_kum[0]['NIM'],
						'TMP_LAHIR' => $api_get_kum[0]['TMP_LAHIR'],
						'TGL_LAHIR' => date('d-m-Y', strtotime($api_get_kum[0]['TGL_LAHIR'])),
						'JUM_SMT' => $api_get_kum[0]['JUM_SMT'],
						'NM_PRODI' => $api_get_kum[0]['NM_PRODI'],
						'NM_FAK' => $api_get_kum[0]['NM_FAK']
					];
					break;
				case 'tdk_men_bea':
					$parameter	= array('api_kode'=>26000, 'api_subkode' => 10, 'api_search' => array($nim));
					$api_get_kum	= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);
					$api_get_bea = $this->Api_sia->get_beasiswa($nim); #cek status beasiswa (semua)

					$data['sia_mhs_kum'] = $api_get_kum[0];
					$data['sia_beasiswa'] = $api_get_bea;

					$data['tmp_verified_mhs'] = [
						'UNIT_ID' => convert_kdfak_ke_unitid($api_get_kum[0]['KD_FAK']), #unit_id fakultas mhs
						'KD_PRODI' => $api_get_kum[0]['KD_PRODI'], #seharusnya juga diconvert ke versi kd simpeg
						'NAMA' => $api_get_kum[0]['NAMA'],
						'NIM' => $api_get_kum[0]['NIM'],
						'TMP_LAHIR' => $api_get_kum[0]['TMP_LAHIR'],
						'TGL_LAHIR' => date('d-m-Y', strtotime($api_get_kum[0]['TGL_LAHIR'])),
						'JUM_SMT' => $api_get_kum[0]['JUM_SMT'],
						'NM_PRODI' => $api_get_kum[0]['NM_PRODI'],
						'NM_FAK' => $api_get_kum[0]['NM_FAK']
					];

					break;
				default:
					# code...
					break;
			}

			#4. MATCHING Persyaratan x Data Mhs
			$ceksyarat_errors = 0;
	        $tmp_final = array( 'num_errors'=>0, 'output'=>array() );
	        $tot_item_persyaratan = count($data['persyaratan']['data']);

	        # 4.1 Loop tiap item persyaratan, bandingkan expected value dg value data mahasiswa
	        foreach ($data['persyaratan']['data'] as $key => $val) {
	        	$tmp_final['output'][$key]['NO.'] = $key + 1;
	        	$tmp_final['output'][$key]['OUT_TXT_JUDUL'] = $val["OUT_TXT_JUDUL"];
	        	$tmp_final['output'][$key]['OUT_TXT_HUBUNGI'] = $val["OUT_TXT_HUBUNGI"];

	        	if ($val["TIPE_OPERATOR"] == '==='){ #NILAI STRING HARUS SAMA
	        		$field_mhs = $data[$val["ASAL_API"]][$val["FIELD"]];
	        		$nilai_pembanding = $val["ISI_REQUIRED"];

	        		if ( $field_mhs === $nilai_pembanding){
	        			$tmp_final['output'][$key]['ISI'] = $field_mhs;
	        			$tmp_final['output'][$key]['STATUS'] = 'pass';
	        		} else {
	        			$tmp_final['output'][$key]['ISI'] = $field_mhs;
	        			$tmp_final['output'][$key]['STATUS'] = 'fail';
	        			$ceksyarat_errors++;
	        		}
	        	}

	        	if ($val["TIPE_OPERATOR"] == 'compare_float'){

	        	}

	        	if($val["TIPE_OPERATOR"] == 'custom')#MULTI CHECK
	        	{
	        		$key_field_syarat = explode('#', $val['FIELD']);

	        		$klist = array_flip($key_field_syarat);
	        		$key_dipilih = array_intersect_key(
	        			array_replace(
	        				$klist,
	        				$data[$val['ASAL_API']]
	        			)
	        			, $klist
	        		);
	        		//exit(print_r($key_dipilih));
	        		//$checker = explode('#', $key_field_syarat);

	        		$sub_error = 0;
	        		$txtstatus = 'Hasil : <br><ol>';
	        		foreach ($key_dipilih as $subfield) {
	        			if (!empty($subfield)){
	        				$txtstatus .= '<li>'.$subfield.'</li>';
	        			} else {
	        				$txtstatus .= '<li>belum diisi</li>';
	        				$sub_error++;
	        			}
	        		}
	        		$txtstatus .= '</ol>';

	        		if ($sub_error > 1){
	        			$ceksyarat_errors++;
	        			$txtstatus .= '<br><p><i>Segera lengkapi DPM di SIA<i><p>';
	        			$tmp_final['output'][$key]['STATUS'] = 'fail';
	        		} else {
	        			$tmp_final['output'][$key]['STATUS'] = 'pass';
	        		}
	        		$tmp_final['output'][$key]['ISI'] = $txtstatus;
	        	}

	        	if ($val["TIPE_OPERATOR"] == 'regex_string'){
	        		$raw_makul = $data[$val['ASAL_API']];
	        		//exit(print_r($raw_makul));

	        		if ($val['FIELD'] == 'NM_MK'){
	        			$kol_nama_makul = array_column($data[$val['ASAL_API']], 'NM_MK');
		        		$hasil_regex = ['count'=>0];
		        		$pattern_dicari = $val['ISI_REQUIRED'];
		        		foreach ($kol_nama_makul as $ky => $vl) {
							$is_found_yet = preg_match("/".$pattern_dicari."/i", $vl, $result);
							//$hasil_regex['result'][] = $result; //tampung pregmatch ke array hasil regex
							if (!empty($result)){ //jika pattern ditemukan selama loop ini
								$hasil_regex['count']++;
								$hasil_regex['result'][]['detail'] = $raw_makul[$ky];
							}
		        		}
		        		if ($hasil_regex['count'] > 0){
		        			//exit(print_r($hasil_regex['result']));
		        			if (!empty($hasil_regex['result'])){

		        				if (isset($hasil_regex['result'][0])){
		        					$rgxdet = $hasil_regex['result'][0]['detail'];
		        				}

		        			}

		        			$tmp_final['output'][$key]['ISI'] = $rgxdet["NM_MK"].' ('.$rgxdet["KD_MK"].')'.' - diampu oleh '.$rgxdet["NM_DOSEN_F"];
		        			$tmp_final['output'][$key]['STATUS'] = 'pass';
		        			$data['tmp_isi_detail']['MATA_KULIAH_TERKAIT'] = $rgxdet["KD_MK"].'#'.$rgxdet["NM_MK"];

		        		} else {
		        			$tmp_final['output'][$key]['ISI'] = $val['OUT_TXT_KET'];
		        			$tmp_final['output'][$key]['STATUS'] = 'fail';
		        			$ceksyarat_errors++;
		        		}
	        		}
	        	}

	        	if ($val["TIPE_OPERATOR"] == 'map_string'){#map setiap item array untuk dibandingkan dengan operan kedua (isi_required)
	        		if (!empty($data[$val['ASAL_API']])) { //cek apakah array berisi atau kosong
	        			/*$key = isset($val["FIELD"]) ? $val["FIELD"] : null;
	        			$isi_required = $val['ISI_REQUIRED'];
	        			*/
	        			$equal = 'A';
						$beasiswa_aktif = array_filter($data[$val['ASAL_API']], function ($item) use ($equal) {
						    if ($item['STATUS'] === $equal){
						    	return true;
						    }
						    return false;
						});


						//jika ditemukan beasiswa aktif = tidak lolos verifikasi,
						//jika yg ditemukan beasiswa tdk aktif = lolos verifikasi dan set nilai2 ke array $beasiswa_pernah_diikuti
						if (!empty($beasiswa_aktif)){
							$tmp_final['output'][$key]['ISI'] = $beasiswa_aktif[0]["NAMA_BEASISWA"];
							$tmp_final['output'][$key]['STATUS'] = 'fail';
							$ceksyarat_errors++;
						} else {
							$tmp_isi_detail['BEASISWA_PERNAH_DIIKUTI'] = '';
							$tmp_final['output'][$key]['ISI'] = 'beasiswa lalu';
							$tmp_final['output'][$key]['STATUS'] = 'pass';
						}

	        		} else { //tidak memiliki riwayat beasiswa apapun
	        			$tmp_final['output'][$key]['ISI'] = 'nihil';
	        			$tmp_final['output'][$key]['STATUS'] = 'pass';
	        		}

	        	}

	        }
	        $tmp_final['num_errors'] = $ceksyarat_errors;
	        $data['hasil_cek_mhs'] = $tmp_final;
			return $data;
		} elseif ($grup_user_login == 'pgw') {
			# code...
		}
	}

// =============== END VERSI PERTAMA ====================
	public function testing_callback($paramdata_cf=array())
	{
		// $paramdata_cf = array(
		// 	array(
		// 		'NIM' => '11650021',
		// 		'KD_PRODI' => '22607'
		// 	)
		// );
		$fungsi_berjalan = call_user_func_array( array($this, 'cf_pengecekan_bebas_teori') , $paramdata_cf);				
		return $fungsi_berjalan;
	}
### **************************** testing purpose ********************	
	/**
	* Cek apakah telah memenuhi syarat untuk bebas teori
	* keperluan : pembuatan surat keterangan bebas teori yang ditindak lanjuti dengan input penetapan bebas teori oleh pegawai TU
	*/
	public function cf_pengecekan_bebas_teori($paramdata=array())
	{		
		#set parameter
		$nim = isset($paramdata['NIM']) ? $paramdata['NIM'] : null;
		$kd_prodi = isset($paramdata['KD_PRODI']) ? $paramdata['KD_PRODI'] : null;		
		$log_error = [];
		#---- CEK KHS KUMULATIF ----- 
		$jumsksw = 0;
		$jumskst = 0;
		$jumnilaiCmin = 0;
		$jumnilaiE = 0;
		$khskum = $this->Api_sia->cek_khs_kumul($nim, 'default');
		$ceknilailulus = array('C-','C/D','D','E');
		#$ceknilailulus = array('E');
		$ceknilaiminC = array('C-','C/D','D');
		$ceknilaiE = array('E');
		foreach($khskum as $k => $kk){
			// JUM SKS WAJIB YANG SUDAH DIAMBIL
			if($kk['NM_JENIS_MK'] == 'WAJIB'){
				// Nilai yang lulus
				if(!in_array($kk['NILAI'], $ceknilailulus)){
					$jumsksw = $jumsksw + $kk['SKS'];
				}
			}
			// SKS TOTAL
			if(!in_array($kk['NILAI'], $ceknilaiE)){
				$jumskst = $jumskst + $kk['SKS'];
			}
			
			// JUM NILAI C-|C/D|D
			if(in_array($kk['NILAI'], $ceknilaiminC)){
				$jumnilaiCmin++;
			}
			// JUM NILAI E
			if(in_array($kk['NILAI'], $ceknilaiE)){
				$jumnilaiE++;
				$log_error[] = $kk;
			}
		}

		// CEK IPK
		$cbtm  	= $this->Api_sia->cek_bebas_teori_mhs($nim);			
		#cek nilai IPK		
		foreach($cbtm["result"]["cek_ipk"] as $dt22 => $qry22){
			if(round($qry22["IPK"],2) >= 2){
				$cek_ipk = true;
			}else{
				$cek_ipk = false;
			}
			$ipkk = round($qry22["IPK"],2);
		}

		$output1 = [
			'jumlah_sks_wajib' => $jumsksw,
			'jumlah_sks_total' => $jumskst,
			'jumlah_nilai_c_minus_kebawah' => $jumnilaiCmin,
			'jumlah_nilai_e' => $jumnilaiE
		];


		//$kd_prodi = '22607'; //PRODI T.INF
		//-------- KURIKULUM Aktif ---------------
		$kurak = $this->Api_sia->cek_kur_aktif($kd_prodi);
		$jumkurak = count($kurak);
		if($jumkurak == 1){
			foreach($kurak as $ku => $kur){
				$kdkur = $kur['KD_KUR'];
			}
			//echo $kdkur;
		}

		//---- cek makul akhir -----
		$cmk = $this->Api_sia->cek_matkul_akhir($nim);
		$arr_mkakhir= [];
		//print_r($cmk);
		foreach($cmk as $cm1 => $cmkkk){
			// array KD MK AKHIR
			$arr_mkakhir[] = $cmkkk['KD_MK'];
		}

		//-------- CEK STANDAR MK WAJIB DI KUR ACUAN ----------
		$totskswajib = 0;
		$cam	= $this->Api_sia->cek_makul_kur($kd_prodi, $kdkur);		
		foreach($cam as $dt11 => $qry11){
			// total sks wajib yang sudah diambil
			if($qry11['JENIS_MK_F'] == 'WAJIB' && !in_array($qry11['KD_MK'], $arr_mkakhir)){ //NM_JENIS_MK
				$totskswajib += $qry11['SKS'];
			}
		}

		//-------- Cek Detil KUR Aktif ------------
		$ckr = $this->Api_sia->cek_detil_kur($kd_prodi, $kdkur);
		$minsksawal = $ckr[0]['MIN_SKS_EVALUASI_AWAL'];


		//--- Proses Pengecekan Bebas Teori
		if($jumsksw < $totskswajib){
			$log = 'Jumlah sks wajib belum memenuhi';
			$bebas_teori = false;
		}else{
			if($jumnilaiCmin <= 3){
				if($jumnilaiE == 0){
					if($cek_ipk){
						if($jumskst >= $minsksawal){
							$bebas_teori = true;
						}else{
							$log = 'SKS MIN LULUS';
							$bebas_teori = false;
						}
					}else{
						$log = 'IPK';
						$bebas_teori = false;
					}
				} else {
					$log = 'masih memiliki nilai E';
					$bebas_teori = false;
				}
			} else {
				$log = 'nilai min C';
				$bebas_teori = false;
			}
		}

		#TAMPILKAN OUTPUT AKHIR
		$response = [];
		if ($bebas_teori){ #memenuhi ketentuan bebas teori
			$response['ISI'] = '<p class="text-success">Memenuhi syarat bebas teori<br/>
							 - '.$jumnilaiCmin.' Matakuliah<br/>
							 - '.$jumnilaiE.' Matakuliah<br/>
							 - '.$ipkk.'<br/>							 
							 - '.$jumsksw.' SKS MK Wajib (Selain TA)<br/>
							 - '.$jumskst.' SKS (Selain TA)<br/>';

			$response['STATUS'] = "pass";
			$response['num_errors'] = 0;
			$response['DATASET'] = [
				'bebas_teori' => [
					'ipk' => $ipkk,
					'jum_nl_cmin' => $jumnilaiCmin,
					'jum_nl_e' => $jumnilaiE,
					'sks_wajib_ditempuh' => $jumsksw,
					'batas_minimum_sks_wajib' => $totskswajib
				]				
			];
		} else {
			$response['ISI'] = 'Belum memenuhi syarat bebas teori<br/>
							 - Jumlah Nilai C-|C/D|D+|D = '.$jumnilaiCmin.' Matakuliah<br/>
							 - Jumlah Nilai E = '.$jumnilaiE.' Matakuliah<br/>
							 - IPK = '.$ipkk.'<br/>									
							 - Lulus '.$jumsksw.' SKS MK Wajib (Selain TA)<br/>
							 - Lulus = '.$jumskst.' SKS (Selain TA)<br/>

							 <p class="text-danger"><b>catatan: '.$log.'</b></p>
			';
			$response['STATUS'] = "fail";
			$response['num_errors'] = 1;
			$response['errors_log']['log'][] = $log;
			$response['errors_log']['log_detail'] = $log_error;
		}
		return $response;

		// $cek_bebas_teori = $this->Api_sia->cek_bebas_teori($nim);
		// if($cek_bebas_teori[0]['STATUS'] == 1){
		// 	$isibebasteori 		= "<br/>- Lulus <br/>
		// 					 - ".$jumnilaiCmin." Matakuliah<br/>
		// 					 - ".$jumnilaiE." Matakuliah<br/>
		// 					 - ".$ipkk."<br/>
		// 					 Keterangan = <br/>
		// 					 Lulus = ".$jumsksw." SKS MK Wajib<br/>
		// 					 Minimun Lulus = ".$totskswajib." SKS MK Wajib<br/>
		// 					Bebas Teori oleh <b>".$cek_bebas_teori[0]['NIP']."</b> pada tanggal <b>".str_replace('-','/',$cek_bebas_teori[0]['TANGGAL_F'])." WIB</b>
		// 				";
		// 	$statusbebasteori = "<span class='badge badge-success'><i class='icon-white icon-ok'></i></span>";
		// 	$cekbt = true;
		// } else {
		// 	$isibebasteori = 'Silakan cek bebas teori dengan klik tombol <b>CEK</b> pada bagian <b>Status</b>';
		// 	$statusbebasteori = "<span id='spncek' style='cursor:pointer;' class='badge badge-warning'>CEK</span>";
		// 	$cekbt = false;
		// }		
	}

### **************************** deprecated *********************
	/**
	* Menentukan permohonan surat lanjut atau berhenti (verifikasi / validasi)
	* data balikan digunakan untuk di-mapping ke pengisian form
	*/
	public function pengecekan_persyaratan($data_persyaratan=array(), $kd_jenis_sakad=null, $nim=null)
	{
		# 1. CEK VALUES KOLOM ASAL_API PADA ARRAY PERSYARATAN
		$asal_api_unq = seleksi_array_duplikat( array_column($data_persyaratan, 'ASAL_API') );

		return $asal_api_unq;

		# 2. LOOPING KE WEBSERVICE TERKAIT (Pencarian data mhs)
	    /*$datas_from_webservice = [];
	    foreach ($asal_api_unq as $req) {
        	$datas_from_webservice[$req] = $this->remote_service_untuk_sakad($req, array('NIM'=>$temp_nim));
        }
		*/
	}

	/**
	* Mengarahkan panggilan API Webservice sesuai kode asal api
	* PENGGUNAAN :
	* - verifikasi permohonan buat surat akademik
	* - automasi form buat surat akademik
	*/
	public function remote_service_untuk_sakad($kd_asal_api, $params=array()){
		$arr_hasil = ['error'=>'gagal mendapatkan resource dari sumber data'];
		switch ($kd_asal_api) {
			case 'sia_mhs_dpm': #PROFIL MAHASISWA DPM
				$parameter	= array('api_kode'=>26000, 'api_subkode' => 12, 'api_search' => array($params['NIM']));
				$arr_hasil	= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);
				//$arr_hasil = $arr_hasil[0];
				/*$arr_hasil['meta'] = [
					'kd_asal_api' => 'SIA_MHS',
					'title'	=> 'Profil Mahasiswa Lengkap'];*/
				//$arr_hasil['meta']	= 'SIA_MHS';
				//$arr_hasil = $this->dummydata->dummy_get_mhs_sia( (int) $params['NIM'] )[0];

				break;
			case 'sia_mhs_kumu': #Profil Mahasiswa + IP Kumulatif
				break;
			case 'sia_nilai': #data nilai MAHASISWA
				# MENUNGGU INFO DARI PTIPD
				/*$arr_hasil['meta'] = [
					'kd_asal_api' => 'SIA_NILAI',
					'title' => 'Rangkuman Nilai Mahasiswa'
				];*/
				//$arr_hasil['meta'] = 'SIA_NILAI';
				$arr_hasil = [
					'IPK'	=> '2.99',
					'JUMLAH_NILAI_BAWAH_C' => 3,
					'JUMLAH_NILAI_E' => 0
				];

				break;
			case 'sia_makul': #data makul semester ini

				break;
			case 'tnde_riwayat': #riwayat persuratan mhs
				# MENUNGGU BUTUH
				break;

			default:
				$arr_hasil = ['errors'=> array(
					'kode_error' => '400', #kode asal api tidak dikenali
					'pesan_error'=> 'Insufficient data'
				)];
				break;
		}

		return $arr_hasil;
	}

}