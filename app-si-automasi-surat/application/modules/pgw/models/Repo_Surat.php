<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Repo_Surat extends CI_Model
{
	public function __construct()
	{
		parent::__construct();           
        //$this->load->model('mdl_tnde','apiconn'); #obj apiconn dimuatkan controller
	}

	public function persyaratan($kd_jenis_sakad, $opsi='kolom')
	{		
		$parameter = array('api_kode' => 14001, 'api_subkode' => 1, 'api_search' => array($kd_jenis_sakad));
		$kolom_syarat = $this->apiconn->api_tnde_sakad('Tnde_surat_akademik/get_persyaratan_verifikasi', 'json', 'POST', $parameter);
		return $kolom_syarat;
	}
	
}