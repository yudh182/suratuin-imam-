<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Test_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();                       
	}

	/**
	* Percobaan mengakses model lain tanpa dimuat
	* hasil : tergantung apakah model yang dipanggil juga dimuat controller atau tidak
	a) jika dimuat juga oleh controller,  model langsung bisa mengkases model lain itu tanpa deklarasi load terlebih dahulu
	b) jika tidak dimuat oleh controller, model tak akan bisa mengakses model lain sebelum dideklarasikan lewat constructor
	*/
	public function akses_method_model_tetangga()
	{
		/**
		* secara default tidak akan bisa mengakses method-method model lain jika tidak diload
		* adapun kalaupun bisa, kalau bisa mengakses tanpa load
		itu diakibatkan model ini juga dimuatkan oleh controller
		*/
		return $this->Automasi_model->get_config_automasi('1', 'UN02006');
	}


	public function get_profil_mhs($nim=null,$opsi=null)
    {
    	if (!empty($nim) || $nim !== null){
	    	switch ($opsi) {
	    		case 'dpm': 
	    			$parameter	= array('api_kode'=>26000, 'api_subkode' => 12, 'api_search' => array($nim) ); 
					$api_get	= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter); //sudah diconvert dari json ke array
	    			break;
	    		case 'kumulatif':
	    			$parameter   = array('api_kode'=>26000, 'api_subkode' => 10, 'api_search' => array($nim) );     			
	    			$api_get	= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter); //sudah 
	    			break;    		
	    		
	    		default:  	    			
					$api_get	= ['error'=>array('message'=>'parameter opsi tidak dikenali')];
	    			break;
	    	}	
    	} else {
    		$api_get = ['error'=>array('type'=>400,'msg'=>'paramater tidak lengkap')];
    	}
    		
		return $api_get;
    }

}