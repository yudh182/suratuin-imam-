<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Repo_Automasi_Surat extends CI_Model
{
	public function __construct()
	{
		parent::__construct();           
        //$this->load->model('common/Mdl_tnde','apiconn'); #obj apiconn dimuatkan controller
	}

	public function get_jenis_surat_mahasiswa($opsi_by='kd', $input=null)
	{
		if ($opsi_by == 'kd'){
			$parameter =array('api_kode'		=> 13002, 'api_subkode'	=> 1, 'api_search'	=> array($input));
		} elseif ($opsi_by == 'path') {
			$parameter =array('api_kode'		=> 13002, 'api_subkode'	=> 2, 'api_search'	=> array($input));
		} elseif ($opsi_by == 'status_aktif') {
			$parameter =array('api_kode'		=> 13001, 'api_subkode'	=> 2, 'api_search'	=> array());
		} elseif ($opsi_by == 'semua') {
			$parameter =array('api_kode'		=> 13001, 'api_subkode'	=> 1, 'api_search'	=> array());
		} else {
			return false;
		}
		
		$jenis_sakad = $this->apiconn->api_tnde_sakad('tnde_master_mix/get_jenis_surat_akademik', 'json', 'POST', $parameter);
		return $jenis_sakad;
	}

}