<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');
/**
* HTTP Controller Surat Akademik untuk user mahasiswa
* dependency modules :
* - surat_keluar
* - admin_master
* - repo_sisurat
*/
use \Firebase\JWT\JWT;
class Automasi_surat_mhs extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('log') !== 'in')
			redirect(site_url('pgw/login?from='.current_url()));

		//$this->load->model('Mdl_tnde', 'apiconn'); #autoload
		$this->load->helper('form');
		$this->load->helper('Tnde_sakad');
		#$this->load->model('Repo_Surat');
		$this->load->model('Logic_Verifikasi');
		$this->load->model('Verifikasi_model');
		$this->load->model('Automasi_model');
		$this->load->module('pgw/api/Api_foto','api_foto');
		//$this->load->module('pgw/Act_surat_keluar_mhs','lib_surat_keluar_mhs');

		// $cek_sesi = $this->Automasi_model->sesi_by_kd_user($this->session->userdata('username'));

		// //exit(var_dump($cek_sesi));
		// if (!empty($cek_sesi)){
		// 	$this->session->set_userdata('sesi_sakad',$cek_sesi);
		// } else {
		// 	$this->session->unset_userdata('sesi_sakad');
		// }
    }

    /**
    * Menangani route method untuk penamaan custom  dalam controller
    */
	public function _remap($method, $params = array())
	{
		/*
		if ($method === 'home'){
            $this->index();
        } elseif ($method === 'cetak') {
        	$this->buat();
        } elseif ($method === 'print') {
        	$this->proses_print_surat();
        } else {
            $this->$method();
        } */

        if (method_exists($this, $method))
        {
        	return call_user_func_array(array($this, $method), $params);
        } else {
        	$act_method = 'action_'.$method;
        	if ($method === 'home'){
        		return call_user_func_array(array($this, 'index'), $params);
        	} elseif ($method === 'cetak') {
        		return call_user_func_array(array($this, 'buat'), $params);
        	} elseif ($method === 'verifikasi') {
        		return call_user_func_array(array($this, 'action_verifikasi'), $params);
        	} elseif ($method === 'print') {
        		return call_user_func_array(array($this, 'action_print_surat'), $params);
        	}
        	/*else {
        		return call_user_func_array(array($this, $act_method), $params);
        	}*/
        }

        show_404();
	}

	public function index()
	{
		$parameter =array('api_kode'		=> 13001, 'api_subkode'	=> 2, 'api_search'	=> array());
		$data['list_jenis_sakad'] = $this->apiconn->api_tnde_sakad('tnde_master_mix/get_jenis_surat_akademik', 'json', 'POST', $parameter); # DAFTAR JENIS SURAT
		if ($this->session->userdata('grup') == 'mhs'){
			$view = 'v_mhs_home';
		} elseif ($this->session->userdata('grup') == 'pgw') {
			$view = 'v_pgw_home';
		}

		$this->load->view('templates/v_header');
		$this->load->view('templates/v_sidebar',$data);
		$this->load->view($view);
		$this->load->view('templates/v_footer');
	}

	public function buat($page='select_multistep')
	{
		$parameter =array('api_kode'		=> 13001, 'api_subkode'	=> 2, 'api_search'	=> array());
		$data['list_jenis_sakad'] = $this->apiconn->api_tnde_sakad('tnde_master_mix/get_jenis_surat_akademik', 'json', 'POST', $parameter); # DAFTAR JENIS SURAT AKADEMIK (AKTIF)
		$data['cek_sesi'] = $this->Automasi_model->sesi_by_kd_user($this->session->userdata('username'));

		//exit(print_r($data));

		$list_urlpath = array_column($data['list_jenis_sakad'], 'URLPATH'); #daftar url path
		$current_urlpath = $this->uri->segment(4);
		//$page = $current_urlpath;

		if ($page == 'select_multistep'){ # Versi Multistep, select option daftar surat
			$view = 'v_buat';
		} elseif (in_array($current_urlpath, $list_urlpath)) { # Versi direct link jenis surat by path
			#pilih array jenis sakad (berdasar path)
			$key_terpilih = array_search($current_urlpath, $list_urlpath);
			$data['jenis_sakad'] = $data['list_jenis_sakad'][$key_terpilih];

			$verified = array('masih_kul','ijin_pen', 'ijin_pen_makul', 'ijin_studi_pen', 'ijin_obs', 'perm_KP', 'hbs_teori', 'tdk_men_bea', 'ket_lulus','kel_baik', 'pndh_studi'); #jenis surat yg sudah bisa memverifikasi
			if (in_array($current_urlpath, $verified)){
		    	$param_jenis_sakad = $data['jenis_sakad'];
		    	$param_nim = $this->session->userdata('username');

		    	if (empty($data['cek_sesi'])){
					$crosscheck_syarat = $this->Verifikasi_model->cek_persyaratan($param_jenis_sakad, $param_nim);
			    	$data['hasil_verifikasi'] = $crosscheck_syarat;

			    	if ($data['hasil_verifikasi']['hasil_cek_mhs']['num_errors'] == 0){
			    		if (!empty($data['hasil_verifikasi']['tmp_verified_mhs'])){
			    			$this->session->set_userdata('tmp_verified_mhs',$data['hasil_verifikasi']['tmp_verified_mhs']);
			    		}

			    		if (!empty($data['hasil_verifikasi']['tmp_isi_detail'])){
			    			$this->session->set_userdata('tmp_verified_detail_skr',$data['hasil_verifikasi']['tmp_isi_detail']);
			    		}
			    		$v2_view = array('masih_kul','ijin_pen_makul');
			    		if (in_array($current_urlpath, $v2_view)){
			    			$view_form = 'v_buatsurat/v2/v_'.$current_urlpath;
			    		} else {
			    			$view_form = 'v_buatsurat/v_'.$current_urlpath;
			    		}
			    		$data['_view_form'] = $this->load->view($view_form, $data, true);
					}
		    	}

				$view = 'v_buat_verified';
			} else {
				$data['resp_persyaratan'] = $this->action_verifikasi(
				array(
						'urlpath'	=> $current_urlpath,
						'nim'=> $this->session->userdata('username'),
						'log'=> $this->session->userdata('username')
					)
				);
				$view = 'v_daftar_syarat';
				$view_form = 'v_buatsurat/v_'.$current_urlpath;
				$data['_view_form'] = $this->load->view($view_form, $data, true);
			}
		}
		
		$this->load->view('templates/v_header');
		$this->load->view('templates/v_sidebar', $data);
		$this->load->view($view,$data);
		$this->load->view('templates/v_footer');
	}	

	public function detail_surat($id_surat_keluar=null)
	{
		$data['title'] = 'Detail Surat';

		## 1. API GET SURAT KELUAR + PENERIMA
		$data['surat_keluar'] = $this->Automasi_model->get_surat_keluar($id_surat_keluar);

		## 2. API GET DETAIL SURAT (AUTOMASI)
		$data['detail_surat_automasi'] = $this->Automasi_model->get_detail_skr_automasi($id_surat_keluar);
		exit(var_dump($data));

		## 3. API GET ke SIMPEG untuk mengambil data pelengkap
		#3.1 unit simpeg
		$parameter = array('api_kode' => 1901, 'api_subkode' => 2, 'api_search' => array($data['surat_keluar']['umum']['TGL_SURAT'], $data['surat_keluar']['umum']['UNIT_ID']));
		$data['unit'] = $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);

		#3.2 penandatanganan tnde
		$parameter							= array('api_kode' => 4004, 'api_subkode' => 1, 'api_search' => array($data['surat_keluar']['umum']['KD_JABATAN_2']));
		$data['penandatangan_surat']	= $this->apiconn->api_tnde('tnde_pegawai/pegawai_by_kd_jabatan', 'json', 'POST', $parameter);

		$psd = $this->apiconn->api_tnde('tnde_general/get_data', 'json', 'POST', array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('MD_PSD', 'ID_PSD', $data['surat_keluar']['umum']['ID_PSD'])));


		$this->load->view('templates/v_header');
		$this->load->view('templates/v_sidebar', $data);
		$this->load->view('v_suratsaya/v_detail_surat', $data);
		$this->load->view('templates/v_footer');
	}

	public function riwayat()#khusus mahasiswa, nanti dipindah ke module mhs
	{
		$nim_user = $this->session->userdata('username');
		$data['header_surat_keluar'] = $this->Automasi_model->get_riwayat($nim_user); //dapatkan riwayat pembuatan surat melalui sistem automasi (bukan SISURAT)

		//$kepada = $this->apiconn->api_tnde('tnde_surat_keluar/get_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($id, 'PS')));
		//$data['kepada'] = $kepada;
		//echo '<br><br><h1>Data Distribusi Penerima</h1><br><br>';
		//echo print_r($data['kepada']);

		$this->load->view('templates/v_header');
		$this->load->view('templates/v_sidebar', $data);
		$this->load->view('v_mhs_riwayat', $data);
		$this->load->view('templates/v_footer');
	}

	#HAPUS SURAT DRAF (DENGAN NOMOR) YANG DIBUAT SELAMA TESTING
	public function hapus_surat_keluar_lengkap()
	{
		$del = $this->input->get('id_surat');

		if (!empty($del)){
			$api_del = $this->Automasi_model->delete_surat_keluar_automasi($del);

			echo json_encode($api_del, JSON_PRETTY_PRINT);
		} else {
			echo json_encode(['error_message'=>'perintah tidak dikenali']);
		}
	}

	public function hapus_detail_surat_mahasiswa($id)
	{
		$api_del = $this->Automasi_model->delete_detail_skr_automasi($id);

		if ($api_del){
			$data['status'] = $api_del;
			$data['success'][] = "Berhasil menghapus data detail surat keluar dari basisdata automasi";
		} else {
			$data['status'] = $api_del;
			$data['errors'][] = "Gagal menghapus data detail surat keluar dari basisdata automasi";
		}

		echo json_encode($data, JSON_PRETTY_PRINT);
	}


    /**
    * Aksi Simpan atau Perbarui Sesi Versi 2
    * Perbaikan aliran data dari verifikasi ke cetak dengan pemanfaatan CI_Session,
    * mengurangi jumlah request API.  mengurangi jumlah hidden field input
	* @param array form fields
	* @return view html
    */
    public function aksi_simpan_perbarui_sesi($paramdata=array())
    {
    	$this->load->library('form_validation');

    	#1. TANGKAP DATA FORM dan SESSION
    	$sess_detail = $this->session->userdata('tmp_verified_detail_skr');
    	$sess_mhs = $this->session->userdata('tmp_verified_mhs');
    	$kd_jenis_sakad_int = (int)$sess_detail['KD_JENIS_SAKAD'];
    	$grup_jenis_sakad = (int)$sess_detail['GRUP_JENIS_SAKAD'];
    	$kd_jenis_surat_induk = (int)$sess_detail['KD_JENIS_SURAT'];
    	$unit_id = $sess_mhs['UNIT_ID'];
    	$data['pengisian_form'] = $_POST;
    	$id_sesi = $this->input->post('id_sesi_buat');

		if ($this->session->userdata('log') == 'in' && $this->input->post('save_sesi') == 'ok'){

	    	#2. Validasi form per jenis surat mahasiswa & format array isian_form
			switch ($kd_jenis_sakad_int) {
				case 1: #Masih Kuliah
					$config = array(array('field' => 'keperluan','label' => 'Keperluan','rules' => 'required'));
					//petakan variabel-variabel $_POST ke format array yang dapat diterima API tnde_surat_akademik/insert_detail_skr_automasi

					$form_ke_grup = [
						'KEPERLUAN' => $this->input->post('keperluan'),
						'CONFIG_MASA_BERLAKU' => 'P5M' //5 BULAN
					];
					break;
				case 2: #Berkelakuan baik
					$config = array(array('field' => 'keperluan','label' => 'Keperluan','rules' => 'required'));

					$form_ke_grup = [
						'KEPERLUAN' => $this->input->post('keperluan'),
						'CONFIG_MASA_BERLAKU' => 'P5M' //5 BULAN
					];
					break;
				case 5: #Pindah Studi
					$config = array(
						array('field' => 'opsi_pindah','label' => 'Opsi Pindah','rules' => 'required'),
						array('field' => 'catatan','label' => 'Catatan','rules' => 'required')
					);
					$opsi_pindah = $this->input->post('opsi_pindah');
					$catatan = $this->input->post('catatan');
					if  ($opsi_pindah == '1'){
						$keperluan = "pengajuan pindah studi ke Program Studi ".$catatan;
					} elseif ($opsi_pindah == '2') {
						$keperluan = "meneruskan studi di Perguruan Tinggi lain";
					} else {
						$keperluan = 'kosong';
					}
					$form_ke_grup = [
						'KEPERLUAN' =>$keperluan,
						'CONFIG_MASA_BERLAKU' => 'P1M' //1 BULAN
					];
					break;
				case 7: #ijin penelitian
					$config = array(
						array('field' => 'judul_penelitian','label' => 'Judul Penelitian','rules' => 'required'),
				        //array('field' => 'instansi_tujuan','label' => 'Instansi Tujuan','rules' => 'required'),
				        array('field' => 'mtd_penelitian','label' => 'Metode Penelitian','rules' => 'required')
					);

					#memetakan variabel-variabel $_POST hasil inputan form ke bentuk array-nya data_umum_sk SISURAT
					$form_ke_umum = [
						'ISI_SURAT' => $this->input->post('keperluan'),
						'PENERIMA_EKS' => [$this->input->post('kepada_penerima_eks').'#0'
						],
						//'INSTANSI_TUJUAN' => $this->input->post('intansi_tujuan'),
						'TEMPAT_TUJUAN' => $this->input->post('tempat_tujuan')
					];

					#memetakan/assign variabel-variabel $_POST hasil inputan form ke bentuk array-nya grup_automasi
					$form_ke_grup = [
						'KEPERLUAN' => $sess_detail['KEPERLUAN'],
						'JUDUL_DLM_KEGIATAN' => $this->input->post('judul_penelitian'),
						//'SUBYEK_DLM_KEGIATAN' => null,
						//'OBYEK_DLM_KEGIATAN' => null,
						//'TEMA_DLM_KEGIATAN' => null,
						'METODE_DLM_KEGIATAN' => $this->input->post('mtd_penelitian'),
						'TEMPAT_KEGIATAN' => $this->input->post('tmpt_penelitian'),
						'TGL_MULAI' => $this->input->post('tgl_mulai'),
						'TGL_BERAKHIR' => $this->input->post('tgl_berakhir'),
						'CONFIG_MASA_BERLAKU' => 'P1M' //1 BULAN
						//'MATA_KULIAH_TERKAIT' => null
					];

				break;
				case 11: #ijin penelitian makul
					$kpd_mahasiswa 	= explode("<$>", $this->input->post('kpd_mahasiswa'));
					$config = array(
						array('field' => 'pilih_makul','label' => 'Pilih Mata Kuliah','rules' => 'required'),
						array('field' => 'kepada_penerima_eks','label' => 'Kepada Yth.','rules' => 'required'),
				        array('field' => 'instansi_tujuan','label' => 'Instansi Tujuan','rules' => 'required'),
				        array('field' => 'tgl_mulai','label' => 'Tanggal mulai','rules' => 'required'),
				    );

					$form_ke_umum = [
						//'ISI_SURAT' => $sess_detail['KEPERLUAN'].' ke '.$this->input->post('instansi_tujuan').' terkait '.$this->input->post('topik_penelitian_makul'),
						'ISI_SURAT' =>	'DISIMPAN DI SISTEM AUTOMASI SURAT',
						'PENERIMA_EKS' => [
							$this->input->post('kepada_penerima_eks').' '.$this->input->post('instansi_tujuan').'#0'
						],
						'PENERIMA_MHS' => $this->input->post('kpd_mahasiswa'),//$kpd_mhs
						'TEMPAT_TUJUAN' => $this->input->post('alamat_tujuan')
					];
				    $form_ke_grup = [
				    	//'KEPERLUAN' => $sess_detail['KEPERLUAN'],
						'MATA_KULIAH_TERKAIT' => $this->input->post('pilih_makul'),
						'TEMPAT_KEGIATAN' => $this->input->post('instansi_tujuan'),
						'TEMA_DLM_KEGIATAN' => $this->input->post('topik_penelitian_makul'),
						'TGL_MULAI' => $this->input->post('tgl_mulai'),
						'TGL_BERAKHIR' => null,
						'CONFIG_MASA_BERLAKU' => 'P1M' //1 BULAN
						//'MATA_KULIAH_TERKAIT' => null
					];
				break;
				case 8: #ijin observasi
					$kpd_mahasiswa 	= explode("<$>", $this->input->post('kpd_mahasiswa'));
					$config = array(
						array('field' => 'pilih_makul','label' => 'Pilih Mata Kuliah','rules' => 'required'),
						array('field' => 'kepada_penerima_eks','label' => 'Kepada Yth.','rules' => 'required'),
				        array('field' => 'instansi_tujuan','label' => 'Instansi Tujuan','rules' => 'required')
				    );

					// $arr_kpd_mhs = explode("<$>", $this->input->post('kpd_mahasiswa'));
					// $kpd_mhs = [];
					// foreach ($arr_kpd_mhs as $key => $val) {
					// 	$val = explode('#', $val);
					// 	$kpd_mhs[$key] = $val[0].'#'.$val[1];
					// }
					// $kpd_mhs = implode('<$>', $kpd_mhs);
					//exit(var_dump($kpd_mhs));

					$form_ke_umum = [
						'ISI_SURAT' => $sess_detail['KEPERLUAN'].' ke '.$this->input->post('instansi_tujuan').' terkait '.$this->input->post('topik_observasi'),
						'PENERIMA_EKS' => [
							$this->input->post('kepada_penerima_eks').' '.$this->input->post('instansi_tujuan').'#0'
						],
						'PENERIMA_MHS' => $this->input->post('kpd_mahasiswa'),//$kpd_mhs
						'TEMPAT_TUJUAN' => $this->input->post('alamat_tujuan')
					];
				    $form_ke_grup = [
				    	'KEPERLUAN' => $sess_detail['KEPERLUAN'],
						'MATA_KULIAH_TERKAIT' => $this->input->post('pilih_makul'),
						'TEMPAT_KEGIATAN' => $this->input->post('instansi_tujuan'),
						'TEMA_DLM_KEGIATAN' => $this->input->post('topik_observasi'),
						'TGL_MULAI' => $this->input->post('tgl_mulai'),
						'TGL_BERAKHIR' => $this->input->post('tgl_mulai'),
						'CONFIG_MASA_BERLAKU' => 'P1M' //1 BULAN
						//'MATA_KULIAH_TERKAIT' => null
					];

					break;
				case 9: #permohonan KP
					$config = array(
						array('field' => 'kepada_penerima_eks','label' => 'Kepada Yth.','rules' => 'required'),
				        array('field' => 'instansi_tujuan','label' => 'Instansi Tujuan','rules' => 'required'),
				         array('field' => 'minat','label' => 'Bidang Minat','rules' => 'required'),
				        array('field' => 'tgl_mulai','label' => 'Tanggal Mulai','rules' => 'required'),
				        array('field' => 'tgl_berakhir','label' => 'Tanggal Berakhir','rules' => 'required'),
				    );
					$form_ke_umum = [

						'ISI_SURAT' => $sess_detail['KEPERLUAN'],
						'PENERIMA_EKS' => [
							$this->input->post('kepada_penerima_eks').' '.$this->input->post('instansi_tujuan').'#0'
						],
						'PENERIMA_MHS' => $this->input->post('kpd_mahasiswa'),//$kpd_mhs
						'TEMPAT_TUJUAN' => $this->input->post('tempat_tujuan')
					];

				    $form_ke_grup = [
				    	'KEPERLUAN' => $sess_detail['KEPERLUAN'],
						'MATA_KULIAH_TERKAIT' => $sess_detail['MATA_KULIAH_TERKAIT'],
						'TEMPAT_KEGIATAN' => $this->input->post('instansi_tujuan'),
						'TEMA_DLM_KEGIATAN' => $this->input->post('minat'),
						'TGL_MULAI' => $this->input->post('tgl_mulai'),
						'TGL_BERAKHIR' => $this->input->post('tgl_mulai'),
						'CONFIG_MASA_BERLAKU' => 'P3M' //1 BULAN
					];
					break;
				case 10: #studi pendahuluan
					$config = array(
						array('field' => 'tema','label' => 'Tema','rules' => 'required'),
				        array('field' => 'instansi_tujuan','label' => 'Instansi Tujuan','rules' => 'required'),
				        array('field' => 'kepada_penerima_eks','label' => 'Kepada Yth.','rules' => 'required')
					);
					$form_ke_umum = [
						'ISI_SURAT' => $this->input->post('keperluan'),
						'PENERIMA_EKS' => [
							$this->input->post('kepada_penerima_eks').' '.$this->input->post('instansi_tujuan').'#0'
						],
						'TEMPAT_TUJUAN' => $this->input->post('alamat_tujuan')
					];

					#memetakan/assign variabel-variabel $_POST hasil inputan form ke bentuk array-nya grup_automasi
					$form_ke_grup = [
						'KEPERLUAN' => $sess_detail['KEPERLUAN'],
						'TEMA_DLM_KEGIATAN' => $this->input->post('tema'),
						'TEMPAT_KEGIATAN' => $this->input->post('instansi_tujuan'),
						'TGL_MULAI' => $this->input->post('tgl_mulai'),
						'TGL_BERAKHIR' => $this->input->post('tgl_mulai'),
						'CONFIG_MASA_BERLAKU' => 'P1M' //1 BULAN
						//'MATA_KULIAH_TERKAIT' => null
					];

					break;
				case 4: #tidak sedang menerima beasiswa
					$config = array(
						array('field' => 'keperluan','label' => 'Keperluan','rules' => 'required'),
					);
					$form_ke_umum = [
						//'PENERIMA_MHS' => $this->session->userdata('username').'#0',
						'ISI_SURAT' => $this->input->post('keperluan')
					];

					$bpd_nama               = $this->input->post('nm_beasiswa_pd');
			        $bpd_keterangan         = $this->input->post('ket_beasiswa_pd');
			        $tot = count($bpd_nama);
			        $no = 0;
			        if(!empty($bpd_nama)){
			            $bea_pd = [];
			            foreach($bpd_nama as $n => $val){
			                $no++;

			                $item_bpd_nama = $bpd_nama[$n];
			                if(!empty($item_bpd_nama)){
			                    $bea_pd[$n]['NAMA_BEASISWA'] = $bpd_nama[$n];
			                    $bea_pd[$n]['KETERANGAN_BEASISWA'] = $bpd_keterangan[$n];
			                    $bea_pd[$n]['NO_URUT']               = $no;
			                }
			            }
			        }
			        $beasiswa_pernah_diikuti = array_values($bea_pd); //re-order index key
					$form_ke_grup = [
						'KEPERLUAN' => $this->input->post('keperluan'),
						'DATA_TAMBAHAN' => $beasiswa_pernah_diikuti, //serialized (meta, data)
						'CONFIG_MASA_BERLAKU' => 'P2M' //1 BULAN
					];

				break;
				default:

					break;
			}
			$this->form_validation->set_message('required', 'kolom {field} wajib diisi');
			$this->form_validation->set_rules($config);
			$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');

			if ($this->form_validation->run()){#pastikan form terisi semua
				# 4.1 SIAPKAN DATA AWAL SESI
	    		$dt = new DateTime();
				$data_umum = array(
					'ID_SESI' => null, //sementara di-null
	                'KD_JENIS_SAKAD' => (int)$kd_jenis_sakad_int,
	                //'KD_GRUP_JENIS_SAKAD' => $grup_jenis_sakad,
	                'KD_JENIS_SURAT' => $kd_jenis_surat_induk,
	                'PEMBUAT_SURAT'	=> $this->session->userdata('username'),
	                'KD_JENIS_ORANG'		=> "M", //mahasiswa
	                'TEMPAT_DIBUAT'		=> "34712", //kodya yogyakarta
	                'KD_STATUS_SIMPAN'	=> "1", //DRAF TANPA NOMOR
	                'KD_SISTEM'			=> "AUTOMASI",
	                'WAKTU_SIMPAN_AWAL' => $dt->format('Y-m-d H:i:s')
		       	);

				# 3. SIAPKAN DATA SESI_BUAT sesuai config automasi
				$data_automasi = $this->Automasi_model->get_config_automasi($kd_jenis_sakad_int,$unit_id);

			 #SESI versi SESSION
				$data['conf_automasi'] = $data_automasi[0];

				$data['detail_skr_automasi'] = array_merge($sess_detail, $form_ke_grup); //$data['detail_skr_automasi'] =  array_diff_key($data['detail_skr_automasi'], array_flip(['id_sesi_buat', 'save_sesi', 'phpsess', 'KD_JENIS_SURAT', 'PATH_JENIS_SAKAD'])); //$data['detail_skr_automasi'] = array_change_key_case($data['detail_skr_automasi'], CASE_UPPER);

				//TEST API INSERT DARI SESI KE DETAIL_SKR_AUTOMASI
				//$data['detail_skr_automasi']['ID_SURAT_KELUAR'] = 'T.F.'.date('h-i-s');
				//$data['detail_skr_automasi']['TGL_SURAT'] = date('Y-m-d');
				$data['detail_skr_automasi']['PEMBUAT_SURAT'] = $this->session->userdata('username');

				/*$parameter	= array('api_kode'=>14004, 'api_subkode' => 1, 'api_search' => array($data['detail_skr_automasi']));
				$data['insert_detail_sakad'] = $this->apiconn->api_tnde_sakad('tnde_surat_akademik/insert_detail_skr_automasi', 'json', 'POST', $parameter);*/
				/*$response = $this->load->view('v_cetak_wpreview',$data, true);
				$this->output->set_status_header(200)
				       		->set_content_type('text/html')
				       		->set_output($response);
				*/


			 #SESI versi API (DB)
				$data_sesi = array_merge($data_umum, $data_automasi[0]); //bakal data sesi_buat_sakad

			 # *** modif dan atau hapus beberpa kolom (yang tidak dibutuhkan tabel SESI_BUAT_SAKAD)
			 	$data_sesi['PERIHAL'] = isset($data_sesi['PERIHAL']) ? ucwords(strtolower($data_sesi['PERIHAL'])).' '.$sess_mhs['NAMA'] : NULL;
				$data_sesi =  array_diff_key($data_sesi, array_flip(['ID', 'FRASA_BUKA', 'FRASA_TUTUP', 'WAKTU_SIMPAN'])); //hapus kolom tidak dibutuhkan

				$data_sesi['DETAIL_SURAT'] = isset($data['detail_skr_automasi']) ? serialize($data['detail_skr_automasi']) : null;

				if (!empty($form_ke_umum['PENERIMA_MHS'])){ #cek adakah mahasiswa diinputkan lewat form
					$arr_mhs = explode("<$>", $form_ke_umum['PENERIMA_MHS']);

					$data_sesi['DETAIL_MHS'] = [];
					$data_sesi['D_PENERIMA_MHS'] = [];
					foreach ($arr_mhs as $key => $val) {
						$val = explode('#', $val);

						$data_sesi['D_PENERIMA_MHS'][$key] = $val[0].'#'.$val[1];

						$data_sesi['DETAIL_MHS'][$key]['NIM'] = $val[0];
						$data_sesi['DETAIL_MHS'][$key]['NAMA'] = $val[4];
					}
					$data_sesi['D_PENERIMA_MHS'] = implode('<$>', $data_sesi['D_PENERIMA_MHS']);
					$data_sesi['DETAIL_MHS'] = serialize($data_sesi['DETAIL_MHS']);
				} else {
					$data_sesi['D_PENERIMA_MHS'] = $sess_mhs['NIM'].'#0';
					$data_sesi['DETAIL_MHS'] = isset($sess_mhs) ? serialize($sess_mhs) : null;
				}

				#SETTING PENERIMA MAHASISWA, PENERIMA EKSTERNAL
				//$data_sesi['D_PENERIMA_MHS'] = isset($form_ke_umum['PENERIMA_MHS']) ? implode('<$>', $form_ke_umum['PENERIMA_MHS']) : null;
				//$data_sesi['D_PENERIMA_MHS'] = isset($form_ke_umum['PENERIMA_MHS']) ? $form_ke_umum['PENERIMA_MHS'] : null;
				$data_sesi['D_PENERIMA_EKS'] = isset($form_ke_umum['PENERIMA_EKS']) ? implode('<$>', $form_ke_umum['PENERIMA_EKS']) : null;
				$data_sesi['TEMPAT_TUJUAN'] = isset($form_ke_umum['TEMPAT_TUJUAN']) ? $form_ke_umum['TEMPAT_TUJUAN'] : null;
				$data_sesi['ISI_SURAT'] = isset($form_ke_umum['ISI_SURAT']) ? $form_ke_umum['ISI_SURAT'] : 'isi surat tersimpan di sistem automasi';

				$data['insert_surat_keluar'] = $data_sesi;


			 # *** selesai modif	kolom

				if ($id_sesi == 'new' OR $id_sesi == '' OR $id_sesi == null){ #INSERT
					$data['insert_surat_keluar']['ID_SESI'] = 'SB'.uniqid();
					//$data['insert_surat_keluar']['DETAIL_LAINNYA'] = serialize($data['detail_skr_automasi']);
					$save_sesi = $this->Automasi_model->simpan_sesi_automasi_buat_v2($data['insert_surat_keluar']);
					if ($save_sesi['status'] == TRUE || $save_sesi['status'] == 1 || $save_sesi['status'] == '1'){
						$data['pesan'] = $save_sesi['success_message'];
						$data['sesi_cetak'] = $save_sesi['data'];
						$response = $this->load->view('v_cetak_wpreview',$data, true);
						$this->output
							->set_status_header(200)
							->set_header('X-ID-SESI: '.$data['sesi_cetak']['ID_SESI'])
							->set_content_type('text/html')
							->set_output($response);
					} else { #JIKA INSERT GAGAL KARENA KESALAHAN DI SERVICE
						$this->output
							->set_status_header(501) //gagal simpan karena kesalahan program di server
							->set_content_type('text/html')
							->set_output(print_r($save_sesi));
					}
				}	elseif (!empty($id_sesi)){ #UPDATE. asumsi : memiliki id sesi yang benar
					$update_sesi = $this->Automasi_model->perbarui_sesi_automasi_buat($id_sesi, $data['insert_surat_keluar']);

					if ($update_sesi['status'] == TRUE || $update_sesi['status'] == 1 || $update_sesi['status'] == '1'){ #RESPONSE HTML JIKA INSERT BERHASIL
						$data['pesan'] = $update_sesi['success_message'];
			       		$data['sesi_cetak'] = $update_sesi['data'];

			       		$response = $this->load->view('v_cetak_wpreview',$data, true);
			       		$this->output
			       			->set_status_header(200)
			       			->set_header('X-ID-SESI: '.$data['sesi_cetak']['ID_SESI'])
			       			->set_content_type('text/html')
			       			->set_output($response);
			       	} else { #JIKA UPDATE GAGAL KARENA KESALAHAN DI SERVICE
			       		$this->output
			       			->set_status_header(501) //gagal simpan karena kesalahan program di server
			       			->set_content_type('text/html')
			       			->set_output(print_r($update_sesi));
			       	}
				}

				/*
				# modif beberapa kolom
				$key_diikutkan = ['ID_SESI', 'KD_JENIS_SAKAD', 'KD_JENIS_SURAT', 'PEMBUAT_SURAT', 'KD_JENIS_ORANG','D_PENERIMA_MHS','D_PENERIMA_MHS_LAIN', 'TEMPAT_DIBUAT','KD_STATUS_SIMPAN','KD_SISTEM', 'WAKTU_SIMPAN_AWAL', 'UNIT_ID', 'KD_PRODI', 'PSD_ID', 'PSD_AN', 'PSD_UB', 'ID_KLASIFIKASI_SURAT', 'KD_SIFAT_SURAT', 'KD_KEAMANAN_SURAT', 'PERIHAL', 'LAMPIRAN', 'ISI_SURAT','TGL_SURAT', 'KD_GRUP_JENIS_SAKAD'];
				$data_to_insert = seleksi_array_by_keys($data_gabung,$key_diikutkan);
				$data_to_insert['PERIHAL'] = isset($data_to_insert['PERIHAL']) ? 'Percobaan Buat '.ucwords(strtolower($data_to_insert['PERIHAL'])).' a.n. '.$sess_mhs['NAMA'] : NULL;
				$data['insert_surat_keluar'] = $data_to_insert;
				//TEST API INSERT SESI_BUAT_SAKAD
				if ($id_sesi == 'new' OR $id_sesi == '' OR $id_sesi == null){
					$data['insert_surat_keluar']['ID_SESI'] = 'SB'.uniqid();
					$data['insert_surat_keluar']['DETAIL_LAINNYA'] = serialize($data['detail_skr_automasi']);
					$save_sesi = $this->Automasi_model->simpan_sesi_automasi_buat_v2($data['insert_surat_keluar']);
					if ($save_sesi['status'] == TRUE || $save_sesi['status'] == 1 || $save_sesi['status'] == '1'){
						$data['pesan'] = $save_sesi['success_message'];
						$data['sesi_cetak'] = $save_sesi['data'];
						$response = $this->load->view('v_cetak_wpreview',$data, true);
						$this->output
							->set_status_header(200)
							->set_header('X-ID-SESI: '.$data['sesi_cetak']['ID_SESI'])
							->set_content_type('text/html')
							->set_output($response);
					} else { #JIKA INSERT GAGAL KARENA KESALAHAN DI SERVICE
						$this->output
							->set_status_header(501) //gagal simpan karena kesalahan program di server
							->set_content_type('text/html')
							->set_output(print_r($save_sesi));
					}
				}
				*/

				# 3.0 SIAPKAN DATA (AWAL) PENERIMA DISTRIBUSI SURAT
				/*
				$kpd_pejabat 			= explode("<$>", $this->input->post('kpd_pejabat'));
				$kpd_pegawai 			= explode("<$>", $this->input->post('kpd_pegawai'));
				$kpd_mahasiswa 	= explode("<$>", $this->input->post('kpd_mahasiswa'));
				$kpd_lainnya 		= $this->input->post('kpd_lainnya');
				$ts_pejabat 			= explode("<$>", $this->input->post('ts_pejabat'));
				$ts_lainnya 			= $this->input->post('ts_lainnya');
				*/
			} else { #INPUT BELUM BENAR
			 	//$this->output->set_status_header(422)->set_content_type('text/html')->set_output(validation_errors());
			 	$data['form_validation'] = [];
			 	$data['form_validation']['success'] = false;
			 	$data['form_validation']['messages'] = [];

			 	foreach ($_POST as $key => $value) {
			 		$data['form_validation']['messages'][$key] = form_error($key);
			 	}

			 	$this->output->set_status_header(422)
			 		->set_content_type('application/json')
			 		->set_output(json_encode($data['form_validation']));
			}
		} else {
    		$this->output
	       			->set_content_type('application/json')
	       			->set_output(json_encode(array('status' => false,'message'=>'Unauthorized')));
	    }
    }

    public function aksi_hapus_sesi()
    {
    	$id_sesi = $this->input->post('id_sesi');

    	$del = $this->Automasi_model->hapus_sesi_automasi_buat($id_sesi);
    	if ($del == TRUE){
    		$this->output
		       	->set_status_header(200)
		       	->set_content_type('text/html')
		       	->set_output('item sesi berhasil dihapus');
		} else {
			if ($del == NULL){
				$sh = 422;
				$msg = 'Gagal memroses data untuk dihapus';
			} else {
				$sh = 501;
				$msg = 'Gagal menghapus data karena kegagalan di server';
			}

			$this->output
		       	->set_status_header($sh)
		       	->set_content_type('text/html')
		       	->set_output($msg);
		}
    }

    public function aksi_hapus_surat($opsi='')
    {
    	if ($opsi == 'detail_skr_automasi'){
    		$id_surat_keluar = $this->input->get('id_sklr');
    		$api_del = $this->Automasi_model->delete_detail_skr_automasi($id_surat_keluar);
    		echo print_r($api_del);
    	} elseif ($opsi == 'hapus_semua') {
    		# code...
    	}

    }

    public function aksi_preview_cetak_surat()
    {

    }


    /**
    * Aksi Simpan Surat lanjut memerintahkan browser mengunduh file cetak (Versi 2)
     * opsi : draf (1), bernomor (2)
     * proses : menyimpan data ke SURAT KELUAR (SISURAT) lewat API, insert berhasil dilanjutkan insert data HEADER SURAT KELUAR DAN D_DETAIL_SAKAD
    */
    public function aksi_simpan_cetak_surat($opsi=null) # a) preview (tanpa simpan langsung cetak), b) bernomor (simpan bernomor langsung cetak) c) simpan_saja d)kirim_perm_ttd
    {
    	$this->load->model('Repo_Simpeg');

    	if (isset($_POST['btn_save_skr_bernomor']) || isset($_POST['btn_preview_skr_bernomor'])){ #Sementara disimpan sbagai draf (testing)
    		$id_sesi_buat = $this->input->post('id_sesi_buat_cetak');
    		$save_final = $this->input->post('save_final_cetak');
    		if (isset($_POST['btn_preview_skr_bernomor'])){
	    		$opsi = 'preview';
	    	} elseif (isset($_POST['btn_save_skr_bernomor'])) {
	    		$opsi = 'bernomor';
	    	}


    		if (!empty($id_sesi_buat) && $save_final == 'ok'){
    			#1. Mengambil data umum dan penerima surat dari sesi
    			$get_sesi = $this->Automasi_model->sesi_by_id($id_sesi_buat); #mapping ke semua API INSERT
    			;
    			$data['isi_mhs'] = ''; //assign data ke variabel lain
    			if ($get_sesi == FALSE || $get_sesi == NULL){
    				exit('Mohon maaf pembuatan surat tidak dapat diselesaikan karena data sesi tidak ditemukan');
    			} else {
    				//assign semua kolom data_sesi ke array $data_sk
					$data_sk = $get_sesi;
    			}

				#2. Modif kolom-kolom untuk proses data
				$data_sk['ID_SURAT_KELUAR'] = uniqid().'p';
				$data_sk['ID_PSD'] = $get_sesi['PSD_ID'];
				$data_sk['ATAS_NAMA'] = $get_sesi['PSD_AN'];
				$data_sk['UNTUK_BELIAU'] = $get_sesi['PSD_UB'];
				$data_sk['STATUS_SK'] = 0;
				$data_sk['WAKTU_SIMPAN'] = date("d/m/Y H:i:s");
				$data_sk['TGL_SURAT'] = DateTime::createFromFormat('Y-m-d', $data_sk['TGL_SURAT'])->format('d/m/Y');

				if ($opsi == 'preview'){
					$data_sk['KD_STATUS_SIMPAN'] =  '2';

					if($data_sk['KD_STATUS_SIMPAN'] == '2' || $data_sk['KD_STATUS_SIMPAN'] == 2){ #2 BERNOMOR
						#dapatkan nomor
						// if(empty($data['errors'])){
						// 	$parameter	= array('api_kode' => 90002, 'api_subkode' => 3, 'api_search' => array($data_sk));
						// 	$req_no 		= $this->apiconn->api_tnde('tnde_surat_keluar/penomoran', 'json', 'POST', $parameter);
						// 	if(empty($req_no['ERRORS'])){
						// 		$data_sk['NO_URUT']	= (!empty($req_no['NO_URUT']) ? $req_no['NO_URUT'] : 0);
						// 		$data_sk['NO_SELA']		= (!empty($req_no['NO_SELA']) ? $req_no['NO_SELA'] : 0);
						// 		$data_sk['NO_SURAT']	= (!empty($req_no['NO_SURAT']) ? $req_no['NO_SURAT'] : 0);
						// 	}else{
						// 		$save_sk = FALSE;
						// 		$data['errors'] = $req_no['ERRORS'];
						// 	}
						// }
						$data_sk['NO_URUT']		= null;
						$data_sk['NO_SELA']		= null;
						$data_sk['NO_SURAT']	= 'belum diset';

					} else { #1 (draf)
						$data_sk['NO_URUT']		= '';
						$data_sk['NO_SELA']		= '';
						$data_sk['NO_SURAT']	= 'belum diset';
					}
				} elseif ($opsi == 'bernomor') {
					$data_sk['KD_STATUS_SIMPAN'] = '2';

					/*if($data_sk['KD_STATUS_SIMPAN'] == '2' || $data_sk['KD_STATUS_SIMPAN'] == 2){ #2 BERNOMOR
						#dapatkan nomor dan set ke no urut , no sela
						if(empty($data['errors'])){
							$parameter	= array('api_kode' => 90002, 'api_subkode' => 3, 'api_search' => array($data_sk));
							$req_no 		= $this->apiconn->api_tnde('tnde_surat_keluar/penomoran', 'json', 'POST', $parameter);
							if(empty($req_no['ERRORS'])){
								$data_sk['NO_URUT']	= (!empty($req_no['NO_URUT']) ? $req_no['NO_URUT'] : 0);
								$data_sk['NO_SELA']		= (!empty($req_no['NO_SELA']) ? $req_no['NO_SELA'] : 0);
								$data_sk['NO_SURAT']	= (!empty($req_no['NO_SURAT']) ? $req_no['NO_SURAT'] : 0);
							}else{
								$save_sk = FALSE;
								$data['errors'] = $req_no['ERRORS'];
							}
						}
					}*/
					if ($data_sk['KD_STATUS_SIMPAN'] == '2' || $data_sk['KD_STATUS_SIMPAN'] == 2) {
						#beri nomor tapi jangan set no_urut dan nomor_sela
						if(empty($data['errors'])){
							$parameter	= array('api_kode' => 90002, 'api_subkode' => 3, 'api_search' => array($data_sk));
							$req_no 		= $this->apiconn->api_tnde('tnde_surat_keluar/penomoran', 'json', 'POST', $parameter);
							if(empty($req_no['ERRORS'])){
								$data_sk['NO_URUT']	= '';
								$data_sk['NO_SELA']		= '';
								$data_sk['NO_SURAT']	= (!empty($req_no['NO_SURAT']) ? $req_no['NO_SURAT'] : '');
							}else{
								$save_sk = FALSE;
								$data['errors'] = $req_no['ERRORS'];
							}
						}
					} else { #1 (draf)
						$data_sk['NO_URUT']		= '';
						$data_sk['NO_SELA']		= '';
						$data_sk['NO_SURAT']	= '';
					}
				}

				#ubah lagi KD_STATUS_SIMPAN KE 1
				$data_sk['KD_STATUS_SIMPAN'] = '1';


				$key_diikutkan = ['ID_SURAT_KELUAR', 'KD_STATUS_SIMPAN', 'KD_JENIS_SURAT', 'ID_PSD', 'ATAS_NAMA', 'UNTUK_BELIAU', 'ID_KLASIFIKASI_SURAT', 'TGL_SURAT', 'STATUS_SK', 'PEMBUAT_SURAT', 'JABATAN_PEMBUAT', 'KD_JENIS_ORANG','UNIT_ID', 'UNTUK_UNIT', 'PERIHAL', 'TEMPAT_DIBUAT', 'TEMPAT_TUJUAN', 'KOTA_TUJUAN', 'ALAMAT_TUJUAN', 'LAMPIRAN', 'KD_SIFAT_SURAT', 'KD_KEAMANAN_SURAT', 'WAKTU_SIMPAN', 'KD_SISTEM', 'NO_URUT', 'NO_SELA', 'ISI_SURAT', 'KD_PRODI', 'NO_SURAT'];
				$data_sk = array_intersect_key($data_sk, array_flip($key_diikutkan));

				#2. Melakukan API Insert ke SISURAT	(surat_keluar) - belum aktif
				if ($opsi == 'preview'){
					$data['insert_surat_keluar'] = $data_sk; //assign data ke variabel lain
					$save_sk = TRUE;
				} elseif ($opsi == 'bernomor') {
					$save_sk = TRUE;

					if($save_sk == TRUE){
						$parameter	= array('api_kode' => 90001, 'api_subkode' => 1, 'api_search' => array($data_sk));
						$save_sk 		= $this->apiconn->api_tnde('tnde_surat_keluar/insert_surat_kaluar', 'json', 'POST', $parameter);

						//exit(var_dump($data_sk));
						if($save_sk	==	FALSE || $save_sk == NULL){
							$data['errors'][] = "Gagal menyimpan surat. #0";
							$data['insert_surat_keluar'] = FALSE;
						}else{
							$save_sk	=	TRUE;
							$data['insert_surat_keluar'] = $data_sk;
						}
					}
				}


    			#3. ======== Melakukan API INSERT PENERIMA_SK KE SISURAT ========
    			$kpd_mahasiswa = (!empty($get_sesi['D_PENERIMA_MHS']) ? explode('<$>', $get_sesi['D_PENERIMA_MHS']) : null);
    			$kpd_pejabat = (!empty($get_sesi['D_PENERIMA_PJB']) ? explode('<$>', $get_sesi['D_PENERIMA_PJB']) : null);
    			$kpd_pegawai = (!empty($get_sesi['D_PENERIMA_PGW']) ? explode('<$>', $get_sesi['D_PENERIMA_PGW']) : null);
    			$ts_pejabat = (!empty($get_sesi['D_TEMBUSAN_PJB']) ? explode('<$>', $get_sesi['D_TEMBUSAN_PJB']) : null);
    			$ts_eks = (!empty($get_sesi['D_TEMBUSAN_EKS']) ? explode('<$>', $get_sesi['D_TEMBUSAN_EKS']) : null);

    			# 2. API SISURAT - INSERT PENERIMA SURAT KELUAR
				if ($save_sk == TRUE){
	    			#============== 3.1 Pengelola Surat (PS)==============
	    			# 3.1 KIRIM SURAT KE PEJABAT (Yang menerangkan, menandatangani, dsb)
	    			if(!empty($kpd_pejabat[0])){
	    				$save_pjb = [];
						foreach($kpd_pejabat as $key=> $val){
							$exp = explode("#", $val);
							$get_pjb = $this->Repo_Simpeg->get_pejabat_aktif($data['insert_surat_keluar']['TGL_SURAT'], $exp[0]);
							if(count($exp) == 2){
								$pjb['ID_SURAT_KELUAR']				= $data_sk['ID_SURAT_KELUAR'];
								$pjb['PENERIMA_SURAT'] = $get_pjb[0]['NIP']; //NIP PEGAWAI
								$pjb['JABATAN_PENERIMA'] = $exp[0];
								$pjb['KD_GRUP_TUJUAN'] = 'PJB01';
								$pjb['KD_STATUS_DISTRIBUSI'] = "PS";
								$pjb['ID_STATUS_SM'] = 1;
								$pjb['KD_JENIS_TEMBUSAN'] = 0;
								$pjb['NO_URUT'] = $key + 1;
								$pjb['KD_JENIS_KEPADA']				= $exp[1];

								if ($opsi == 'preview'){
									$save_pjb[] = $pjb;
								} elseif ($opsi == 'bernomor') {
									$savepjb = $this->apiconn->get_api('tnde_surat_keluar/insert_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($pjb)));
									if($savepjb == FALSE){
										$data['errors'][] = "Gagal mengirim ke tujuan surat ke ".$pjb['PENERIMA_SURAT'];
										$save_pjb[] = FALSE;
									}elseif ($savepjb == TRUE) {
										$save_pjb[] = $pjb;
									}
								}

							}
						}

						$data['distribusi_ps']['penerima_pejabat'] = $save_pjb;
	    			}

	    			# 3.2 KIRIM SURAT KE MAHASISWA
	    			if(!empty($kpd_mahasiswa[0])){
	    				$save_mhs = [];
						foreach($kpd_mahasiswa as $key=> $val){
							$exp = explode("#", $val);
							if(count($exp) == 2){
								$mhs['ID_SURAT_KELUAR']				= $data_sk['ID_SURAT_KELUAR'];
								$mhs['PENERIMA_SURAT'] = $exp[0];
								$mhs['JABATAN_PENERIMA'] = '0';
								$mhs['KD_GRUP_TUJUAN'] = 'MHS01';
								$mhs['KD_STATUS_DISTRIBUSI'] = "PS";
								$mhs['ID_STATUS_SM'] = 1;
								$mhs['KD_JENIS_TEMBUSAN'] = 0;
								$mhs['NO_URUT'] = $key + 1;
								$mhs['KD_JENIS_KEPADA']				= $exp[1];


								if ($opsi == 'preview'){
									$save_mhs[] = $mhs;
								} elseif ($opsi == 'bernomor') {
									$savemhs = $this->apiconn->get_api('tnde_surat_keluar/insert_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($mhs)));
									if($savemhs == FALSE || $savemhs == NULL){
										$data['errors'][] = "Gagal mengirim ke tujuan surat ke ".$exp[0];
										$save_mhs[] = FALSE;
									}elseif ($savemhs == TRUE) {
										$save_mhs[] = $mhs;
									}
								}
								/*$save_mhs = $this->apiconn->get_api('tnde_surat_keluar/insert_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($mhs)));
								if($save_mhs == FALSE){
									$data['errors'][] = "Gagal mengirim ke tujuan surat ke ".$exp[0]; // Mahasiswa
								}*/
							}
						}

						$data['distribusi_ps']['penerima_mahasiswa'] = $save_mhs;
					}

					# 3.3 KIRIM SURAT KE EKSTERNAL
					$kpd_eks = (!empty($get_sesi['D_PENERIMA_EKS']) ? explode('<$>', $get_sesi['D_PENERIMA_EKS']) : null);
					if(!empty($kpd_eks[0])){
						$save_eks = [];
						foreach($kpd_eks as $key => $val){
							$exp = explode("#", $val);

							if(count($exp) == 2){
								$eks['ID_SURAT_KELUAR']				= $data_sk['ID_SURAT_KELUAR'];
								$eks['PENERIMA_SURAT']				= $exp[0];
								$eks['JABATAN_PENERIMA']			= $exp[0]; //misal: kepala, kabag, sekda, dsb
								$eks['KD_GRUP_TUJUAN']				= "EKS01";
								$eks['KD_STATUS_DISTRIBUSI']		= "PS";
								$eks['ID_STATUS_SM'] = 1;
								$eks['KD_JENIS_TEMBUSAN'] = 0;
								$eks['NO_URUT'] = $key + 1;
								$eks['KD_JENIS_KEPADA']				= $exp[1]; //1 = untuk perhatian , 0
								//$save_eks[] = $eks;

								if ($opsi == 'preview'){
									$save_eks[] = $eks;
								} elseif ($opsi == 'bernomor') {
									$saveeks = $this->apiconn->get_api('tnde_surat_keluar/insert_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($eks)));
									if($saveeks == FALSE || $saveeks == NULL){
										$data['errors'][] = "Gagal mengirim ke tujuan surat ke ".$exp[0];
										$save_eks[] = FALSE;
									}elseif ($saveeks == TRUE) {
										$save_eks[] = $eks;
									}
								}
								/*$save_mhs = $this->apiconn->get_api('tnde_surat_keluar/insert_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($mhs)));
								if($save_mhs == FALSE){
									$data['errors'][] = "Gagal mengirim ke tujuan surat ke ".$exp[0]; // Mahasiswa
								}*/
							}
						}

						$data['distribusi_ps']['penerima_eksternal'] = $save_eks;
					}

	    			#============== 3.2 Tembusan Surat (TS)==============
					if(!empty($ts_pejabat)){
						$ts_urut = 0;
						foreach($ts_pejabat as $key => $val){
							$exp = explode("#", $val);
							$get_pjb = $this->Repo_Simpeg->get_pejabat_aktif($data['insert_surat_keluar']['TGL_SURAT'], $exp[0]);
							if(count($exp) == 2 ){
								$ts_pjb['ID_SURAT_KELUAR'] 				= $data['insert_surat_keluar']['ID_SURAT_KELUAR'];
								$ts_pjb['PENERIMA_SURAT'] 				= $get_pjb[0]['NIP']; //NIP PEGAWAI
								$ts_pjb['JABATAN_PENERIMA'] 			= $exp[0];
								$ts_pjb['KD_GRUP_TUJUAN'] 				= 'PJB01';
								$ts_pjb['KD_STATUS_DISTRIBUSI'] 	= "TS";
								$ts_pjb['ID_STATUS_SM'] 						= "1";
								$ts_pjb['NO_URUT']									= $ts_urut + 1;
								$ts_pjb['KD_JENIS_TEMBUSAN'] 		= (string)$exp[1]; #1 = sebagai laporan, 0 = biasa
								$ts_pjb['KD_JENIS_KEPADA'] 				= '0';
								//$save_ts_pjb = $ts_pjb;

								if ($opsi == 'preview'){
									$save_ts_pjb[] = $ts_pjb;
								} elseif ($opsi == 'bernomor') {
									$savetspjb = $this->apiconn->get_api('tnde_surat_keluar/insert_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($ts_pjb)));
									if($savetspjb == FALSE || $savetspjb == NULL){
										$data['errors'][] = "Gagal mengirim ke tujuan surat ke ".$exp[0];
										$save_ts_pjb[] = FALSE;
									}elseif ($savetspjb == TRUE) {
										$save_ts_pjb[] = $ts_pjb;
									}
								}

								/*$parameter	= array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($ts_pjb));
								$save_ts_pjb = $this->mdl_tnde->get_api('tnde_surat_keluar/insert_penerima_sk', 'json', 'POST', $parameter);
								if($save_ts_pjb == FALSE){
									$data['errors'][] = "Gagal mengirim tembusan surat ke ".$exp[3]; // Pejabat
								}*/
							}
						}

						$data['distribusi_ts']['tembusan_pejabat'] = $save_ts_pjb;
					}
				}

			#4. Melakukan API Insert ke AUTOMASI
				#======= 4.1 SIMPAN DATA BERDASARKAN JENIS SURATNYA ====
				$detail_surat_sesi = unserialize($get_sesi['DETAIL_SURAT']);
				//$tgl_surat_pg = DateTime::createFromFormat('d/m/Y', $data['success']['data']['TGL_SURAT']); //obj tanggal dengan format sesuai postgresql (API AUTOMASI)
				$tgl_surat_pg = DateTime::createFromFormat('d/m/Y', $data_sk['TGL_SURAT']); //obj tanggal dengan format sesuai postgresql (API AUTOMASI)
				$detail_skr_automasi = $detail_surat_sesi;
				$tgl_bmb = isset($detail_surat_sesi['CONFIG_MASA_BERLAKU']) ? $tgl_surat_pg->add(new DateInterval($detail_surat_sesi['CONFIG_MASA_BERLAKU'])) : null;
				$detail_skr_automasi['BATAS_MASA_BERLAKU'] = isset($tgl_bmb) ?  $tgl_bmb->format('Y-m-d') : null;

				$detail_skr_automasi = array_diff_key($detail_skr_automasi, array_flip(['CONFIG_MASA_BERLAKU'])); //hapus kolom tidak dibutuhkan

				$header_skr_automasi = [
					'ID_SURAT_KELUAR' => $data_sk['ID_SURAT_KELUAR'], //'ID_SURAT_KELUAR' => $data['success']['data']['ID_SURAT_KELUAR'],
					'NO_SURAT' => $data['insert_surat_keluar']['NO_SURAT'], //'NO_SURAT' => $data['success']['data']['NO_SURAT'],
    				'TGL_SURAT' => $tgl_surat_pg->format('Y-m-d'), //'TGL_SURAT' => $data_sk['TGL_SURAT'],
    				'PEMBUAT_SURAT' => $data_sk['PEMBUAT_SURAT'], //'PEMBUAT_SURAT' => $data['success']['data']['PEMBUAT_SURAT'],
    				//'NIM' => ($data['success']['data']['KD_JENIS_ORANG'] == 'M' ? $data['success']['data']['PEMBUAT_SURAT'] : ''),
    				'KD_JENIS_SAKAD' => $get_sesi['KD_JENIS_SAKAD']
				];
				$data_skr_automasi = array_merge($header_skr_automasi, $detail_skr_automasi);
				//exit(print_r($data_skr_automasi));
				//$data['insert_skr_automasi'] = $data_skr_automasi;


				if ($opsi == 'preview'){
					$data['insert_detail_skr_automasi']['data'] = $data_skr_automasi;
				}elseif ($opsi == 'bernomor'){
					if (empty($data['errors'])){
						$parameter	= array('api_kode'=>14004, 'api_subkode' => 1, 'api_search' => array($data_skr_automasi));
						$data['insert_detail_skr_automasi'] = $this->apiconn->api_tnde_sakad('tnde_surat_akademik/insert_detail_skr_automasi', 'json', 'POST', $parameter);
					}
				}



				$pjb_ps = explode('#', $get_sesi['D_PENERIMA_PJB']);
				$pjb_ts = explode('#', $get_sesi['D_TEMBUSAN_PJB']);
				$tmp_verified_mhs = $this->session->userdata('tmp_verified_mhs');
				$data['bagian_surat_lain'] = [
					'opsi_cetak' => $opsi,
					'judul_jenis' => $detail_surat_sesi['NM_JENIS_SAKAD'],
					'path' => $detail_surat_sesi['PATH_JENIS_SAKAD'],
					'kd_unit_prodi' => $tmp_verified_mhs['KD_PRODI'],
					'nm_unit_prodi' => $tmp_verified_mhs['NM_PRODI'],
					'kd_jabatan_psd' => $pjb_ps[0], #Set Kode Jabatan Penandatangan sesuai pejabat yg menerangkan dlm surat
					'kd_jabatan_atas_nama' => $pjb_ts[0] #Set Kode Jabatan Penandatangan sesuai tembusan pertama surat
				];



			# 5. GENERATE PDF
				$data['verified_mhs'] = unserialize($get_sesi['DETAIL_MHS']);


				if (empty($data['errors'])){
					$token_keaslian = $this->test_terbitkan_jwt();
					//$data['qrcode'] = base_url().'cek_keaslian/'.$token_keaslian;
					$this->generate_pdf_surat($data);

					//lanjut hapus sesi
				} else {
					$this->output
		       			->set_status_header(200)
		       			->set_content_type('text/html')
		       			->set_output(json_encode($data['errors']));
				}

				if ($opsi == 'bernomor'){
					$del_sesi = $this->Automasi_model->hapus_sesi_automasi_buat($get_sesi['ID_SESI']);
					$data['hapus_sesi_sebelumnya'] = $del_sesi;
				}

				//(!empty($get_sesi['TMP_MHS']) ? unserialize($get_sesi['TMP_MHS']): NULL) ;
				//$data['embed_qrcode']['tokenstring_ck1'] = 'http://exp.uin-suka.ac.id/surat/scanqrcode/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ'; //CONTOH
				//$this->load->view('reports/v_sampel_report', $data['embed_qrcode']);

			# 6. HAPUS SESI
				/*
				if (empty($data['errors'])){
					$del_sesi = $this->Automasi_model->hapus_sesi_automasi_buat($get_sesi['ID_SESI']);
					$data['hapus_sesi_sebelumnya'] = $del_sesi;
				}

				exit(print_r($data));
				*/
				//exit(var_dump($api_insert_skr_automasi));

    		} else {
    			exit("Maaf , penyimpanan surat gagal karena masalah teknis, silakan merefresh halaman untuk mengulang pembuatan surat");
    		}
    	}

    	if (isset($_POST['btn_save_skr_ttd']) && $opsi == 'kirim_perm_ttd' ){
    		//exit('sedang memproses permohonan pembuatan surat skema penandatanganan digital');
    		$data['nama_jabatan'] = 'Wakil Dekan Bidang Akademik';
    		$data['nama_pegawai'] = 'Dr. Agung Fatwanto, S.Si., M.Kom.';
    		$data['nip'] = '197701032005011003';

    		$response = $this->load->view('v_perm_ttd',$data, true);
			$this->output
				->set_status_header(200)
				->set_content_type('text/html')
				->set_output($response);
    	}

    	#cetak ulang (sumber data dari remote API , bukan lagi parameter dalam bentuk arrays)
    	if (isset($_POST['print_surat_bernomor_tersimpan'])) {} #SURAT BARU SAJA DISIMPAN DAN BELUM DIAJUKAN KE TU

    	if (isset($_POST['print_surat_ttd'])) {} #CETAK SURAT YANG TELAH DITANDATANGANI SCR DIGITAL
    }

    public function test_pangkat($kd_jabatan)
    {
    	$this->load->model('Repo_Simpeg');
    	$pgw_psd = $this->Repo_Simpeg->get_pejabat_aktif(date('d/m/Y'), $kd_jabatan);
		$nip_pgw = $pgw_psd[0]['NIP'];

		$parameter = array('api_kode' => 1123, 'api_subkode' => 3, 'api_search' => array(date('d/m/Y'), $nip_pgw));
		$pang_gol = $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);

		exit(var_dump($pang_gol));
    }
    protected function generate_pdf_surat($paramdata=null)
    {
    	$this->load->model('Repo_Simpeg');
    	$this->load->model('Repo_SIA');
    	$this->load->model('common/Konversi_model','konversi_model');
		$this->load->library('common/Pdf_creator');

		#1. Cek data array pada parameter

		# verified_mhs, insert_surat_keluar, distribusi_ps, distribusi_ts, insert_detail_skr_automasi
		$kd_jenis_sakad = (int)$paramdata['insert_detail_skr_automasi']['data']['KD_JENIS_SAKAD'];

		$data_mhs = $paramdata['verified_mhs'];
		//$data['isi_mhs'] = $data_mhs;
		$raw_isi_surat = $paramdata['insert_detail_skr_automasi']['data'];

	 #2. REQUEST API UNTUK DATA-DATA
		## UNIT PENERBIT SURAT
		$parameter = array('api_kode' => 1901, 'api_subkode' => 2, 'api_search' => array($paramdata['insert_surat_keluar']['TGL_SURAT'], $paramdata['insert_surat_keluar']['UNIT_ID']));
		$data['unit'] = $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
		//$data['template']['logo_unit']	= $this->api_foto->logo_unit($paramdata['insert_surat_keluar']['UNIT_ID'], $paramdata['insert_surat_keluar']['TGL_SURAT']); #file gambar

		$u_nama = (!empty($data['unit'][0]['UNIT_NAMA']) ? strtoupper($data['unit'][0]['UNIT_NAMA']) : '');
		$u_alamat = (!empty($data['unit'][0]['UNIT_ALAMAT']) ? $data['unit'][0]['UNIT_ALAMAT'] : '');
		$u_telp1 = (!empty($data['unit'][0]['UNIT_TELP1']) ? $data['unit'][0]['UNIT_TELP1'] : '');
		$u_kdpos = (!empty($data['unit'][0]['UNIT_KDPOS']) ? $data['unit'][0]['UNIT_KDPOS'] : '');
		$u_telp2 = (!empty($data['unit'][0]['UNIT_TELP2']) ? $data['unit'][0]['UNIT_TELP2'] : '');
		$u_fax1 = (!empty($data['unit'][0]['UNIT_FAX1']) ? $data['unit'][0]['UNIT_FAX1'] : '');
		$u_fax2 = (!empty($data['unit'][0]['UNIT_FAX2']) ? $data['unit'][0]['UNIT_FAX2'] : '');
		//$u_web = (!empty($data['unit'][0]['UNIT_WEB1']) ? $data['unit'][0]['UNIT_WEB1'] : $this->konversi_model->website_unit($paramdata['insert_surat_keluar']['UNIT_ID']));
		$u_web = $this->konversi_model->website_unit($paramdata['insert_surat_keluar']['UNIT_ID']);
		$u_kab = (!empty($data['unit'][0]['UNIT_NMKAB2']) ? $data['unit'][0]['UNIT_NMKAB2'] : '');

		if (!empty($data['unit'])){
			$data['kop'] = [
				'fkl' => $u_nama,
				'almt' => $u_alamat.' '.$u_kdpos,
				'tlp' => $u_telp1,
				'fax' => $u_fax1,
				'web' => $u_web,
				'kabkota' => $u_kab
			];
			$data['template']['kopsurat'] = [
				'kementrian' => 'KEMENTERIAN AGAMA REPUBLIK INDONESIA',
				'univ' => 'UNIVERSITAS ISLAM NEGERI SUNAN KALIJAGA YOGYAKARTA',
				'fkl' => $u_nama,
				'almt' => $u_alamat.' '.$u_kdpos,
				'tlp' => $u_telp1,
				'fax' => $u_fax1,
				'web' => $u_web,
				'kabkota' => $u_kab
			];
		}


	 ## PEJABAT PENANDATANGAN SURAT
		$pgw_psd = $this->Repo_Simpeg->get_pejabat_aktif($paramdata['insert_surat_keluar']['TGL_SURAT'], $paramdata['bagian_surat_lain']['kd_jabatan_psd']);
		if (!empty($paramdata['insert_surat_keluar']['ATAS_NAMA'])){
			$pgw_psd_an = $this->Repo_Simpeg->get_pejabat_aktif($paramdata['insert_surat_keluar']['TGL_SURAT'], $paramdata['bagian_surat_lain']['kd_jabatan_atas_nama']);
			$data['label_psd_an'] = $pgw_psd_an[0]['STR_NAMA_S1']; //$data['label_psd_an'] = $pgw_psd_an[0]['STR_NAMA_A212'];
		}
		$data['label_psd'] = $pgw_psd[0]['STR_NAMA_S1'];
		//$data['label_psd'] = $pgw_psd[0]['STR_NAMA_A212'];
		$data['nm_pgw_psd'] = $pgw_psd[0]['NM_PGW_F'];
		$data['nip_pgw_psd'] = $pgw_psd[0]['NIP'];


		#3.SET UP DATA PEMBENTUK PDF
		$watermark_img = file_get_contents(FCPATH.'/assets/images/watermark-preview.png');
		if ($paramdata['bagian_surat_lain']['opsi_cetak'] == 'preview'){
			$data['watermark_img'] = base64_encode($watermark_img);
		}

		$data['qrcode_otentikasi'] = base_url('publik/cek_surat/validasi_qrcode/').$paramdata['insert_surat_keluar']['ID_SURAT_KELUAR'];
		$data['judul_surat'] = strtoupper($paramdata['bagian_surat_lain']['judul_jenis']);
		$data['nomor'] = $paramdata['insert_surat_keluar']['NO_SURAT'];
		$data['thn_akademik'] = $this->Repo_SIA->get_tahun_ajaran('info_ta_smt');
		$data['tgl_surat'] = tanggal_indo($paramdata['insert_surat_keluar']['TGL_SURAT'], 'd/m/Y');


	 # CONDITIONAL CHECK CUSTOM DATA TIAP JENIS SURAT
		$path = $paramdata['bagian_surat_lain']['path'];
    	switch ($path) {
			case 'masih_kul':	#1. PDF Surat Keterangan Masih Kuliah
				if (!empty($pgw_psd_an)){
					$data['frasa_pendahuluan'] = $pgw_psd_an[0]['STR_NAMA'].' menerangkan bahwa :';
				}

				$data['isi_pgw'] = '';
				$tgl_lahir_raw = DateTime::createFromFormat('d-m-Y h:i:s', $data_mhs['TGL_LAHIR']); //format pada sesi (postgre)
				$data['isi_mhs'] = [
					'nama' => $data_mhs['NAMA'],
					'nim' => $data_mhs['NIM'],
					'tmp_lahir' => $data_mhs['TMP_LAHIR'],
					//'tgl_lahir' => $tgl_lahir_raw->format('d/m/Y'),
					'tgl_lahir' => display_tanggal_indo($data_mhs['TGL_LAHIR'], 'd-m-Y'),
					'smt' => $data_mhs['JUM_SMT'],
					'prodi' => $data_mhs['NM_PRODI'],
					'fkl' => $data_mhs['NM_FAK'],
				];

				//$data['isi_ts_pjb'] = '';
				//$data['isi_ts_lain'] = null;
				$buka_frasa_isi = 'Surat keterangan ini dibuat untuk melengkapi salah satu syarat ';
				$data['isi_surat'] = $buka_frasa_isi.$raw_isi_surat['KEPERLUAN'];
				$view = 'reports/v2/v_r_masih_kul';
			break;
			case 'kel_baik': #2. PDF Surat Keterangan Berkelakuan Baik
				$data['isi_mhs'] = [
					'nama' => $data_mhs['NAMA'],
					'nim' => $data_mhs['NIM'],
					'tmp_lahir' => $data_mhs['TMP_LAHIR'],
					'tgl_lahir' => display_tanggal_indo($data_mhs['TGL_LAHIR'], 'd-m-Y'),
					'smt' => $data_mhs['JUM_SMT'],
					'prodi' => $data_mhs['NM_PRODI'],
					'fkl' => $data_mhs['NM_FAK']
				];
				$data['isi_surat'] = [
					'keperluan' => $raw_isi_surat["KEPERLUAN"]
				];
				$view = 'reports/v2/v_r_kel_baik';
			break;
			case 'tdk_men_bea': #4. PDF Surat Keterangan Tidak Sedang menerima beasiswa
				$data['isi_mhs'] = [
					'nama' => $data_mhs['NAMA'],
					'nim' => $data_mhs['NIM'],
					'tmp_lahir' => $data_mhs['TMP_LAHIR'],
					'tgl_lahir' => display_tanggal_indo($data_mhs['TGL_LAHIR'], 'd-m-Y'),
					'smt' => $data_mhs['JUM_SMT'],
					'prodi' => $data_mhs['NM_PRODI'],
					'fkl' => $data_mhs['NM_FAK']
				];

				if (!empty($raw_isi_surat["DATA_TAMBAHAN"])){
					$bea = '<ul>';
					foreach ($raw_isi_surat["DATA_TAMBAHAN"] as $k=>$v) {
						$bea .= '<li>'.$v['NAMA_BEASISWA'].'</li>';
					}
					$bea .= '</ul>';
				} else {
					$bea = '';
				}
				$data['isi_surat'] = [
					'daftar_beasiswa_pernah_diikuti' => $bea,
				];
				$view = 'reports/v3/v_r_tdk_men_bea';

			break;

			case 'pndh_studi': #5. PDF Surat Keterangan Pindah Studi
				$data['isi_mhs'] = [
					'nama' => $data_mhs['NAMA'],
					'nim' => $data_mhs['NIM'],
					'tmp_lahir' => $data_mhs['TMP_LAHIR'],
					'tgl_lahir' => display_tanggal_indo($data_mhs['TGL_LAHIR'], 'd-m-Y'),
					'smt' => $data_mhs['JUM_SMT'],
					'prodi' => $data_mhs['NM_PRODI'],
					'fkl' => $data_mhs['NM_FAK']
				];
				$data['isi_surat'] = [
					'keperluan' => $raw_isi_surat["KEPERLUAN"]
				];
				$view = 'reports/v3/v_r_pindah';
			break;

			case 'ijin_pen':
				$data['tujuan'] = [
					//'kepada' => nl2br($paramdata['distribusi_ps']['penerima_eksternal'][0]['PENERIMA_SURAT']),
					'kepada' => str_replace(['\r\n', '\r', '\n'], '<br/>', $paramdata['distribusi_ps']['penerima_eksternal'][0]['PENERIMA_SURAT']),
					'instansi' => '',
					'alamat' => $paramdata['insert_surat_keluar']['TEMPAT_TUJUAN']
				];

				$data['isi_mhs'] = [
					'nama' => $data_mhs['NAMA'],
					'nim' => $data_mhs['NIM'],
					'tmp_lahir' => $data_mhs['TMP_LAHIR'],
					'tgl_lahir' => display_tanggal_indo($data_mhs['TGL_LAHIR'], 'd-m-Y'),
					'smt' => $data_mhs['JUM_SMT'],
					'prodi' => $data_mhs['NM_PRODI'],
					'fkl' => $data_mhs['NM_FAK'],
					'hp' => $data_mhs['NO_HP'],
					'alamat' => $data_mhs['ALAMAT']
				];

				$data['isi_surat'] = [
					'judul'	=> $raw_isi_surat["JUDUL_DLM_KEGIATAN"],
					'tempat_penelitian' => $raw_isi_surat["TEMPAT_KEGIATAN"],
					'metode' => $raw_isi_surat["METODE_DLM_KEGIATAN"],
					'tgl_mulai' => display_tanggal_indo($raw_isi_surat["TGL_MULAI"], 'Y-m-d'),
					'tgl_berakhir'=> display_tanggal_indo($raw_isi_surat["TGL_BERAKHIR"], 'Y-m-d'),
				];

				$data['daftar_tembusan'][] = isset($pgw_psd_an) ? $pgw_psd_an[0]['STR_NAMA_S1'].' (sebagai laporan)' : '';
				$view = 'reports/v3/v_r_penelitian';
			break;

			case 'ijin_studi_pen':
				$data['tujuan'] = [
					'kepada' => str_replace(['\r\n', '\r', '\n'], '<br/>', $paramdata['distribusi_ps']['penerima_eksternal'][0]['PENERIMA_SURAT']),
					'instansi' => '',
					'alamat' => $paramdata['insert_surat_keluar']['ALAMAT_TUJUAN']
				];
				$data['isi_mhs'] = [
					'nama' => $data_mhs['NAMA'],
					'nim' => $data_mhs['NIM'],
					//'tmp_lahir' => $data_mhs['TMP_LAHIR'],
					//'tgl_lahir' => display_tanggal_indo($data_mhs['TGL_LAHIR'], 'd-m-Y'),
					'smt' => $data_mhs['JUM_SMT'],
					'prodi' => $data_mhs['NM_PRODI'],
					'fkl' => $data_mhs['NM_FAK']
				];
				$data['isi_surat'] = [
					'tema' => $raw_isi_surat["TEMA_DLM_KEGIATAN"],
					'tempat_penelitian' => $raw_isi_surat["TEMPAT_KEGIATAN"],
					'tgl_mulai' => display_tanggal_indo($raw_isi_surat["TGL_MULAI"], 'Y-m-d'),
					'tgl_berakhir'=> display_tanggal_indo($raw_isi_surat["TGL_BERAKHIR"], 'Y-m-d'),
				];
				$data['daftar_tembusan'][] = isset($pgw_psd_an) ? $pgw_psd_an[0]['STR_NAMA_S1'].' (sebagai laporan)' : '';
				$view = 'reports/v2/v_r_studi_pen';
			break;
			case 'ijin_obs':
				$data['tujuan'] = [
					'kepada' => str_replace(['\r\n', '\r', '\n'], '<br/>', $paramdata['distribusi_ps']['penerima_eksternal'][0]['PENERIMA_SURAT']),
					'instansi' => '',
					'alamat' => $paramdata['insert_surat_keluar']['ALAMAT_TUJUAN']
				];
				//$data['isi_mhs'] = $data_mhs[0]['NAMA'];
				$data['isi_mhs'] = $data_mhs;
				//exit(var_dump($data['isi_mhs']));

				$makul = explode('#', $raw_isi_surat['MATA_KULIAH_TERKAIT']);
				//sementara
				$data['prodi'] = $paramdata['bagian_surat_lain']['nm_unit_prodi'];
				$data['isi_surat'] = [
					'nama_kegiatan' => 'Observasi',//$raw_isi_surat["NAMA_KEGIATAN"],
					'kode_makul' => $makul[0],
					'nama_makul' => $makul[1],
					'topik_observasi' => $raw_isi_surat["TEMA_DLM_KEGIATAN"],
					'tempat' => $raw_isi_surat["TEMPAT_KEGIATAN"],
					'tgl_mulai' => display_tanggal_indo($raw_isi_surat["TGL_MULAI"], 'Y-m-d'),
					'tgl_berakhir'=> display_tanggal_indo($raw_isi_surat["TGL_BERAKHIR"], 'Y-m-d'),
				];
				$data['daftar_tembusan'][] = isset($pgw_psd_an) ? $pgw_psd_an[0]['STR_NAMA_S1'].' (sebagai laporan)' : '';
				$view = 'reports/v2/v_r_observasi';
			break;
			case 'perm_KP':
				$data['tujuan'] = [
					'kepada' => str_replace(['\r\n', '\r', '\n'], '<br/>', $paramdata['distribusi_ps']['penerima_eksternal'][0]['PENERIMA_SURAT']),
					'instansi' => '',
					'alamat' => $paramdata['insert_surat_keluar']['TEMPAT_TUJUAN']
				];

				$makul = explode('#', $raw_isi_surat['MATA_KULIAH_TERKAIT']);
				$data['isi_mhs'] = [
					'nama' => $data_mhs['NAMA'],
					'nim' => $data_mhs['NIM'],
					'tmp_lahir' => $data_mhs['TMP_LAHIR'],
					'tgl_lahir' => display_tanggal_indo($data_mhs['TGL_LAHIR'], 'd-m-Y'),
					'smt' => $data_mhs['JUM_SMT'],
					'prodi' => $data_mhs['NM_PRODI'],
					'fkl' => $data_mhs['NM_FAK']
				];
				$data['isi_surat'] = [
					'nama_kegiatan' => $makul[1],//$raw_isi_surat["NAMA_KEGIATAN"],
					'kode_makul' => $makul[0],
					'nama_makul' => $makul[1],
					'tempat' => $raw_isi_surat["TEMPAT_KEGIATAN"],
					'minat' => $raw_isi_surat["TEMA_DLM_KEGIATAN"],
					'tgl_mulai' => display_tanggal_indo($raw_isi_surat["TGL_MULAI"], 'Y-m-d'),
					'tgl_berakhir'=> display_tanggal_indo($raw_isi_surat["TGL_BERAKHIR"], 'Y-m-d'),
				];
				$view = 'reports/v3/v_r_kp';
			break;

			default:
				# code...
				break;
		}

		$this->load->view($view,$data);


		//return $data;
		//$data['qrcode_keaslian'] = base_url('cek_keaslian_surat/'.$data['skr_lengkap']['surat_keluar_automasi']['TOKEN_KEASLIAN']);
    }

    public function test_repo_autosurat($opsi='kd', $input=null)
    {
    	$this->load->model('Repo_Automasi_Surat','repo_autosurat');

		$get_jenis_surat_mhs = $this->repo_autosurat->get_jenis_surat_mahasiswa($opsi, $input);

		echo var_dump($get_jenis_surat_mhs);
    }

    /**
	* menciptakan PDF untuk Cetak Surat
	* sumber data :
		1) cetak langsung seetelah dikirim : array yang tersimpan session
		2) cetak ulang (belum diajukan) : array hasil API GET
	*/
    public function export_pdf_surat($id_surat_keluar=null)
    {
    }


    public function test_verifikasi($jenis_sakad, $nim)
    {
    	$this->load->model('Repo_Automasi_Surat');
    	$kd_jenis_sakad_int = (int)$jenis_sakad;

    	$arr_jenis_sakad = $this->Repo_Automasi_Surat->get_jenis_surat_mahasiswa('kd', $kd_jenis_sakad_int);
		$data['raw_jenis_sakad'] = $arr_jenis_sakad;

		$data['pre_isi_surat'] = $this->Verifikasi_model->verifikasi_persyaratan($arr_jenis_sakad, $nim);
		echo var_dump($data);
    }

     /**
    * @param Array data-data tersimpan
    * @return string jwt
    */
    public function test_terbitkan_jwt($paramdata=null)
    {
    	$this->config->load('common/signature', TRUE);
    	$this->load->helper('common/Package_loader');
        load_package_firebase_jwt();

    	$seckey = $this->config->item('hmac_seckey', 'signature');
    	define('SECRET_KEY',$seckey);   // secret key can be a random string and keep in secret
    	define('ALGORITHM','HS256');   // Algorithm used to sign the token

        $tokenId    = 'sig.'.uniqid();
        $issuedAt   = time();
        $notBefore  = $issuedAt + 10;  //Adding 10 seconds
        $expire     = $notBefore + 7200; // Adding 120 minutes
        $serverName = base_url(); /// set your domain name

        /*
        * Create the token as an array
        */
        $data = [
            'iat'  => $issuedAt,         // Issued at: time when the token was generated
            'jti'  => $tokenId,          // Json Token Id: an unique identifier for the token
            'iss'  => $serverName,       // Issuer
            'nbf'  => $notBefore,        // Not before
            'exp'  => $expire,           // Expire
            'data' => [                  // Data related to the logged user you can set your required data
                'id_surat_keluar' => 'T.5a96de2283f23',
                'no_surat' => 'B-901/Un.02/TST/02/18',
                'judul_surat'   => 'SURAT IJIN OBSERVASI',
                /*'tujuan_surat'   => [
                	'kepada' => 'Direktur NSA Security Agency c.q Biro SDM',
                ],
                'isi_surat'   => [
                	'dasar' => 'pelaksanaan tugas final mata kuliah analisis desain jaringan',
                	'keperluan' => 'observasi dan wawancara',
                	'peserta' => [
                		['nim'=>'15650051', 'nama'=> 'Didik Eko Pramono'],
                		['nim'=>'15650020', 'nama'=> 'Erna Novia Safitri'],
                		['nim'=>'15650010', 'nama'=> 'Dhoni Ari Nugroho'],
                	]
                ],
                'penandatanganan' => [
                	'jabatan_psd' => 'a.n Dekan \n Wakil Dekan Bidang Akademik',
                	'nama_pejabat_psd' => 'Dr. Agung Suprapto M.Si.'
                ],
                'distribusi_surat'   => [
                	'tembusan' => 'Dekan (sebagai laporan)'
                ]*/
            ]
        ];
        //$secretKey = base64_decode(SECRET_KEY);

        /// Here we will transform this array into JWT:
        $jwt = JWT::encode(
            $data, //Data to be encoded in the JWT
            SECRET_KEY, // The signing key
            ALGORITHM
        );
        //$unencodedArray = ['jwt' => $jwt];
        //echo json_encode($jwt);
        return $jwt;
    }

    public function test_verifikasi_jwt()
    {
    	$token_value = $this->input->get('token');
    	$this->config->load('common/signature', TRUE);
    	$this->load->helper('common/Package_loader');
        load_package_firebase_jwt();

    	$seckey = $this->config->item('hmac_seckey', 'signature');
    	define('SECRET_KEY',$seckey);   // secret key can be a random string and keep in secret
    	define('ALGORITHM','HS256');   // Algorithm used to sign the token


    	try {
            //$secretKey = base64_decode('Your-Secret-Key');
            $DecodedDataArray = JWT::decode($token_value, $seckey, array('HS256'));

            $waktu_diterbitkan = date("Y-m-d H:i:s", $DecodedDataArray->iat);
            $waktu_kadaluarsa = date("Y-m-d H:i:s", $DecodedDataArray->exp);

            echo '<h2 style="green">Waktu terbit : '.$waktu_diterbitkan.'</h2>';
            echo "<br>";
            echo '<h2 style="color: red; ">Waktu kadaluarsa : '.$waktu_kadaluarsa.'</h2>';

            echo  "{'status' : 'success' ,'data':".json_encode($DecodedDataArray)." }";
            die();
        } catch (Firebase\JWT\ExpiredException $e ) {
             echo "{'status' : 'fail' ,'msg':'Expired'}";
            die();
        } catch (Firebase\JWT\SignatureInvalidException $e) {
            echo "{'status' : 'fail' ,'msg':'Unauthorized - Signature verification failed'}";
            die();
        }

    }


}

/* End of file Automasi_surat_mhs.php */
/* Location: ./application/modules/mhs2/controllers/Automasi_surat_mhs.php */
