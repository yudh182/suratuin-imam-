<?php if(!defined('BASEPATH'))exit('No direct script access allowed');

class Act_surat_keluar_mhs extends MY_Controller {

	public function __construct()
	{
		parent::__construct();				
		$this->load->model('Verifikasi_model');
		$this->load->model('Automasi_model');
    }

    /**
    * Menyimpan data umum surat, distribusi, dan detail [programmatically]
    * mode tersedia : debug, testing (surat bernomor, tapi status simpan tetap sbg draf, supaya masih bisa dihapus), production
    * @param array data sesi (db + php session)
    * @return array (success message + data atau error message)
    */
    protected function save_surat_keluar($paramdata=array(), $mode='testing')
    {
    	#INISIALISASI DATA AWAL
    	$get_sesi = $paramdata['sesi_buat_sakad'];
    	$id_sesi_buat = $paramdata['formdata']['id_sesi_buat_cetak']; 
    	$save_final = $paramdata['formdata']['save_final_cetak'];

    	#ASSIGN KE VARIABEL TAMPUNGAN
    	if ($get_sesi == FALSE || $get_sesi == NULL){
			return ['errors' => [
				'message' => 'Mohon maaf pembuatan surat tidak dapat diselesaikan karena data sesi tidak ditemukan'
			]];
			$save_sk = FALSE;
		} else {			
			$data_sk = $get_sesi;	//assign semua kolom data_sesi ke array $data_sk
		}

		#2. Modif kolom-kolom untuk proses data
		$data_sk['ID_SURAT_KELUAR'] = uniqid().'p';
		$data_sk['ID_PSD'] = $get_sesi['PSD_ID'];
		$data_sk['ATAS_NAMA'] = $get_sesi['PSD_AN'];
		$data_sk['UNTUK_BELIAU'] = $get_sesi['PSD_UB'];
		$data_sk['STATUS_SK'] = 0;
		$data_sk['WAKTU_SIMPAN'] = date("d/m/Y H:i:s");
		$data_sk['TGL_SURAT'] = DateTime::createFromFormat('Y-m-d', $data_sk['TGL_SURAT'])->format('d/m/Y');
		$data_sk['KD_STATUS_SIMPAN'] = '2'; //bernomor

		#3. ambilkan nomor tersedia
		if ($data_sk['KD_STATUS_SIMPAN'] == '2' || $data_sk['KD_STATUS_SIMPAN'] == 2) {#1 (bernomor)
			#beri nomor tapi jangan set no_urut dan nomor_sela
			if(empty($data['errors'])){
				$parameter	= array('api_kode' => 90002, 'api_subkode' => 3, 'api_search' => array($data_sk));
				$req_no 		= $this->apiconn->api_tnde('tnde_surat_keluar/penomoran', 'json', 'POST', $parameter);
				if(empty($req_no['ERRORS'])){
					$data_sk['NO_URUT']	= '';
					$data_sk['NO_SELA']		= '';
					$data_sk['NO_SURAT']	= (!empty($req_no['NO_SURAT']) ? $req_no['NO_SURAT'] : '');
				}else{
					$save_sk = FALSE;
					$data['errors'] = $req_no['ERRORS'];
				}
			}
		} elseif ($data_sk['KD_STATUS_SIMPAN'] == '1' || $data_sk['KD_STATUS_SIMPAN'] == 1) {#1 (draf)
			$data_sk['NO_URUT']		= '';
			$data_sk['NO_SELA']		= '';
			$data_sk['NO_SURAT']	= '';
		} else { 
		#code			
		}

		#4. seleksi kolom untuk data umum SURAT_KELUAR
		$key_diikutkan = ['ID_SURAT_KELUAR', 'KD_STATUS_SIMPAN', 'KD_JENIS_SURAT', 'ID_PSD', 'ATAS_NAMA', 'UNTUK_BELIAU', 'ID_KLASIFIKASI_SURAT', 'TGL_SURAT', 'STATUS_SK', 'PEMBUAT_SURAT', 'JABATAN_PEMBUAT', 'KD_JENIS_ORANG','UNIT_ID', 'UNTUK_UNIT', 'PERIHAL', 'TEMPAT_DIBUAT', 'TEMPAT_TUJUAN', 'KOTA_TUJUAN', 'ALAMAT_TUJUAN', 'LAMPIRAN', 'KD_SIFAT_SURAT', 'KD_KEAMANAN_SURAT', 'WAKTU_SIMPAN', 'KD_SISTEM', 'NO_URUT', 'NO_SELA', 'ISI_SURAT', 'KD_PRODI', 'NO_SURAT'];			
		$data_sk = array_intersect_key($data_sk, array_flip($key_diikutkan));

		if (is_array($data_sk)){
			$save_sk = TRUE;
		} else {
			$save_sk = FALSE;
		}


		#5. API INSERT DATA UMUM SURAT_KELUAR (SISURAT)
		if($save_sk == TRUE){
			$parameter	= array('api_kode' => 90001, 'api_subkode' => 1, 'api_search' => array($data_sk));
			$save_sk 		= $this->apiconn->api_tnde('tnde_surat_keluar/insert_surat_kaluar', 'json', 'POST', $parameter);

			//exit(var_dump($data_sk));
			if($save_sk	==	FALSE || $save_sk == NULL){
				$data['errors'][] = "Gagal menyimpan surat. #0";
				$data['surat_keluar'] = FALSE;
			}else{
				$save_sk	=	TRUE;
				$data['surat_keluar'] = $data_sk;
			}
		}

		# 6. API INSERT DATA PENERIMA SURAT KELUAR	(SISURAT)
		if ($save_sk == TRUE){
			#============== 3.1 Pengelola Surat (PS)==============
			# 3.1 KIRIM SURAT KE PEJABAT (Yang menerangkan, menandatangani, dsb)
			if(!empty($kpd_pejabat[0])){
				$save_pjb = [];
				foreach($kpd_pejabat as $key=> $val){
					$exp = explode("#", $val);
					$get_pjb = $this->Repo_Simpeg->get_pejabat_aktif($data['insert_surat_keluar']['TGL_SURAT'], $exp[0]);						
					if(count($exp) == 2){
						$pjb['ID_SURAT_KELUAR']				= $data_sk['ID_SURAT_KELUAR'];
						$pjb['PENERIMA_SURAT'] = $get_pjb[0]['NIP']; //NIP PEGAWAI
						$pjb['JABATAN_PENERIMA'] = $exp[0];
						$pjb['KD_GRUP_TUJUAN'] = 'PJB01';
						$pjb['KD_STATUS_DISTRIBUSI'] = "PS";
						$pjb['ID_STATUS_SM'] = 1;
						$pjb['KD_JENIS_TEMBUSAN'] = 0;
						$pjb['NO_URUT'] = $key + 1;
						$pjb['KD_JENIS_KEPADA']				= $exp[1];

						if ($opsi == 'preview'){
							$save_pjb[] = $pjb;	
						} elseif ($opsi == 'bernomor') {
							$savepjb = $this->apiconn->get_api('tnde_surat_keluar/insert_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($pjb)));
							if($savepjb == FALSE){
								$data['errors'][] = "Gagal mengirim ke tujuan surat ke ".$pjb['PENERIMA_SURAT'];
								$save_pjb[] = FALSE;	
							}elseif ($savepjb == TRUE) {
								$save_pjb[] = $pjb;	
							}
						}
													
					}
				}

				$data['distribusi_ps']['penerima_pejabat'] = $save_pjb;
			}

			# 3.2 KIRIM SURAT KE MAHASISWA
			if(!empty($kpd_mahasiswa[0])){
				$save_mhs = [];
				foreach($kpd_mahasiswa as $key=> $val){
					$exp = explode("#", $val);
					if(count($exp) == 2){
						$mhs['ID_SURAT_KELUAR']				= $data_sk['ID_SURAT_KELUAR'];
						$mhs['PENERIMA_SURAT'] = $exp[0];
						$mhs['JABATAN_PENERIMA'] = '0';
						$mhs['KD_GRUP_TUJUAN'] = 'MHS01';
						$mhs['KD_STATUS_DISTRIBUSI'] = "PS";
						$mhs['ID_STATUS_SM'] = 1;
						$mhs['KD_JENIS_TEMBUSAN'] = 0;
						$mhs['NO_URUT'] = $key + 1;
						$mhs['KD_JENIS_KEPADA']				= $exp[1];
						

						if ($opsi == 'preview'){
							$save_mhs[] = $mhs;
						} elseif ($opsi == 'bernomor') {
							$savemhs = $this->apiconn->get_api('tnde_surat_keluar/insert_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($mhs)));
							if($savemhs == FALSE || $savemhs == NULL){
								$data['errors'][] = "Gagal mengirim ke tujuan surat ke ".$exp[0];
								$save_mhs[] = FALSE;
							}elseif ($savemhs == TRUE) {
								$save_mhs[] = $mhs;	
							}
						}
						/*$save_mhs = $this->apiconn->get_api('tnde_surat_keluar/insert_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($mhs)));
						if($save_mhs == FALSE){
							$data['errors'][] = "Gagal mengirim ke tujuan surat ke ".$exp[0]; // Mahasiswa
						}*/
					}
				}

				$data['distribusi_ps']['penerima_mahasiswa'] = $save_mhs;
			}
			
			# 3.3 KIRIM SURAT KE EKSTERNAL
			$kpd_eks = (!empty($get_sesi['D_PENERIMA_EKS']) ? explode('<$>', $get_sesi['D_PENERIMA_EKS']) : null);
			if(!empty($kpd_eks[0])){
				$save_eks = [];					
				foreach($kpd_eks as $key => $val){
					$exp = explode("#", $val);

					if(count($exp) == 2){
						$eks['ID_SURAT_KELUAR']				= $data_sk['ID_SURAT_KELUAR'];
						$eks['PENERIMA_SURAT']				= $exp[0];
						$eks['JABATAN_PENERIMA']			= $exp[0]; //misal: kepala, kabag, sekda, dsb
						$eks['KD_GRUP_TUJUAN']				= "EKS01";
						$eks['KD_STATUS_DISTRIBUSI']		= "PS";	
						$eks['ID_STATUS_SM'] = 1;
						$eks['KD_JENIS_TEMBUSAN'] = 0;
						$eks['NO_URUT'] = $key + 1;
						$eks['KD_JENIS_KEPADA']				= $exp[1]; //1 = untuk perhatian , 0
						//$save_eks[] = $eks;

						if ($opsi == 'preview'){
							$save_eks[] = $eks;
						} elseif ($opsi == 'bernomor') {
							$saveeks = $this->apiconn->get_api('tnde_surat_keluar/insert_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($eks)));
							if($saveeks == FALSE || $saveeks == NULL){
								$data['errors'][] = "Gagal mengirim ke tujuan surat ke ".$exp[0];
								$save_eks[] = FALSE;
							}elseif ($saveeks == TRUE) {
								$save_eks[] = $eks;	
							}
						}
						/*$save_mhs = $this->apiconn->get_api('tnde_surat_keluar/insert_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($mhs)));
						if($save_mhs == FALSE){
							$data['errors'][] = "Gagal mengirim ke tujuan surat ke ".$exp[0]; // Mahasiswa
						}*/												
					}
				}

				$data['distribusi_ps']['penerima_eksternal'] = $save_eks;
			}

			#============== 3.2 Tembusan Surat (TS)==============    			
			if(!empty($ts_pejabat)){
				$ts_urut = 0;
				foreach($ts_pejabat as $key => $val){
					$exp = explode("#", $val);
					$get_pjb = $this->Repo_Simpeg->get_pejabat_aktif($data['insert_surat_keluar']['TGL_SURAT'], $exp[0]);						
					if(count($exp) == 2 ){
						$ts_pjb['ID_SURAT_KELUAR'] 				= $data['insert_surat_keluar']['ID_SURAT_KELUAR'];
						$ts_pjb['PENERIMA_SURAT'] 				= $get_pjb[0]['NIP']; //NIP PEGAWAI
						$ts_pjb['JABATAN_PENERIMA'] 			= $exp[0];
						$ts_pjb['KD_GRUP_TUJUAN'] 				= 'PJB01';
						$ts_pjb['KD_STATUS_DISTRIBUSI'] 	= "TS";
						$ts_pjb['ID_STATUS_SM'] 						= "1";
						$ts_pjb['NO_URUT']									= $ts_urut + 1;
						$ts_pjb['KD_JENIS_TEMBUSAN'] 		= (string)$exp[1]; #1 = sebagai laporan, 0 = biasa
						$ts_pjb['KD_JENIS_KEPADA'] 				= '0';					
						//$save_ts_pjb = $ts_pjb;

						if ($opsi == 'preview'){
							$save_ts_pjb[] = $ts_pjb;
						} elseif ($opsi == 'bernomor') {
							$savetspjb = $this->apiconn->get_api('tnde_surat_keluar/insert_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($ts_pjb)));
							if($savetspjb == FALSE || $savetspjb == NULL){
								$data['errors'][] = "Gagal mengirim ke tujuan surat ke ".$exp[0];
								$save_ts_pjb[] = FALSE;
							}elseif ($savetspjb == TRUE) {
								$save_ts_pjb[] = $ts_pjb;	
							}
						}

						/*$parameter	= array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($ts_pjb));
						$save_ts_pjb = $this->mdl_tnde->get_api('tnde_surat_keluar/insert_penerima_sk', 'json', 'POST', $parameter);
						if($save_ts_pjb == FALSE){
							$data['errors'][] = "Gagal mengirim tembusan surat ke ".$exp[3]; // Pejabat
						}*/
					}
				}

				$data['distribusi_ts']['tembusan_pejabat'] = $save_ts_pjb;
			}
		}

		#7. API INSERT DETAIL_SURAT DAN HEADER_SURAT (AUTOMASI)		
		#======= 4.1 SIMPAN DATA BERDASARKAN JENIS SURATNYA ====
		$detail_surat_sesi = unserialize($get_sesi['DETAIL_SURAT']);
		$tgl_surat_pg = DateTime::createFromFormat('d/m/Y', $data_sk['TGL_SURAT']); //obj tanggal dengan format sesuai postgresql (API AUTOMASI)				

		$detail_skr_automasi = $detail_surat_sesi;
		$tgl_bmb = isset($detail_surat_sesi['CONFIG_MASA_BERLAKU']) ? $tgl_surat_pg->add(new DateInterval($detail_surat_sesi['CONFIG_MASA_BERLAKU'])) : null;
		$detail_skr_automasi['BATAS_MASA_BERLAKU'] = isset($tgl_bmb) ?  $tgl_bmb->format('Y-m-d') : null;

		$detail_skr_automasi = array_diff_key($detail_skr_automasi, array_flip(['CONFIG_MASA_BERLAKU'])); //hapus kolom tidak dibutuhkan

		$header_skr_automasi = [					
			'ID_SURAT_KELUAR' => $data_sk['ID_SURAT_KELUAR'],
			'NO_SURAT' => $data['insert_surat_keluar']['NO_SURAT'],
			'TGL_SURAT' => $tgl_surat_pg->format('Y-m-d'),
			'PEMBUAT_SURAT' => $data_sk['PEMBUAT_SURAT'],	
			'KD_JENIS_SAKAD' => $get_sesi['KD_JENIS_SAKAD']
		];
		$data_skr_automasi = array_merge($header_skr_automasi, $detail_skr_automasi);		
					
		
		if (empty($data['errors'])){
			$parameter	= array('api_kode'=>14004, 'api_subkode' => 1, 'api_search' => array($data_skr_automasi));
			$data['detail_surat_automasi'] = $this->apiconn->api_tnde_sakad('tnde_surat_akademik/insert_detail_skr_automasi', 'json', 'POST', $parameter);

			if (empty($data['detail_surat_automasi']) OR $data['detail_surat_automasi'] == FALSE){
				$data['errors'][] = 'Gagal menyimpan data detail surat ke basisdata automasi';
			}
		}
				
		#8. Send back hasil dalam bentuk array
		return $data;		
    }

    public function cetak_surat_tersimpan($id_surat_keluar=null, $mode='full')
    {
    	$this->load->model('Repo_Simpeg');
    	$this->load->model('Repo_SIA');
    	$this->load->model('Repo_Automasi_Surat');
    	$this->load->helper('Tnde_sakad');

    	## 1. API GET SURAT KELUAR (UMUM + PENERIMA)
		$data['surat_keluar'] = $this->Automasi_model->get_surat_keluar($id_surat_keluar);
		$skr_umum = $data['surat_keluar']['umum'];
		$skr_penerima_ps = $data['surat_keluar']['penerima']['PS'];
		$skr_penerima_ts = $data['surat_keluar']['penerima']['TS'];

		## 2. API GET DETAIL SURAT (AUTOMASI)
		$data['detail_surat_automasi'] = $this->Automasi_model->get_detail_skr_automasi($id_surat_keluar);

		#seleksi hanya penerima eksternal
		foreach ($skr_penerima_ps as $k=>$v) {
			if ($v['KD_GRUP_TUJUAN'] !== 'EKS01'){
				continue;
			} else {
				$ketemu = $v['PENERIMA_SURAT'];
				break;
			}
		}
		//$data['tujuan_surat']['pimpinan_instansi'] = $save_eks[0]['PENERIMA_SURAT'];
		$data['tujuan_surat']['pimpinan_instansi'] = $ketemu;
		$data['tujuan_surat']['tempat_tujuan'] = $data['surat_keluar']['umum']['TEMPAT_TUJUAN'];

		## 3. UNIT
		### 3.1 Unit penerbit surat
		$parameter = array('api_kode' => 1901, 'api_subkode' => 2, 'api_search' => array($data['surat_keluar']['umum']['TGL_SURAT'], $data['surat_keluar']['umum']['UNIT_ID']));
		$data['unit_penerbit'] = $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);

		## PEGAWAI/PEJABAT PENANDATANGANAN disebut dalam surat
		if (!empty($skr_umum['ID_PSD'])){
			$pgw_psd = $this->Repo_Simpeg->get_pejabat_aktif($skr_umum['TGL_SURAT'], $skr_umum['KD_JABATAN_2']);
			$nip_pgw = $pgw_psd[0]['NIP'];
			$parameter = array('api_kode' => 1123, 'api_subkode' => 3, 'api_search' => array($skr_umum['TGL_SURAT'], $nip_pgw));
			$pang_gol = $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
			$data['psd_detail']['pangkat'] = (!empty($pang_gol) ? $pang_gol[0] : []);
			$data['psd_detail']['pegawai'] = $pgw_psd;
		}
		
		if (!empty($skr_umum['ATAS_NAMA'])){
			$an = $this->apiconn->api_tnde('tnde_general/get_data', 'json', 'POST', array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('MD_PSD', 'ID_PSD', $skr_umum['ATAS_NAMA'])));
			if(!empty($an['KD_JABATAN'])){
				$parameter = array('api_kode' => 1121, 'api_subkode' => 9, 'api_search' => array($skr_umum['TGL_SURAT'], $an['KD_JABATAN'], 3));
				$data['psd_detail']['atas_nama'] = $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
			}			
			$data['psd_ringkas']['label_psd_an'] = $data['psd_detail']['atas_nama'][0]['STR_NAMA_S1']; //$data['label_psd_an'] = $pgw_psd_an[0]['STR_NAMA_A212'];
		}		
		$data['psd_ringkas']['label_psd'] = $pgw_psd[0]['STR_NAMA_S1'];
		//$data['label_psd'] = $pgw_psd[0]['STR_NAMA_A212'];
		$data['psd_ringkas']['nm_pgw_psd'] = $pgw_psd[0]['NM_PGW_F'];

		## PEJABAT disebut dalam surat
		$psd_profil_pegawai = array_intersect_key($pgw_psd[0], array_flip(['NIP','NM_PGW_F','UNIT_NAMA','STR_NAMA','STR_NAMA_A212']));
		if (!empty($data['psd_detail']['pangkat'])){
			$psd_pangkat_pegawai = array_intersect_key($data['psd_detail']['pangkat'], array_flip(['HIE_GOLRU','HIE_NAMA']));	
		} else {
			$psd_pangkat_pegawai = [];
		}
		
		$data['pejabat_dalam_surat'] = array_merge($psd_profil_pegawai, $psd_pangkat_pegawai);

		## MAHASISWA disebut dalam surat
		$kepada = $skr_penerima_ps;		
		$kd_jenis_sakad_int = (int)$data['detail_surat_automasi']['KD_JENIS_SAKAD'];

		$arr_jenis_sakad = $this->Repo_Automasi_Surat->get_jenis_surat_mahasiswa('kd', $kd_jenis_sakad_int);
		$data['raw_jenis_sakad'] = $arr_jenis_sakad;
		//exit(var_dump($data['raw_jenis_sakad']));

		foreach ($kepada as $k=>$v) {
			if($v['KD_GRUP_TUJUAN'] == 'MHS01'){
				$multi_mhs = [8,11]; #observasi, penelitian matkul
				if (in_array($kd_jenis_sakad_int, $multi_mhs)){
					$tmp_mhs = $this->Repo_SIA->get_profil_mhs($v['PENERIMA_SURAT'],'kumulatif');
					$data['mhs_dalam_surat'][$k]['NIM']  = $tmp_mhs[0]['NIM']; 
					$data['mhs_dalam_surat'][$k]['NAMA']  = $tmp_mhs[0]['NAMA_F']; 
				} else {
					$data['mhs_dalam_surat'][] = $this->Verifikasi_model->prepare_isi_surat($arr_jenis_sakad, $v['PENERIMA_SURAT'])['tmp_verified_mhs'];	
				}
				
			} else {
				continue;
			}
		}

		##ambil data prodi berdasarkan data mahasiswa disebut dlm surat
		$getmhs = $this->Repo_SIA->get_profil_mhs($skr_umum['PEMBUAT_SURAT'],'kumulatif');		
		$data['prodi'] = $getmhs[0]['NM_PRODI']; 


		// foreach ($kepada as $k=>$v) {
		// 	if($v['KD_GRUP_TUJUAN'] == 'MHS01'){
		// 		#cek apakah terdaftar untuk request sia_mhs_kum
		// 		if (in_array($kd_jenis_sakad_int, $config_api_by_jenis_surat['sia_mhs_kum'])){
		// 			$get_mhs = $this->Repo_SIA->get_profil_mhs($v['PENERIMA_SURAT'],'kumulatif');
		// 			$data['detail_penerima'][$k]['mahasiswa']['kum'] = (!empty($get_mhs[0]) ? $get_mhs[0] : '');

		// 		}

		// 		#cek apakah terdaftar untuk request sia_mhs_dpm
		// 		if (in_array($kd_jenis_sakad_int, $config_api_by_jenis_surat['sia_mhs_dpm'])){
		// 			$get_mhs = $this->Repo_SIA->get_profil_mhs($v['PENERIMA_SURAT'],'dpm');
		// 			$data['detail_penerima'][$k]['mahasiswa']['dpm'] = (!empty($get_mhs[0]) ? $get_mhs[0] : '');
		// 		}

		// 		#cek apakah terdaftar untuk request sia_mhs_makul
		// 		if (in_array($kd_jenis_sakad_int, $config_api_by_jenis_surat['sia_mhs_makul'])){
					
		// 		}
				
		// 	} else {
		// 		continue;
		// 	}
		// }

		// ## TEMBUSAN INTERNAL (PEJABAT)
		// $tembusan = $skr_penerima_ts; //assign
		// if(!empty($tembusan)){
		// 	foreach($tembusan as $key => $val){
		// 		switch($val['KD_GRUP_TUJUAN']){
		// 			case 'PJB01':
		// 				$parameter 	= array('api_kode' => 1101, 'api_subkode' => 1, 'api_search' => array($val['JABATAN_PENERIMA']));
		// 				$jab_tembusan = $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
		// 				break;
		// 			case 'MHS01':
		// 				$parameter	= array('api_kode'=>26000, 'api_subkode' => 12, 'api_search' => array($val['PENERIMA_SURAT']));
		// 				$get_mhs 		= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);
		// 				$data['penerima_tembusan'][$key]['mahasiswa'] = (!empty($get_mhs[0]) ? $get_mhs[0] : '');
		// 				break;
		// 			case 'EKS01':

		// 				break;
		// 		break;

		// 	}
		// }
		

		if ($mode === 'full'){
			$this->generate_pdf_surat_v2($data);	
		} elseif ($mode === 'debug') {
			exit(var_dump($data));
		}		
    }

    public function cetak_surat_preview($paramdata=array())
    {
    }

    public function cetak_surat_tersubmit()
    {}


    /**
    * Menghasilkan PDF Surat 
    * perbaikan dari versi sebelumnya : 
    	- mengurangi additional request ke API (sampai hanya butuh 1 request saja)
    	- lebih semantik    
    */
    public function generate_pdf_surat_v2($paramdata=null)
    {
    	//$this->load->model('Repo_SIA');
    	$this->load->helper('Tnde_sakad');
    	$this->load->model('common/Konversi_model','konversi_model');
		$this->load->library('common/Pdf_creator');


    	$elemen_pdf_surat = ['meta', 'kop', 'frasa_buka', 'isi_pejabat_psd', 'isi_mhs', 'isi_surat', 'frasa_penutup', 'psd', 'tembusan'];

    	$raw_skr_umum = $paramdata['surat_keluar']['umum'];
    	$raw_tembusan = $paramdata['surat_keluar']['penerima']['TS'];
    	$raw_pengelola = $paramdata['surat_keluar']['penerima']['PS'];

    	$raw_data_mhs = $paramdata['mhs_dalam_surat'];
    	$raw_isi_surat = $paramdata['detail_surat_automasi'];
    	$raw_unit_penerbit = $paramdata['unit_penerbit'][0];
    	$raw_psd = $paramdata['psd_detail'];

    	#siapkan variabel2 pembentuk pdf
    	if (!empty($raw_unit_penerbit)){
    		$data['template']['kopsurat'] = [
				'kementrian' => 'KEMENTERIAN AGAMA REPUBLIK INDONESIA',
				'univ' => 'UNIVERSITAS ISLAM NEGERI SUNAN KALIJAGA YOGYAKARTA',
				'fkl' => (!empty($raw_unit_penerbit['UNIT_NAMA']) ? strtoupper($raw_unit_penerbit['UNIT_NAMA']) : ''),
				'almt' => (!empty($raw_unit_penerbit['UNIT_ALAMAT']) ? $raw_unit_penerbit['UNIT_ALAMAT'] : '').' '.(!empty($raw_unit_penerbit['UNIT_KDPOS']) ? $raw_unit_penerbit['UNIT_KDPOS'] : ''),
				'tlp' => (!empty($raw_unit_penerbit['UNIT_TELP1']) ? $raw_unit_penerbit['UNIT_TELP1'] : ''),
				'fax' => (!empty($raw_unit_penerbit['UNIT_FAX1']) ? $raw_unit_penerbit['UNIT_FAX1'] : ''),
				'web' => $this->konversi_model->website_unit($paramdata['surat_keluar']['umum']['UNIT_ID']),
				'kabkota' => (!empty($raw_unit_penerbit['UNIT_NMKAB2']) ? $raw_unit_penerbit['UNIT_NMKAB2'] : '')
			];
    	}

    	if (!empty($paramdata['psd_ringkas'])){
    		$data['psd'] = $paramdata['psd_ringkas'];
    	}

    	if (!empty($paramdata['pejabat_dalam_surat'])){
    		$data['pejabat_dalam_surat'] = $paramdata['pejabat_dalam_surat'];	
    	}

    	$data['judul_surat'] = strtoupper($paramdata['raw_jenis_sakad']['NM_JENIS_SAKAD']);
		$data['nomor'] = $paramdata['surat_keluar']['umum']['NO_SURAT'];
		$data['thn_akademik'] = $this->Repo_SIA->get_tahun_ajaran('info_ta_smt');
		$data['tgl_surat'] = tanggal_indo($paramdata['surat_keluar']['umum']['TGL_SURAT'], 'd/m/Y');
		$data['qrcode_otentikasi'] = base_url('publik/cek_surat/validasi_qrcode/').$paramdata['surat_keluar']['umum']['ID_SURAT_KELUAR'];

		##SET UP DATA PEMBENTUK PDF
		$watermark_img = file_get_contents(FCPATH.'/assets/images/watermark-preview.png');
		$opsi = (isset($paramdata['opsi']) ? $paramdata['opsi'] : null);
		if (!empty($opsi)){
			if ($opsi == 'preview'){
				$data['watermark_img'] = base64_encode($watermark_img);
			}	
		}
		

		## CONDITIONAL CHECK CUSTOM DATA TIAP JENIS SURAT
		$kd_jenis_sakad = $paramdata['raw_jenis_sakad']['KD_JENIS_SAKAD'];
		$path = $paramdata['raw_jenis_sakad']['URLPATH'];
		switch ($path) {
			case 'masih_kul':	#1. PDF Surat Keterangan Masih Kuliah
				$data['isi_pgw'] = '';				
				$data['isi_mhs'] = [
					'nama' => $raw_data_mhs['NAMA'],
					'nim' => $raw_data_mhs['NIM'],
					'tmp_lahir' => $raw_data_mhs['TMP_LAHIR'],		
					'tgl_lahir' => display_tanggal_indo($raw_data_mhs['TGL_LAHIR'], 'd-m-Y'),
					'smt' => $raw_data_mhs['JUM_SMT'],
					'prodi' => $raw_data_mhs['NM_PRODI'],
					'fkl' => $raw_data_mhs['NM_FAK'],
				];
				$buka_frasa_isi = 'Surat keterangan ini dibuat untuk melengkapi salah satu syarat ';
				$data['isi_surat'] = $buka_frasa_isi.$raw_isi_surat['KEPERLUAN'];
				$view = 'reports/v2.1/v_r_masih_kul';
			break;
			case 'kel_baik': #2. PDF Surat Keterangan Berkelakuan Baik
				$data['isi_mhs'] = [
					'nama' => $raw_data_mhs[0]['NAMA'],
					'nim' => $raw_data_mhs[0]['NIM'],
					'tmp_lahir' => $raw_data_mhs[0]['TMP_LAHIR'],
					'tgl_lahir' => display_tanggal_indo($raw_data_mhs[0]['TGL_LAHIR'], 'd-m-Y'),
					'smt' => $raw_data_mhs[0]['JUM_SMT'],
					'prodi' => $raw_data_mhs[0]['NM_PRODI'],
					'fkl' => $raw_data_mhs[0]['NM_FAK']
				];
				$data['isi_surat'] = [					
					'keperluan' => $raw_isi_surat["KEPERLUAN"]
				];
				$view = 'reports/v2.1/v_r_kel_baik';
			break;
			case 'ijin_pen_makul':
				$data['tujuan'] = [					
					'kepada' => str_replace(['\r\n', '\r', '\n'], '<br/>', $paramdata['tujuan_surat']['pimpinan_instansi']),
					'instansi' => '',
					'alamat_tempat' => $paramdata['tujuan_surat']['tempat_tujuan']
				];				
				$data['isi_mhs'] = $raw_data_mhs;				

				$makul = explode('#', $raw_isi_surat['MATA_KULIAH_TERKAIT']);
				$data['prodi'] = $paramdata['prodi'];
				$data['isi_surat'] = [
					'nama_kegiatan' => (!empty($raw_isi_surat["NAMA_KEGIATAN"]) ? $raw_isi_surat["NAMA_KEGIATAN"] : ''),
					'kode_makul' => $makul[0],
					'nama_makul' => $makul[1],
					'topik_penelitian_makul' => $raw_isi_surat["TEMA_DLM_KEGIATAN"],
					'tempat' => $raw_isi_surat["TEMPAT_KEGIATAN"],				
					'tgl_mulai' => display_tanggal_indo($raw_isi_surat["TGL_MULAI"], 'Y-m-d'),
					'tgl_berakhir'=> null,
				];
				$data['daftar_tembusan'][] = isset($pgw_psd_an) ? $pgw_psd_an[0]['STR_NAMA_S1'].' (sebagai laporan)' : '';
				$view = 'reports/v2.1/v_r_pen_makul';	
			break;		
			
			default:
				# code...
				break;
		}$this->load->view($view,$data);
    }

    public function aksi_simpan_cetak_surat_baru($mode='pdf') # a) preview (tanpa simpan langsung cetak), b) bernomor (simpan bernomor langsung cetak) c) simpan_saja d)kirim_perm_ttd
    {
    	$this->load->model('Repo_Simpeg');
    	$this->load->model('Repo_SIA');
    	$this->load->model('Repo_Automasi_Surat');    	

    	if (isset($_POST['btn_save_skr_bernomor']) || isset($_POST['btn_preview_skr_bernomor'])){ #Sementara disimpan sbagai draf (testing)
    		$id_sesi_buat = $this->input->post('id_sesi_buat_cetak'); 
    		$save_final = $this->input->post('save_final_cetak');
    		if (isset($_POST['btn_preview_skr_bernomor'])){
	    		$opsi = 'preview';
	    		$data['opsi'] = $opsi;
	    	} elseif (isset($_POST['btn_save_skr_bernomor'])) {
	    		$opsi = 'bernomor';
	    		$data['opsi'] = $opsi;
	    	}
	    	

    		if (!empty($id_sesi_buat) && $save_final == 'ok'){
    			#1. Mengambil data umum dan penerima surat dari sesi
    			$get_sesi = $this->Automasi_model->sesi_by_id($id_sesi_buat); #mapping ke semua API INSERT
    			;
    			$data['isi_mhs'] = ''; //assign data ke variabel lain
    			if ($get_sesi == FALSE || $get_sesi == NULL){
    				exit('Mohon maaf pembuatan surat tidak dapat diselesaikan karena data sesi tidak ditemukan');
    			} else {
    				//assign semua kolom data_sesi ke array $data_sk
					$data_sk = $get_sesi;	
    			}
    			
				#2. Modif kolom-kolom untuk proses data
				$data_sk['ID_SURAT_KELUAR'] = uniqid().'p';
				$data_sk['ID_PSD'] = $get_sesi['PSD_ID'];
				$data_sk['ATAS_NAMA'] = $get_sesi['PSD_AN'];
				$data_sk['UNTUK_BELIAU'] = $get_sesi['PSD_UB'];
				$data_sk['STATUS_SK'] = 0;
				$data_sk['WAKTU_SIMPAN'] = date("d/m/Y H:i:s");
				$data_sk['TGL_SURAT'] = DateTime::createFromFormat('Y-m-d', $data_sk['TGL_SURAT'])->format('d/m/Y');

				if ($opsi == 'preview'){
					$data_sk['KD_STATUS_SIMPAN'] =  '2';

					if($data_sk['KD_STATUS_SIMPAN'] == '2' || $data_sk['KD_STATUS_SIMPAN'] == 2){ #2 BERNOMOR
						#dapatkan nomor
						// if(empty($data['errors'])){
						// 	$parameter	= array('api_kode' => 90002, 'api_subkode' => 3, 'api_search' => array($data_sk));
						// 	$req_no 		= $this->apiconn->api_tnde('tnde_surat_keluar/penomoran', 'json', 'POST', $parameter);
						// 	if(empty($req_no['ERRORS'])){
						// 		$data_sk['NO_URUT']	= (!empty($req_no['NO_URUT']) ? $req_no['NO_URUT'] : 0);
						// 		$data_sk['NO_SELA']		= (!empty($req_no['NO_SELA']) ? $req_no['NO_SELA'] : 0);
						// 		$data_sk['NO_SURAT']	= (!empty($req_no['NO_SURAT']) ? $req_no['NO_SURAT'] : 0);
						// 	}else{
						// 		$save_sk = FALSE;
						// 		$data['errors'] = $req_no['ERRORS'];
						// 	}
						// }
						$data_sk['NO_URUT']		= null;
						$data_sk['NO_SELA']		= null;
						$data_sk['NO_SURAT']	= 'belum diset';

					} else { #1 (draf)
						$data_sk['NO_URUT']		= '';
						$data_sk['NO_SELA']		= '';
						$data_sk['NO_SURAT']	= 'belum diset';
					}
				} elseif ($opsi == 'bernomor') {
					$data_sk['KD_STATUS_SIMPAN'] = '1';

					/*if($data_sk['KD_STATUS_SIMPAN'] == '2' || $data_sk['KD_STATUS_SIMPAN'] == 2){ #2 BERNOMOR
						#dapatkan nomor dan set ke no urut , no sela
						if(empty($data['errors'])){
							$parameter	= array('api_kode' => 90002, 'api_subkode' => 3, 'api_search' => array($data_sk));
							$req_no 		= $this->apiconn->api_tnde('tnde_surat_keluar/penomoran', 'json', 'POST', $parameter);
							if(empty($req_no['ERRORS'])){
								$data_sk['NO_URUT']	= (!empty($req_no['NO_URUT']) ? $req_no['NO_URUT'] : 0);
								$data_sk['NO_SELA']		= (!empty($req_no['NO_SELA']) ? $req_no['NO_SELA'] : 0);
								$data_sk['NO_SURAT']	= (!empty($req_no['NO_SURAT']) ? $req_no['NO_SURAT'] : 0);
							}else{
								$save_sk = FALSE;
								$data['errors'] = $req_no['ERRORS'];
							}
						}
					}*/
					if ($data_sk['KD_STATUS_SIMPAN'] == '2' || $data_sk['KD_STATUS_SIMPAN'] == 2) {
						#beri nomor tapi jangan set no_urut dan nomor_sela
						if(empty($data['errors'])){
							$parameter	= array('api_kode' => 90002, 'api_subkode' => 3, 'api_search' => array($data_sk));
							$req_no 		= $this->apiconn->api_tnde('tnde_surat_keluar/penomoran', 'json', 'POST', $parameter);

							$array_penomoran_surat_aktif = [11];
							$kd_jenis_sakad_int = (int)$get_sesi['KD_JENIS_SAKAD'];
							if (in_array($kd_jenis_sakad_int, $array_penomoran_surat_aktif)){

								if(empty($req_no['ERRORS'])){
									$data_sk['NO_URUT']	= (!empty($req_no['NO_URUT']) ? $req_no['NO_URUT'] : 0);
									$data_sk['NO_SELA']		= (!empty($req_no['NO_SELA']) ? $req_no['NO_SELA'] : 0);
									$data_sk['NO_SURAT']	= (!empty($req_no['NO_SURAT']) ? $req_no['NO_SURAT'] : 0);
								}else{
									$save_sk = FALSE;
									$data['errors'] = $req_no['ERRORS'];
								}
								
								$data_sk['KD_STATUS_SIMPAN'] = '2';
							} else {

								if(empty($req_no['ERRORS'])){
									$data_sk['NO_URUT']	= '';
									$data_sk['NO_SELA']		= '';
									$data_sk['NO_SURAT']	= (!empty($req_no['NO_SURAT']) ? $req_no['NO_SURAT'] : '');
								}else{
									$save_sk = FALSE;
									$data['errors'] = $req_no['ERRORS'];
								}
								#ubah lagi KD_STATUS_SIMPAN KE 1
								$data_sk['KD_STATUS_SIMPAN'] = '1';
							}
							
						}
					} else { #1 (draf)
						$data_sk['NO_URUT']		= '';
						$data_sk['NO_SELA']		= '';
						$data_sk['NO_SURAT']	= '';
						#ubah lagi KD_STATUS_SIMPAN KE 1
						$data_sk['KD_STATUS_SIMPAN'] = '1';
					}
				}

				

				
				$key_diikutkan = ['ID_SURAT_KELUAR', 'KD_STATUS_SIMPAN', 'KD_JENIS_SURAT', 'ID_PSD', 'ATAS_NAMA', 'UNTUK_BELIAU', 'ID_KLASIFIKASI_SURAT', 'TGL_SURAT', 'STATUS_SK', 'PEMBUAT_SURAT', 'JABATAN_PEMBUAT', 'KD_JENIS_ORANG','UNIT_ID', 'UNTUK_UNIT', 'PERIHAL', 'TEMPAT_DIBUAT', 'TEMPAT_TUJUAN', 'KOTA_TUJUAN', 'ALAMAT_TUJUAN', 'LAMPIRAN', 'KD_SIFAT_SURAT', 'KD_KEAMANAN_SURAT', 'WAKTU_SIMPAN', 'KD_SISTEM', 'NO_URUT', 'NO_SELA', 'ISI_SURAT', 'KD_PRODI', 'NO_SURAT'];			
				$data_sk = array_intersect_key($data_sk, array_flip($key_diikutkan));

				#2. Melakukan API Insert ke SISURAT	(surat_keluar) - belum aktif
				if ($opsi == 'preview'){
					$data['surat_keluar']['umum'] = $data_sk; //assign data ke variabel lain
					$save_sk = TRUE;
				} elseif ($opsi == 'bernomor') {
					exit(json_encode($data_sk));
					$save_sk = TRUE;

					if($save_sk == TRUE){
						$parameter	= array('api_kode' => 90001, 'api_subkode' => 1, 'api_search' => array($data_sk));
						$save_sk 		= $this->apiconn->api_tnde('tnde_surat_keluar/insert_surat_kaluar', 'json', 'POST', $parameter);

						//exit(var_dump($data_sk));
						if($save_sk	==	FALSE || $save_sk == NULL){
							$data['errors'][] = "Gagal menyimpan surat. #0";
							$data['surat_keluar']['umum'] = FALSE;
						}else{
							$save_sk	=	TRUE;
							$data['surat_keluar']['umum'] = $data_sk;
						}
					}
				}

			
    			#3. ======== Melakukan API INSERT PENERIMA_SK KE SISURAT ========
    			$kpd_mahasiswa = (!empty($get_sesi['D_PENERIMA_MHS']) ? explode('<$>', $get_sesi['D_PENERIMA_MHS']) : null);
    			$kpd_pejabat = (!empty($get_sesi['D_PENERIMA_PJB']) ? explode('<$>', $get_sesi['D_PENERIMA_PJB']) : null);
    			$kpd_pegawai = (!empty($get_sesi['D_PENERIMA_PGW']) ? explode('<$>', $get_sesi['D_PENERIMA_PGW']) : null);
    			$ts_pejabat = (!empty($get_sesi['D_TEMBUSAN_PJB']) ? explode('<$>', $get_sesi['D_TEMBUSAN_PJB']) : null);
    			$ts_eks = (!empty($get_sesi['D_TEMBUSAN_EKS']) ? explode('<$>', $get_sesi['D_TEMBUSAN_EKS']) : null);

    			# 2. API SISURAT - INSERT PENERIMA SURAT KELUAR			
				if ($save_sk == TRUE){
	    			#============== 3.1 Pengelola Surat (PS)==============
	    			# 3.1 KIRIM SURAT KE PEJABAT (Yang menerangkan, menandatangani, dsb)
	    			if(!empty($kpd_pejabat[0])){
	    				$save_pjb = [];
						foreach($kpd_pejabat as $key=> $val){
							$exp = explode("#", $val);
							$get_pjb = $this->Repo_Simpeg->get_pejabat_aktif($data['surat_keluar']['umum']['TGL_SURAT'], $exp[0]);	
							if(count($exp) == 2){
								$pjb['ID_SURAT_KELUAR']				= $data_sk['ID_SURAT_KELUAR'];
								$pjb['PENERIMA_SURAT'] = $get_pjb[0]['NIP']; //NIP PEGAWAI
								$pjb['JABATAN_PENERIMA'] = $exp[0];
								$pjb['KD_GRUP_TUJUAN'] = 'PJB01';
								$pjb['KD_STATUS_DISTRIBUSI'] = "PS";
								$pjb['ID_STATUS_SM'] = 1;
								$pjb['KD_JENIS_TEMBUSAN'] = 0;
								$pjb['NO_URUT'] = $key + 1;
								$pjb['KD_JENIS_KEPADA']				= $exp[1];

								if ($opsi == 'preview'){
									$save_pjb[] = $pjb;	
								} elseif ($opsi == 'bernomor') {
									$savepjb = $this->apiconn->get_api('tnde_surat_keluar/insert_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($pjb)));
									if($savepjb == FALSE){
										$data['errors'][] = "Gagal mengirim ke tujuan surat ke ".$pjb['PENERIMA_SURAT'];
										$save_pjb[] = FALSE;	
									}elseif ($savepjb == TRUE) {
										$save_pjb[] = $pjb;	
									}
								}
							#....							
							}
						}

						$data['surat_keluar']['penerima']['PS'] = $save_pjb;
	    			}

	    			# 3.2 KIRIM SURAT KE MAHASISWA
	    			if(!empty($kpd_mahasiswa[0])){
	    				$save_mhs = [];
						foreach($kpd_mahasiswa as $key=> $val){
							$exp = explode("#", $val);
							if(count($exp) == 2){
								$mhs['ID_SURAT_KELUAR']				= $data_sk['ID_SURAT_KELUAR'];
								$mhs['PENERIMA_SURAT'] = $exp[0];
								$mhs['JABATAN_PENERIMA'] = '0';
								$mhs['KD_GRUP_TUJUAN'] = 'MHS01';
								$mhs['KD_STATUS_DISTRIBUSI'] = "PS";
								$mhs['ID_STATUS_SM'] = 1;
								$mhs['KD_JENIS_TEMBUSAN'] = 0;
								$mhs['NO_URUT'] = $key + 1;
								$mhs['KD_JENIS_KEPADA']				= $exp[1];
								

								if ($opsi == 'preview'){
									$save_mhs[] = $mhs;
									//array_push($data['surat_keluar']['penerima']['PS'], $mhs);
								} elseif ($opsi == 'bernomor') {
									$savemhs = $this->apiconn->get_api('tnde_surat_keluar/insert_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($mhs)));
									if($savemhs == FALSE || $savemhs == NULL){
										$data['errors'][] = "Gagal mengirim ke tujuan surat ke ".$exp[0];
										$save_mhs[] = FALSE;
									}elseif ($savemhs == TRUE) {
										$save_mhs[] = $mhs;
										$data['success'][] = "Berhasil mengirim ke tujuan surat ke ".$exp[0];
										//array_push($data['surat_keluar']['penerima']['PS'], $mhs);

									}
								}
								/*$save_mhs = $this->apiconn->get_api('tnde_surat_keluar/insert_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($mhs)));
								if($save_mhs == FALSE){
									$data['errors'][] = "Gagal mengirim ke tujuan surat ke ".$exp[0]; // Mahasiswa
								}*/
							}
						}

						// $data['surat_keluar']['penerima']['PS'] = $save_mhs;		
						$data['surat_keluar']['penerima']['PS'] = array_merge($data['surat_keluar']['penerima']['PS'], $save_mhs);
						$getmhs = $this->Repo_SIA->get_profil_mhs($save_mhs[0]['PENERIMA_SURAT'],'kumulatif');
						//exit(var_dump($getmhs));
						$data['prodi'] = $getmhs[0]['NM_PRODI'];
					}

					# 3.3 KIRIM SURAT KE EKSTERNAL
					$kpd_eks = (!empty($get_sesi['D_PENERIMA_EKS']) ? explode('<$>', $get_sesi['D_PENERIMA_EKS']) : null);
					if(!empty($kpd_eks[0])){
						$save_eks = [];					
						foreach($kpd_eks as $key => $val){
							$exp = explode("#", $val);

							if(count($exp) == 2){
								$eks['ID_SURAT_KELUAR']				= $data_sk['ID_SURAT_KELUAR'];
								$eks['PENERIMA_SURAT']				= $exp[0];
								$eks['JABATAN_PENERIMA']			= $exp[0]; //misal: kepala, kabag, sekda, dsb
								$eks['KD_GRUP_TUJUAN']				= "EKS01";
								$eks['KD_STATUS_DISTRIBUSI']		= "PS";	
								$eks['ID_STATUS_SM'] = 1;
								$eks['KD_JENIS_TEMBUSAN'] = 0;
								$eks['NO_URUT'] = $key + 1;
								$eks['KD_JENIS_KEPADA']				= $exp[1]; //1 = untuk perhatian , 0
								//$save_eks[] = $eks;

								if ($opsi == 'preview'){
									$save_eks[] = $eks;
								} elseif ($opsi == 'bernomor') {
									$saveeks = $this->apiconn->get_api('tnde_surat_keluar/insert_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($eks)));
									if($saveeks == FALSE || $saveeks == NULL){
										$data['errors'][] = "Gagal mengirim ke tujuan surat ke ".$exp[0];
										$save_eks[] = FALSE;
									}elseif ($saveeks == TRUE) {
										$save_eks[] = $eks;	
									}
								}
								/*$save_mhs = $this->apiconn->get_api('tnde_surat_keluar/insert_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($mhs)));
								if($save_mhs == FALSE){
									$data['errors'][] = "Gagal mengirim ke tujuan surat ke ".$exp[0]; // Mahasiswa
								}*/												
							}
						}

						// $data['distribusi_ps']['penerima_eksternal'] = $save_eks;
						$data['surat_keluar']['penerima']['PS'] = array_merge($data['surat_keluar']['penerima']['PS'], $save_eks);
						$data['tujuan_surat']['pimpinan_instansi'] = $save_eks[0]['PENERIMA_SURAT'];
						$data['tujuan_surat']['tempat_tujuan'] = $data['surat_keluar']['umum']['TEMPAT_TUJUAN'];
						//$data['tujuan_surat']['pimpinan_instansi'] = $save_eks[0]['PENERIMA_SURAT'];

					}

	    			#============== 3.2 Tembusan Surat (TS)============== 
	    			$data['surat_keluar']['penerima']['TS'] = [];   			
					if(!empty($ts_pejabat)){
						$ts_urut = 0;
						foreach($ts_pejabat as $key => $val){
							$exp = explode("#", $val);
							$get_pjb = $this->Repo_Simpeg->get_pejabat_aktif($data['surat_keluar']['umum']['TGL_SURAT'], $exp[0]);						
							if(count($exp) == 2 ){
								$ts_pjb['ID_SURAT_KELUAR'] 				= $data['surat_keluar']['umum']['ID_SURAT_KELUAR'];
								$ts_pjb['PENERIMA_SURAT'] 				= $get_pjb[0]['NIP']; //NIP PEGAWAI
								$ts_pjb['JABATAN_PENERIMA'] 			= $exp[0];
								$ts_pjb['KD_GRUP_TUJUAN'] 				= 'PJB01';
								$ts_pjb['KD_STATUS_DISTRIBUSI'] 	= "TS";
								$ts_pjb['ID_STATUS_SM'] 						= "1";
								$ts_pjb['NO_URUT']									= $ts_urut + 1;
								$ts_pjb['KD_JENIS_TEMBUSAN'] 		= (string)$exp[1]; #1 = sebagai laporan, 0 = biasa
								$ts_pjb['KD_JENIS_KEPADA'] 				= '0';					
								//$save_ts_pjb = $ts_pjb;

								if ($opsi == 'preview'){
									$save_ts_pjb[] = $ts_pjb;
								} elseif ($opsi == 'bernomor') {
									$savetspjb = $this->apiconn->get_api('tnde_surat_keluar/insert_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($ts_pjb)));
									if($savetspjb == FALSE || $savetspjb == NULL){
										$data['errors'][] = "Gagal mengirim ke tujuan surat ke ".$exp[0];
										$save_ts_pjb[] = FALSE;
									}elseif ($savetspjb == TRUE) {
										$save_ts_pjb[] = $ts_pjb;	
									}
								}

								/*$parameter	= array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($ts_pjb));
								$save_ts_pjb = $this->mdl_tnde->get_api('tnde_surat_keluar/insert_penerima_sk', 'json', 'POST', $parameter);
								if($save_ts_pjb == FALSE){
									$data['errors'][] = "Gagal mengirim tembusan surat ke ".$exp[3]; // Pejabat
								}*/
							}
						}

						// $data['distribusi_ts']['tembusan_pejabat'] = $save_ts_pjb;
						$data['surat_keluar']['penerima']['TS'] = array_merge($data['surat_keluar']['penerima']['TS'], $save_ts_pjb);
					}
				}

			#4. Melakukan API Insert ke AUTOMASI  				
				#======= 4.1 SIMPAN DATA BERDASARKAN JENIS SURATNYA ====
				$detail_surat_sesi = unserialize($get_sesi['DETAIL_SURAT']);
				//$tgl_surat_pg = DateTime::createFromFormat('d/m/Y', $data['success']['data']['TGL_SURAT']); //obj tanggal dengan format sesuai postgresql (API AUTOMASI)
				$tgl_surat_pg = DateTime::createFromFormat('d/m/Y', $data_sk['TGL_SURAT']); //obj tanggal dengan format sesuai postgresql (API AUTOMASI)				
				$detail_skr_automasi = $detail_surat_sesi;
				$tgl_bmb = isset($detail_surat_sesi['CONFIG_MASA_BERLAKU']) ? $tgl_surat_pg->add(new DateInterval($detail_surat_sesi['CONFIG_MASA_BERLAKU'])) : null;
				$detail_skr_automasi['BATAS_MASA_BERLAKU'] = isset($tgl_bmb) ?  $tgl_bmb->format('Y-m-d') : null;

				$detail_skr_automasi = array_diff_key($detail_skr_automasi, array_flip(['CONFIG_MASA_BERLAKU'])); //hapus kolom tidak dibutuhkan

				$header_skr_automasi = [					
					'ID_SURAT_KELUAR' => $data['surat_keluar']['umum']['ID_SURAT_KELUAR'], //'ID_SURAT_KELUAR' => $data['success']['data']['ID_SURAT_KELUAR'],	
					'NO_SURAT' => $data['surat_keluar']['umum']['NO_SURAT'], //'NO_SURAT' => $data['success']['data']['NO_SURAT'],
    				'TGL_SURAT' => $tgl_surat_pg->format('Y-m-d'), //'TGL_SURAT' => $data_sk['TGL_SURAT'],       				
    				'PEMBUAT_SURAT' => $data['surat_keluar']['umum']['PEMBUAT_SURAT'], //'PEMBUAT_SURAT' => $data['success']['data']['PEMBUAT_SURAT'],
    				//'NIM' => ($data['success']['data']['KD_JENIS_ORANG'] == 'M' ? $data['success']['data']['PEMBUAT_SURAT'] : ''), 		
    				'KD_JENIS_SAKAD' => $detail_skr_automasi['KD_JENIS_SAKAD']
				];
				$data_skr_automasi = array_merge($header_skr_automasi, $detail_skr_automasi);	



							
				if ($opsi == 'preview'){
					$data['detail_surat_automasi'] = $data_skr_automasi;
				}elseif ($opsi == 'bernomor'){
					if (empty($data['errors'])){
						$parameter	= array('api_kode'=>14004, 'api_subkode' => 1, 'api_search' => array($data_skr_automasi));
						$insert_detail_skr_automasi = $this->apiconn->api_tnde_sakad('tnde_surat_akademik/insert_detail_skr_automasi', 'json', 'POST', $parameter);
						if (!empty($insert_detail_skr_automasi['data'])){
							$data['success'][] = 'Berhasil menambahkan data detail surat ke sistem automasi';
							$data['detail_surat_automasi'] = $insert_detail_skr_automasi['data'];
						} else {
							$data['detail_surat_automasi'] = FALSE;
							$data['errors'][] = 'Gagal menambahkan data detail surat ke sistem automasi';
						}
					}	
				}

				$kd_jenis_sakad_int = (int)$data['detail_surat_automasi']['KD_JENIS_SAKAD'];
				$arr_jenis_sakad = $this->Repo_Automasi_Surat->get_jenis_surat_mahasiswa('kd', $kd_jenis_sakad_int);
				$data['raw_jenis_sakad'] = $arr_jenis_sakad;
				
				 

				// $pjb_ps = explode('#', $get_sesi['D_PENERIMA_PJB']);
				// $pjb_ts = explode('#', $get_sesi['D_TEMBUSAN_PJB']);
				// $tmp_verified_mhs = $this->session->userdata('tmp_verified_mhs');
				// $data['bagian_surat_lain'] = [
				// 	'opsi_cetak' => $opsi,
				// 	'judul_jenis' => $detail_surat_sesi['NM_JENIS_SAKAD'],
				// 	'path' => $detail_surat_sesi['PATH_JENIS_SAKAD'],
				// 	'kd_unit_prodi' => $tmp_verified_mhs['KD_PRODI'],
				// 	'nm_unit_prodi' => $tmp_verified_mhs['NM_PRODI'],
				// 	'kd_jabatan_psd' => $pjb_ps[0], #Set Kode Jabatan Penandatangan sesuai pejabat yg menerangkan dlm surat
				// 	'kd_jabatan_atas_nama' => $pjb_ts[0] #Set Kode Jabatan Penandatangan sesuai tembusan pertama surat
				// ];

				## 3. UNIT
				### 3.1 Unit penerbit surat
				$parameter = array('api_kode' => 1901, 'api_subkode' => 2, 'api_search' => array($data['surat_keluar']['umum']['TGL_SURAT'], $data['surat_keluar']['umum']['UNIT_ID']));
				$data['unit_penerbit'] = $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);

				## PEGAWAI/PEJABAT PENANDATANGANAN disebut dalam surat
				$pjb_ps = explode('#', $get_sesi['D_PENERIMA_PJB']);
				$pjb_ts = explode('#', $get_sesi['D_TEMBUSAN_PJB']);

				$skr_umum = $data['surat_keluar']['umum'];
				if (!empty($skr_umum['ID_PSD'])){					
					$pgw_psd = $this->Repo_Simpeg->get_pejabat_aktif($skr_umum['TGL_SURAT'], $pjb_ps[0]);
					$nip_pgw = $pgw_psd[0]['NIP'];

					$parameter = array('api_kode' => 1123, 'api_subkode' => 3, 'api_search' => array($skr_umum['TGL_SURAT'], $nip_pgw));
					$pang_gol = $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
					
					$data['psd_detail']['pangkat'] = (!empty($pang_gol) ? $pang_gol[0] : []);
					$data['psd_detail']['pegawai'] = $pgw_psd;
				}
				if (!empty($skr_umum['ATAS_NAMA'])){
					$an = $this->apiconn->api_tnde('tnde_general/get_data', 'json', 'POST', array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('MD_PSD', 'ID_PSD', $skr_umum['ATAS_NAMA'])));
					if(!empty($an['KD_JABATAN'])){
						$parameter = array('api_kode' => 1121, 'api_subkode' => 9, 'api_search' => array($skr_umum['TGL_SURAT'], $an['KD_JABATAN'], 3));
						$data['psd_detail']['atas_nama'] = $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
					}			
					$data['psd_ringkas']['label_psd_an'] = $data['psd_detail']['atas_nama'][0]['STR_NAMA_S1']; //$data['label_psd_an'] = $pgw_psd_an[0]['STR_NAMA_A212'];
				}
				$data['psd_ringkas']['label_psd'] = $pgw_psd[0]['STR_NAMA_S1'];
				$data['psd_ringkas']['nm_pgw_psd'] = $pgw_psd[0]['NM_PGW_F'];

				## PEJABAT disebut dalam surat
				$psd_profil_pegawai = array_intersect_key($pgw_psd[0], array_flip(['NIP','NM_PGW_F','UNIT_NAMA','STR_NAMA','STR_NAMA_A212']));
				$psd_pangkat_pegawai = array_intersect_key($data['psd_detail']['pangkat'], array_flip(['HIE_GOLRU','HIE_NAMA']));
				$data['pejabat_dalam_surat'] = array_merge($psd_profil_pegawai, $psd_pangkat_pegawai);

				## MAHASISWA disebut dalam surat				
				$data['mhs_dalam_surat'] = unserialize($get_sesi['DETAIL_MHS']);
				
			# 5. GENERATE PDF
				

				
				if (empty($data['errors'])){
					//$token_keaslian = $this->test_terbitkan_jwt();
					//$data['qrcode'] = base_url().'cek_keaslian/'.$token_keaslian;					
					//$this->generate_pdf_surat($data);

					
					if ($mode === 'pdf'){
						$this->generate_pdf_surat_v2($data);
					} elseif ($mode === 'debug') {
						exit(var_dump($data));	
					}		

					//lanjut hapus sesi					
				} else {
					$this->output
		       			->set_status_header(200)
		       			->set_content_type('text/html')		       			
		       			->set_output(json_encode($data['errors']));
				}

				if ($opsi == 'bernomor'){
					$del_sesi = $this->Automasi_model->hapus_sesi_automasi_buat($get_sesi['ID_SESI']);
					$data['hapus_sesi_sebelumnya'] = $del_sesi;	
				}

				//(!empty($get_sesi['TMP_MHS']) ? unserialize($get_sesi['TMP_MHS']): NULL) ;
				//$data['embed_qrcode']['tokenstring_ck1'] = 'http://exp.uin-suka.ac.id/surat/scanqrcode/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ'; //CONTOH
				//$this->load->view('reports/v_sampel_report', $data['embed_qrcode']);

			# 6. HAPUS SESI
				/*
				if (empty($data['errors'])){
					$del_sesi = $this->Automasi_model->hapus_sesi_automasi_buat($get_sesi['ID_SESI']);
					$data['hapus_sesi_sebelumnya'] = $del_sesi;	
				}
				
				exit(print_r($data));
				*/
				//exit(var_dump($api_insert_skr_automasi));			

    		} else {
    			exit("Maaf , penyimpanan surat gagal karena masalah teknis, silakan merefresh halaman untuk mengulang pembuatan surat");
    		}
    	} 

    	if (isset($_POST['btn_save_skr_ttd']) && $opsi == 'kirim_perm_ttd' ){
    		//exit('sedang memproses permohonan pembuatan surat skema penandatanganan digital');	
    		$data['nama_jabatan'] = 'Wakil Dekan Bidang Akademik';
    		$data['nama_pegawai'] = 'Dr. Agung Fatwanto, S.Si., M.Kom.';
    		$data['nip'] = '197701032005011003';

    		$response = $this->load->view('v_perm_ttd',$data, true);
			$this->output
				->set_status_header(200)				
				->set_content_type('text/html')		       			
				->set_output($response);
    	}

    	#cetak ulang (sumber data dari remote API , bukan lagi parameter dalam bentuk arrays)
    	if (isset($_POST['print_surat_bernomor_tersimpan'])) {} #SURAT BARU SAJA DISIMPAN DAN BELUM DIAJUKAN KE TU

    	if (isset($_POST['print_surat_ttd'])) {} #CETAK SURAT YANG TELAH DITANDATANGANI SCR DIGITAL
    }

}