<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');
Class Akademik extends MY_Controller{
	
	public function __construct(){
		parent::__construct();
		$this->load->library('common/Curl');
		$this->load->model('common/Mdl_tnde','apiconn');
		$this->load->model('Repo_SIA');
	}
	

	function auto_mahasiswa(){
		if(ISSET($_POST['q'])){
			$referer = (!empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '');
			$auth = host_allowed($referer);
			if($auth){
				$q = strtoupper(str_replace("'", "''", $_POST['q']));
				$tot = count(str_split($q));
				if($tot > 2){
					$data = $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', array('api_kode' => 26000, 'api_subkode' => 26,'api_search' => array($q)));
					if(count($data) > 0){
						foreach($data as $val){
							$value = $val['NIM']."#0#MHS01#MAHASISWA#".$val['NAMA_F'];
							$return_arr[] = array('name' => $val['NIM'].' - '.$val['NAMA_F'], 'id' => $value, 'nim' => $val['NIM']);
						}
						$joss = json_encode($return_arr);
						echo $joss;
					}else{
						echo 0;
					}
				}
			}
		}
	}

	function auto_mahasiswa_sl2(){
		if(ISSET($_GET['q'])){
			 //$referer = (!empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '');
			 //$auth = host_allowed($referer);
			  //if($auth){
				$q = strtoupper(str_replace("'", "''", $_GET['q']));
				$tot = count(str_split($q));
				if($tot > 2){
					$data = $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', array('api_kode' => 26000, 'api_subkode' => 26,'api_search' => array($q)));
					if(count($data) > 0){
						foreach($data as $val){
							$value = $val['NIM']."#0#MHS01#MAHASISWA#".$val['NAMA_F'];
							$return_arr[] = array('text' => $val['NIM'].' - '.$val['NAMA_F'], 'id' => $value, 'nim' => $val['NIM']);
						}
						$joss = json_encode($return_arr);
						echo $joss;
					}else{
						echo 0;
					}
				}
			 //}
		}	
	}
	
	function auto_kabupaten(){
		if(ISSET($_POST['q'])){
			$referer = (!empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '');
			$auth = host_allowed($referer);
			if($auth){
				$q = strtoupper(str_replace("'", "''", $_POST['q']));
				$tot = count(str_split($q));
				if($tot > 2){
					$data = $this->apiconn->api_sia('sia_master/data_search', 'json', 'POST', array('api_kode' => 12000, 'api_subkode' => 3,'api_search' => array($q)));
					if(count($data) > 0){
						foreach($data as $val){
							$nm_kab = (!empty($val['NM_KAB1']) ? $val['NM_KAB1'].". ".$val['NM_KAB2'] : $val['NM_KAB2']);
							$return_arr[] = array('name' => strtoupper($nm_kab), 'id' => $val['KD_KAB']);
						}
						$joss = json_encode($return_arr);
						echo $joss;
					}else{
						echo 0;
					}
				}
			}
		}
	}

	#SELECT2 AJAX
	function auto_kabupaten_sl2(){
		if(ISSET($_GET['search'])){
			// $referer = (!empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '');
			// $auth = host_allowed($referer);
			// if($auth){
				$q = strtoupper(str_replace("'", "''", $_GET['search']));
				$tot = count(str_split($q));
				if($tot > 2){
					$data = $this->apiconn->api_sia('sia_master/data_search', 'json', 'POST', array('api_kode' => 12000, 'api_subkode' => 3,'api_search' => array($q)));
					if(count($data) > 0){
						foreach($data as $val){
							$nm_kab = (!empty($val['NM_KAB1']) ? $val['NM_KAB1'].". ".$val['NM_KAB2'] : $val['NM_KAB2']);
							$return_arr['results'][] = array('id' => $val['KD_KAB'], 'text' => strtoupper($nm_kab));
						}
						$joss = json_encode($return_arr);
						header("Content-type:application/json");
						echo $joss;
					}else{
						echo 0;
					}
				}
			//}
		}

	}

	function auto_kabupaten2(){
		if(ISSET($_POST['q'])){
			$referer = (!empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '');
			$auth = host_allowed($referer);
			if($auth){
				$q = strtoupper(str_replace("'", "''", $_POST['q']));
				$tot = count(str_split($q));
				if($tot > 2){
					$data = $this->apiconn->api_sia('sia_master/data_search', 'json', 'POST', array('api_kode' => 12000, 'api_subkode' => 3,'api_search' => array($q)));
					if(count($data) > 0){
						foreach($data as $val){
							$nm_kab = (!empty($val['NM_KAB1']) ? $val['NM_KAB1'].". ".$val['NM_KAB2'] : $val['NM_KAB2']);
							$return_arr[] = array('text' => strtoupper($nm_kab), 'id' => $val['KD_KAB']);
						}
						$joss = json_encode($return_arr);
						echo $joss;
					}else{
						echo 0;
					}
				}
			}
		}
	}

	function auto_pejabat_eks(){
		if(isset($_GET['term'])){
			$return_arr = array();
			$q = strtoupper($_GET['term']);
			$parameter	= array('api_kode' => 90001, 'api_subkode' => 1, 'api_search' => array($q));
			$data 				= $this->apiconn->api_tnde('tnde_surat_keluar/get_tujuan_eks', 'json','POST', $parameter);
			$hasil = array();
			foreach($data as $val){
				$hasil[] = htmlentities($val['PENERIMA_SURAT']);
			}
			echo json_encode($hasil);
		}
	}


	/**
	* Dirancang default untuk plugin select2.js
	*/
	public function auto_makul_smt()
	{
		if ($this->session->userdata('log') == 'in'){			
			// if(isset($_GET['term'])){
				// $return_arr = array();
				// $q = strtoupper($_GET['term']);
				$data = $this->Repo_SIA->get_makul_smt($this->session->userdata('username'));				
				//exit(print_r($data));
				$hasil = array();
				foreach($data as $key=>$val){
					$hasil[$key]["id"] = $val["KD_MK"];
					$hasil[$key]["text"] = $val["NM_MK"].'  (Dosen : '.$val["NM_DOSEN_F"].')';
				}
				header('Content-Type: application/json;');
				echo json_encode($hasil);
			// }
			
		} else {
			echo json_encode(
				[
					[
						'id' => '',
						'text' => 'SESI AND TELAH HABIS, SILAKAN LOGIN ULANG'
					]
				]
			);
		}
	}

	public function auto_makul_smt_powerfull($nim)
	{					
		$data = $this->Repo_SIA->get_makul_smt($nim);
		
		if (!empty($data)){
			$hasil = array();
			foreach($data as $key=>$val){
				$hasil[$key]["id"] = $val["KD_MK"];
				$hasil[$key]["text"] = $val["NM_MK"].'  (Dosen : '.$val["NM_DOSEN_F"].')';
			}
			header('Content-Type: application/json;');
			echo json_encode($hasil);
		} else {
			header('Content-Type: application/json;');
			echo json_encode(
				[
					[
						'id' => '',
						'text' => 'data tidak ditemukan',
						'error' => 'krs kosong atau null data'
					]
				]
			);
		}	
	}


	public function method_dengan_return()
	{
		return array('message'=>'oke');
	}

}