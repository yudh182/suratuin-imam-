<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');
class Psd_digital extends MY_Controller{
	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('log') !== 'in' && $this->session->userdata('username') !== '11650021')
			redirect(site_url('pgw/login?from='.current_url())); 	
		$this->load->module('pgw/api/Api_foto','api_foto');			
	}

	public function index()
	{
		$view = 'v_home_psd_digital';
		$this->load->view('templates/v_header');
		$this->load->view('templates/v_sidebar');
		$this->load->view($view);
		$this->load->view('templates/v_footer');
	}
	public function inbox()
	{		
		$view = 'v_psd_digital/v_inbox';
		$this->load->view('templates/v_header');
		$this->load->view('templates/v_sidebar');
		$this->load->view($view);
		$this->load->view('templates/v_footer');
	}

	public function detail_permohonan()
	{

	}

	public function aksi_menyetujui()
	{}

	/**
	* Menunda persetujuan dengan mengirimkan catatan ke pemohon dan atau pegawai pelaksana (TU) terkait
	*/
	public function aksi_tunda()
	{}

	public function aksi_menolak()
	{}

	protected function terbitkan_signature()
	{}

	/**
	* Unduh file kunci dari Server akibat kehilangan file (.pem) atau sedang tidak membawa trusted device sebelumnya
	* proteksi : menginputkan password
	*/
	protected function unduh_kunci()
	{}

}