<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');
class Test_baru extends MY_Controller {
	
	public function __construct()
	{
		parent::__construct();		

		//$this->load->library('common/Curl'); #CEK APAKAH AUTOLOAD PADA MODULE BERHASIL
		//$this->load->model('common/Mdl_tnde','apiconn');
		//$this->load->model('Test_model');
		//$this->load->model('Verifikasi_model');
		/**
		* tentang module loader
		 * jika suatu controller dimuat, maka semua class (library dan model) dan helper bisa langsung diakses oleh controller yang memuat
		 */
		//$this->load->module('pgw/Test_automasi'); //untuk menggunakan object gunakan lowercase
		$this->load->module('pgw/api/Api_foto', 'api_foto');
    }

    public function index()
    {
    	//echo print_r($this->Test_model->get_profil_mhs('11650021','kumulatif'));
    	$this->test_automasi->test_api_photo2('11650021');
    }

    //mengakses model Verifikasi , yang mana tanpa ijin telah menggunakan methodnya Automasi
    public function coba_rumput_tetangga()
    {
    	echo print_r($this->Verifikasi_model->akses_method_model_tetangga());
    }

    /**
    * Percobaan testing, verifikasi memuat class automasi yang sebenarnya telah dimuat oleh controller Test_automasi. 
    hasil: muat double di controller dan memuat lagi di model tidak menyebabkan masalah apapun
    */
    public function test_tek($opsi='')
	{		
		echo print_r($this->Verifikasi_model->akses_method_model_tetangga());
	}

	public function isiform()
	{
		$this->load->view('templates/v_header');
		$this->load->view('templates/v_sidebar');
		$this->load->view('v_eksperimen/v_exp_buat_surat_ijin');
		$this->load->view('templates/v_footer');
	}

	/**
	* Coba menggunakan $this->apiconn tanpa di load lewat constructor
	* hasil : bisa menggunakan instance $CI->apiconn karena masih satu module
	*/
	public function coba_autoload_alias()
	{
		$parameter =array('api_kode'		=> 13001, 'api_subkode'	=> 1, 'api_search'	=> array());
		$data['jenis_sakad'] = $this->apiconn->api_tnde_sakad('tnde_master_mix/get_jenis_surat_akademik', 'json', 'POST', $parameter);

		exit(print_r($data));
	}

	public function cobatanggal()
	{
		$tgl = DateTime::createFromFormat('Y-m-d', '2018-02-24')->format('d/m/Y');

		//var_dump($tgl);
		var_dump(time());
	}

	public function coba_openssl($opsi='rpb')
	{
		if ($opsi == 'rpb') {//random psedu bytes
		$rpb = openssl_random_pseudo_bytes(12);		
			echo var_dump(bin2hex($rpb));
		} elseif ($opsi == 'encrypt') {
			$key = openssl_random_pseudo_bytes(12);
			//$key should have been previously generated in a cryptographically safe way, like openssl_random_pseudo_bytes
			$plaintext = "message to be encrypted";
			$cipher = "aes-128-gcm";
			if (in_array($cipher, openssl_get_cipher_methods()))
			{
			    $ivlen = openssl_cipher_iv_length($cipher);
			    $iv = openssl_random_pseudo_bytes($ivlen);
			    $ciphertext = openssl_encrypt($plaintext, $cipher, $key, $options=0, $iv, $tag);
			    //store $cipher, $iv, and $tag for decryption later
			    $original_plaintext = openssl_decrypt($ciphertext, $cipher, $key, $options=0, $iv, $tag);
			    echo $original_plaintext."\n";

			    echo '<h3>Encrypted text version : <br>';
			    echo $ciphertext;
			}
		}
	}


	/**
	* Library Random Compat untuk dapat menggunakan random_bytes() dan random_int() yang baru tersedia di PHP7
	* alasan : 
	*/
	public function coba_librandom($length=null, $representation='hex')
	{
		#Muat package (3rd party) random_compat dengan helper custom ini
		$this->load->helper('common/Package_loader');
		load_package_random_compat();

		try {
    		$string = random_bytes($length);
		} catch (TypeError $e) {
		    // Well, it's an integer, so this IS unexpected.
		    die("An unexpected error has occurred"); 
		} catch (Error $e) {
		    // This is also unexpected because 32 is a reasonable integer.
		    die("An unexpected error has occurred");
		} catch (Exception $e) {
		    // If you get this message, the CSPRNG failed hard.
		    die("Could not generate a random string. Is our OS secure?");
		}
		
		//var_dump(random_int(0, 100));	
		if ($representation == 'hex')
		{
			var_dump(bin2hex($string));	
		} elseif ($representation == 'base64') {
			var_dump(base64_encode(bin2hex($string)));	
		}
			
	}

	public function string($tool='', $input=null)
	{
		if ($tool == 'panjang_karakter'){
			echo strlen($input);
		}
	}

	public function tanggalan_server($mode='tanggal')
	{
		switch ($mode) {
			case 'tanggal':
				echo date('d/m/Y h:i:s');
				break;
			case 'timezone':
				echo date_default_timezone_get();
				break;
			case 'list_timezone':
				echo var_dump(timezone_identifiers_list());
				break;
			case 'list_timezone_abbre':
			echo json_encode(timezone_abbreviations_list(), JSON_PRETTY_PRINT);
			break;
			default:
				# code...
				break;
		}		
	}
}