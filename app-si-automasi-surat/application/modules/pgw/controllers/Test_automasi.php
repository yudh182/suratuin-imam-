<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');

use \Firebase\JWT\JWT;
/**
* HTTP Controller Surat Akademik untuk user mahasiswa
 * dependency modules :
 * - surat_keluar
 * - admin_master
 * - repo_sisurat

 * PR : - bagaimana menjadikan autoload tanpa composer, cukup use??
 * Solusi sementara : include file-file JWT di core/MY_Controller
*/
class Test_automasi extends MY_Controller {
    
    public function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('log') !== 'in')
            redirect(site_url('pgw/login?from='.current_url()));        

        //$this->load->library('common/Curl');  #selama berada di modul pgw, Curl sudah di autoload otomatis
        //$this->load->model('common/Mdl_tnde', 'apiconn');
        $this->load->model('Repo_SIA');
        $this->load->model('Repo_Surat');               
        $this->load->model('Automasi_model');
        $this->load->model('Verifikasi_model'); 
    }

    public function cek_passing_data()
    {
        /*$raw_kpd_mahasiswa = $this->input->post('kpd_mahasiswa');
        $kpd_urut           = $this->input->post('kpd_urut');
        echo $raw_kpd_mahasiswa;
        echo '<br><br>';
        echo print_r($kpd_urut);*/
        //echo var_dump($_POST);

        /*$bpd_nama               = $this->input->post('nm_beasiswa_pd'); 
        $bpd_keterangan         = $this->input->post('ket_beasiswa_pd');
        $tot = count($bpd_nama);
        $no = 0;
        if(!empty($bpd_nama)){
            $tdk_men_bea = [];
            foreach($bpd_nama as $n => $val){
                $no++;

                $item_bpd_nama = $bpd_nama[$n];                 
                if(!empty($item_bpd_nama)){ 
                    $tdk_men_bea[$n]['NAMA_BEASISWA'] = $bpd_nama[$n];                       
                    $tdk_men_bea[$n]['KETERANGAN_BEASISWA'] = $bpd_keterangan[$n];
                    $tdk_men_bea[$n]['NO_URUT']               = $no;
                }
                
            }
        }
        $tdk_men_bea = array_values($tdk_men_bea); //re-order index key
        */

        //echo json_encode($tdk_men_bea, JSON_PRETTY_PRINT);
        echo print_r($_POST);
    }

    public function cek_serialize()
    {
        $str = 'a:3:{i:0;a:2:{s:3:"NIM";s:8:"15650051";s:4:"NAMA";s:17:"DIDIK EKO PRAMONO";}i:1;a:2:{s:3:"NIM";s:8:"15650020";s:4:"NAMA";s:18:"ERNA NOVIA SAFITRI";}i:2;a:2:{s:3:"NIM";s:8:"15650010";s:4:"NAMA";s:17:"DHONI ARI NUGROHO";}}';

        echo var_dump(unserialize($str));
    }

    public function testing_bea($nim)
    {
        var_dump($this->Verifikasi_model->testing_bea($nim));
        
    }

# ************************ TESTING terkait API SIA ************************ #
    
    public function api_wizard()
    {
        $this->load->view('v_apisia_wizard/api_wizard');
    }
    public function get_makul_smt($nim=null,$opsi='')
    {
        $CI =& get_instance();

        $kd_makul = null;        

        // if (empty($opsi)){
        //     $ta = $CI->s00_lib_api->get_api_json(URL_API_SIA.'sia_krs/data_procedure', 'POST', array('api_kode'=>50000, 'api_subkode'=>2));
        //     $cek=$CI->s00_lib_api->get_api_json(URL_API_SIA.'sia_krs/data_search', 'POST', array('api_kode'=>64000, 'api_subkode' => 22, 'api_search' => array($ta['data'][':hasil1'],$ta['data'][':hasil3'],$nim)));
        // }            
        $ta = $CI->s00_lib_api->get_api_json(URL_API_SIA.'sia_krs/data_procedure', 'POST', array('api_kode'=>50000, 'api_subkode'=>2));

        $hasil = [];
        for ($i=1; $i < 21; $i++) { 
            $cek=$CI->s00_lib_api->get_api_json(URL_API_SIA.'sia_krs/data_search', 'POST', array('api_kode'=>64000, 'api_subkode' => $i, 'api_search' => array($nim))); 
            $hasil[] = $cek;
            //echo 'Hasil Loop ke - '.$i;
            echo var_dump($hasil);            
        }                    
    }

    public function get_khs_smt($nim=null,$opsi='')
    {
        $CI =& get_instance();
        $hasil = [];
        for ($i=1; $i < 20; $i++) { 
            $cek=$CI->s00_lib_api->get_api_json(URL_API_SIA.'sia_khs/data_search', 'POST', array('api_kode'=>72000, 'api_subkode' => $i, 'api_search' => array($nim))); 
            $hasil[] = $cek;
            echo 'Hasil Loop ke - '.$i.' <br><hr>';
            echo var_dump($hasil);            
            echo '<hr>';
        }                    
    }

    public function test_5($nim)
    {
         $CI =& get_instance();
            $cek=$CI->s00_lib_api->get_api_json(URL_API_SIA.'sia_krs/data_search', 'POST', array('api_kode'=>64000, 'api_subkode' => 5, 'api_search' => array($nim)));         
            return $cek;
        
    }

    public function test_17($nim)
    {
            $CI =& get_instance();
            $cek=$CI->s00_lib_api->get_api_json(URL_API_SIA.'sia_krs/data_search', 'POST', array('api_kode'=>64000, 'api_subkode' => 17, 'api_search' => array($nim)));      
            return $cek;          
       
    }

    /**
* Takes an array and returns an array of duplicate items
*/
    protected function get_duplicates( $array ) {
        return array_unique( array_diff_assoc( $array, array_unique( $array ) ) );
    }

    public function cek_bebasteori()
    {
        $cek = $this->test_17('11650021');
        //$data['krs_all'] = $cek;
        $list_kd_mk = array_column($cek, 'KD_MK');
        $list_nm_mk = array_column($cek, 'NM_MK');        
        $list_bobotnilai = array_column($cek, 'BOBOT_NILAI');

        $kd_makul_duplikat = $this->get_duplicates($list_kd_mk);       
        //$nm_makul_duplikat = $this->get_duplicates($list_nm_mk);

        $data['duplikat'] = $kd_makul_duplikat;

        /*echo "kode makul : <br><hr>";
        echo var_dump($list_kd_mk);

        echo "makul duplikat : <br><hr>";
        echo var_dump($nm_makul_duplikat);
        */
        $new_makuls = [];
        $duplikat_makuls = [];
        foreach ($cek as $key => $val) {
            if ($val["KD_TA"] == '2017' && $val["KD_SMT"] == '2'){
                continue;
            }

            if (empty($val["NILAI"])){

                continue;
            }

            #Tampung makul yang duplikat tp memiliki nilai (untuk komparasi)
            if (!empty($val["NILAI"]) && in_array($val["KD_MK"], $kd_makul_duplikat)){
                $duplikat_makuls[$val["KD_MK"]][] = $val;
                continue;

            }
            $new_makuls[] = $val;
        }
        $data['krs_bernilai'] = $new_makuls;

        $data['duplikat_makul'] = $duplikat_makuls;       
        $dmk_keys = array_keys($duplikat_makuls);       
        $makul_terpilih = [];
		foreach ($dmk_keys as $key) {
			$max = -9999999; //will hold max val
			$found_item = null; //will hold item with max val;
			//$arr = array(array('Total' => 13, 'Key1' => 'somethinga'),array('Total' => 3, 'Key1' => 'something b'),array('Total' => 43, 'Key1' => 'something c'));
			foreach($duplikat_makuls[$key] as $k=>$v)
			{
			    if( (float)$v['BOBOT_NILAI'] > $max )
			    {
			       $max = (float)$v['BOBOT_NILAI'];
			       $found_item = $v;
			    }
			}

			//echo "max value is $max<br>";
			$makul_terpilih[] = $found_item; 
		}
			
        
		foreach ($makul_terpilih as $k=>$v) {
			$data['krs_bernilai'][] = $v;
		}
		      
        echo var_dump($data['krs_bernilai']);
    }

    public function test_18($nim)
    {
        $CI =& get_instance();
        $cek=$CI->s00_lib_api->get_api_json(URL_API_SIA.'sia_krs/data_search', 'POST', array('api_kode'=>64000, 'api_subkode' => 18, 'api_search' => array($nim)));         
        echo var_dump($cek);
    }

    public function test_19($nim)
    {
 $CI =& get_instance();
            $cek=$CI->s00_lib_api->get_api_json(URL_API_SIA.'sia_krs/data_search', 'POST', array('api_kode'=>64000, 'api_subkode' => 19, 'api_search' => array($nim)));         
            echo var_dump($cek);            
        
    }
    public function test_20($nim)
    {
 $CI =& get_instance();
            $cek=$CI->s00_lib_api->get_api_json(URL_API_SIA.'sia_krs/data_search', 'POST', array('api_kode'=>64000, 'api_subkode' => 20, 'api_search' => array($nim)));         
            echo var_dump($cek);            
        
    }


    //======kusus foto==============
    public function test_api_photo()
    {
        
        $nim =  $_GET['nim'];
        
        $nim        = preg_replace("/[^0-9]/", "", $nim);
        //$api_url    = URL_APISIA.'foto/ns_mahasiswa_auto/'.$nim;
        $api_url    = URL_APISIA.'foto/ns_mahasiswa_auto/'.$nim;
        
        header("Content-type: image/jpeg");              
        $bn=file_get_contents($api_url);        
        echo  $bn;
        /*if($bn ==''){
             $url = $this->tf_encode('FOTOMASUK#'.$nim.'#QL:100#WM:1#SZ:300');
             echo file_get_contents(URL_APISIA.'foto/pgw/990/'.$url.'.jpg');
        
         }else{
            
             $url = $this->tf_encode('FOTOAUTO#'.$nim.'#QL:100#WM:1#SZ:300');
             $ur=URL_APISIA."foto/mhs/990/$url.jpg";
        

             $opts['http']['header'] .= "Referer: http://exp.uin-suka.ac.id/\r\n";
             $context = stream_context_create($opts);
             echo file_get_contents($ur, false, $context); 
        
        } */                
    }

    function foto_mahasiswa($id_user=null){
        // if ($this->session->userdata('log')=='in') {
        //     $nim = $this->user;
        //     if (isset($id_user)) {
        //         $nim = $id_user;
        //     }
        $nim = $_GET['nim'];
        $nim        = preg_replace("/[^0-9]/", "", $nim);
        $api_url    = URL_APISIA.'foto/ns_mahasiswa_auto/'.$nim;
        
        // $builder = http_build_query(array('g3mb0k' => '&&&&&&&**********'),'','&');
        // $opts = array('http' =>
        //      array(
        //      'method'  => 'POST',
        //      'header'  => 'Content-type: application/x-www-form-urlencoded',
        //      'content' => $builder
        //      )
        //  );
        
        
            
            
                header("Content-type: image/jpeg");
                
                $bn=file_get_contents($api_url);
                if($bn==''){
                    //echo $bn;
                    // echo file_get_contents(base_url().'asset/default_avatar.jpg');
                    // $url = $this->tf_encode('FOTOAUTO#'.$nim.'#QL:100#WM:0#SZ:300');
                    // echo '<img src="http://static.uin-suka.ac.id/foto/pgw/990/'.$url.'.jpg">';
                    // echo file_get_contents('http://static.uin-suka.ac.id/foto/pgw/990/'.$url.'.jpg');
                    $url = $this->tf_encode('FOTOMASUK#'.$nim.'#QL:100#WM:1#SZ:300');
                    echo file_get_contents(APP_STATIC.'foto/pgw/990/'.$url.'.jpg');
                    // echo '<img src="http://static.uin-suka.ac.id/foto/pgw/990/'.$url.'.jpg">';
                }else{
                    // echo $bn;
                    $url = $this->tf_encode('FOTOAUTO#'.$nim.'#QL:100#WM:1#SZ:300');
                    $ur=APP_STATIC."foto/mhs/990/$url.jpg";
                

                    $opts['http']['header'] .= "Referer: http://learning.uin-suka.ac.id/\r\n";
                    $context = stream_context_create($opts);
                    echo file_get_contents($ur, false, $context); 
                
                }   // echo file_get_contents($api_url, false, stream_context_create($opts));
            
        //}
    }

    function tembak_foto_learning($nim)
    {
        $url = 'http://mobile.learning.uin-suka.ac.id/auto_surat/foto_mahasiswa?nim='.$nim;
        //header("Content-type: image/jpeg");            
        $fot=file_get_contents($url);
        //echo $fot;
        $type = "jpeg";
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($fot);
        echo '<img src="'.$base64.'" />';        
    }

    function testing_xhtml()
    {
        $xstr = "Gubernur Daerah Istimewa Yogyakarta\n c.q Kepala Biro Administrasi Pembangunan";
        echo nl2br($xstr,false);
    }




   
    function tf_encode($kd_kelas, $enckey = ''){ $hasil = ''; #return $kd_kelas;
        if ($enckey == ''){ $str    = 'sng3bAdac5UEmQzv2YBTH8CVh7jXpRo0etfOK4MINSlwFZ6iL9kPD1JWyuqGxr#-.:/'; } else { $str = $enckey; }
        
        $arr_e = array();  $arr_e1 = array(); $arr_r = array(); $arr_r1 = array();
        for($j = 0; $j < strlen($str); $j++){
            $j_ = $j; if ($j_ < 10) { $j_ = '0'.$j_; }
            $arr_e1[$j] = substr($str,$j,1);
            $arr_e[$j_] = substr($str,$j,1);
            $arr_r1[substr($str,$j,1)] = $j;
            $arr_r[substr($str,$j,1)] = $j_;
        }
        
        $total = 0;
        for($i = 0; $i < strlen($kd_kelas); $i++){
            $total = (int)substr($kd_kelas,$i,1) + $total; 
        } $u = fmod($total,10);
        
        $kd_enc = $arr_e1[$u];
        for($i = 0; $i < strlen($kd_kelas); $i++){
            $k = ($arr_r1[substr($kd_kelas,$i,1)]+$u); if($k < 10) { $k = '0'.$k; }
            $kd_enc .= ''.$k.rand(0,9); 
        } return $kd_enc;
    }

    function tf_encode2($kd_kelas, $enckey = ''){ $hasil = ''; #return $kd_kelas;
        if ($enckey == ''){ $str    = 'XpRo0etfOK4MINSlwFZsng3bAdac5_UEmQzv2YBTH8CVh7j6iL9kPD1JWyuqGxr#-.:/ *='; } else { $str = $enckey; }
        
        $arr_e = array();  $arr_e1 = array(); $arr_r = array(); $arr_r1 = array();
        for($j = 0; $j < strlen($str); $j++){
            $j_ = $j; if ($j_ < 10) { $j_ = '0'.$j_; }
            $arr_e1[$j] = substr($str,$j,1);
            $arr_e[$j_] = substr($str,$j,1);
            $arr_r1[substr($str,$j,1)] = $j;
            $arr_r[substr($str,$j,1)] = $j_;
        }
        
        $total = 0;
        for($i = 0; $i < strlen($kd_kelas); $i++){
            $total = (int)substr($kd_kelas,$i,1) + $total; 
        } $u = fmod($total,10);
        
        $kd_enc = $arr_e1[$u];
        for($i = 0; $i < strlen($kd_kelas); $i++){
            $k = ($arr_r1[substr($kd_kelas,$i,1)]+$u); if($k < 10) { $k = '0'.$k; }
            $kd_enc .= ''.$k.rand(0,9); 
        } return $kd_enc;
    }

    public function test_api_photo2($nim)
    {                        
        $nim        = preg_replace("/[^0-9]/", "", $nim);
        //$api_url    = URL_APISIA.'foto/ns_mahasiswa_auto/'.$nim;
        $api_url    = URL_APISIA.'foto/ns_mahasiswa_auto/'.$nim;
        
        header("Content-type: image/jpeg");              
        $bn=file_get_contents($api_url);
        echo  $bn;
        // if($bn==''){
        
        //     $url = $this->tf_encode('FOTOMASUK#'.$nim.'#QL:100#WM:1#SZ:300');
        //     echo file_get_contents(APP_STATIC.'foto/pgw/990/'.$url.'.jpg');
        
        // }else{
            
        //     $url = $this->tf_encode('FOTOAUTO#'.$nim.'#QL:100#WM:1#SZ:300');
        //     $ur=APP_STATIC."foto/mhs/990/$url.jpg";
        

        //     $opts['http']['header'] .= "Referer: http://exp.uin-suka.ac.id/\r\n";
        //     $context = stream_context_create($opts);
        //     echo file_get_contents($ur, false, $context); 
        
        // }                 
    }

    public function riset_kalkulasi_float()
    {
        //casting string ke int dulu
        $nilai_satu = '2.75';
        $nilai_dua = '1.75';
        
        if ((float)$nilai_satu > (float)$nilai_dua){
            $arit = 'pass - nilai diatas C-';
        } else {
            $arit = 'fail - nilai dibawah C-';
        }
        exit(var_dump($arit));
    }

    public function riset_package($test_case=null)
    {
        $this->load->helper('common/MY_date');

        $tgl_surat = '21/06/2018';
        echo waktu_oracle($tgl_surat);
    }

    public function test_api_profil_mhs($nim=null)
    {
        $datamhs = $this->Repo_SIA->get_profil_mhs($nim,'kumulatif');

        echo print_r($datamhs);
    }


    public function test_cek_makul_regex($nim='11650021')
    {
        $raw_krs = $this->Repo_SIA->get_makul_smt($nim);
        $kol_nama_makul = array_column($raw_krs, 'NM_MK');

        $hasil_regex = ['count'=>0];        
        foreach ($kol_nama_makul as $key => $val) {         
            #1. cara pertama dengan strpos
            /*if ( strpos($val, 'Metode Penelitian') !== false OR strpos($val, 'Penelitian') !== false OR strpos($val, 'Metode') !== false) {
                $hasil_regex[] = ['is_found'=>true,'index'=>$key];
            }*/

            #2. cara kedua dengan preg_match
            $k1 = "Mutu Perangkat Lunak";
            $k2 = "Metode Penelitian";
            $is_found_yet = preg_match("/".$k1."|".$k2."/i", $val, $result);
            $hasil_regex['result'][] = $result; //tampung pregmatch ke array hasil regex                    
            if (!empty($result)){ //jika ditemukan selama loop ini
                $hasil_regex['count']++;
                $hasil_regex['result'][$key]['detail'] = $raw_krs[$key];
            }           
        }
        echo print_r($hasil_regex);     
    }

    public function test_api_makul_mhs($opsi='smt_aktif')
    {
        //$nim = $this->input->get('')
        $nim = $this->input->get('nim');

        if ($opsi=='smt_aktif'){
            $data['nim'] = $nim;
            $datamakul = $this->Repo_SIA->raw_get_makul_smt('smt_aktif',$data);
        } elseif ($opsi=='smt_lalu') {
            $ta_mulai = isset($_GET['ta_mulai']) ? $_GET['ta_mulai'] : null; //tahun saat tahun ajaran dimulai
            $smt = isset($_GET['smt']) ? $_GET['smt'] : null; //1) ganjil , 2) genap    
            $data = [
                'nim' => $nim,
                'ta_mulai' => $ta_mulai,
                'smt' => $smt
            ];

            $datamakul = $this->Repo_SIA->raw_get_makul_smt('smt_lalu',$data);
        }
                
        echo print_r($datamakul);
    }
    public function test_thn_ajaran()
    {       
        //echo print_r();
        echo("<pre>".print_r($this->Repo_SIA->get_tahun_ajaran(),true)."</pre>");
    }


# ************************ TESTING terkait API AUTOMASI ************************ #
    public function test_cek_persyaratan($path_jenis_sakad)
    {   
        $param_jenis_sakad = [
            'KD_JENIS_SAKAD' => null,
            'URLPATH' => $path_jenis_sakad
        ];
        $param_nim = $this->session->userdata('username');
        $dibandingkan = $this->Verifikasi_model->cek_persyaratan($param_jenis_sakad, $param_nim);

        echo print_r($dibandingkan);
    }

    public function grab_isi_form()
    {
        echo print_r($_POST);

        //key array harus sama persis dengan kolom di database
        $data['detailskr_in_sisurat'] = [
            'D_PENERIMA_EKS' => $this->input->post('kepada_penerima_eks'), //disimpan sbg satu record PENERIMA_SK grup tujuan EKSTERNAL
            'ALAMAT_TUJUAN' => $this->input->post('instansi_tujuan'),
            'TEMPAT_TUJUAN' => $this->input->post('tempat_tujuan') //kode kota/kab          
        ];

        $currdate = new DateTime();
        $batas_masa_berlaku = $currdate->add(new DateInterval("P3M")); // tambah 3 bulan
        $data['detailskr_in_automasi'] = [
            'MATA_KULIAH_TERKAIT' => $this->input->post('kd_makul'),
            'KEPERLUAN' => $this->input->post('keperluan'), // kondisional sesuai jenis surat akad
            'NM_KEGIATAN' => $this->input->post('nama_kegiatan'), //ex: wawancara, observasi, pkl, penelitian
            'JUDUL_DLM_KEGIATAN' => $this->input->post('judul'),
            'TEMA_DLM_KEGIATAN' => $this->input->post('tema'),
            'SUBYEK_DLM_KEGIATAN' => $this->input->post('subyek'),
            'OBYEK_DLM_KEGIATAN' => $this->input->post('obyek'), //string delimiter
            'TEMA_DLM_KEGIATAN' => $this->input->post('tema'),
            'METODE_DLM_KEGIATAN' => $this->input->post('metode'),
            'TGL_MULAI' => $this->input->post('tgl_mulai'),
            'TGL_BERAKHIR' => $this->input->post('tgl_berakhir'),
            'DATA_LAINNYA' => $this->input->post('data_lainnya'),
            'BATAS_MASA_BERLAKU' => $batas_masa_berlaku->format('Y-m-d'),
        ];

        echo print_r($data);
    }

    /**
    * Prototipe ation baru
     * aksi_simpan_sesi() versi ke-2
     * result : sesi tersimpan di database, reuse $data untuk tampilkan preview pdf
     */
    public function test_aksi_simpan_sesi()
    {
        # 1. GRAB INPUT-INPUT DARI USER
        $kd_jenis_sakad = $this->input->post('inp_kd_jenis_sakad');     
        $id_sesi = $this->input->post('id_sesi_buat');
        $grup_jenis_sakad = $this->input->post('inp_grup_jenis_sakad');
        $kd_jenis_surat_induk = $this->input->post('inp_kd_jenis_surat_induk');

        #2. CASTING Beberapa input untuk clear logic pada method ini
        $kd_jenis_sakad_int = (int)$kd_jenis_sakad;
        $grup_jenis_sakad_int = (int)$grup_jenis_sakad;
        if ($this->session->userdata('log') == 'in' && $this->input->post('save_sesi') == 'ok'){
            $dt = new DateTime();   

            # 3. SIAPKAN DATA AWAL SESI
            $data_awal = array(
                'ID_SESI' => null, //sementara di-null                
                'KD_JENIS_SAKAD' => $kd_jenis_sakad_int,
                'KD_GRUP_JENIS_SAKAD' => $grup_jenis_sakad_int,
                'KD_JENIS_SURAT' => (int)$kd_jenis_surat_induk,
                'PEMBUAT_SURAT' => $this->session->userdata('username'),
                'KD_JENIS_ORANG'        => "M", //mahasiswa
                //'D_PENERIMA_MHS'  => '11650021',
                'D_PENERIMA_MHS'    => $this->session->userdata('username'),
                'TEMPAT_DIBUAT'     => "34712", //kodya yogyakarta
                'KD_STATUS_SIMPAN'  => "1", //DRAF TANPA NOMOR
                'KD_SISTEM'         => "AUTOMASI",
                'WAKTU_SIMPAN_AWAL' => $dt->format('Y-m-d H:i:s') //format yang dipakai di postgre
            );
            # 4. PEROLEH DATA MAHASISWA (API SIA)
            $nim = $data_awal['D_PENERIMA_MHS'];
            $get_mhs = $this->Repo_SIA->get_profil_mhs($nim,'kumulatif');
            //$data['KD_PRODI'] = $get_mhs[0]['KD_PRODI'];
            $unit_id = convert_kdfak_ke_unitid($get_mhs[0]['KD_FAK']); #unit id fakultas mhs pemohon dalam suratsakad_int
            $conf_automasi = $this->Automasi_model->get_config_automasi($kd_jenis_,$unit_id);

            #4. GRAB INPUT LAINNYA dan MODIF DATA KOLOM SESUAI JENIS SURAT
            switch ($kd_jenis_sakad_int) {
                case 1://Surat Keterangan Masih Kuliah                  
                    $data['D_PENERIMA_MHS_LAIN'] = implode( '#', array($this->session->userdata('username'), 'MHS01', $this->session->userdata('nama')) );
                    $tmp_mhs = [
                        'NAMA' => $get_mhs[0]['NAMA'],
                        'NIM' => $get_mhs[0]['NIM'],
                        'TMP_LAHIR' => $get_mhs[0]['TMP_LAHIR'],
                        'TGL_LAHIR' => date('d-m-Y', strtotime($get_mhs[0]['TGL_LAHIR'])),
                        'JUM_SMT' => $get_mhs[0]['JUM_SMT'],
                        'NM_PRODI' => $get_mhs[0]['NM_PRODI'],
                        'NM_FAK' => $get_mhs[0]['NM_FAK']
                    ];

                    $data['TMP_MHS'] = serialize($tmp_mhs);
                    $data['DETAIL_LAINNYA'] = null;
                    $data['ISI_SURAT'] = ($this->input->post('keperluan') == null ? '' : nl2br($this->input->post('keperluan')));
                    break;                  
                default:
                    $data['ISI_SURAT'] = null;
                break;
            }
        } else {
            $this->output
                    ->set_status_header('application/json')
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('status' => false,'message'=>'Unauthorized. tidak dapat menjalankan aksi karena user belum login')));
        }
    }
   
    public function test_insert_skr_automasi()
    {
        $date = new DateTime();
        $date->add(new DateInterval('P3M')); //tambahkan 3 bulan
        $kolom_required = ['ID_SURAT_KELUAR','KEPERLUAN','NM_KEGIATAN','LINGKUP_TEMPAT_KEGIATAN','SUBYEK_KEGIATAN','OBYEK_KEGIATAN','TEMA_KEGIATAN','TGL_MULAI','TGL_SELESAI','BATAS_MASA_BERLAKU','DATA_LAINNYA'];
        $data['surat_keluar'] = [
            'ID_SURAT_KELUAR' => 'T.'.uniqid(),
            'PEMBUAT_SURAT' => $this->session->userdata('username'),
            'TGL_SURAT' => '2018-02-20',
            'KD_JENIS_SAKAD' => 7
        ];
        $data['skr_automasi'] = [
            'ID_SURAT_KELUAR' => 'T.'.uniqid(),         
            //'GRUP_JENIS_SAKAD' => 2,
            'KEPERLUAN' => 'VINI VIDI VICI dolor sit amet',
            'MASA_BERLAKU_SAMPAI' => $date->format('Y-m-d'),
            'NM_KEGIATAN' => 'Penelitian',
            'JUDUL_DLM_KEGIATAN' => 'Analisis Performansi jaringan pada highload',
        ];
        $data['save_automasi'] = array_merge($data['surat_keluar'], $data['skr_automasi']);

        exit(var_dump($data['save_automasi']));
        $parameter  = array('api_kode' => 14004, 'api_subkode' => 1, 'api_search' => array($data['skr_automasi']));
        $api_insert         = $this->apiconn->api_tnde_sakad('tnde_surat_akademik/insert_detail_skr_automasi', 'json', 'POST', $parameter);

        echo print_r($api_insert);

    }

    public function test_api_rawinsert_detail_sakad()
    {
        $date = new DateTime();
        $date->add(new DateInterval('P3M')); //tambahkan 3 bulan
        //echo $date->format('Y-m-d') . "\n";

        //Surat Ket. Masih Kuliah
        /*$data = [
            'ID_SURAT_KELUAR' => 'T.'.uniqid(),
            'KD_JENIS_SAKAD' => 1,
            'KEPERLUAN' => 'VINI VIDI VICI dolor sit amet',
            'MASA_BERLAKU_SAMPAI' => $date->format('Y-m-d')
        ];*/

        $data = [
            'ID_SURAT_KELUAR' => 'T.'.uniqid(),
            'KD_JENIS_SAKAD' => 1,
            'KEPERLUAN' => 'VINI VIDI VICI dolor sit amet',
            'MASA_BERLAKU_SAMPAI' => $date->format('Y-m-d'),
            'DATA_TAMBAHAN' => serialize(array('ipk'=>'3.4','nilai_c_kebawah'=>0,'nilai_e'=>0,'sks_tempuh'=>149))
        ];
        $kd_grup = 1; //1 (surat2 keterangan fakultas), 2 (ijin dan permohonan fakultas), 3 (permohonan lainnya)
        $api_insert = $this->Automasi_model->insert_detail_sakad($kd_grup, $data);

        echo print_r($api_insert);
    }

    public function test_api_rawinsert_hsk()
    {
        $data = [
            'ID_SURAT_KELUAR' => 'T.'.uniqid(),
            'NO_SURAT' => null,
            'TGL_SURAT' => date('Y/m/d'),
            'PEMBUAT_SURAT' => $this->session->userdata('username'),
            'NIM' => $this->session->userdata('username')           
        ];
        $api_insert = $this->Automasi_model->insert_header_surat_keluar($data);

        if (is_array($api_insert)){
            echo print_r($api_insert);
        } else {
            echo var_dump($api_insert);
        }
    }

    public function test_api_rawinsert_automasi($id_sesi_buat='')
     {
        //$id_sesi_buat = 'BSM5a809eb9a990b'; 
        $save_final = 'ok';

        if ($opsi='draf' && $id_sesi_buat !=='' && $save_final == 'ok'){
            //assign semua kolom data_sesi ke array $data_sk
            $get_sesi = $this->Automasi_model->sesi_by_id($id_sesi_buat);
            $data_sk = $get_sesi;

            //$kpd_mahasiswa    = explode("<$>", $get_sesi['D_PENERIMA_MHS_LAIN']);         
            //modif data beberapa kolom
            $data_sk['ID_SURAT_KELUAR'] = 'T.'.uniqid();
            $data_sk['ID_PSD'] = $get_sesi['PSD_ID'];
            $data_sk['ATAS_NAMA'] = $get_sesi['PSD_AN'];
            $data_sk['UNTUK_BELIAU'] = $get_sesi['PSD_UB'];
            $data_sk['STATUS_SK'] = 0;
            $data_sk['WAKTU_SIMPAN'] = date("d/m/Y H:i:s");         
            $data_sk['NO_URUT']     = '';
            $data_sk['NO_SELA']     = '';
            $data_sk['NO_SURAT']    = '';
            
            #format tgl         
            $data_sk['TGL_SURAT'] = date("d-m-Y", strtotime($data_sk['TGL_SURAT']));
            //$data_sk['TGL_SURAT'] = date("d/m/Y"); // format agar sesuai SISURAT

            $key_diikutkan = ['ID_SURAT_KELUAR', 'KD_STATUS_SIMPAN', 'KD_JENIS_SURAT', 'ID_PSD', 'ATAS_NAMA', 'UNTUK_BELIAU', 'ID_KLASIFIKASI_SURAT', 'TGL_SURAT', 'STATUS_SK', 'PEMBUAT_SURAT', 'JABATAN_PEMBUAT', 'KD_JENIS_ORANG','UNIT_ID', 'UNTUK_UNIT', 'PERIHAL', 'TEMPAT_DIBUAT', 'TEMPAT_TUJUAN', 'KOTA_TUJUAN', 'LAMPIRAN', 'KD_SIFAT_SURAT', 'KD_KEAMANAN_SURAT', 'WAKTU_SIMPAN', 'KD_SISTEM', 'NO_URUT', 'NO_SELA', 'ISI_SURAT', 'KD_PRODI', 'NO_SURAT'];
            
            $data_sk = seleksi_array_by_keys($data_sk,$key_diikutkan);
            $save_sk = TRUE;
            //$parameter    = array('api_kode' => 90001, 'api_subkode' => 1, 'api_search' => array($data_sk));

            //echo print_r($parameter['api_search'][0]);            
            //exit(print_r($data_sk));

            
            if($save_sk == TRUE){
                $parameter  = array('api_kode' => 90001, 'api_subkode' => 1, 'api_search' => array($data_sk));
                $save_sk        = $this->apiconn->api_tnde('tnde_surat_keluar/insert_surat_kaluar', 'json', 'POST', $parameter);
                if($save_sk ==  FALSE){
                    $data['errors'][] = "Gagal menyimpan surat. #0";
                }else{
                    $save_sk    =   TRUE;
                }
            }
            


            #DISTRIBUSI PENERIMA SURAT KELUAR



            if(! ISSET($data['errors']) && $save_sk == TRUE){
                $data['success'][] = "Berhasil menyimpan surat keluar.";
                echo print_r($data['success']);
                //$this->session->set_flashdata('success', $data['success']);
            }else{
                //$this->session->set_flashdata('errors', $data['errors']);             
                echo 'yang jelas penyimpanan surat gagal';
            }
            exit();
            
        } else {
            exit('Maaf fitur ini belum dapat digunakan karena sedang dikembangkan!');
        }
    } 


# ************************ TESTING terkait API TNDE x AUTOMASI ************************ #
    public function test_api_tnde_detsk($id=null)
    {
        //$id = '5a7a928a44615p';
        //$id = '5a7aa23cb8588p';
        $data['surat_keluar'] = $this->apiconn->api_tnde('tnde_surat_keluar/detail_surat_keluar', 'json', 'POST', array('api_kode' => 90001, 'api_subkode' => 2, 'api_search' => array($id)));

        $kepada = $this->apiconn->api_tnde('tnde_surat_keluar/get_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($id, 'PS')));
            $data['kepada'] = $kepada;

        echo '<h1>Data Umum</h1><br><br>';
        echo print_r($data['surat_keluar']);

        echo '<br><br><h1>Data Distribusi Penerima</h1><br><br>';
        echo print_r($data['kepada']);      
    }

    public function test_api_surat_keluar_lengkap($id_surat_keluar=null)
    {
        echo json_encode($this->Automasi_model->get_surat_keluar_automasi($id_surat_keluar), JSON_PRETTY_PRINT);
    }
    

    public function tes_api_verifikasi($nim=null)
    {
        $datamhs = $this->Logic_Verifikasi->remote_service_untuk_sakad('SIA_MHS', array('NIM'=>$nim));
        echo print_r($datamhs);
    }


# ************************ TESTING terkait API SIMPEG ************************ #
    public function test_simpeg_unit($unit_id)
    {
        $parameter = array('api_kode' => 1901, 'api_subkode' => 2, 'api_search' => array(date('d/m/Y'), $unit_id)); 
        $data['unit'] = $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
        echo var_dump($data['unit']);
    }

    public function tes_api_simpeg(){
        $this->load->model('Repo_Simpeg');
        $unit = $this->input->get('unit_id');
        echo print_r($this->Repo_Simpeg->check_anggota_unit($unit));
    }

    public function test_api_simpeg_profil($opsi='by_jabatan'){
        $this->load->model('Repo_Simpeg');

        if ($opsi == 'by_jabatan'){
          $kd_jab = $this->input->get('kd_jabatan');
          echo print_r($this->Repo_Simpeg->get_pejabat_aktif(date('d/m/Y'), $kd_jab));      

            //$parameter = array('api_kode' => 1121, 'api_subkode' => 9, 'api_search' => array($data['surat_keluar']['TGL_SURAT'], $psd['KD_JABATAN'], 3));
            //$data['psd'] = $this->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);                
        } elseif ($opsi == 'by_nip') {
            # code...
        } elseif ($opsi == 'detail_kepegawaian') {
            //$parameter = array('api_kode' => 1123, 'api_subkode' => 3, 'api_search' => array($data['surat_keluar']['TGL_SURAT'], $data['psd'][0]['KD_PGW']));
            $parameter = array('api_kode' => 1123, 'api_subkode' => 3, 'api_search' => array('d/m/Y', $this->input->get('nip')));
            $pang_gol = $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
            //$parameter = array('api_kode' => 2001, 'api_subkode' => 2, 'api_search' => array($data['psd'][0]['KD_PGW']));
            $parameter = array('api_kode' => 2001, 'api_subkode' => 2, 'api_search' => array($this->input->get('nip'))); // atau kode pegawai untuk pegawai non pns
            $pgw = $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
            $data['psd_detail']['pangkat'] = (!empty($pang_gol) ? $pang_gol[0] : '');
            $data['psd_detail']['pegawai'] = (!empty($pgw) ? $pgw[0] : '');

            echo print_r($data);
        }                            
    }


    
# ************************ TESTING terkait API AUTOMASI ************************ #
    public function test_api_cek_syarat($opsi='',$data=null){
        if ($opsi == 'by_kd') {
            echo print_r($this->Logic_Verifikasi->get_persyaratan_by_kd(1));
        }       

        if ($opsi == 'by_path'){
            echo print_r($this->Logic_Verifikasi->get_persyaratan_by_path($data));
        }
    }

    public function tes_api_config_automasi()
    {
        //echo print_r($this->Repo_Surat->get_config_automasi('2','UN02006'));      
    }   

        public function test_arr()
    {
        $each_mhs = [
            'meta_title' => 'Daftar Distribusi Penerima Mahasiswa',
            'data' => array(
                array(
                    $this->session->userdata('username'), //NIM
                    'MHS01', //KD_GRUP_TUJUAN
                    $this->session->userdata('nama') //NAMA MAHASISWA
                )/*,
                array(
                    '11650004',
                    'MHS01',
                    'AlVISYAHRIN'
                ),*/
            )
        ];

        $arr_tmpmhs = [];       
        foreach ($each_mhs['data'] as $key => $val) {
            //$arr_tmpmhs[$key][] = $implode('#', $val);            
            $arr_tmpmhs[] =  implode('#', $val);
        }
        
        $data['D_PENERIMA_MHS_LAIN'] = implode('<$>', $arr_tmpmhs);

        echo $data['D_PENERIMA_MHS_LAIN'];
        echo '<br>';
        echo print_r(explode("<$>", $data['D_PENERIMA_MHS_LAIN']));
    }

    public function test_fungsi_array()
    {
        $data = [
            'ID_SURAT_KELUAR' => 'POS33',
            'KEPERLUAN' => 'pelaksanaan praktek kerja lapangan di industri IT',
            'NM_KEGIATAN' => 'Kerja Praktek',
            'LINGKUP_TEMPAT_KEGIATAN' => 'PT GAMA MEVIDA',
            'SUBYEK_KEGIATAN' => 'MATA KULIAH PRAKTEK KERJA LAPANGAN',
            'OBYEK_KEGIATAN' => null,
            //'TEMA_KEGIATAN' => null,
            'TGL_MULAI' => '2018/02/15',
            'TGL_SELESAI' => '2018/04/15',
            'BATAS_MASA_BERLAKU' => null
            //'DATA_LAINNYA' => null
        ];
        $kolom_required = ['ID_SURAT_KELUAR','KEPERLUAN','NM_KEGIATAN','LINGKUP_TEMPAT_KEGIATAN','SUBYEK_KEGIATAN','OBYEK_KEGIATAN','TEMA_KEGIATAN','TGL_MULAI','TGL_SELESAI','BATAS_MASA_BERLAKU','DATA_LAINNYA'];
        $seleksi_arr_data = array_intersect_key($data, array_flip($kolom_required));

        echo var_dump($seleksi_arr_data);
    }

    public function test_jwt_terbitkan()
    {
        $this->load->helper('common/Package_loader');
        load_package_firebase_jwt();
        
        define('SECRET_KEY','Your-Secret-Key');  /// secret key can be a random string and keep in secret from anyone
        define('ALGORITHM','HS256');   // Algorithm used to sign the token

        //$tokenId    = base64_encode(mcrypt_create_iv(32));
        //$tokenId    = base64_encode(uniqid());
        $issuedAt   = time();
        $notBefore  = $issuedAt + 10;  //Adding 10 seconds
        $expire     = $notBefore + 7200; // Adding 60 seconds
        $serverName = base_url(); /// set your domain name

        /*
        * Create the token as an array
        */
        $data = [
            'iat'  => $issuedAt,         // Issued at: time when the token was generated
            'jti'  => $tokenId,          // Json Token Id: an unique identifier for the token
            'iss'  => $serverName,       // Issuer
            'nbf'  => $notBefore,        // Not before
            'exp'  => $expire,           // Expire
            'data' => [                  // Data related to the logged user you can set your required data
                //'id'   => $row[0]['id'], // id from the users table
                //'name' => $row[0]['name'], //  name
                'id_surat_keluar' => 'uiusai78787',
                'kd_user' => '1993178800091921',
                'jab'   => 'ST004' //contoh
            ]
        ];
        //$secretKey = base64_decode(SECRET_KEY);

        /// Here we will transform this array into JWT:
        $jwt = JWT::encode(       
            $data, //Data to be encoded in the JWT
            SECRET_KEY, // The signing key
             ALGORITHM 
        ); 
        //$unencodedArray = ['jwt' => $jwt];
        echo json_encode($jwt);



        /**
         * IMPORTANT:
         * You must specify supported algorithms for your application. See
         * https://tools.ietf.org/html/draft-ietf-jose-json-web-algorithms-40
         * for a list of spec-compliant algorithms.
        */
        /*
        $jwt = JWT::encode($token, $key);
        $decoded = JWT::decode($jwt, $key, array('HS256'));

        print_r($decoded);
        */
    }

    public function test_jwt_cek()
    {
        $token_value = $this->input->get('tokVal');
        try {
            //$secretKey = base64_decode('Your-Secret-Key');

            $secretKey = 'Your-Secret-Key';
            $DecodedDataArray = JWT::decode($token_value, $secretKey, array('HS256'));

            $waktu_diterbitkan = date("Y-m-d H:i:s", $DecodedDataArray->iat);
            $waktu_kadaluarsa = date("Y-m-d H:i:s", $DecodedDataArray->exp);

            echo '<h2 style="green">Waktu terbit : '.$waktu_diterbitkan.'</h2>';
            echo "<br>";
            echo '<h2 style="color: red; ">Waktu kadaluarsa : '.$waktu_kadaluarsa.'</h2>';

            echo  "{'status' : 'success' ,'data':".json_encode($DecodedDataArray)." }";
            die();
        } catch (Firebase\JWT\ExpiredException $e ) {
             echo "{'status' : 'fail' ,'msg':'Expired'}";
            die();
        } catch (Firebase\JWT\SignatureInvalidException $e) {
            echo "{'status' : 'fail' ,'msg':'Unauthorized - Signature verification failed'}";
            die();
        }

    }

    public function test_api_sesi($id_sesi=null)
    {
        echo print_r($this->Automasi_model->sesi_by_id($id_sesi));
    }
    public function test_api_det_automasi($id=null)
    {
        echo print_r($this->Automasi_model->get_surat_keluar_automasi($id));
    }

    public function test_api_tnde_raw_insertsk()
    {
        $data_sk = [
            'ID_SURAT_KELUAR' => uniqid().'p',
            'TGL_SURAT' => '07/02/2018',
            'PERIHAL' => 'AKTIF KULIAH A.N BBNG',
            'NO_SURAT' => '',
            'TEMPAT_DIBUAT' => '34043',
            'TEMPAT_TUJUAN' => '',
            'KD_JENIS_ORANG' => 'M',
            'ID_PSD' => '76',
            'ATAS_NAMA' => '7',
            'ID_KLASIFIKASI_SURAT' => '325',
            //'NO_URUT' => '601',
            'NO_URUT' => '',
            'NO_SELA' => '',
            //'ISI_SURAT' => 'Percobaan buat surat keterangan oleh mahasiswa melalui sistem automasi surat 2018 kedua.',
            'ISI_SURAT' => null,
            'KD_SIFAT_SURAT' => 'B',
            'KD_KEAMANAN_SURAT' => 'B',
            'PEMBUAT_SURAT' => '11650021',
            'STATUS_SK' => '0',
            'KD_JENIS_SURAT' => '11',
            'UNTUK_UNIT' => 'UN02006',
            'LAMPIRAN' => '-',
            //'JABATAN_PEMBUAT' => 'JS0000Q2', 
            'JABATAN_PEMBUAT' => '', 
            'KD_STATUS_SIMPAN' => '1',
            'KD_PRODI' => '',
            'UNTUK_BELIAU' => '',
            'KOTA_TUJUAN' => '',
            'WAKTU_SIMPAN' => '07/02/2018 12:50:45',
            'UNIT_ID' => 'UN02006'
        ];
        $kolom_baru = array('KD_PEGAWAI' => '196409251986032003', 'NM_PEGAWAI' => 'Listiyati');
        $parameter  = array('api_kode' => 90001, 'api_subkode' => 1, 'api_search' => array($data_sk));
        $save_sk        = $this->apiconn->api_tnde('tnde_surat_keluar/insert_surat_kaluar', 'json', 'POST', $parameter);
        if($save_sk ==  FALSE){
            $data['errors'][] = "Gagal menyimpan surat. #0";
        }else{
            $save_sk    =   TRUE;
        }

        if ($save_sk == TRUE && !isset($data['errors'])){
            echo 'Berhasil Menyimpan Surat dengan API';
        } elseif ($save_sk == FALSE) {
            echo "Gagal Menyimpan Surat dengan API";
        }
    }

    /*
    * Delete Surat Keluar di SISURAT (Umum + Penerima distribusi)
    */
    public function test_api_delete_from_automasi($id_surat_keluar=null) //pastikan surat yang dihapus , adalah surat ujicoba
    {
        $api_del = $this->Automasi_model->delete_surat_keluar_automasi($id_surat_keluar);

        echo print_r($api_del);
    }

    /**
    * Raw delete data detail surat keluar API AUTOMASI
    */
    public function test_api_delete_dsk_automasi($id_surat_keluar=null)
    {

    }
}
