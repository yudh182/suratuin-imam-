<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');
class Act_surat_mhs_deprecated extends MY_Controller {
	
	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('log') !== 'in')
			redirect(site_url('pgw/login?from='.current_url()));

		//$this->load->model('Mdl_tnde', 'apiconn'); #autoload
		$this->load->helper('form');
		$this->load->helper('Tnde_sakad');				
		#$this->load->model('Repo_Surat');
		$this->load->model('Logic_Verifikasi');	
		$this->load->model('Verifikasi_model');			
		$this->load->model('Automasi_model');
		$this->load->module('pgw/api/Api_foto','api_foto');

		$cek_sesi = $this->Automasi_model->sesi_by_kd_user($this->session->userdata('username'));

		//exit(var_dump($cek_sesi));
		if (!empty($cek_sesi)){
			$this->session->set_userdata('sesi_sakad',$cek_sesi);
		} else {
			$this->session->unset_userdata('sesi_sakad');
		}
    }

   	public function load_partial_isi()
	{
		$data['_partial_isi_surat'] = $this->load->view('isi/ijin_pen',$data,TRUE);
		$this->load->view('v_isi_form',$data);
	}

	public function load_partial_verifikasi()
	{
		$this->load->helper('tnde_sakad');
		$json_string = '{ "output": [ { "NO.": 1, "OUT_TXT_TITLE": "Status Mahasiswa = Aktif", "OUT_TXT_HUBUNGI": "Petugas Fakultas", "ISI": "AKTIF", "STATUS": "pass" } ], "num_errors": 0 }';
		$arr = json_decode($json_string, TRUE);
		$data['response_verifikasi'] = $arr;
		$this->load->view('v_verifikasi_gagal', $data);
	}


   	public function test_kode_keaslian()
	{
		$datatoken = $this->Automasi_model->generateTokenKeaslian($this->session->userdata('username'),'cek_keaslian');
		//$qrcode = $this->Automasi_model->generateQRCodeKeaslian($this->session->userdata('username'), 'tt_digital');
		
		echo print_r($datatoken);
	}

   /**
	* Action Verifikasi (deprecated)
	*/
	public function action_verifikasi($paramdata=array()) #Verifikasi validasi atas permohonan buat surat tertentu
    {    	
    	/*if ($_SERVER['REQUEST_METHOD'] == 'GET'){
    		//cek uri segment (untuk mengetahui jenis surat), cek session (userdata data mahasiswa)

    		//lakukan pengecekan 

    		//output view html

    	}*/

    	# JIKA DIAKSES OLEH METHOD LAIN DALAM CONTROLLER INI
    	if (!empty($paramdata)){    		
    		$datasyarat = $this->Logic_Verifikasi->get_persyaratan_by_path($paramdata['urlpath']);
    		//$cek = $this->Logic_Verifikasi->pengecekan_persyaratan($datasyarat['data'], $datasyarat['meta']['KD_JENIS_SAKAD'], $paramdata['nim']);

    		return $datasyarat;
    		//exit(print_r($cek));
    	} else {
    		exit("data masih kosong");
    	}

    	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    		# cek current user (mahasiswa atau pegawai)
    		$tipe_user = $this->session->userdata('grup');
    		if (in_array('mhs', $tipe_user)){
    			$path_jenis_sakad = $this->uri->segment(4); 
    			
    			$resp_persyaratan = $this->Logic_Verifikasi->get_persyaratan_by_path($path_jenis_sakad);

    		} elseif (in_array('pgw', $tipe_user)) {
    			$tmp_jenis_sakad = $this->input->post('kd_jns_srt');
	        	$tmp_jenis_surat_induk = $this->input->post('kd_jns_srt_induk');
	        	$tmp_nim = $this->input->post('nim');
	        	$tmp_log = $this->session->userdata('username');
    		}

    		


    		# validasi form (pastikan berisi)

    		# cek data param yang diterima

    		# lakukan pengecekan

    		# output json
    	}
    }


   /**
    * Aksi Simpan atau Perbarui Sesi Versi 1
    */
    public function aksi_simpan_sesi($paramdata=array())
    {
    	$this->load->model('Repo_SIA');
    	$this->load->library('form_validation');
    	$this->load->helper('Tnde_converter_to_simpeg');
		

    	# 1. GRAB INPUT-INPUT DARI USER
		$kd_jenis_sakad = $this->input->post('inp_kd_jenis_sakad');
		$grup_jenis_sakad = $this->input->post('inp_grup_jenis_sakad');
		$kd_jenis_surat_induk = $this->input->post('inp_kd_jenis_surat_induk');
		$id_sesi = $this->input->post('id_sesi_buat');

		$kd_jenis_sakad_int = (int)$kd_jenis_sakad;

		# 2. SIAPKAN DATA (AWAL) PENERIMA DISTRIBUSI SURAT
		/*
		$kpd_pejabat 			= explode("<$>", $this->input->post('kpd_pejabat'));
		$kpd_pegawai 			= explode("<$>", $this->input->post('kpd_pegawai'));
		$kpd_mahasiswa 	= explode("<$>", $this->input->post('kpd_mahasiswa'));
		$kpd_lainnya 		= $this->input->post('kpd_lainnya');
		$ts_pejabat 			= explode("<$>", $this->input->post('ts_pejabat'));
		$ts_lainnya 			= $this->input->post('ts_lainnya');
		*/

    	if ($this->session->userdata('log') == 'in' && $this->input->post('save_sesi') == 'ok'){
    		# 3. SIAPKAN DATA AWAL SESI
    		$dt = new DateTime();		
			$data = array(
				'ID_SESI' => null, //sementara di-null
                //'ID_SESI' => $id_sesi,
                //'KD_JENIS_SAKAD' => 1,
                //'KD_JENIS_SURAT' => 11,
                'KD_JENIS_SAKAD' => (int)$kd_jenis_sakad_int,
                'KD_GRUP_JENIS_SAKAD' => (int)$grup_jenis_sakad,
                'KD_JENIS_SURAT' => (int)$kd_jenis_surat_induk,
                'PEMBUAT_SURAT'	=> $this->session->userdata('username'),
                'KD_JENIS_ORANG'		=> "M", //mahasiswa
                //'D_PENERIMA_MHS'	=> '11650021',
                'D_PENERIMA_MHS'	=> $this->session->userdata('username'),
                'TEMPAT_DIBUAT'		=> "34712", //kodya yogyakarta
                'KD_STATUS_SIMPAN'	=> "1", //DRAF TANPA NOMOR
                'KD_SISTEM'			=> "AUTOMASI",
                'WAKTU_SIMPAN_AWAL' => $dt->format('Y-m-d H:i:s')
	       	);

			# 4. PEROLEH DATA MAHASISWA (API SIA)
			$nim = $data['D_PENERIMA_MHS'];
	       	$get_mhs = $this->Repo_SIA->get_profil_mhs($nim,'kumulatif');
	       	$data['KD_PRODI'] = $get_mhs[0]['KD_PRODI'];

	       	//menyiapkan data umum surat	    	
	       	$unit_id = convert_kdfak_ke_unitid($get_mhs[0]['KD_FAK']); #unit id fakultas mhs pemohon dalam surat       	       	
			$data_automasi = $this->Automasi_model->get_config_automasi($kd_jenis_sakad_int,$unit_id);


	       	# 4. MODIF DATA KOLOM SESUAI JENIS SURAT
	       	switch ($kd_jenis_sakad_int) {
				case 1://Surat Keterangan Masih Kuliah
					/*$each_mhs = [
						'meta_title' => 'Daftar Distribusi Penerima Mahasiswa',
						'data' => array(
							array(
								$this->session->userdata('username'), //NIM
								'MHS01', //KD_GRUP_TUJUAN
								$this->session->userdata('nama') //NAMA MAHASISWA
							), 
						)
					];
					foreach ($each_mhs['data'] as $key => $val) {
						implode('#', $val);
					}*/
					$data['D_PENERIMA_MHS_LAIN'] = implode( '#', array($this->session->userdata('username'), 'MHS01', $this->session->userdata('nama')) );
					$tmp_mhs = [
						'NAMA' => $get_mhs[0]['NAMA'],
						'NIM' => $get_mhs[0]['NIM'],
						'TMP_LAHIR' => $get_mhs[0]['TMP_LAHIR'],
						'TGL_LAHIR' => date('d-m-Y', strtotime($get_mhs[0]['TGL_LAHIR'])),
						'JUM_SMT' => $get_mhs[0]['JUM_SMT'],
						'NM_PRODI' => $get_mhs[0]['NM_PRODI'],
						'NM_FAK' => $get_mhs[0]['NM_FAK']
					];

					$data['TMP_MHS'] = serialize($tmp_mhs);
					$data['DETAIL_LAINNYA'] = null;
					$data['ISI_SURAT'] = ($this->input->post('keperluan') == null ? '' : nl2br($this->input->post('perlu')));					
					break;
				case 7://Surat Ijin Observasi
					break;
				case 8://Surat Ijin Observasi
					break;
				
				
				default:
					$data['ISI_SURAT'] = 'lorem ipsum dolor sit amet';
					//$data['ISI_SURAT'] = ($this->input->post('body_surat') == null ? '' : nl2br($this->input->post('body_surat')));
					break;
			}
							    		    	    	
			
			$data_gabung = array_merge($data, $data_automasi[0]);

			# modif beberapa kolom
			$data_gabung['PERIHAL'] = $data_gabung['PERIHAL'].' a.n. '.ucfirst($get_mhs[0]['NAMA_F']); 
					
			//$strkeys = implode(', ', array_keys($data_gabung));		
			/*$data_form_custom = array(
				'ISI_SURAT'			=> ($this->input->post('perlu') == null ? '' : nl2br($this->input->post('perlu')))
			);*/

			$key_diikutkan = ['ID_SESI', 'KD_JENIS_SAKAD', 'KD_JENIS_SURAT', 'PEMBUAT_SURAT', 'KD_JENIS_ORANG','D_PENERIMA_MHS','D_PENERIMA_MHS_LAIN', 'TEMPAT_DIBUAT','KD_STATUS_SIMPAN','KD_SISTEM', 'WAKTU_SIMPAN_AWAL', 'UNIT_ID', 'KD_PRODI', 'PSD_ID', 'PSD_AN', 'PSD_UB', 'ID_KLASIFIKASI_SURAT', 'KD_SIFAT_SURAT', 'KD_KEAMANAN_SURAT', 'PERIHAL', 'LAMPIRAN', 'ISI_SURAT','TGL_SURAT', 'KD_GRUP_JENIS_SAKAD', 'TMP_MHS'];
			
			$data_to_insert = seleksi_array_by_keys($data_gabung,$key_diikutkan);
	       

			#Validasi form per jenis surat mahasiswa
			switch ($kd_jenis_sakad_int) {
				case 1: #Masih Kuliah
					$config = array(
				        array(
				                'field' => 'perlu',
				                'label' => 'Keperluan',
				                'rules' => 'required'
				        )
				    );
					break;
				case 7: #Masih Kuliah
					$config = array(
				        array(
				                'field' => 'judul',
				                'label' => 'Judul Penelitian',
				                'rules' => 'required'
				        ),
				        array(
				                'field' => 'instansi',
				                'label' => 'Instansi',
				                'rules' => 'required'
				        ),
				        array(
				                'field' => 'metode',
				                'label' => 'Metode Penelitian',
				                'rules' => 'required'
				        )
					);
					break;

				
				default:

					break;
			}
			
			$this->form_validation->set_message('required', 'kolom {field} wajib diisi');
			$this->form_validation->set_rules($config);
			$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
			
			if ($this->form_validation->run()){#pastikan form terisi semua
				if ($id_sesi == 'new'){
					$data_to_insert['ID_SESI'] = 'BSM'.uniqid();
					$mulai = $this->Automasi_model->simpan_sesi_automasi_buat_v2($data_to_insert);
				
			       	if ($mulai['status'] == TRUE || $mulai['status'] == 1 || $mulai['status'] == '1'){ #RESPONSE HTML JIKA INSERT BERHASIL
			       		#1. set session untuk data saat mencetak
			       			///$this->session->set_userdata('current_sesi_buat', $mulai['data']['ID_SESI']);
			       			//$this->session->set_userdata('tmp_id_sesi',)
			       			//$this->session->set_userdata('tmp_data_surat_mhs')
			       			//$this->session->set_userdata('tmp_data_surat_isi'); //diambilkan dari sesi db
			       			//$this->session->set_userdata('tmp_data_surat_psd'); // diambilkan dari sesi db       		
			       		#2. tampil sbg html;
			       		//$data['success_message'] = $mulai['success_message'];
			       		$data['pesan'] = $mulai['success_message'];
			       		$data['sesi_cetak'] = $mulai['data'];

			       		$response = $this->load->view('v_cetak',$data, true);
			       		$this->output
			       			->set_status_header(200)
			       			->set_header('X-ID-SESI: '.$data['sesi_cetak']['ID_SESI'])
			       			->set_content_type('text/html')		       			
			       			->set_output($response);
			       	} else { #JIKA INSERT GAGAL KARENA KESALAHAN DI SERVICE
			       		$this->output
			       			->set_status_header(501) //gagal simpan karena kesalahan program di server
			       			->set_content_type('text/html')		       			
			       			->set_output(print_r($mulai));
			       	}

				} else { //asumsi : memiliki id sesi yang benar
					$perbarui = $this->Automasi_model->perbarui_sesi_automasi_buat($id_sesi, $data_to_insert);
				
					if ($perbarui['status'] == TRUE || $perbarui['status'] == 1 || $perbarui['status'] == '1'){ #RESPONSE HTML JIKA INSERT BERHASIL
						$data['pesan'] = $perbarui['success_message'];
			       		$data['sesi_cetak'] = $perbarui['data'];

			       		$response = $this->load->view('v_cetak',$data, true);
			       		$this->output
			       			->set_status_header(200)
			       			->set_header('X-ID-SESI: '.$id_sesi)
			       			->set_content_type('text/html')		       			
			       			->set_output($response);
			       	} else { #JIKA UPDATE GAGAL KARENA KESALAHAN DI SERVICE
			       		$this->output
			       			->set_status_header(501) //gagal simpan karena kesalahan program di server
			       			->set_content_type('text/html')
			       			->set_output(print_r($perbarui));
			       	}					
					/*$this->output
			       			->set_status_header(200)
			       			//->set_header('X-ID-SESI: '.$data['sesi_cetak']['ID_SESI'])
			       			->set_content_type('text/html')		       			
			       			->set_output('<p>sementara belum berfungsi</p>');*/
				}				
			} else {
				/*
				foreach ($_POST as $key => $value) {
    				$data['errors'][$key] = form_error($key);
    			}
    			$this->output->set_content_type('application/json')
		       			->set_output(json_encode($data));
		       	*/
			 	$this->output
		       			//->set_content_type('application/json')
			 			->set_status_header(422)
			 			->set_content_type('text/html')
		       			->set_output(json_encode(validation_errors()));		       	
			}
			
    	} else {
    		$this->output
	       			->set_content_type('application/json')
	       			->set_output(json_encode(array('status' => false,'message'=>'Unauthorized - tidak dapat menjalankan aksi karena data login tidak dikenali atau mungkin anda belum login')));
    		//exit('tidak dapat menjalankan aksi karena data login tidak dikenali atau user belum login');
    	}    			
    }

    /**
    * Action Simpan Surat (Versi 1)
     * opsi : draf (1), bernomor (2)
     * proses : menyimpan data ke SURAT KELUAR (SISURAT) lewat API, insert berhasil dilanjutkan insert data HEADER SURAT KELUAR DAN D_DETAIL_SAKAD
    */
    public function aksi_simpan_final_surat($opsi='draf')
    {
    	if (isset($_POST['btn_save_skr_bernomor'])){
    		exit('sedang memproses pembuatan surat bernomor (langsung cetak)');	
    	} if (isset($_POST['btn_save_skr_ttd'])){
    		exit('sedang memproses permohonan pembuatan surat skema penandatanganan digital');	
    	} else {
    		exit("Unidentified - submit form anda mungkin keliru");
    	}
    	$id_sesi_buat = $this->input->post('id_sesi_buat_cetak'); 
    	$save_final = $this->input->post('save_final_cetak');

    	if ($opsi=='draf' && $id_sesi_buat !== '' && $save_final == 'ok'){
    		//assign semua kolom data_sesi ke array $data_sk
    		$get_sesi = $this->Automasi_model->sesi_by_id($id_sesi_buat);
    		$data_sk = $get_sesi;

    		$kpd_mahasiswa 	= explode("<$>", $get_sesi['D_PENERIMA_MHS_LAIN']);

    		//exit(print_r($kpd_mahasiswa));
    		//modif data beberapa kolom
			$data_sk['ID_SURAT_KELUAR'] = uniqid().'p';
			$data_sk['ID_PSD'] = $get_sesi['PSD_ID'];
			$data_sk['ATAS_NAMA'] = $get_sesi['PSD_AN'];
			$data_sk['UNTUK_BELIAU'] = $get_sesi['PSD_UB'];
			$data_sk['STATUS_SK'] = 0;
			$data_sk['WAKTU_SIMPAN'] = date("d/m/Y H:i:s");

			#!PENTING : format date di postgre Y-m-d, sementara di oracle sisurat menerapkan d-m-Y
			$tgl = DateTime::createFromFormat('Y-m-d', $data_sk['TGL_SURAT']); //format pada sesi (postgre)		
			$data_sk['TGL_SURAT'] = $tgl->format('d/m/Y');
			//$data_sk['TGL_SURAT'] = date("d-m-Y", strtotime($data_sk['TGL_SURAT'])); 
			
			if($data_sk['KD_STATUS_SIMPAN'] == '2' || $data_sk['KD_STATUS_SIMPAN'] == 2){ #2 BERNOMOR
				#dapatkan nomor
				if(empty($data['errors'])){
					$parameter	= array('api_kode' => 90002, 'api_subkode' => 3, 'api_search' => array($data_sk));
					$req_no 		= $this->apiconn->api_tnde('tnde_surat_keluar/penomoran', 'json', 'POST', $parameter);
					if(empty($req_no['ERRORS'])){
						$data_sk['NO_URUT']	= (!empty($req_no['NO_URUT']) ? $req_no['NO_URUT'] : 0);
						$data_sk['NO_SELA']		= (!empty($req_no['NO_SELA']) ? $req_no['NO_SELA'] : 0);
						$data_sk['NO_SURAT']	= (!empty($req_no['NO_SURAT']) ? $req_no['NO_SURAT'] : 0);
					}else{
						$save_sk = FALSE;
						$data['errors'] = $req_no['ERRORS'];
					}
				}
			} else { #1 (draf)*/
				$data_sk['NO_URUT']		= '';
				$data_sk['NO_SELA']		= '';
				$data_sk['NO_SURAT']	= '';
			}
			

			$key_diikutkan = ['ID_SURAT_KELUAR', 'KD_STATUS_SIMPAN', 'KD_JENIS_SURAT', 'ID_PSD', 'ATAS_NAMA', 'UNTUK_BELIAU', 'ID_KLASIFIKASI_SURAT', 'TGL_SURAT', 'STATUS_SK', 'PEMBUAT_SURAT', 'JABATAN_PEMBUAT', 'KD_JENIS_ORANG','UNIT_ID', 'UNTUK_UNIT', 'PERIHAL', 'TEMPAT_DIBUAT', 'TEMPAT_TUJUAN', 'KOTA_TUJUAN', 'LAMPIRAN', 'KD_SIFAT_SURAT', 'KD_KEAMANAN_SURAT', 'WAKTU_SIMPAN', 'KD_SISTEM', 'NO_URUT', 'NO_SELA', 'ISI_SURAT', 'KD_PRODI', 'NO_SURAT'];
			
			$data_sk = seleksi_array_by_keys($data_sk,$key_diikutkan);
			$save_sk = TRUE;
			
			//exit(print_r($data_sk));
		# 1. API SISURAT - INSERT DATA UMUM SURAT KELUAR
			if($save_sk == TRUE){
				$parameter	= array('api_kode' => 90001, 'api_subkode' => 1, 'api_search' => array($data_sk));
				$save_sk 		= $this->apiconn->api_tnde('tnde_surat_keluar/insert_surat_kaluar', 'json', 'POST', $parameter);
				if($save_sk	==	FALSE){
					$data['errors'][] = "Gagal menyimpan surat. #0";
				}else{
					$save_sk	=	TRUE;
				}
			}
			# 2. API SISURAT - INSERT PENERIMA SURAT KELUAR			
			if ($save_sk == TRUE){
				// 2.1 KIRIM SURAT KE MAHASISWA (Mahasiswa disebut dalam surat)
				if(!empty($kpd_mahasiswa[0])){
					foreach($kpd_mahasiswa as $k => $val){
						$no_urut = $k + 1;
						$exp = explode("#", $val);
						if(count($exp) == 3){
							$mhs['ID_SURAT_KELUAR']				= $data_sk['ID_SURAT_KELUAR'];
							$mhs['PENERIMA_SURAT']				= $exp[0];
							$mhs['JABATAN_PENERIMA']			= "0"; //$exp[1]
							$mhs['KD_GRUP_TUJUAN']				= $exp[1]; //MHS01
							$mhs['KD_STATUS_DISTRIBUSI']	= "PS";
							$mhs['ID_STATUS_SM']						= 1;
							$mhs['KD_JENIS_TEMBUSAN']		= 0;
							$mhs['NO_URUT']					= $no_urut;
							//$mhs['NO_URUT']								= (!empty($kpd_urut[$mhs['PENERIMA_SURAT']]) ? $kpd_urut[$mhs['PENERIMA_SURAT']] : "");
							$mhs['KD_JENIS_KEPADA']				= "0";
							//$mhs['KD_JENIS_KEPADA']				= (!empty($up[$mhs['PENERIMA_SURAT']]) ? "1" : "0");

							$save_mhs = $this->apiconn->api_tnde('tnde_surat_keluar/insert_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($mhs)));
							if($save_mhs == FALSE){
								$data['errors'][] = "Gagal mengirim ke tujuan surat ke ".$exp[3]; // Mahasiswa
							}
						}
					}
				}

				// 2.1 KIRIM SURAT KE TEMBUSAN PEJABAT (TERTERA DI DAFTAR TEMBUSAN)
			}

			if(! ISSET($data['errors']) && $save_sk == TRUE){
				$data['success']['message'][] = "Berhasil menyimpan surat keluar.";
				$data['success']['data'] = $data_sk;

				# API AUTOMASI 3,4,5,6
				#3. Automasi insert header surat keluar
				$tgl_surat_postgre = DateTime::createFromFormat('d/m/Y', $data['success']['data']['TGL_SURAT']); //format tanggal untuk postgresql					
				$header_surat_keluar = [
					'ID_SURAT_KELUAR' => $data['success']['data']['ID_SURAT_KELUAR'],
					'NO_SURAT' => $data['success']['data']['NO_SURAT'],
    				'TGL_SURAT' => $tgl_surat_postgre->format('Y-m-d'),  		
    				'PEMBUAT_SURAT' => $data['success']['data']['PEMBUAT_SURAT'],
    				'NIM' => ($data['success']['data']['KD_JENIS_ORANG'] == 'M' ? $data['success']['data']['PEMBUAT_SURAT'] : ''),
    				'KD_JENIS_SAKAD' => $get_sesi['KD_JENIS_SAKAD']
				];				

				$api_insert_hsk = $this->Automasi_model->insert_header_surat_keluar($header_surat_keluar);
				if ($api_insert_hsk !== NULL || $api_insert_hsk !== FALSE || $api_insert_hsk['status'] !== FALSE){
					$data['success']['message'][] = "Berhasil menyimpan header surat keluar (automasi).";
					//$data['cetak']['header_surat_keluar'] = $header_surat_keluar;
					$data['cetak']['header_surat_keluar'] = $api_insert_hsk['data'];
				} elseif ($api_insert_hsk['status'] !== FALSE) {
					$data['errors'][] = $api_insert_hsk['error_message'];
				} else {
					$data['errors'][] = "Gagal menyimpan header surat keluar.";
				}
				

				#4. Atomasi detail sakad
				//$date = DateTime::createFromFormat('d-m-Y', $data['success']['data']['TGL_SURAT']);
				//$tgl = $date->format('d/m/Y');
				$kd_jenis_sakad_int = (int)$get_sesi['KD_JENIS_SAKAD'];
				$kd_grup = $get_sesi['KD_GRUP_JENIS_SAKAD']; //1 (surat2 keterangan fakultas), 2 (ijin dan permohonan fakultas), 3 (permohonan lainnya)
				$date = new DateTime();
				switch ($kd_jenis_sakad_int) {
					case 1: #Surat Ket. Masih Kuliah
						$date->add(new DateInterval('P3M')); //tambahkan 3 bulan
						$data_detail = [
				    		'ID_SURAT_KELUAR' => $data['success']['data']['ID_SURAT_KELUAR'],
				    		//'KD_JENIS_SAKAD' => $get_sesi['KD_JENIS_SAKAD'],
				    		'KEPERLUAN' => $get_sesi['ISI_SURAT'],
				    		'BATAS_MASA_BERLAKU' => $date->format('Y-m-d'),
				    		'DATA_TAMBAHAN' => null
				    		//'DATA_TAMBAHAN' => serialize(array('ipk'=>'3.4','nilai_c_kebawah'=>0,'nilai_e'=>0,'sks_tempuh'=>149))
				    	];
						break;
					
					default:
						exit("berhenti sebelum insert detail surat");
						break;
				}
										    	
		    	$api_insert = $this->Automasi_model->insert_detail_sakad($kd_grup, $data_detail);
		    	if ($api_insert !== NULL || $api_insert !== FALSE || $api_insert['status'] !== FALSE){
					$data['success']['message'][] = "Berhasil menyimpan detail surat mahasiswa (automasi).";
					$data['cetak']['detail_sakad'] = $api_insert['data'];
				} elseif ($api_insert['status'] !== FALSE) {
					$data['errors'][] = $api_insert['error_message'];
				} else {
					$data['errors'][] = "Gagal menyimpan detail surat mahasiswa (automasi).";
				}

				#5. Atomasi TIKET_ADMINISTRASI (TOKEN KEASLIAN)
				$tiket_adm = $this->Automasi_model->generateTokenKeaslian('cek_keaslian', $data['success']['data']['PEMBUAT_SURAT'], $data['success']['data']['ID_SURAT_KELUAR'], 'TOKEN SEMENTARA UNTUK DRAF SURAT');
				if ($tiket_adm !== NULL || $tiket_adm !== FALSE || $tiket_adm['status'] !== FALSE){
					$data['success']['message'][] = "Berhasil menyimpan data tiket administrasi dengan token keaslian (automasi).";
					$data['cetak']['qrcode'] = base_url('cek_keaslian_surat/qrcode/'.$tiket_adm); //response berupa string token 
				} else {
					$data['errors'][] = "Gagal menyimpan data tiket administrasi dengan token keaslian (automasi).";
				}

				#6. hapus record sesi_buat_sakad karena semua data telah disimpan di pos masing-masing
				if (empty($data['errors'])){
					//AMBIL DATA KOLOM TMP_MHS SEBELUM DIHAPUS UNTUK GENERATE PDF
					$data['cetak']['tmp_mhs'] = $get_sesi['TMP_MHS'];
					$data['cetak']['detail_lainnya'] = $get_sesi['DETAIL_LAINNYA'];

					//HAPUS SESI
					$del_sesi = $this->Automasi_model->hapus_sesi_automasi_buat($id_sesi_buat);
				}


				echo print_r($data);
				
				# 6. CETAK HASIL AKHIR KE PDF
				//$this->aksi_print_surat();

				
				
				$this->session->set_flashdata('success', $data['success']);
			} else{
				$this->session->set_flashdata('errors', $data['errors']);
				echo print_r($data['errors']);
			}			    		
    	} else {
    		exit('Maaf fitur ini belum dapat digunakan karena sedang dikembangkan!');
    	}
    }

    /**
    * Print pdf
    * @param $path = string urlpath jenis surat. ex: masih_kul, hbs_teori, lulus, ijin_pen, dsb.
    * @param $paramdata = data yang dilewatkan. pada direct pengajuan , paramdata berfungsi sebagai data utama yg membangun pdf
    * @param $opsi = direct_pengajuan atau finish_pengajuan
    */
    public function aksi_print_surat($path, $paramdata=null, $opsi='direct_pengajuan')
    {
    	$this->load->model('Repo_Simpeg');
    	$this->load->model('Repo_SIA');
    	$this->load->model('mhs/Convert_model','Convert_model');
    	$this->load->library('mhs/pdf_surat');

    	if ($opsi == 'direct_pengajuan'){ //DATA PROFIL MAHASISWA menggunakan variabel array yg dilewatkan ke $data    		    		
	    	$kd_jenis_sakad = (int)$data['surat_keluar_automasi']['KD_JENIS_SAKAD'];
	    	$path_jenis_sakad = $data['surat_keluar_automasi']['URLPATH'];
	    	$tmp_mhs = $data['TMP_MHS'];
	    	$data['data_mahasiswa'] = unserialize($tmp_mhs);
    	} elseif ($opsi == 'finish_pengajuan') {
    		$id_surat_keluar = $paramdata;

    		//API GET DATA KOMPLIT SURAT KELUAR	
    		$data['skr_lengkap'] = $this->Automasi_model->get_surat_keluar_automasi($id_surat_keluar);	

    		//API REQUEST NAMA PEGAWAI PSD
			$data['pgw_psd'] = $this->Repo_Simpeg->get_pejabat_psd($data['skr_lengkap']['surat_keluar_sisurat']['TGL_SURAT'], $data['skr_lengkap']['surat_keluar_sisurat']['KD_JABATAN_2']);

			//cek psd atas_nama
			if ($data['skr_lengkap']['surat_keluar_sisurat']['ATAS_NAMA'] !== '' || !empty($data['skr_lengkap']['surat_keluar_sisurat']['ATAS_NAMA']) ){
				$an = $this->apiconn->api_tnde('tnde_general/get_data', 'json', 'POST', array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('MD_PSD', 'ID_PSD', $data['skr_lengkap']['surat_keluar_sisurat']['ATAS_NAMA'])));
				if(!empty($an['KD_JABATAN'])){
					$parameter = array('api_kode' => 1121, 'api_subkode' => 9, 'api_search' => array($data['skr_lengkap']['surat_keluar_sisurat']['TGL_SURAT'], $an['KD_JABATAN'], 3));
					$data['pgw_psd_an'] = $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
				}
			}
			

			//API REQUEST DATA MAHASISWA (FROM DISTRIBUSI_PENERIMA_SK)

			foreach ($data['skr_lengkap']['kepada'] as $key => $val)
			{
				if ($val['KD_GRUP_TUJUAN'] == 'MHS01' && $val['KD_STATUS_DISTRIBUSI'] == 'PS'){						
       				$get_mhs = $this->Repo_SIA->get_profil_mhs($val['PENERIMA_SURAT'], 'kumulatif');
					$kpd['mhs'][] = $get_mhs[0];
				}
			}			
    	} else {
    		exit('error. invalid parameter');
    	}


    	
    	if (in_array($path, array('perm_KP','ijin_obs'))){    		
    	}
    	$kd_fak = $kpd['mhs'][0]['KD_FAK'];
		$data['kop'] = [
			'tlp' => $this->Convert_model->tlp($kd_fak),
			'web' => $this->Convert_model->web($kd_fak),
			'fkl' => $kpd['mhs'][0]['NM_FAK_J']
		];

		$data['judul_surat'] = strtoupper($data['skr_lengkap']['surat_keluar_automasi']['NM_JENIS_SAKAD']);
		$data['nomor'] = 'DIKOSONGKAN DAHULU';
		$data['thn_akademik'] = $this->Repo_SIA->get_tahun_ajaran('info_ta_smt');
		$data['tgl_surat'] = tanggal_indo($data['skr_lengkap']['surat_keluar_sisurat']['TGL_SURAT'], 'd/m/Y');		

		$data['label_psd_an'] = $data['pgw_psd_an'][0]['STR_NAMA_S1'];
		$data['label_psd'] = $data['pgw_psd'][0]['STR_NAMA_A212'];
		$data['nm_pgw_psd'] = $data['pgw_psd'][0]['NM_PGW_F'];
		$data['qrcode_keaslian'] = base_url('cek_keaslian_surat/'.$data['skr_lengkap']['surat_keluar_automasi']['TOKEN_KEASLIAN']);

		# CONDITIONAL CHECK CUSTOM DATA TIAP JENIS SURAT
    	switch ($path) {
			case 'masih_kul':								
				if (!empty($an)){
					$data['frasa_pendahuluan'] = $data['pgw_psd_an'][0]['STR_NAMA'].' menerangkan bahwa :';		
				}

				$data['isi_pgw'] = '';
				$tgl_lahir_raw = DateTime::createFromFormat('d-m-Y h:i:s', $kpd['mhs'][0]['TGL_LAHIR']); //format pada sesi (postgre)		
				$data['isi_mhs'] = [
					'nama' => $kpd['mhs'][0]['NAMA'],
					'nim' => $kpd['mhs'][0]['NIM'],
					'tmp_lahir' => $kpd['mhs'][0]['TMP_LAHIR'],
					'tgl_lahir' => $tgl_lahir_raw->format('d/m/Y'),
					'smt' => $kpd['mhs'][0]['JUM_SMT'],
					'prodi' => $kpd['mhs'][0]['NM_PRODI'],
					'fkl' => $kpd['mhs'][0]['NM_FAK'],
				];
								
				//$data['isi_ts_pjb'] = '';
				//$data['isi_ts_lain'] = null;
				$buka_frasa_isi = 'Surat keterangan ini dibuat untuk melengkapi salah satu syarat ';
				$data['isi_surat'] = $buka_frasa_isi.$data['skr_lengkap']['surat_keluar_automasi']['KEPERLUAN'].'.';
				$view = 'reports/v_masihkuliah';
			break;
			
			default:
				# code...
				break;
		}				

		//echo print_r($data);
		$this->load->view($view,$data);
    }

    public function test_format_tanggal()
    {
    	//$date = date_create_from_format('j-M-Y', '15-Feb-2009');
		//echo date_format($date, 'Y-m-d');

		$date = DateTime::createFromFormat('Y-m-d', '2009-02-15');
		$tgl = $date->format('d/m/Y');

		echo $tgl;
    }

    public function test_sesi_penerima($id_sesi='',$opsi='get')
    {
    	$id_surat_keluar = 'T.5a812333f27ba'; #ID SURAT KELUAR yang akan diupdate penerima-nya
    	if ($opsi == 'get'){
    		$get_sesi = $this->Automasi_model->sesi_by_id($id_sesi);
			$kpd_mahasiswa 	= explode("<$>", $get_sesi['D_PENERIMA_MHS_LAIN']);

			//echo var_dump($kpd_mahasiswa[0]);
			/*if(!empty($kpd_mahasiswa[0])){// pastikan ada isinya
				echo "<ul>";
				foreach ($kpd_mahasiswa as $key => $val) {
					$exp = explode("#", $val);
					$no_urut = $key + 1;
					echo '<li>
						<ol>
							<li>No : '.$no_urut.'</li>
							<li>NIM : '.$exp[0].'</li>
							<li>GRUP TUJUAN : '.$exp[1].'</li>
							<li>NAMA MAHASISWA : '.$exp[2].'</li>							
						</ol>
					</li>';
				}
				echo "</ul>";
			}
			exit();*/

			// KIRIM SURAT KE MAHASISWA (Mahasiswa disebut dalam surat)
			if(!empty($kpd_mahasiswa[0])){					
				foreach($kpd_mahasiswa as $key => $val){
					$exp = explode("#", $val);
					$no_urut = $key + 1;
					if(count($exp) == 5){
						//$mhs['ID_SURAT_KELUAR']				= $data_sk['ID_SURAT_KELUAR'];
						$mhs['ID_SURAT_KELUAR']				= $id_surat_keluar; //TEST
						$mhs['PENERIMA_SURAT']				= $exp[0]; //NIM
						$mhs['JABATAN_PENERIMA']			= $exp[1]; //0
						$mhs['KD_GRUP_TUJUAN']				= $exp[2]; //MHS01
						$mhs['KD_STATUS_DISTRIBUSI']	= "PS";
						$mhs['ID_STATUS_SM']						= 1;
						$mhs['KD_JENIS_TEMBUSAN']		= 0;
						$mhs['NO_URUT']								= "0";
						//$mhs['KD_JENIS_KEPADA']				= (!empty($up[$mhs['PENERIMA_SURAT']]) ? "1" : "0");
						$mhs['KD_JENIS_KEPADA']				= "0"; // 0 = biasa, 1 = untuk perhatian
						$save_mhs = $this->apiconn->api_tnde('tnde_surat_keluar/insert_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($mhs)));
						
						if($save_mhs == FALSE){
							$data['errors'][] = "Gagal mengirim ke tujuan surat (ID ".$mhs['ID_SURAT_KELUAR'].") ke ".$exp[4]." - ".$exp[3]; // Mahasiswa
						}
					}
				}

			}

			if(ISSET($data['errors'])){
             	echo '<h1 style="color: red;">'.print_r($data).'</h1>';
            }else{
                echo '<h1 style="color: green;">Berhasil mengirim surat</h1>';
            }
    	}
    	
    	if ($opsi == 'del'){
    		//exit("laman yang anda minta sedang dalam pengembangan");
    		$id_surat_keluar = $id_sesi;
    		#2.1 HAPUS DULU DISTRIBUSI TUJUAN/PENERIMA SURAT
            $parameter              = array('api_kode' => 1001, 'api_subkode' => 3, 'api_search' => array('D_PENERIMA_SK', 'ID_SURAT_KELUAR', $id_surat_keluar));
            $cek_penerima_skr   =   $this->apiconn->api_tnde('tnde_general/get_data', 'json', 'POST', $parameter);
            if(count($cek_penerima_skr) > 0){
                $del_ps['ID_SURAT_KELUAR']  =   $id_surat_keluar;
                $parameter              = array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('D_PENERIMA_SK', $del_ps));
                $del_penerima_skr   =   $this->apiconn->api_tnde('tnde_general/delete_data', 'json', 'POST', $parameter);
                if($del_penerima_skr == FALSE){
                    $data['errors'][]   = "Gagal membatalkan pengiriman surat.";
                }

                if ($del_penerima_skr == TRUE){
                	$data['success'][]   = "Berhasil menghapus distribusi penerima / membatalkan pengiriman surat.";	                	
                }

                echo print_r($data);
            }
    	}
    }


}