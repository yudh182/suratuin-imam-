<?php

class Prototipe_Automasi extends MY_Controller
{

    /** Set hasil verifikasi ke session php dan fields pengisian form surat
     * data untuk ditulis ke form fields : view hasil (lolos atau tidak lolos),
     * data untuk ditulis ke session (jika lolos verifikasi): status_verifikasi, tmp_mhs
     */
    public function set_verifikasi_ke_sesi($data=null)
    {
        $kd_jenis_sakad = $data['KD_JENIS_SAKAD'];
        $nim = $this->session->userdata('log_nim');
        $detail_jenis_sakad = $this->Master_model->detail_jenis_sakad_by_kd($kd_jenis_sakad);

        $verifikasi = $this->Verifikasi_model->pengecekan_persyaratan($kd_jenis_sakad, $nim); //multiple request ke webservice terkait

        if ($verifikasi['status'] == TRUE OR $verifikasi['status'] == '1' OR $verifikasi == TRUE){
            $data['detail_jenis_sakad'] = $detail_jenis_sakad;
            $data['hasil_verifikasi'] = $verifikasi['data'];
            $this->session->set_userdata('data_hasil_verifikasi', $verifikasi['data']);
            $this->load->view("buat/v_terverifikasi_buat_".$detail_jenis_sakad['URLPATH'], $data);
        } else {
            $data['hasil_verifikasi'] = $verifikasi['data'];
            $this->load->view("buat/v_verifikasi_gagal", $data);
        }
    }

    public function save_sesi()
    {
        # 1. grab data dari submit form ($_POST) dan session (data_hasil_verifikasi) 

    }


    public function cetak_surat_tersimpan()
    {   # 1. AMBIL DATA SURAT KELUAR DARI API SISURAT DAN API AUTOMASI

        # 2. SIAPKAN DATA PENDUKUNG DINAMIS (PANGGILAN KE API SIMPEG, API SIA dan API LAINNYA jika diperlukan)                      
        $kementerian = $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', 
            array('api_kode' => 1901, 'api_subkode' => 4, 'api_search' => array(date('d/m/Y'), 'UK000001'))
        );        
        $negara = $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', 
            array('api_kode' => 1901, 'api_subkode' => 4, 'api_search' => array(date('d/m/Y'), 'UN10009'))
        );
        $universitas   = $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', 
            array('api_kode' => 1901, 'api_subkode' => 4, 'api_search' => array(date('d/m/Y'), 'UK000002'))
        );        
        $data['universitas'] = (!empty($universitas[0]['UNIT_NAMA_S1']) ? $universitas[0]['UNIT_NAMA_S1'] : ''); 

        $unit_fakultas = $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', 
            array('api_kode' => 1901, 'api_subkode' => 4, 'api_search' => array(date('d/m/Y'), $data_skr['surat_keluar']['UNIT_ID']))
        );
        $unit_prodi = '';
        
        $tgl_surat_strip	= str_replace("/", "-", $data['surat_keluar']['TGL_SURAT']);
        $data['logo_unit']	= tf_encode($data['surat_keluar']['UNIT_ID'].'#'.$tgl_surat_strip.'#QL:50#WM:0#SZ:150');
        $pdf['logo_unit'] = (!empty($data['logo_unit']) ? $data['logo_unit'] : '');

        $parameter = array('api_kode' => 12000, 'api_subkode' => 1, 'api_search' => array($data['surat_keluar']['KOTA_TUJUAN']));
        $data['kota'] = $this->apiconn->api_sia('sia_master/data_search', 'json', 'POST', $parameter);
        $pdf['kota'] = (!empty($data['kota']) ? $data['kota'] : '');

        $parameter = array('api_kode' => 12000, 'api_subkode' => 1, 'api_search' => array($data['surat_keluar']['TEMPAT_DIBUAT']));
        $tempat_dibuat = $this->mdl_tnde->api_sia('sia_master/data_search', 'json', 'POST', $parameter);       


        # SET REAL DATA TCPDF

        //data sementara, belum benar valuenya
        $data['kop_surat'] = [
            'kemngr' => strtoupper($kementerian.' '.$negara),
            'univ' => (!empty($universitas[0]['UNIT_NAMA_S1']) ? $universitas[0]['UNIT_NAMA_S1'] : ''),
            'fkl' => (!empty($unit_fakultas[0]['UNIT_NAMA']) ? $universitas[0]['UNIT_NAMA_'] : ''),
            'alamat' => (!empty($unit_fakultas[0]['UNIT_ALAMAT']) ? $universitas[0]['UNIT_ALAMAT'] : ''),, 
            'tlp'   => (!empty($unit_fakultas[0]['UNIT_TELP']) ? $universitas[0]['UNIT_NAMA_'] : ''),
            'web' => (!empty($unit_fakultas[0]['UNIT_WEB']) ? $universitas[0]['UNIT_WEB'] : '')
        ];
        $data['nm_tempat_dibuat'] = (!empty($tempat_dibuat[0]['NM_KAB2']) ? $tempat_dibuat[0]['NM_KAB2'] : '');
        $data['tgl_terbit_surat'] = tgl_indo($data_skr['surat_keluar']['TGL_SURAT'], 'd/m/Y');
        $data['judul_jenis_surat'] = strtoupper($data_skr['surat_keluar_automasi']['NM_JENIS_SAKAD']);
        $data['no_surat'] = strtoupper($data_skr['surat_keluar']['NO_SURAT']);
        

         if (!empty($data_skr['kepada']))
                switch($data['kepada'][0]['KD_GRUP_TUJUAN']){

                }


    # SET DATA DAN VIEW REPORT SESUAI JENIS SURAT AKAD
        switch ($path_jenis_sakad) {
            case 'kel_baik': #KETERANGAN kelakuan baik
                break;
            case 'masih_kul': #KETERANGAN
                break;
            case 'tdk_bea': #KETERANGAN
                break;
            case 'studi_pen': #IJIN Studi Pendahuluan (PPL, Semprop)
                break;
            case 'ijin_pen': #IJIN Penelitian (Skripsi)
                break;
            case 'hbs_teori': #KETERANGAN habis teori (utk daftar munaqosyah)
                break;            
            case 'lulus': #KETERANGAN lulus
                break;            
            case 'kp_pkl': #IJIN
                break;            
            case 'ijin_obs': #IJIN Observasi
                $parameter = array('api_kode' => 12000, 'api_subkode' => 1, 'api_search' => array($data['surat_keluar']['TEMPAT_TUJUAN']));
                $data['tempat_tujuan'] = $this->apiconn->api_sia('sia_master/data_search', 'json', 'POST', $parameter);
                $data['tujuan_surat_eks'] = [
                    'pihak_tujuan' => 'Kepala XYZ', //cari dari $kpd_lainnya
                    'alamat_tujuan' => $data['tempat_tujuan']
                ];

                break;            
            case 'pindah_studi': #PERMOHONAN LAIN
                break;
            default:
                $data['errors'][] = 'parameter path jenis surat tidak dikenali';
                break;
        }

        if (!isset($data['errors'])){
            $this->load->view($view,$data); //generate pdf to download
        } else {
            $this->session->set_flashdata('errors', $data['errors']);
            redirect(); //redirect ke url sebelumnya ()
        }
    }   

    /** Cetak preview pdf dengan data array yang dilewatkan di parameter + data session
     */
    public function preview_surat($data=null)
    {

    }
    /** Cetak Surat Langsung 
     * Data pembangun pdf menggunakan variabel $data yang sebelumnya digunakan untuk API INSERT ke SERVICE SURAT KELUAR (SISURAT) dan SERVICE DETAIL SURAT (AUTOMASI)
     */
    public function cetak_surat_langsung()
    {

    }

    /** Mahasiswa mengirim permohonan psd digital
     * a) dikirim ke pegawai TU Unit Fakultasnya (permintaan paraf digital)
     * b) dikirim ke pejabat psd (permintaan persetujuan psd digital)
     */
    public function send_permohonan_psd_digital()
    {

    }

    /** Aksi Pejabat Memberikan Persetujuan psd digital
     */
    public function approve_psd_digital()
    {

    }

    /** Aksi Pejabat Membatalkan (menolak memberi persetujuan) psd digital
     * misal : alasan karena menilai ada kejanggalan, mendapat rekomendasi TU, dsb
     */
    public function cancel_psd_digital()
    {

    }



}