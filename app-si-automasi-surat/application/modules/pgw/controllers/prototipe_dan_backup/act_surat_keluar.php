<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');
Class Act_surat_keluar extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('mdl_tnde');
		$log = $this->session->userdata('log_kd_pgw');
		if(empty($log)){
			redirect();
		}
	}
	
	function cari_surat($jenis=''){
		$referer = (!empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '');
		$auth = host_allowed($referer);
		$hasil = array();
		if($auth){
			if(!empty($_POST['q'])){
				$q = $this->input->post('q');
				$status			= '2,3';
				$api_search	= array($q, $jenis, $status);
				$parameter		= array('api_kode'=>90009, 'api_subkode' => 2, 'api_search' => $api_search);
				$surat				=$this->mdl_tnde->get_api('tnde_surat_keluar/get_surat_keluar_all', 'json', 'POST', $parameter);
				if(!empty($surat)){
					$content = '<div id="copy_errors"></div><table class="table table-bordered"><tr><th width="170px">Nomor  Surat</th><th>Perihal</th><th width="82px">Aksi</th></tr>';
					foreach($surat as $key => $val){
						$content = $content.'<tr><td>'.$val['NO_SURAT'].'</td><td>'.$val['PERIHAL'].'</td><td><button type="button" id="id-sk_'.$val['ID_SURAT_KELUAR'].'" class="btn btn-default btn-sm copy_surat">Copy <span class="copy_loading" id="copy-pro_'.$val['ID_SURAT_KELUAR'].'">'.loading_circle(14).'</span></button></td></tr>';
					}
					$content = $content.'</table>';
					$hasil['success'] = $content;
				}else{
					$hasil['errors'] = notifications('Pencarian surat undangan dengan kata kunci <b>"'.$q.'"</b> tidak ditemukan', 'errors');
				}
			}else{
				$hasil['errors'] = notifications('Ketikkan kata pencarian', 'errors');
			}
		}else{
			$hasil['errors'] = notifications('Akses ditolak', 'errors');
		}
		echo json_encode($hasil);
	}
	
	function copy(){
		$referer = (!empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '');
		$auth = host_allowed($referer);
		$hasil = array();
		if($auth){
			if(!empty($_POST['id_surat_keluar'])){
				$id_surat_keluar			= $this->input->post('id_surat_keluar');
				$parameter					= array('api_kode' => 90001, 'api_subkode' => 2, 'api_search' => array($id_surat_keluar));
				$data['surat_keluar']	= $this->mdl_tnde->get_api('tnde_surat_keluar/detail_surat_keluar', 'json', 'POST', $parameter);
				if(!empty($data['surat_keluar'])){
					$wru['ID_SURAT_KELUAR']	= $id_surat_keluar;
					$or['NO_URUT']		= 'ASC';
					$parameter				= array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('D_AGENDA_UNDANGAN', $wru, $or));
					$data['agenda'] 		= $this->mdl_tnde->get_api('tnde_general/order_data', 'json', 'POST', $parameter);
					$parameter							= array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($id_surat_keluar));
					$data['detail_sundangan'] 	= $this->mdl_tnde->get_api('tnde_surat_keluar/detail_sundangan_by_id_surat_keluar', 'json', 'POST', $parameter);
					$parameter						= array('api_kode'=>2400, 'api_subkode'=>1, 'api_search'=> array($data['detail_sundangan']['KD_PEMINJAMAN']));
					$data['pinjam_simar'] 	= $this->mdl_tnde->api_simar('simar_general/get_peminjaman_ruang', 'json', 'POST', $parameter);
					$parameter = array('api_kode' => 12000, 'api_subkode' => 1, 'api_search' => array($data['surat_keluar']['TEMPAT_DIBUAT']));
					$data['tempat_dibuat'] = $this->mdl_tnde->api_sia('sia_master/data_search', 'json', 'POST', $parameter);
				
					$kepada = $this->mdl_tnde->get_api('tnde_surat_keluar/get_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($id_surat_keluar, 'PS')));
					$data['kepada'] = $kepada;
					$tembusan = $this->mdl_tnde->get_api('tnde_surat_keluar/get_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($id_surat_keluar, 'TS')));
					$data['tembusan'] = $tembusan;
					if(!empty($kepada)){
						foreach($kepada as $key => $val){
							$parameter = array('api_kode' => 2001, 'api_subkode' => 2, 'api_search' => array($val['PENERIMA_SURAT']));
							$pgw_kepada = $this->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
							$parameter = array('api_kode' => 1123, 'api_subkode' => 3, 'api_search' => array($data['surat_keluar']['TGL_SURAT'], $val['PENERIMA_SURAT']));
							$pangkat = $this->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
							$parameter = array('api_kode' => 1101, 'api_subkode' => 1, 'api_search' => array($val['JABATAN_PENERIMA']));
							$jab_kepada = $this->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
							$data['simpeg_kepada'][$key]['pegawai'] = (!empty($pgw_kepada) ? $pgw_kepada[0] : '');
							$tmp_jab = (!empty($jab_kepada) ? $jab_kepada[0] : '');
							$data['simpeg_kepada'][$key]['jabatan'] = $tmp_jab;
							$data['simpeg_kepada'][$key]['pangkat'] = (!empty($pangkat) ? $pangkat[0] : '');
							$data['simpeg_kepada'][$key]['status_jabatan'] = status_jabatan($tmp_jab);
							$data['simpeg_kepada'][$key]['kd_grup_tujuan'] = $val['KD_GRUP_TUJUAN'];
							if($val['KD_GRUP_TUJUAN'] == 'MHS01'){
								$parameter	= array('api_kode'=>26000, 'api_subkode' => 12, 'api_search' => array($val['PENERIMA_SURAT']));
								$get_mhs 		= $this->mdl_tnde->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);
								$data['simpeg_kepada'][$key]['mahasiswa'] = (!empty($get_mhs[0]) ? $get_mhs[0] : '');
							}
						}
					}
					if(!empty($tembusan)){
						foreach($tembusan as $key => $val){
							$parameter = array('api_kode' => 2001, 'api_subkode' => 2, 'api_search' => array($val['PENERIMA_SURAT']));
							$pgw_tembusan = $this->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
							$parameter = array('api_kode' => 1123, 'api_subkode' => 3, 'api_search' => array($data['surat_keluar']['TGL_SURAT'], $val['PENERIMA_SURAT']));
							$pangkat = $this->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
							$parameter = array('api_kode' => 1101, 'api_subkode' => 1, 'api_search' => array($val['JABATAN_PENERIMA']));
							$jab_tembusan = $this->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
							$data['simpeg_tembusan'][$key]['pegawai'] = (!empty($pgw_tembusan) ? $pgw_tembusan[0] : '');
							$tmp_jab = (!empty($jab_tembusan) ? $jab_tembusan[0] : '');
							$data['simpeg_tembusan'][$key]['jabatan'] = $tmp_jab;
							$data['simpeg_tembusan'][$key]['pangkat'] = (!empty($pangkat) ? $pangkat[0] : '');
							$data['simpeg_tembusan'][$key]['status_jabatan'] = status_jabatan($tmp_jab);
							$data['simpeg_tembusan'][$key]['kd_grup_tujuan'] = $val['KD_GRUP_TUJUAN'];
							if($val['KD_GRUP_TUJUAN'] == 'MHS01'){
								$parameter	= array('api_kode'=>26000, 'api_subkode' => 12, 'api_search' => array($val['PENERIMA_SURAT']));
								$get_mhs 		= $this->mdl_tnde->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);
								$data['simpeg_tembusan'][$key]['mahasiswa'] = (!empty($get_mhs[0]) ? $get_mhs[0] : '');
							}
						}
					}
					$_SESSION['copy_data'] = $data;
					$hasil['success'] = 1;
				}else{
					$hasil['errors'] = notifications('Surat tidak ditemukan', 'errors');
				}
			}else{
				$hasil['errors'] = notifications('Tidak ada permintaan', 'errors');
			}
		}else{
			$hasil['errors'] = notifications('Akses ditolak', 'errors');
		}
		echo json_encode($hasil);
	}
	
	function save_sk(){
		$referer = (!empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '');
		$auth = host_allowed($referer);
		if(($this->input->post('save_sk') != null) && ($auth)){
			$data_sk['ID_SURAT_KELUAR'] 			= $this->input->post('id_surat_keluar');
			$data_sk['KD_STATUS_SIMPAN'] 		= $this->input->post('kd_status_simpan');
			$data_sk['KD_JENIS_SURAT'] 				= $this->input->post('kd_jenis_surat');
			$data_sk['ID_PSD'] 									= $this->input->post('id_psd');
			$data_sk['ATAS_NAMA'] 						= $this->input->post('an');
			$data_sk['UNTUK_BELIAU'] 					= $this->input->post('ub');
			$data_sk['ID_KLASIFIKASI_SURAT'] 	= $this->input->post('id_klasifikasi_surat');
			$data_sk['TGL_SURAT'] 						= $this->input->post('tgl_surat');
			$data_sk['STATUS_SK'] 						= ($this->input->post('status_sk') == null ? 0 : $this->input->post('status_sk'));
			$data_sk['PEMBUAT_SURAT'] 			= $this->session->userdata('kd_pgw');
			$data_sk['JABATAN_PEMBUAT'] 		= $this->session->userdata('str_id');
			$data_sk['KD_JENIS_ORANG']				= "P";
			$data_sk['UNIT_ID'] 								= $this->input->post('unit_id');
			$data_sk['UNTUK_UNIT']						= $this->input->post('untuk_unit');
			$data_sk['PERIHAL'] 								= str_replace("'", "''", $this->input->post('perihal'));
			$tempat_dibuat 										= str_replace("'", "''", $this->input->post('tempat_dibuat'));
			$data_sk['TEMPAT_DIBUAT'] 				= (!empty($tempat_dibuat) ? $tempat_dibuat : "34712");
			$data_sk['TEMPAT_TUJUAN'] 				= str_replace("'", "''", $this->input->post('tempat'));
			$data_sk['KOTA_TUJUAN'] 					= str_replace("'", "''", $this->input->post('kota_tujuan'));
			$data_sk['LAMPIRAN'] 							= str_replace("'", "''", $this->input->post('lampiran'));
			$data_sk['KD_SIFAT_SURAT'] 				= $this->input->post('kd_sifat_surat');
			$data_sk['KD_KEAMANAN_SURAT']		= (!empty($this->input->post('kd_keamanan_surat')) ? $this->input->post('kd_keamanan_surat') : 'B');
			
			$data_sk['WAKTU_SIMPAN'] 				= date("d/m/Y H:i:s");
			$data_sk['KD_SISTEM'] 							= 'SISURAT';
			$data_sk['NO_URUT']	= (int)preg_replace("/[^0-9]/", "", $this->input->post('no_urut'));
			$data_sk['NO_SELA']		= (int)preg_replace("/[^0-9]/", "", $this->input->post('no_sela'));
			switch($data_sk['KD_JENIS_SURAT']){
				case 11:
					$data_sk['ISI_SURAT'] 	= ($this->input->post('body_surat') == null ? '' : nl2br($this->input->post('body_surat')));
					break;
				default:
					$data_sk['ISI_SURAT'] 	= ($this->input->post('body_surat') == null ? '' : fixed_isi_surat($this->input->post('body_surat')));
					break;
			}
				
			$parameter 	= array('api_kode' => 1901, 'api_subkode' => 4, 'api_search' => array(date('d/m/Y'), $data_sk['UNIT_ID']));
			$jenis_unit 	= $this->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
			if(!empty($jenis_unit[0]['UNIT_JENIS'])){
				if($jenis_unit[0]['UNIT_JENIS'] == 'J004'){
					$data_sk['KD_PRODI']			 					= $this->input->post('kd_prodi');
				}else{
					$data_sk['KD_PRODI']			 					= '';
				}
			}else{
					$data_sk['KD_PRODI']			 					= '';
			}
			
			$dasar 					= $this->input->post('dasar');
			$menimbang 		= $this->input->post('menimbang');
			$mengingat	 		= $this->input->post('mengingat');
			$menetapkan	 	= $this->input->post('menetapkan');
			
			$pertimbangan 			= $this->input->post('pertimbangan');
			$kd_pertimbangan	= $this->input->post('kd_pertimbangan');
			$kpd_pejabat 			= explode("<$>", $this->input->post('kpd_pejabat'));
			$kpd_pegawai 			= explode("<$>", $this->input->post('kpd_pegawai'));
			$kpd_mahasiswa 	= explode("<$>", $this->input->post('kpd_mahasiswa'));
			$kpd_lainnya 		= $this->input->post('kpd_lainnya');
			$ts_pejabat 			= explode("<$>", $this->input->post('ts_pejabat'));
			$ts_lainnya 			= $this->input->post('ts_lainnya');
			
			$save_sk = TRUE;
			if($data_sk['KD_STATUS_SIMPAN'] == 2){
				if($data_sk['KD_JENIS_SURAT'] == 19){ #Surat SPD
					$kd_pegawai 		= $kpd_pegawai[0];
					if(!empty($kd_pegawai)||!empty($kpd_lainnya[0])){
						if(!empty($kd_pegawai)){
							$data_sk['SPD_PGW'] = $kd_pegawai;
						}
					}else{
						$save_sk = FALSE;
						$data['errors'][] = "Gagal membuat surat. Masukkan data pegawai yang melaksanakan perjalanan dinas.";
					}
				}
				if(empty($data['errors'])){
					$parameter	= array('api_kode' => 90002, 'api_subkode' => 3, 'api_search' => array($data_sk));
					$req_no 		= $this->mdl_tnde->get_api('tnde_surat_keluar/penomoran', 'json', 'POST', $parameter);
					if(empty($req_no['ERRORS'])){
						$data_sk['NO_URUT']	= (!empty($req_no['NO_URUT']) ? $req_no['NO_URUT'] : 0);
						$data_sk['NO_SELA']		= (!empty($req_no['NO_SELA']) ? $req_no['NO_SELA'] : 0);
						$data_sk['NO_SURAT']	= (!empty($req_no['NO_SURAT']) ? $req_no['NO_SURAT'] : 0);
					}else{
						$save_sk = FALSE;
						$data['errors'] = $req_no['ERRORS'];
					}
				}
			}else{
				$data_sk['NO_URUT']		= '';
				$data_sk['NO_SELA']		= '';
				$data_sk['NO_SURAT']	= '';
			}

			/*
			pre($data_sk);
			if(!empty($data['errors'])){
				pre($data['errors']);
			}else{
				echo 'tidak ada error';
			}
			die();
			*/
			
			#VALIDASI
			$config_rule = array(
								array('field' => 'id_surat_keluar', 'label' => 'Id surat keluar', 'rules' => 'required'),
								array('field' => 'id_psd', 'label' => 'Penandatangan Surat', 'rules' => 'required'),
								array('field' => 'unit_id', 'label' => 'Unit/Bagian/Fakultas', 'rules' => 'required'),
								array('field' => 'kd_jenis_surat', 'label' => 'Jenis surat', 'rules' => 'required'),
								array('field' => 'id_klasifikasi_surat', 'label' => 'Klasifikasi Surat', 'rules' => 'required'),
								array('field' => 'kd_status_simpan', 'label' => 'Status simpan', 'rules' => 'required')
							);
			$this->form_validation->set_rules($config_rule);
			if($this->form_validation->run() == TRUE){
				switch($data_sk['KD_JENIS_SURAT']){
					case 2: #surat tugas
						// $cek_isian_golongan	= multiple_input($this->input->post('pangkat_gol'));
						// if($cek_isian_golongan['valid'] == FALSE){
							// $save_sk = FALSE;
							// $data['errors'][] = "Gagal menyimpan surat perjalanan dinas. Isikan data pegawai yang melaksanakan tugas dengan lengkap.";
						// }
						break;
					case 3: //jenis surat undangan
						$simar['KD_GEDUNG']						= $this->input->post('kd_gedung');
						$simar['KD_RUANG']							= $this->input->post('kd_ruang');
						$simar['TGL_MULAI']							= $this->input->post('tgl_mulai')." ".$this->input->post('jam_mulai').":00";
						$simar['TGL_SELESAI']						= $this->input->post('tgl_selesai')." ".$this->input->post('jam_selesai').":00";
						if($simar['KD_RUANG'][0] != null){
							foreach($simar['KD_RUANG'] as $kd_ruang){
								$parameter		= array('api_kode' => 1011, 'api_subkode' => 1, 'api_search' => array($kd_ruang, $simar['TGL_MULAI'], $simar['TGL_SELESAI']));
								$data_simar 	= $this->mdl_tnde->api_simar('simar_general/get_jadwal', 'json', 'POST', $parameter);
								if(count($data_simar) > 0){
									foreach($data_simar as $key => $val){
										$data['ruang_tidak_tersedia'][$key] = $val;
										$parameter 	= array('api_kode' => 2001, 'api_subkode' => 2, 'api_search' => array($val['NIM'])); #kd_pgw
										$pgw 		= $this->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
										$data['peminjam_ruang'][$key] = (!empty($pgw[0]) ? $pgw[0] : '');
									}
								}
							} 
							if(ISSET($data['ruang_tidak_tersedia'])){ 
								$this->session->set_flashdata('ruang_tidak_tersedia', $data['ruang_tidak_tersedia']);
								$data['errors'][] = "Gagal menyimpan surat undangan dikarenakan ruangan telah digunakan.";
								$save_sk = FALSE;
							}
							if(!empty($data['peminjam_ruang'])){
								$this->session->set_flashdata('peminjam_ruang', $data['peminjam_ruang']);
							}
						}
						#CEK BENTROK JADWAL PEGAWAI/PEJABAT
						$pgw_list = array();
						if(!empty($kpd_pejabat)){
							foreach($kpd_pejabat as $val){
								if(!empty($val)){
									if(! in_array($val, $pgw_list)){
										$pgw_list[] = $val;
									}
								}
							}
						}
						if(!empty($kpd_pegawai)){
							foreach($kpd_pegawai as $val){
								if(!empty($val)){
									if(! in_array($val, $pgw_list)){
										$pgw_list[] = $val;
									}
								}
							}
						}
						/*if(!empty($pgw_list)){
							$bentrok = $this->sisurat_external->agenda($pgw_list, $simar['TGL_MULAI'], $simar['TGL_SELESAI']);
							if(!empty($bentrok)){
								foreach($bentrok as $kd_pgw => $val){
									$parameter 		= array('api_kode' => 2001, 'api_subkode' => 2, 'api_search' => array($kd_pgw)); #kd_pgw
									$pgw_bentrok	= $this->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
									if(!empty($val['TA'])){
										$data['bentrok'][$kd_pgw]['TA'] = $val['TA'];
									}
									$data['bentrok'][$kd_pgw]['pegawai'] = (!empty($pgw_bentrok[0]) ? $pgw_bentrok[0] : '');
								}
							}
							if(!empty($data['bentrok'])){
								$this->session->set_flashdata('bentrok', $data['bentrok']);
							}
						}*/
					break;
					case 19:
						$tgs['NO_SURAT']	= str_replace(" ", "", $this->input->post('no_tugas'));
						$parameter		= array('api_kode' => 1001, 'api_subkode' => 4, 'api_search' => array('MD_SURAT_KELUAR2', $tgs));
						$cek_tugas		= $this->mdl_tnde->get_api('tnde_general/get_data', 'json', 'POST', $parameter);
						if(empty($cek_tugas)){
							$data['errors'][] = "Nomor surat tugas tidak ditemukan. Pastikan Anda menuliskan nomor surat tugas dengan benar.";
							$save_sk = FALSE;
						}
						break;
					case 21:
						$sp_isi_surat			= $this->input->post('isi_surat');
						$sp_jumlah				= $this->input->post('jumlah');
						$sp_keterangan		= $this->input->post('keterangan');
						$cek_sp_isi 			= multiple_input($sp_isi_surat);
						$cek_sp_jml 			= multiple_input($sp_jumlah);
						$cek_sp_ket 			= multiple_input($sp_keterangan);
						if(($cek_sp_isi['valid'] == FALSE) && ($cek_sp_jml['valid'] == FALSE) && ($cek_sp_ket['valid'] == FALSE)){
							$data['errors'][] = "Isi surat tidak boleh kosong.";
						}
						break;
					case 22: #notulen rapat
						$config = array(
											array('field' => 'kd_jenis_surat', 'label' => 'Jenis surat', 'rules' => 'required'),
											array('field' => 'no_undangan', 'label' => 'No undangan rapat', 'rules' => 'required'),
											array('field' => 'pimpinan_rapat', 'label' => 'Pimpinan rapat', 'rules' => 'required'),
											array('field' => 'notulis', 'label' => 'Notulis', 'rules' => 'required'),
											array('field' => 'kd_status_simpan', 'label' => 'Status simpan', 'rules' => 'required')
										);
						$this->form_validation->set_rules($config);
						$save_sk = FALSE;
						if($this->form_validation->run() == TRUE){
							$no_undangan = $this->input->post('no_undangan');
							$parameter	= array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('MD_SURAT_KELUAR2', 'NO_SURAT', $no_undangan));
							$cek_exist 	= $this->mdl_tnde->get_api('tnde_general/get_data', 'json', 'POST', $parameter);
							if(!empty($cek_exist['ID_SURAT_KELUAR'])){
								$parameter		= array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('D_NOTULEN', 'ID_SK_UNDANGAN', $cek_exist['ID_SURAT_KELUAR']));
								$cek_notulen	= $this->mdl_tnde->get_api('tnde_general/get_data', 'json', 'POST', $parameter);
								if(empty($cek_notulen['ID_NOTULEN'])){
									$save_sk	= TRUE;
								}
							}else{
								$data['errors'][] = "Data surat undangan dengan nomor </b>'".$no_undangan."'</b> tidak ada.";
							}
						}else{
							$data['errors'][] = validation_errors();
						}
						$kd_status_presensi = $this->input->post('kd_status_presensi');
						$wakil = $this->input->post('wakil');
						if(!empty($kd_status_presensi)){
							$tt = 1;
							$w = 1;
							foreach($kd_status_presensi as $k => $val){
								if($val == 'DW'){ #DIWAKILKAN
									if(empty($wakil[$k])){
										$ada_error[] = $tt;
										$tt++;
									}else{
										$parameter		= array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('D_PENERIMA_SK', 'ID_PENERIMA_SK', $k));
										$cek_peserta	= $this->mdl_tnde->get_api('tnde_general/get_data', 'json', 'POST', $parameter);
										$yg_diundang = $cek_peserta['PENERIMA_SURAT']; #kd_pegawai

										$yg_mewakili = $wakil[$k]; #kd_pegawai
										if($yg_diundang == $yg_mewakili){
											$wakil_error[] = $w;
											$w++;
										}
									}
								} 
							}
							if(ISSET($ada_error)){
								$data['errors'][] = "Kolom 'diwakilkan oleh' harus di isi, untuk peserta yang tidak mengikuti acara dengan status diwakilkan.";
								$save_sk = FALSE;
							}
							if(ISSET($wakil_error)){
								$data['errors'][] = "Peserta yang tidak dapat hadir tidak boleh diwakilkan oleh peserta yang sama.";
								$save_sk = FALSE;
							}
						}
					break;
				}
				
				# WAJIB UPLOAD DOKUMEN
				$wajib_dokumen = array(6, 13, 14, 16);
				if(in_array($data_sk['KD_JENIS_SURAT'], $wajib_dokumen)){
					if(!empty($_FILES['dokumen']['name'])){
						$doc_sk 				= $_FILES['dokumen'];
						$cek_doc 			= cek_doc($doc_sk);
						if(!empty($cek_doc['errors'])){
							$data['errors'] = $cek_doc['errors'];
							$save_sk = FALSE;
						}
					}else{
						$data['errors'][] = "Dokumen surat tidak boleh kosong.";
						$save_sk = FALSE;
					}
				}
				
				#WAJIB BODY SURAT
				$wajib_body_surat 		= array(11, 15, 17, 18);
				if(in_array($data_sk['KD_JENIS_SURAT'], $wajib_body_surat)){
					if(empty($data_sk['ISI_SURAT'])){
						$data['errors'][] = "Kolom 'Body Surat atau Isi Surat' tidak boleh kosong.";
						$save_sk = FALSE;
					}
				}
				
				#WAJIB TUJUAN SURAT
				$tujuan_eks 		= multiple_input($kpd_lainnya);
				$wajib_tujuan 		= array(3, 21, 9, 10, 11, 13, 14, 15, 16, 2, 5, 6, 19);
				if(in_array($data_sk['KD_JENIS_SURAT'], $wajib_tujuan)){
					if(($tujuan_eks['valid'] == FALSE) && empty($kpd_pejabat[0]) && empty($kpd_pegawai[0]) && empty($kpd_mahasiswa[0])){
						$data['errors'][] = "Kolom tujuan surat atau kolom peserta tidak boleh kosong";
						$save_sk = FALSE;
					}
				}
				
				#WAJIB UNIT YG MENGELUARKAN
				$wajib_untuk_unit = array(10, 12, 13, 14, 15, 16, 4, 6, 8);
				if(in_array($data_sk['KD_JENIS_SURAT'], $wajib_untuk_unit)){
					if(empty($data_sk['UNTUK_UNIT'])){
						$data['errors'][] = "Kolom 'Unit yang mengeluarkan' tidak boleh kosong ";
						$save_sk = FALSE;
					}
				}
			}else{
				$data['errors'][] = validation_errors();
				$save_sk = FALSE;
			}

			if($save_sk == TRUE){
				$parameter	= array('api_kode' => 90001, 'api_subkode' => 1, 'api_search' => array($data_sk));
				$save_sk 		= $this->mdl_tnde->get_api('tnde_surat_keluar/insert_surat_kaluar', 'json', 'POST', $parameter);
				if($save_sk	==	FALSE){
					$data['errors'][] = "Gagal menyimpan surat. #0";
				}else{
					$save_sk	=	TRUE;
					if(!empty($_FILES['dokumen'])){
						$waktu_simpan 	= date("d/m/Y H:i:s");
						$doc_sk 				= $_FILES['dokumen'];
						$cek_doc 			= cek_doc($doc_sk);
						if($cek_doc['valid'] == TRUE){
							$meta['tmp_name'] 			= $doc_sk['tmp_name'];
							$meta['name'] 					= str_replace("_","", str_replace(" ","", $doc_sk['name']));
							$meta['type']	 					= $doc_sk['type'];
							$meta['isi_dokumen']			= '0';
							$meta['id_surat_keluar']		= $data_sk['ID_SURAT_KELUAR'];
							$insert_file		= $this->_insert_file($meta);
							if(!empty($insert_file['errors'])){
								$data['errors'] = $insert_file['errors'];
							}
						}else{
							$data['errors'] = $cek_doc['errors'];
						}
					}
				}
			}
			if($save_sk == TRUE){
				# SIMPAN DATA BERDASARKAN JENIS SURATNYA
				switch($data_sk['KD_JENIS_SURAT']){
					case 2: #surat tugas
						$tugas['ID_SURAT_KELUAR']		= $this->input->post('id_surat_keluar');
						$tugas['KEPERLUAN']					= str_replace("'", "''", $this->input->post('keperluan'));
						$tugas['TGL_MULAI']						= ($this->input->post('tgl_mulai') != null ? $this->input->post('tgl_mulai')." 00:00:01" : "");
						$tugas['TGL_SELESAI']					= ($this->input->post('tgl_selesai') != null ? $this->input->post('tgl_selesai')." 23:59:59" : "");
						$tugas['TEMPAT']								= str_replace("'", "''", $this->input->post('tempat_tugas'));
						$parameter		= array('api_kode' => 4004, 'api_subkode' => 1, 'api_search' => array($tugas));
						$insert_stugas 	= $this->mdl_tnde->get_api('tnde_pegawai/insert_detail_stugas', 'json', 'POST', $parameter);
						if($insert_stugas == TRUE){
							$save_sk = TRUE; 
						}else{
							$save_sk = FALSE;
							$data['errors'][] = "Gagal menyimpan detail surat tugas.";
						}

						break;
					case 3:
						$undangan['ID_SURAT_KELUAR'] 	= $this->input->post('id_surat_keluar');
						$undangan['ACARA'] 							= str_replace("'", "''", $this->input->post('acara'));
						$undangan['TGL_MULAI'] 					= $this->input->post('tgl_mulai')." ".$this->input->post('jam_mulai').":00";
						if((empty($this->input->post('tgl_selesai')))&&(!empty($this->input->post('jam_selesai')))){
							$undangan['TGL_SELESAI']			= $this->input->post('tgl_mulai')." ".$this->input->post('jam_selesai').":00";
						}else if((!empty($this->input->post('tgl_selesai')))&&(empty($this->input->post('jam_selesai')))){
							$undangan['TGL_SELESAI']			= $this->input->post('tgl_selesai')." 00:00:00";
						}else if(!empty($this->input->post('tgl_selesai'))&&(!empty($this->input->post('jam_selesai')))){
							$undangan['TGL_SELESAI']			= $this->input->post('tgl_selesai')." ".$this->input->post('jam_selesai').":00";
						}else{
							$undangan['TGL_SELESAI']			= "";
						}
						
						$undangan['TEMPAT'] 						= str_replace("'", "''", $this->input->post('tempat_lain'));
						$parameter		= array('api_kode' => 4004, 'api_subkode' => 1, 'api_search' => array($undangan));
						$detail_acara 	= $this->mdl_tnde->get_api('tnde_pegawai/insert_detail_undangan', 'json', 'POST', $parameter);
						if($detail_acara == TRUE){
							if($simar['KD_RUANG'][0] != null){
								$pinjam = array(
										'kd_peminjam'=>$this->session->userdata('kd_pegawai'),
										'nama'=>$this->session->userdata('nm_pgw_f'),
										'alamat'=>'Jl.Jogja',
										'institusi'=>$this->session->userdata('unit_nama'),
										'jabatan'=>$this->session->userdata('str_nama'),
										'telp'=>'0888888888',
										'tgl_mulai'=>$simar['TGL_MULAI'],
										'tgl_selesai'=>$simar['TGL_SELESAI'],
										'keperluan'=>$undangan['ACARA'],
										'kd_group_user'=>'4',
										'kd_system'=>'2',
										'kd_gedung'=>$simar['KD_GEDUNG']
										);
								$parameter				= array('api_kode' => 1044, 'api_subkode' => 1, 'api_search' => $pinjam);
								$hasil_pinjam		 	= $this->mdl_tnde->api_simar('simar_general/insert_procedure_peminjaman_kelas02', 'json', 'POST', $parameter);
								if(ISSET($hasil_pinjam['data'][':hasil'])){
									if($hasil_pinjam['data'][':hasil'] != null){
										$kd_peminjaman 								= $hasil_pinjam['data'][':hasil'];
										$dpinjam['KD_PEMINJAMAN'] 		= $kd_peminjaman;
										$wpinjam['ID_SURAT_KELUAR']	= $this->input->post('id_surat_keluar');
										$parameter										= array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('D_DETAIL_SUNDANGAN', $dpinjam, $wpinjam));
										$up_kd_peminjaman 						= $this->mdl_tnde->get_api('tnde_general/update_data', 'json', 'POST', $parameter);
										if($up_kd_peminjaman == FALSE){
											$data['errors'][] = "Gagal memperbaharui KD_PEMINJAMAN";
										}
										foreach($simar['KD_RUANG'] as $ruang){
												$api_search = array(
														'kd_peminjaman'=>$kd_peminjaman,
														'kd_ruang'=>$ruang
												);
												$parameter		= array('api_kode' => 1050, 'api_subkode' => 1, 'api_search' => $api_search);
												$insert_detail 	= $this->mdl_tnde->api_simar('simar_general/insert_det_peminjaman_kelas', 'json', 'POST', $parameter);
												if($insert_detail == TRUE){
													$save_sk = TRUE;
												}else{
													$save_sk = FALSE;
													$data['errors'][] = "Gagal meminjam ruang ".$ruang;
												}
										}
									}else{
										$save_sk = FALSE;
										$data['errors'][] = "Gagal meminjam ruang.";
									}
								}else{
									$save_sk = FALSE;
									$data['errors'][] = "Gagal meminjam ruang.";
								}
							}
						}else{
							$save_sk = FALSE;
							$data['errors'][] = "Gagal menyimpan detail acara.";
						}
						$no_rp				= $this->input->post('no_rp');
						$pembahasan	= $this->input->post('rincian_pembahasan');
						$cek_data	= multiple_input($pembahasan);
						if($cek_data['valid'] == TRUE){
							$n = 0;
							foreach($pembahasan as $key => $val){
								$agenda['AGENDA']							= str_replace("'", "''", $val);
								if($agenda['AGENDA'] != null){
									$n = $n + 1;
									$agenda['ID_SURAT_KELUAR'] 		= $this->input->post('id_surat_keluar');
									$agenda['NO_URUT'] 							= (!empty($no_rp[$key]) ? $no_rp[$key] : $n);
									$parameter											= array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('D_AGENDA_UNDANGAN', $agenda));
									$detail_agenda										= $this->mdl_tnde->get_api('tnde_general/insert_data', 'json', 'POST', $parameter); 
									if($detail_agenda == FALSE){
										$save_sk	= FALSE;
										$data['errors'][] = "Gagal menyimpan rincian pembahasan ke-".$n;
									}
								}
							}
						}

						break;
					case 5:
						$d_instruksi['ID_SURAT_KELUAR']	= $this->input->post('id_surat_keluar');
						$d_instruksi['DALAM_RANGKA']		= $this->input->post('dalam_rangka');
						$d_instruksi['UNTUK']						= $this->input->post('untuk');
						$insert_instruksi = $this->mdl_tnde->get_api('tnde_general/insert_data', 'json', 'POST', array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('D_DETAIL_INSTRUKSI', $d_instruksi)));
						if($insert_instruksi == TRUE){
							$save_sk = TRUE; 
						}else{
							$save_sk = FALSE;
							$data['errors'][] = "Gagal menyimpan detail surat instruksi.";
						}
						break;

					case 19:
						$detail_spd['ID_SURAT_KELUAR']		=	str_replace("'", "''", $this->input->post('id_surat_keluar'));
						$detail_spd['SURAT_TUGAS']				=	str_replace(" ", "", $this->input->post('no_tugas'));
						$detail_spd['TRANSPORTASI']				=	str_replace("'", "''", $this->input->post('transportasi'));
						$detail_spd['TEMPAT_BERANGKAT']	=	str_replace("'", "''", $this->input->post('tempat_berangkat'));
						$detail_spd['TEMPAT_TUJUAN']			=	str_replace("'", "''", $this->input->post('tempat_tujuan'));
						$detail_spd['TEMPAT_BERANGKAT_LAIN']	=	str_replace("'", "''", $this->input->post('tempat_berangkat_lain'));
						$detail_spd['TEMPAT_TUJUAN_LAIN']			=	str_replace("'", "''", $this->input->post('tempat_tujuan_lain'));
						$detail_spd['TGL_BERANGKAT']			=	($this->input->post('tgl_berangkat') == null ? '' : $this->input->post('tgl_berangkat')." 00:00:00");
						$detail_spd['TGL_TIBA']							=	($this->input->post('tgl_tiba') == null ? '' : $this->input->post('tgl_tiba')." 00:00:00");
						$detail_spd['TGL_KEMBALI']					=	($this->input->post('tgl_kembali') == null ? '' : $this->input->post('tgl_kembali')." 00:00:00");
						$detail_spd['ANGGARAN_INSTANSI']	= str_replace("'", "''", $this->input->post('instansi'));
						$detail_spd['ANGGARAN_AKUN']		= str_replace("'", "''", $this->input->post('akun'));
						$detail_spd['KETERANGAN']					= str_replace("'", "''", $this->input->post('keterangan'));
						$parameter		= array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($detail_spd));
						$insert_d_spd	=	$this->mdl_tnde->get_api('tnde_surat_keluar/insert_detail_spd', 'json', 'POST', $parameter);
						
						// print_r($insert_d_spd);
						
						// echo "cek";
						if($insert_d_spd == FALSE){
							$data['errors'][] = "Gagal menyimpan detail surat perjalanan dinas.";
						}

						$pengikut			= $this->input->post('nm_pengikut');
						$tgl_lahir_peng	= $this->input->post('tgl_lahir_pengikut');
						$ket_pengikut	= $this->input->post('ket_pengikut');
						$cek_pengikut	= multiple_input($pengikut);
						if($cek_pengikut['valid'] == TRUE){
							foreach($pengikut as $key => $val){
								$nama = str_replace("'", "''", $val);
								if($nama != null){
									$d_pengikut['ID_SURAT_KELUAR']	= $this->input->post('id_surat_keluar');
									$d_pengikut['NAMA']							= str_replace("'", "''", $nama);
									$d_pengikut['TGL_LAHIR']					= $tgl_lahir_peng[$key];
									$d_pengikut['KETERANGAN']			= str_replace("'", "''", $ket_pengikut[$key]);
									$parameter			= array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($d_pengikut));
									$insert_pengikut	=	$this->mdl_tnde->get_api('tnde_surat_keluar/insert_pengikut_spd', 'json', 'POST', $parameter);
									if($insert_pengikut == FALSE){
										$data['errors'][] = "Gagal menyimpan data pengikut dengan nama ".$d_pengikut['NAMA'].", dan tanggal lahir ".$d_pengikut['TGL_LAHIR'];
									}
								}
							}
						}
						break;
					case 21: #SURAT PENGANTAR
						$sp_isi_surat				= $this->input->post('isi_surat');
						$sp_jumlah					= $this->input->post('jumlah');
						$sp_keterangan			= $this->input->post('keterangan');
						$tot = count($sp_isi_surat);
						$no = 1;
						if(!empty($sp_isi_surat)){
							foreach($sp_isi_surat as $n => $val){
								$isi_surat = fixed_isi_surat($sp_isi_surat[$n]);
								if(!empty($isi_surat)){
									$pengantar['ID_SURAT_KELUAR']	= $this->input->post('id_surat_keluar');
									$pengantar['ISI_SURAT']				= fixed_isi_surat($sp_isi_surat[$n]);
									$pengantar['JUMLAH']					= fixed_isi_surat($sp_jumlah[$n]);
									$pengantar['KETERANGAN']		= fixed_isi_surat($sp_keterangan[$n]);
									$pengantar['NO_URUT']				= $no;
									if(($pengantar['ISI_SURAT'] != NULL) || ($pengantar['JUMLAH'] != NULL) || ($pengantar['KETERANGAN'] != NULL)){
										$insert_sp = $this->mdl_tnde->get_api('tnde_general/insert_data', 'json', 'POST', array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('D_DETAIL_SPENGANTAR', $pengantar)));
										if($insert_sp == FALSE){
											$data['errors'][] = "Gagal menyimpan isi surat ke- ".$no;
										}
									}
									
								}
								$no++;
							}
						}
						if(ISSET($data['errors'])){ $save_sk = FALSE;}
						break;
					case 22: #NOTULEN RAPAT
						$notulen['ID_SURAT_KELUAR']	= $this->input->post('id_surat_keluar');
						$notulen['PIMPINAN_RAPAT']		= $this->input->post('pimpinan_rapat');
						
						$notulen['NOTULIS']						= $this->input->post('notulis');
						$notulen['ID_SK_UNDANGAN']		= $cek_exist['ID_SURAT_KELUAR'];
						$parameter				= array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('D_NOTULEN', $notulen));
						$insert_d_notulen 	= $this->mdl_tnde->get_api('tnde_general/insert_data', 'json', 'POST', $parameter);
						if($insert_d_notulen == TRUE){
							#SIMPAN PRESENSI UNDANGAN 
							$kd_status_presensi = $this->input->post('kd_status_presensi');
							$wakil = $this->input->post('wakil');
							$nm_peserta = $this->input->post('nm_peserta');
							#PESERTA UNDANGAN
							if(!empty($kd_status_presensi)){
								$p = 1;
								foreach($kd_status_presensi as $kagi => $val){
									$presensi['ID_SURAT_KELUAR']			= $this->input->post('id_surat_keluar');
									$presensi['ID_PENERIMA_SK']				= $kagi;
									$presensi['KD_STATUS_PRESENSI']	= $val;
									if(!empty($wakil[$kagi])){
										$presensi['WAKIL'] = $wakil[$kagi];
									}else{
										$presensi['WAKIL'] = '';
									}
									$parameter			= array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('D_PRESENSI_UNDANGAN', $presensi));
									$insert_presensi = $this->mdl_tnde->get_api('tnde_general/insert_data', 'json', 'POST', $parameter);
									if($insert_presensi == FALSE){
										$data['errors'][] = "Gagal menyimpan data presensi ".$nm_peserta[$kagi];
									}
									
									#KIRIM NOTULEN KE PEGAWAI YG TERKAIT DENGAN ISI NOTULEN
									$parameter			= array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('D_PENERIMA_SK', 'ID_PENERIMA_SK', $kagi));
									$cek_undangan	= $this->mdl_tnde->get_api('tnde_general/get_data', 'json', 'POST', $parameter);
									if(!empty($cek_undangan)){
										$p_notulen['ID_SURAT_KELUAR'] 			= $this->input->post('id_surat_keluar');
										$p_notulen['PENERIMA_SURAT'] 				= $cek_undangan['PENERIMA_SURAT'];
										$p_notulen['JABATAN_PENERIMA'] 			= $cek_undangan['JABATAN_PENERIMA'];
										$p_notulen['KD_GRUP_TUJUAN'] 			= $cek_undangan['KD_GRUP_TUJUAN'];
										$p_notulen['KD_STATUS_DISTRIBUSI'] 	= 'PS';
										$p_notulen['ID_STATUS_SM'] 					= '1'; #belum dibaca
										$p_notulen['KD_JENIS_TEMBUSAN']		= '0';
										$p_notulen['KD_JENIS_KEPADA']				= '0';
										$p_notulen['NO_URUT']								= $p;
										$parameter		= array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('D_PENERIMA_SK', $p_notulen));
										$kirim_notulen = $this->mdl_tnde->get_api('tnde_general/insert_data', 'json', 'POST', $parameter);
										if($kirim_notulen == FALSE){
											$data['errors'][] = "Gagal mengirim laporan notulen ke pegawai ".$nm_peserta[$kagi];
										}else{
											$p++;
										}
									}else{
										$data['errors'][] = "id_penerima_sk:".$kagi." not found.";
									}
									#KIRIM NOTULEN KEPEGAWAI YG MEWAKILI
									$parameter 	= array('api_kode' => 2001, 'api_subkode' => 2, 'api_search' => array($wakil[$kagi]));
									$detail 			= $this->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
									if(!empty($detail[0])){
										$nm_pgw = str_replace("'", "", $detail[0]['NM_PGW_F']);
										$parameter 	= array('api_kode' => 1121, 'api_subkode' => 3, 'api_search' => array($data_sk['TGL_SURAT'], $wakil[$kagi], 1));
										$jab_str 			= $this->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
										if(!empty($jab_str)){
											foreach($jab_str as $dd){
												if($dd['RR_JENIS2'] == 1){
													$jab = $dd;
												}
											}
											if(empty($jab)){
												$jab = $jab_str[0];
											}
										}
										$str_id	= (!empty($jab['STR_ID']) ? $jab['STR_ID'] : '');
							
										$p_notulen_wk['ID_SURAT_KELUAR'] 				= $this->input->post('id_surat_keluar');
										$p_notulen_wk['PENERIMA_SURAT'] 				= $wakil[$kagi];
										$p_notulen_wk['JABATAN_PENERIMA'] 			= $str_id;
										$p_notulen_wk['KD_GRUP_TUJUAN'] 				= 'PGW01';
										$p_notulen_wk['KD_STATUS_DISTRIBUSI'] 	= 'PS';
										$p_notulen_wk['ID_STATUS_SM'] 					= '1'; #belum dibaca
										$p_notulen_wk['KD_JENIS_TEMBUSAN']			= '0';
										$p_notulen_wk['KD_JENIS_KEPADA']				= '0';
										$p_notulen_wk['NO_URUT']								= $p;
										$parameter		= array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('D_PENERIMA_SK', $p_notulen_wk));
										$kirim_notulen_wk = $this->mdl_tnde->get_api('tnde_general/insert_data', 'json', 'POST', $parameter);
										if($kirim_notulen_wk == FALSE){
											$data['errors'][] = "Gagal mengirim laporan notulen ke pegawai ".$nm_pgw;
										}else{
											$p++;
										}
									}
								}
							}
							#PESERTA TAMBAHAN :: PGW01
							$peserta_pgw = explode("<$>", $this->input->post('peserta_pgw'));
							$t = 0;
							if(!empty($peserta_pgw)){
								foreach($peserta_pgw as $d_pgw){
									$parameter 	= array('api_kode' => 2001, 'api_subkode' => 2, 'api_search' => array($d_pgw));
									$detail 			= $this->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
									if(!empty($detail[0])){
										$nm_pgw = str_replace("'", "", $detail[0]['NM_PGW_F']);
										$parameter 	= array('api_kode' => 1121, 'api_subkode' => 3, 'api_search' => array($data_sk['TGL_SURAT'], $d_pgw, 1));
										$jab_str 			= $this->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
										if(!empty($jab_str)){
											foreach($jab_str as $dd){
												if($dd['RR_JENIS2'] == 1){
													$jab = $dd;
												}
											}
											if(empty($jab)){
												$jab = $jab_str[0];
											}
										}
										$str_id	= (!empty($jab['STR_ID']) ? $jab['STR_ID'] : '');
										
										$tam_peg['ID_SURAT_KELUAR'] 			= $this->input->post('id_surat_keluar');
										$tam_peg['PENERIMA_SURAT'] 			= $d_pgw;
										$tam_peg['JABATAN_PENERIMA'] 		= $str_id;
										$tam_peg['KD_GRUP_TUJUAN'] 			= 'PGW01';
										$tam_peg['KD_STATUS_DISTRIBUSI']	= 'PS';
										$tam_peg['ID_STATUS_SM'] 				= '1'; #belum dibaca
										$tam_peg['KD_JENIS_TEMBUSAN']		= '0';
										$tam_peg['KD_JENIS_KEPADA']			= '0';
										$tam_peg['NO_URUT']							= $t;
										$parameter		= array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($tam_peg));
										$tam_peserta	= $this->mdl_tnde->get_api('tnde_surat_keluar/insert_procedure_penerima_sk', 'json', 'POST', $parameter);
										if(!empty($tam_peserta['data'][':hasil'])){
											$detail['ID_PENERIMA_SK']	= $tam_peserta['data'][':hasil'];
											$parameter		= array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('D_PRESENSI_UND_TB', $detail));
											$tambahkan	= $this->mdl_tnde->get_api('tnde_general/insert_data', 'json', 'POST', $parameter);
											if($tambahkan == false){
												$data['errors'][] = "Gagal menyimpan data peserta tambahan <b>'".$nm_pgw."'</b>. #PGW.01";
											}
											$t++;
										}else{
											$data['errors'][] = "Gagal menyimpan data peserta tambahan <b>'".$nm_pgw."'</b>. #PGW.00";
										}
									}
								}
							}
							#PESERTA TAMBAHAN :: LAINNYA
							$peserta_eks = $this->input->post('peserta_eks');
							$cek_peserta_eks = multiple_input($peserta_eks);
							if($cek_peserta_eks['valid'] == TRUE){
								foreach($peserta_eks as $d_eks){
									if($d_eks != null){
										$tam_eks['ID_SURAT_KELUAR'] 			= $this->input->post('id_surat_keluar');
										$tam_eks['PENERIMA_SURAT'] 			= str_replace("'", "''", $d_eks);
										$tam_eks['JABATAN_PENERIMA'] 		= "";
										$tam_eks['KD_GRUP_TUJUAN'] 			= 'EKS01';
										$tam_eks['KD_STATUS_DISTRIBUSI']	= "PS";
										$tam_eks['ID_STATUS_SM'] 				= "1";
										$tam_eks['KD_JENIS_TEMBUSAN'] 	= "0"; 
										$tam_eks['KD_JENIS_KEPADA']			= "0";
										$tam_eks['NO_URUT']							= $t;
										$parameter				= array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($tam_eks));
										$tam_peserta_eks	= $this->mdl_tnde->get_api('tnde_surat_keluar/insert_procedure_penerima_sk', 'json', 'POST', $parameter);
										if(!empty($tam_peserta_eks['data'][':hasil'])){
											$det_eks['ID_PENERIMA_SK']		= $tam_peserta_eks['data'][':hasil'];
											$parameter		= array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('D_PRESENSI_UND_TB', $det_eks));
											$tambahkan	= $this->mdl_tnde->get_api('tnde_general/insert_data', 'json', 'POST', $parameter);
											if($tambahkan == false){
												$data['errors'][] = "Gagal menyimpan data peserta tambahan. #EKS.01";
											}
											$t++;
										}else{
											$data['errors'][] = "Gagal menyimpan data peserta tambahan. #EKS.00";
										}
									}
								}
							}
							#PESERTA TAMBAHAN :: MHS01
							$peserta_mhs = explode("<$>", $this->input->post('peserta_mhs'));
							if(!empty($peserta_mhs)){
								foreach($peserta_mhs as $d_msh){
									$exp_msh = explode("#", $d_msh);
									$tam_mhs['ID_SURAT_KELUAR'] 			= $this->input->post('id_surat_keluar');
									$tam_mhs['PENERIMA_SURAT'] 			= $exp_msh[0];
									$tam_mhs['JABATAN_PENERIMA'] 		= $exp_msh[1];
									$tam_mhs['KD_GRUP_TUJUAN'] 			= $exp_msh[2];
									$tam_mhs['KD_STATUS_DISTRIBUSI']	= 'PS';
									$tam_mhs['ID_STATUS_SM'] 				= '1'; #belum dibaca
									$tam_mhs['KD_JENIS_TEMBUSAN']		= '0';
									$tam_mhs['KD_JENIS_KEPADA']			= '0';
									$tam_mhs['NO_URUT']							= $t;
									$parameter		= array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($tam_mhs));
									$ins_mhs	= $this->mdl_tnde->get_api('tnde_surat_keluar/insert_procedure_penerima_sk', 'json', 'POST', $parameter);
									if(!empty($ins_mhs['data'][':hasil'])){
										$detail['ID_PENERIMA_SK']	= $ins_mhs['data'][':hasil'];
										$parameter		= array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('D_PRESENSI_UND_TB', $detail));
										$tambahkan	= $this->mdl_tnde->get_api('tnde_general/insert_data', 'json', 'POST', $parameter);
										if($tambahkan == false){
											$data['errors'][] = "Gagal menyimpan data peserta tambahan <b>'".$exp_msh[4]."'</b>. #MHS.01";
										}
										$t++;
									}else{
										$data['errors'][] = "Gagal menyimpan data peserta tambahan <b>'".$exp_msh[4]."'</b>. #MHS.00";
									}
								}
							}
							#TOPIK NOTULEN
							$topik = $this->input->post('topik');
							$tindak_lanjut = $this->input->post('tindak-lanjut');
							$batas_waktu = $this->input->post('batas-waktu');
							$pj = $this->input->post('pj');
							$t = 1;
							foreach($topik as $key => $val){
								$top['ID_SURAT_KELUAR']	= $this->input->post('id_surat_keluar');
								$top['TOPIK']							= $val;
								$top['BATAS_WAKTU']			= $batas_waktu[$key][0][0];
								$top['NO_URUT']					= $t;
								$parameter = array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($top));
								$insert_topik = $this->mdl_tnde->get_api('tnde_surat_keluar/insert_p_d_topik_notulen', 'json', 'POST', $parameter);
								if(!empty($insert_topik['data'][':hasil'])){
									#RENCANA TINDAK LANJUT
									$r = 1;
									foreach($tindak_lanjut[$key] as $ren => $v_ren){
										$rencana['ID_TOPIK'] 			= $insert_topik['data'][':hasil'];
										$rencana['RENCANA']			= $v_ren[0];
										$rencana['BATAS_WAKTU']	= $batas_waktu[$key][$ren][0];
										$rencana['NO_URUT'] 			= $r;
										$parameter = array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($rencana));
										$insert_rencana = $this->mdl_tnde->get_api('tnde_surat_keluar/insert_p_d_rencana_notulen', 'json', 'POST', $parameter);
										if(!empty($insert_rencana['data'][':hasil'])){
											#SIMPAN DATA PENANGGUNG JAWAB [RENCANA]
											$pj_rencana = explode("<$>", $pj[$key][$ren][0]);
											if(!empty($pj_rencana[0])){
												$n_ren = 1;
												foreach($pj_rencana as $dat){
													$exp = explode("#", $dat);
													$pj_ren['KD_PEGAWAI']	= $exp[0];
													$pj_ren['KD_JABATAN']		= $exp[1];
													$pj_ren['NO_URUT']			= $n_ren;
													$pj_ren['ID_RENCANA']		= $insert_rencana['data'][':hasil'];
													$parameter	= array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('D_PJ_RENCANA', $pj_ren));
													$insert_ren 	= $this->mdl_tnde->get_api('tnde_general/insert_data', 'json', 'POST', $parameter);
													if($insert_ren == FALSE){
														$data['errors'][] = "Gagal menyimpan data penanggung jawab '".$exp[3]."' pada topik ke-".$t.", rencana tindak lanjut ke-".$n_ren; #nama jabatan, no topik, no rencana
													}else{
														$n_ren++;
													}
												}
											}
											#SUB RENCANA TINDAK LANJUT
											$s = 1;
											foreach($v_ren as $sub => $v_sub){
												if($sub != 0){
													$subren['ID_RENCANA']		= $insert_rencana['data'][':hasil'];
													$subren['SUBRENCANA']		= $v_sub;
													$subren['BATAS_WAKTU']	= $batas_waktu[$key][$ren][$sub];
													$subren['NO_URUT']				= $s;
													$parameter					= array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($subren));
													$insert_subrencana	= $this->mdl_tnde->get_api('tnde_surat_keluar/insert_p_d_subrencana_notulen', 'json', 'POST', $parameter);
													if(!empty($insert_subrencana['data'][':hasil'])){
														#SIMPAN DATA PENANGGUNG JAWAB [SUBRENCANA]
														$pj_subrencana = explode("<$>", $pj[$key][$ren][$sub]);
														if(!empty($pj_subrencana[0])){
															$n_subren = 1;
															foreach($pj_subrencana as $dat){
																$exp = explode("#", $dat);
																$pj_subren['KD_PEGAWAI']			= $exp[0];
																$pj_subren['KD_JABATAN']			= $exp[1];
																$pj_subren['NO_URUT']					= $n_subren;
																$pj_subren['ID_SUBRENCANA']	= $insert_subrencana['data'][':hasil'];
																$parameter	= array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('D_PJ_SUBRENCANA', $pj_subren));
																$insert_subren 	= $this->mdl_tnde->get_api('tnde_general/insert_data', 'json', 'POST', $parameter);
																if($insert_subren == FALSE){
																	$data['errors'][] = "Gagal menyimpan data penanggung jawab '".$exp[3]."' pada topik ke-".$t.", rencana ke-".$n_ren.", pada sub rencana ke-".$n_subren; #nama jabatan, no topik, no rencana, no sub rencana
																}else{
																	$n_subren++;
																}
															}
														}
														$s++;
													}else{
														$data['errors'][] = 'Gagal menyimpan data subrencana tindak lanjut ke-'.$s.', rencana ke-'.$r.', pada topik ke-'.$t.'.';
													}
												}
											}
											$r++;
										}else{
											$data['errors'][] = 'Gagal menyimpan data rencana tindak lanjut ke-'.$r.' , pada topik ke-'.$t.'.';
										}
									}
									$t++;
								}else{
									$data['errors'][] = 'Gagal menyimpan data topik ke-'.$t.', dengan isi topik <b>"'.$top['TOPIK'].'"</b>.';
								}
							}
						}else{
							$data['errors'][] = "Gagal menyimpan data detail notulen rapat";
						}
						break;
				}
			}
			
			if($save_sk == TRUE){
				// INSERT DETAIL PERTIMBANGAN
				$cek_pertimbangan	=	multiple_input($pertimbangan);
				if($cek_pertimbangan['valid'] == TRUE){
					$a = 1;
					$n = 1;
					foreach($pertimbangan as $key => $val){
						if($val != null){
							$d_pertimbangan['ID_SURAT_KELUAR']		= $this->input->post('id_surat_keluar');
							$d_pertimbangan['ISI_PERTIMBANGAN']	= str_replace("'", "''", $val);
							$d_pertimbangan['NO_URUT']						= $n;
							$d_pertimbangan['KD_PERTIMBANGAN']	= $kd_pertimbangan[$key];
							$parameter				= array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('D_DETAIL_PERTIMBANGAN', $d_pertimbangan));
							$insert_pertimbangan	=	$this->mdl_tnde->get_api('tnde_general/insert_data', 'json', 'POST', $parameter);
							if($insert_pertimbangan == TRUE){
								$n++;
							}else{
								$data['errors'][]	=	'Gagal menyimpan data <b>"'.$d_pertimbangan['ISI_PERTIMBANGAN'].'"</b>';
							}
							$a++;
						}
					}
				}			
			
				// KIRIM SURAT KELUAR
				$jab_lainnya		= $this->input->post('jab_lainnya');
				$kpd_urut			= $this->input->post('kpd_urut');
				$up						= $this->input->post('up'); #untuk perhatian (surat naskah dinas umum / korespondensi)
				// KIRIM SURAT KE PEJABAT
				if(!empty($kpd_pejabat[0])){
					foreach($kpd_pejabat as $val){
						$exp = explode("#", $val);
						if(count($exp) == 5){
							$pjb['ID_SURAT_KELUAR']			= $this->input->post('id_surat_keluar');
							$pjb['PENERIMA_SURAT']				= $exp[0];
							$pjb['JABATAN_PENERIMA']		= $exp[1];
							$pjb['KD_GRUP_TUJUAN']			= $exp[2];
							$pjb['KD_STATUS_DISTRIBUSI']	= "PS";
							$pjb['ID_STATUS_SM']					= 1;
							$pjb['KD_JENIS_TEMBUSAN']		= 0;
							$pjb['NO_URUT']								= (!empty(conv_to_int($kpd_urut[$pjb['JABATAN_PENERIMA']."_".$pjb['PENERIMA_SURAT']])) ? conv_to_int($kpd_urut[$pjb['JABATAN_PENERIMA']."_".$pjb['PENERIMA_SURAT']]) : "");
							$pjb['KD_JENIS_KEPADA']			= (!empty($up[$pjb['JABATAN_PENERIMA']."_".$pjb['PENERIMA_SURAT']]) ? "1" : "0");
							$save_pjb = $this->mdl_tnde->get_api('tnde_surat_keluar/insert_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($pjb)));
							if($save_pjb == FALSE){
								$data['errors'][] = "Gagal mengirim ke tujuan surat ke ".$exp[3]; // Pejabat
							}
							
						}
					}
				}
				// KIRIM SURAT KE PEGAWAI
				if(!empty($kpd_pegawai[0])){
					foreach($kpd_pegawai as $val){
						$parameter 	= array('api_kode' => 2001, 'api_subkode' => 2, 'api_search' => array($val));
						$detail 			= $this->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
						if(!empty($detail[0])){
							$nm_pgw = str_replace("'", "", $detail[0]['NM_PGW_F']);
							/*$parameter 	= array('api_kode' => 1121, 'api_subkode' => 3, 'api_search' => array($data_sk['TGL_SURAT'], $val, 1));
							$jab_str 			= $this->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
							if(!empty($jab_str)){
								foreach($jab_str as $dd){
									if($dd['RR_JENIS2'] == 1){
										$jab = $dd;
									}
								}
								if(empty($jab)){
									$jab = $jab_str[0];
								}
							}
							$str_id	= (!empty($jab['STR_ID']) ? $jab['STR_ID'] : '');*/
							$str_id	= '';
		
							$pgw['ID_SURAT_KELUAR']			= $this->input->post('id_surat_keluar');
							$pgw['PENERIMA_SURAT']			= $val;
							$pgw['JABATAN_PENERIMA']		= $str_id;
							$pgw['KD_GRUP_TUJUAN']				= 'PGW01';
							$pgw['KD_STATUS_DISTRIBUSI']	= "PS";
							$pgw['ID_STATUS_SM']					= 1;
							$pgw['KD_JENIS_TEMBUSAN']		= 0;
							$pgw['NO_URUT']							= (!empty($kpd_urut[$val]) ? $kpd_urut[$val] : "");
							$pgw['KD_JENIS_KEPADA']			= (!empty($up[$val]) ? "1" : "0");
							$save_pgw = $this->mdl_tnde->get_api('tnde_surat_keluar/insert_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($pgw)));
							if($save_pgw == FALSE){
								$data['errors'][] = "Gagal mengirim ke tujuan surat ke ".$nm_pgw; // Pegawai
							}
						}
					}
				}
				// KIRIM SURAT KE MAHASISWA
				if(!empty($kpd_mahasiswa[0])){
					foreach($kpd_mahasiswa as $val){
						$exp = explode("#", $val);
						if(count($exp) == 5){
							$mhs['ID_SURAT_KELUAR']				= $this->input->post('id_surat_keluar');
							$mhs['PENERIMA_SURAT']				= $exp[0];
							$mhs['JABATAN_PENERIMA']			= $exp[1];
							$mhs['KD_GRUP_TUJUAN']				= $exp[2];
							$mhs['KD_STATUS_DISTRIBUSI']	= "PS";
							$mhs['ID_STATUS_SM']						= 1;
							$mhs['KD_JENIS_TEMBUSAN']		= 0;
							$mhs['NO_URUT']								= (!empty($kpd_urut[$mhs['PENERIMA_SURAT']]) ? $kpd_urut[$mhs['PENERIMA_SURAT']] : "");
							$mhs['KD_JENIS_KEPADA']				= (!empty($up[$mhs['PENERIMA_SURAT']]) ? "1" : "0");
							$save_mhs = $this->mdl_tnde->get_api('tnde_surat_keluar/insert_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($mhs)));
							if($save_mhs == FALSE){
								$data['errors'][] = "Gagal mengirim ke tujuan surat ke ".$exp[4]; // Mahasiswa
							}
						}
					}
				}
				// KIRIM SURAT KE LAINNYA
				$cek_kpd = multiple_input($kpd_lainnya);
				if($cek_kpd['valid'] == TRUE){
					foreach($kpd_lainnya as $key => $val){
						if($val != null){
							$ln['ID_SURAT_KELUAR'] 				= $this->input->post('id_surat_keluar');
							$ln['PENERIMA_SURAT'] 				= str_replace("'", "''", $val);
							$ln['JABATAN_PENERIMA'] 			= (!empty($jab_lainnya[$key]) ? str_replace("'", "''", $jab_lainnya[$key]) : '');
							$ln['KD_GRUP_TUJUAN'] 				= 'EKS01';
							$ln['KD_STATUS_DISTRIBUSI']	= "PS";
							$ln['ID_STATUS_SM'] 						= "1";
							$ln['KD_JENIS_TEMBUSAN'] 		= 0; 
							$ln['NO_URUT']									= (!empty($kpd_urut[$key]) ? $kpd_urut[$key] : "");
							$ln['KD_JENIS_KEPADA']				= (!empty($up[$key]) ? "1" : "0");
							$save_ln = $this->mdl_tnde->get_api('tnde_surat_keluar/insert_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($ln)));
							if($save_ln == FALSE){
								$data['errors'][] = "Gagal mengirim ke tujuan surat ke ".$val;
							}
						}
					}
				}
				
				#=========Tembusan Surat============================

				$lap_pjb 	= $this->input->post('lap_ts');
				$ts_urut		= $this->input->post('ts_urut');
				if(!empty($ts_pejabat)){
					foreach($ts_pejabat as $key => $val){
						$exp = explode("#", $val);
						if(count($exp) > 3){
							$ts_pjb['ID_SURAT_KELUAR'] 				= $this->input->post('id_surat_keluar');
							$ts_pjb['PENERIMA_SURAT'] 				= $exp[0];
							$ts_pjb['JABATAN_PENERIMA'] 			= $exp[1];
							$ts_pjb['KD_GRUP_TUJUAN'] 				= $exp[2];
							$ts_pjb['KD_STATUS_DISTRIBUSI'] 	= "TS";
							$ts_pjb['ID_STATUS_SM'] 						= "1";
							$ts_pjb['NO_URUT']									= (!empty($ts_urut[$ts_pjb['JABATAN_PENERIMA']."_".$ts_pjb['PENERIMA_SURAT']]) ? $ts_urut[$ts_pjb['JABATAN_PENERIMA']."_".$ts_pjb['PENERIMA_SURAT']] : "");
							$ts_pjb['KD_JENIS_TEMBUSAN'] 		= (!empty($lap_pjb[$ts_pjb['JABATAN_PENERIMA']."_".$ts_pjb['PENERIMA_SURAT']]) ? "1" : "0");
							$ts_pjb['KD_JENIS_KEPADA'] 				= '0';					
							$parameter	= array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($ts_pjb));
							$save_ts_pjb = $this->mdl_tnde->get_api('tnde_surat_keluar/insert_penerima_sk', 'json', 'POST', $parameter);
							if($save_ts_pjb == FALSE){
								$data['errors'][] = "Gagal mengirim tembusan surat ke ".$exp[3]; // Pejabat
							}
						}
					}
				}
				$cek_ts = multiple_input($ts_lainnya);
				if($cek_ts['valid'] == TRUE){
					foreach($ts_lainnya as $key => $val){
						if($val != null){
							$ts_ln['ID_SURAT_KELUAR'] 			= $this->input->post('id_surat_keluar');
							$ts_ln['PENERIMA_SURAT'] 				= str_replace("'", "''", $val);
							$ts_ln['JABATAN_PENERIMA'] 		= "";
							$ts_ln['KD_GRUP_TUJUAN'] 			= 'EKS01';
							$ts_ln['KD_STATUS_DISTRIBUSI'] 	= "TS";
							$ts_ln['ID_STATUS_SM'] 					= "1";
							$ts_ln['NO_URUT']								= (!empty($ts_urut[$key]) ? $ts_urut[$key] : "");
							$ts_ln['KD_JENIS_TEMBUSAN'] 		= (!empty($lap_pjb[$key]) ? "1" : "0");
							$ts_ln['KD_JENIS_KEPADA'] 			= '0';					
							$save_ts_ln = $this->mdl_tnde->get_api('tnde_surat_keluar/insert_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($ts_ln)));
							if($save_ts_ln == FALSE){
								$data['errors'][] = "Gagal mengirim ke tembusan surat ke ".$val;
							}
						}
					}
				}
		
			}
			if(! ISSET($data['errors'])){
				$data['success'][] = "Berhasil menyimpan surat keluar.";
				$this->session->set_flashdata('success', $data['success']);
			}else{
				$this->session->set_flashdata('errors', $data['errors']);
			}
			redirect('pegawai/surat_keluar');
		}else{
			redirect();
		}
	}
	
	function _insert_file($meta){
		$hasil = array();
		$hasil['valid'] = FALSE;
		if(!empty($meta)){
			#INSERT META
			$mm['ID_SURAT_KELUAR']	= $meta['id_surat_keluar'];
			$mm['NM_DOKUMEN_SK']	= $meta['name'];
			$mm['ISI_DOKUMEN_SK']	= $meta['isi_dokumen'];
			$mm['TIPE_FILE']					= $meta['type'];
			$mm['TIPE_DATA']				= 'BLOB';
			$parameter		= array('api_kode' => 90001, 'api_subkode' => 1, 'api_search' => array($mm));
			$insert_meta	= $this->mdl_tnde->get_api('tnde_surat_keluar/insert_dokumen', 'json', 'POST', $parameter);
			if(!empty($insert_meta[0]['ID'])){
				#INSERT BLOG
				$file['FILE_BLOB'] 				= base64_encode(file_get_contents($meta['tmp_name']));
				$wh['ID_DOKUMEN_SK']	= $insert_meta[0]['ID'];
				$parameter	= array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('BLOB_DOKUMEN_SK', $file, $wh));
				$upload 		= $this->mdl_tnde->get_api('tnde_general/insert_blob', 'json', 'POST', $parameter);
				if($upload){
					$hasil['valid'] = TRUE;
				}else{
					$hasil['errors'] = 'Gagal menyimpan dokumen '.$mm['NM_DOKUMEN_SK'].' #1';
				}
			}else{
				$hasil['errors'] = 'Gagal menyimpan dokumen '.$mm['NM_DOKUMEN_SK'].' #0';
			}
		}
		return $hasil;
	}
	
	function add_doc(){
		$referer = (!empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '');
		$auth = host_allowed($referer);
		if((!empty($_POST['add_doc'])) && ($auth)){
			$id_surat_keluar 	= $this->input->post('id_surat_keluar');
			$dokumen 				= $_FILES['doc'];
			$cek_file			 		= cek_doc($dokumen);
			$parameter		= array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('MD_SURAT_KELUAR2', 'ID_SURAT_KELUAR', $id_surat_keluar));
			$cek_status 	= $this->mdl_tnde->get_api('tnde_general/get_data', 'json', 'POST', $parameter);
			if($cek_status['KD_STATUS_SIMPAN'] != 3){
				if($cek_file['valid'] == TRUE){
					$meta['tmp_name'] 			= $dokumen['tmp_name'];
					$meta['name'] 					= str_replace("_","", str_replace(" ","", $dokumen['name']));
					$meta['type']	 					= $dokumen['type'];
					$meta['isi_dokumen']			= '0';
					$meta['id_surat_keluar']		= $id_surat_keluar;
					$insert_file		= $this->_insert_file($meta);
					if(!empty($insert_file['errors'])){
						$warnings[] = $insert_file['errors'];
					}
					if(!empty($warnings)){
						$this->session->set_flashdata('errors', $warnings);
					}else{
						$this->session->set_flashdata('success', 'Berhasil menambahkan dokumen');
					}
					redirect('pegawai/surat_keluar/detail/'.$id_surat_keluar);
				}else{
					$this->session->set_flashdata('errors', $cek_file['errors']);
					redirect('pegawai/surat_keluar/detail/'.$id_surat_keluar);
				}
			}else{
				$this->session->set_flashdata('errors', 'Gagal memperbarui data. Surat yang telah final tidak dapat diperbarui.');
				redirect('pegawai/surat_keluar/detail/'.$id_surat_keluar);
			}
		}else{
			redirect();
		}
	}
	
	function update_doc(){
		$referer = (!empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '');
		$auth = host_allowed($referer);
		if((!empty($_POST['update_dok'])) && ($auth)){
			$id_surat_keluar		=	$this->input->post('id_surat_keluar');
			$dok 						= $_FILES['up_dok'];
			$cek_dok 				= cek_doc($dok);
			$id_dok_lama 		= $this->input->post('id_dok_lama');
			$nm_dok_lama 		= $this->input->post('nm_dok_lama');
			$parameter				= array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('MD_SURAT_KELUAR2', 'ID_SURAT_KELUAR', $id_surat_keluar));
			$cek_status 			= $this->mdl_tnde->get_api('tnde_general/get_data', 'json', 'POST', $parameter);
			if($cek_status['KD_STATUS_SIMPAN'] != 3){
				if($cek_dok['valid'] == TRUE){
					$wh['ID_DOKUMEN_SK']	= $id_dok_lama;
					$parameter		= array('api_kode' => 1002, 'api_subkode' => 1, 'api_search' => array('MD_DOKUMEN_SK2', $wh));
					$status_dok	= $this->mdl_tnde->get_api('tnde_general/get_data', 'json', 'POST', $parameter);
					if(!empty($status_dok[0])){
						$tipe_data 					= $status_dok[0]['TIPE_DATA'];
						$file['FILE_BLOB'] 		= base64_encode(file_get_contents($dok['tmp_name']));
						$wb['ID_DOKUMEN_SK']	= $id_dok_lama;
						$parameter	= array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('BLOB_DOKUMEN_SK', $file, $wb));
						$upload 		= $this->mdl_tnde->get_api('tnde_general/insert_blob', 'json', 'POST', $parameter);
						if($upload == TRUE){
							if($tipe_data == 'FILE'){
								$dir = "dokumen_sk2/";
								if(file_exists($dir.$nm_dok_lama)){
									$act = @unlink($dir.$nm_dok_lama);
									if($act == FALSE){
										$data['errors'][] = "Gagal memperbarui dokumen #1";
									}
								}
							}
							if(empty($data['errors'])){
								$db['NM_DOKUMEN_SK']	= str_replace("_","", str_replace(" ","", $dok['name']));
								$db['TIPE_FILE']					= $dok['type'];
								$db['TIPE_DATA']				= 'BLOB';
								$ww['ID_DOKUMEN_SK']	= $id_dok_lama;
								$parameter		= array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('MD_DOKUMEN_SK2', $db, $ww));
								$update_meta	= $this->mdl_tnde->get_api('tnde_general/update_data', 'json', 'POST', $parameter);
								if($update_meta == FALSE){
									$data['errors'][] = "Gagal memperbarui data dokumen. #2";
								}
							}
							if(empty($data['errors'])){
								$data['success'][] = "Berhasil memperbarui dokumen";
							}
						}else{
							$data['errors'][] = "Gagal memperbarui dokumen #0";
						}
					}else{
						$data['errors'][] = "Dokumen tidak ditemukan";
					}
				}else{
					$data['errors'] = $cek_dok['errors'];
				}
			}else{
				$data['errors'][] = "Gagal memperbarui dokumen. Surat yang telah final tidak dapat di perbarui.";
			}
			if(!empty($data['errors'])){
				$this->session->set_flashdata('errors', $data['errors']);
			}
			if(!empty($data['success'])){
				$this->session->set_flashdata('success', $data['success']);
			}
			redirect('pegawai/surat_keluar/detail/'.$id_surat_keluar);
		}else{
			redirect();
		}
	}
	
	function del_doc(){
		$referer = (!empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '');
		$auth = host_allowed($referer);
		if((!empty($_POST['del_dok_sk'])) && ($auth)){
			$id_surat_keluar		= $this->input->post('id_surat_keluar');
			$id_dokumen_sk	= $this->input->post('id_dokumen');
			$parameter		= array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('MD_SURAT_KELUAR2','ID_SURAT_KELUAR', $id_surat_keluar));
			$cek_status 	= $this->mdl_tnde->get_api('tnde_general/get_data', 'json', 'POST', $parameter);
			if($cek_status['KD_STATUS_SIMPAN'] != 3){
					$wh['ID_DOKUMEN_SK']	= $id_dokumen_sk;
					$parameter	= array('api_kode' => 1002, 'api_subkode' => 1, 'api_search' => array('MD_DOKUMEN_SK2', $wh));
					$tipe_dok	= $this->mdl_tnde->get_api('tnde_general/get_data', 'json', 'POST', $parameter);
					if(!empty($tipe_dok)){
						if($tipe_dok[0]['TIPE_DATA'] == 'FILE'){
							$dir = "dokumen_sk2/";
							if(file_exists($dir.$tipe_dok[0]['NM_DOKUMEN_SK'])){
								$act = @unlink($dir.$tipe_dok[0]['NM_DOKUMEN_SK']);
								if($act == FALSE){
									$data['errors'][] = "Gagal menghapus dokumen #0";
								}
							}
						}
						if(empty($data['errors'])){
							$wd['ID_DOKUMEN_SK'] = $id_dokumen_sk;
							$parameter	= array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('MD_DOKUMEN_SK2', $wd));
							$del_dok 	= $this->mdl_tnde->get_api('tnde_general/delete_data', 'json', 'POST', $parameter);
							if($del_dok){
								$data['success'][] = "Berhasil menghapus dokumen";
							}else{
								$data['errors'][] = "Gagal menghapus dokumen #1";
							}
						}
					}else{
						$data['errors'][] = "Dokumen tidak ditemukan";
					}
			}else{
				$data['errors'][] = "Gagal menghapus dokumen. Surat yang telah final, tidak dapat diperbaharui lagi.";
			}
			if(!empty($data['errors'])){
				$this->session->set_flashdata('errors', $data['errors']);
			}
			if(!empty($data['success'])){
				$this->session->set_flashdata('success', $data['success']);
			}
			redirect('pegawai/surat_keluar/detail/'.$id_surat_keluar);
		}else{
			redirect();
		}
	}
	
	function del_sk(){
		$referer = (!empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '');
		$auth = host_allowed($referer);
		if(($this->input->post('del-sk') != null) && ($auth)){
			$jab_sistem 	= $this->session->userdata('jabatan_sistem');
			$get_mod	 	= $this->sisurat_general->jabatan_sistem_all($this->session->userdata('kd_pgw'));
			$data['detail_modules'] = $get_mod;
			$data['modules'] = array_keys($get_mod);

			if(!empty($get_mod)){
				$mod 				= $this->sisurat_surat_keluar->unit_surat_keluar($get_mod);
				$unit_list 		= (!empty($mod['UNIT_ID']) ? $mod['UNIT_ID'] : array());
				$jenis_surat 	= (!empty($mod['KD_JENIS_SURAT']) ? $mod['KD_JENIS_SURAT'] : array());
			}else{
				$mod				= array();
				$unit_list 			= array();
				$jenis_surat 	= array();
			}
			
			$id_surat_keluar	=	$this->input->post('id_surat_keluar');
			$parameter			= array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('MD_SURAT_KELUAR2', 'ID_SURAT_KELUAR', $id_surat_keluar));
			$cek_sk					= $this->mdl_tnde->get_api('tnde_general/get_data', 'json', 'POST', $parameter);
			if(ISSET($cek_sk['ID_SURAT_KELUAR'])){
				if((in_array($cek_sk['UNIT_ID'], $unit_list)) && (in_array($cek_sk['KD_JENIS_SURAT'], $jenis_surat))){
					$act_delete	= FALSE;
					if($cek_sk['KD_STATUS_SIMPAN'] == 1){
						
							$parameter	=	array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('MD_DOKUMEN_SK2', 'ID_SURAT_KELUAR', $id_surat_keluar));
							$dok				=	$this->mdl_tnde->get_api('tnde_general/get_data', 'json', 'POST', $parameter);
							if(count($dok) > 0){
								$dir	=	"dokumen_sk2/";
								foreach($dok as $val){
									switch($val['TIPE_DATA']){
										case 'FILE':
											if( @unlink($dir.$val['NM_DOKUMEN_SK']) ){
												$parameter	= array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('MD_DOKUMEN_SK2', 'ID_DOKUMEN_SK', $val['ID_DOKUMEN_SK']));
												$del_dok 	= $this->mdl_tnde->get_api('tnde_general/delete_by_field_id', 'json', 'POST', $parameter);
												if($del_dok == FALSE){
													$data['errors'][] = "Gagal menghapus data dokumen <b>".$val['NM_DOKUMEN_SK']."</b>";
												}
											}else{
												$data['errors'][] = "Gagal menghapus file dokumen <b>".$val['NM_DOKUMEN_SK']."</b>";
											}
											break;
										case 'BLOB':
											$parameter	= array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('MD_DOKUMEN_SK2', 'ID_DOKUMEN_SK', $val['ID_DOKUMEN_SK']));
											$del_dok 	= $this->mdl_tnde->get_api('tnde_general/delete_by_field_id', 'json', 'POST', $parameter);
											if($del_dok == FALSE){
												$data['errors'][] = "Gagal menghapus data dokumen <b>".$val['NM_DOKUMEN_SK']."</b>";
											}
											break;
									}
								}
							}
							
							$parameter	=	array('api_kode' => 1001, 'api_subkode' => 3, 'api_search' => array('MD_LAMPIRAN_SK2', 'ID_SURAT_KELUAR', $id_surat_keluar));
							$lamp				=	$this->mdl_tnde->get_api('tnde_general/get_data', 'json', 'POST', $parameter);
							if(count($lamp) > 0){
								$dir	=	"lampiran_sk2/";
								foreach($lamp as $val){
									switch($val['TIPE_DATA']){
										case 'FILE':
											if( @unlink($dir.$val['NM_LAMPIRAN_SK']) ){
												$parameter	= array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('MD_LAMPIRAN_SK2', 'ID_LAMPIRAN_SK', $val['ID_LAMPIRAN_SK']));
												$del_lamp 	= $this->mdl_tnde->get_api('tnde_general/delete_by_field_id', 'json', 'POST', $parameter);
												if($del_lamp == FALSE){
													$data['errors'][] = "Gagal menghapus data lampiran <b>".$val['NM_LAMPIRAN_SK']."</b>";
												}
											}else{
												$data['errors'][] = "Gagal menghapus file lampiran <b>".$val['NM_LAMPIRAN_SK']."</b>";
											}
											break;
										case 'BLOB':
											$parameter	= array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('MD_LAMPIRAN_SK2', 'ID_LAMPIRAN_SK', $val['ID_LAMPIRAN_SK']));
											$del_lamp 	= $this->mdl_tnde->get_api('tnde_general/delete_by_field_id', 'json', 'POST', $parameter);
											if($del_lamp == FALSE){
												$data['errors'][] = "Gagal menghapus data lampiran <b>".$val['NM_LAMPIRAN_SK']."</b>";
											}
											break;
									}
								}
							}
							$parameter	=	array('api_kode' => 1001, 'api_subkode' => 3, 'api_search' => array('MD_SCAN_SK2', 'ID_SURAT_KELUAR', $id_surat_keluar));
							$scan				=	$this->mdl_tnde->get_api('tnde_general/get_data', 'json', 'POST', $parameter);
							if(count($scan) > 0){
								$dir	=	"scan_sk2/";
								foreach($scan as $val){
									switch($val['TIPE_DATA']){
										case 'FILE':
											if( @unlink($dir.$val['NM_SCAN_SK']) ){
												$parameter	= array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('MD_SCAN_SK2', 'ID_SCAN_SK', $val['ID_SCAN_SK']));
												$del_lamp 	= $this->mdl_tnde->get_api('tnde_general/delete_by_field_id', 'json', 'POST', $parameter);
												if($del_lamp == FALSE){
													$data['errors'][] = "Gagal menghapus data scan surat <b>".$val['NM_SCAN_SK']."</b>";
												}
											}else{
												$data['errors'][] = "Gagal menghapus file scan surat <b>".$val['NM_SCAN_SK']."</b>";
											}
											break;
										case 'BLOB':
											$parameter	= array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('MD_SCAN_SK2', 'ID_SCAN_SK', $val['ID_SCAN_SK']));
											$del_lamp 	= $this->mdl_tnde->get_api('tnde_general/delete_by_field_id', 'json', 'POST', $parameter);
											if($del_lamp == FALSE){
												$data['errors'][] = "Gagal menghapus data scan surat <b>".$val['NM_SCAN_SK']."</b>";
											}
											break;
									}
								}
							}
							$parameter				= array('api_kode' => 1001, 'api_subkode' => 3, 'api_search' => array('D_PENERIMA_SK', 'ID_SURAT_KELUAR', $id_surat_keluar));
							$cek_penerima_sk	=	$this->mdl_tnde->get_api('tnde_general/get_data', 'json', 'POST', $parameter);
							if(count($cek_penerima_sk) > 0){
								$del_ps['ID_SURAT_KELUAR']	=	$id_surat_keluar;
								$parameter				= array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('D_PENERIMA_SK', $del_ps));
								$del_penerima_sk	=	$this->mdl_tnde->get_api('tnde_general/delete_data', 'json', 'POST', $parameter);
								if($del_penerima_sk == FALSE){
									$data['errors'][]	= "Gagal membatalkan pengiriman surat.";
									if($cek_sk['KD_JENIS_SURAT'] == 3){
										$data['errors'][] = "Surat undangan ini tidak dapat dihapus dikarenakan data surat undangan digunakan dalam notulen rapat. Jika tetap ingin menghapus surat undangan ini, silahkan hapus notulen rapat terlebih dahulu.";
									}
								}
							}
							$parameter				= array('api_kode' => 1001, 'api_subkode' => 3, 'api_search' => array('D_DETAIL_PERTIMBANGAN', 'ID_SURAT_KELUAR', $id_surat_keluar));
							$cek_d_pertimbangan=	$this->mdl_tnde->get_api('tnde_general/get_data', 'json', 'POST', $parameter);
							if(count($cek_d_pertimbangan) > 0){
								$del_p['ID_SURAT_KELUAR']	=	$id_surat_keluar;
								$parameter				= array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('D_DETAIL_PERTIMBANGAN', $del_p));
								$del_d_pertimbangan	=	$this->mdl_tnde->get_api('tnde_general/delete_data', 'json', 'POST', $parameter);
								if($del_d_pertimbangan == FALSE){
									$data['errors'][]	= "Gagal menghapus detail detail pertimbangan surat.";
								}
							}
							
							if(ISSET($data['errors'])){
								$act_delete	=	FALSE;
							}else{
								$act_delete	= TRUE;
							}
						
						if($act_delete == TRUE){
							switch($cek_sk['KD_JENIS_SURAT']){
								case 2:
									$parameter		= array('api_kode' => 1001, 'api_subkode' => 3, 'api_search' => array('D_DETAIL_STUGAS', 'ID_SURAT_KELUAR', $id_surat_keluar));
									$cek_stugas		=	$this->mdl_tnde->get_api('tnde_general/get_data', 'json', 'POST', $parameter);
									if(count($cek_stugas) > 0){
										$del_stugas['ID_SURAT_KELUAR']	= $id_surat_keluar;
										$parameter		= array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('D_DETAIL_STUGAS', $del_stugas));
										$act_delete		= 	$this->mdl_tnde->get_api('tnde_general/delete_data', 'json', 'POST', $parameter);
										if($act_delete == FALSE){
											$data['errors'][]	= "Gagal menghapus data detail surat tugas.";
										}
									}else{
										$act_delete	=	TRUE;
									}
									
									break;
								case 3:
									$parameter		= array('api_kode' => 1001, 'api_subkode' => 3, 'api_search' => array('D_AGENDA_UNDANGAN', 'ID_SURAT_KELUAR', $id_surat_keluar));
									$cek_agenda	=	$this->mdl_tnde->get_api('tnde_general/get_data', 'json', 'POST', $parameter);
									if(count($cek_agenda) > 0){
										$del_agenda['ID_SURAT_KELUAR']	= $id_surat_keluar;
										$parameter		= array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('D_AGENDA_UNDANGAN', $del_agenda));
										$act_delete		= 	$this->mdl_tnde->get_api('tnde_general/delete_data', 'json', 'POST', $parameter);
										if($act_delete == FALSE){
											$data['errors'][] = "Gagal menghapus data agenda undangan..";
										}
									}else{
										$act_delete	=	TRUE;
									}
									if($act_delete == TRUE){
										$parameter			= array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('D_DETAIL_SUNDANGAN', 'ID_SURAT_KELUAR', $id_surat_keluar));
										$detail_undangan	=	$this->mdl_tnde->get_api('tnde_general/get_data', 'json', 'POST', $parameter);
										if(ISSET($detail_undangan['KD_PEMINJAMAN'])){
											$parameter		= array('api_kode' => 1066, 'api_subkode' => 1, 'api_search' => array($detail_undangan['KD_PEMINJAMAN']));
											$act_delete		= $this->mdl_tnde->api_simar('simar_general/delete_peminjaman', 'json', 'POST', $parameter);
											if($act_delete == FALSE){
												$data['errors'][] = "Gagal menghapus surat dikarenakan gagal membatalkan peminjaman ruang.";
											}
										}else{
											$act_delete		= TRUE;
										}
									}
									if($act_delete == TRUE){
										$del_detail['ID_SURAT_KELUAR']	= $id_surat_keluar;
										$parameter		= array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('D_DETAIL_SUNDANGAN', $del_detail));
										$act_delete		= 	$this->mdl_tnde->get_api('tnde_general/delete_data', 'json', 'POST', $parameter);
										if($act_delete == FALSE){
											$data['errors'][] = "Gagal menghapus data detail surat undangan.";
										}else{
											$act_delete = TRUE;
										}
									}
									
									break;
								case 5:
									$parameter				= array('api_kode' => 1001, 'api_subkode' => 3, 'api_search' => array('D_DETAIL_INSTRUKSI', 'ID_SURAT_KELUAR', $id_surat_keluar));
									$cek_instruksi			=	$this->mdl_tnde->get_api('tnde_general/get_data', 'json', 'POST', $parameter);
									if(count($cek_instruksi) > 0){
										$del_instruksi['ID_SURAT_KELUAR']	= $id_surat_keluar;
										$parameter		= array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('D_DETAIL_INSTRUKSI', $del_instruksi));
										$act_delete		= 	$this->mdl_tnde->get_api('tnde_general/delete_data', 'json', 'POST', $parameter);
										if($act_delete == FALSE){
											$data['errors'][] = "Gagal menghapus data detail surat instruksi.";
										}
									}else{
										$act_delete	=	TRUE;
									}
									break;
								case 21:
									$parameter				= array('api_kode' => 1001, 'api_subkode' => 3, 'api_search' => array('D_DETAIL_SPENGANTAR', 'ID_SURAT_KELUAR', $id_surat_keluar));
									$cek_spengantar		=	$this->mdl_tnde->get_api('tnde_general/get_data', 'json', 'POST', $parameter);
									if(count($cek_spengantar) > 0){
										$del_spengantar['ID_SURAT_KELUAR']	= $id_surat_keluar;
										$parameter		= array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('D_DETAIL_SPENGANTAR', $del_spengantar));
										$act_delete		= 	$this->mdl_tnde->get_api('tnde_general/delete_data', 'json', 'POST', $parameter);
										if($act_delete == FALSE){
											$data['errors'][] = "Gagal menghapus data detail surat pengantar.";
										}
									}else{
										$act_delete	=	TRUE;
									}
									break;
							}
						}
					}else{
						$srt_tanpa_no = array(17, 18); // jenis surat laporan, telaah staf
						if(in_array($cek_sk['KD_JENIS_SURAT'], $srt_tanpa_no)){
							$act_delete				= TRUE;
							$parameter				= array('api_kode' => 1001, 'api_subkode' => 3, 'api_search' => array('D_PENERIMA_SK', 'ID_SURAT_KELUAR', $id_surat_keluar));
							$cek_penerima_sk	=	$this->mdl_tnde->get_api('tnde_general/get_data', 'json', 'POST', $parameter);
							if(count($cek_penerima_sk) > 0){
								foreach($cek_penerima_sk as $val){
									if($val['ID_STATUS_SM'] > 1){
										$act_delete	= FALSE;
									}
								}
								if($act_delete == TRUE){
									$del_ps['ID_SURAT_KELUAR']	=	$id_surat_keluar;
									$parameter				= array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('D_PENERIMA_SK', $del_ps));
									$del_penerima_sk	=	$this->mdl_tnde->get_api('tnde_general/delete_data', 'json', 'POST', $parameter);
									if($del_penerima_sk == FALSE){
										$data['errors'][]	= "Gagal membatalkan pengiriman surat. 0";
										$act_delete	=	FALSE;
									}
								}else{
									$data['errors'][] = "Surat yang telah dibaca pegawai terkait, tidak dapat dihapus.";
								}
							}
						}else{
							$data['errors'][] = "Surat yang telah memiliki nomor, tidak dapat dihapus.";
						}
					}
					if($act_delete == TRUE){
						$d_surat['ID_SURAT_KELUAR']	=	$id_surat_keluar;
						$parameter	= array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('MD_SURAT_KELUAR2', $d_surat));
						$del_surat		=	$this->mdl_tnde->get_api('tnde_general/delete_data', 'json', 'POST', $parameter);
						if($del_surat == TRUE){
							$data['success'][]	=	"Berhasil menghapus surat.";
						}else{
							$data['errors'][]	= "Gagal menghapus surat.";
						}
					}
				}else{
					$data['errors'][] = "Akses Ditolak";
				}
			}else{
				$data['errors'][]	= "Data tidak ditemukan";
			}
			if(!empty($data['errors'])){
				$this->session->set_flashdata('errors', $data['errors']);
			}
			if(!empty($data['success'])){
				$this->session->set_flashdata('success', $data['success']);
			}
			redirect('pegawai/surat_keluar');
		}else{
			redirect();
		}
	}
	
	function _insert_lamp($meta){
		$hasil = array();
		$hasil['valid'] = FALSE;
		if(!empty($meta)){
			#INSERT META
			$mm['ID_SURAT_KELUAR']	= $meta['id_surat_keluar'];
			$mm['NM_LAMPIRAN_SK']	= $meta['name'];
			$mm['TIPE_FILE']					= $meta['type'];
			$mm['TIPE_DATA']				= 'BLOB';
			$parameter		= array('api_kode' => 90002, 'api_subkode' => 1, 'api_search' => array($mm));
			$insert_meta	= $this->mdl_tnde->get_api('tnde_surat_keluar/insert_dokumen', 'json', 'POST', $parameter);
			if(!empty($insert_meta[0]['ID'])){
				#INSERT BLOG
				$file['FILE_BLOB'] 				= base64_encode(file_get_contents($meta['tmp_name']));
				$wh['ID_LAMPIRAN_SK']	= $insert_meta[0]['ID'];
				$parameter	= array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('BLOB_LAMPIRAN_SK', $file, $wh));
				$upload 		= $this->mdl_tnde->get_api('tnde_general/insert_blob', 'json', 'POST', $parameter);
				if($upload){
					$hasil['valid'] = TRUE;
				}else{
					$hasil['errors'] = 'Gagal menyimpan lampiran '.$mm['NM_LAMPIRAN_SK'].' #1';
				}
			}else{
				$hasil['errors'] = 'Gagal menyimpan lampiran '.$mm['NM_LAMPIRAN_SK'].' #0';
			}
		}
		return $hasil;
	}
	
	function add_lamp(){
		$referer = (!empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '');
		$auth = host_allowed($referer);
		if((!empty($_POST['add_lamp'])) && ($auth)){
			$id_surat_keluar 	= $this->input->post('id_surat_keluar');
			$lampiran 				= $_FILES['lamp'];
			$cek_file			 		= cek_single_file($lampiran);
			$parameter		= array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('MD_SURAT_KELUAR2', 'ID_SURAT_KELUAR', $id_surat_keluar));
			$cek_status 	= $this->mdl_tnde->get_api('tnde_general/get_data', 'json', 'POST', $parameter);
			if($cek_status['KD_STATUS_SIMPAN'] != 3){
				if($cek_file['valid'] == TRUE){
					$meta['tmp_name'] 			= $lampiran['tmp_name'];
					$meta['name'] 					= str_replace("_","", str_replace(" ","", $lampiran['name']));
					$meta['type']	 					= $lampiran['type'];
					$meta['id_surat_keluar']		= $id_surat_keluar;
					$insert_file		= $this->_insert_lamp($meta);
					if(!empty($insert_file['errors'])){
						$warnings[] = $insert_file['errors'];
					}
					if(!empty($warnings)){
						$this->session->set_flashdata('errors', $warnings);
					}else{
						$this->session->set_flashdata('success', 'Berhasil menambahkan lampiran');
					}
					redirect('pegawai/surat_keluar/detail/'.$id_surat_keluar);
				}else{
					$this->session->set_flashdata('errors', $cek_file['errors']);
					redirect('pegawai/surat_keluar/detail/'.$id_surat_keluar);
				}
			}else{
				$this->session->set_flashdata('errors', 'Gagal memperbarui data. Surat yang telah final tidak dapat diperbarui.');
				redirect('pegawai/surat_keluar/detail/'.$id_surat_keluar);
			}
		}else{
			redirect();
		}
	}
	
	function update_lamp(){
		$referer = (!empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '');
		$auth = host_allowed($referer);
		if(($this->input->post('update_lamp') != null) && ($auth)){
			$id_surat_keluar		=	$this->input->post('id_surat_keluar');
			$lamp 						= $_FILES['up_lamp'];
			$cek_lamp 				= cek_single_file($lamp);
			$id_lamp_lama 		= $this->input->post('id_lamp_lama');
			$nm_lamp_lama 		= $this->input->post('nm_lamp_lama');
			$parameter				= array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('MD_SURAT_KELUAR2', 'ID_SURAT_KELUAR', $id_surat_keluar));
			$cek_status 			= $this->mdl_tnde->get_api('tnde_general/get_data', 'json', 'POST', $parameter);
			if($cek_status['KD_STATUS_SIMPAN'] != 3){
				if($cek_lamp['valid'] == TRUE){
					$wh['ID_LAMPIRAN_SK']	= $id_lamp_lama;
					$parameter		= array('api_kode' => 1002, 'api_subkode' => 1, 'api_search' => array('MD_LAMPIRAN_SK2', $wh));
					$status_lamp	= $this->mdl_tnde->get_api('tnde_general/get_data', 'json', 'POST', $parameter);
					if(!empty($status_lamp[0])){
						$tipe_data 					= $status_lamp[0]['TIPE_DATA'];
						$file['FILE_BLOB'] 		= base64_encode(file_get_contents($lamp['tmp_name']));
						$wb['ID_LAMPIRAN_SK']	= $id_lamp_lama;
						$parameter	= array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('BLOB_LAMPIRAN_SK', $file, $wb));
						$upload 		= $this->mdl_tnde->get_api('tnde_general/insert_blob', 'json', 'POST', $parameter);
						if($upload == TRUE){
							if($tipe_data == 'FILE'){
								$dir = "lampiran_sk2/";
								if(file_exists($dir.$nm_lamp_lama)){
									$act = @unlink($dir.$nm_lamp_lama);
									if($act == FALSE){
										$data['errors'][] = "Gagal memperbarui lampiran #1";
									}
								}
							}
							if(empty($data['errors'])){
								$db['NM_LAMPIRAN_SK']	= str_replace("_","", str_replace(" ","", $lamp['name']));
								$db['TIPE_FILE']					= $lamp['type'];
								$db['TIPE_DATA']				= 'BLOB';
								$ww['ID_LAMPIRAN_SK']	= $id_lamp_lama;
								$parameter		= array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('MD_LAMPIRAN_SK2', $db, $ww));
								$update_meta	= $this->mdl_tnde->get_api('tnde_general/update_data', 'json', 'POST', $parameter);
								if($update_meta == FALSE){
									$data['errors'][] = "Gagal memperbarui data lampiran. #2";
								}
							}
							if(empty($data['errors'])){
								$data['success'][] = "Berhasil memperbarui lampiran";
							}
						}else{
							$data['errors'][] = "Gagal memperbarui lampiran #0";
						}
					}else{
						$data['errors'][] = "Lampiran tidak ditemukan";
					}
				}else{
					$data['errors'] = $cek_lamp['errors'];
				}
			}else{
				$data['errors'][] = "Gagal memperbarui lampiran. Surat yang telah final tidak dapat di perbarui.";
			}
			if(!empty($data['errors'])){
				$this->session->set_flashdata('errors', $data['errors']);
			}
			if(!empty($data['success'])){
				$this->session->set_flashdata('success', $data['success']);
			}
			redirect('pegawai/surat_keluar/detail/'.$id_surat_keluar);
		}else{
			redirect();
		}
	}
	
	function del_lamp(){
		$referer = (!empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '');
		$auth = host_allowed($referer);
		if((!empty($_POST['del_lamp_sk'])) && ($auth)){
			$id_surat_keluar		= $this->input->post('id_surat_keluar');
			$id_lampiran_sk		= $this->input->post('id_lampiran');
			$parameter		= array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('MD_SURAT_KELUAR2','ID_SURAT_KELUAR', $id_surat_keluar));
			$cek_status 	= $this->mdl_tnde->get_api('tnde_general/get_data', 'json', 'POST', $parameter);
			if($cek_status['KD_STATUS_SIMPAN'] != 3){
					$wh['ID_LAMPIRAN_SK']	= $id_lampiran_sk;
					$parameter	= array('api_kode' => 1002, 'api_subkode' => 1, 'api_search' => array('MD_LAMPIRAN_SK2', $wh));
					$tipe_lamp	= $this->mdl_tnde->get_api('tnde_general/get_data', 'json', 'POST', $parameter);
					if(!empty($tipe_lamp)){
						if($tipe_lamp[0]['TIPE_DATA'] == 'FILE'){
							$dir = "lampiran_sk2/";
							if(file_exists($dir.$tipe_lamp[0]['NM_LAMPIRAN_SK'])){
								$act = @unlink($dir.$tipe_lamp[0]['NM_LAMPIRAN_SK']);
								if($act == FALSE){
									$data['errors'][] = "Gagal menghapus lampiran #0";
								}
							}
						}
						if(empty($data['errors'])){
							$wd['ID_LAMPIRAN_SK'] = $id_lampiran_sk;
							$parameter	= array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('MD_LAMPIRAN_SK2', $wd));
							$del_lamp 	= $this->mdl_tnde->get_api('tnde_general/delete_data', 'json', 'POST', $parameter);
							if($del_lamp){
								$data['success'][] = "Berhasil menghapus lampiran";
							}else{
								$data['errors'][] = "Gagal menghapus lampiran #1";
							}
						}
					}else{
						$data['errors'][] = "Lampiran tidak ditemukan";
					}
			}else{
				$data['errors'][] = "Gagal menghapus lampiran. Surat yang telah final, tidak dapat diperbaharui lagi.";
			}
			if(!empty($data['errors'])){
				$this->session->set_flashdata('errors', $data['errors']);
			}
			if(!empty($data['success'])){
				$this->session->set_flashdata('success', $data['success']);
			}
			redirect('pegawai/surat_keluar/detail/'.$id_surat_keluar);
		}else{
			redirect();
		}
	}
	
	function _insert_scan($meta){
		$hasil = array();
		$hasil['valid'] = FALSE;
		if(!empty($meta)){
			#INSERT META
			$mm['ID_SURAT_KELUAR']	= $meta['id_surat_keluar'];
			$mm['NM_SCAN_SK']			= $meta['name'];
			$mm['ISI_SCAN_SK']				= $meta['isi_dokumen'];
			$mm['TIPE_FILE']					= $meta['type'];
			$mm['TIPE_DATA']				= 'BLOB';
			$parameter		= array('api_kode' => 90003, 'api_subkode' => 1, 'api_search' => array($mm));
			$insert_meta	= $this->mdl_tnde->get_api('tnde_surat_keluar/insert_dokumen', 'json', 'POST', $parameter);
			if(!empty($insert_meta[0]['ID'])){
				#INSERT BLOG
				$file['FILE_BLOB'] 				= base64_encode(file_get_contents($meta['tmp_name']));
				$wh['ID_SCAN_SK']			= $insert_meta[0]['ID'];
				$parameter	= array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('BLOB_SCAN_SK', $file, $wh));
				$upload 		= $this->mdl_tnde->get_api('tnde_general/insert_blob', 'json', 'POST', $parameter);
				if($upload){
					$hasil['valid'] = TRUE;
				}else{
					$hasil['errors'] = 'Gagal menyimpan scan surat '.$mm['NM_SCAN_SK'].' #1';
				}
			}else{
				$hasil['errors'] = 'Gagal menyimpan scan surat '.$mm['NM_SCAN_SK'].' #0';
			}
		}
		return $hasil;
	}
	
	function add_scan(){
		$referer = (!empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '');
		$auth = host_allowed($referer);
		if((!empty($_POST['add_ss'])) && ($auth)){
			$id_surat_keluar 	= $this->input->post('id_surat_keluar');
			$scan 						= $_FILES['scan'];
			$cek_file			 		= cek_pdf($scan);
			$parameter		= array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('MD_SURAT_KELUAR2', 'ID_SURAT_KELUAR', $id_surat_keluar));
			$cek_status 	= $this->mdl_tnde->get_api('tnde_general/get_data', 'json', 'POST', $parameter);
			if($cek_status['KD_STATUS_SIMPAN'] != 3){
				if($cek_file['valid'] == TRUE){
					$meta['tmp_name'] 			= $scan['tmp_name'];
					$meta['name'] 					= str_replace("_","", str_replace(" ","", $scan['name']));
					$meta['type']	 					= $scan['type'];
					$meta['isi_dokumen']			= '0';
					$meta['id_surat_keluar']		= $id_surat_keluar;
					$insert_file		= $this->_insert_scan($meta);
					if(!empty($insert_file['errors'])){
						$warnings[] = $insert_file['errors'];
					}
					if(!empty($warnings)){
						$this->session->set_flashdata('errors', $warnings);
					}else{
						$this->session->set_flashdata('success', 'Berhasil menambahkan scan surat');
					}
					redirect('pegawai/surat_keluar/detail/'.$id_surat_keluar);
				}else{
					$this->session->set_flashdata('errors', $cek_file['errors']);
					redirect('pegawai/surat_keluar/detail/'.$id_surat_keluar);
				}
			}else{
				$this->session->set_flashdata('errors', 'Gagal memperbarui data. Surat yang telah final tidak dapat diperbarui.');
				redirect('pegawai/surat_keluar/detail/'.$id_surat_keluar);
			}
		}else{
			redirect();
		}
	}
	
	function update_scan(){
		$referer = (!empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '');
		$auth = host_allowed($referer);
		if(($this->input->post('update_scan') != null) && ($auth)){
			$id_surat_keluar		=	$this->input->post('id_surat_keluar');
			$scan 						= $_FILES['up_scan'];
			$cek_scan 				= cek_pdf($scan);
			$id_scan_lama 		= $this->input->post('id_scan_lama');
			$nm_scan_lama		= $this->input->post('nm_scan_lama');
			$parameter				= array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('MD_SURAT_KELUAR2', 'ID_SURAT_KELUAR', $id_surat_keluar));
			$cek_status 			= $this->mdl_tnde->get_api('tnde_general/get_data', 'json', 'POST', $parameter);
			if($cek_status['KD_STATUS_SIMPAN'] != 3){
				if($cek_scan['valid'] == TRUE){
					$wh['ID_SCAN_SK']	= $id_scan_lama;
					$parameter		= array('api_kode' => 1002, 'api_subkode' => 1, 'api_search' => array('MD_SCAN_SK2', $wh));
					$status_scan	= $this->mdl_tnde->get_api('tnde_general/get_data', 'json', 'POST', $parameter);
					if(!empty($status_scan[0])){
						$tipe_data 					= $status_scan[0]['TIPE_DATA'];
						$file['FILE_BLOB'] 		= base64_encode(file_get_contents($scan['tmp_name']));
						$wb['ID_SCAN_SK']	= $id_scan_lama;
						$parameter	= array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('BLOB_SCAN_SK', $file, $wb));
						$upload 		= $this->mdl_tnde->get_api('tnde_general/insert_blob', 'json', 'POST', $parameter);
						if($upload == TRUE){
							if($tipe_data == 'FILE'){
								$dir = "scan_sk2/";
								if(file_exists($dir.$nm_scan_lama)){
									$act = @unlink($dir.$nm_scan_lama);
									if($act == FALSE){
										$data['errors'][] = "Gagal memperbarui scan surat #1";
									}
								}
							}
							if(empty($data['errors'])){
								$db['NM_SCAN_SK']			= str_replace("_","", str_replace(" ","", $scan['name']));
								$db['TIPE_FILE']					= $scan['type'];
								$db['TIPE_DATA']				= 'BLOB';
								$ww['ID_SCAN_SK']			= $id_scan_lama;
								$parameter		= array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('MD_SCAN_SK2', $db, $ww));
								$update_meta	= $this->mdl_tnde->get_api('tnde_general/update_data', 'json', 'POST', $parameter);
								if($update_meta == FALSE){
									$data['errors'][] = "Gagal memperbarui data scan surat. #2";
								}
							}
							if(empty($data['errors'])){
								$data['success'][] = "Berhasil memperbarui scan surat";
							}
						}else{
							$data['errors'][] = "Gagal memperbarui scan surat #0";
						}
					}else{
						$data['errors'][] = "Scan surat tidak ditemukan";
					}
				}else{
					$data['errors'] = $cek_scan['errors'];
				}
			}else{
				$data['errors'][] = "Gagal memperbarui scan surat. Surat yang telah final tidak dapat di perbarui.";
			}
			if(!empty($data['errors'])){
				$this->session->set_flashdata('errors', $data['errors']);
			}
			if(!empty($data['success'])){
				$this->session->set_flashdata('success', $data['success']);
			}
			redirect('pegawai/surat_keluar/detail/'.$id_surat_keluar);
		}else{
			redirect();
		}
	}
	
	function del_scan(){
		$referer = (!empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '');
		$auth = host_allowed($referer);
		if((!empty($_POST['del_scan_sk'])) && ($auth)){
			$id_surat_keluar		= $this->input->post('id_surat_keluar');
			$id_scan_sk			= $this->input->post('id_scan');
			$parameter		= array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('MD_SURAT_KELUAR2','ID_SURAT_KELUAR', $id_surat_keluar));
			$cek_status 	= $this->mdl_tnde->get_api('tnde_general/get_data', 'json', 'POST', $parameter);
			if($cek_status['KD_STATUS_SIMPAN'] != 3){
					$wh['ID_SCAN_SK']	= $id_scan_sk;
					$parameter	= array('api_kode' => 1002, 'api_subkode' => 1, 'api_search' => array('MD_SCAN_SK2', $wh));
					$tipe_scan	= $this->mdl_tnde->get_api('tnde_general/get_data', 'json', 'POST', $parameter);
					if(!empty($tipe_scan)){
						if($tipe_scan[0]['TIPE_DATA'] == 'FILE'){
							$dir = "scan_sk2/";
							if(file_exists($dir.$tipe_scan[0]['NM_SCAN_SK'])){
								$act = @unlink($dir.$tipe_scan[0]['NM_SCAN_SK']);
								if($act == FALSE){
									$data['errors'][] = "Gagal menghapus scan surat #0";
								}
							}
						}
						if(empty($data['errors'])){
							$wd['ID_SCAN_SK'] = $id_scan_sk;
							$parameter	= array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('MD_SCAN_SK2', $wd));
							$del_lamp 	= $this->mdl_tnde->get_api('tnde_general/delete_data', 'json', 'POST', $parameter);
							if($del_lamp){
								$data['success'][] = "Berhasil menghapus scan surat";
							}else{
								$data['errors'][] = "Gagal menghapus scan surat #1";
							}
						}
					}else{
						$data['errors'][] = "Scan surat tidak ditemukan";
					}
			}else{
				$data['errors'][] = "Gagal menghapus scan surat. Surat yang telah final, tidak dapat diperbaharui lagi.";
			}
			if(!empty($data['errors'])){
				$this->session->set_flashdata('errors', $data['errors']);
			}
			if(!empty($data['success'])){
				$this->session->set_flashdata('success', $data['success']);
			}
			redirect('pegawai/surat_keluar/detail/'.$id_surat_keluar);
		}else{
			redirect();
		}
	}
	
	function set_final(){
		$referer = (!empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '');
		$auth = host_allowed($referer);
		if(($this->input->post('kesshou') != null) && ($auth)){ #set final
			$id_surat_keluar = $this->input->post('id_surat_keluar');
			$kd_keamanan_surat = $this->input->post('kd_keamanan_surat');
			$config_rule = array(
								array('field' => 'id_surat_keluar', 'label' => 'Id surat keluar', 'rules' => 'required'),
								array('field' => 'kd_keamanan_surat', 'label' => 'Tingkat Keamanan Surat', 'rules' => 'required')
							);
			$this->form_validation->set_rules($config_rule);
			if($this->form_validation->run() == TRUE){
				$parameter		= array('api_kode' => 90008, 'api_subkode' => 1, 'api_search' => array($id_surat_keluar));
				$penerima_sk	= $this->mdl_tnde->get_api('tnde_surat_keluar/get_penerima_sk', 'json', 'POST', $parameter);
				if(!empty($penerima_sk)){
					$kd_pgw = $this->session->userdata('log_kd_pgw');
					#$soushin = $this->sisurat_surat_masuk->insert_surat_masuk_from_sk_v2($id_surat_keluar, $kd_keamanan_surat, $kd_pgw);
					
					$par = array($id_surat_keluar, $kd_pgw, $kd_keamanan_surat);
					$kirim = $this->mdl_tnde->get_api('tnde_surat_masuk/insert_surat_masuk_from_sk', 'json', 'POST', array('api_kode' => 80002, 'api_subkode' => 1, 'api_search' => $par));
					if(!empty($kirim['ERRORS'])){
						$data['errors'] = $kirim['ERRORS'];
					}else{
						$data['success'][] = "Berhasil memperbarui status surat.";
					}
				}else{
					/*$wr['ID_SURAT_KELUAR'] = $id_surat_keluar;
					$surat['KD_STATUS_SIMPAN'] = 3;
					$parameter = array('api_kode' => 1001, 'api_subkode' => 1, 'api_search' => array('MD_SURAT_KELUAR2', $surat, $wr));
					$kesshou = $this->mdl_tnde->get_api('tnde_general/update_data', 'json', 'POST', $parameter);
					if($kesshou){
						$data['success'][] = "Berhasil memperbaharui surat keluar";
					}else{
						$data['errors'][] = "Gagal memperbaharui surat keluar";
					}*/
					$data['errors'][] = "Gagal memperbarui status final dikarenakan tujuan surat kosong.";
				}
			}else{
				$data['errors'][] = validation_errors();
			}
			if(!empty($data['errors'])){
				$this->session->set_flashdata('errors', $data['errors']);
			}
			if(!empty($data['success'])){
				$this->session->set_flashdata('success', $data['success']);
			}
			redirect($referer);
		}else{
			redirect();
		}
	}
	
	function agenda_peserta($id_surat_keluar=''){
		$referer = (!empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '');
		$auth = host_allowed($referer);
		if($auth){
			$header = '<table class="table table-bordered">
								<thead>
									<tr><td width="26px">No</td><td width="240px">Peserta</td><td>Agenda</td></tr>
								</thead>';
			$footer = '</table>';
			
			$tgl_mulai		= $this->input->post('tgl_mulai')." ".$this->input->post('jam_mulai').":00";
			$tgl_selesai		= $this->input->post('tgl_selesai')." ".$this->input->post('jam_selesai').":00";
			
			$pgw_list			= array();
			$str_pjb			= $this->input->post('kpd_pejabat');
			$arr_pjb			= explode("<$>", $str_pjb);
			$str_pgw			= $this->input->post('kpd_pegawai');
			$arr_pgw			= explode("<$>", $str_pgw);
			if(!empty($arr_pjb)){
				foreach($arr_pjb as $val){
					$exp = explode("#", $val);
					if(!empty($exp[0]) && ! in_array($exp[0], $pgw_list)){
						$pgw_list[] = $exp[0];
					}
				}
			}
			if(!empty($arr_pgw)){
				foreach($arr_pgw as $dd){
					$exp2 = explode("#", $dd);
					if(!empty($exp2[0]) && ! in_array($exp2[0], $pgw_list)){
						$pgw_list[] = $exp2[0];
					}
				}
			}
			
			$agenda	= $this->sisurat_external->agenda($pgw_list, $tgl_mulai, $tgl_selesai);
			if(!empty($agenda)){
				$n	= 1;
				foreach($agenda as $kd_pgw => $val){
					$parameter = array('api_kode' => 2001, 'api_subkode' => 2, 'api_search' => array($kd_pgw));
					$pgw 			= $this->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
					$nm_pgw	= (!empty($pgw[0]['NM_PGW_F']) ? $pgw[0]['NM_PGW_F'] : '');
					if(!empty($val['PERKULIAHAN'])){
						$arr_tr[] = '<tr><td>'.$n.'</td><td>'.$nm_pgw.'<hr/>'.$kd_pgw.'</td><td>'.$val['PERKULIAHAN'].'</td></tr>';
						$n++;
					}
					if(!empty($val['UNDANGAN'])){
						foreach($val['UNDANGAN'] as $dd){
							if($id_surat_keluar != $dd['ID_SURAT_KELUAR']){
								$arr_tr[] = '<tr><td>'.$n.'</td><td>'.$nm_pgw.'<hr/>'.$kd_pgw.'</td><td><b>'.$dd['ACARA'].'</b> pada tanggal '.$dd['TGL_MULAI'].' sampai dengan '.$dd['TGL_SELESAI'].'</td></tr>';
								$n++;
							}
						}
					}
					if(!empty($val['TA'])){
						$arr_tr[] = '<tr><td>'.$n.'</td><td>'.$nm_pgw.'<hr/>'.$kd_pgw.'</td><td><b>'.$val['TA']['ACARA'].'</b> pada tanggal '.$val['TA']['TGL_MUNA'].' jam '.$val['TA']['JAM_MULAI'].' sampai '.$val['TA']['JAM_SELESAI'].'</td></tr>';
						$n++;
					}
					if(!empty($val['TR_EN'])){
						foreach($val['TR_EN'] as $dd){
							$exp 	= explode("@", $dd);
							$acara	= (!empty($exp[0]) ? $exp[0] : '');
							$hari	= (!empty($exp[1]) ? $exp[1] : '');
							$pukul	= (!empty($exp[2]) ? $exp[2] : '');
							$periode	= (!empty($exp[3]) ? $exp[3] : '');
							$arr_tr[] = '<tr><td>'.$n.'</td><td>'.$nm_pgw.'<hr/>'.$kd_pgw.'</td><td><b>'.$acara.'</b> pada hari '.$hari.' pukul '.$pukul.' '.$periode.'</td></tr>';
							$n++;
						}
					}
					if(!empty($val['TR_AR'])){
						foreach($val['TR_AR'] as $dd){
							$exp 	= explode("@", $dd);
							$acara	= (!empty($exp[0]) ? $exp[0] : '');
							$hari	= (!empty($exp[1]) ? $exp[1] : '');
							$pukul	= (!empty($exp[2]) ? $exp[2] : '');
							$periode	= (!empty($exp[3]) ? $exp[3] : '');
							$arr_tr[] = '<tr><td>'.$n.'</td><td>'.$nm_pgw.'<hr/>'.$kd_pgw.'</td><td><b>'.$acara.'</b> pada hari '.$hari.' pukul '.$pukul.' '.$periode.'</td></tr>';
							$n++;
						}
					}
					if(!empty($val['TR_BI'])){
						foreach($val['TR_BI'] as $dd){
							$exp 	= explode("@", $dd);
							$acara	= (!empty($exp[0]) ? $exp[0] : '');
							$hari	= (!empty($exp[1]) ? $exp[1] : '');
							$pukul	= (!empty($exp[2]) ? $exp[2] : '');
							$periode	= (!empty($exp[3]) ? $exp[3] : '');
							$arr_tr[] = '<tr><td>'.$n.'</td><td>'.$nm_pgw.'<hr/>'.$kd_pgw.'</td><td><b>'.$acara.'</b> pada hari '.$hari.' pukul '.$pukul.' '.$periode.'</td></tr>';
							$n++;
						}
					}
					if(!empty($val['ICT'])){
						foreach($val['ICT'] as $dd){
							$exp 	= explode("@", $dd);
							$acara	= (!empty($exp[0]) ? $exp[0] : '');
							$hari	= (!empty($exp[1]) ? $exp[1] : '');
							$pukul	= (!empty($exp[2]) ? $exp[2] : '');
							$periode	= (!empty($exp[3]) ? $exp[3] : '');
							$arr_tr[] = '<tr><td>'.$n.'</td><td>'.$nm_pgw.'<hr/>'.$kd_pgw.'</td><td><b>'.$acara.'</b> pada hari '.$hari.' pukul '.$pukul.' '.$periode.'</td></tr>';
							$n++;
						}
					}
				}
			}
			
			if(!empty($arr_tr)){
				$tr = implode("", $arr_tr);
			}else{
				$tr = '<tr><td colspan="3"><center>TIDAK ADA AGENDA</center></td></tr>';
			}
			
			$isi			= '<tbody>'.$tr.'</tbody>';
			$hasil['success'] = $header.$isi.$footer;
			$hasil['tgl_awal'] 	= $agenda['TGL_AWAL'];
			$hasil['tgl_selesai'] 	= $agenda['TGL_SELESAI'];
			echo json_encode($hasil);
		}else{
			redirect('pegawai');
		}
	}
}
?>