<?php if(!defined('BASEPATH'))exit('No direct script access allowed');

class Surat_keluar_mahasiswa extends MY_Controller {
	
	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('log') !== 'in')
			redirect(site_url('pgw/login?from='.current_url()));
		
		$this->load->helper('form');
		$this->load->helper('Tnde_sakad');		
		$this->load->model('Automasi_model');
		$this->load->model('Verifikasi_model');
		$this->load->module('pgw/api/Api_foto','api_foto');

		$cek_sesi = $this->Automasi_model->sesi_by_kd_user($this->session->userdata('username'));

		//exit(var_dump($cek_sesi));
		if (!empty($cek_sesi)){
			$this->session->set_userdata('sesi_sakad',$cek_sesi);
		} else {
			$this->session->unset_userdata('sesi_sakad');
		}
    }

    public function index()
    {
    	$this->load->library('pagination');

    	###	SET HAK AKSES ###
    	if ($this->session->userdata('username') === '11650021'){
    		$modules = ['SKR','SPR','MTR'];
    		$data['modules'] = $modules;
    	}




		$unit_list 	= "'UN02006','UN02008'"; #SAINTEK DAN FEBI

		$list									= array(11);
		$jenis_surat_list			= implode(",", $list);
		$config['base_url'] 		= base_url()."pgw/Surat_keluar_mahasiswa/index";
		$config['per_page'] 		= 20;
		$config['uri_segment'] 	= 4;
		$status_simpan				= "0"; //$status_simpan				= "0,1,2,3";
		$parameter	= array('api_kode' => 90000,'api_subkode' => 1,'api_search' => array($unit_list, $jenis_surat_list, $status_simpan));
		$tot 				= $this->apiconn->api_tnde('tnde_surat_keluar/get_surat_keluar','json','POST', $parameter);
		
		$config['total_rows'] = $tot['TOTAL'];

		$this->pagination->initialize($config);
		$page = $this->uri->segment(4);
		if (empty($page)){$page = '1';}
		// if(empty($page)){
		// 	$subkode = 1;
		// }else{
		// 	$subkode = 2;
		// }
		$data['links'] = $this->pagination->create_links();
		$parameter					= array('api_kode' => 90006, 'api_subkode' => 2, 'api_search' => array($unit_list, $jenis_surat_list, $status_simpan, $config['per_page'], $page));
		$data['surat_keluar'] 	= $this->apiconn->api_tnde('tnde_surat_keluar/get_surat_keluar', 'json', 'POST', $parameter);

		$arr_unit = [
			array('id' => 'UN02006'),
			array('id' => 'UN02008')
		];
		$arr_unit2 = [];
		foreach($arr_unit as $key => $val){
			if(!empty($val['id'])){
				$parameter 	= array('api_kode' => 1901, 'api_subkode' => 4, 'api_search' => array(date('d/m/Y'), $val['id'])); 
				$unit_data 	= $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
				//$arr_unit[$key]['data'] = $unit_data;				
				$arr_unit2[$key] = $unit_data[0];
			}
		}

		## ASSIGN DATA UNIT ke ITEM-ITEM SURAT KELUAR
		// if(!empty($data['surat_keluar'])){
		// 	foreach($data['surat_keluar'] as $key => $val){
		// 		if(!empty($val['UNTUK_UNIT'])){
		// 			$key_cari = array_column($arr_unit2, 'UNIT_ID');
		// 			foreach($key_cari as $k=>$v){
		// 				if ($v === $val['UNTUK_UNIT']){
		// 					$selectedunit = $arr_unit2[$k];

		// 					$data['untuk_unit'][$key] = (!empty($selectedunit[0]) ? $selectedunit[0] : '');		
		// 				}
		// 			}

					
		// 		}
		// 	}
		// }
		
		// exit(var_dump($data['untuk_unit']));		
		
		$this->load->view('templates/v_header');
		$this->load->view('templates/v_sidebar', $data);
		$this->load->view('v_surat_keluar_mahasiswa', $data);
		$this->load->view('templates/v_footer');
    }

    public function buat($page='select_multistep')
	{
		$parameter =array('api_kode'		=> 13001, 'api_subkode'	=> 2, 'api_search'	=> array());
		$data['list_jenis_sakad'] = $this->apiconn->api_tnde_sakad('tnde_master_mix/get_jenis_surat_akademik', 'json', 'POST', $parameter); # DAFTAR JENIS SURAT AKADEMIK (AKTIF)
		$data['cek_sesi'] = $this->Automasi_model->sesi_by_kd_user($this->session->userdata('username'));

		//exit(print_r($data));

		$list_urlpath = array_column($data['list_jenis_sakad'], 'URLPATH'); #daftar url path
		$current_urlpath = $this->uri->segment(4);
		//$page = $current_urlpath;

		if ($page == 'select_multistep'){ # Versi Multistep, select option daftar surat
			$view = 'v_buat';
		} elseif (in_array($current_urlpath, $list_urlpath)) { # Versi direct link jenis surat by path
			#pilih array jenis sakad (berdasar path)
			$key_terpilih = array_search($current_urlpath, $list_urlpath);
			$data['jenis_sakad'] = $data['list_jenis_sakad'][$key_terpilih];

			$verified = array('masih_kul','ijin_pen', 'ijin_pen_makul', 'ijin_studi_pen', 'ijin_obs', 'perm_KP', 'hbs_teori', 'tdk_men_bea', 'ket_lulus','kel_baik', 'pndh_studi'); #jenis surat yg sudah bisa memverifikasi
			if (in_array($current_urlpath, $verified)){
		    	$param_jenis_sakad = $data['jenis_sakad'];
		    	$param_nim = $this->session->userdata('username');

		    	if (empty($data['cek_sesi'])){
					$crosscheck_syarat = $this->Verifikasi_model->cek_persyaratan($param_jenis_sakad, $param_nim);
			    	$data['hasil_verifikasi'] = $crosscheck_syarat;

			    	if ($data['hasil_verifikasi']['hasil_cek_mhs']['num_errors'] == 0){
			    		if (!empty($data['hasil_verifikasi']['tmp_verified_mhs'])){
			    			$this->session->set_userdata('tmp_verified_mhs',$data['hasil_verifikasi']['tmp_verified_mhs']);
			    		}

			    		if (!empty($data['hasil_verifikasi']['tmp_isi_detail'])){
			    			$this->session->set_userdata('tmp_verified_detail_skr',$data['hasil_verifikasi']['tmp_isi_detail']);
			    		}
			    		$v2_view = array('masih_kul');
			    		$v3_view = array('ijin_pen_makul');
			    		if (in_array($current_urlpath, $v2_view)){
			    			$view_form = 'v_buatsurat/v2/v_'.$current_urlpath;
			    		} elseif (in_array($current_urlpath, $v3_view)){
			    			$view_form = 'v_buatsurat/v3_alpha/v_'.$current_urlpath;
			    		} else {
			    			$view_form = 'v_buatsurat/v_'.$current_urlpath;
			    		}
			    		$data['_view_form'] = $this->load->view($view_form, $data, true);
					}
		    	}

				$view = 'v_buat_verified';
			} else {
				$data['resp_persyaratan'] = $this->action_verifikasi(
				array(
						'urlpath'	=> $current_urlpath,
						'nim'=> $this->session->userdata('username'),
						'log'=> $this->session->userdata('username')
					)
				);
				$view = 'v_daftar_syarat';
				$view_form = 'v_buatsurat/v_'.$current_urlpath;
				$data['_view_form'] = $this->load->view($view_form, $data, true);
			}
		} else {
			$view = 'v_buatsurat/v_ijin_studi_pen';
		}
		$this->load->view('templates/v_header');
		$this->load->view('templates/v_sidebar', $data);
		$this->load->view($view,$data);
		$this->load->view('templates/v_footer');
	}	

    public function detail($id_surat_keluar=null)
	{
		$data['title'] = 'Detail Surat';
		
		## 1. API GET SURAT KELUAR + PENERIMA
		$data['surat_keluar'] = $this->Automasi_model->get_surat_keluar($id_surat_keluar);	

		## 2. API GET DETAIL SURAT (AUTOMASI)
		$data['detail_surat_automasi'] = $this->Automasi_model->get_detail_skr_automasi($id_surat_keluar);


		## 3. API GET ke SIMPEG untuk mengambil data pelengkap
		#3.1 unit simpeg
		$parameter = array('api_kode' => 1901, 'api_subkode' => 2, 'api_search' => array($data['surat_keluar']['umum']['TGL_SURAT'], $data['surat_keluar']['umum']['UNIT_ID'])); 
		$data['unit'] = $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);

		#3.2 penandatanganan tnde
		$parameter							= array('api_kode' => 4004, 'api_subkode' => 1, 'api_search' => array($data['surat_keluar']['umum']['KD_JABATAN_2']));
		$data['penandatangan_surat']	= $this->apiconn->api_tnde('tnde_pegawai/pegawai_by_kd_jabatan', 'json', 'POST', $parameter);

		$psd = $this->apiconn->api_tnde('tnde_general/get_data', 'json', 'POST', array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('MD_PSD', 'ID_PSD', $data['surat_keluar']['umum']['ID_PSD'])));


		$this->load->view('templates/v_header');
		$this->load->view('templates/v_sidebar', $data);
		$this->load->view('v_suratsaya/v_detail_surat', $data);
		$this->load->view('templates/v_footer');
	}

	public function edit()
	{}

	public function aksi_hapus_surat($id_surat_keluar=null)
	{}

	public function aksi_perbarui_surat($id_surat_keluar=null)
	{}	

}