<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');
/**
* Gateway untuk mengakses endpoint api-api terkait surat keluar pada WEBSERVICE TNDE
* @author Mukhlas Imam Muhajir <mukhlas.imam.muhajir@gmail.com>
*/
class Api_tnde_sk extends MY_Controller {
	
	public function __construct()
	{
		parent::__construct();		
    }

	public function index()
	{
		echo ['message'=>'You got me! Aha..'];		
	}	
}