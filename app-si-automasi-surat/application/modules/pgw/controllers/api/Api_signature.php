<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');

//use \Firebase\JWT\JWT;
class Api_signature extends MY_Controller {
	
	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('log') !== 'in')
			redirect(site_url('pgw/login?from='.current_url()));

        $this->config->load('common/signature', TRUE);
        $this->load->helper('common/Package_loader');
        load_package_firebase_jwt();				
    }

    public function jwt_encode()
    {
        
    }

    public function jwt_decode()
    {}

    public function pembangkit_pasangan_kunci($show='public')
    {
       $config = array(
            "digest_alg" => "sha256",
            "private_key_bits" => 1024,
            "private_key_type" => OPENSSL_KEYTYPE_RSA,
        );
            
        // Create the private and public key
        $res = openssl_pkey_new($config);
        // Extract the private key from $res to $privKey
        openssl_pkey_export($res, $privKey);
        // Extract the public key from $res to $pubKey
        $pubKey = openssl_pkey_get_details($res);
        $pubKey = $pubKey["key"];

        if ($show == 'private'){
            $p1 = $privKey;
            echo var_dump($p1);
        } elseif ($show == 'public') {
            echo var_dump($pubKey);
        } elseif ($show == 'testing') {
            $data = 'plaintext data goes here';

            $sig = [
                'private_key' => $privKey,
                'public_key' => $pubKey,
                'data' => $data
            ];

            exit(var_dump($sig));

            // Encrypt the data to $encrypted using the public key
            openssl_public_encrypt($data, $encrypted, $pubKey);

            // Decrypt the data using the private key and store the results in $decrypted
            openssl_private_decrypt($encrypted, $decrypted, $privKey);

            echo $decrypted;
        }
    }

    public function pembangkit_keypair()
    {
        $config = array(
            "digest_alg" => "sha256",
            "private_key_bits" => 1024,
            "private_key_type" => OPENSSL_KEYTYPE_RSA,
        );            
        // Create the private and public key
        $res = openssl_pkey_new($config);


        $privKey = openssl_pkey_get_private($res);
        $pubKey = openssl_pkey_get_public($res);

        echo var_dump($privKey);
    }

    public function enkripsi_pesan()
    {
        $pesan = "ini adalah contoh pesan";
        $config = array(
            "digest_alg" => "sha256",
            "private_key_bits" => 1024,
            "private_key_type" => OPENSSL_KEYTYPE_RSA,
        );            
       // Create the private and public key
        $res = openssl_pkey_new($config);
        // Extract the private key from $res to $privKey
        openssl_pkey_export($res, $privKey);        
        // Extract the public key from $res to $pubKey
        $pubKey = openssl_pkey_get_details($res);        
        $pubKey = $pubKey["key"];
        
        $rsa_privkey = $privKey;

        $encryptedviaprivatekey = ""; //holds text encrypted with the private key
        $decryptedviapublickey = "";

        /*$rsa_privkey = "-----BEGIN PRIVATE KEY----- MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBAPp3+GhZWitGEYUs akoLtLYmrTp9NWKMztHrwE6cBSl3YQzG/tJ8ov17v2tAo/pk6eOyjomepSjpV3fi p2vHJF1/vvs/FijoGksk8IhsNg/scXbqRQ1k2AgA0B5HkAOOaJwiSdjGNSUxPBf0 yTbMvkPvGcpW5PoEW3a0orqa/LdfAgMBAAECgYEA3pioh/SBElxfTb+/Zrcoj4mo AocsXr1NQXsHmdVHMScLdVOVPHPQVv8Xi7YTBYGR6+7QhrYbilR2i/E/YaXGRiAe sDJhiEUs2nTd53HVFlm1ajGrX1aRCWCW8n+QFeR/kn3HQtuhFhgzj2V/Y8lUu5KQ w4K1Vv21LWa93lBsRKECQQD+Iegli97E86tyJ+hb3hf4VOjgoLArfVQE4/Cjxd8a HsLUWYxBN0Hx84RiEw9Fs75eEFEQMBJnRXU3EuM0jU5VAkEA/E8rvnHAjWyiQfam x/EeIK5SMfvqBf95cvapIwNzZC16KqU97pWxoJsWO2QY5ZKvKrH/gtSmILc6vDl6 VS064wJAG7y1qstuYA68arRCXyyJbzhNRp8jO0vtjK8YR7fEFwS+fRDdBw0x7GeI zm+IHjJ1F60uO9cTWyQwm/c6xzSZFQJBAKD0oKNNTdjqhFgwURUb7GxMyIK6ggBx ibidQijj+qjdmiG6aFfbhwU96mDY6kkJ2lUSWD3OCmy0dvO8dx3/TKECQQCvKEQw W+UF5opY2G5D//VV9bi4RXd2leEnFwI02Nvzrivqgp3fI30vsnkdtyz76w4+ffC5 PIqpdiwcsL13qRgf -----END PRIVATE KEY-----
        ";*/        

        openssl_private_encrypt($pesan, $encryptedviaprivatekey, $rsa_privkey);    
    
        echo var_dump($encryptedviaprivatekey);

        openssl_public_decrypt($encryptedviaprivatekey, $decryptedviapublickey, $pubKey);

        echo var_dump($decryptedviapublickey);
    }

    public function coba_enkrip()
    {
        $data = 'string yang pendek sdsds dsds dsd';
        $rsa_privkey = "-----BEGIN PRIVATE KEY-----
MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBAL3HWzT/V5iXhEKD
XX37JC3OnPlsnc6NEoDk06k+CX63V+GGfoUHuP35hLAwiVonuIEnnj48gjroR9uu
ycvgkHkPKhMcQRtzGO311eJMVIV+WgSvqQfi9sjM24YAbkOTl5PfUhgM2EIURzOL
ETmZ17BFAdn1F7g/dd9KKIeCxbL3AgMBAAECgYEAjbuCeX0tvW+DPm1/eGQErBU/
lOQyox0msY60KRSE4Ncq0eqxOMcAuOUN3MwS+Hrrx8F/5DkqhbYOXJoYaaQoIIXy
YvpDz6qIyU1/JFKAO377VgCNnRYKsUqfiD6gQm6pSZAteyMrJv5aTfkvaogTGPFG
PlZitHOOQUn2X4eMgrECQQD3R6+QmrmTmfZnzKeqXec0Wj1MwrdSOrT/jdxuqZ/n
1E/BI39X3CzviVrXh3HLOnBn4JZToQTwW3XojNAyqOmfAkEAxHiUPJwdkA4Msx9J
Ukah5EOnQGb3wBtyLUwOrpGvUxlh/xuz6smz8fFZIAI0r1gH4ZyDsbF5jyky3TVs
X2vnqQJAdAFMSqFbd/QdlZp7+b+tzHCdG1qpl8j0yoKpq7ylyKNgevIFNpmS9xjP
5hajerzCqOjCfR8n6v5tNRiCP5HgbwJBAIJsDPODKvjC6tlJbpHog3WVF4TgFZz4
Wz4InEJ0QW+njO8dg7zbctGsRl7COBkiFfFFxgKCbtCDJ9nCqhNZ/SkCQQDpP3Bj
6kU6bOyAsNLvD9uVs4Ydi96b9BkaXn36JK2gL981SzDPiqFoXIQFfKRJpz8EQme1
lLE5i/2owQsUXVIb
-----END PRIVATE KEY-----";

        openssl_private_encrypt($data, $crypted, $rsa_privkey);
        echo var_dump(bin2hex($crypted));
    }
    public function dekripsi_pesan()
    {
        $encrypted_in_hex = '32458633a3572902fee27e42a5b0cf0f716b6c542b0927c3f833f346e4b575316dea8bf5c1cd5ec03049afc34912666b44ba3d106854c6795f3ad391a80ab2610250d12009c5ce2988973a1265e6146060c3100492a2c6e1a9b6e0f6ecaf9585f6fe1c3d3eb57d3b00c29482ea02c4f0711647d914c2b8657b439b21a27fbb74';
        $encrypted_in_bin = hex2bin($encrypted_in_hex);

        $rsa_pubkey = "-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC9x1s0/1eYl4RCg119+yQtzpz5
bJ3OjRKA5NOpPgl+t1fhhn6FB7j9+YSwMIlaJ7iBJ54+PII66EfbrsnL4JB5DyoT
HEEbcxjt9dXiTFSFfloEr6kH4vbIzNuGAG5Dk5eT31IYDNhCFEczixE5mdewRQHZ
9Re4P3XfSiiHgsWy9wIDAQAB
-----END PUBLIC KEY-----";


        openssl_public_decrypt($encrypted_in_bin, $decrypted, $rsa_pubkey);

        echo var_dump($decrypted);
    }

    public function format_kunci()
    {
        $rsa_pubkey = "-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC9x1s0/1eYl4RCg119+yQtzpz5
bJ3OjRKA5NOpPgl+t1fhhn6FB7j9+YSwMIlaJ7iBJ54+PII66EfbrsnL4JB5DyoT
HEEbcxjt9dXiTFSFfloEr6kH4vbIzNuGAG5Dk5eT31IYDNhCFEczixE5mdewRQHZ
9Re4P3XfSiiHgsWy9wIDAQAB
-----END PUBLIC KEY-----";
        $base64_pubkey = base64_encode($rsa_pubkey);

        $rsa_pubkey_hex = implode(unpack("H*", $rsa_pubkey));
        $back_to_string = pack("H*", $rsa_pubkey_hex);

        echo var_dump($back_to_string);

    }


    public function coba_sign_pesan()
    {
        $pesan = 'saya adalah pesan dalam plain text saya adalah pesan dalam plain text saya adalah pesan dalam plain text saya adalah pesan dalam plain text saya adalah pesan dalam plain text saya adalah pesan dalam plain text saya adalah pesan dalam plain text saya adadalah pesan dalam plain text saya adalah pesan dalam plain text saya adalah pesan dalam plain text saya adalah pesan dalam plain text saya adalah pesan dalam plain text';
        $md_pesan = hash('sha256', $pesan);

$privateKey = <<<EOD
-----BEGIN RSA PRIVATE KEY-----
MIICXAIBAAKBgQC8kGa1pSjbSYZVebtTRBLxBz5H4i2p/llLCrEeQhta5kaQu/Rn
vuER4W8oDH3+3iuIYW4VQAzyqFpwuzjkDI+17t5t0tyazyZ8JXw+KgXTxldMPEL9
5+qVhgXvwtihXC1c5oGbRlEDvDF6Sa53rcFVsYJ4ehde/zUxo6UvS7UrBQIDAQAB
AoGAb/MXV46XxCFRxNuB8LyAtmLDgi/xRnTAlMHjSACddwkyKem8//8eZtw9fzxz
bWZ/1/doQOuHBGYZU8aDzzj59FZ78dyzNFoF91hbvZKkg+6wGyd/LrGVEB+Xre0J
Nil0GReM2AHDNZUYRv+HYJPIOrB0CRczLQsgFJ8K6aAD6F0CQQDzbpjYdx10qgK1
cP59UHiHjPZYC0loEsk7s+hUmT3QHerAQJMZWC11Qrn2N+ybwwNblDKv+s5qgMQ5
5tNoQ9IfAkEAxkyffU6ythpg/H0Ixe1I2rd0GbF05biIzO/i77Det3n4YsJVlDck
ZkcvY3SK2iRIL4c9yY6hlIhs+K9wXTtGWwJBAO9Dskl48mO7woPR9uD22jDpNSwe
k90OMepTjzSvlhjbfuPN1IdhqvSJTDychRwn1kIJ7LQZgQ8fVz9OCFZ/6qMCQGOb
qaGwHmUK6xzpUbbacnYrIM6nLSkXgOAwv7XXCojvY614ILTK3iXiLBOxPu5Eu13k
eUz9sHyD6vkgZzjtxXECQAkp4Xerf5TGfQXGXhxIX52yH+N2LtujCdkQZjXAsGdm
B2zNzvrlgRmgBrklMTrMYgm1NPcW+bRLGcwgW2PTvNM=
-----END RSA PRIVATE KEY-----
EOD;

$hexpk = <<<EOD
2d2d2d2d2d424547494e205253412050524956415445204b45592d2d2d2d2d0a4d4949435841494241414b42675143386b47613170536a6253595a566562745452424c78427a3548346932702f6c6c4c4372456551687461356b6151752f526e0a767545523457386f4448332b336975495957345651417a7971467077757a6a6b44492b3137743574307479617a795a384a58772b4b675854786c644d50454c390a352b7156686758767774696858433163356f4762526c454476444636536135337263465673594a34656864652f7a55786f3655765337557242514944415141420a416f4741622f4d585634365878434652784e7542384c7941746d4c4467692f78526e54416c4d486a5341436464776b794b656d382f2f38655a747739667a787a0a62575a2f312f646f514f75484247595a553861447a7a6a3539465a373864797a4e466f4639316862765a4b6b672b36774779642f4c72475645422b587265304a0a4e696c304752654d324148444e5a555952762b48594a50494f7242304352637a4c517367464a384b36614144364630435151447a62706a596478313071674b310a63503539554869486a505a5943306c6f45736b37732b68556d54335148657241514a4d5a5743313151726e324e2b796277774e626c444b762b733571674d51350a35744e6f51394966416b4541786b796666553679746870672f483049786531493272643047624630356269497a4f2f693737446574336e3459734a566c44636b0a5a6b63765933534b326952494c346339795936686c4968732b4b39775854744757774a42414f3944736b6c34386d4f37776f505239754432326a44704e5377650a6b39304f4d6570546a7a53766c686a626675504e314964687176534a544479636852776e316b494a374c515a67513866567a394f43465a2f36714d4351474f620a71614777486d554b36787a7055626261636e5972494d366e4c536b58674f417776375858436f6a7659363134494c544b336958694c424f78507535457531336b0a65557a397348794436766b675a7a6a747858454351416b703458657266355447665158475868784958353279482b4e324c74756a43646b515a6a58417347646d0a42327a4e7a76726c67526d6742726b6c4d54724d59676d314e5063572b62524c4763776757325054764e4d3d0a2d2d2d2d2d454e44205253412050524956415445204b45592d2d2d2d2d
EOD;
    //exit(var_dump(hex2bin($hexpk)));

        openssl_sign($md_pesan, $signature, hex2bin($hexpk));

        openssl_sign(data, signature, priv_key_id)        
        echo var_dump(base64_encode($signature));
    }
}