<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');

class Api_beasiswa extends MY_Controller {
    
    public function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('log') !== 'in')
            redirect(site_url('pgw/login?from='.current_url()));
        $this->load->model('Repo_Beasiswa');
    }

    

    public function cek_beasiswa($nim=null)
    {	   	
        $api_get = $this->Repo_Beasiswa->get_beasiswa($nim);
        echo json_encode($api_get, JSON_PRETTY_PRINT);

    }


    public function get_beasiswa($nim=null)
    {
	   	$parameter  = array('api_kode' => 1000, 'api_subkode' => 3, 'api_search' => array($nim));
        $api_get    = $this->apiconn->api_beasiswa('sia_beasiswa_mhs/get_status_mhs', 'json', 'POST', $parameter);

        echo var_dump($api_get);

    }

    public function get_beasiswa_diikuti($nim)
	{		
		$parameter  = array('api_kode' => 1000, 'api_subkode' => 3, 'api_search' => array($nim));
        $api_get         = $this->apiconn->api_beasiswa('sia_beasiswa_mhs/get_status_mhs', 'json', 'POST', $parameter);
        
        $equal = 'A'; 
		$result = array_filter($api_get, function ($item) use ($equal) {		    
		    if ($item['STATUS'] === $equal){
		    	return true;
		    }
		    return false;
		});

        echo var_dump($result);
	}

    public function cek_bidikmisi($nim)
    {
    	$api_get = $this->api_simple_connect('mahasiswa_bidikmisi', 
    		'json', array('nim'=>$nim)
    	);
    	echo var_dump($api_get);
    }


    /**
    * Menjalankan CURL untuk mengakses Webservice
    * @param (string) $endpoint 
    * @param (enum) json | xml | html $format
    * @param (array) $parameter
    */
    protected function api_simple_connect($endpoint=null, $format='json', $parameter=array()){
    	$apihost = 'http://service2.uin-suka.ac.id';
    	$apipath = 'servsibayar/index.php/data/md_mahasiswa/';
    	$parameter = implode('/', $parameter);
        $ch = curl_init();
        $URL_API = $apihost.'/'.$apipath.'/'.$endpoint.'/'.$format.'/'.$parameter;
        //$data = array('api_kode' => $apikod, 'api_subkode' => $apisub, $apitxt => $apisrc);
        curl_setopt($ch, CURLOPT_URL, $URL_API);
        //curl_setopt($ch, CURLOPT_POST, 1);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('HeaderName: '.hash('sha256','tester01')));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        return $result;
    }

}