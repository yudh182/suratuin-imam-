<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');
class Api_qrcode extends MY_Controller {
	
	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('log') !== 'in')
			redirect(site_url('pgw/login?from='.current_url()));
		
    }

    public function index()
    {
    	$arr_pesan = [
    		'id_surat_keluar' => uniqid(),
    		'unit_id' => 'UN02006',
    		'kd_jenis_surat' => 1,
    		'kd_jenis_surat_induk' => 11,
    		'tanggal' => date('Y-m-d'),
    		'nomor' => '1377/Un.02/TST/KM.00.4/03/2018',
    		'penandatanganan' => [
    			'nip' => '19921787885267',
    			'kd_jabatan' => 'ST0278',
    			'atas_nama' => null,
    			'untuk_beliau' => null
    		],    	
    		'dicetak_oleh' => '11650021',
    		'penerima_surat' => [
    			[
    				'id' => '11650021',
    				'jabatan_penerima' => null,
    				'grup' => 'MHS01'
    			],
    			[
    				'id' => '19921787885267',
    				'jabatan_penerima' => 'ST0278',
    				'grup' => 'PJB01'
    			],

    		],  
    		'tembusan_surat' => []
    	];

    	$json_pesan = json_encode($arr_pesan);
    	
    	$hash_pesan = hash_hmac('sha256', $json_pesan, 'SECRETKEY990');

    	$hash_pesan2 = hash_hmac('sha256', $json_pesan, 'SECRETKEY990');
    	
    	if ($hash_pesan == $hash_pesan2){
    		echo "identik";
    	} else {
    		echo "tidak identik";
    	}
    }
}    