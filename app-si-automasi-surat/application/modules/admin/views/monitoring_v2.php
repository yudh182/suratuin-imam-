<div style="width:700px">
	<h3 style="color:#333333">Pengolah Surat</h3>
	<table class="table table-bordered table-hover">
		<tbody>
			<tr>
				<th width="26px">No</th><th>Pegawai</th><th>Sebagai</th><th>Status Akses</th><th width="105px">Waktu Akses</th>
			</tr>
			<tr>
				<td>1</td>
				<td>
					<?php
						switch($monitoring['SURAT_MASUK']['JENIS_PENCATAT']){
							case 'P':
								$nm_pencatat	= (!empty($monitoring['ARSIP']['PEGAWAI']['NM_PGW_F']) ? $monitoring['ARSIP']['PEGAWAI']['NM_PGW_F'] : '-');
								$jab_pencatat	= (!empty($monitoring['ARSIP']['JABATAN']['STR_NAMA']) ? $monitoring['ARSIP']['JABATAN']['STR_NAMA'] : '-');
								$kd_pencatat	= (!empty($monitoring['ARSIP']['PEGAWAI']['KD_PGW']) ? $monitoring['ARSIP']['PEGAWAI']['KD_PGW'] : '-');
								break;
							case 'M':
								$nm_pencatat	= (!empty($monitoring['ARSIP']['MAHASISWA']['NAMA_F']) ? $monitoring['ARSIP']['MAHASISWA']['NAMA_F'] : '-');
								$jab_pencatat	= (!empty($monitoring['ARSIP']['JABATAN']) ? $monitoring['ARSIP']['JABATAN'] : '-');
								$kd_pencatat	= (!empty($monitoring['ARSIP']['MAHASISWA']['NIM']) ? $monitoring['ARSIP']['MAHASISWA']['NIM'] : '-');
								break;
							default:
								$nm_pencatat = '';
								$jab_pencatat = '';
								$kd_pencatat = '';
								break;
						}
						
						echo $nm_pencatat;
						echo "<br/><hr/>";
						echo $jab_pencatat;
						echo "<br/><hr/>";
						echo $kd_pencatat;
					?>
				</td>
				<td>ARSIP</td>
				<td><span class="label label-success">TELAH DIPROSES</span></td>
				<td>
					<?php
					echo (!empty($monitoring['SURAT_MASUK']['WAKTU_PENCATATAN']) ? $monitoring['SURAT_MASUK']['WAKTU_PENCATATAN'] : '-');
					?>
				</td>
			</tr>
			<?php
			if(!empty($monitoring['PENGARAH'])){ ?>
				<tr>
					<td>2</td>
					<td>
						<?php
						switch($monitoring['SURAT_MASUK']['JENIS_PENCATAT']){
							case 'P':
								$nm_pengarah	= (!empty($monitoring['PENGARAH']['PEGAWAI']['NM_PGW_F']) ? $monitoring['PENGARAH']['PEGAWAI']['NM_PGW_F'] : '-');
								$jab_pengarah	= (!empty($monitoring['PENGARAH']['JABATAN']['STR_NAMA']) ? $monitoring['PENGARAH']['JABATAN']['STR_NAMA'] : '-');
								$kd_pengarah	= (!empty($monitoring['PENGARAH']['PEGAWAI']['KD_PGW']) ? $monitoring['PENGARAH']['PEGAWAI']['KD_PGW'] : '-');
								break;
							case 'M':
								$nm_pengarah	= (!empty($monitoring['PENGARAH']['MAHASISWA']['NAMA_F']) ? $monitoring['PENGARAH']['MAHASISWA']['NAMA_F'] : '-');
								$jab_pengarah	= (!empty($monitoring['PENGARAH']['JABATAN']) ? $monitoring['PENGARAH']['JABATAN'] : '-');
								$kd_pengarah	= (!empty($monitoring['PENGARAH']['MAHASISWA']['NIM']) ? $monitoring['PENGARAH']['MAHASISWA']['NIM'] : '-');
								break;
							default:
								$nm_pengarah = '';
								$jab_pengarah = '';
								$kd_pengarah = '';
								break;
						}
						
						echo $nm_pengarah;
						echo "<br/><hr/>";
						echo $jab_pengarah;
						echo "<br/><hr/>";
						echo $kd_pengarah;
						?>
					</td>
					<td>PENGARAH SURAT</td>
					<td>
						<span class="label label-<?php if($monitoring['SURAT_MASUK']['ID_STATUS_SM']==1){ echo "danger";}else if($monitoring['SURAT_MASUK']['ID_STATUS_SM']==2){ echo "warning";}else{ echo "success";}?>">
						<?php echo $monitoring['SURAT_MASUK']['NM_STATUS_SM'];?>
						</span>
					</td>
					<td>
						<?php
						echo (!empty($monitoring['SURAT_MASUK']['WAKTU_AKSES']) ? $monitoring['SURAT_MASUK']['WAKTU_AKSES'] : '-');
						?>
					</td>
				</tr>
				<?php
			}
			
			$n = 3;
			if(!empty($monitoring['PENERIMA_SURAT'])){
				foreach($monitoring['PENERIMA_SURAT'] as $val){ ?>
					<tr>
						<td><?php echo $n;?></td>
						<td>
							<?php
							switch($val['KD_GRUP_TUJUAN']){
								case 'PGW01':
									$nm_penerima	= (!empty($val['SIMPEG_PENERIMA']['PEGAWAI']['NM_PGW_F']) ? $val['SIMPEG_PENERIMA']['PEGAWAI']['NM_PGW_F'] : '-');
									$jab_penerima	= (!empty($val['SIMPEG_PENERIMA']['JABATAN']['STR_NAMA']) ? $val['SIMPEG_PENERIMA']['JABATAN']['STR_NAMA'] : '-');
									$kd_penerima	= (!empty($val['SIMPEG_PENERIMA']['PEGAWAI']['KD_PGW']) ? $val['SIMPEG_PENERIMA']['PEGAWAI']['KD_PGW'] : '-');
									break;
								case 'PJB01':
									$nm_penerima	= (!empty($val['SIMPEG_PENERIMA']['PEGAWAI']['NM_PGW_F']) ? $val['SIMPEG_PENERIMA']['PEGAWAI']['NM_PGW_F'] : '-');
									$jab_penerima	= (!empty($val['SIMPEG_PENERIMA']['JABATAN']['STR_NAMA']) ? $val['SIMPEG_PENERIMA']['JABATAN']['STR_NAMA'] : '-');
									$kd_penerima	= (!empty($val['SIMPEG_PENERIMA']['PEGAWAI']['KD_PGW']) ? $val['SIMPEG_PENERIMA']['PEGAWAI']['KD_PGW'] : '-');
									break;
								case 'MHS01':
									$nm_penerima	= (!empty($val['MAHASISWA_PENERIMA']['MAHASISWA']['NAMA_F']) ? $val['MAHASISWA_PENERIMA']['MAHASISWA']['NAMA_F'] : '-');
									$jab_penerima	= (!empty($val['MAHASISWA_PENERIMA']['JABATAN']) ? $val['MAHASISWA_PENERIMA']['JABATAN'] : '-');
									$kd_penerima	= (!empty($val['MAHASISWA_PENERIMA']['MAHASISWA']['NIM']) ? $val['MAHASISWA_PENERIMA']['MAHASISWA']['NIM'] : '-');
									break;
								default:
									$nm_penerima = '';
									$jab_penerima = '';
									$kd_penerima = '';
									break;
							}
							
							echo $nm_penerima;
							echo "<br/><hr/>";
							echo $jab_penerima;
							echo "<br/><hr/>";
							echo $kd_penerima;
							?>
						</td>
						<td>PENERIMA SURAT</td>
						<td>
							<span class="label label-<?php if($val['ID_STATUS_SM']==1){ echo "danger";}else if($val['ID_STATUS_SM']==2){ echo "warning";}else{ echo "success";}?>">
							<?php echo $val['NM_STATUS_SM'];?>
							</span>
						</td>
						<td>
							<?php
							echo (!empty($val['WAKTU_AKSES']) ? $val['WAKTU_AKSES'] : '-');
							?>
						</td>
					</tr>
					<?php
					$n++;
				}
			}
			
			if(!empty($monitoring['TEMBUSAN_SURAT'])){
				foreach($monitoring['TEMBUSAN_SURAT'] as $val){ ?>
					<tr>
						<td><?php echo $n;?></td>
						<td>
							<?php
							switch($val['KD_GRUP_TUJUAN']){
								case 'PGW01':
									$nm_penerima	= (!empty($val['SIMPEG_PENERIMA']['PEGAWAI']['NM_PGW_F']) ? $val['SIMPEG_PENERIMA']['PEGAWAI']['NM_PGW_F'] : '-');
									$jab_penerima	= (!empty($val['SIMPEG_PENERIMA']['JABATAN']['STR_NAMA']) ? $val['SIMPEG_PENERIMA']['JABATAN']['STR_NAMA'] : '-');
									$kd_penerima	= (!empty($val['SIMPEG_PENERIMA']['PEGAWAI']['KD_PGW']) ? $val['SIMPEG_PENERIMA']['PEGAWAI']['KD_PGW'] : '-');
									break;
								case 'PJB01':
									$nm_penerima	= (!empty($val['SIMPEG_PENERIMA']['PEGAWAI']['NM_PGW_F']) ? $val['SIMPEG_PENERIMA']['PEGAWAI']['NM_PGW_F'] : '-');
									$jab_penerima	= (!empty($val['SIMPEG_PENERIMA']['JABATAN']['STR_NAMA']) ? $val['SIMPEG_PENERIMA']['JABATAN']['STR_NAMA'] : '-');
									$kd_penerima	= (!empty($val['SIMPEG_PENERIMA']['PEGAWAI']['KD_PGW']) ? $val['SIMPEG_PENERIMA']['PEGAWAI']['KD_PGW'] : '-');
									break;
								case 'MHS01':
									$nm_penerima	= (!empty($val['MAHASISWA_PENERIMA']['MAHASISWA']['NAMA_F']) ? $val['MAHASISWA_PENERIMA']['MAHASISWA']['NAMA_F'] : '-');
									$jab_penerima	= (!empty($val['MAHASISWA_PENERIMA']['JABATAN']) ? $val['MAHASISWA_PENERIMA']['JABATAN'] : '-');
									$kd_penerima	= (!empty($val['MAHASISWA_PENERIMA']['MAHASISWA']['NIM']) ? $val['MAHASISWA_PENERIMA']['MAHASISWA']['NIM'] : '-');
									break;
								default:
									$nm_penerima = '';
									$jab_penerima = '';
									$kd_penerima = '';
									break;
							}
							
							echo $nm_penerima;
							echo "<br/><hr/>";
							echo $jab_penerima;
							echo "<br/><hr/>";
							echo $kd_penerima;
							?>
						</td>
						<td>TEMBUSAN SURAT</td>
						<td>
							<span class="label label-<?php if($val['ID_STATUS_SM']==1){ echo "danger";}else if($val['ID_STATUS_SM']==2){ echo "warning";}else{ echo "success";}?>">
							<?php echo $val['NM_STATUS_SM'];?>
							</span>
						</td>
						<td>
							<?php
							echo (!empty($val['WAKTU_AKSES']) ? $val['WAKTU_AKSES'] : '-');
							?>
						</td>
					</tr>
					<?php
					$n++;
				}
			}
			?>
		</tbody>
	</table>
	<?php
	if(!empty($monitoring['DISTRIBUSI'])){ ?>
		<h3>Disposisi/Terusan/Memo/Nota Dinas</h3>
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th width="26px">No</th><th>Waktu Kirim</th><th>Dari</th><th>Kepada</th><th width="110px">Status Akses</th><th width="105px">Waktu Akses</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$a = 1;
				foreach($monitoring['DISTRIBUSI'] as $val){ ?>
					<tr>
						<td>
							<?php echo $a;?>
						</td>
						<td>
							<?php echo $val['WAKTU_KIRIM'];?>
						</td>
						<td>
							<?php 
							$nm_pengirim = '';
							switch($val['KD_GRUP_PENGIRIM']){
								case 'PJB01':
									$nm_pengirim = (!empty($val['SIMPEG_PENGIRIM']['JABATAN']['STR_NAMA']) ? $val['SIMPEG_PENGIRIM']['JABATAN']['STR_NAMA'] : '-');
									break;
								case 'PGW01':
									$nm_pengirim = (!empty($val['SIMPEG_PENGIRIM']['PEGAWAI']['NM_PGW_F']) ? $val['SIMPEG_PENGIRIM']['PEGAWAI']['NM_PGW_F'] : '-');
									break;
								case 'MHS01':
									$nm_pengirim = (!empty($val['MAHASISWA_PENGIRIM']['MAHASISWA']['NAMA_F']) ? $val['MAHASISWA_PENGIRIM']['MAHASISWA']['NAMA_F'] : '-');
									break;
								default:
									$nm_pengirim = '';
									break;
							}
							echo $nm_pengirim;
							?>
						</td>
						<td>
							<?php 
							$nm_penerima = '';
							switch($val['KD_GRUP_TUJUAN']){
								case 'PJB01':
									$nm_penerima = (!empty($val['SIMPEG_PENERIMA']['JABATAN']['STR_NAMA']) ? $val['SIMPEG_PENERIMA']['JABATAN']['STR_NAMA'] : '-');
									break;
								case 'PGW01':
									$nm_penerima = (!empty($val['SIMPEG_PENERIMA']['PEGAWAI']['NM_PGW_F']) ? $val['SIMPEG_PENERIMA']['PEGAWAI']['NM_PGW_F'] : '-');
									break;
								case 'MHS01':
									$nm_penerima = (!empty($val['MAHASISWA_PENERIMA']['MAHASISWA']['NAMA_F']) ? $val['MAHASISWA_PENERIMA']['MAHASISWA']['NAMA_F'] : '-');
									break;
								default:
									$nm_penerima = '';
									break;
							}
							echo $nm_penerima;
							?>
						</td>
						<td>
							<span class="label label-<?php if($val['ID_STATUS_SM']==1){ echo "danger";}else if($val['ID_STATUS_SM']==2){ echo "warning";}else{ echo "success";}?>">
								<?php echo $val['NM_STATUS_SM'];?>
							</span>
						</td>
						<td>
							<?php echo (!empty($val['WAKTU_AKSES']) ? $val['WAKTU_AKSES'] : '-');?>
						</td>
					</tr>
					<?php
					$a++;
				}
				?>
			</tbody>
		</table><?php
	} ?>
</div>
<div class="clear"></div><br />