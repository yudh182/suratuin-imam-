<div class="col-med-9">
	<div class="content-space">
		<ul id="crumbs">
			<li>
				<a title="Master Data" href="<?php echo base_url();?>pegawai/admin_master">Master Data</a>
			</li>
			<li>
				<a title="Penandatanganan Surat" href="#">Penandatanganan Surat</a>
			</li>
		</ul><br/>
		<?php
		if(ISSET($success)){ 
			foreach($success as $value){ ?>		
				<div class="bs-callout bs-callout-success" style="margin-bottom:5px">
					<?php echo $value;?>
				</div><?php
			}
		}
		if(ISSET($errors)){ ?>
			<div class="bs-callout bs-callout-error" style="margin-bottom:5px">
				<p><b>Terjadi Kegagalan</b></p><?php
				foreach($errors as $value){
					echo "- ".$value."<br/>";
				} ?>
			</div><?php
		}
		?>
		<div class="overviewhead" style="height:48px">
			<div class="right">
				<form action="<?php echo base_url();?>pegawai/admin_master/psd_cari" method="POST">
					<input type="text" name="perihal" placeholder="Cari Penandatanganan Surat" style="width:160px" />
					<input type="hidden" name="q_psd" value="cari" />
					<button type="submit" class="btn btn-default btn-small" style="border-radius:0;padding:5px;">
						Cari
					</button>
				</form>
			</div>
		</div>
		<table class="table table-bordered table-hover">
			  <thead>
				  <tr>
					  <th width="22px"><center>No</center></th>
					  <th width="168px"><center>Kode Penandatanganan Surat</center></th>
					  <th width=""><center>Pejabat</center></th>
					  <th width="120px"><center>Aksi</center></th>
				  </tr>
			  </thead>   
			  <tbody id="content-check">
			  <?php
			  $uri = (int)preg_replace("/[^0-9]/", "", $this->uri->segment(4));
			  if(ISSET($uri)){
				$n = $uri;
			  }else{
				$n = 0;
			  }
			  if(!empty($psd_master)){
				  foreach($psd_master as $key => $data){
					$n = $n + 1; ?>
					<tr>	
						<td class="centered"><?php echo $n;?></td>
						<td class="centered"><?php echo $data['KD_PSD'];?></td>
						<td>
							<?php 
							$unit	= (!empty($jabatan[$key][0]['SUB_UNIT_ID']) ? " [ ".$jabatan[$key][0]['UNIT_NAMA']." ]" : '');
							echo (!empty($jabatan[$key][0]['STR_NAMA']) ? $jabatan[$key][0]['STR_NAMA'].$unit : '');
							?>
						</td>
						<td>
							<center>
								<a class="btn btn-default btn-small" href="<?php echo base_url();?>pegawai/admin_master/psd_detail/<?php echo $data['ID_PSD'];?>" title="Lihat Detail" data-rel="tooltip">
									Lihat
								</a>
								<a class="btn btn-small btn-danger btn-act" title="Hapus" data-rel="tooltip" onclick="daletePSD($(this));" data-id_psd="<?php echo $data['ID_PSD'];?>">
									Hapus
								</a>
							</center>
						</td>
					</tr><?php
				  }
			  }else{ ?>
				<tr class="error centered"><td colspan="4">Belum ada data penandatanganan surat dinas yang disimpan</td></tr><?php
			  }
			  ?>	
			  </tbody>
		 </table>
		<?php
		if(ISSET($links)){
			echo $links;
		}
		?>
	</div>
</div>
<div class="modal fade" id="modalDeletePSD">
  <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><i class="icon-remove"></i></button>
				<div class="contenttitle">
					<h3>Konfirmasi</h3>
				</div>
			</div>
			<div class="modal-body">
				<p><!--<img class="modal-icon" src="img/trash.png" />--> Apakah Anda yakin ingin menghapus data ini ?</p>
			</div>
			<div class="modal-footer">
				<form action="<?php echo base_url();?>pegawai/admin_master/psd_master" method="POST" >
					<a href="#" class="btn btn-default rec" data-dismiss="modal">Tidak</a>
					<input type="hidden" id="id_psd" name="id_psd" value="" />
					<input type="hidden" name="del-psd" value="Delete PSD" />
					<input type="submit" class="btn btn-danger" value="Ya" />
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">		
	function daletePSD(el){
		var id_psd = el.data('id_psd');
		document.getElementById('id_psd').value = id_psd;
		$('#modalDeletePSD').modal('show');
	}
</script>