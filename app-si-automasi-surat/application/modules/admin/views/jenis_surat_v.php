<div class="col-med-9">
	<div class="content-space">
		<ul id="crumbs">
			<li>
				<a title="Master Data" href="<?php echo base_url();?>pegawai/admin_master">Master Data</a>
			</li>
			<li>
				<a title="Jenis Surat" href="#">Jenis Surat</a>
			</li>
		</ul><br/>
			<?php
			$sess_errors = $this->session->flashdata('errors');
			if((!empty($errors))||(!empty($sess_errors))){ ?>
				<div class="bs-callout bs-callout-error" style="margin-bottom:5px">
					<?php
					if(!empty($errors)){
						if(is_array($errors)){
							foreach($errors as $value){
								echo "- ".$value."<br/>";
							}
						}else{
							echo $errors;
						}
					}
					if(!empty($sess_errors)){
						if(is_array($sess_errors)){
							foreach($sess_errors as $value){
								echo "- ".$value."<br/>";
							}
						}else{
							echo $sess_errors;
						}
					}
					?>
				</div><?php
			}
			
			$sess_success = $this->session->flashdata('success');
			if((!empty($success))||(!empty($sess_success))){  ?>
				<div class="bs-callout bs-callout-success" style="margin-bottom:5px">
					<?php
					if(!empty($success)){
						if(is_array($success)){
							foreach($success as $value){
								echo "- ".$value."<br/>";
							}
						}else{
							echo $success;
						}
					}
					if(!empty($sess_success)){
						if(is_array($sess_success)){
							foreach($this->session->flashdata('success') as $value){
								echo "- ".$value."<br/>";
							}
						}else{
							echo $sess_success;
						}
					}
					?>
				</div><?php
			}
			?>
		<table class="table table-bordered table-hover">
			  <thead>
				  <tr>
					  <th width="22px"><center>No</center></th>
					  <th width=""><center>Nama Jenis Surat</center></th>
					  <th width="260px"><center>Kelompok Penomoran Surat</center></th>
					  <th width="100px"><center>Aksi</center></th>
				  </tr>
			  </thead>   
			  <tbody id="content-check">
				<?php
				if(!empty($jenis_surat)){
					$n = 1;
					foreach($jenis_surat as $val){ ?>
						<tr>
							<td><?php echo $n; ?></td>
							<td><?php echo $val['NM_JENIS_SURAT']; ?></td>
							<td><?php echo ($val['KD_KAT_PENOMORAN'] != NULL ? $val['NM_KAT_PENOMORAN'] : 'BELUM ADA DATA RIWAYAT KELOMPOK PENOMORAN'); ?></td>
							<td class="centered">
								<a class="btn btn-small btn-default" href="<?php echo base_url('pegawai/admin_master/his_jenis_surat/'.$val['KD_JENIS_SURAT']);?>">Lihat & Edit</a>
							</td>
						</tr><?php
						$n++;
					}
				}else{ ?>
					<tr><td colspan="3"><center>BELUM ADA JENIS SURAT YANG DITAMBAHKAN</center></td></tr><?php
				}
				?>
			  </tbody>
		 </table>
	</div>
</div>