<div class="col-med-3">
	<div id="sidebar-sia">
		<?php
		$modul = $this->uri->segment(1);
		$surat_diterbitkan   = ($modul == 'surat_diterbitkan' ? TRUE : FALSE);
		$admin_master   = ($modul == 'master_settings' ? TRUE : FALSE);		

		$log_username = $this->session->userdata('username');				
		?>
		<nav class="accordion">
			<ol>
				<!-- start user profile -->				
				<li>
					<div class="sia-profile" style="margin-bottom:10px;width:223px">						
						<?php if ($this->session->userdata('grup') == 'pgw')
						echo '<img class="sia-profile-image" alt="foto pengguna"  src="'.$this->api_foto->foto_pgw($this->session->userdata("username")).'" />';
						?>

						<h2><?php  echo $this->session->userdata('nama'); ?></h2>		
						<p style="text-align:center; font-weight:bold;"><?php echo trans_nip_pgw($this->session->userdata('username')); ?></p>
						<hr>						
						<table>
							<tr>
								<td>Unit</td>
								<td>PTIPD</td>
							</tr>
							<tr>
								<td>Jabatan</td>
								<td><b><?php echo (!empty($this->session->userdata('str_nama')) ? $this->session->userdata('str_nama') : ''); ?></b></td>
							</tr>
						</table>						
					</div>
					<div id="separate"></div>
				</li>
				<!-- end user profie -->

				<!-- start app menu -->
				<li>
					<a class="item full" href="<?php echo site_url('admin/semua_surat_diterbitkan'); ?>">
						<span>
							Surat Diterbitkan
							<span>
								<div class="underline-menu"></div>
							</span>
						</span>
					</a>

					<?php if ($surat_diterbitkan == TRUE): ?>
					<ol>						
						<li class="submenu">
							<a href="<?php echo site_url('admin/arsip_surat_keluar'); ?>">
								Surat-surat Keluar
							</a>
						</li>
						<li class="submenu">
							<a href="<?php echo site_url('admin/arsip_monitor'); ?>">
								Monitor Pengajuan Surat
						</li>
						<li class="submenu">
							<a href="<?php echo site_url('administrasi/data_notifikasi'); ?>">
								Data-data Notifikasi
							</a>
						</li>												
						<li class="submenu">
							<hr>
						</li>
					</ol>
					<?php endif; ?>					
				</li>

								
				<li>
					<a class="item full" href="<?php echo site_url('pgw/master_auto_surat_mhs/jenis');?>">
						<!-- <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span> -->
						<span>
							Pengaturan Master Data
							<span>
								<div class="underline-menu"></div>
							</span>
						</span>
					</a>

					<?php if ($admin_master == false): ?>
					<ol id="menu-master-automasi" class="submenus-container" style="display: block;">
						<li class="submenu">
							<a href="<?= site_url('admin/master/jenis'); ?>">
								Master Jenis Surat Mahasiswa
							</a>
						</li>
						<li class="submenu">
							<a href="<?= site_url('admin/master/repository_persyaratan'); ?>">
								Master Persyaratan Verifikasi
							</a>
						</li>
						<li class="submenu">
							<a href="<?= site_url('admin/master/config_automasi'); ?>">
								Master Hak Akses Pengguna
							</a>
						</li>
						<li class="submenu">
							<hr>
						</li>																		
					</ol>										
					<?php endif; ?>	
				</li>				

				<li>
					<a class="item full" href="<?php echo base_url(); ?>auth/logout">
						<span>Logout</span>
						<div class="underline-menu"></div>
					</a>
				</li>
			</ol>
		</nav>
	</div>
</div>	