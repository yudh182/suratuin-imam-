<div class="col-med-9">
	<div class="content-space">
		<ul id="crumbs">
			<li>
				<a title="Surat Keluar" href="#">Surat Keluar</a>
			</li>
			<li>
				<a title="Arsip Surat Keluar" href="#">Arsip Surat Keluar</a>
			</li>
		</ul><br/>
		<?php
		if(ISSET($success)){ 
			foreach($success as $value){ ?>		
				<div class="bs-callout bs-callout-success" style="margin-bottom:5px">
					<?php echo $value; if(ISSET($tetapi)){ echo ". <b>Tetapi dengan beberapa kesalahan</b>";}?>
				</div><?php
			}
		}
		if(ISSET($errors)){ ?>
			<div class="bs-callout bs-callout-error" style="margin-bottom:5px">
				<p><b>Terjadi Kegagalan</b></p><?php
				foreach($errors as $value){
					echo "- ".$value."<br/>";
				} ?>
			</div><?php
		}
		?>
		<div class="cari">
			<div class="right">
				<form action="<?php echo base_url();?>pegawai/surat_keluar/cari" method="POST">
					<!--<select name="switch" style="width:160px;">
						<option value="saya" <?php if($this->session->userdata('surat')=='saya'){ echo "selected";}?>>Surat Saya</option>
						<option value="semua" <?php if($this->session->userdata('surat')=='semua'){ echo "selected";}?>>Semua Surat</option>
					</select>&nbsp;-->
					<input type="text" name="perihal" placeholder="Cari Surat Keluar" style="width:160px;margin-top:0px" value="<?php if($this->uri->segment(4) != null){ echo back_cari($this->uri->segment(4));}?>"/>
					<input type="hidden" name="qsk" value="true" />
					<button type="submit" class="btn btn-default btn-small" style="border-radius:0;padding:5px;margin-top:0px">
						Cari
					</button>
				</form>
			</div>
		</div>
		<form action="<?php echo base_url();?>pegawai/surat_keluar" method="POST">
			<div class="overviewhead" style="height:48px">
				<input type="hidden" name="del-all" value="surat masuk" />
				<!--<button class="btn btn-small" href="#" onClick="return confirm('Anda yakin ingin menghapus data surat masuk secara permanen ?');">
					<i class="icon-trash"></i>
					Delete
				</button>-->
			</div>
			<table class="table table-bordered table-hover">
				  <thead>
					  <tr>
						  <!--<th width="2%"><center><input type="checkbox" id="checkall" /></center></th>-->
						  <th width="20px"><center>No</center></th>
						  <th width="140px"><center>Nomor Surat</center></th>
						  <th width="72px"><center>Tanggal Surat</center></th>
						  <!--<th width="16%"><center>Kepada</center></th>-->
						  <th width="100px"><center>Jenis Surat</center></th>
						  <th><center>Perihal</center></th>
						  <th width="100px"><center>Aksi</center></th>
					  </tr>
				  </thead>  
				  <tbody id="content-check">
						<?php
						$url = $this->uri->segment(5);
						if(ISSET($url)){
							$i = $url;
						}else{
							$i = 0;
						}
						if(!empty($surat_keluar)){
							foreach($surat_keluar as $key => $data){
								$i = $i + 1; 
								switch($data['KD_STATUS_SIMPAN']){
									case 0:
										$status = 'danger';
										break;
									case 3:
										$status = 'success';
										break;
									default:
										$status = '';
										break;
								} ?>
								<tr class="<?php echo $status;?>">
									<td class="center"><?php echo $i;?></td>
									<td class="center"><?php echo $data['NO_SURAT'];?></td>
									<td class="center">
										<?php echo $data['TGL_SURAT'];?>
										<hr/>
										<?php echo (!empty($untuk_unit[$key]['UNIT_NAMA']) ? $untuk_unit[$key]['UNIT_NAMA'] : ''); ?>
									</td>
									<td class="center"><?php echo $data['NM_JENIS_SURAT'];?></td>
									<td class="center">
										<?php echo $data['PERIHAL'];?>
									</td>
									<td>
										<?php
										$keamanan = array('R', 'SR');
										if(in_array('SPR', $modules)){
											$aksi = TRUE;
										}else{
											if(in_array($data['KD_KEAMANAN_SURAT'], $keamanan)){
												if($data['PEMBUAT_SURAT'] == $this->session->userdata('kd_pgw')){
													$aksi = TRUE;
												}else{
													$aksi = FALSE;
												}
											}else{
													$aksi = TRUE;
											}
										}
										
										if($aksi == TRUE){ ?>
											<a class="btn btn-default btn-small" href="<?php echo base_url();?>pegawai/surat_keluar/detail/<?php echo $data['ID_SURAT_KELUAR'];?>" title="Lihat & Edit Data" data-rel="tooltip">
												Lihat
											</a>
											<?php
											switch($data['KD_STATUS_SIMPAN']){
												case 0: ?>
													<form action="<?php echo base_url();?>pegawai/surat_keluar" method="POST">
														<input type="hidden" name="id_surat_keluar" value="<?php echo $data['ID_SURAT_KELUAR'];?>" />
														<input type="hidden" name="suitchi" value="1" />
														<button class="btn btn-default btn-small" title="Aktifkan"><i class="icon-star"></i> Aktifkan</button>
													</form><?php
													break;
												case 1: 
													if($data['KD_KAT_PENOMORAN'] == 0){ #SURAT TANPA NOMOR ?>
														<a class="btn btn-default btn-small" title="Final" data-rel="tooltip" data-id_surat_keluar="<?php echo $data['ID_SURAT_KELUAR'];?>" onclick="kesshou($(this));">
															Final
														</a><?php
													}else{ ?>
														<a class="btn btn-default btn-small" title="Beri Nomor" data-rel="tooltip" data-id_surat_keluar="<?php echo (string)$data['ID_SURAT_KELUAR'];?>" onClick="berinomor($(this));">
															Beri Nomor
														</a><?php
													}
													?>
													<a class="btn btn-default btn-small" title="Hapus Data" data-rel="tooltip" data-id_surat_keluar="<?php echo $data['ID_SURAT_KELUAR'];?>" onClick="daleteSuratKeluar($(this));">
														Hapus
													</a><?php
													break;
												case 2: ?>
													<a class="btn btn-default btn-small" title="Final" data-rel="tooltip" data-id_surat_keluar="<?php echo $data['ID_SURAT_KELUAR'];?>" onclick="kesshou($(this));">
														Final
													</a>
													<a class="btn btn-default btn-small" title="Hapus Data" data-rel="tooltip" data-id_surat_keluar="<?php echo $data['ID_SURAT_KELUAR'];?>" onclick="kyanseru($(this));">
														Batalkan
													</a><?php
													break;
											}
										}
										?>
									</td>
								</tr><?php
							}
						}else{ ?>
							<tr class="error centered"><td colspan="6">Tidak ada surat keluar yang cocok dengan kata kunci <b>"<?php echo back_cari($this->uri->segment(4));?>"</b></td></tr><?php
						}
						?>			
				  </tbody>
			 </table>
		 </form>
		 <?php
		 if(ISSET($links)){
			echo $links;
		 }
		 ?>
		 <div class="info_table">
			<table>
				<tr><td colspan="2"><b>Keterangan</b></td></tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr>
					<td><div class="persegi bg_error"></div></td>
					<td>&nbsp; : Surat <b>dibatalkan</b>.</td>
				</tr>
				<tr>
					<td style="padding-top:5px"><div class="persegi bg_success"></div></td>
					<td>&nbsp; : Surat telah <b>ditanda tangani</b>.</td>
				</tr>
				<tr>
					<td style="padding-top:5px"><div class="persegi"></div></td>
					<td>&nbsp; : Surat <b>belum</b> ditanda tangani</b>.</td>
				</tr>
			</table>
		 </div>
		 <br/>
	</div>
</div>
<div class="modal fade" id="modalDeleteSuratKeluar">
  <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><i class="icon-remove"></i></button>
				<div class="contenttitle">
					<h3>Konfirmasi</h3>
				</div>
			</div>
			<div class="modal-body">
				<p><!--<img class="modal-icon" src="img/trash.png" />--> Apakah Anda yakin ingin menghapus data ini ?</p>
			</div>
			<div class="modal-footer">
				<form action="<?php echo base_url();?>pegawai/surat_keluar" method="POST" >
					<a href="#" class="btn btn-default rec" data-dismiss="modal">Tidak</a>
					<input type="hidden" id="id_surat_keluar" name="id_surat_keluar" value="" />
					<input type="hidden" name="del-sk" value="Delete Surat Keluar" />
					<input type="submit" class="btn btn-danger" value="Ya" />
				</form>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modalBerinomor">
  <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><i class="icon-remove"></i></button>
				<div class="contenttitle">
					<h3>Konfirmasi</h3>
				</div>
			</div>
			<div class="modal-body">
				<p>Surat yang telah diberi nomor tidak dapat dihapus. Anda yakin ingin memberi nomor surat pada surat ini?</p>
			</div>
			<div class="modal-footer">
				<form action="<?php echo base_url();?>pegawai/surat_keluar" method="POST" >
					<a href="#" class="btn btn-default rec" data-dismiss="modal">Tidak</a>
					<input type="hidden" id="id_sk_berinomor" name="id_surat_keluar" value="" />
					<input type="hidden" name="berinomor" value="1" />
					<input type="submit" class="btn btn-danger" value="Ya" />
				</form>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modalFinal">
  <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><i class="icon-remove"></i></button>
				<div class="contenttitle">
					<h3>Konfirmasi</h3>
				</div>
			</div>
			<div class="modal-body">
				<p>Apakah Anda yakin surat ini telah ditanda tangani?</p>
				<p>Jika surat ini telah ditanda tangani. Silahkan pilih tingkat keamanan surat, kemudian klik "Ya". Jika surat belum ditanda tangani klik "Tidak".</p>
			</div>
			<div class="modal-footer">
				<form action="<?php echo base_url();?>pegawai/act_surat_keluar/set_final" method="POST" >
					<div style="float:left">
						Tingkat Keamanan Surat : 
						<select name="kd_keamanan_surat">
							<option value="">-- Pilih Tingkat Keamanan Surat --</option>
							<?php
							if(!empty($tingkat_keamanan)){
								foreach($tingkat_keamanan as $val){ ?>
									<option value="<?php echo $val['KD_KEAMANAN_SURAT'];?>"><?php echo $val['NM_KEAMANAN_SURAT'];?></option><?php
								}
							}
							?>
						</select>
					</div>
					<a href="#" class="btn btn-default rec" data-dismiss="modal">Tidak</a>
					<input type="hidden" id="id_sk_final" name="id_surat_keluar" value="" />
					<input type="hidden" name="kesshou" value="1" />
					<input type="submit" class="btn btn-danger" value="Ya" />
				</form>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modalBatalkan">
  <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><i class="icon-remove"></i></button>
				<div class="contenttitle">
					<h3>Konfirmasi</h3>
				</div>
			</div>
			<div class="modal-body">
				<p>Apakah Anda yakin ingin membatalkan surat ini.?</p>
			</div>
			<div class="modal-footer">
				<form action="<?php echo base_url();?>pegawai/surat_keluar" method="POST" >
					<a href="#" class="btn btn-default rec" data-dismiss="modal">Tidak</a>
					<input type="hidden" id="id_sk_batalkan" name="id_surat_keluar" value="" />
					<input type="hidden" name="kyanseru" value="1" />
					<input type="submit" class="btn btn-danger" value="Ya" />
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">		
	function daleteSuratKeluar(el){
		var id_surat_keluar = el.data('id_surat_keluar');
		document.getElementById('id_surat_keluar').value = id_surat_keluar;
		$('#modalDeleteSuratKeluar').modal('show');
	}
	
	function berinomor(el){
		var id_surat_keluar = el.data('id_surat_keluar');
		document.getElementById('id_sk_berinomor').value = id_surat_keluar;
		$('#modalBerinomor').modal('show');
	}	
	
	function kesshou(el){
		var id_surat_keluar = el.data('id_surat_keluar');
		document.getElementById('id_sk_final').value = id_surat_keluar;
		$('#modalFinal').modal('show');
	}	
	
	function kyanseru(el){
		var id_surat_keluar = el.data('id_surat_keluar');
		document.getElementById('id_sk_batalkan').value = id_surat_keluar;
		$('#modalBatalkan').modal('show');
	}
</script>
<script type="text/javascript">
	function addEvent(obj, evType, fn){
		if(obj.addEventListener){
			obj.addEventListener(evType, fn, false);
			return true;
		}else if(obj.attachEvent){
			var r = obj.attachEvent("on"+evType, fn);
			return r;
		}else{
			return false;
		}
	}
	
	addEvent(window, 'load', initCheckboxes);
	
	function initCheckboxes(){
		addEvent(document.getElementById('checkall'), 'click', setCheckboxes);
	}
	
	function setCheckboxes(){
		var cb = document.getElementById('content-check').getElementsByTagName('input');
		
		for(var i = 0; i < cb.length; i++){
			cb[i].checked = document.getElementById('checkall').checked;
		}
	}
</script>
<script type="text/javascript">
$('select').on('change', function() {
  window.location="<?php echo base_url();?>pegawai/surat_keluar/select_mode/"+this.value;
});
</script>