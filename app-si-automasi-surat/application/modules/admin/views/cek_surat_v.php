<div class="col-med-9">
	<div class="content-space">
		<ul id="crumbs">
			<li>
				<a title="Surat" href="#">Surat</a>
			</li>
			<li>
				<a title="Cek Surat" href="#">Cek Surat</a>
			</li>
		</ul><br/>
		<div class="overviewhead" style="height:48px">
			<div class="cari">
				<div class="right">
					<form action="<?php echo base_url();?>surat/cek_surat" method="post" style="margin-top:-10px;margin-right:-10px;color:#727272">
						Kata kunci &nbsp;
						<input type="text" name="q" class="wajib" style="width:400px" placeholder="Ketikkan kata kunci" value="<?php echo str_replace("_", " ", urldecode($this->uri->segment(3)));?>"/>
						<input type="hidden" name="act" value="cari"/>
						<button type="submit" class="btn btn-default btn-small" style="border-radius:0;padding:5px;">
							Cari
						</button>
					</form>
				</div>
			</div>
		</div>
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th width="4%"><center>No</center></th>
					<th width="18%"><center>Nomor Surat</center></th>
					<th width="8%"><center>Tanggal Surat</center></th>
					<th><center>Perihal</center></th>
					<th width="8%"><center>Aksi</center></th>
				</tr>
			</thead>   
			<tbody id="content-check">
				<?php
				if(!empty($cari)){
					if(!empty($surat)){
						$uri = conv_to_int($this->uri->segment(4));
						if(!empty($uri)){
							$n = $uri + 1;;
						}else{
							$n = 1;
						}
						foreach($surat as $val){ ?>
							<tr>
								<td><?php echo $n;?></td>
								<td><?php echo $val['NO_SURAT'];?></td>
								<td><?php echo $val['TGL_SURAT'];?></td>
								<td><?php echo $val['PERIHAL'];?></td>
								<td>
									<center>
										<a class="btn btn-default btn-small" title="Lihat" data-rel="tooltip" data-kd_jenis="<?php echo $val['G_JENIS'];?>" data-id_surat="<?php echo $val['G_ID_SURAT'];?>" onclick="detail($(this));">
											Lihat
										</a>
									</center>
								</td>
							</tr><?php
							$n++;
						}
					}else{ ?>
						<tr class="error"><td colspan="6">Tidak ada surat yang cocok dengan kata kunci <b>"<?php echo str_replace("_", " ", urldecode($this->uri->segment(3)));?>"</b></td></tr><?php
					}
				}
				?>
			</tbody>
		</table>
		 <?php
		if(ISSET($links)){
			echo $links;
		}
		?>
		<div id="content_result">
			<div id="result">&nbsp;</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$("content_result").hide();
	});
</script>
<script type="text/javascript">
	function cek_surat(el){
		id_surat_masuk = el.data('id_surat_masuk');
		$.post("<?php echo base_url();?>ajax/cek_surat", {
			id_surat_masuk: id_surat_masuk,
		}, function(response){
			setTimeout("finishAjax('result', '"+escape(response)+"')", 400);
		});
	}
	
	function finishAjax(id, response){
		$("#content_result").show();
		$("#"+id).html(unescape(response));
		$("#"+id).fadeIn();
	}
</script>
<script type="text/javascript">
	function detail(el){
		var id_surat = el.data('id_surat');
		var kd_jenis = el.data('kd_jenis');
		$.ajax({
				url : "<?php echo base_url() ?>ajax/detail_global_surat",
				type: "POST",                  
				beforeSend: function(){
					$("#result").html(        '<div id="separate"></div>'+
															'<center><img src="http://akademik.uin-suka.ac.id/asset/img/loading.gif"></center>'+
															'<div id="separate"></div>'+
															'<center><font size="2px">Harap menunggu</font></center>');
					$("html, body").animate({ scrollTop: $("#result").offset().top }, 1000);
				},
				data    : {id_surat: id_surat, kd_jenis: kd_jenis},                
				success: function(html){                       
						$("#result").html(html);
				}
		});
	}

</script>