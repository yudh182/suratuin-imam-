<div class="monitoring_content">	
	<h3 style="color:#333333">Pengolah Surat</h3>
	<table class="table table-bordered table-hover">
		<tbody>
			<tr>
				<th width="26px">No</th><th>KD PEGAWAI / NIP</th><th>Jabatan</th><th>Nama Pegawai</th><th>Sebagai</th><th>Status Akses</th><th>Waktu Akses</th>
			</tr>
			<tr>
				<td>1</td><td><?php echo $surat_masuk['PENCATAT_SURAT'];?></td>
				<td><?php echo (!empty($simpeg_arsip[0]['STR_NAMA']) ? $simpeg_arsip[0]['STR_NAMA'] : $arsip['NM_JABATAN']);?></td>
				<td><?php echo (!empty($simpeg_arsip[0]['NM_PGW_F']) ? $simpeg_arsip[0]['NM_PGW_F'] : $arsip['NM_PEGAWAI']);?></td>
				<td>ARSIP</td><td><span class="label label-success">TELAH DIPROSES</span></td>
				<td>
					<?php
					if(!empty($surat_masuk['WAKTU_PENCATATAN'])){
						echo $surat_masuk['WAKTU_PENCATATAN']." WIB";
					}
					?>
				</td>
			</tr>
			<?php
			if(ISSET($surat_masuk['PENGARAH_SURAT'])){ ?>
				<tr>
					<td>2</td><td><?php echo $surat_masuk['PENGARAH_SURAT'];?></td>
					<td><?php echo (!empty($simpeg_pengarah[0]['STR_NAMA']) ? $simpeg_pengarah[0]['STR_NAMA'] : $pengarah['NM_JABATAN']);?></td>
					<td><?php echo (!empty($simpeg_pengarah[0]['NM_PGW_F']) ? $simpeg_pengarah[0]['NM_PGW_F'] : $pengarah['NM_PEGAWAI']);?></td>
					<td>PENGARAH SURAT</td><td><span class="label label-<?php if($surat_masuk['ID_STATUS_SM']==1){ echo "danger";}else if($surat_masuk['ID_STATUS_SM']==2){ echo "warning";}else{ echo "success";}?>"><?php echo $surat_masuk['NM_STATUS_SM'];?></span></td>
					<td>
						<?php
						if(!empty($surat_masuk['WAKTU_AKSES'])){
							echo $surat_masuk['WAKTU_AKSES']." WIB";
						}
						?>
					</td>
				</tr>
				<?php
			}
			if(ISSET($tembusan)){
				$n = 2;
				foreach($tembusan as $key => $data){
					$n = $n + 1; ?>
					<tr>
						<td><?php echo $n;?></td><td><?php echo $data['PENERIMA_SURAT'];?></td>
						<td><?php echo (!empty($jabatan_tembusan[$key]) ? $jabatan_tembusan[$key] : $data['NM_JABATAN']);?></td>
						<td><?php echo (!empty($simpeg_tembusan[$key]['NM_PGW_F']) ? $simpeg_tembusan[$key]['NM_PGW_F'] : $data['NM_PEGAWAI']);?></td>
						<td>TEMBUSAN SURAT</td><td><span class="label label-<?php if($data['ID_STATUS_SM']==1){ echo "danger";}else if($data['ID_STATUS_SM']==2){ echo "warning";}else{ echo "success";}?>"><?php echo $data['NM_STATUS_SM'];?></span></td>
						<td>
							<?php
							if(!empty($data['WAKTU_AKSES'])){
								echo $data['WAKTU_AKSES']." WIB";
							}
							?>
						</td>
					</tr>
					<?php
				}
			}
			if(ISSET($pengolah)){
				foreach($pengolah as $key => $data){ 
					$n = $n + 1; ?>
					<tr>
						<td><?php echo $n;?></td><td><?php echo $data['PENERIMA_SURAT'];?></td>
						<td><?php echo (!empty($jabatan_pengolah[$key]) ? $jabatan_pengolah[$key] : $data['NM_JABATAN']);?></td>
						<td><?php echo (!empty($simpeg_pengolah[$key]['NM_PGW_F']) ? $simpeg_pengolah[$key]['NM_PGW_F'] : $data['NM_PEGAWAI']);?></td>
						<td>PENGOLAH SURAT</td><td><span class="label label-<?php if($data['ID_STATUS_SM']==1){ echo "danger";}else if($data['ID_STATUS_SM']==2){ echo "warning";}else{ echo "success";}?>"><?php echo $data['NM_STATUS_SM'];?></span></td>
						<td>
							<?php
							if(!empty($data['WAKTU_AKSES'])){
								echo $data['WAKTU_AKSES']." WIB";
							}
							?>
						</td>
					</tr>
					<?php
				}
			}
			?>
		</tbody>
	</table>
	<h3>Disposisi/Terusan/Memo/Nota Dinas</h3>
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th width="26px">No</th><th width="12%">Waktu Kirim</th><th>Dari</th><th>Kepada</th><th width="17%">Status</th><th width="12%">Waktu Akses</th>
			</tr>
		</thead>
		<tbody>
		<?php
		if(ISSET($disposisi)){
			$n = 1;
			foreach($disposisi as $key => $data){ ?>
				<tr>
					<td><?php echo $n;?></td><td><?php echo $data[0]['WAKTU_KIRIM'];?></td>
					<td><?php echo (!empty($simpeg_disposisi[$key][1]['NM_PEMROSES']) ? $simpeg_disposisi[$key][1]['NM_PEMROSES'] : $data[1]['NM_PEMROSES'])."<br/><hr/>".(!empty($jabatan_pemroses[$key]) ? $jabatan_pemroses[$key] : $data[1]['NM_JABATAN_PEMROSES'])."<br/><hr/>".$data[0]['PEMROSES_SURAT'];?></td>
					<td><?php echo (!empty($simpeg_disposisi[$key][1]['NM_PENERIMA']) ? $simpeg_disposisi[$key][1]['NM_PENERIMA'] : $data[1]['NM_PENERIMA'])."<br/><hr/>".(!empty($jabatan_penerima[$key]) ? $jabatan_penerima[$key] : $data[1]['NM_JABATAN_PENERIMA'])."<br/><hr/>".$data[0]['PENERIMA_SURAT'];?></td>
					<td><span class="label label-<?php if($data[0]['ID_STATUS_SM']==1){ echo "danger";}else if($data[0]['ID_STATUS_SM']==2){ echo "warning";}else{ echo "success";}?>"><?php echo $data[0]['NM_STATUS_SM'];?></span></td>
					<td>
						<?php
						if(!empty($data[0]['WAKTU_AKSES'])){
							echo $data[0]['WAKTU_AKSES']." WIB";
						}
						?>
					</td>
				</tr><?php
				$n++;
			}
		}
		?>
		</tbody>
	</table>
</div>