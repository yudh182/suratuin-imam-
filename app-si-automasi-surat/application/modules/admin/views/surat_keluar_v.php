<div class="col-med-9">
	<div class="content-space">
		<ul id="crumbs">
			<li>
				<a title="Surat Keluar" href="#">Surat Keluar</a>
			</li>
			<li>
				<a title="Arsip Surat Keluar" href="#">Arsip Surat Keluar</a>
			</li>
		</ul><br/>
			<?php
			$sess_errors = $this->session->flashdata('errors');
			if((!empty($errors))||(!empty($sess_errors))){ ?>
				<div class="bs-callout bs-callout-error" style="margin-bottom:5px">
					<?php
					if(!empty($errors)){
						if(is_array($errors)){
							foreach($errors as $value){
								echo "<p>- ".$value."</p>";
							}
						}else{
							echo $errors;
						}
					}
					
					if(!empty($sess_errors)){
						if(is_array($sess_errors)){
							foreach($sess_errors as $value){
								echo "<p>- ".$value."</p>";
							}
						}else{
							echo $sess_errors;
						}
					}
					
					$ruang_tidak_tersedia	= $this->session->flashdata('ruang_tidak_tersedia');
					$peminjam_ruang 			= $this->session->flashdata('peminjam_ruang');
					if(!empty($ruang_tidak_tersedia)){
						echo "<br/>";
						echo "<b>Berikut Ruangan yang telah terpakai.</b><br/>";
						echo "<table class='white'>";
						foreach($ruang_tidak_tersedia as $key => $val){
							echo "<tr><td>Nama Ruang</td><td> : ".$val['NM_RUANG']."</td></tr>";
							echo "<tr><td>Keperluan</td><td> : ".$val['KEPERLUAN']."</td></tr>";
							echo "<tr><td>Tanggal Mulai</td><td> : ".$val['TGL_MULAI']."</td></tr>";
							echo "<tr><td>Tanggal Selesai</td><td> : ".$val['TGL_SELESAI']."</td></tr>";
							echo "<tr><td>Nama Peminjam</td><td> : ".(!empty($peminjam_ruang[$key]['NM_PGW_F']) ? $peminjam_ruang[$key]['NM_PGW_F'] : '')."</td></tr>";
							echo "<tr><td>NIP/NIM</td><td> : ".nip_pgw($val['NIM'])."</td></tr>";
							echo "<tr><td>Telepon</td><td> : ".(!empty($peminjam_ruang[$key]['TELEPON_HP1']) ? $peminjam_ruang[$key]['TELEPON_HP1'] : '-')."</td></tr>";
							echo "<tr><td colspan='2'><hr style='width:100%;color:#fff'/></td>";
						}
						echo "</table><br/>";
						echo "Silahkan hubungi peminjam untuk konfirmasi.";
					}
					?>
				</div><?php
			}
			$sess_success	= $this->session->flashdata('success');
			if((!empty($success))||(!empty($sess_success))){  ?>
				<div class="bs-callout bs-callout-success" style="margin-bottom:5px">
					<?php
					if(!empty($success)){
						if(is_array($success)){
							foreach($success as $value){
								echo "<p>- ".$value."</p>";
							}
						}else{
							echo $success;
						}
					}
					if(!empty($sess_success)){
						if(is_array($sess_success)){
							foreach($sess_success as $value){
								echo "<p>- ".$value."</p>";
							}
						}else{
							echo $sess_success;
						}
					}
					?>
				</div><?php
			}
			
			$bentrok = $this->session->flashdata('bentrok');
			if(!empty($bentrok)){ ?>
				<div class="bs-callout bs-callout-error" style="margin-bottom:5px">
					<b>Mungkin beberapa peserta tidak dapat menghadiri undangan dikarenakan jadwal agenda bersamaan dengan agenda yang lain. Berikut beberapa peserta yang memiliki agenda lain.</b><br/>
					<?php
					echo '<table class="white">';
					// $n = 1;
					foreach($bentrok as $kd_pgw => $val){
						if(!empty($val['TA'])){
							$nm_pgw	= (!empty($val['pegawai']['NM_PGW_F']) ? $val['pegawai']['NM_PGW_F'] : '');
							echo '<tr><td valign="top" width="20px">- </td><td>Peserta <b>'.$nm_pgw.'</b> memiliki agenda <b>'.$val['TA']['ACARA'].'</b> pada tanggal <b>'.$val['TA']['TGL_MUNA'].' '.$val['TA']['JAM_MULAI'].' s/d '.$val['TA']['TGL_MUNA'].' '.$val['TA']['JAM_SELESAI'].' WIB</b></td></tr>';
						}
					}
					echo '</table><br/>';
					?>
				</div><?php
			}
			?>
		<div class="cari">
			<div class="right">
				<form action="<?php echo base_url();?>pegawai/surat_keluar/cari" method="POST">
					<input type="text" name="perihal" placeholder="Cari Surat Keluar" style="width:160px;margin-top:0px" />
					<input type="hidden" name="qsk" value="true" />
					<button type="submit" class="btn btn-default btn-small" style="border-radius:0;padding:5px;margin-top:0px">
						Cari
					</button>
				</form>
			</div>
		</div>
		<form action="<?php echo base_url();?>pegawai/surat_keluar" method="POST">
			<div class="overviewhead" style="height:48px">
				<input type="hidden" name="del_sk_all" value="surat masuk" />
				<!--<button class="btn btn-small" href="#" onClick="return confirm('Anda yakin ingin menghapus data surat keluar secara permanen ?');">
					<i class="icon-trash"></i>
					Delete
				</button>-->
			</div>
			<table class="table table-bordered table-hover">
				  <thead>
					  <tr>
						  <th width="20px"><center>No</center></th>
						  <th width="140px"><center>Nomor Surat</center></th>
						  <th width="72px"><center>Tanggal Surat</center></th>
						  <!--<th width="16%"><center>Kepada</center></th>-->
						  <th width="100px"><center>Jenis Surat</center></th>
						  <th><center>Perihal</center></th>
						  <th width="100px"><center>Aksi</center></th>
					  </tr>
				  </thead>   
				  <tbody id="content-check">
						<?php
						$url = $this->uri->segment(4);
						if(ISSET($url)){
							$i = $url;
						}else{
							$i = 0;
						}
						if(!empty($surat_keluar)){
							foreach($surat_keluar as $key => $data){
								$i = $i + 1; 
								switch($data['KD_STATUS_SIMPAN']){
									case 0:
										$status = 'danger';
										break;
									case 3:
										$status = 'success';
										break;
									default:
										$status = '';
										break;
								} ?>
								<tr class="<?php echo $status;?>">
									<td class="center"><?php echo $i;?></td>
									<td class="center"><?php echo $data['NO_SURAT'];?></td>
									<td class="center">
										<?php echo $data['TGL_SURAT'];?>
										<hr/>
										<?php echo (!empty($untuk_unit[$key]['UNIT_NAMA']) ? $untuk_unit[$key]['UNIT_NAMA'] : ''); ?>
									</td>
									<td class="center"><?php echo $data['NM_JENIS_SURAT'];?></td>
									<td class="center">
										<?php echo $data['PERIHAL'];?>
									</td>
									<td>
										<?php
										$keamanan = array('R', 'SR');
										if(in_array('SPR', $modules)){
											$aksi = TRUE;
										}else{
											if(in_array($data['KD_KEAMANAN_SURAT'], $keamanan)){
												if($data['PEMBUAT_SURAT'] == $this->session->userdata('kd_pgw')){
													$aksi = TRUE;
												}else{
													$aksi = FALSE;
												}
											}else{
													$aksi = TRUE;
											}
										}
										
										if($aksi == TRUE){ ?>
											<a class="btn btn-default btn-small" href="<?php echo base_url();?>pegawai/surat_keluar/detail/<?php echo $data['ID_SURAT_KELUAR'];?>" title="Lihat & Edit Data" data-rel="tooltip">
												Lihat
											</a>
											<?php
											switch($data['KD_STATUS_SIMPAN']){
												case 0: ?>
													<form action="<?php echo base_url();?>pegawai/surat_keluar" method="POST">
														<input type="hidden" name="id_surat_keluar" value="<?php echo $data['ID_SURAT_KELUAR'];?>" />
														<input type="hidden" name="suitchi" value="1" />
														<button class="btn btn-default btn-small" title="Aktifkan"><i class="icon-star"></i> Aktifkan</button>
													</form><?php
													break;
												case 1: 
													if($data['KD_KAT_PENOMORAN'] == 0){ #SURAT TANPA NOMOR ?>
														<a class="btn btn-default btn-small" title="Final" data-rel="tooltip" data-id_surat_keluar="<?php echo $data['ID_SURAT_KELUAR'];?>" onclick="kesshou($(this));">
															Final
														</a><?php
													}else{ ?>
														<a class="btn btn-default btn-small" title="Beri Nomor" data-rel="tooltip" data-id_surat_keluar="<?php echo (string)$data['ID_SURAT_KELUAR'];?>" onClick="berinomor($(this));">
															Beri Nomor
														</a><?php
													}
													?>
													<a class="btn btn-default btn-small" title="Hapus Data" data-rel="tooltip" data-id_surat_keluar="<?php echo $data['ID_SURAT_KELUAR'];?>" onClick="daleteSuratKeluar($(this));">
														Hapus
													</a><?php
													break;
												case 2: ?>
													<a class="btn btn-default btn-small" title="Final" data-rel="tooltip" data-id_surat_keluar="<?php echo $data['ID_SURAT_KELUAR'];?>" onclick="kesshou($(this));">
														Final
													</a>
													<a class="btn btn-default btn-small" title="Hapus Data" data-rel="tooltip" data-id_surat_keluar="<?php echo $data['ID_SURAT_KELUAR'];?>" onclick="kyanseru($(this));">
														Batalkan
													</a><?php
													break;
											}
										}
										?>
									</td>
								</tr><?php
							}
						}else{ ?>
							<tr class="error centered"><td colspan="6">Belum ada surat keluar yang disimpan</td></tr><?php
						}
						?>			
				  </tbody>
			 </table>
			 <br/>
		 </form>
		 <?php
		 if(ISSET($links)){
			echo $links;
		 }
		 ?>
		 <div class="info_table">
			<table>
				<tr><td colspan="2"><b>Keterangan</b></td></tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr>
					<td><div class="persegi bg_error"></div></td>
					<td>&nbsp; : Surat <b>dibatalkan</b>.</td>
				</tr>
				<tr>
					<td style="padding-top:5px"><div class="persegi bg_success"></div></td>
					<td>&nbsp; : Surat telah <b>ditanda tangani</b>.</td>
				</tr>
				<tr>
					<td style="padding-top:5px"><div class="persegi"></div></td>
					<td>&nbsp; : Surat <b>belum</b> ditanda tangani</b>.</td>
				</tr>
			</table>
		 </div>
		 <br/>
	</div>
</div>
<div class="modal fade" id="modalDeleteSuratKeluar">
  <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><i class="icon-remove"></i></button>
				<div class="contenttitle">
					<h3>Konfirmasi</h3>
				</div>
			</div>
			<div class="modal-body">
				<p><!--<img class="modal-icon" src="img/trash.png" />--> Apakah Anda yakin ingin menghapus data ini ?</p>
			</div>
			<div class="modal-footer">
			
				<form action="<?php echo base_url('pegawai/act_surat_keluar/del_sk');?>" method="POST" >
					<a href="#" class="btn btn-default rec" data-dismiss="modal">Tidak</a>
					<input type="hidden" id="id_surat_keluar" name="id_surat_keluar" value="" />
					<input type="hidden" name="del-sk" value="Delete Surat Keluar" />
					<input type="submit" class="btn btn-danger" value="Ya" />
				</form>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modalBerinomor">
  <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><i class="icon-remove"></i></button>
				<div class="contenttitle">
					<h3>Konfirmasi</h3>
				</div>
			</div>
			<div class="modal-body">
				<p>Surat yang telah diberi nomor tidak dapat dihapus. Anda yakin ingin memberi nomor surat pada surat ini?</p>
			</div>
			<div class="modal-footer">
				<form action="<?php echo base_url();?>pegawai/surat_keluar" method="POST" >
					<a href="#" class="btn btn-default rec" data-dismiss="modal">Tidak</a>
					<input type="hidden" id="id_sk_berinomor" name="id_surat_keluar" value="" />
					<input type="hidden" name="berinomor" value="1" />
					<input type="submit" class="btn btn-danger" value="Ya" />
				</form>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modalFinal">
  <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><i class="icon-remove"></i></button>
				<div class="contenttitle">
					<h3>Konfirmasi</h3>
				</div>
			</div>
			<div class="modal-body">
				<p>Apakah Anda yakin surat ini telah ditanda tangani?</p>
				<p>Jika surat ini telah ditanda tangani. Silahkan pilih tingkat keamanan surat, kemudian klik "Ya". Jika surat belum ditanda tangani klik "Tidak".</p>
			</div>
			<div class="modal-footer">
				<form action="<?php echo base_url('pegawai/act_surat_keluar/set_final');?>" method="POST" >
					<div style="float:left">
						Tingkat Keamanan Surat : 
						<select name="kd_keamanan_surat">
							<option value="">-- Pilih Tingkat Keamanan Surat --</option>
							<?php
							if(!empty($tingkat_keamanan)){
								foreach($tingkat_keamanan as $val){ ?>
									<option value="<?php echo $val['KD_KEAMANAN_SURAT'];?>"><?php echo $val['NM_KEAMANAN_SURAT'];?></option><?php
								}
							}
							?>
						</select>
					</div>
					<a href="#" class="btn btn-default rec" data-dismiss="modal">Tidak</a>
					<input type="hidden" id="id_sk_final" name="id_surat_keluar" value="" />
					<input type="hidden" name="kesshou" value="1" />
					<input type="submit" class="btn btn-danger" value="Ya" />
				</form>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modalBatalkan">
  <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><i class="icon-remove"></i></button>
				<div class="contenttitle">
					<h3>Konfirmasi</h3>
				</div>
			</div>
			<div class="modal-body">
				<p>Apakah Anda yakin ingin membatalkan surat ini.?</p>
			</div>
			<div class="modal-footer">
				<form action="<?php echo base_url();?>pegawai/surat_keluar" method="POST" >
					<a href="#" class="btn btn-default rec" data-dismiss="modal">Tidak</a>
					<input type="hidden" id="id_sk_batalkan" name="id_surat_keluar" value="" />
					<input type="hidden" name="kyanseru" value="1" />
					<input type="submit" class="btn btn-danger" value="Ya" />
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">		
	function daleteSuratKeluar(el){
		var id_surat_keluar = el.data('id_surat_keluar');
		document.getElementById('id_surat_keluar').value = id_surat_keluar;
		$('#modalDeleteSuratKeluar').modal('show');
	}
	
	function berinomor(el){
		var id_surat_keluar = el.data('id_surat_keluar');
		var id_sk_string = id_surat_keluar;
		document.getElementById('id_sk_berinomor').value = id_sk_string;
		$('#modalBerinomor').modal('show');
	}	
	
	function kesshou(el){
		var id_surat_keluar = el.data('id_surat_keluar');
		document.getElementById('id_sk_final').value = id_surat_keluar;
		$('#modalFinal').modal('show');
	}	
	
	function kyanseru(el){
		var id_surat_keluar = el.data('id_surat_keluar');
		document.getElementById('id_sk_batalkan').value = id_surat_keluar;
		$('#modalBatalkan').modal('show');
	}
</script>
<script type="text/javascript">
	function addEvent(obj, evType, fn){
		if(obj.addEventListener){
			obj.addEventListener(evType, fn, false);
			return true;
		}else if(obj.attachEvent){
			var r = obj.attachEvent("on"+evType, fn);
			return r;
		}else{
			return false;
		}
	}
	
	addEvent(window, 'load', initCheckboxes);
	
	function initCheckboxes(){
		addEvent(document.getElementById('checkall'), 'click', setCheckboxes);
	}
	
	function setCheckboxes(){
		var cb = document.getElementById('content-check').getElementsByTagName('input');
		
		for(var i = 0; i < cb.length; i++){
			cb[i].checked = document.getElementById('checkall').checked;
		}
	}
</script>
