<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');
class Master extends MY_Controller{
	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('log') !== 'in' && $this->session->userdata('username') !== '197905062006041003')
			redirect(site_url('auth/login?from='.current_url()));

		$this->load->helper('common/Trans_general');
		$this->load->module('repo/Api_foto','api_foto');
		$this->load->module('repo/Api_client_tnde','api_client_tnde');
	}

	public function index()
	{
		echo "at least, i am exist at master page";
	}	

	public function jenis($page='index')
	{
		$this->load->model('pgw/Verifikasi_model');
		$this->load->helper('pgw/Tnde_sakad');

		if ($page=='index'){
			$parameter =array('api_kode'		=> 13001, 'api_subkode'	=> 1, 'api_search'	=> array());
			$data['jenis_sakad'] = $this->apiconn->api_tnde_sakad('tnde_master_mix/get_jenis_surat_akademik', 'json', 'POST', $parameter);
			
			$this->load->view('layout-admin/v_header');
			$this->load->view('layout-admin/v_sidebar', $data);
			$this->load->view('v_master/v_jenis_sakad', $data);
			$this->load->view('layout-admin/v_footer');
		} elseif ($page=='detail') {
			$parameter =array('api_kode'		=> 13001, 'api_subkode'	=> 3, 'api_search'	=> array($this->uri->segment(5)));
			$jenis_sakad = $this->apiconn->api_tnde_sakad('tnde_master_mix/get_jenis_surat_akademik', 'json', 'POST', $parameter);
			$data['jenis_sakad'] = $jenis_sakad[0];

			$parameter = array(
	            'api_kode' => 15003, 'api_subkode' => 1, 
	            'api_search' => array(
	                'CONFIG_AUTOMASI_SAKAD',
	                array('KD_JENIS_SAKAD'=>(int)$data['jenis_sakad']['KD_JENIS_SAKAD'] ),
	                'ID ASC'
            ));        	
			$data['conf_auto'] = $this->apiconn->api_tnde_sakad('surat_general/get_data', 'json', 'POST', $parameter);
			$current_persyaratan = $this->Verifikasi_model->get_persyaratan_by_kd($this->uri->segment(5));
			$data['current_persyaratan'] = $current_persyaratan['data'];
			
			$data['master_persyaratan'] = $this->apiconn->api_tnde_sakad('tnde_master_mix/get_master_persyaratan', 'json', 'POST', 
				[
					'api_kode'=>13001,'api_subkode'=>1,'api_search'=>array()
				]
			);
			//exit(var_dump($data));						
			$this->load->view('layout-admin/v_header');
			$this->load->view('layout-admin/v_sidebar', $data);
			$this->load->view('v_master/v_detail_jenis_sakad', $data);
			$this->load->view('layout-admin/v_footer');
		} else {
			//exit('404 page');
			show_404();
		}
	}

	public function repository_persyaratan($page='index')
	{}

	public function repository_config_automasi($page='index')
	{}

	public function tes()
	{
		$arr = [
			'status' => true,
			'pesan'		=> 'input berhasil'
		];

		echo print_r($arr);
	}

	public function action_perbarui_persyaratan_sakad()
	{	
		$referer = (!empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '');
		$auth = host_allowed($referer);
		if(($this->input->post('btn-perbarui-persyaratan') != null) && $auth){
			$kd_sub_jenis_surat = $this->input->post('kd_sjs_syarat');
			$persyaratan_terpilih = $this->input->post('update_persyaratan');

			if ($persyaratan_terpilih !== NULL){ //pastikan ada isinya
				$data_syarat = serialize($persyaratan_terpilih);
			} else { //jika tidak ada syarat dipilih
				$data_syarat = '';
			}
			

			$apirequest = $this->repo_mtrsakad->update_persyaratan_sub_jenis_surat($kd_sub_jenis_surat, $data_syarat);

			//exit(var_dump($apirequest));
			if ($apirequest !== NULL && $apirequest == true){
				//var_dump($apirequest);
				$this->session->set_flashdata('success', 'berhasil memperbarui persyaratan untuk sub jenis surat ini');
				redirect($_SERVER['HTTP_REFERER']);
			} else {
				$this->session->set_flashdata('errors', 'Gagal memperbarui persyaratan untuk sub jenis surat ini');
				redirect($_SERVER['HTTP_REFERER']);				
			}
		}
	}	

	public function uji_tanggalan_sistem()
	{
		// old way
		//date_default_timezone_set("Asia/Jakarta");
		//echo date('Y-m-d H:i:s',time());

		// new proper way (jarene..)
		$dt = new DateTime();		
		echo $dt->format('Y-m-d H:i:s');
	}

}		