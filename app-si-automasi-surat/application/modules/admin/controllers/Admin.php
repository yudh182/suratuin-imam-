<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');
/**
* Diakses oleh Pegawai TU masing-masing unit fakultas
* @author Mukhlas Imam Muhajir <muhajiros.space@gmail.com>
*/
class Admin extends MX_Controller{
	public function
	 __construct()
	{
		parent::__construct();
		$this->load->helper('common/Trans_general');
		$this->load->module('repo/Api_foto','api_foto');
		$this->load->module('repo/Api_client_tnde','api_client_tnde');
	}

	public function index()
	{
		$view = 'v_dashboard_administrasi';

		$this->load->view('layout-admin/v_header');
		$this->load->view('layout-admin/v_sidebar');
		$this->load->view('home');
		$this->load->view('layout-admin/v_footer');
	}

}