<?php if(!defined('BASEPATH'))exit('No direct script access allowed');

class Kelola_surat extends MY_Controller {

	public function __construct()
	{
		parent::__construct();	

		$this->load->module('pgw/api/Api_foto','api_foto');
		$this->load->module('pgw/external/Akademik','akademik');
		$this->load->model('pgw/Automasi_model');
		$this->load->model('common/Api_automasi_surat','autosurat');
    }

    public function index()
    {
    	$help = [
    		'title' => 'Pengelolaan data semua transaksi persuratan oleh admin',
    		'hak_akses_dibutuhkan' => 'SRTMTR0001',
    		'halaman_tersedia' => [
    			[
    				'page' => 'surat_keluar',
    				'deskripsi' => 'Lihat, edit, atau hapus surat-surat yang telah dibuat mahasiswa',
    				'link' => site_url("admin/Kelola_surat/surat_keluar")
    			],
    			[
    				'page' => 'pengajuan',    				
    				'deskripsi' => 'Lihat, edit, atau hapus data-data pengajuan surat oleh mahasiswa ke unit fakultas',
    				'link' => site_url("admin/Kelola_surat/pengajuan")
    			],

    		]
    	];
    	echo json_encode($help);
    }


    /**
     * Arsip surat keluar semua mahasiswa, semua unit fakultas     
     * capabilities: lihat, edit, hapus
     */
    public function surat_keluar()
	{
		#0. Mengmabil data daftar jenis surat				
		$data['list_jenis_sakad'] = $this->autosurat->get_jenis_surat('aktif');
		
		#1. Mengambil data riwayat
		$nim = $this->session->userdata('username');
		$parameter	= array('search'=>$nim);
		$data['riwayat'] = $this->apiconn->api_autosurat('arsip_surat_keluar/data_search_by/nim', 'json', 'GET', $parameter);
		//echo json_encode($arsip);

		#3. Render view akhir
		$this->load->view('common/templates/v_header');
		$this->load->view('common/templates/v_sidebar', $data);
		$this->load->view('content/v_riwayat',$data);
		$this->load->view('common/templates/v_footer');
	}

	public function pengajuan()
	{

	}

	public function act_hapus_surat()
	{

	}

	public function act_perbarui_surat()
	{

	}

}



