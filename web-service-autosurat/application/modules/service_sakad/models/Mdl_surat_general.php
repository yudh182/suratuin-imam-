<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');
/**
* CRUD simpel untuk berbagai tabel
*/
class Mdl_surat_general extends CI_Model{
	
	function __construct(){
		parent::__construct();
		$this->tnde = $this->load->database('tnde',TRUE);
	}
		
	
	function count_by_table($table){
		return $this->tnde->count_all_results($table);
	}	
	
	function count_by_field_in($table, $field, $data){
		$this->tnde->where_in($field, $data);
		$this->tnde->from($table);
		return $this->tnde->count_all_results();
	}
	
	function get_data($table=''){
		$query = $this->tnde->get($table);
		$hasil = $query->result();
		
		return $hasil;
	}
	
	function get_data_by_field_array($table, $where){
		$query = $this->tnde->get_where($table, $where);
		return $query->result_array();
	}
	
	function insert_data($table, $data){
		$this->tnde->insert($table, $data);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function update_data($table, $data, $where){
		$this->tnde->update($table, $data, $where);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function delete_data($table, $where){
		$this->tnde->delete($table, $where);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}	
	
	function delete_data_in($table, $field, $in){
		$this->tnde->where_in($field, $in);
		$this->tnde->delete($table);		
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function order_data($table, $where, $order){
		$this->tnde->from($table);
		$this->tnde->where($where);
		$this->tnde->order_by(key($order), $order[key($order)]);
		$query = $this->tnde->get();
		return $query->row_array();
	}	
	
	function order_data_result($table, $where, $order){
		$this->tnde->from($table);
		$this->tnde->where($where);
		$this->tnde->order_by(key($order), $order[key($order)]);
		$query = $this->tnde->get();
		return $query->result();
	}
		
		
	
	function get_data_by_field($table,$field,$value){
		$data[$field] = $value;
		$query = $this->tnde->get_where($table,$data);
		return $query->row_array();
	}
	
	function get_data_by_field2($table,$field,$value){
		$data[$field] = $value;
		$query = $this->tnde->get_where($table,$data);
		return $query->result();
	}
	
	function get_data_by_field3($table,$data){
		$query = $this->tnde->get_where($table,$data);
		return $query->row_array();
	}
	
	function get_data_by_field_like($table, $field, $value){
		$sql = "SELECT * FROM ".$table." WHERE UPPER(".$field.") LIKE UPPER('%".$value."%')";
		return $this->tnde->query($sql)->result();
	}
	
	function get_data_by_field_in($table, $field, $value){
		$this->tnde->where_in($field, $value);
		$this->tnde->from($table);
		return $this->tnde->get()->result();
	}
	
	function get_data_by_field_not_in($table, $field, $value){
		$this->tnde->where_not_in($field, $value);
		$this->tnde->from($table);
		return $this->tnde->get()->result();
	}
	
	function count_data($table){
		return $this->tnde->count_all_results($table);
	}
	
	function count_data_by_field($table,$field,$value){
		$this->tnde->where($field, $value);
		$this->tnde->from($table);
		return $this->tnde->count_all_results();
	}
	
	function delete_by_field_id($table, $field, $value){
		$data[$field] = $value;
		$this->tnde->delete($table,$data);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
		
	function insert_blob($table='', $data='', $where){
		$col_id		= key($where);
		$val_id			= $where[$col_id];
		$blob_col	= key($data);
		$blob_val		= $data[$blob_col];
		$sql_cek	= "SELECT COUNT(*) TOTAL FROM ".$table." WHERE ".$col_id."='".$val_id."'";
		$cek		= $this->tnde->query($sql_cek)->row_array();
		$where__ = array(
					array('method' => 'IN', 'name' => ':kodeb', 'value' => $val_id, 'length' => 32, 'type' => 'SQLT_CHR'),
				);
		$blob__ = array(
					array('method' => 'IN', 'name' => ':sesuatu', 'value' => $blob_val, 'length' => 0, 'type' => 'BLOB')
				);
		if(empty($cek['TOTAL'])){
			$insert	= $this->insert_data($table, $where);
			if($insert){
				$sql = "UPDATE ".$table." SET ".$blob_col." = EMPTY_BLOB() WHERE ".$col_id." = :kodeb RETURNING ".$blob_col." INTO :sesuatu";
				$act = $this->tnde->call_blob($sql, $where__, $blob__);
				$hasil = $act['result'];
				#$hasil = array($act['result'], '#2');
			}else{
				$hasil = FALSE;
				#$hasil = array($act['result'], '#1');
			}
		}else{
				$sql = "UPDATE ".$table." SET ".$blob_col." = EMPTY_BLOB() WHERE ".$col_id." = :kodeb RETURNING ".$blob_col." INTO :sesuatu";
				$act = $this->tnde->call_blob($sql, $where__, $blob__);
				$hasil = $act['result'];
				#$hasil = array($act['result'], '#0');
		}
		return $hasil;
	}
	
	function get_blob($table, $blob_col, $where){
		$col_id		= key($where);
		$val_id			= $where[$col_id];
		$sql			= "SELECT ".$blob_col." FROM ".$table." WHERE ".$col_id."='".$val_id."'";
		$hasil		= $this->tnde->query($sql)->row_array();
		// return strlen($hasil[$blob_col]->load());
		return $hasil[$blob_col]->load();
		// return base64_encode(2);
	}
	
	function get_data_by_par($table='', $where='', $order=''){
		$this->tnde->from($table);
		if(!empty($where)){
			$this->tnde->where($where);
		}
		if(!empty($order)){
			$this->tnde->order_by($order);
		}
		$data	= $this->tnde->get();
		return $data->result_array();
	}

	function get_data_by_par_v2($table='', $where='', $order=''){
		$this->tnde->from($table);
		if(!empty($where)){
			$this->tnde->where($where);
		}
		if(!empty($order)){
			$this->tnde->order_by($order);
		}
		$data	= $this->tnde->get();
		return $data->result_array();
	}
}
?>