<?php
if(! defined('BASEPATH'))exit('No direct script access allowed');
class Mdl_direktori_instansi extends CI_Model{
	
	function __construct(){
		parent::__construct();		
		$this->tnde = $this->load->database('dbwilayah',TRUE);
	}

	 public function get_instansi_perizinan()
    {
        //$query = $this->tnde->get_where('INSTANSI_PERIZINAN',['TINGKAT_INSTANSI'=>'1']);
        $query = $this->tnde->get('INSTANSI_PERIZINAN');

        if($query->num_rows() != 0){
            return $query->result_array();            
        } else {
            return FALSE;
        }        
    }
}