<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'modules/service_sakad/libraries/REST_Controller.php'; // This can be removed if you use __autoload() in config.php OR use Modular Extensions
class Tnde_master_mix extends REST_Controller {
	function __construct(){
		parent::__construct();		
		$this->load->model('Mdl_master_surat_akademik', 'mdl_13000');

		# API KODE : 13001 (GET | 200 Data atau FALSE), 13002 (GET | 200 Dataset, 13003 (INSERT | 201 Data, False)
	}

	public function index_get()
	{
		echo "Baru Aja Kickstart Gaes!!! masih polosan";
	}

	public function test_get()
	{
		$data = $this->mdl_13000->get_all_md_jenis_sakad();
		echo json_encode($data, JSON_PRETTY_PRINT);
		//echo var_dump($data);
	}
	
	public function get_jenis_surat_akademik_post($format='json')
	{
		$kode				=	(int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		=	(int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	=	$this->input->post('api_search');

		$response = []; $status = 200;
		if ($kode == 13001){ # RESULT_ARRAY atau FALSE
			switch($subkode){
				case 1: $query = $this->mdl_13000->get_all_md_jenis_sakad();break; #TAMPILKAN SEMUANYA (AKTIF maupun TIDAK AKTIF)
				case 2: $query = $this->mdl_13000->get_md_jenis_sakad(true);break; # HANYA YG AKTIF TAMPIL
				case 3: $query = $this->mdl_13000->get_jenis_sakad_by_kd($api_search[0]);break;
				case 4: $query = $this->mdl_13000->get_md_jenis_sakad(false);break; # HANYA YG TAK AKTIF TAMPIL
				default:
					$query = [
						'error_id'	=> 46,
						'error_message'	=> 'Api subkode salah atau tidak dikenali oleh api kode'
					]; $status=400; //bad syntax
				break;
			}
		} elseif ($kode == 13002) { #ROW ARRAY ATAU FALSE
			switch($subkode){
				case 1: $query = $this->mdl_13000->get_jenis_sakad_by_kd_v2($api_search[0]);break;
				case 2: $query = $this->mdl_13000->get_jenis_sakad_by_path_v2($api_search[0]);break;
				default:
					$query = [
						'status'	=> 400,						
						'error_message'	=> 'Api subkode salah atau tidak dikenali oleh api kode'
					]; $status=400; //bad syntax
				break;
			}
		} else {
			$query = [
				'error_id'	=> 46,
				'error_msg'	=> 'Data parameter tidak dikenali oleh API ENDPOINT ini' 
			];$status=400; //bad syntax
		}

		$this->response($query, $status);
	}

	public function get_master_persyaratan_post($format='json')
	{
		$kode = isset($_POST['api_kode']) ? (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']) : NULL;
		$subkode = isset($_POST['api_subkode']) ? (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']) : NULL;
		$api_search = $this->input->post('api_search');

		$response = []; $status = 200;
		if ($kode === 13001){
			switch($subkode){			
				case 1: #DATA MASTER SEMUA PERSYARATAN (KEPERLUAN EDIT SUB JENIS SURAT)
					$response = $this->mdl_13000->get_all_persyaratan();
				break;
				default: 
				$response['error'] = array("status"=>400, 'message'=>'Terdapat kesalahan pada inputan parameter api subkode'); $status=400;
				break;
			}					
		} else {
			$response['error'] = array("status"=>400, 'message'=>'Terdapat kesalahan pada inputan parameter api kode'); $status=400;
		}
		
		$this->response($response, $status);		
	}

	public function get_config_automasi_sakad_post($format='json')
	{
		$kode = isset($_POST['api_kode']) ? (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']) : NULL;
		$subkode = isset($_POST['api_subkode']) ? (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']) : NULL;
		$api_search = $this->input->post('api_search');

		$response = []; $status = 200;
		if ($kode === 18001){
			switch($subkode){			
				case 1: #DATA MASTER SEMUA CONFIG SURAT AKADEMIK
					$response = $this->mdl_18001->get_all_config_automasi_sakad();break;
				case 2:#DATA CONFIG SURAT BERDASAR INPUTAN KD_SUB_JENIS_SURAT					
					$response = $this->mdl_18001->get_config_automasi_sakad_by_kd_sjs($api_search[0]);break;				
				case 3:#DATA CONFIG SURAT BERDASAR INPUTAN KD_SUB_JENIS_SURAT & UNIT_ID										
					$response = $this->mdl_18001->get_config_automasi_sakad_by_par($api_search[0],$api_search[1]);break;
				case 4:#DATA CONFIG SURAT BERDASAR INPUTAN UNIT_ID										
					$response = $this->mdl_18001->get_config_automasi_sakad_by_unit_id($api_search[0]);break;				
				default: 
				$response['error'] = array("status"=>400, 'message'=>'Terdapat kesalahan pada inputan parameter api subkode'); $status=400;
				break;
			}					
		} else {
			$response['error'] = array("status"=>400, 'message'=>'Terdapat kesalahan pada inputan parameter api kode'); $status=400;
		}
		
		$this->response($response, $status);
	}

	public function update_config_automasi_post($format='json')
	{

	}

	public function update_sub_jenis_surat_post($format='json')
	{
		$kode = isset($_POST['api_kode']) ? (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']) : NULL;
		$subkode = isset($_POST['api_subkode']) ? (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']) : NULL;
		$api_search = $this->input->post('api_search');

		$response = []; $status = 200;
		if ($kode === 18003){
			switch($subkode){
				case 1: #UPDATE DATA UMUM SUB_JENIS_SURAT
					$response = $this->mdl_18001->update_general_sub_jenis_surat();
				break;
				case 2: #UPDATE KOLOM PERSYARATAN SUB_JENIS_SURAT (parameter KD_SUB_JENIS_SURAT, array serialize CEK_SYARAT)
					$response = $this->mdl_18001->update_persyaratan_sub_jenis_surat($api_search[0],$api_search[1]);
				break;
				default: 
				$response['error'] = array("status"=>400, 'message'=>'Terdapat kesalahan pada inputan parameter api subkode'); $status=400;
				break;
			}					
		} else {
			$response['error'] = array("status"=>400, 'message'=>'Terdapat kesalahan pada inputan parameter api kode'); $status=400;
		}
		
		$this->response($response, $status);		
	}

	function get_sub_jenis_surat_nodb() #DUMMY
	{
		$data = array(
			array(
				"KD_SUB_JENIS_SURAT"	=> 1,
				"GRUP"					=> 1, #grup surat2 akademik
				"INDUK_KD_JENIS_SURAT"	=> 11,
				"NM_SUB_JENIS_SURAT"	=> "SURAT KETERANGAN MASIH KULIAH",
				"IS_AKTIF"				=> 1,
				"KET"					=> "Penomoran mengikuti kode jenis surat induk"
			),
			array(
				"KD_SUB_JENIS_SURAT"	=> 2,
				"GRUP"					=> 1, #grup surat2 akademik
				"INDUK_KD_JENIS_SURAT"	=> 11,
				"NM_SUB_JENIS_SURAT"	=> "SURAT KETERANGAN KELAKUAN BAIK",
				"IS_AKTIF"				=> 1,
				"KET"					=> "Penomoran mengikuti kode jenis surat induk (HIS SURAT KETERANGAN DI UNIT MASING2)"
			),
			array(
				"KD_SUB_JENIS_SURAT"	=> 2,
				"GRUP"					=> 1, #grup surat2 akademik
				"INDUK_KD_JENIS_SURAT"	=> 11,
				"NM_SUB_JENIS_SURAT"	=> "SURAT KETERANGAN LULUS",
				"IS_AKTIF"				=> 1, #1 = AKTIF, 0 = TIDAK AKTIF
				"KET"					=> "Penomoran mengikuti kode jenis surat induk"
			),
			array(
				"KD_SUB_JENIS_SURAT"	=> 2,
				"GRUP"					=> 1, #grup surat2 akademik
				"INDUK_KD_JENIS_SURAT"	=> 6,
				"NM_SUB_JENIS_SURAT"	=> "SURAT IJIN RISET PENELITIAN SKRIPSI",
				"IS_AKTIF"				=> 1, #1 = AKTIF, 0 = TIDAK AKTIF
				"KET"					=> "Penomoran mengikuti kode jenis surat induk"
			)
		);

		if($data){
			echo json_encode($data);
        }
	}

}