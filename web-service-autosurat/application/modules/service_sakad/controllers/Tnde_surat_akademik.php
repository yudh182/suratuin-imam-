<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
    
require APPPATH.'modules/service_sakad/libraries/REST_Controller.php'; // This can be removed if you use __autoload() in config.php OR use Modular Extensions
class Tnde_surat_akademik extends REST_Controller {

	//protected $http_status = 200; //default
	
	function __construct() {
        parent::__construct();
        //$this->load->database();
        $this->tnde = $this->load->database('tnde',TRUE);               
        #perbaikan, ubah semua api kode dan subkode dari fungsi2 yang terhubung ke endpoint disini
        $this->load->model("mdl_master_surat_akademik", "mdl_13000");

        $this->load->model("mdl_surat_akademik", "mdl_14000");
        $this->load->model("mdl_sesi_surat_akademik", "mdl_14006");
        $this->load->model("mdl_surat_general", "mdl_15000");

    }

	function index_get($format = 'json')
	{
        $this->response( array("status"=>true, "message"=>"Ya, aku adalah json") , 200); // 200 being the HTTP response code
    }

## ******************* VERSI 1 (beberapa deprecated) ************************
    /**
    * Dapatkan data Surat Keluar yang dibuat melalui Sistem Automasi
    */
    public function get_surat_keluar_automasi_post($format='json')
    {
		$kode = isset($_POST['api_kode']) ? (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']) : NULL;
		$subkode = isset($_POST['api_subkode']) ? (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']) : NULL;
		$api_search = $this->input->post('api_search');
		
		$status = 200;
		if ($kode === 14001){
			switch($subkode){			
				case 1: #JOIN D_HEADER_SURAT_KELUAR, D_TIKET_ADM, D_DETAIL_SAKAD								
					$q_dskr = $this->mdl_14000->get_d_surat_keluar_automasi_v1($api_search[0]);
					$q_jns = $this->mdl_13000->get_jenis_sakad_by_kd_v2($q_dskr['KD_JENIS_SAKAD']);
					if ($q_dskr !== false && $q_jns !== false){
						$query = $q_dskr + $q_jns;
					} else {
						$query =  array("status"=>false, 'error'=> ['error_code'=>501, 'error_message'=>'query pengambilan data dari database gagal']);$status=501;
					}				
				break;

				default: 
				$query = array("status"=>false, 'error'=> ['error_code'=>400, 'error_message'=>'Terdapat kesalahan pada inputan parameter api subkode']); $status=400;
				break;
			}					
		} else {
			$query = array("status"=>false, 'error'=> ['error_code'=>400, 'error_message'=>'Terdapat kesalahan pada inputan parameter api kode']); $status=400;
		}
		
		$this->response($query, $status);		
	}

	/**
	* @status_fungsi AKTIF DIGUNAKAN
	*/
	public function get_persyaratan_verifikasi_post($format='json')
	{
		$kode = isset($_POST['api_kode']) ? (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']) : NULL;
		$subkode = isset($_POST['api_subkode']) ? (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']) : NULL;
		$api_search = $this->input->post('api_search');

		$response = [];
		$status = 200;
		if ($kode === 14001){
			switch($subkode){			
				case 1: #PERSYARATAN VERIFIKASI BY KODE SUB_JENIS_SURAT					
					$query_jenis_sakad = $this->mdl_13000->get_jenis_sakad_by_kd($api_search[0]);
					$kol_syarat = $query_jenis_sakad[0]['CEK_SYARAT_VERIFIKASI']; 
					$arr_kol_syarat = explode('#', $kol_syarat); //$kol_cek_syarat = unserialize($query_ceksyarat['TEST_CEK_SYARAT']);
					
					//2. query ambil data persyaratan dengan kondisi dari data CEK SYARAT
					$query_csyarat = $this->mdl_13000->cari_persyaratan($arr_kol_syarat);

					$response['meta'] = [
						'dataset title' => 'Data Persyaratan Verifikasi untuk '.$query_jenis_sakad[0]['NM_JENIS_SAKAD'],
						'dataset_fk' => $query_jenis_sakad[0]['KD_JENIS_SAKAD']
					];
					$response['data'] = $query_csyarat;
				break;
				case 2: #PERSYARATAN VERIFIKASI BY PATH SUB_JENIS_SURAT					
					$query_jenis_sakad = $this->mdl_13000->get_jenis_sakad_by_path($api_search[0]);
					$kol_syarat = $query_jenis_sakad[0]['CEK_SYARAT_VERIFIKASI']; 
					$arr_kol_syarat = explode('#', $kol_syarat);
					
					//2. query ambil data persyaratan dengan kondisi dari data CEK SYARAT
					$query_csyarat = $this->mdl_13000->cari_persyaratan($arr_kol_syarat);

					$response['meta'] = [
						'dataset_title' => 'Data Persyaratan Verifikasi untuk '.$query_jenis_sakad[0]['NM_JENIS_SAKAD'],
						'KD_JENIS_SAKAD' => $query_jenis_sakad[0]['KD_JENIS_SAKAD'],
						'KD_JENIS_SURAT_INDUK' => $query_jenis_sakad[0]['KD_JENIS_SURAT_INDUK']
					];
					$response['data'] = $query_csyarat;
				break;
				default: 
				$response['error'] = array("status"=>400, 'message'=>'Terdapat kesalahan pada inputan parameter api subkode'); $status=400;
				break;
			}					
		} else {
			$response['error'] = array("status"=>400, 'message'=>'Terdapat kesalahan pada inputan parameter api kode'); $status=400;
		}
		
		$this->response($response, $status);		
	}

    public function get_sesi_sakad_post($format = 'json')
	{		
		$kode				=	(int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		=	(int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	=	$this->input->post('api_search');
				
		switch($kode){
			case 14006:	switch($subkode){
			default:
					case 1: #ambil entri berdasarkan kolom id_sesi 
					$query = $this->mdl_14006->get_one_sesi_sakad($api_search[0]); 
						break; 
					case 2: #cek/ambil entri berdasarkan LOG KD USER
					$query = $this->mdl_14006->get_sesi_sakad_by_user($api_search[0]);
						break; 
			}
			break;
		}

		//cek final
		/*if ($query != FALSE){
			$this->response($query, 200);	
		} else {
			$this->response(['status'=>false, 'error'=>'internal error happen']);
		}*/
		$this->response($query, 200);
	}

	public function insert_entry_sesi_sakad_post($format = 'json')
	{
		$kode				=	(int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		=	(int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	=	$this->input->post('api_search');
		
		if ($kode !== 14006) {
			$this->response(array("status"=>400, 'message'=>'api kode tidak valid (bad request 400)', 'error_type'=>1001));
		} else {
			switch ($subkode) {
				case 1:
					$query = $this->mdl_14006->insert_entry_minimal($api_search[0]); break;
				case 2:
					$query = $this->mdl_14006->insert_entry_lengkap($api_search[0]); break;
				case 3:
					$query = $this->mdl_14006->insert_entry_data($api_search[0]); break;
				default:
					$query = ["status"=>false,'code'=>400,'error_message'=>'api subkode tidak valid (bad request 400)'];
					$http_status = 400;
					exit();					
					break;
			}
			// ???? Bagaimana jika terjadi error hingga yang dihasilkan pesan error html , variabel $query tidak terpakai			

			$this->response($query);
		}		
	}

	public function insert_entry_sesi_sakad_v2_post($format = 'json')
	{
		$kode				=	(int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		=	(int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	=	$this->input->post('api_search');

		if ($kode !== 14006) {
			$this->response(array("status"=>400, 'message'=>'api kode tidak valid (bad request 400)', 'error_type'=>1001));
		} else {
			switch ($subkode) {
				case 3:
					$query = $this->mdl_14006->insert_entry_data_v2($api_search[0]); break;
				default:
					$query = ["status"=>400, 'message'=>'api subkode tidak valid (bad request 400)', 'error_type'=>1002];					
					$this->response($query,400);
					exit();					
					break;
			}

			$this->response($query,200);
			//$this->response($query,200);
		}
	}

	public function update_entry_sesi_sakad_post($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');

		$status = 200;
		if ($kode !== 14006) {
			$this->response(array("status"=>400, 'message'=>'api kode tidak valid (bad request 400)', 'error_type'=>1001));
		} else {
			switch ($subkode) {									
				case 1: $query = $this->mdl_14006->update_entry_sesi_v1($api_search[0], $api_search[1]);
				break;
				default:
					$query = ["status"=>400, 'message'=>'api subkode tidak valid (bad request 400)', 'error_type'=>1002];					
					$this->response($query,400);
					exit();					
					break;
			}
		}	
		
		$this->response($query,$status);
	}

	public function delete_sesi_sakad_post($format='json')
	{
		$kode				=	(int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		=	(int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	=	$this->input->post('api_search');

		$status = 200;
		if ($kode !== 14007) {
			$this->response(array("status"=>400, 'message'=>'api kode tidak valid (bad request 400)', 'error_type'=>1001));
		} else {
			switch ($subkode) {
				case 1:
					$query = $this->mdl_14006->delete_entry_sesi_sakad($api_search[0]); break;

				default:
					$query = ["error"=>["status"=>400, 'message'=>'api subkode tsidak valid (bad request 400)', 'error_type'=>1002]];					
					$this->response($query,400);
					exit();					
					break;
			}

			$this->response($query,$status);
		}
	}
    

	/**	
	* Menyimpan duplikat data utama surat keluar untuk keperluan automasi
	 * api_kode default: 14003 (array | false), 14004 (transactions true | false)
	 * 
	*/
	public function insert_header_surat_keluar_post($format='json')
	{
		$kode = isset($_POST['api_kode']) ? (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']) : NULL;
		$subkode = isset($_POST['api_subkode']) ? (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']) : NULL;
		$api_search = $this->input->post('api_search');
		
		$status = 200; // http status code
		switch ($kode) {
			case 14003:
				switch ($subkode) {
					case 1:
						$query = $this->mdl_14000->insert_header_surat_keluar_v1($api_search[0]); break;
					default:
						$query = ["status"=>400, 'message'=>'api subkode tidak valid (bad request 400)', 'error_type'=>1002];					
						$this->response($query,400);
						exit();					
						break;
				}
				break;						
			default:
				$query = ["status"=>400, 'message'=>'api kode yang dikirimkan tidak valid (bad request 400)'];
				$status = 400;				
				break;
		}

		$this->response($query,$status);
	}

	/**
	* Menyimpan detail surat keluar untuk keperluan automasi dan dokumentasi (karena belum diakomodir sisurat)
	*/
	public function insert_detail_sakad_post()
	{
		$kode = isset($_POST['api_kode']) ? (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']) : NULL;
		$subkode = isset($_POST['api_subkode']) ? (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']) : NULL;
		$api_search = $this->input->post('api_search');
		$status = 200; // http status code

		switch ($kode) {
			case 14003: #api kode untuk insert (versi 1)
				switch ($subkode) {
					case 1:
						$query = $this->mdl_14000->insert_detail_sakad_keterangan($api_search[0]); break;
					case 2:
						$query = $this->mdl_14000->insert_detail_sakad_ijin($api_search[0]); break;
					case 3:
						$query = $this->mdl_14000->insert_detail_sakad_permohonan_lain($api_search[0]); break;
					default:
						$query = ["status"=>400, 'message'=>'api subkode tidak valid (bad request 400)', 'error_type'=>1002];					
						$this->response($query,400);
						exit();					
						break;
				}
				break;			
			
			default:
				$query = ["status"=>400, 'message'=>'api kode yang dikirimkan tidak valid (bad request 400)'];
				$status = 400;				
				break;
		}

		$this->response($query,$status);
	}

	public function detail_surat_akademik_get($format = 'json')
	{
		$kode				=	(int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		=	(int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	=	$this->input->post('api_search');

		switch($kode){
			case 14008:	switch($subkode){
				default:
					case 1: $query = $this->mdl_18002->get_detail_surat_akademik_by_id_sk($api_search[0]);  break;
					case 2: $query = $this->mdl_18002->cek_keaslian_surat_akademik($api_search[0]);  break; #CEK KEASLIAN SURAT MHS OLEH PEGAWAI TU (dependency : eksekusi procedure)
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}

## ******************* VERSI 2 ************************
	public function surat_keluar_automasi_post()
	{
		$this->mdl_14000->get_d_header_surat_keluar();
	}


	public function get_detail_skr_automasi_post()
	{
		$kode = isset($_POST['api_kode']) ? (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']) : NULL;
		$subkode = isset($_POST['api_subkode']) ? (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']) : NULL;
		$api_search = $this->input->post('api_search');
		
		$status = 200;
		if ($kode === 14001){
			switch($subkode){			
				case 1: #JOIN D_HEADER_SURAT_KELUAR, D_DETAIL_SAKAD_{grup}
					# 1.1 DAPATKAN HEADER_SURAT_KELUAR (query ke-1)
					$q0 = $this->mdl_14000->get_d_header_surat_keluar_by_id($api_search[0]);

					#1.2 DAPATKAN KD_GRUP_JENIS_SAKAD
					$q1 = $this->mdl_13000->get_jenis_sakad_by_kd($q0['KD_JENIS_SAKAD']);
					//exit(print_r($q1));
					# 2. DAPATKAN DETAIL (query ke-2)
					$q2 = $this->mdl_14000->get_detail_surat_keluar_automasi($api_search[0], $q1[0]['GRUP_JENIS_SAKAD']);

					# 3. GABUNGKAN HEADER dan DETAIL 
					$filtered_jenis = array_intersect_key($q1[0], array_flip(array('GRUP_JENIS_SAKAD','KD_JENIS_SURAT_INDUK', 'NM_JENIS_SAKAD','KETERANGAN')));
					$response = array_merge($q0, $q2, $filtered_jenis);
					//$response[] = $filtered_jenis;
				break;
				default: 
					$response = array("status"=>false, 'error'=> ['error_code'=>400, 'error_message'=>'Terdapat kesalahan pada inputan parameter api subkode']); $status=400;
				break;
			}
		} else {
			$response = array("status"=>false, 'error'=> ['error_code'=>400, 'error_message'=>'Terdapat kesalahan pada inputan parameter api kode']); $status=400;
		}

		$this->response($response, $status);
	}


	/** 
	* Insert Detail Skr (Surat Keluar) Automasi 
	 * Simpan copy header surat keluar + detail surat keluar mahasiswa (surat keterangan akademik, permohonan ijin, permohonan lain) + tiket administrasi (token keaslian dan atau psd digital)
	 * transactions (api kode 14004)
	 * @param array multidimensional
	 * @return array atau FALSE
	 */
	public function insert_detail_skr_automasi_post()
	{
		$kode = isset($_POST['api_kode']) ? (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']) : NULL;
		$subkode = isset($_POST['api_subkode']) ? (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']) : NULL;
		$api_search = $this->input->post('api_search');
		
		$status = 200; // http status code
		switch ($kode) {
			case 14004: #SKEMA CETAK BERNOMOR
				switch ($subkode) {
					case 1: #SEKALIGUS GENERATE TOKEN UNTUK TIKET_ADMINISTRASI
						# 0) intersep $api_search[0] untuk query jenis sakad by kd
						if (empty($api_search[0]['GRUP_JENIS_SAKAD'])){
							$q0 = $this->mdl_13000->get_jenis_sakad_by_kd($api_search[0]['KD_JENIS_SAKAD']);
							$api_search[0]['GRUP_JENIS_SAKAD'] = $q0[0]['GRUP_JENIS_SAKAD'];
						} else {
							$q0 = null;
						}
						
						//exit(var_dump($q0));
						# 1) Query Transactions (header + detail)						
						$query = $this->mdl_14000->trans_insert_detail_surat_keluar_automasi($api_search[0]); 
						
						#2) generate token, insert record ke D_TIKET_ADMINISTRASI
						//$token_str = '';

						# 3) format response dengan apik
						break;
					case 2: #TIDAK TERMASUK GENERATE TOKEN
						break;
					default:
						$query = ["status"=>400, 'message'=>'api subkode tidak valid (bad request 400)', 'error_type'=>1002];					
						$this->response($query,400);
						exit();					
						break;
				}
				break;						
			case 14005: #SKEMA CETAK BERTANDA TANGAN DIGITAL
				break;
			default:
				$query = ["status"=>400, 'message'=>'api kode yang dikirimkan tidak valid (bad request 400)'];
				$status = 400;				
				break;
		}

		$this->response($query,$status);
	}

	/**
    * Hapus data-data detail surat dari header_sk, detail_sakad, tiket_administrasi    
    * @param ID_SURAT_KELUAR (string)
    * @return api_kode 14007 : 
    */
	public function delete_detail_surat_automasi_post($format='json')
	{
		$kode = isset($_POST['api_kode']) ? (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']) : NULL;
		$subkode = isset($_POST['api_subkode']) ? (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']) : NULL;
		$api_search = $this->input->post('api_search');		
		$status = 200;
		if ($kode == 14007){
			switch ($subkode) {
				case 1:
					$cek_hsk = $this->mdl_14000->get_d_header_surat_keluar_by_id($api_search[0]); //cek header untuk tahu kd_jenis_sakad
					$cek_js = $this->mdl_13000->get_jenis_sakad_by_kd_v2($cek_hsk['KD_JENIS_SAKAD']);					
					$cek_grup = $cek_js['GRUP_JENIS_SAKAD'];

					$query = $this->mdl_14000->trans_delete_detail_surat_automasi($api_search[0], $cek_grup);
					break;
				
				default:
					$query = ["status"=>400, 'error_message'=>'api subkode tidak valid (bad request 400)', 'error_type'=>1002]; $status = 422;
					break;
			}			
		} else {
			$query = ["status"=>400, 'error_message'=>'api kode tidak valid (bad request 400)', 'error_type'=>1001]; $status = 422;
		}
		//echo print_r($query);
		$this->response($query,$status);
	}

# ******************* BOOILERPLATE ******************* 
	public function arsip_surat_akademik_post($format = 'json')
	{}
	
	public function pengurusan_surat_akademik_post($format = 'json')
	{

	}

	public function test_insert_procedure1_post($format = 'json')
	{}

	public function test_pdo_get()
	{
		exit(json_encode($this->mdl_14000->test_pdo_header_skr(), JSON_PRETTY_PRINT));
	}

	public function test_pdo_post()
	{
		$api_search = $this->input->post('api_search');

		$query = $this->mdl_14000->test_pdo_insert_header_skr($api_search[0]);

		$this->response($query, 200);
	}
}