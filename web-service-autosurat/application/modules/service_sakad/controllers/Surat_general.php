<?php if(!defined('BASEPATH'))exit('No direct script access allowed');
require APPPATH.'modules/service_sakad/libraries/REST_Controller.php';
class Surat_general extends REST_Controller{
	
	public function __construct(){
		parent::__construct();
		$this->load->model('Mdl_surat_general', 'mdl_15000');
	}
	
	public function index(){
		echo "^^";
	}
	
		
	function get_data_post($format = 'json'){
		$kode = (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode = (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search = $this->input->post('api_search');
		switch($kode){
			case 15001: switch($subkode){
				default:
					case 1: $query = $this->mdl_15000->get_data($api_search[0]); break;
					case 2: $query = $this->mdl_15000->get_data_by_field($api_search[0],$api_search[1],$api_search[2]); break; #output row_array
					case 3: $query = $this->mdl_15000->get_data_by_field2($api_search[0],$api_search[1],$api_search[2]); break; #output result
					case 4: $query = $this->mdl_15000->get_data_by_field3($api_search[0],$api_search[1]); break;#output row_array
					case 5: $query = $this->mdl_15000->get_data_by_field_like($api_search[0],$api_search[1], $api_search[2]); break;
					case 6: $query = $this->mdl_15000->get_data_by_field_in($api_search[0],$api_search[1], $api_search[2]); break;
					case 7: $query = $this->mdl_15000->get_data_by_field_not_in($api_search[0],$api_search[1], $api_search[2]); break;
			}
			break;
			case 15002:
				default:
					case 1:
						$order	= (!empty($api_search[2]) ? $api_search[2] : '');
						$query = $this->mdl_15000->get_data_by_field_array($api_search[0], $api_search[1], $order); break;
				break;
			case 15003:
				default:
					case 1:
						$table		= (!empty($api_search[0]) ? $api_search[0] : '');
						$where	= (!empty($api_search[1]) ? $api_search[1] : '');
						$order		= (!empty($api_search[2]) ? $api_search[2] : '');
						$query 	= $this->mdl_15000->get_data_by_par($table, $where, $order); break;
				break;
		}
		$this->response($query, 200);
	}
	
	function insert_data_post($format = 'json'){
		$kode = (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode = (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search = $this->input->post('api_search');
		switch($kode){
			case 15001: switch($subkode){
				default:
					case 1: $query = $this->mdl_15000->insert_data($api_search[0], $api_search[1]); break;
			}
			break;
		}
		$this->response($query, 200);
	}
	/*
	
	function update_data($format = 'json'){
		$kode = (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode = (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search = $this->input->post('api_search');
		switch($kode){
			case 1001: switch($subkode){
				default:
					case 1: $query = $this->mdl_15000->update_data($api_search[0], $api_search[1], $api_search[2]); break; //table, array_data, array_where
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);	
	}
	
	function delete_data($format = 'json'){
		$kode = (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode = (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search = $this->input->post('api_search');
		switch($kode){
			case 1001: switch($subkode){
				default:
					case 1: $query = $this->mdl_15000->delete_data($api_search[0], $api_search[1]); break;
					case 2: $query = $this->mdl_15000->delete_data_in($api_search[0], $api_search[1], $api_search[2]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);	
	}	
	
	function order_data($format = 'json'){
		$kode = (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode = (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search = $this->input->post('api_search');
		switch($kode){
			case 1001: switch($subkode){
				default:
					case 1: $query = $this->mdl_15000->order_data($api_search[0], $api_search[1], $api_search[2]); break;
					case 2: $query = $this->mdl_15000->order_data_result($api_search[0], $api_search[1], $api_search[2]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);	
	}
						
	function count_data($format = 'json'){
		$kode = (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode = (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search = $this->input->post('api_search');
		switch($kode){
			case 1001: switch($subkode){
				default:
					case 1: $query = $this->mdl_15000->count_data($api_search[0]); break;
					case 2: $query = $this->mdl_15000->count_data_by_field($api_search[0],$api_search[1],$api_search[2]); break;
					case 3: $query = $this->mdl_15000->count_by_field_in($api_search[0], $api_search[1], $api_search[2]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function delete_by_field_id($format = 'json'){
		$kode = (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode = (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search = $this->input->post('api_search');
		switch($kode){
			case 1001: switch($subkode){
				default:
					case 1: $query = $this->mdl_15000->delete_by_field_id($api_search[0],$api_search[1],$api_search[2]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);	
	}
		
	function insert_blob($format = 'json'){
		$kode 			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode 		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search 	= $this->input->post('api_search');		
		switch($kode){
			case 1001: switch($subkode){
				default: 
					case 1: $query = $this->mdl_15000->insert_blob($api_search[0], $api_search[1], $api_search[2]);break;
			}
			break;		
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function get_blob($format = 'json'){
		$kode 			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode 		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search 	= $this->input->post('api_search');		
		switch($kode){
			case 1001: switch($subkode){
				default: 
					case 1: $query = $this->mdl_15000->get_blob($api_search[0], $api_search[1], $api_search[2]);break;
			}
			break;		
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	*/
}