<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');
Class Sisurat_general{
	function __construct(){
		define("URL_API_SIA", "http://service.uin-suka.ac.id/servsiasuper/index.php/sia_public/");
		define("URL_API_JABAT", "http://service.uin-suka.ac.id/servsiasuper/index.php/simpeg_master/init/");
		define("URL_API_AD", "http://service.uin-suka.ac.id/servad/");
	}
	
	function jabatan_sistem($kd_pgw=''){
		$ci =& get_instance();
		$parameter 				= array('api_kode' => 1124, 'api_subkode' => 2, 'api_search' => array($kd_pgw));
		$jab_sia_db		 		= $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
		$jabatan_sistem 		= jab_surat($jab_sia_db);
		$sis_asm = array();
		if(!empty($jabatan_sistem)){
			foreach($jabatan_sistem as $key => $val){
				$kd_modul = substr($val['SIS_ID'], 3, 3);
				$parameter = array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('D_JAB_SM', 'KD_JAB_SISTEM', $val['SIS_ID']));
				$jab_detail = $ci->mdl_tnde->get_api('tnde_general/get_data', 'json', 'POST', $parameter);
				if(!empty($jab_detail)){
					$sis_asm[$kd_modul][] = $jab_detail;
				}
			}
		}
		return $sis_asm;
	}
	
	function jabatan_sistem_all($kd_pgw=''){
		$ci =& get_instance();
		$parameter 				= array('api_kode' => 1124, 'api_subkode' => 2, 'api_search' => array($kd_pgw));
		$jab_sia_db		 		= $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
		$jabatan_sistem 		= jab_surat($jab_sia_db);
		$sis = array();
		if(!empty($jabatan_sistem)){
			foreach($jabatan_sistem as $key => $val){
				$kd_modul = substr($val['SIS_ID'], 3, 3);
				switch($kd_modul){
					case 'ASM':
						$parameter = array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('D_JAB_SM', 'KD_JAB_SISTEM', $val['SIS_ID']));
						$jab_detail = $ci->mdl_tnde->get_api('tnde_general/get_data', 'json', 'POST', $parameter);
						if(!empty($jab_detail)){
							$sis[$kd_modul][] = $jab_detail;
						}
						break;
					case 'PSM':
						$parameter = array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('D_JAB_SM', 'KD_JAB_SISTEM', $val['SIS_ID']));
						$jab_detail = $ci->mdl_tnde->get_api('tnde_general/get_data', 'json', 'POST', $parameter);
						if(!empty($jab_detail)){
							$sis[$kd_modul][] = $jab_detail;
						}
						break;
					case 'SKR':
						$parameter = array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('D_JAB_SK', 'KD_JAB_SISTEM', $val['SIS_ID']));
						$jab_detail = $ci->mdl_tnde->get_api('tnde_general/get_data', 'json', 'POST', $parameter);
						if(!empty($jab_detail)){
							$sis[$kd_modul][] = $jab_detail;
						}
						break;
					case 'NMR':
						$parameter = array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('D_JAB_PENOMORAN', 'KD_JAB_SISTEM', $val['SIS_ID']));
						$jab_detail = $ci->mdl_tnde->get_api('tnde_general/get_data', 'json', 'POST', $parameter);
						if(!empty($jab_detail)){
							$sis[$kd_modul][] = $jab_detail;
						}
						break;
					case 'AVA':
						$parameter = array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('D_JAB_AVATAR', 'KD_JAB_SISTEM', $val['SIS_ID']));
						$jab_detail = $ci->mdl_tnde->get_api('tnde_general/get_data', 'json', 'POST', $parameter);
						if(!empty($jab_detail)){
							$sis[$kd_modul][] = $jab_detail;
						}
						break;
					default:
						$sis[$kd_modul][] = $val;
						break;
				}
			}
		}
		return $sis;
	}
	
	function status_jabatan_by_str_id($str_id=''){
		$ci =& get_instance();
		$parameter = array('api_kode' => 1101, 'api_subkode' => 1, 'api_search' => array($str_id));
		$detail = $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
		if(!empty($detail[0])){
			$hasil = status_jabatan($detail[0]);
		}else{
			$hasil = '';
		}
		return $hasil;
	}
	
	function grup_user($kode='', $jabatan=''){
		$ci =& get_instance();
		$parameter	= array('api_kode'=>26000, 'api_subkode' => 12, 'api_search' => array($kode));
		$mhs		 		= $ci->mdl_tnde->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);
		$hasil = '';
		if(empty($mhs[0])){
			$parameter 	= array('api_kode' => 2001, 'api_subkode' => 2, 'api_search' => array($kode)); #kd_pgw
			$pegawai 		= $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
			if(!empty($pegawai[0])){
				$status	= $ci->sisurat_general->status_jabatan_by_str_id($jabatan);
				switch($status){
					case 'PEJABAT':
						$hasil = 'PJB01';
						break;
					case 'PELAKSANA':
						$hasil = 'PGW01';
						break;
					default:
						$hasil = 'PGW01';
						break;
				}
			}else{
				$hasil = '';
			}
		}else{
			$hasil	= 'MHS01';
		}
		return $hasil;
	}
	
	function membawahi($date, $kd_pgw, $status=1){
		$ci =& get_instance();
		$hasil = array();
		$parameter = array('api_kode' => 1216, 'api_subkode' => 13, 'api_search' => array($date, $kd_pgw, $status));
		$membawahi = $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
		
		if(!empty($membawahi)){
			$tmp_pgw	= array();
			foreach($membawahi as $key => $data){
				$tmp_pgw[$key] = array();
				if(!empty($data)){
					foreach($data as $k => $dd){
						if(!in_array($dd['LOW_KD_PGW'], $tmp_pgw[$key])){
							$hasil[$key][$k]['raw'] = $dd;
							$hasil[$key][$k]['status_jabatan'] = $ci->sisurat_general->status_jabatan_by_str_id($dd['LOW_STR_ID']);
							$tmp_pgw[$key][] = $dd['LOW_KD_PGW'];
						}
					}
				}
			}
		}
		return $hasil;
	}
	
	function gmu_membawahi($date, $kd_pgw, $status=1, $gmu_slug='UINTI'){
		$ci =& get_instance();
		$hasil = array();
		$parameter = array('api_kode' => 1216, 'api_subkode' => 10, 'api_search' => array($date, $kd_pgw, $status));
		$membawahi = $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
		
		if(!empty($membawahi)){
			$tmp_pgw	= array();
			foreach($membawahi as $key => $data){
				$tmp_pgw[$key] = array();
				if(!empty($data)){
					foreach($data as $k => $dd){
						if(!in_array($dd['LOW_KD_PGW'], $tmp_pgw[$key])){
							if($dd['GMU_SLUG'] == $gmu_slug){
								$hasil[$key][$k]['raw'] = $dd;
								$hasil[$key][$k]['status_jabatan'] = $ci->sisurat_general->status_jabatan_by_str_id($dd['LOW_STR_ID']);
								$tmp_pgw[$key][] = $dd['LOW_KD_PGW'];
							}
						}
					}
				}
			}
		}
		return $hasil;
	}
	
	function bawahan($kd_pgw='', $jab='', $date=''){
		$hasil = array();
		$ci =& get_instance();
		$date				= (!empty($date) ? $date : date('d/m/Y'));
		$status			= 1;
		$membawahi 	= $ci->sisurat_general->gmu_membawahi($date, $kd_pgw, $status, 'UINTI');
		if(!empty($membawahi[$jab])){
			foreach($membawahi[$jab] as $key => $val){
				$nip 			= $val['raw']['LOW_KD_PGW'];
				$nm_pgw	= $val['raw']['LOW_NM_PGW_F'];
				$str_nama	= $val['raw']['LOW_STR_NAMA'];
				$str_id 		= $val['raw']['LOW_STR_ID'];
				$id				= $nip."#".$str_id.'#'.$nm_pgw.'#'.$str_nama;
				if($val['status_jabatan'] == 'PEJABAT'){
					$name = $str_nama;
				}else{
					$name = $nm_pgw;
				}
				$hasil[$val['status_jabatan']][] = array('id' => $id, 'name' => $name, 'str_id' => $str_id, 'kd_pgw' => $nip, 'nm_pgw_f' => $nm_pgw, 'str_nama' => $str_nama);
			}
		}
		return $hasil;
	}
	
	function notif_pengarah_sm($unit_array){
		$ci =& get_instance();
		$hasil_unit = array();
		if(!empty($unit_array)){
			foreach($unit_array as $val){
				$parameter = array('api_kode' => 1225, 'api_subkode' => 5, 'api_search' => array($val, 'SRT0001A', array('SRT0001B')));
				$d_unit = $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
				if(!in_array($val, $hasil_unit)){
					$hasil_unit[] = $val;
				}
				if(!empty($d_unit[0]['GRU_INSIDE']['SRT0001B'])){
					if($d_unit[0]['GRU_INSIDE']['SRT0001B'] == 1){
						$parameter	= array('api_kode' => 1225, 'api_subkode' => 3, 'api_search' => array('SRT0001B'));
						$pau = $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
						if(!empty($pau)){
							foreach($pau as $dd){
								if(!in_array($dd['UNIT_ID'], $hasil_unit)){
									$hasil_unit[] = $dd['UNIT_ID'];
								}
							}
						}
					}					
				}
			}
		}
		$parameter		= array('api_kode' => 1002, 'api_subkode' => 1, 'api_search' => array($hasil_unit));
		$cek 	= $ci->mdl_tnde->get_api('tnde_general/notifications', 'json', 'POST', $parameter);
		if(!empty($cek['TOTAL'])){
			$hasil = $cek['TOTAL'];
		}else{
			$hasil = 0;
		}
		return $hasil;
	}
	
	function data_unit($unit_array){
		$ci =& get_instance();
		$hasil = array();
		if(!empty($unit_array)){
			foreach($unit_array as $val){
				$parameter = array('api_kode' => 1901, 'api_subkode' => 2, 'api_search' => array(date('d/m/Y'), $val));
				$unit = $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
				if(!empty($unit[0])){
					$hasil[] = $unit[0];
				}
			}
		}
		return $hasil;
	}
	
	function gru_inside($unit_id='', $gmu_slug='', $arr_gmu_slug){
		$ci =& get_instance();
		$parameter		= array('api_kode' => 1225, 'api_subkode' => 5, 'api_search' => array($unit_id, $gmu_slug, $arr_gmu_slug));
		$cek_kel_unit 	= $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
		$hasil = array();
		if(!empty($cek_kel_unit[0]['GRU_INSIDE'])){
			foreach($cek_kel_unit[0]['GRU_INSIDE'] as $key => $val){
				if($val == 1){
					$hasil[] = $key;
				}
			}
		}
		return $hasil;
	}
	
	function prodi_by_unit_id($unit_id=''){
		$ci =& get_instance();
		$parameter = array('api_kode' => 1901, 'api_subkode' => 4, 'api_search' => array(date('d/m/Y'), $unit_id));
		$unit = $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
		$hasil = array();
		if(!empty($unit[0])){
			if($unit[0]['UNIT_JENIS'] == 'J004'){ #UNIT JENIS FAKULTAS
				$kd_sia = (!empty($unit[0]['SIA_KODE']) ? $unit[0]['SIA_KODE'] : '');
				$parameter = array('api_kode' => 19000, 'api_subkode' => 6, 'api_search' => array($kd_sia));
				$sia_m = $ci->mdl_tnde->api_sia('sia_master/data_search', 'json', 'POST', $parameter);
				if(!empty($sia_m)){
					foreach($sia_m as $val){
						$parameter = array('api_kode' => 1901, 'api_subkode' => 6, 'api_search' => array(date('d/m/Y'), $val['KD_PRODI']));
						$unit_prodi = $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
						if(!empty($unit_prodi[0])){
							$hasil[] = $unit_prodi[0];
						}
					}
				}
			}
		}
		return $hasil;
	}
	
	function psd_by_unit_id($unit_id=''){
		$ci =& get_instance();
		$unit_pau		= $ci->sisurat_surat_keluar->unit_pau($unit_id);
		$psd_arr		= array();
		$tmp_psd		= array();
		/*if(!empty($unit_pau)){
			foreach($unit_pau as $data){*/
				$parameter 	= array('api_kode' => 1121, 'api_subkode' => 8, 'api_search' => array(date('d/m/Y'), $unit_pau, 1));
				$pejabat			= $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
				$pjb1				= (!empty($pejabat) ? array_column($pejabat, 'STR_ID') : array());
				$parameter 	= array('api_kode' => 1121, 'api_subkode' => 8, 'api_search' => array(date('d/m/Y'), 'UN01000', 1));
				$rektorat		= $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
				$pjb2				= (!empty($rektorat) ? array_column($rektorat, 'STR_ID') : array());
				// $pjb3 				= array('ST000140', 'ST000111', 'ST000114');
				$pjb3 				= array('ST000140', 'ST000111', 'ST000114', 'ST000116'); //tambahan kepala bagian rumah tangga
				$pjb_arr			= array_merge($pjb1, $pjb2, $pjb3);
				// if(!empty($unit_id)) $pjb_arr			= array_merge($pjb_arr, array($unit_id));
				// log_message('error', 'psd: ' . json_encode($pjb_arr));
				$pjb_list			= implode_to_string($pjb_arr);
				
				$parameter	= array('api_kode' => 110012, 'api_subkode' => 2, 'api_search' => array($pjb_list));
				$psd				= $ci->mdl_tnde->get_api('tnde_master/get_psd', 'json', 'POST', $parameter);
				if(!empty($psd)){
					foreach($psd as $val){
						if(! in_array($val['ID_PSD'], $tmp_psd)){
							$psd_arr[] 		= $val;
							$tmp_psd[] 	= $val['ID_PSD'];
						}
					}
				}
			/*}
		}*/
		return $psd_arr;
	}	
	
	function psd_sk_by_unit_id($unit_id=''){
		$ci =& get_instance();
		$parameter = array('api_kode' => 1225, 'api_subkode' => 5, 'api_search' => array($unit_id, 'SRT0002A', array('SRT0002B')));
		$d_unit = $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
		$hasil_unit = array();
		if($d_unit[0]['GRU_INSIDE']['SRT0002B'] == 1){
			$parameter	= array('api_kode' => 1225, 'api_subkode' => 3, 'api_search' => array('SRT0002B'));
			$pau = $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
			if(!empty($pau)){
				foreach($pau as $dd){
					if(!in_array($dd['UNIT_ID'], $hasil_unit)){
						$hasil_unit[] = $dd['UNIT_ID'];
					}
				}
			}
		}else{
			$hasil_unit[] = $unit_id;
		}
		$pjb_arr = array();
		if(!empty($hasil_unit)){
			foreach($hasil_unit as $val){
				$parameter 	= array('api_kode' => 1121, 'api_subkode' => 8, 'api_search' => array(date('d/m/Y'), $val, 1));
				$pejabat			= $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
			
				if(!empty($pejabat)){
					foreach($pejabat as $dd){
						if(!in_array($dd['STR_ID'], $pjb_arr)){
							$pjb_arr[] = $dd['STR_ID'];
						}
					}
				}
			}
		}
		$pjb_list			= implode_to_string($pjb_arr);
		$parameter	= array('api_kode' => 11001, 'api_subkode' => 1, 'api_search' => array($pjb_list));
		$psd				= $ci->mdl_tnde->get_api('tnde_master/psd_sk', 'json', 'POST', $parameter);
		return $psd;
	}
	
	function akun_list($kd_pgw=''){
		$ci =& get_instance();
		$parameter 	= array('api_kode' => 1121, 'api_subkode' => 3, 'api_search' => array(date('d/m/Y'), $kd_pgw, 1));
		$jab_str 			= $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
		$struktural 		= jabatan_struktural($jab_str);
		$hasil	= array();
		if(!empty($struktural['PELAKSANA'])){
			foreach($struktural['PELAKSANA'] as $val){
				$hasil[] = $val;
			}
		}
		if(!empty($struktural['PEJABAT'])){
			foreach($struktural['PEJABAT'] as $val){
				$hasil[] = $val;
			}
		}
		
		$jab_sis = $ci->sisurat_general->jabatan_sistem_all($kd_pgw);
		if(!empty($jab_sis['AVA'])){
			foreach($jab_sis['AVA'] as $val){
				$parameter = array('api_kode' => 1121, 'api_subkode' => 9, 'api_search' => array(date('d/m/Y'), $val['STR_ID'], 1));
				$pgw = $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
				if(!empty($pgw[0])){
					$hasil[] = $pgw[0];
				}
			}
		}
		return $hasil;
	}

	function notifikasi($data){
		$ci =& get_instance();
		$kd_pgw		= (!empty($data['KD_PGW']) ? $data['KD_PGW'] : '');
		$str_id			= (!empty($data['STR_ID']) ? $data['STR_ID'] : '');
		$psm_unit		= (!empty($data['PSM_UNIT']) ? $data['PSM_UNIT'] : array());
		$avatar			= $data['AVATAR'];
		/* NOTIFIKASI PENGARAH SURAT */
		$psd					= $ci->sisurat_general->notif_pengarah_sm($psm_unit);
		$hasil['PSM'] 	= (!empty($psd) ? $psd : 0);
		/*END*/
		/* NOTIFIKASI SURAT TEMBUSAN['PEJABAT'] */
		$parameter		= array('api_kode' => 80001,'api_subkode' => 2,'api_search' => array('TS', 1, $str_id));
		$tot 					= $ci->mdl_tnde->get_api('tnde_surat_masuk/get_distribusi_pejabat','json','POST', $parameter);
		$hasil['ST']			= (!empty($tot['TOTAL']) ? $tot['TOTAL'] : 0);
		/*END*/
		/* NOTIFIKASI SURAT DINAS[PEJABAT] */
		$parameter		= array('api_kode' => 80001,'api_subkode' => 2,'api_search' => array('PS', 1, $str_id));
		$tot 					= $ci->mdl_tnde->get_api('tnde_surat_masuk/get_distribusi_pejabat','json','POST', $parameter);
		$hasil['SD']		= (!empty($tot['TOTAL']) ? $tot['TOTAL'] : 0);
		/*END*/		
		/* NOTIFIKASI SURAT DINAS[PELAKSANA] */
		$status_distribusi	= "'PS','TS'";
		$parameter			= array('api_kode' => 80001,'api_subkode' => 2,'api_search' => array($status_distribusi, 1, $kd_pgw));
		$tot 						= $ci->mdl_tnde->get_api('tnde_surat_masuk/get_distribusi_personal','json','POST', $parameter);
		$hasil['SP']				= (!empty($tot['TOTAL']) ? $tot['TOTAL'] : 0);
		/*END*/
		/* NOTIFIKASI DISPOSISI */
		$parameter	= array('api_kode' => 80001,'api_subkode' => 2,'api_search' => array('D', 1, $kd_pgw, $str_id));
		$tot 				= $ci->mdl_tnde->get_api('tnde_surat_masuk/get_distribusi_pegawai','json','POST', $parameter);
		$hasil['DM']	= (!empty($tot['TOTAL']) ? $tot['TOTAL'] : 0);
		/*END*/
		/* NOTIFIKASI TERUSAN MEMO */
		$parameter	= array('api_kode' => 80001,'api_subkode' => 2,'api_search' => array('TM', 1, $kd_pgw, $str_id));
		$tot 				= $ci->mdl_tnde->get_api('tnde_surat_masuk/get_distribusi_pegawai','json','POST', $parameter);
		$hasil['TM']	= (!empty($tot['TOTAL']) ? $tot['TOTAL'] : 0);
		/*END*/
		return $hasil;
	}
	
	function tgl_libur_by_periode($tgl1='', $tgl2=''){
		$ci =& get_instance();
		$date1 = str_replace("/", "-", $tgl1);
		$date2 = str_replace("/", "-", $tgl2);
		$parameter = array('api_kode' => 1000, 'api_subkode' => 2, 'api_search' => array($date1, $date2));
		$libur	 		= $ci->mdl_tnde->api_ikd('ikd_admin/Lihat_hari_lbur', 'json', 'POST', $parameter);
		$hasil 			= array();
		if(!empty($libur)){
			foreach($libur as $val){
				$hasil[$val['TANGGAL']][] = $val;
			}
		}
		return $hasil;
	}
	
	function tgl_libur_pns_by_periode($tgl1='', $tgl2=''){
		$ci =& get_instance();
		$asal	= str_replace("/", "-", $tgl1);
		$dt		= new DateTime($asal);
		$dt->modify('-1 day');
		$from	= $dt->format('d-m-Y');
		$to 		= str_replace("/", "-", $tgl2);
		$hasil	= array();
		
		$parameter	= array('api_kode' => 11002, 'api_subkode' => 1, 'api_search' => array($tgl2));
		$hari_kerja	= $ci->mdl_tnde->get_api('tnde_master/get_config_hari_kerja', 'json', 'POST', $parameter);
		$hari_libur	= (!empty($hari_kerja) ? array_column($hari_kerja, 'NM_HARI') : array());
		while (strtotime($from)<strtotime($to)){
			$from  	= mktime(0,0,0,date("m",strtotime($from)),date("d",strtotime($from))+1,date("Y",strtotime($from)));
			$from	=	date("d-m-Y", $from);
			$format = str_replace("-","/", $from);
			$hari		= hari($format);
			if(! in_array($hari, $hari_libur)){
				$hasil[$format][] = "Hari ".$hari." bukan hari kerja";
			}
			$parameter	= array('api_kode' => 1000, 'api_subkode' => 3, 'api_search' => array($from));
			$tgl_libur 	= $ci->mdl_tnde->api_ikd('ikd_admin/Lihat_hari_lbur', 'json', 'POST', $parameter);
			if(!empty($tgl_libur)){
				foreach($tgl_libur as $val){
					$tgl_fix	= str_replace("-","/",$val['TANGGAL']);
					$hasil[$tgl_fix][] = huruf_kapital($val['KETERANGAN']);
				}
			}
		}
		return $hasil;
	}
	
	function tgl_libur_pns_by_tgl($tgl){
		$ci =& get_instance();
		$hari_libur	= array('Sabtu', 'Minggu');
		$hari			= hari($tgl);
		$hasil			= array();
		if(in_array($hari, $hari_libur)){
			$hasil[] = "Hari ".$hari." bukan hari kerja.";
		}else{
			$tgl_ikd		= str_replace("/", "-", $tgl);
			$parameter	= array('api_kode' => 1000, 'api_subkode' => 3, 'api_search' => array($tgl_ikd));
			$tgl_libur 	= $ci->mdl_tnde->api_ikd('ikd_admin/Lihat_hari_lbur', 'json', 'POST', $parameter);
			if(!empty($tgl_libur[0])){
				$hasil[]	= "Hari Libur ".huruf_kapital($tgl_libur[0]['KETERANGAN']);
			}
		}
		return $hasil;
	}
	
	function jab_lain($kd_pgw='', $tgl=''){
		$ci =& get_instance();
		$parameter = array('api_kode' => 1122, 'api_subkode' => 13, 'api_search' => array($tgl, $kd_pgw));
		$fungsional = $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
		$parameter 	= array('api_kode' => 20000, 'api_subkode' => 3, 'api_search' => array($kd_pgw));
		$dosen 			= $ci->mdl_tnde->api_sia('sia_dosen/data_search', 'json', 'POST', $parameter);
		$hasil['fungsional'] 		= $fungsional;
		$hasil['dosen'] 			= $dosen;
		// $hasil	= 2;
		return $hasil;
	}
	
	function kirim_email($id=''){
		// set_time_limit(900);
		$ci =& get_instance();
		$ci->load->model('mdl_tnde');
		$data['surat_keluar']  = $ci->mdl_tnde->get_api('tnde_surat_keluar/detail_surat_keluar', 'json', 'POST', array('api_kode' => 90001, 'api_subkode' => 2, 'api_search' => array($id)));

		$parameter = array('api_kode' => 2001, 'api_subkode' => 2, 'api_search' => array($data['surat_keluar']['PEMBUAT_SURAT']));
		$data['detail_pembuat'] = $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
		
		$psd = $ci->mdl_tnde->get_api('tnde_general/get_data', 'json', 'POST', array('api_kode' => 1001, 'api_subkode' => 2, 'api_search' => array('MD_PSD', 'ID_PSD', $data['surat_keluar']['ID_PSD'])));
		if(!empty($psd['KD_JABATAN'])){
			$parameter = array('api_kode' => 1121, 'api_subkode' => 9, 'api_search' => array($data['surat_keluar']['TGL_SURAT'], $psd['KD_JABATAN'], 3));
			$data['psd'] = $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);

			$parameter = array('api_kode' => 1121, 'api_subkode' => 3, 'api_search' => array(tgl($data['surat_keluar']['WAKTU_SIMPAN']), $data['psd'][0]['KD_PGW'], 1)); #tgl, kd_pgw, status aktif/tidak aktif
			$data['psd_detail']['jabatan'] = $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
		
			// $data['url'] = base_url() . 'verifikasi/psd_email?uid=' . $id . '&req=email&authid=' . $data['psd'][0] ['KD_PGW'] ;
			
			$ci->load->library('s00_lib_siaenc');
			$param = array('uid'=> $id, 'req'=>'email', 'authid'=> $data['psd'][0] ['KD_PGW']);
			
			$data_enc = $ci->s00_lib_siaenc->encrypt(json_encode($param));
		
			$data['url'] = base_url() . 'verifikasi/psd_email?req=' . $data_enc;
			
			$pesan		= $ci->load->view('mail_content', $data, true);
			$penerima	= $data['psd'][0] ['KD_PGW'] . '@uin-suka.ac.id';
			$judul 		= 'Pembuatan ' . $data['surat_keluar']['PERIHAL'];
			
			$config['protocol']		='smtp';
			$config['smtp_host']	='ssl://smtp.gmail.com';  
			$config['smtp_port']	='465';
			$config['smtp_user']	= 'surat-no-replay@uin-suka.ac.id';  
			$config['smtp_pass']	= 'surat123456';
			$config['mailtype']		='html';
			
			$ci->load->library('email', $config);
			$ci->email->set_newline("\r\n");
			$ci->email->from($config['smtp_user'], 'Sistem Informasi Surat');
			$ci->email->to($penerima);
			$ci->email->subject($judul);
			$ci->email->message($pesan);
			
			return $ci->email->send();
			/* tambahan error */
		} else {
			return false;
		}
	}
}
?>
