<!DOCTYPE html>
<html>
<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">                	
		<!-- <meta name="viewport" content="width=device-width,initial-scale=1"> -->
		<title>Sistem Informasi Surat UIN Sunan Kalijaga</title>
		
        <link href="http://static.uin-suka.ac.id/images/favicon.png" type="image/x-icon" rel="shortcut icon">		
		<link href="http://static.uin-suka.ac.id/plugins/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
		<link href="http://surat.uin-suka.ac.id/asset/css/bootstrap-timepicker.css" rel="stylesheet" type="text/css"/>
		<link type="text/css" href="http://surat.uin-suka.ac.id/asset/css/ui.multiselect.css" rel="stylesheet" />
		<link href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
		<!-- <link href="http://surat.uin-suka.ac.id/asset/css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css"/> -->

		<link href="http://exp.uin-suka.ac.id/surat/assets/css/staticuin/style_global.css" rel="stylesheet" type="text/css"/>
		<link href="http://static.uin-suka.ac.id/css/style_layout.css" rel="stylesheet" type="text/css"/>		
		<link href="http://surat.uin-suka.ac.id/asset/css/tnde.css" rel="stylesheet" type="text/css"/>
		<link href="<?= base_url('assets/css/style_tnde_sakad.css'); ?>" rel="stylesheet" type="text/css"/><!-- <- - - custom css untuk sakad -->
		
		<link href="http://static.uin-suka.ac.id/css/docs.css" rel="stylesheet" type="text/css"/>
		<link href="http://static.uin-suka.ac.id/css/breadcrumb.css" rel="stylesheet" type="text/css"/>
				
		<script src="http://exp.uin-suka.ac.id/surat/assets/js/jquery-2.2.4.min.js"></script>						
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
		<script type="text/javascript" src="http://surat.uin-suka.ac.id/asset/js/ui.multiselect.js"></script>
		<script src="http://exp.uin-suka.ac.id/surat/assets/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?= base_url('assets/js/adminlte.min.js'); ?>"></script>
		<link rel="stylesheet"
          href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
          crossorigin="anonymous">
         
		<script type="text/javascript" src="<?php echo base_url();?>assets/jsplugin/jquery-tokeninput/jquery.tokeninput_editbaru.js"></script>
		<link rel="stylesheet" href="<?php echo base_url();?>assets/jsplugin/jquery-tokeninput/token-input.css" type="text/css" />

		<!-- <script type="text/javascript" src="<?php echo base_url('assets/jsplugin/select2-4.0.5/js/select2.min.js'); ?>"></script> -->
		<script type="text/javascript" src="<?php echo base_url('assets/jsplugin/select2-4.0.5/js/select2.full.min.js'); ?>"></script>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/jsplugin/select2-4.0.5/css/select2.min.css'); ?>">
		<script type="text/javascript" src="<?php echo base_url('assets/js/suratfunctions.js'); ?>"></script>

		<script type="text/javascript" src="http://surat.uin-suka.ac.id/asset/js/bootstrap-timepicker.js"></script>	
		<style type="text/css">
			.ui-autocomplete{
				max-height: 180px;
				max-width: 440px;
				overflow: auto;
			}
		</style>				
</head>
<body>
	<div class="app_header-top"></div>
	<div class="app_main">
		<div class="app_header">
			<div class="center">
				<div class="app_uin_id">
					<a href="http://exp.uin-suka.ac.id/surat/" ></a>
				</div>
				<div class="app_header_right">
					<div class="app_system_id">Sistem Informasi Surat</div>
					<div class="app_univ_id">UIN Sunan Kalijaga</div>
					<div class="app_subsystem_id badge">Automasi Persuratan Mahasiswa</div>
				</div>
			<div class="clear"></div>
			</div>
		</div>

		<div id="app_content">
			<div class="app-row">

			<!-- next codes: view sidebar, crumb, content, footer -->