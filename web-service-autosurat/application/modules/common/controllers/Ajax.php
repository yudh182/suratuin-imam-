<?php if(!defined('BASEPATH'))exit('No direct script access allowed');

class Ajax extends MY_Controller{
	function __construct(){
		parent::__construct();
		$this->load->library('Curl');
		$this->load->model('Mdl_tnde', 'apiconn');	
		$this->load->helper('Tnde_general');			
	}


	/**
	* Sumber Data : API TNDE - Master
	*/
	function auto_klasifikasi_surat(){
		if(ISSET($_POST['q'])){
			// $referer = (!empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '');
			// $auth = host_allowed($referer);
			// if($auth){
				$q = strtoupper(str_replace("'", "''", $_POST['q']));
				$tot = count(str_split($q));
				if($tot > 0){
					$parameter = array('api_kode' => 11000, 'api_subkode' => 1,'api_search' => array($q));
					$data = $this->apiconn->api_tnde('tnde_master/get_klasifikasi_surat', 'json', 'POST', $parameter);
					if(count($data) > 0){
						foreach($data as $val){
							$return_arr[] = array('name' => $val['KD_KLASIFIKASI_SURAT']." - ".$val['NM_KLASIFIKASI_SURAT'], 'id' => $val['ID_KLASIFIKASI_SURAT']);
						}
						$joss = json_encode($return_arr);
						echo $joss;
					}else{
						echo 0;
					}
				}
			//}
		}
	}

	/**
	* Sumber Data : API TNDE - Master
	*/
	function auto_psd_v2(){
		if(!empty($_GET['id'])){
			$unit_id 	= $_GET['id'];
			$psd				= $this->sisurat_general->psd_by_unit_id($unit_id);
			$select_data	= array();
			if(!empty($psd)){
				$n = 1;
				foreach($psd as $val){
					$parameter 	= array('api_kode' => 1101, 'api_subkode' => 1, 'api_search' => array($val['KD_JABATAN']));
					$jab		 		= $this->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
					$select_data[] = array('optionValue' => $val['ID_PSD'], 'optionDisplay' => $n.". ".$val['KD_PSD']." - ".$jab[0]['STR_NAMA']);
					$n++;
				}
			}
			$hasil = json_encode($select_data);
			echo $hasil;
		}
	}


	function auto_psd_custom()
	{
		$q = strtoupper($_GET['q']);
		/*$parameter 	= array('api_kode' => 1121, 'api_subkode' => 4, 'api_search' => array($q, 1));
		$simpeg 		= $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);*/
		$parameter					= array('api_kode' => 11002, 'api_subkode' => 3, 'api_search' => array($q, '', 100)); //tanpa paging, tanpa jab list
		$data['psd_master'] 		= $this->apiconn->api_tnde('tnde_master/get_psd', 'json', 'POST', $parameter);

		echo var_dump($data['psd_master']);


		/*$parameter	= array('api_kode' => 110012, 'api_subkode' => 2, 'api_search' => array($pjb_list));
		$psd				= $ci->mdl_tnde->get_api('tnde_master/get_psd', 'json', 'POST', $parameter);
		if(!empty($psd)){
			foreach($psd as $val){
				if(! in_array($val['ID_PSD'], $tmp_psd)){
					$psd_arr[] 		= $val;
					$tmp_psd[] 	= $val['ID_PSD'];
				}
			}
		}*/
	}

	function psd_master($opsi='semua')
	{				
		if ($opsi == 'semua'){
			$parameter					= array('api_kode' => 11001, 'api_subkode' => 1, 'api_search' => array(100));
			$data['psd_master'] 		= $this->apiconn->api_tnde('tnde_master/get_psd', 'json', 'POST', $parameter);


		} elseif ($opsi == 'simpeg_cari') {
			$keyword = 'WAKIL DEKAN BIDANG AKADEMIK';

			$parameter 	= array('api_kode' => 1121, 'api_subkode' => 4, 'api_search' => array($keyword, 1));
			$simpeg 		= $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);

			/*
			$jab			= array_column($simpeg, 'STR_ID');
			$jab_list		= implode_to_string($jab);
			$parameter					= array('api_kode' => 11002, 'api_subkode' => 2, 'api_search' => array($keyword, $jab_list, 100, 1));
			$data['psd_master'] 		= $this->apiconn->get_api('tnde_master/get_psd', 'json', 'POST', $parameter);*/
			//$data['psd_master'] = $jab_list;
			$data['psd_master'] = $simpeg;

		} elseif ($opsi == 'jabatan_in_unit') {
			$unit = $_GET['unit_id'];
			$parameter 	= array('api_kode' => 1121, 'api_subkode' => 8, 'api_search' => array(date('d/m/Y'), $unit, 1));
			$simpeg 		= $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);

			$data['psd_master'] = $simpeg;

		}
		
		echo var_dump($data['psd_master']);	
	}


	/**
	* Sumber Data : API SIMPEG
	*/
	function auto_unit(){
		if(ISSET($_POST['q'])){
			$referer = (!empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '');
			$auth = host_allowed($referer);
			if($auth){
				$q = str_replace("'", "''", $_POST['q']);
				$tot = count(str_split($q));
				// echo "KEYWORD = ".$q."<br/>";
				if($tot > 1){
					$parameter 	= array('api_kode' => 1001, 'api_subkode' => 6, 'api_search' => array($q));
					$unit 				= $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
					if(!empty($unit)){
						foreach($unit as $val){
							$hasil[]	= array('name' => $val['UNIT_NAMA'], 'id' => $val['UNIT_ID']);
						}
						echo json_encode($hasil);
					}else{
						echo 0;
					}
				}
			}
		}
	}

	/**
	* Sumber Data : API SIMPEG
	*/
	function auto_pejabat(){
		if(ISSET($_POST['q'])){
			$referer = (!empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '');
			$auth = host_allowed($referer);
			if($auth){
				$q = str_replace("'", "''", $_POST['q']);
				$tot = count(str_split($q));
				// echo "KEYWORD = ".$q."<br/>";
				if($tot > 1){
					$parameter = array('api_kode' => 1121, 'api_subkode' => 4, 'api_search' => array($q, 1));
					$simpeg = $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
					if(!empty($simpeg)){
						foreach($simpeg as $val){
							if(!empty($val['GRP_C'])){ #cek grup jabatan
								// $exp = explode("@", $val['GRP_C']);
								// $kel = (!empty($exp[2]) ? $exp[2] : '');
								// $exp2 = explode("#", $kel);
								if(strpos(strtoupper($val['GRP_C']), 'KELPJB')){ #PEJABAT									
									$temp[] = array(
										'KD_PGW' => $val['KD_PGW'],
										'NM_PGW_F' => str_replace("'", "", $val['NM_PGW_F']),
										'STR_ID' => $val['STR_ID'],
										'STR_NAMA' => str_replace("'", "", $val['STR_NAMA']),
										'STR_NAMA_S1' => str_replace("'", "", $val['STR_NAMA_S1']),
										'STR_NAMA_S2' => str_replace("'", "", $val['STR_NAMA_S2'])
									);
								}
							}
						}
					}
					if(!empty($temp)){
						foreach($temp as $val){
							$id = $val['KD_PGW']."#".$val['STR_ID']."#PJB01#".$val['STR_NAMA']."#".$val['NM_PGW_F'];
							$return_arr[] = array('name' => str_replace("'","", $val['STR_NAMA'])." [".str_replace("'","", $val['NM_PGW_F'])."]", 'id' => $id, 'kd_pgw' => $val['KD_PGW'], 'str_id' => $val['STR_ID']);
						}
						$joss = json_encode($return_arr);
						echo $joss;
					}else{
						echo 0;
					}
				}
			}
		}
	}

	/**
	* Get jabatan-jabatan struktural (bukan pejabatnya)
	* todo : ganti ke api jabatan. sementara pakai api get pegawai/pejabat aktif
	*/
	function auto_jabatan()
	{
		$keyword = $_POST['q']; //$keyword = 'WAKIL DEKAN BIDANG AKADEMIK';
		$parameter 	= array('api_kode' => 1121, 'api_subkode' => 4, 'api_search' => array($keyword, 1));
		$get_data 		= $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);

		if (count($get_data) > 0){			
			#FORMAT UNTUK TOKENINPUT			
			if (!empty($get_data)){
				foreach($get_data as $val){
					$id = $val['STR_ID']."#PJB01#".$val['STR_NAMA'];
					$return_arr[] = array('name' => str_replace("'","", $val['STR_NAMA'])." [".str_replace("'","", $val['STR_ID'])."]", 'id' => $id, 'str_id' => $val['STR_ID']);
				}

				$joss = json_encode($return_arr, JSON_PRETTY_PRINT);
			} else {
				$joss = 0;
			}
			
			echo $joss;
		}		
	}

}