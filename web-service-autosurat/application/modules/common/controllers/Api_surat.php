<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');

class Automasi_surat_mhs extends MY_Controller {
	
	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('log') !== 'in')
			redirect(site_url('pgw/login?from='.current_url()));

		//$this->load->model('Mdl_tnde', 'apiconn'); #autoload
		$this->load->helper('form');				
		
		$this->load->model('Logic_Verifikasi');	
		$this->load->model('Verifikasi_model');			
		$this->load->model('Automasi_model');		
    }

    public function save_surat_keluar($data=array())
    {
    	$data_sesi = [
            'ID_SURAT_KELUAR' => uniqid().'p',
            'TGL_SURAT' => '07/02/2018',
            'PERIHAL' => 'AKTIF KULIAH A.N BBNG',
            'NO_SURAT' => '',
            'TEMPAT_DIBUAT' => '34043',
            'TEMPAT_TUJUAN' => '',
            'KD_JENIS_ORANG' => 'M',
            'ID_PSD' => '76',
            'ATAS_NAMA' => '7',
            'ID_KLASIFIKASI_SURAT' => '325',
            //'NO_URUT' => '601',
            'NO_URUT' => '',
            'NO_SELA' => '',
            'ISI_SURAT' => 'Percobaan buat surat keterangan oleh mahasiswa melalui sistem automasi surat 2018 kedua.',
            'KD_SIFAT_SURAT' => 'B',
            'KD_KEAMANAN_SURAT' => 'B',
            'PEMBUAT_SURAT' => $this->input->userdata('username'),
            'STATUS_SK' => '0',
            'KD_JENIS_SURAT' => '11',
            'UNTUK_UNIT' => 'UN02006',
            'LAMPIRAN' => '-',
            //'JABATAN_PEMBUAT' => 'JS0000Q2', 
            'JABATAN_PEMBUAT' => '', 
            'KD_STATUS_SIMPAN' => '1',
            'KD_PRODI' => '',
            'UNTUK_BELIAU' => '',
            'KOTA_TUJUAN' => '',
            'WAKTU_SIMPAN' => date("d/m/Y H:i:s"),
            'UNIT_ID' => 'UN02006'
        ];

    	#2. Modif kolom-kolom untuk proses data
		//$data_sk['ID_SURAT_KELUAR'] = 'T.'.uniqid();
		//$data_sk['ID_PSD'] = $get_sesi['PSD_ID'];
		//$data_sk['ATAS_NAMA'] = $get_sesi['PSD_AN'];
		//$data_sk['UNTUK_BELIAU'] = $get_sesi['PSD_UB'];				


			if($data_sk['KD_STATUS_SIMPAN'] == '2' || $data_sk['KD_STATUS_SIMPAN'] == 2){ #2 BERNOMOR
				#dapatkan nomor
				if(empty($data['errors'])){
					$parameter	= array('api_kode' => 90002, 'api_subkode' => 3, 'api_search' => array($data_sk));
					$req_no 		= $this->apiconn->api_tnde('tnde_surat_keluar/penomoran', 'json', 'POST', $parameter);
					if(empty($req_no['ERRORS'])){
						$data_sk['NO_URUT']	= (!empty($req_no['NO_URUT']) ? $req_no['NO_URUT'] : 0);
						$data_sk['NO_SELA']		= (!empty($req_no['NO_SELA']) ? $req_no['NO_SELA'] : 0);
						$data_sk['NO_SURAT']	= (!empty($req_no['NO_SURAT']) ? $req_no['NO_SURAT'] : 0);
					}else{
						$save_sk = FALSE;
						$data['errors'] = $req_no['ERRORS'];
					}
				}
			} else { #1 (draf)
				$data_sk['NO_URUT']		= '';
				$data_sk['NO_SELA']		= '';
				$data_sk['NO_SURAT']	= '';
			}
			
			$key_diikutkan = ['ID_SURAT_KELUAR', 'KD_STATUS_SIMPAN', 'KD_JENIS_SURAT', 'ID_PSD', 'ATAS_NAMA', 'UNTUK_BELIAU', 'ID_KLASIFIKASI_SURAT', 'TGL_SURAT', 'STATUS_SK', 'PEMBUAT_SURAT', 'JABATAN_PEMBUAT', 'KD_JENIS_ORANG','UNIT_ID', 'UNTUK_UNIT', 'PERIHAL', 'TEMPAT_DIBUAT', 'TEMPAT_TUJUAN', 'KOTA_TUJUAN', 'ALAMAT_TUJUAN', 'LAMPIRAN', 'KD_SIFAT_SURAT', 'KD_KEAMANAN_SURAT', 'WAKTU_SIMPAN', 'KD_SISTEM', 'NO_URUT', 'NO_SELA', 'ISI_SURAT', 'KD_PRODI', 'NO_SURAT'];			
			$data_sk = array_intersect_key($data_sk, array_flip($key_diikutkan));
			//$data['insert_surat_keluar'] = $data_sk; //assign data ke variabel lain
			$parameter  = array('api_kode' => 90001, 'api_subkode' => 1, 'api_search' => array($data_sk));
	        $save_sk        = $this->apiconn->api_tnde('tnde_surat_keluar/insert_surat_kaluar', 'json', 'POST', $parameter);
	        if($save_sk ==  FALSE){
	            $data['errors'][] = "Gagal menyimpan surat. #0";
	        }else{
	            $save_sk    =   TRUE;
	        }


		#2. Melakukan API Insert ke SISURAT	(surat_keluar) - belum aktif

			#3. ======== Melakukan API INSERT PENERIMA_SK KE SISURAT ========
			$kpd_mahasiswa = (!empty($get_sesi['D_PENERIMA_MHS']) ? explode('<$>', $get_sesi['D_PENERIMA_MHS']) : null);
			$kpd_pejabat = (!empty($get_sesi['D_PENERIMA_PJB']) ? explode('<$>', $get_sesi['D_PENERIMA_PJB']) : null);
			$kpd_pegawai = (!empty($get_sesi['D_PENERIMA_PGW']) ? explode('<$>', $get_sesi['D_PENERIMA_PGW']) : null);
			$ts_pejabat = (!empty($get_sesi['D_TEMBUSAN_PJB']) ? explode('<$>', $get_sesi['D_TEMBUSAN_PJB']) : null);
			$ts_eks = (!empty($get_sesi['D_TEMBUSAN_EKS']) ? explode('<$>', $get_sesi['D_TEMBUSAN_EKS']) : null);

			#============== 3.1 Pengelola Surat (PS)==============
			# 3.1 KIRIM SURAT KE PEJABAT (Yang menerangkan, menandatangani, dsb)
			if(!empty($kpd_pejabat[0])){
				$save_pjb = [];
				foreach($kpd_pejabat as $key=> $val){
					$exp = explode("#", $val);
					$get_pjb = $this->Repo_Simpeg->get_pejabat_aktif($data['insert_surat_keluar']['TGL_SURAT'], $exp[0]);						
					if(count($exp) == 2){
						$pjb['ID_SURAT_KELUAR']				= $data_sk['ID_SURAT_KELUAR'];
						$pjb['PENERIMA_SURAT'] = $get_pjb[0]['NIP']; //NIP PEGAWAI
						$ts_pjb['JABATAN_PENERIMA'] = $exp[0];
						$pjb['KD_GRUP_TUJUAN'] = 'PJB01';
						$pjb['KD_STATUS_DISTRIBUSI'] = "PS";
						$pjb['ID_STATUS_SM'] = 1;
						$pjb['KD_JENIS_TEMBUSAN'] = 0;
						$pjb['NO_URUT'] = $key + 1;
						$pjb['KD_JENIS_KEPADA']				= $exp[1];
						$save_pjb[] = $pjb;
						/*$save_mhs = $this->apiconn->get_api('tnde_surat_keluar/insert_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($mhs)));
						if($save_mhs == FALSE){
							$data['errors'][] = "Gagal mengirim ke tujuan surat ke ".$exp[0]; // Mahasiswa
						}*/
					}
				}

				$data['distribusi_ps']['penerima_pejabat'] = $save_pjb;
			}

			# 3.2 KIRIM SURAT KE MAHASISWA
			if(!empty($kpd_mahasiswa[0])){
				$save_mhs = [];
				foreach($kpd_mahasiswa as $key=> $val){
					$exp = explode("#", $val);
					if(count($exp) == 2){
						$mhs['ID_SURAT_KELUAR']				= $data_sk['ID_SURAT_KELUAR'];
						$mhs['PENERIMA_SURAT'] = $exp[0];
						$mhs['JABATAN_PENERIMA'] = '0';
						$mhs['KD_GRUP_TUJUAN'] = 'MHS01';
						$mhs['KD_STATUS_DISTRIBUSI'] = "PS";
						$mhs['ID_STATUS_SM'] = 1;
						$mhs['KD_JENIS_TEMBUSAN'] = 0;
						$mhs['NO_URUT'] = $key + 1;
						$mhs['KD_JENIS_KEPADA']				= $exp[1];
						$save_mhs[] = $mhs;
						/*$save_mhs = $this->apiconn->get_api('tnde_surat_keluar/insert_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($mhs)));
						if($save_mhs == FALSE){
							$data['errors'][] = "Gagal mengirim ke tujuan surat ke ".$exp[0]; // Mahasiswa
						}*/
					}
				}

				$data['distribusi_ps']['penerima_mahasiswa'] = $save_mhs;
			}
			
			# 3.3 KIRIM SURAT KE EKSTERNAL
			$kpd_eks = (!empty($get_sesi['D_PENERIMA_EKS']) ? explode('<$>', $get_sesi['D_PENERIMA_EKS']) : null);
			if(!empty($kpd_eks[0])){
				$save_eks = [];					
				foreach($kpd_eks as $key => $val){
					$exp = explode("#", $val);

					if(count($exp) == 2){
						$eks['ID_SURAT_KELUAR']				= $data_sk['ID_SURAT_KELUAR'];
						$eks['PENERIMA_SURAT']				= $exp[0];
						$eks['JABATAN_PENERIMA']			= $exp[0]; //misal: kepala, kabag, sekda, dsb
						$eks['KD_GRUP_TUJUAN']				= "EKS01";
						$eks['KD_STATUS_DISTRIBUSI']		= "PS";	
						$eks['ID_STATUS_SM'] = 1;
						$eks['KD_JENIS_TEMBUSAN'] = 0;
						$eks['NO_URUT'] = $key + 1;
						$eks['KD_JENIS_KEPADA']				= $exp[1]; //1 = untuk perhatian , 0
						$save_eks[] = $eks;
						/*$save_mhs = $this->apiconn->get_api('tnde_surat_keluar/insert_penerima_sk', 'json', 'POST', array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($mhs)));
						if($save_mhs == FALSE){
							$data['errors'][] = "Gagal mengirim ke tujuan surat ke ".$exp[0]; // Mahasiswa
						}*/												
					}
				}

				$data['distribusi_ps']['penerima_eksternal'] = $save_eks;
			}

			#============== 3.2 Tembusan Surat (TS)==============    			
			if(!empty($ts_pejabat)){
				$ts_urut = 0;
				foreach($ts_pejabat as $key => $val){
					$exp = explode("#", $val);
					$get_pjb = $this->Repo_Simpeg->get_pejabat_aktif($data['insert_surat_keluar']['TGL_SURAT'], $exp[0]);						
					if(count($exp) == 2 ){
						$ts_pjb['ID_SURAT_KELUAR'] 				= $data['insert_surat_keluar']['ID_SURAT_KELUAR'];
						$ts_pjb['PENERIMA_SURAT'] 				= $get_pjb[0]['NIP']; //NIP PEGAWAI
						$ts_pjb['JABATAN_PENERIMA'] 			= $exp[0];
						$ts_pjb['KD_GRUP_TUJUAN'] 				= 'PJB01';
						$ts_pjb['KD_STATUS_DISTRIBUSI'] 	= "TS";
						$ts_pjb['ID_STATUS_SM'] 						= "1";
						$ts_pjb['NO_URUT']									= $ts_urut + 1;
						$ts_pjb['KD_JENIS_TEMBUSAN'] 		= (string)$exp[1]; #1 = sebagai laporan, 0 = biasa
						$ts_pjb['KD_JENIS_KEPADA'] 				= '0';					
						$save_ts_pjb = $ts_pjb;
						/*$parameter	= array('api_kode' => 90009, 'api_subkode' => 1, 'api_search' => array($ts_pjb));
						$save_ts_pjb = $this->mdl_tnde->get_api('tnde_surat_keluar/insert_penerima_sk', 'json', 'POST', $parameter);
						if($save_ts_pjb == FALSE){
							$data['errors'][] = "Gagal mengirim tembusan surat ke ".$exp[3]; // Pejabat
						}*/
					}
				}

				$data['distribusi_ts']['tembusan_pejabat'] = $save_ts_pjb;
			}
    }
				



//usage
$parameter	= array('api_kode' => 90001, 'api_subkode' => 1, 'api_search' => array($data_sk));
$save_sk 		= $this->apiconn->api_tnde('tnde_surat_keluar/insert_surat_kaluar', 'json', 'POST', $parameter);