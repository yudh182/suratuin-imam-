<?php

if (!defined('BASEPATH'))exit('No direct script access allowed');

class Mailer extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->helper('form');

		$this->load->library('form_validation');
		// $this->load->library('encrypt');
		$this->load->library('email');
	}

	public function index()
	{
		$help = [
			'judul' => 'Email ke pengguna melalui aplikasi surat',
			'available_methods' => [
				'kirim_email' => 'Mengirim email yang berisi pesan sederhana (tanpa callback action)',
				'kirim_email_template' => 'Mengirim email yang berisi konten html'				
			]
		];

		$this->output->set_status_header(200)->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($help, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));

	}

	// Send Gmail to another user
	public function kirim_email() {

		//SMTP & mail configuration
		$config = array(
		    'protocol'  => 'smtp',
		    'smtp_host' => 'smtp.gmail.com',
		    'smtp_crypto' => 'ssl',
		    'smtp_port' => 465,
		    //'smtp_user' => 'muchlazz@gmail.com',
		    //'smtp_pass' => 'agamakuislamIsSecrd@GOOGL13',
		 	'smtp_user'	=> 'surat-no-replay@uin-suka.ac.id',
			'smtp_pass'	=> 'surat123456',
			'smtp_timeout' => '30',
			'mailpath' => '/usr/sbin/sendmail',
		    'mailtype'  => 'html',
		    'newline' => "\r\n",
		    'crlf' => "\r\n",
		    'charset'   => 'utf-8',
		    'dsn' => true,
		);
		// $config['newline'] = "\r\n"; 

		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");

		// penerima
		$recipient = '11650021@student.uin-suka.ac.id';

		//Email content
		$htmlContent = '<h1>Sending email via SMTP server</h1>';
		$htmlContent .= '<p>This email has sent via SMTP server from CodeIgniter application.</p>';

		// $this->email->to('muhajiros.space@gmail.com');
		// $this->email->from('muchlazz@gmail.com','Sistem Automasi Surat');
		$this->email->to($recipient);
		$this->email->from($config['smtp_user'], 'Sistem Informasi Surat');
		$this->email->subject('How to send email via SMTP server in CodeIgniter');
		$this->email->message($htmlContent);


		if ($this->email->send()) {
			echo 'Email berhasil dikirimkan ke '.$recipient;
		} else {
			show_error($this->email->print_debugger());
		}		
	}



	public function cek_ports()
	{
		$fp = fsockopen('localhost', 80, $errno, $errstr, 5);
		if (!$fp) {
	        echo('port 80 is closed or blocked.');
	    } else {
	        echo('port 80 is open and available');
	        fclose($fp);
	    }

	    $fp = fsockopen('localhost', 443, $errno, $errstr, 5);
		if (!$fp) {
	        echo('port 443 is closed or blocked.');
	    } else {
	        echo('port 443 is open and available');
	        fclose($fp);
	    }

	   $fp = fsockopen('localhost', 25, $errno, $errstr, 5);
	    if (!$fp) {
	        echo('port 25 is closed or blocked.');
	    } else {
	        echo('port 25 is open and available');
	        fclose($fp);
	    }
	   $fp = fsockopen('localhost', 587, $errno, $errstr, 5);
	    if (!$fp) {
	        echo('port 587 is closed or blocked.');
	    } else {
	        echo('port 587 is open and available');
	        fclose($fp);
	    }
	   $fp = fsockopen('localhost', 465, $errno, $errstr, 5);
	    if (!$fp) {
	        echo('port 465 is closed or blocked.');
	    } else {
	        echo('port 465 is open and available');
	        fclose($fp);
	    }
	}

	public function cek_crypto()
	{		
		echo crypt("petanikode", "garam");
	}

	public function cek_ssl()
	{


	}

}