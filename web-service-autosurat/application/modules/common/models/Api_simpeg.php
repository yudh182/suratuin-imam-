<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api_simpeg extends CI_Model
{
	public function __construct()
	{
		parent::__construct();             		
        //$this->load->library('S00_lib_api'); //dari library root app
	}

	public function get_profil_pegawai($username) //By NIP
	{
		$parameter = array('api_kode' => 2001, 'api_subkode' => 2, 'api_search' => array($username));
		$simpeg_pgw = $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
		return $simpeg_pgw;
	}

	public function get_jabatan_struktural($username) // By NIP
	{
		$parameter = array('api_kode' => 1121, 'api_subkode' => 3, 'api_search' => array(date('d/m/Y'), $username, 1));
		$jab_str = $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);

		return $jab_str;	
	}

	public function get_jabatan_fungsional($username)
	{
		$parameter = array('api_kode' => 1122, 'api_subkode' => 3, 'api_search' => array(date('d/m/Y'), $username, 1));
		$jab_fung = $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);

		return $jab_fung;
	}

	public function get_pejabat_psd($tgl_surat=null, $kd_jabatan=null)
	{		
		$parameter = array('api_kode' => 1121, 'api_subkode' => 9, 'api_search' => array($tgl_surat, $kd_jabatan, 3));
		$data_psd = $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
		return $data_psd;
	}


	/**
	* Get Profil Pejabat (menempati suatu jabatan aktif)  pada suatu tanggal
	*/
	public function get_pejabat_aktif($tgl_surat=null, $kd_jabatan=null)
	{		
		$parameter = array('api_kode' => 1121, 'api_subkode' => 9, 'api_search' => array($tgl_surat, $kd_jabatan, 3));
		$data_pgw = $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
		return $data_pgw;
	}
	

	public function check_anggota_unit($unit_id){        
        $parameter	= array('api_kode'=>1121, 'api_subkode' => 15, 'api_search' => array($unit_id) ); 
		$request	= $this->apiconn->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter); //

		return $request;
        //$datar = $this->siaapi_getdata('simpeg_public/simpeg_mix/data_search',1121,15,'api_search',$parameter);
        //$datrr = json_decode($datar,true);

        /*$result = array();
        foreach ($datrr as $value){
            array_push($result, array(
                'nip'   => $value['NIP'],
                'nama'  => $value['NM_PGW_F']
            ));
        }
        echo json_encode($result, JSON_PRETTY_PRINT);*/
    }

}