<?php
/**
* Konversi kode entitas yang berbeda antar sistem
* misal : 
*	- entitas fakultas saintek, di SIA kodenya 06 sementara di SIMPEG kodenya = UN02006
* 	- entitas jabatan pejabat penandatangan, dekan saintek di SISURAT kodenya 2 sementara di SIMPEG kodenya = ST000306, dsb.
*/
class Konversi_model extends CI_Model {

    function __construct() {
        parent::__construct();    
    }

    function convert_kdfak_ke_unitid($kd_fak){
		//$kd_fak = (int)$kd_fak;
        switch ($kd_fak) {
            case '01':
                return 'UN02001';
                break;
            case '02':
                return 'UN02002';
                break;
            case '03':
                return 'UN02003';
                break;
            case '04':
                return 'UN02004';
                break;
            case '05':
                return 'UN02005';
                break;
            case '06':
                return 'UN02006';
                break;
            case '07':
                return 'UN02007';
                break;            
            case '08':
                return 'UN02008';
                break;
            default:
                return null;
                break;
        }
    }

    function website_unit($unit_id){
        switch ($unit_id) {
            case 'UN02001':
                return 'http://adab.uin-suka.ac.id';
                break;
            case 'UN02002':
                return 'http://dakwah.uin-suka.ac.id';
                break;
            case 'UN02003':
                return 'http://syariah.uin-suka.ac.id';
                break;
            case 'UN02004':
                return 'http://tarbiyah.uin-suka.ac.id';
                break;
            case 'UN02005':
                return 'http://ushuluddin.uin-suka.ac.id';
                break;
            case 'UN02006':
                return 'http://saintek.uin-suka.ac.id';
                break;
            case 'UN02007':
                return 'http://fishum.uin-suka.ac.id';
                break;
            case 'UN02008':
                return 'http://febi.uin-suka.ac.id';
                break;
            default:
                return 'http://uin-suka.ac.id';
                break;
        }

    }
}