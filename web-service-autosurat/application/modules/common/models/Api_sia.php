<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api_sia extends CI_Model
{
	public function __construct()
	{
		parent::__construct();            
        //$this->load->library('S00_lib_api'); //dari library root app
        //$this->load->model('common/Mdl_tnde','apiconn'); dapat diakses langsung karena terdaftar di autoload
	}
	

    public function get_tahun_ajaran($opsi='info_ta_smt')
    {
    	//$ta = $CI->s00_lib_api->get_api_json(URL_API_SIA.'sia_krs/data_procedure', 'POST', array('api_kode'=>50000, 'api_subkode'=>2));

    	$parameter	= array('api_kode'=>50000, 'api_subkode' => 2 ); 
		$ta	= $this->apiconn->api_sia('sia_krs/data_procedure', 'json', 'POST', $parameter);		
		
		if ($opsi == 'info_ta_smt'){
			$resp = [
				'semester_sekarang' => $ta['data'][':hasil4'],
				'tahun_ajaran' => $ta['data'][':hasil2']
			];
		} else {
			$resp = $ta['data'];
		}
		return $resp;

    }
    
    /*public function get_makul_smt($nim=null)
	{
        $CI =& get_instance();
        $ta = $CI->s00_lib_api->get_api_json(URL_API_SIA.'sia_krs/data_procedure', 'POST', array('api_kode'=>50000, 'api_subkode'=>2));
        $cek=$CI->s00_lib_api->get_api_json(URL_API_SIA.'sia_krs/data_search', 'POST', array('api_kode'=>64000, 'api_subkode' => 22, 'api_search' => array($ta['data'][':hasil1'],$ta['data'][':hasil3'],$nim)));
        $response = array();
		$posts = array();
		for($i=0; $i<count($cek); $i++){
			$posts[] = array(
            		"nama_makul"            =>  $cek[$i]['NM_MK'],
            		"kode_makul"                  =>  $cek[$i]['KD_MK'],
            		"kode_kelas"			=> $cek[$i]['KD_KELAS']
        	);
		}
		$return = 0;
		$response['makul'] = $posts;
		$hasil = json_decode(json_encode($response), true);
		for($i=0;$i<count($hasil);$i++){ //perform linear search dengan looping dan pengecekan nilai if
			if($hasil['makul'][$i]['kode_makul'] == 'TIF404033'){
				$return = 'succes';
				break;
			}
		}

		//return $return;
		return $hasil; 
    }
    */

    /**
    * Dapatkan mata kuliah yang diikuti semester ini
    * todo : API CEK TAHUN AJARAN DAN SEMESTER SAAT INI
    */
    public function get_makul_smt($nim=null,$opsi='')
	{
        $CI =& get_instance();

        $kd_makul = null;        

        if (empty($opsi)){
        	$ta = $CI->s00_lib_api->get_api_json(URL_API_SIA.'sia_krs/data_procedure', 'POST', array('api_kode'=>50000, 'api_subkode'=>2));
	    	$cek=$CI->s00_lib_api->get_api_json(URL_API_SIA.'sia_krs/data_search', 'POST', array('api_kode'=>64000, 'api_subkode' => 22, 'api_search' => array($ta['data'][':hasil1'],$ta['data'][':hasil3'],$nim)));
		    //$hasil = json_decode($cek);
		        /*
		        $response = array();
				$posts = array();

				for($i=0; $i<count($cek); $i++){
					$posts[] = array(
		            		"nama_makul"            =>  $cek[$i]['NM_MK'],
		            		"kode_makul"                  =>  $cek[$i]['KD_MK'],
		            		"kode_kelas"			=> $cek[$i]['KD_KELAS']
		        	);
				}
				$return = 0;
				$response['makul'] = $posts;
				$hasil = json_decode(json_encode($response), true);
				for($i=0;$i<count($hasil);$i++){ //perform linear search dengan looping dan pengecekan nilai if
					if($hasil['makul'][$i]['kode_makul'] == 'TIF404033'){
						$return = 'succes';
						break;
					}
				}*/	
        } elseif ($opsi == 'cari_TA'){
        	$kd_makul = 'USK403007';

        	$ta = $CI->s00_lib_api->get_api_json(URL_API_SIA.'sia_krs/data_procedure', 'POST', array('api_kode'=>50000, 'api_subkode'=>2));
        	$cek=$CI->s00_lib_api->get_api_json(URL_API_SIA.'sia_krs/data_search', 'POST', array('api_kode'=>64000, 'api_subkode' => 22, 'api_search' => array($ta['data'][':hasil1'],$ta['data'][':hasil3'],$nim)));        	
        } elseif ($opsi == 'cari_KP') {
        	$kd_makul = 'TIF404045';
        } elseif  ($opsi == 'test'){
        	$cek=$CI->s00_lib_api->get_api_json(URL_API_SIA.'sia_krs/data_search', 'POST', array('api_kode'=>64000, 'api_subkode' => 22, 'api_search' => array('2015','1',$nim)));
        } elseif ($opsi == 'regex_ta_skripsi') {
        	$kd_makul = 'USK403007';

        	$ta = $CI->s00_lib_api->get_api_json(URL_API_SIA.'sia_krs/data_procedure', 'POST', array('api_kode'=>50000, 'api_subkode'=>2));
        	$cek=$CI->s00_lib_api->get_api_json(URL_API_SIA.'sia_krs/data_search', 'POST', array('api_kode'=>64000, 'api_subkode' => 22, 'api_search' => array($ta['data'][':hasil1'],$ta['data'][':hasil3'],$nim)));

        	$kolom_untuk_diregexp = '';
        }

		//return $return;
		return $cek;
    }

    /**
    * Hati-hati!! ini adalah method untuk kepentingan pengembangan saja
    */
    public function raw_get_makul_smt($opsi='smt_aktif', $data=array())
    {
    	if ($opsi == 'smt_aktif'){
    		$nim = $data['nim'];
    		$ta = $this->s00_lib_api->get_api_json(URL_API_SIA.'sia_krs/data_procedure', 'POST', array('api_kode'=>50000, 'api_subkode'=>2));
    		$cek= $this->s00_lib_api->get_api_json(URL_API_SIA.'sia_krs/data_search', 'POST', array('api_kode'=>64000, 'api_subkode' => 22, 'api_search' => array($ta['data'][':hasil1'],$ta['data'][':hasil3'],$nim)));	
    	} elseif ($opsi='smt_lalu') {
    		$nim = $data['nim'];
    		$ta_mulai = $data['ta_mulai'];
    		$smt = $data['smt'];

    		$cek= $this->s00_lib_api->get_api_json(URL_API_SIA.'sia_krs/data_search', 'POST', array('api_kode'=>64000, 'api_subkode' => 22, 'api_search' => array($ta_mulai,$smt,$nim)));	
    	}
    	
    	
    	return $cek;
    }
    

    public function get_profil_mhs($nim=null,$opsi=null)
    {
    	if (!empty($nim) || $nim !== null){
	    	switch ($opsi) {
	    		case 'dpm': 
	    			$parameter	= array('api_kode'=>26000, 'api_subkode' => 12, 'api_search' => array($nim) );
					$api_get	= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter); //sudah diconvert dari json ke array
	    			break;
	    		case 'dpm2': 
	    			$parameter	= array('api_kode'=>26000, 'api_subkode' => 7, 'api_search' => array($nim) ); 
					$api_get	= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter); //sudah diconvert dari json ke array
	    			break;
	    		case 'kumulatif':
	    			$parameter   = array('api_kode'=>26000, 'api_subkode' => 10, 'api_search' => array($nim) );     			
	    			$api_get	= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter); //sudah 
	    			break;    		
	    		
	    		default:  
	    			$parameter	= array('api_kode'=>26000, 'api_subkode' => 10, 'api_search' => array($nim) ); 
					$api_get	= $this->apiconn->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter); //sudah diconvert dari json ke array  			
	    			break;
	    	}	
    	} else {
    		$api_get = ['error'=>array('type'=>400,'msg'=>'paramater tidak lengkap')];
    	}
    		
		return $api_get;
    }


#------------------------ CEK DATA TERKAIT KELULUSAN (TA, YUDISIUM, WISUDA)  ---------------------- #
    /**
	* CEK DATA KELULUSAN (TA,IPK, Yudisium, Munaqasyah)
	* @type API REQUEST (GET)
	* @return [array] | NULL
	*/
	public function cekTIYM($nim)
	{
	/*
		http://service.uin-suka.ac.id/servsiasuper/index.php/sia_public/sia_mahasiswa/data_search
		Array
		(
			[0] => Array
				(
					[NIM] => 09650026
					[KD_TA_LULUS] => 2012
					[TA] => 2012/2013
					[KD_SMT] => 3
					[TAHUN] => 2012
					[TGL_LULUS] => 10-JUN-13
					[TGL_LULUS_F] => 10-06-2013 00:00:00
					[NO_IJAZAH] => 
					[JUDUL_TA] => Pengembangan Sistem Informasi Alumni Dengan Pendekatan Metode Agile di UIN Sunan Kalijaga
					[KD_PRODI] => 22607
					[IPK] => 3.58
					[SKS_KUM] => 149
					[HARKAT] => 533
					[PREDIKAT] => DENGAN PUJIAN
					[TANGGAL] => 10 JUNE      2013
					[TGL_YUDISIUM] => 09-07-2013 00:00:00
					[NO_] => 1
				)

		)
		*/
		/*
		[
		    {
		        "NIM": "11650006",
		        "KD_TA_LULUS": "2017",
		        "TA": "2017/2018",
		        "KD_SMT": "1",
		        "TAHUN": "2017",
		        "TGL_LULUS": "24-NOV-17",
		        "TGL_LULUS_F": "24-11-2017 00:00:00",
		        "NO_IJAZAH": null,
		        "JUDUL_TA": "<p>Pengujian Usabilitas dengan Metode Heuristic Evaluation pada Sistem Event Universitas Islam Negeri Sunan Kalijaga Yogyakarta</p>\n",
		        "KD_PRODI": "22607",
		        "PREDIKAT": "SANGAT MEMUASKAN",
		        "TANGGAL": "24 NOVEMBER  2017",
		        "TGL_YUDISIUM": "30-01-2018 00:00:00",
		        "TGL_IJAZAH_F": "30-01-2018 00:00:00",
		        "SKS_KUM_TRANS": "158",
		        "HARKAT_TRANS": "521",
		        "IPK_TRANS": "3.3",
		        "SKS_KUM": "158",
		        "HARKAT": "521",
		        "IPK": "3.3",
		        "NO_": 1
		    }
		]
	*/	//test: gunakan nim	: 11650013, 11650002
		$api_url = URL_API_SIA.'sia_mahasiswa/data_search';
		$parameter = array(	'api_kode' => 30000, 'api_subkode' => 5, 
								'api_search' => array($nim));
			
		$data = $this->s00_lib_api->get_api_json($api_url,'POST', $parameter);
		return $data;
		#print_r($data);
	}

	#KHS KUMULATIF
	public function cek_khs_kumul($nim, $mode='default'){
		/*
		$this->api->get_api_json(URL_API_SIA.'sia_krs/data_search', 'POST', array('api_kode'=>65000, 'api_subkode' => 6, 'api_search' => array($v_nim))); 
		*/
		$api_url = URL_API_SIA."sia_krs/data_search";
		$parameter = array(	'api_kode' => 65000, 'api_subkode' => 6,
								'api_search' => array($nim));
								
		$data = $this->s00_lib_api->get_api_json($api_url,'POST', $parameter);
		return($data);
	}

# ----------------------- CEK DATA TERKAIT BEASISWA ----------------------------------
	public function cekRiwayatBeasiswa($nim, $mode='')
	{
		$riwayat_bea = $this->apiconn->api_beasiswa('sia_beasiswa_mhs/get_riwayat_beasiswa', 'json', 'POST',
		array(	'api_kode' => 1000, 'api_subkode' => 1, 'nim' => $nim));
		return $riwayat_bea;
	}

	public function cekBeasiswaAktif($nim, $mode)
	{
		
	}

	public function get_beasiswa($nim)
	{
		$parameter  = array('api_kode' => 1000, 'api_subkode' => 3, 'api_search' => array($nim));
        $api_get         = $this->apiconn->api_beasiswa('sia_beasiswa_mhs/get_status_mhs', 'json', 'POST', $parameter);
        return $api_get;
	}

	public function get_beasiswa_diikuti($nim)
	{		
		$parameter  = array('api_kode' => 1000, 'api_subkode' => 3, 'api_search' => array($nim));
        $api_get         = $this->apiconn->api_beasiswa('sia_beasiswa_mhs/get_status_mhs', 'json', 'POST', $parameter);
        
        $equal = 'A'; 
		$result = array_filter($api_get, function ($item) use ($equal) {
		    /*if (stripos($item['name'], $like) !== false) {
		        return true;
		    }*/
		    if ($item['STATUS'] === $equal){
		    	return true;
		    }
		    return false;
		});

        return $result;
	}


#---------- KURIKULUM ---------------#
	#DETIL KURIKULUM
	public function cek_detil_kur($kd_prodi, $kd_kur){
		$api_url = URL_API_SIA."sia_kurikulum/data_search";
		$parameter = array(	'api_kode' => 38000, 'api_subkode' => 5,
								'api_search' => array($kd_prodi, $kd_kur));
								
		$data = $this->s00_lib_api->get_api_json($api_url,'POST', $parameter);
		return($data);
	}

	#MK JENIS SKRIPSI DI KURIKULUM
	private function cek_mk_skripsi($kd_kur){
		$api_url = URL_API_SIA."sia_kurikulum/data_search";
		$parameter = array(	'api_kode' => 40000, 'api_subkode' => 18,
								'api_search' => array($this->kdprodi, $kd_kur));
								
		$data = $this->api->get_api_json($api_url,'POST', $parameter);
		return($data);
	}

	#KUR AKTIF
	public function cek_kur_aktif($kd_prodi){		
		$api_url = URL_API_SIA."sia_kurikulum/data_search";
		$parameter = array(	'api_kode' => 38000, 'api_subkode' => 8,
								'api_search' => array($kd_prodi));		
		$data = $this->s00_lib_api->get_api_json($api_url,'POST', $parameter);
		return($data);
	}

	#CEK MATKUL Akhir
	function cek_matkul_akhir($nim){
		$api_url = URL_API_SIA."sia_krs/data_search";
		$parameter = array(	'api_kode' => 64000, 'api_subkode' => 15,
								'api_search' => array($nim));
		$data = $this->s00_lib_api->get_api_json($api_url,'POST', $parameter);
		return($data);		
	}

	#MAKUL WAJIB BY KUR
	public function cek_makul_kur($kd_prodi, $kd_kur){
		/*
			$arr_data = array('22607', 'TIF2013', 'W'); 
			$aksi = $this->s00_lib_api->get_api_json(URL_API_SIA.'sia_kurikulum/data_search', 'POST', array('api_kode'=>40000, 'api_subkode' => 20, 'api_search' => $arr_data));
		*/
		$api_url = URL_API_SIA."sia_kurikulum/data_search";
		$parameter = array(	'api_kode' => 40000, 'api_subkode' => 20,
								'api_search' => array($kd_prodi,$kd_kur,'W'));
								
		$data = $this->s00_lib_api->get_api_json($api_url,'POST', $parameter);
		return($data);
	}



	###more...
	# CEK BEBAS TEORI DR SIA (HASIL INPUTAN PEGAWAI TU)
	public function cek_bebas_teori($nim){
		$api_url = URL_API_SIA."sia_krs/data_search";
		$parameter = array(	'api_kode' => 65001, 'api_subkode' => 1,
								'api_search' => array($nim));
								
		$data = $this->s00_lib_api->get_api_json($api_url,'POST', $parameter);
		return($data);				
	}
	# CEK BEBAS TEORI BY MHS
	public function cek_bebas_teori_mhs($nim){
		$api_url = URL_API_SIA."sia_krs/data_procedure";
		$parameter = array(	'api_kode' => 65001, 'api_subkode' => 1,
								'api_datapost' => array($nim));
								
		$data = $this->s00_lib_api->get_api_json($api_url,'POST', $parameter);
		return ($data);		
	}
}