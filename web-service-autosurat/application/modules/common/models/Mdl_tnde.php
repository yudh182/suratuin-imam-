<?php
class Mdl_tnde extends CI_Model {
	public function __construct()
	{
		$this->load->library('common/Curl');
	}
	public function api_autosurat($url, $output='json', $postorget='GET', $parameter)
	{
		$api_url = 'http://exp.uin-suka.ac.id/surat/service_sakad/'.$url;
		$hasil = null;

		if ( $postorget == 'GET' && isset($parameter['api_kode']) && isset($parameter['api_subkode']) ){
			$this->curl->option(
				'HTTPHEADER', 
				array(
					'X-API-KODE: '.$parameter['api_kode'],
					'X-API-SUBKODE: '.$parameter['api_subkode']
				)
			);			
		}

		if ( $postorget == 'GET' && isset($parameter['api_search'])){			
			$querystring = http_build_query($parameter['api_search']);			
		}

		


		if ($postorget == 'POST'){
			$hasil = $this->curl->simple_post($api_url, $parameter);			
		} elseif ($postorget == 'GET'){						
			// $hasil = $this->curl->simple_get($api_url.'?'.$querystring);
			$hasil = (isset($querystring) ? $this->curl->simple_get($api_url.'?'.$querystring) : $this->curl->simple_get($api_url));
		}		
		return json_decode($hasil, TRUE);
	}

	public function api_autosurat_v2($url, $output='json', $postorget='GET', $parameter)
	{
		$api_url = 'http://exp.uin-suka.ac.id/surat/service_autosurat/'.$url;
		$hasil = null;

		if ( $postorget == 'GET' && isset($parameter['api_kode']) && isset($parameter['api_subkode']) ){
			$this->curl->option(
				'HTTPHEADER', 
				array(
					'X-API-KODE: '.$parameter['api_kode'],
					'X-API-SUBKODE: '.$parameter['api_subkode']
				)
			);			
		}

		if ( $postorget == 'GET' && !empty($parameter['api_search'])){			
			$querystring = http_build_query($parameter['api_search']);			
		}

		


		if ($postorget == 'POST'){
			$hasil = $this->curl->simple_post($api_url, $parameter);			
		} elseif ($postorget == 'GET'){						
			// $hasil = $this->curl->simple_get($api_url.'?'.$querystring);
			$hasil = (isset($querystring) ? $this->curl->simple_get($api_url.'?'.$querystring) : $this->curl->simple_get($api_url));
		}		
		return json_decode($hasil, TRUE);
	}


	public function api_beasiswa($url, $output='json', $postorget='GET', $parameter){
		$api_url = 'http://service.uin-suka.ac.id/servbeasiswa/index.php/bea_public/'.$url.'/'.$output;
		// $api_url = 'http://service.uin-suka.ac.id/servsiasuper/index.php/tnde_public/'.$url.'/'.$output;
		$hasil = null;
		if ($postorget == 'POST'){
			$hasil = $this->curl->simple_post($api_url, $parameter);			
		} else {
			$hasil = $this->curl->simple_get($api_url);
		}
		return json_decode($hasil, TRUE);	
	}

	public function api_bayar($url, $output='json', $postorget='GET', $parameter){
		$api_url = 'http://service2.uin-suka.ac.id/servsibayar/index.php/data/md_mahasiswa/'.$url.'/format/'.$output;		
		$hasil = null;
		if ($postorget == 'GET'){			
			$hasil = $this->curl->simple_get($api_url);
		}
		return json_decode($hasil, TRUE);	
	}

	public function api_tnde_sakad($url, $output='json', $postorget='GET', $parameter){
		$api_url = 'http://exp.uin-suka.ac.id/surat/service_sakad/'.$url.'/'.$output;
		// $api_url = 'http://service.uin-suka.ac.id/servsiasuper/index.php/tnde_public/'.$url.'/'.$output;
		$hasil = null;
		if ($postorget == 'POST'){
			$hasil = $this->curl->simple_post($api_url, $parameter);			
		} else {
			$hasil = $this->curl->simple_get($api_url);
		}
		return json_decode($hasil, TRUE);
	}

	/*
	 * SURAT2018 versi Service6.uin-suka.ac.id
	 */
	function api_tnde_v3($url, $output='json', $postorget='GET', $parameter){	
				
		$api_url = 'http://service6.uin-suka.ac.id/index.php/tnde_public/'.$url.'/'.$output;
		$hasil = null;
		if ($postorget == 'POST'){
			$hasil = $this->curl->simple_post($api_url, $parameter);
		} else {
			$hasil = $this->curl->simple_get($api_url);
		}
		return json_decode($hasil, TRUE);
	}

	#API TNDE (Service2)
	function api_tnde_v2($url, $output='json', $postorget='GET', $parameter){			
		$api_url = 'http://service2.uin-suka.ac.id/servtnde/tnde_public/'.$url.'/'.$output;
		
		$hasil = null;
		if ($postorget == 'POST'){
			$hasil = $this->curl->simple_post($api_url, $parameter);
		} else {
			$hasil = $this->curl->simple_get($api_url);
		}
		return json_decode($hasil, TRUE);
	}

	#API TNDE (EXperiment version)
	function api_tnde($url, $output='json', $postorget='GET', $parameter){	
		$api_url = 'http://exp.uin-suka.ac.id/surat/tnde_public/'.$url.'/'.$output;
		// $api_url = 'http://service2.uin-suka.ac.id/servtnde/tnde_public/'.$url.'/'.$output;
		// $api_url = 'http://service.uin-suka.ac.id/servsiasuper/index.php/tnde_public/'.$url.'/'.$output;
		$hasil = null;
		if ($postorget == 'POST'){
			$hasil = $this->curl->simple_post($api_url, $parameter);
		} else {
			$hasil = $this->curl->simple_get($api_url);
		}
		return json_decode($hasil, TRUE);
	}

	#API TNDE (EXperiment/staging version)
	function api_tnde_staging($url, $output='json', $postorget='GET', $parameter){	
		$api_url = 'http://exp.uin-suka.ac.id/surat/index.php/tnde_public/'.$url.'/'.$output;
		// $api_url = 'http://service2.uin-suka.ac.id/servtnde/tnde_public/'.$url.'/'.$output;
		// $api_url = 'http://service.uin-suka.ac.id/servsiasuper/index.php/tnde_public/'.$url.'/'.$output;
		$hasil = null;
		if ($postorget == 'POST'){
			$hasil = $this->curl->simple_post($api_url, $parameter);
		} else {
			$hasil = $this->curl->simple_get($api_url);
		}
		return json_decode($hasil, TRUE);
	}


	function get_api($url, $output='json', $postorget='GET', $parameter){	
		$api_url = 'http://service2.uin-suka.ac.id/servtnde/tnde_public/'.$url.'/'.$output;
		// $api_url = 'http://service.uin-suka.ac.id/servsiasuper/index.php/tnde_public/'.$url.'/'.$output;
		$hasil = null;
		if ($postorget == 'POST'){
			$hasil = $this->curl->simple_post($api_url, $parameter);
		} else {
			$hasil = $this->curl->simple_get($api_url);
		}
		return json_decode($hasil, TRUE);
	}
	
	function api_simar($url, $output='json', $postorget='GET', $parameter){	
		$api_url = 'http://service2.uin-suka.ac.id/servaset/simar_public/'.$url.'/'.$output;
		// $api_url = 'http://service.uin-suka.ac.id/servsiasuper/index.php/tnde_public/'.$url.'/'.$output;
		$hasil = null;
		if ($postorget == 'POST'){
			$hasil = $this->curl->simple_post($api_url, $parameter);
		} else {
			$hasil = $this->curl->simple_get($api_url);
		}
		return json_decode($hasil, TRUE);
	}
	
	function api_service($url, $output='json', $postorget='GET', $parameter){
		$api_url = 'http://service.uin-suka.ac.id/servsiasuper/index.php/'.$url.'/'.$output;
		$hasil = null;
		if ($postorget == 'POST'){
			$hasil = $this->curl->simple_post($api_url, $parameter);
		} else {
			$hasil = $this->curl->simple_get($api_url);
		}
		return json_decode($hasil, TRUE);
	}

	function api_simpeg($url, $output='json', $postorget='GET', $parameter){	
		$api_url = 'http://service2.uin-suka.ac.id/servsimpeg/simpeg_public/'.$url.'/'.$output;
		// $api_url = 'http://service.uin-suka.ac.id/servsiasuper/simpeg_public/'.$url.'/'.$output;
		$hasil = null;
		
		$this->curl->option('HTTPHEADER', array('HeaderName: '.$this->encrypt001('dedy5u__4t')));
						
		if ($postorget == 'POST'){
			$hasil = $this->curl->simple_post($api_url, $parameter);
		} else {
			$hasil = $this->curl->simple_get($api_url);
		}
		return json_decode($hasil, TRUE);
	}	
	
	function api_ict($url, $output='json', $postorget='GET', $parameter){	
		$api_url = 'http://service.uin-suka.ac.id/servsiasuper/ict_public/'.$url.'/'.$output;
		$hasil = null;
		
		$this->curl->option('HTTPHEADER', array('HeaderName: '.$this->encrypt001('dedy5u__4t')));
						
		if ($postorget == 'POST'){
			$hasil = $this->curl->simple_post($api_url, $parameter);
		} else {
			$hasil = $this->curl->simple_get($api_url);
		}
		return json_decode($hasil, TRUE);
	}		
	
	function api_pbba($url, $output='json', $postorget='GET', $parameter){	
		$api_url = 'http://service2.uin-suka.ac.id/servtoec/pbba_public/'.$url.'/'.$output;
		$hasil = null;
		
		$this->curl->option('HTTPHEADER', array('HeaderName: '.$this->encrypt001('dedy5u__4t')));
						
		if ($postorget == 'POST'){
			$hasil = $this->curl->simple_post($api_url, $parameter);
		} else {
			$hasil = $this->curl->simple_get($api_url);
		}
		return json_decode($hasil, TRUE);
	}	
	
	function api_skripsi($url, $output='json', $postorget='GET', $parameter){	
		$api_url = 'http://service2.uin-suka.ac.id/servtugasakhir/sia_skripsi_public/'.$url.'/'.$output;
		$hasil = null;
		
		$this->curl->option('HTTPHEADER', array('HeaderName: '.$this->encrypt001('dedy5u__4t')));
						
		if ($postorget == 'POST'){
			$hasil = $this->curl->simple_post($api_url, $parameter);
		} else {
			$hasil = $this->curl->simple_get($api_url);
		}
		return json_decode($hasil, TRUE);
	}
	
	function api_ikd($url, $output='json', $postorget='GET', $parameter){
		$api_url = 'http://service.uin-suka.ac.id/servikd/index.php/ikd_public/'.$url.'/'.$output;
		$hasil = null;
		
		$this->curl->option('HTTPHEADER', array('HeaderName: '.$this->encrypt001('dedy5u__4t')));
		
		if ($postorget == 'POST'){
			$hasil = $this->curl->simple_post($api_url, $parameter);
		} else {
			$hasil = $this->curl->simple_get($api_url);
		}
		return json_decode($hasil, TRUE);
	}
	
	function api_link($url, $output='json', $postorget='GET', $parameter){	
		$api_url = $url.'/'.$output;
		$hasil = null;
		
		$this->curl->option('HTTPHEADER', array('HeaderName: '.$this->encrypt001('dedy5u__4t')));
		
		if ($postorget == 'POST'){
			$hasil = $this->curl->simple_post($api_url, $parameter);
		} else {
			$hasil = $this->curl->simple_get($api_url);
		}
		return json_decode($hasil, TRUE);
	}
	
	function simple_api($url, $output='json', $postorget='GET', $parameter){
		$api_url = $url.'/'.$output;
		$hasil = null;
		if ($postorget == 'POST'){
			$hasil = $this->curl->simple_post($api_url, $parameter);
		} else {
			$hasil = $this->curl->simple_get($api_url);
		}
		return json_decode($hasil, TRUE);
	}
	
	function auth_ad($username, $password){
		$api_url = "http://service.uin-suka.ac.id/servad/adlogauthgr.php?aud=8f304662ebfee3932f2e810aa8fb628723&uss=".$username."&pss=".$password;
		$hasil = null;
		$hasil = $this->curl->simple_get($api_url);
		return json_decode($hasil, TRUE);
	}

	function auth_ad_from_automasi($username, $password){
		$api_url = "http://service.uin-suka.ac.id/servad/adlogauthgr.php?aud=8f304662ebfee3932f2e810aa8fb628735&uss=".$username."&pss=".$password;
		$hasil = null;
		$hasil = $this->curl->simple_get($api_url);
		return json_decode($hasil, TRUE);
	}
	
	function api_sia($url, $output='json', $postorget='GET', $parameter){	
		$api_url = 'http://service.uin-suka.ac.id/servsiasuper/index.php/sia_public/'.$url.'/'.$output;
		$hasil = null;
		
		$this->curl->option('HTTPHEADER', array('HeaderName: '.$this->encrypt001('dedy5u__4t')));
						
		if ($postorget == 'POST'){
			$hasil = $this->curl->simple_post($api_url, $parameter);
		} else {
			$hasil = $this->curl->simple_get($api_url);
		}
		return json_decode($hasil, TRUE);
	}

	/* Simple POST Curl ke API SIA tanpa didecode ke array atau object
	*/
	function api_sia_json($url, $output='json', $postorget='GET', $parameter){	
		$api_url = 'http://service.uin-suka.ac.id/servsiasuper/index.php/sia_public/'.$url.'/'.$output;
		$hasil = null;
		
		$this->curl->option('HTTPHEADER', array('HeaderName: '.$this->encrypt001('dedy5u__4t')));
						
		if ($postorget == 'POST'){
			$hasil = $this->curl->simple_post($api_url, $parameter);
		} else {
			$hasil = $this->curl->simple_get($api_url);
		}
		return $hasil;
		//return json_decode($hasil);
	}
	
	function encrypt001($kata = ''){
		$this->load->library('s00_lib_siaenc');
		return $this->s00_lib_siaenc->encrypt($kata);
	}
	
	function api_foto($url){
		$api_url = "http://service.uin-suka.ac.id/foto/".$url;
		$hasil = null;
		$hasil = $this->curl->simple_get($api_url);
		return json_decode($hasil, TRUE);
	}
	
}
?>
