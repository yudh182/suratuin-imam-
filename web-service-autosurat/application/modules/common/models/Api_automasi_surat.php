<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api_automasi_surat extends CI_Model
{
	public function __construct()
	{
		parent::__construct();        
	}

 # --------- MASTER --------- #
	public function get_jenis_surat($mode='aktif')
	{
		if ($mode == 'aktif'){
			# DAFTAR JENIS SURAT (AKTIF)
			$parameter =array('api_kode'		=> 13001, 'api_subkode'	=> 2, 'api_search'	=> array());
			$api_get = $this->apiconn->api_tnde_sakad('tnde_master_mix/get_jenis_surat_akademik', 'json', 'POST', $parameter);	
		} elseif ($mode == 'semua') {
			# DAFTAR JENIS SURAT (AKTIF maupun NON-AKTIF)
		}
		

		return $api_get;
	}

	public function get_jenis_surat_mahasiswa($opsi_by='kd', $input=null)
	{
		if ($opsi_by == 'kd'){
			$parameter =array('api_kode'		=> 13002, 'api_subkode'	=> 1, 'api_search'	=> array($input));
		} elseif ($opsi_by == 'path') {
			$parameter =array('api_kode'		=> 13002, 'api_subkode'	=> 2, 'api_search'	=> array($input));
		} elseif ($opsi_by == 'status_aktif') {
			$parameter =array('api_kode'		=> 13001, 'api_subkode'	=> 2, 'api_search'	=> array());
		} elseif ($opsi_by == 'semua') {
			$parameter =array('api_kode'		=> 13001, 'api_subkode'	=> 1, 'api_search'	=> array());
		} else {
			return false;
		}
		
		$jenis_sakad = $this->apiconn->api_tnde_sakad('tnde_master_mix/get_jenis_surat_akademik', 'json', 'POST', $parameter);
		return $jenis_sakad;
	}

 # --------- PEMBUATAN --------- #


# --------- ARSIP DAN CEK SURAT --------- #
	public function get_detail_skr_automasi($id_surat_keluar)
	{
	    $parameter = array('api_kode' => 14001, 'api_subkode' => 1, 'api_search' => array($id_surat_keluar));
	    $api_get = $this->apiconn->api_tnde_sakad('Tnde_surat_akademik/get_detail_skr_automasi','json','POST', $parameter) ;
	    return $api_get;
	}
 # --------- ADMINISTRASI --------- #

}	