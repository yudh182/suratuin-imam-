<?php
if(! defined('BASEPATH'))exit('No direct script access allowed');
/**
=====================================================================
* Transformasi simpel data master surat (TNDE / surat.uin-suka.ac.id)
* MD_JENIS_SURAT, MD_STATUS_SIMPAN, MD_KEAMANAN_SURAT, MD_SIFAT_SURAT
=====================================================================
*/

/**
* Transformasi KD_JENIS_SURAT pada MD_JENIS_SURAT ke array khusus. 
* Hanya digunakan untuk quick use SISTEM AUTOMASI SURAT
*/
function trans_jenis_surat($kd_jenis_surat="", $mode='arr'){
    $kd = (int) $kd_jenis_surat;
    
    switch ($kd) {
    	case 6:
    		$output = [
    			'KD_JENIS_SURAT' => 6,
    			'NM_JENIS_SURAT' => 'SURAT KETERANGAN',
    			'KELOMPOK_MODUL' => 'SURAT KELUAR',
    			'PREFIX_HAK_AKSES' => 'SKRxxxxx'
    		];
    		break;
    	case 11:
    		$output = [
    			'KD_JENIS_SURAT' => 11,
    			'NM_JENIS_SURAT' => 'SURAT DINAS (UMUM)',
    			'KELOMPOK_MODUL' => 'SURAT KELUAR',
    			'PREFIX_HAK_AKSES' => 'SKRxxxxx'
    		];
    		break;    	
    	default:
    		return ['error'=>'kode jenis surat tidak digunakan di aplikasi automasi surat'];
    		break;
    }
    return $output;
}

function trans_status_simpan($kd_status_simpan=''){
	 switch ($kd_status_simpan) {
    	case 0: $output='DIBATALKAN';
    		break;
    	case 1: $output='DRAF';
    		break;
    	case 2: $output='BERNOMOR';
    		break;
    	case 1: $output='DISAHKAN';
    		break;
    	default:
    		return ['error'=>'status simpan surat tidak dikenali!'];
    		break;
    }
}

function trans_keamanan_surat($kd_keamanan_surat=''){
	 switch ($kd_keamanan_surat) {
    	case 'B': $output='BIASA';
    		break;
    	case 'R': $output='RAHASIA';
    		break;
    	case 'SR': $output='SANGAT RAHASIA';
    		break;
    	case 1: $output='DISAHKAN';
    		break;
    	default:
    		return ['error'=>'status simpan surat tidak dikenali!'];
    		break;
    }
}