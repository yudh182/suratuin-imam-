<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');

function cek_file($dokumen=''){
	$size = array_sum($dokumen['size']);
	$tot = count($dokumen);
	$status['tot_size'] = $size;

	$status['properti_all_file']	=	$dokumen;
	if($tot > 0){
		$status['empty'] = FALSE;
		if($size > 20116125){
			$error[] = "Total file yang anda upload terlalu besar. Maksimal total dalam sekali upload tidak boleh lebih dari 20Mb";
		}else{
			$uploadFile = $dokumen;
			
			$extensionList = array("pdf","PDF","Pdf");
			$cekFile = $dokumen['name'];
			$tot = count($cekFile);
			for($i=0;$i<$tot;$i++){
				if($cekFile[$i] != null){
					$exp = explode(".",$cekFile[$i]);
					$extension = array_pop($exp);
					if(in_array($extension,$extensionList)){
					}else{
						$error[] = "Gagal upload file scan <b>".$cekFile[$i]."</b>. File harus dalam format <b>pdf</b> ";
					}
				}
			}	
		}
	}else{
		$error[] = "Tidak ada file yang diupload.";
		$status['empty'] = TRUE;
	}
	if(ISSET($error)){
		$status['valid'] = FALSE;
		$status['errors'] = $error;
	}else{
		$status['valid'] = TRUE;
		$status['total'] = $tot;
		$status['errors'] = NULL;
	}
	return $status;
}

function cek_pdf($dokumen){
	if($dokumen['size'] != 0){
		if($dokumen['size'] > 20116125){
			$status['empty'] = FALSE;
			$error[] = "File yang anda upload terlalu besar. File tidak boleh lebih dari 20Mb";
		}else{
			$uploadFile = $dokumen;
			$extensionList = array("pdf","PDF","Pdf");
			$exp = explode(".",$dokumen['name']);
			$extension = array_pop($exp);
			if(in_array($extension,$extensionList)){
			}else{
				$error[] = "Gagal upload dokumen <b>".$dokumen['name']."</b>. Dokumen harus dalam format <b>pdf</b> ";
			}
			$status['empty'] = FALSE;		
		}
	}else{
		$error[] = "File dokumen tidak boleh kosong";
		$status['empty'] = TRUE;
	}
	if(ISSET($error)){
		$status['valid'] = FALSE;
		$status['errors'] = $error;
	}else{
		$status['valid'] = TRUE;
		$status['errors'] = NULL;
	}
	return $status;
}

function cek_docx($dokumen){
	if($dokumen['size'] != 0){
		if($dokumen['size'] > 20116125){
			$status['empty'] = FALSE;
			$error[] = "File yang anda upload terlalu besar. File tidak boleh lebih dari 20Mb";
		}else{
			$uploadFile = $dokumen;
			$extensionList = array("docx","DOCX","Docx");
			$exp = explode(".",$dokumen['name']);
			$extension = array_pop($exp);
			if(in_array($extension,$extensionList)){
			}else{
				$error[] = "Gagal upload dokumen <b>".$dokumen['name']."</b>. Dokumen harus dalam format <b>docx</b> ";
			}

			$status['empty'] = FALSE;		
		}
	}else{
		$error[] = "File dokumen tidak boleh kosong";
		$status['empty'] = TRUE;
	}
	if(ISSET($error)){
		$status['valid'] = FALSE;
		$status['errors'] = $error;
	}else{
		$status['valid'] = TRUE;
		$status['errors'] = NULL;
	}
	return $status;
}

function cek_doc($dokumen){
	if(! empty($dokumen['size'])){
		if($dokumen['size'] > 20116125){
			$status['empty'] = FALSE;
			$error[] = "File yang anda upload terlalu besar. File tidak boleh lebih dari 20Mb";
		}else{
			$uploadFile = $dokumen;
			$extensionList = array("doc", "DOC", "Doc", "docx","DOCX","Docx");
			$exp = explode(".",$dokumen['name']);
			$extension = array_pop($exp);
			if(in_array($extension,$extensionList)){
			}else{
				$error[] = "Gagal upload dokumen <b>".$dokumen['name']."</b>. Dokumen harus dalam format <b>doc</b> atau <b>docx</b> ";
			}

			$status['empty'] = FALSE;		
		}
	}else{
		$error[] = "File dokumen tidak boleh kosong";
		$status['empty'] = TRUE;
	}
	if(ISSET($error)){
		$status['valid'] = FALSE;
		$status['errors'] = $error;
	}else{
		$status['valid'] = TRUE;
		$status['errors'] = NULL;
	}
	return $status;
}

function cek_lamp($dokumen){
	$size 	= array_sum($dokumen['size']);
	$tot 	= count($dokumen['name']);
	$status['tot_size'] = $size;
	if($size != 0){
		$status['empty'] = FALSE;
		if($size > 20116125){
			$error[] = "Total file yang anda upload terlalu besar. Maksimal total dalam sekali upload tidak boleh lebih dari 20Mb";
		}
	}else{
		$error[] = "Tidak ada dokumen lampiran yang diupload";
		$status['empty'] = TRUE;
	}
	if(ISSET($error)){
		$status['valid'] = FALSE;
		$status['errors'] = $error;
	}else{
		$status['valid'] = TRUE;
		$status['total'] = $tot;
		$status['errors'] = NULL;
	}
	return $status;
}

function cek_file_type($dokumen, $type=''){
	if(empty($type)){
		$type	= array("PDF", "DOC", "DOCX", "ZIP", "RAR", "XLS", "XLSX", "PPT", "PPTX", "TXT", "PNG", "JPG", "JPEG", "BMP", "GIF");
	}
	
	$size 	= array_sum($dokumen['size']);
	$tot 	= count($dokumen);
	$status['tot_size'] = $size;

	$status['properti_all_file']	=	$dokumen;
	if($size > 0){
		$status['empty'] = FALSE;
		if($size > 21116125){
			$error[] = "Total file yang anda upload terlalu besar. Maksimal total dalam sekali upload tidak boleh lebih dari 20Mb";
		}else{
			$uploadFile = $dokumen;
			$cekFile = $dokumen['name'];
			$tot = count($cekFile);
			for($i=0;$i<$tot;$i++){
				if($cekFile[$i] != null){
					$exp = explode(".",$cekFile[$i]);
					$tipe = array_pop($exp);
					$extension = strtoupper($tipe);
					if(in_array($extension, $type)){
					}else{
						$list		= implode(", ", $type);
						$error[] = "Gagal upload file <b>".$cekFile[$i]."</b>. Format dokumen tidak diperbolehkan.";
					}
				}
			}
			if(!empty($error)){
				$status['format']	= FALSE;
				$status['file_types'] = $type;
			}else{
				$status['format']	= TRUE;
			}
		}
	}else{
		$error[] = "Tidak ada dokumen yang diupload";
		$status['empty'] = TRUE;
	}
	if(ISSET($error)){
		$status['valid'] = FALSE;
		$status['errors'] = $error;
	}else{
		$status['valid'] = TRUE;
		$status['total'] = $tot;
		$status['errors'] = NULL;
	}
	return $status;
}

function cek_single_file($dokumen){
	$size = $dokumen['size'];
	$tot = count($dokumen['name']);
	$status['tot_size'] = $size;
	$status['name']	= $dokumen['name'];
	if($size > 0){
		$status['empty'] = FALSE;
		if($size > 20116125){
			$error[] = "Total file yang anda upload terlalu besar. Maksimal total dalam sekali upload tidak boleh lebih dari 20Mb";
		}
	}else{
		$error[] = "Tidak ada dokumen yang diupload";
		$status['empty'] = TRUE;
	}
	if(ISSET($error)){
		$status['valid'] = FALSE;
		$status['errors'] = $error;
	}else{
		$status['valid'] = TRUE;
		$status['total'] = $tot;
		$status['errors'] = NULL;
	}
	return $status;
}

function karakter_abnormal($teks=''){
	$hasil = '';
	for($i = 0; $i < strlen($teks); $i++){
		if((ord(substr($teks,$i,1)) > 126)||(ord(substr($teks,$i,1)) < 20)){
			
		} else {
			$hasil .= substr($teks,$i,1);
		}
	} return $hasil;
}

function multiple_input($data, $required=FALSE){
	$status['valid'] = FALSE;
	$tot = 0;
	$kosong = 0;
	if(! empty($data)){
		foreach($data as $val){
			if($val != null){
				$status['valid'] = TRUE;
				$tot = $tot + 1;
			}else{
				$kosong++;
			}
		}
	}
	$status['total'] = $tot;
	if($required == TRUE){
		if($kosong > 0){
			$status['valid'] = FALSE;
		}
	}
	return $status;
}
?>