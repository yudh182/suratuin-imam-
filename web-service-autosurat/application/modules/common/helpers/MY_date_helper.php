<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');
function waktu_indo($data){
	$exp = explode('-',$data);
	if(count($exp) == 3){
		switch($exp[1]){
			case "JAN":
				$bln = '01';
				break;
			case "FEB":
				$bln = '02';
				break;
			case "MAR":
				$bln = '03';
				break;
			case "APR":
				$bln = '04';
				break;
			case "MAY":
				$bln = '05';
				break;
			case "JUN":
				$bln = '06';
				break;
			case "JUL":
				$bln = '07';
				break;
			case "AGT":
				$bln = '08';
				break;
			case "SEP":
				$bln = '09';
				break;
			case "OCT":
				$bln = '10';
				break;
			case "NOV":
				$bln = '11';
				break;
			case "DEC":
				$bln = '12';
				break;
		}
		$hasil = $exp[0].'/'.$bln.'/20'.$exp[2];
	}else{
		$hasil = '';
	}
	
	return $hasil;
}

function waktu_indo2($data){
	$exp = explode('-',$data);
	if(count($exp) == 3){
		$hasil = $exp[0]."/".$exp[1]."/".$exp[2];
	}else{
		$hasil = "";
	}
	return $hasil;
}

function waktu_oracle($data){
	$exp = explode('/',$data);
	if(count($exp) == 3){
		switch($exp[1]){
			case 1:
				$bln = "JAN";
				break;
			case 2:
				$bln = "FEB";
				break;
			case 3:
				$bln = "MAR";
				break;
			case 4:
				$bln = "APR";
				break;
			case 5:
				$bln = "MAY";
				break;
			case 6:
				$bln = "JUN";
				break;
			case 7:
				$bln = "JUL";
				break;
			case 8:
				$bln = "AGT";
				break;
			case 9:
				$bln = "SEP";
				break;
			case 10:
				$bln = "OCT";
				break;
			case 11:
				$bln = "NOV";
				break;
			case 12:
				$bln = "DEC";
				break;
		}
		$split = str_split($exp[2]);
		$year = $split[2].$split[3];
		$hasil = $exp[0].'-'.$bln.'-'.$year;
	}else{
		$hasil = '';
	}
	return $hasil;
}

function waktu_oracle2($data){
	$exp = explode('/',$data);
	if(count($exp) == 3){
		$hasil = $exp[0]."-".$exp[1]."-".$exp[2];
	}else{
		$hasil = $data;
	}
	return $hasil;
}

function tgl_indo($waktu){
	$exp = explode(' ',$waktu);
	if(count($exp) == 2){
		$exp2 = explode('-',$exp[0]);
		if(count($exp2) == 3){
			$hasil = $exp2[2].'/'.$exp2[1].'/'.$exp2[0];
		}else{
			$hasil = $waktu;
		}
	}else{
		$hasil = $waktu;
	}
	
	return $hasil;
}

function tgl_indo2($waktu){
	$exp = explode(' ',$waktu);
	if(count($exp) == 3){
		switch($exp[1]){
			case 'Januari': $bln = '01'; break;
			case 'Febuari': $bln = '02'; break;
			case 'Maret': $bln = '03'; break;
			case 'April': $bln = '04'; break;
			case 'Mei': $bln = '05'; break;
			case 'Juni': $bln = '06'; break;
			case 'Juli': $bln = '07'; break;
			case 'Agustus': $bln = '08'; break;
			case 'September': $bln = '09'; break;
			case 'Oktober': $bln = '10'; break;
			case 'November': $bln = '11'; break;
			case 'Desember': $bln = '12'; break;
		}
		$hasil = $exp[0]."/".$bln."/".$exp[2];
	}else{
		$hasil = count($exp);
	}
	
	return $hasil;
}

function hari($data){
	// $ubah = gmdate($tanggal, time()+60*60*8);
	$cek = explode(" ", $data);
	
	$pecah = explode("/",$cek[0]);
	$tgl = $pecah[0];
	$bln = $pecah[1];
	$thn = $pecah[2];

	$day = date("l", mktime(0,0,0,$bln,$tgl,$thn));
	$hasil = "";
	
	// $day = date('l', strtotime($data));
	
	switch($day){
		case "Sunday":
			$hasil = "Minggu";
			break;
		case "Monday":
			$hasil = "Senin";
			break;
		case "Tuesday":
			$hasil = "Selasa";
			break;
		case "Wednesday":
			$hasil = "Rabu";
			break;
		case "Thursday";
			$hasil = "Kamis";
			break;
		case "Friday";
			$hasil = "Jumat";
			break;
		case "Saturday";
			$hasil = "Sabtu";
			break;
		default:
			$hasil = $day;
			break;
	}
	return $hasil;
}

function nama_hari($tanggal)
{
	$ubah = gmdate($tanggal, time()+60*60*8);
	$pecah = explode("-",$ubah);
	$tgl = $pecah[2];
	$bln = $pecah[1];
	$thn = $pecah[0];

	$nama = date("l", mktime(0,0,0,$bln,$tgl,$thn));
	$nama_hari = "";
	if($nama=="Sunday") {$nama_hari="Minggu";}
	else if($nama=="Monday") {$nama_hari="Senin";}
	else if($nama=="Tuesday") {$nama_hari="Selasa";}
	else if($nama=="Wednesday") {$nama_hari="Rabu";}
	else if($nama=="Thursday") {$nama_hari="Kamis";}
	else if($nama=="Friday") {$nama_hari="Jumat";}
	else if($nama=="Saturday") {$nama_hari="Sabtu";}
	return $nama_hari;
}

function waktu_meridian($date){
	$data = strtoupper($date);
	$exp = explode(" ",$data);
	$exp_jam = explode(":",$exp[1]);
	if($exp_jam[0] >= 12){
		$s = "PM";
		$j = $exp_jam[0] - 12;
	}else{
		$s = "AM";
		$j = $exp_jam[0];
	}
	return $exp[0]." ".$j.":".$exp_jam[1].":".$exp_jam[2].".000000 ".$s;
}

function hapus_milliseconds($date){
	$exp = explode(",",$date);
	if(count($exp) == 2){
		$hasil = $exp[0];
	}else{
		$hasil = "";
	}
	return $hasil;
}

function tahun($date){
	$exp = explode(" ", $date);
	$exp2 = explode("/", $exp[0]);
	if(count($exp2) == 3){
		return $exp2[2];
	}else{
		return FALSE;
	}
}

function tahun2($date){
	$exp = explode("/", $date);
	if(count($exp) == 3){
		return $exp[2];
	}else{
		return FALSE;
	}
}

function tgl_bulan_indo($date){
	$exp = explode("/", $date);
	if(count($exp) == 3){
		switch($exp[1]){
			case 1:
				$bln = "Januari";
				break;
			case 2:
				$bln = "Februari";
				break;
			case 3:
				$bln = "Maret";
				break;
			case 4:
				$bln = "April";
				break;
			case 5:
				$bln = "Mei";
				break;
			case 6:
				$bln = "Juni";
				break;
			case 7:
				$bln = "Juli";
				break;
			case 8:
				$bln = "Agustus";
				break;
			case 9:
				$bln = "September";
				break;
			case 10:
				$bln = "Oktober";
				break;
			case 11:
				$bln = "November";
				break;
			case 12:
				$bln = "Desember";
				break;
		}
		$hasil = $exp[0].' '.$bln.' '.$exp[2];
		return $hasil;
	}else{
		return false;
	}
}

function date_to_number($data=''){
	$hasil = mktime(intval(substr($data,11,2)), intval(substr($data,14,2)), intval(substr($data,17,2)), substr($data,3,2), substr($data,0,2), substr($data,6,4));
	return $hasil;
}

function tgl($date=''){
	if(!empty($date)){
		$exp = explode(" ", $date);
		if(count($exp) > 1){
			return $exp[0];
		}else{
			return $date;
		}
	}else{
		return $date;
	}
}

function bulan($data=''){
	$exp = explode(" ", $data);
	if(count($exp) > 1){
		$exp2 = explode("/", $exp[0]);
		$no_tgl = $exp2[1];
	}else{
		$exp2 = explode("/", $data);
		if(count($exp2) > 1){
			$no_tgl = $exp2[1];
		}else{
			$no_tgl = $data;
		}
	}
	if(!empty($no_tgl)){
		switch($no_tgl){
			case 1:
				$bln = "Januari";
				break;
			case 2:
				$bln = "Februari";
				break;
			case 3:
				$bln = "Maret";
				break;
			case 4:
				$bln = "April";
				break;
			case 5:
				$bln = "Mei";
				break;
			case 6:
				$bln = "Juni";
				break;
			case 7:
				$bln = "Juli";
				break;
			case 8:
				$bln = "Agustus";
				break;
			case 9:
				$bln = "September";
				break;
			case 10:
				$bln = "Oktober";
				break;
			case 11:
				$bln = "November";
				break;
			case 12:
				$bln = "Desember";
				break;
		}
		return $bln;
	}
}

function bln($date=''){
	if(!empty($date)){
		$exp = explode("/", $date);
		if(!empty($exp[1])){
			return $exp[1];
		}
	}
}

function jam($waktu=''){
	$exp = explode(" ", $waktu);
	if(count($exp) == 2){
		return $exp[1];
	}
}

function selisih_hari($tgl1, $tgl2){
	$tgl1 = str_replace("/","-", $tgl1);
	$tgl2 = str_replace("/","-", $tgl2);
	$selisih = strtotime($tgl2) - strtotime($tgl1);
	$hari = $selisih / (60*60*24);
	return $hari;
}

function daftar_bulan(){
	$data	= array(1 => "Januari", 2 => "Februari", 3 => "Maret", 4 => "April", 5 => "Mei", 6 => "Juni", 7 => "Juli", 8 => "Agustus", 9 => "September", 10 => "Oktober", 11 => "November", 12 => "Desember");
	return $data;
}

function tot_hari_perbulan($bulan = 0, $tahun = ''){ 
	if($bulan < 1 OR $bulan > 12){ 
		return 0; 
	} 
	if( ! is_numeric($tahun) OR strlen($tahun) != 4){ 
		$tahun = date('Y'); 
	} 
	if($bulan == 2){ 
		if($tahun % 400 == 0 OR ($tahun % 4 == 0 AND $tahun % 100 != 0)){
			return 29; 
		} 
	} 
	$jumlah_hari = array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
	return $jumlah_hari[$bulan - 1]; 
}

function tgl_hari($date=""){
	$exp = explode("/", $date);
	if(count($exp) == 3){
		return $exp[0];
	}else{
		return $date;
	}
}

#******************* Mulai Helper Tanggal untuk Sistem Automasi Surat ******************* 
/**
* Format string waktu untuk disimpan di postgre
* format timestamp default di postgresql adalah Y-m-d h:i:s
* default format timestamp masukan 
*/
function format_waktu_postgre()
{

}

/*
* Format string tanggal untuk disimpan di postgre
* ISO Date	"2018-03-25" (The International Standard) seperti javascript
* kemungkinan format masukan (sehingga perlu diubah): 
	* Short Date	"03/25/2015"
	* 
*/
function format_tanggal_postgre($format_to='Y-m-d', $from_format='d/m/Y')
{
}

/**
* Konversi penulisan tanggal antar format
 * @param string $to_format. opsi 'Y-m-d' , ''
 * @param string $from_format = 'Y-m-d' atau 'd/m/Y' 
 */
function convert_format_tanggal($to_format=null, $from_format=null)
{

}

/**
* Tampilkan tanggalan format indonesia
 * @param string $tanggal
 * @param string $from_format = 'Y-m-d' atau 'd/m/Y'
 * @param boolean $cetak_hari. ex: Kamis, 22 Februarui 2018 (TRUE), 22 Februarui 2018 (FALSE)
 * @return string tanggal format indonesia
 */
function display_tanggal_indo($tanggal, $from_format='Y-m-d', $cetak_hari = false)
{
    $hari = array ( 1 =>    'Senin',
                'Selasa',
                'Rabu',
                'Kamis',
                'Jumat',
                'Sabtu',
                'Minggu'
            );
            
    $bulan = array (
        1 =>   'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember'
            );
    if ($from_format == 'Y-m-d'){
        $split    = explode('-', $tanggal);
        $tgl_indo = $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];    
    } elseif ($from_format == 'd/m/Y') {
        $split    = explode('/', $tanggal);
        $tgl_indo = $split[0] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[2];    
    } elseif ($from_format == 'd-m-Y') {
    	$split    = explode('-', $tanggal);
        $tgl_indo = $split[0] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[2];    
    }
    
    
    if ($cetak_hari) {
        $num = date('N', strtotime($tanggal));
        return $hari[$num] . ', ' . $tgl_indo;
    }
    return $tgl_indo;
}

function transform_datetime_format($datestring, $from_format, $to_format)
{	
	$date = DateTime::createFromFormat($from_format, $datestring);
	return $date->format($to_format);
}

#******************* Selesai Helper Tanggal untuk Sistem Automasi Surat ******************* 
?>