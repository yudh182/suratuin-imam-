<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    function groupArray($arr, $group, $preserveGroupKey = false, $preserveSubArrays = false) {
        $temp = array();
        foreach($arr as $key => $value) {
            $groupValue = $value[$group];
            if(!$preserveGroupKey)
            {
                unset($arr[$key][$group]);
            }
            if(!array_key_exists($groupValue, $temp)) {
                $temp[$groupValue] = array();
            }

            if(!$preserveSubArrays){
                $data = count($arr[$key]) == 1? array_pop($arr[$key]) : $arr[$key];
            } else {
                $data = $arr[$key];
            }
            $temp[$groupValue][] = $data;
        }

        if(!$preserveSubArrays && count($arr[$key])==1){
            $temp[$groupValue][] = current($arr[$key]);
        } else {
            $temp[$groupValue][] = $arr[$key];
        }
        return $temp;
    }
    //penggunaan : groupArray($arr, "group"); groupArray($arr, "group", true);groupArray($arr, "group", false, true);