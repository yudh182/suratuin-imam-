<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_notifikasi extends CI_Model {
	
	private $table_name = 'MONITOR_SURAT';

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('tnde_pdo', true);
		$this->load->helper('useful_array_tool');
	}

	
	public function find_monitor($limit=20, $offset=0)
	{		
		$this->db->select('*');
	    $this->db->from($this->table_name);
	    $this->db->limit($limit, $offset);

	    $query = $this->db->get();
	    if($query->num_rows() != 0)
	    {
	        return $query->result_array();
	    }
	    else
	    {
	        return FALSE;
	    }
	}

	public function find_monitor_aktif_by_pemonitor($dimonitor_oleh=null, $kd_kat_monitor=null, $limit=20, $offset=0)
	{		
		$array = array('DIMONITOR_OLEH' => $dimonitor_oleh, 'KD_KAT_MONITOR' => $kd_kat_monitor, 'IS_AKTIF'=>true);

		$this->db->select('*');
	    $this->db->from($this->table_name);
	    $this->db->where($array);
	    $this->db->limit($limit, $offset);

	    $query = $this->db->get();
	    if($query->num_rows() != 0)
	    {
	        return $query->result_array();
	    }
	    else
	    {
	        return FALSE;
	    }
	}

	public function find_monitor_aktif_by_id($id=null)
	{

		$this->db->select('*');
	    $this->db->from($this->table_name);
	    $this->db->where('ID', $id);
	    $this->db->where('IS_AKTIF', true);
	    $this->db->limit($limit, $offset);

	    $query = $this->db->get();
	    if($query->num_rows() != 0)
	    {
	        return $query->row_array();
	    }
	    else
	    {
	        return FALSE;
	    }
	}

	public function find_monitor_aktif_by_id_surat($id_surat=null)
	{

		$this->db->select('*');
	    $this->db->from($this->table_name);
	    $this->db->where('ID_SURAT_KELUAR', $id);
	    $this->db->where('IS_AKTIF', true);
	    $this->db->limit($limit, $offset);

	    $query = $this->db->get();
	    if($query->num_rows() != 0)
	    {
	        return $query->result_array();
	    }
	    else
	    {
	        return FALSE;
	    }
	}




	##single inserting
	public function insert_monitor_event_surat($data){
		// return $data;
		$notnull_fields = ['ID_SURAT', 'DIMONITOR_OLEH', 'KD_KAT_MONITOR'];
				
		
		if (! Useful_array_tool::array_keys_exists($notnull_fields, $data)){
	    	return [
	    		'status' => (bool)FALSE,
				'code'	=> 422,
				'errror_message' => 'Gagal menyimpan data karena parameter data minimal tidak terpenuhi'
	    	];
		}

		$this->db->insert($this->table_name, $data);
		if($this->db->affected_rows() > 0){
			//return TRUE;			
			return [
				'status' => (bool)TRUE,
				'code'	=> 201,
				'success_message' => 'Berhasil menyimpan data monitor baru',
				'data'	=> $data //tampilkan kembali data input sebagai bagian response
			];
		}else{
			//return FALSE;			
			return [
				'status' => (bool)FALSE,
				'code'	=> 422,
				'errror_message' => 'Gagal menyimpan data monitor baru' 
			];
		}									
	}

	public function delete_monitor_event_surat_by_id($data)
	{		
		$this->db->delete($this->table_name, array('ID' => $data));
	    if($this->db->affected_rows() != 0)
	    {
	        //return TRUE;
	        return [
				'status' => (bool)TRUE,
				'code'	=> 200,
				'success_message' => 'Berhasil menghapus data monitor dengan ID '.$data,				
			];
	    }
	    else
	    {
	        //return FALSE;
	        return [
				'status' => (bool)FALSE,
				'code'	=> 422,
				'success_message' => 'Gagal menghapus data monitor dengan ID '.$data,				
			];
	    }
	}

	public function stop_monitor_event_surat_by_id($data=null)
	{		
		$this->db->set([
			'WAKTU_STOP_MONITOR' => date('Y-m-d h:i:s'),
			'IS_AKTIF' => false,
		]);
		$this->db->where('ID', $data);
		$this->db->update($this->table_name);

		if($this->db->affected_rows() > 0){
			return [
				'status' => (bool)TRUE,				
				'code'	=> 200,				
				'success_message' => 'Berhasil berhenti memonitor event surat'				
			];
		}else{			
			return [
				'status' => (bool)FALSE,
				'code'	=> 422,
				'errror_message' => 'Gagal berhenti memonitor event surat' 
			];
		}
	}

}
