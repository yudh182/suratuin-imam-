<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');   
require APPPATH.'modules/service_autosurat/libraries/REST_Controller.php'; // This can be removed if you use __autoload() in config.php OR use Modular Extensions
class Notifikasi extends REST_Controller {
		
	function __construct() {
        parent::__construct();
                
        $this->load->model("Mdl_notifikasi", "mdl_16000");
    }

    public function monitor_event_surat_get()
    {
    	$api_kode = (int)$this->input->get_request_header('X-API-KODE');
        $api_subkode = (int)$this->input->get_request_header('X-API-SUBKODE');        

        $code = 200; //default: 200 OK
		if ($api_kode == 16000){

			$query = $this->mdl_16000->find_monitor();	

		} elseif ($api_kode == 16001){ #filter by pemonitor (aktif)
			$api_search = [				
				'DIMONITOR_OLEH' => $this->input->get('pemonitor'),
				'KD_KAT_MONITOR' => $this->input->get('kategori_monitor'),
			];
			switch ($api_subkode) {
				case 1: #by pemonitor (aktif)
					$query = $this->mdl_16000->find_monitor_aktif_by_pemonitor($api_search['DIMONITOR_OLEH'], $api_search['KD_KAT_MONITOR']);
					break;
				case 2: #by pemonitor dan kat_monitor
					# code...
					break;
				
				default:
					# code...
					break;
			}
		} elseif ($api_kode == 16002) { #filter by parameter lainnya
			# code...
		} else {
			$query = ["error"=>"Credentials untuk mengakses resource ini tidak lengkap"];
            $code = 422;
		}

		// $query = $this->mdl_16000->find_monitor();
		$code = isset($query['code']) ? $query['code'] : $code;
		$this->response($query, $code);
    }

    public function cek_querystring_get()
    {
    	$this->response($_GET, 200);
    }


    public function insert_monitor_event_surat_post() #16010
    {
    	$api_kode				=	(int) $_POST['api_kode'];
		$api_subkode		=	(int) $_POST['api_subkode'];
		$api_search	=	$this->input->post('api_search');

		$code = 200; //default: 200 OK
		if ($api_kode == 16000){
			switch ($api_subkode) {
				case 1:
					$query = $this->mdl_16000->insert_monitor_event_surat($api_search[0]); 
					$code = isset($query['code']) ? $query['code'] : $code;
					break;
					
				default:
					$query = ["error"=>"Credentials untuk mengakses endpoint ini tidak lengkap (api subkode tidak valid)"];
					$code = 422;					
					break;
			}
		} else {
			$query = ["error"=>"Credentials untuk mengakses endpoint ini tidak lengkap (api kode tidak valid)"];
            $code = 422;
		}

		$this->response($query, $code);
    }
   

    public function update_monitor_event_surat_post() #16020
    {
    	$api_kode				=	(int) $_POST['api_kode'];
		$api_subkode		=	(int) $_POST['api_subkode'];
		$api_search	=	$this->input->post('api_search');

    	#stop monitoring, info perubahan
    	if ($api_kode == 16021){
			switch ($api_subkode) {
				case 1:
					$query = $this->mdl_16000->stop_monitor_event_surat_by_id($api_search[0]); 
					$code = isset($query['code']) ? $query['code'] : $code;
					break;
					
				default:
					$query = ["error"=>"Credentials untuk mengakses endpoint ini tidak lengkap (api subkode tidak valid)"];
					$code = 422;					
					break;
			}
		} else {
			$query = ["error"=>"Credentials untuk mengakses endpoint ini tidak lengkap (api kode tidak valid)"];
            $code = 422;
		}

		$this->response($query, $code);
    }

    public function delete_monitor_event_surat_post() #16030
    {
    	$api_kode				=	(int) $_POST['api_kode'];
		$api_subkode		=	(int) $_POST['api_subkode'];
		$api_search	=	$this->input->post('api_search');

		$code = 200; //default: 200 OK
		if ($api_kode == 16030){
			switch ($api_subkode) {
				case 1:
					$query = $this->mdl_16000->delete_monitor_event_surat_by_id($api_search[0]); 
					$code = isset($query['code']) ? $query['code'] : $code;
					break;
					
				default:
					$query = ["error"=>"Credentials untuk mengakses endpoint ini tidak lengkap (api subkode tidak valid)"];
					$code = 422;					
					break;
			}
		} else {
			$query = ["error"=>"Credentials untuk mengakses endpoint ini tidak lengkap (api kode tidak valid)"];
            $code = 422;
		}

		$this->response($query, $code);
    }


}