<?php
if(! defined('BASEPATH'))exit('No direct script access allowed');
class Mdl_tnde_admin extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->tnde = $this->load->database('tnde', TRUE);
	}
	
	function total_pencarian_mendalam($data){
		$this->tnde->like($data);
		$this->tnde->from('MD_SURAT_MASUK');
		return $this->tnde->count_all_results();
	}
	
	function tot_ds_by_kd_pegawai($id_surat_masuk, $kd_pegawai, $kd_status_distribusi){
		$sql = "SELECT COUNT(*) AS TOTAL FROM D_DISTRIBUSI_SURAT WHERE ID_SURAT_MASUK='".$id_surat_masuk."' AND PEMROSES_SURAT='".$kd_pegawai."' AND KD_STATUS_DISTRIBUSI='".$kd_status_distribusi."'";
		return $this->tnde->query($sql)->row_array();
	}
	
	function tot_ds_by_kd_jabatan($id_surat_masuk, $kd_jabatan, $kd_status_distribusi){
		$sql = "SELECT COUNT(*) AS TOTAL FROM D_DISTRIBUSI_SURAT WHERE ID_SURAT_MASUK='".$id_surat_masuk."' AND JABATAN_PEMROSES='".$kd_jabatan."' AND KD_STATUS_DISTRIBUSI='".$kd_status_distribusi."'";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_pencarian_mendalam2($data){
		$sql ="SELECT COUNT(ID_SURAT_MASUK) AS TOTAL FROM MD_SURAT_MASUK WHERE KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND TGL_MASUK=TO_DATE('".$data['TGL_MASUK']."', 'DD/MM/YYYY') AND TGL_SURAT=TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY') AND 
					UPPER(DARI) LIKE '%".$data['DARI']."%' AND UPPER(NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(PERIHAL) LIKE '%".$data['PERIHAL']."%' AND UPPER(INDEKS_BERKAS) LIKE '%".$data['INDEKS_BERKAS']."%' ";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_pencarian_mendalam3($data){
		$sql ="SELECT COUNT(ID_SURAT_MASUK) AS TOTAL FROM MD_SURAT_MASUK WHERE KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND TGL_MASUK=TO_DATE('".$data['TGL_MASUK']."', 'DD/MM/YYYY') AND 
					UPPER(DARI) LIKE '%".$data['DARI']."%' AND UPPER(NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(PERIHAL) LIKE '%".$data['PERIHAL']."%' AND UPPER(INDEKS_BERKAS) LIKE '%".$data['INDEKS_BERKAS']."%' ";
		return $this->tnde->query($sql)->row_array();
	}

	function total_pencarian_mendalam4($data){
		$sql ="SELECT COUNT(ID_SURAT_MASUK) AS TOTAL FROM MD_SURAT_MASUK WHERE KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND TGL_SURAT=TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY') AND 
					UPPER(DARI) LIKE '%".$data['DARI']."%' AND UPPER(NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(PERIHAL) LIKE '%".$data['PERIHAL']."%' AND UPPER(INDEKS_BERKAS) LIKE '%".$data['INDEKS_BERKAS']."%' ";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_pencarian_mendalam5($data){
		$sql ="SELECT COUNT(ID_SURAT_MASUK) AS TOTAL FROM MD_SURAT_MASUK WHERE KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND 
					UPPER(DARI) LIKE '%".$data['DARI']."%' AND UPPER(NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(PERIHAL) LIKE '%".$data['PERIHAL']."%' AND UPPER(INDEKS_BERKAS) LIKE '%".$data['INDEKS_BERKAS']."%' ";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_pencarian_mendalam6($data){
		$sql =" SELECT COUNT(DISTINCT(A.ID_SURAT_MASUK)) AS TOTAL FROM MD_SURAT_MASUK A, MD_DOKUMEN B WHERE (A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND A.TGL_MASUK=TO_DATE('".$data['TGL_MASUK']."', 'DD/MM/YYYY') AND UPPER(A.DARI) LIKE '%".$data['DARI']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(A.INDEKS_BERKAS) LIKE '%".$data['INDEKS_BERKAS']."%' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK) AND (UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' OR UPPER(B.ISI_DOKUMEN) LIKE '%".$data['PERIHAL']."%') ";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_pencarian_mendalam7($data){
		$sql ="SELECT COUNT(DISTINCT(A.ID_SURAT_MASUK)) AS TOTAL FROM MD_SURAT_MASUK A, MD_DOKUMEN B WHERE (A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND A.TGL_SURAT=TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY') AND UPPER(A.DARI) LIKE '%".$data['DARI']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(A.INDEKS_BERKAS) LIKE '%".$data['INDEKS_BERKAS']."%' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK) AND (UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' OR UPPER(B.ISI_DOKUMEN) LIKE '%".$data['PERIHAL']."%')";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_pencarian_mendalam8($data){
		$sql ="SELECT COUNT(DISTINCT(A.ID_SURAT_MASUK)) AS TOTAL FROM MD_SURAT_MASUK A, MD_DOKUMEN B WHERE (A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND UPPER(A.DARI) LIKE '%".$data['DARI']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(A.INDEKS_BERKAS) LIKE '%".$data['INDEKS_BERKAS']."%' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK) AND (UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' OR UPPER(B.ISI_DOKUMEN) LIKE '%".$data['PERIHAL']."%')";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_pencarian_mendalam9($data){
		$sql ="SELECT COUNT(DISTINCT(A.ID_SURAT_MASUK)) AS TOTAL FROM MD_SURAT_MASUK A, MD_DOKUMEN B WHERE (A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND A.TGL_MASUK=TO_DATE('".$data['TGL_MASUK']."', 'DD/MM/YYYY') AND A.TGL_SURAT=TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY') AND 
					UPPER(A.DARI) LIKE '%".$data['DARI']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(A.INDEKS_BERKAS) LIKE '%".$data['INDEKS_BERKAS']."%' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK) AND (UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' OR UPPER(B.ISI_DOKUMEN) LIKE '%".$data['PERIHAL']."%')";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_pencarian_sk_all($data){
		$sql = "SELECT COUNT(DISTINCT(A.ID_SURAT_KELUAR)) AS TOTAL FROM MD_SURAT_KELUAR A, MD_DOKUMEN_SK B WHERE (UPPER(A.PEMBUAT_SURAT) LIKE '%".$data['PEMBUAT_SURAT']."%' AND A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND A.TGL_SURAT=TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY') AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND B.ID_SURAT_KELUAR=A.ID_SURAT_KELUAR) AND (UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' OR UPPER(B.ISI_DOKUMEN_SK) LIKE '%".$data['PERIHAL']."%')";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_pencarian_sk_all2($data){
		$sql = "SELECT COUNT(DISTINCT(A.ID_SURAT_KELUAR)) AS TOTAL FROM MD_SURAT_KELUAR A, MD_DOKUMEN_SK B WHERE (UPPER(A.PEMBUAT_SURAT) LIKE '%".$data['PEMBUAT_SURAT']."%' AND A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND B.ID_SURAT_KELUAR=A.ID_SURAT_KELUAR) AND (UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' OR UPPER(B.ISI_DOKUMEN_SK) LIKE '%".$data['PERIHAL']."%')";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_pencarian_sk_all3($data){
		$sql = "SELECT COUNT(*) AS TOTAL FROM MD_SURAT_KELUAR WHERE UPPER(PEMBUAT_SURAT) LIKE '%".$data['PEMBUAT_SURAT']."%' AND UPPER(KD_TU_BAGIAN) LIKE '%".$data['KD_TU_BAGIAN']."%' AND TGL_SURAT=TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY') AND UPPER(NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(PERIHAL) LIKE '%".$data['PERIHAL']."%'";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_pencarian_sk_all4($data){
		$sql = "SELECT COUNT(*) AS TOTAL FROM MD_SURAT_KELUAR WHERE UPPER(PEMBUAT_SURAT) LIKE '%".$data['PEMBUAT_SURAT']."%' AND UPPER(KD_TU_BAGIAN) LIKE '%".$data['KD_TU_BAGIAN']."%' AND UPPER(NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(PERIHAL) LIKE '%".$data['PERIHAL']."%'";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_surat_masuk_tgl_awal($perihal, $tgl_awal){
		$sql = "SELECT COUNT(ID_SURAT_MASUK) AS TOTAL FROM MD_SURAT_MASUK WHERE UPPER(PERIHAL) LIKE '%".$perihal."%' AND TGL_MASUK >= TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') ";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_surat_masuk_tgl_akhir($perihal, $tgl_akhir){
		$sql = "SELECT COUNT(ID_SURAT_MASUK) AS TOTAL FROM MD_SURAT_MASUK WHERE UPPER(PERIHAL) LIKE '%".$perihal."%' AND TGL_MASUK <= TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS')";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_surat_masuk_periode($perihal, $tgl_awal, $tgl_akhir){
		$sql = "SELECT COUNT(ID_SURAT_MASUK) AS TOTAL FROM MD_SURAT_MASUK WHERE UPPER(PERIHAL) LIKE '%".$perihal."%' AND TGL_MASUK BETWEEN TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS')";
		return $this->tnde->query($sql)->row_array();
	}

	function total_surat_masuk_perihal($perihal){
		$sql = "SELECT COUNT(ID_SURAT_MASUK) AS TOTAL FROM MD_SURAT_MASUK WHERE UPPER(PERIHAL) LIKE '%".$perihal."%' ";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_dm_by_nip_tgl_awal($nip, $perihal, $tgl_awal){
		$sql = "SELECT COUNT(A.ID_DISTRIBUSI_SURAT) AS TOTAL FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B WHERE B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND A.WAKTU_KIRIM >= TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND UPPER(B.PERIHAL) LIKE '%".$perihal."%' AND A.KD_STATUS_DISTRIBUSI='D' AND A.PENERIMA_SURAT='".$nip."' ";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_dm_by_nip_tgl_akhir($nip, $perihal, $tgl_akhir){
		$sql = "SELECT COUNT(A.ID_DISTRIBUSI_SURAT) AS TOTAL FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B WHERE B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND A.WAKTU_KIRIM <= TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND UPPER(B.PERIHAL) LIKE '%".$perihal."%' AND A.KD_STATUS_DISTRIBUSI='D' AND A.PENERIMA_SURAT='".$nip."' ";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_dm_by_nip_periode($nip, $perihal, $tgl_awal, $tgl_akhir){
		$sql = "SELECT COUNT(A.ID_DISTRIBUSI_SURAT) AS TOTAL FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B WHERE B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND A.WAKTU_KIRIM BETWEEN TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND UPPER(B.PERIHAL) LIKE '%".$perihal."%' AND A.KD_STATUS_DISTRIBUSI='D' AND A.PENERIMA_SURAT='".$nip."' ";
		return $this->tnde->query($sql)->row_array();	
	}	

	function total_dm_by_nip_perihal($nip, $perihal){
		$sql = "SELECT COUNT(A.ID_DISTRIBUSI_SURAT) AS TOTAL FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B WHERE B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND UPPER(B.PERIHAL) LIKE '%".$perihal."%' AND A.KD_STATUS_DISTRIBUSI='D' AND A.PENERIMA_SURAT='".$nip."' ";
		return $this->tnde->query($sql)->row_array();	
	}	
	
	/*==============================*/
	function total_dm_by_kd_pegawai_tgl_awal($kd_pegawai, $perihal, $tgl_awal){
		$sql = "SELECT COUNT(A.ID_DISTRIBUSI_SURAT) AS TOTAL FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B WHERE B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND A.WAKTU_KIRIM >= TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND UPPER(B.PERIHAL) LIKE '%".$perihal."%' AND A.KD_STATUS_DISTRIBUSI='D' AND A.PENERIMA_SURAT='".$kd_pegawai."' ";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_dm_by_kd_pegawai_tgl_akhir($kd_pegawai, $perihal, $tgl_akhir){
		$sql = "SELECT COUNT(A.ID_DISTRIBUSI_SURAT) AS TOTAL FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B WHERE B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND A.WAKTU_KIRIM <= TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND UPPER(B.PERIHAL) LIKE '%".$perihal."%' AND A.KD_STATUS_DISTRIBUSI='D' AND A.PENERIMA_SURAT='".$kd_pegawai."' ";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_dm_by_kd_pegawai_periode($kd_pegawai, $perihal, $tgl_awal, $tgl_akhir){
		$sql = "SELECT COUNT(A.ID_DISTRIBUSI_SURAT) AS TOTAL FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B WHERE B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND A.WAKTU_KIRIM BETWEEN TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND UPPER(B.PERIHAL) LIKE '%".$perihal."%' AND A.KD_STATUS_DISTRIBUSI='D' AND A.PENERIMA_SURAT='".$kd_pegawai."' ";
		return $this->tnde->query($sql)->row_array();	
	}	

	function total_dm_by_kd_pegawai_perihal($kd_pegawai, $perihal){
		$sql = "SELECT COUNT(A.ID_DISTRIBUSI_SURAT) AS TOTAL FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B WHERE B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND UPPER(B.PERIHAL) LIKE '%".$perihal."%' AND A.KD_STATUS_DISTRIBUSI='D' AND A.PENERIMA_SURAT='".$kd_pegawai."' ";
		return $this->tnde->query($sql)->row_array();	
	}	
	/*==============================*/
	
	function total_user_by_nama($nama){
		$sql = "SELECT COUNT(A.ID_USER) AS TOTAL FROM D_USER_TNDE A, D_PEGAWAI B WHERE B.KD_PEGAWAI=A.KD_PEGAWAI AND UPPER(B.NM_PEGAWAI) LIKE '%".$nama."%' ";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_user_by_nama_nip($key){
		$sql = "SELECT COUNT(A.ID_USER) AS TOTAL FROM D_USER_TNDE A, D_PEGAWAI B WHERE B.KD_PEGAWAI=A.KD_PEGAWAI AND (UPPER(B.NM_PEGAWAI) LIKE '%".$key."%' OR UPPER(B.KD_PEGAWAI) LIKE '%".$key."%')";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_user_by_nama_kd_pegawai($key){
		$sql = "SELECT COUNT(A.ID_USER) AS TOTAL FROM D_USER_TNDE A, D_PEGAWAI B WHERE B.KD_PEGAWAI=A.KD_PEGAWAI AND (UPPER(B.NM_PEGAWAI) LIKE '%".$key."%' OR UPPER(B.KD_PEGAWAI) LIKE '%".$key."%')";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_sk_by_kd_peg_or_jab($kd_pegawai, $kd_jabatan){
		$sql = "SELECT COUNT(*) AS TOTAL FROM MD_SURAT_KELUAR WHERE PEMBUAT_SURAT='".$kd_pegawai."' OR JABATAN_PEMBUAT='".$kd_jabatan."'";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_sk_by_kd_peg($kd_pegawai){
		$this->tnde->where('PEMBUAT_SURAT',$kd_pegawai);
		$this->tnde->from('MD_SURAT_KELUAR');
		return $this->tnde->count_all_results();
	}
	
	function total_surat_keluar(){
		return $this->tnde->count_all('MD_SURAT_KELUAR');
	}
	
	function total_sk_by_perihal($perihal){
		$sql = "SELECT COUNT(*) AS TOTAL FROM MD_SURAT_KELUAR WHERE UPPER(PERIHAL) LIKE '%".$perihal."%'";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_sk_by_perihal_kd_peg($kd_pegawai, $perihal){
		$sql = "SELECT COUNT(*) AS TOTAL FROM MD_SURAT_KELUAR WHERE PEMBUAT_SURAT='".$kd_pegawai."' AND UPPER(PERIHAL) LIKE '%".$perihal."%'";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_sk_by_perihal_kd_peg_or_jab($kd_pegawai, $kd_jabatan, $perihal){
		$sql = "SELECT COUNT(*) AS TOTAL FROM MD_SURAT_KELUAR WHERE (PEMBUAT_SURAT='".$kd_pegawai."' OR JABATAN_PEMBUAT='".$kd_jabatan."') AND (UPPER(PERIHAL) LIKE '%".$perihal."%')";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_sk_by_perihal_kd_jabatan($kd_jabatan, $perihal){
		$sql = "SELECT COUNT(*) AS TOTAL FROM MD_SURAT_KELUAR WHERE JABATAN_PEMBUAT='".$kd_jabatan."' AND UPPER(PERIHAL) LIKE '%".$perihal."%'";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_sk_by_kd_jab($kd_jabatan){
		$this->tnde->where('JABATAN_PEMBUAT', $kd_jabatan);
		$this->tnde->from('MD_SURAT_KELUAR');
		return $this->tnde->count_all_results();
	}
	
	function get_surat_masuk_limit($limit){
		$sql = "SELECT * FROM ( SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, A.ID_STATUS_SM, B.NM_STATUS_SM FROM MD_SURAT_MASUK A, MD_STATUS_SM B WHERE B.ID_STATUS_SM=A.ID_STATUS_SM ORDER BY A.WAKTU_PENCATATAN DESC) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_surat_masuk_paging($limit, $page){
		$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, ID_STATUS_SM, NM_STATUS_SM, ROWNUM R FROM ( SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, A.ID_STATUS_SM, B.NM_STATUS_SM FROM MD_SURAT_MASUK A, MD_STATUS_SM B WHERE B.ID_STATUS_SM=A.ID_STATUS_SM ORDER BY A.WAKTU_PENCATATAN DESC) WHERE ROWNUM <= ".$limit.") WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function get_surat_masuk_tgl_awal_limit($limit, $perihal, $tgl_awal){
		$sql = "SELECT * FROM ( SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, A.ID_STATUS_SM, B.NM_STATUS_SM FROM MD_SURAT_MASUK A, MD_STATUS_SM B WHERE B.ID_STATUS_SM=A.ID_STATUS_SM AND UPPER(A.PERIHAL) LIKE '%".$perihal."%' AND A.TGL_MASUK >= TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') ORDER BY A.WAKTU_PENCATATAN DESC) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_surat_masuk_tgl_awal_paging($limit, $page, $perihal, $tgl_awal){
		$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, ID_STATUS_SM, NM_STATUS_SM, ROWNUM R FROM ( SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, A.ID_STATUS_SM, B.NM_STATUS_SM FROM MD_SURAT_MASUK A, MD_STATUS_SM B WHERE B.ID_STATUS_SM=A.ID_STATUS_SM AND UPPER(A.PERIHAL) LIKE '%".$perihal."%' AND A.TGL_MASUK >= TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') ORDER BY A.WAKTU_PENCATATAN DESC) WHERE ROWNUM <= ".$limit.") WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function get_surat_masuk_tgl_akhir_limit($limit, $perihal, $tgl_akhir){
		$sql = "SELECT * FROM ( SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, A.ID_STATUS_SM, B.NM_STATUS_SM FROM MD_SURAT_MASUK A, MD_STATUS_SM B WHERE B.ID_STATUS_SM=A.ID_STATUS_SM AND UPPER(A.PERIHAL) LIKE '%".$perihal."%' AND A.TGL_MASUK <= TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') ORDER BY A.WAKTU_PENCATATAN DESC) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_surat_masuk_tgl_akhir_paging($limit, $page, $perihal, $tgl_akhir){
		$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, ID_STATUS_SM, NM_STATUS_SM, ROWNUM R FROM ( SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, A.ID_STATUS_SM, B.NM_STATUS_SM FROM MD_SURAT_MASUK A, MD_STATUS_SM B WHERE B.ID_STATUS_SM=A.ID_STATUS_SM AND UPPER(A.PERIHAL) LIKE '%".$perihal."%' AND A.TGL_MASUK <= TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') ORDER BY A.WAKTU_PENCATATAN DESC) WHERE ROWNUM <= ".$limit.") WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function get_surat_masuk_periode_limit($limit, $perihal, $tgl_awal, $tgl_akhir){
		$sql = "SELECT * FROM ( SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, A.ID_STATUS_SM, B.NM_STATUS_SM FROM MD_SURAT_MASUK A, MD_STATUS_SM B WHERE B.ID_STATUS_SM=A.ID_STATUS_SM AND UPPER(A.PERIHAL) LIKE '%".$perihal."%' AND A.TGL_MASUK BETWEEN TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') ORDER BY A.WAKTU_PENCATATAN DESC) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_surat_masuk_periode_paging($limit, $page, $perihal, $tgl_awal, $tgl_akhir){
		$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, ID_STATUS_SM, NM_STATUS_SM, ROWNUM R FROM ( SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, A.ID_STATUS_SM, B.NM_STATUS_SM FROM MD_SURAT_MASUK A, MD_STATUS_SM B WHERE B.ID_STATUS_SM=A.ID_STATUS_SM AND UPPER(A.PERIHAL) LIKE '%".$perihal."%' AND A.TGL_MASUK BETWEEN TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') ORDER BY A.WAKTU_PENCATATAN DESC) WHERE ROWNUM <= ".$limit.") WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}

	function get_surat_masuk_perihal_limit($limit, $perihal){
		$sql = "SELECT * FROM ( SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, A.ID_STATUS_SM, B.NM_STATUS_SM FROM MD_SURAT_MASUK A, MD_STATUS_SM B WHERE B.ID_STATUS_SM=A.ID_STATUS_SM AND UPPER(A.PERIHAL) LIKE '%".$perihal."%' ORDER BY A.WAKTU_PENCATATAN DESC) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}

	function get_surat_masuk_perihal_paging($limit, $page, $perihal){
		$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, ID_STATUS_SM, NM_STATUS_SM, ROWNUM R FROM ( SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, A.ID_STATUS_SM, B.NM_STATUS_SM FROM MD_SURAT_MASUK A, MD_STATUS_SM B WHERE B.ID_STATUS_SM=A.ID_STATUS_SM AND UPPER(A.PERIHAL) LIKE '%".$perihal."%' ORDER BY A.WAKTU_PENCATATAN DESC) WHERE ROWNUM <= ".$limit.") WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function get_dm_by_nip_limit($nip, $limit){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, B.NM_STATUS_SM, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, 
				C.ID_DISTRIBUSI_SURAT, C.ID_STATUS_SM FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE C.PENERIMA_SURAT='".$nip."' AND 
				A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.")";
		return $this->tnde->query($sql)->result();
	}

	function get_dm_by_nip_paging($nip, $limit, $page){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, NM_STATUS_SM, WAKTU_KIRIM, WAKTU_PENYELESAIAN, 
				ID_DISTRIBUSI_SURAT, ID_STATUS_SM, ROWNUM R FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, B.NM_STATUS_SM, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, 
				TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, C.ID_DISTRIBUSI_SURAT, C.ID_STATUS_SM FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE 
				C.PENERIMA_SURAT='".$nip."' AND A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE 
				ROWNUM <= ".$limit.") WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function get_dm_by_nip_tgl_awal_limit($nip, $limit, $perihal, $tgl_awal){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, C.ID_STATUS_SM, B.NM_STATUS_SM, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, C.ID_DISTRIBUSI_SURAT FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND C.PENERIMA_SURAT='".$nip."' AND C.WAKTU_KIRIM >= TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND UPPER(A.PERIHAL) LIKE '%".$perihal."%' AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.")";
		return $this->tnde->query($sql)->result();
	}
	
	function get_dm_by_nip_tgl_awal_paging($nip, $limit, $page, $perihal, $tgl_awal){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, WAKTU_PENCATATAN, ID_STATUS_SM, NM_STATUS_SM, ID_DISTRIBUSI_SURAT, WAKTU_KIRIM, WAKTU_PENYELESAIAN, ROWNUM R FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, C.ID_STATUS_SM, B.NM_STATUS_SM, C.ID_DISTRIBUSI_SURAT, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND C.PENERIMA_SURAT='".$nip."' AND C.WAKTU_KIRIM >= TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND UPPER(PERIHAL) LIKE '%".$perihal."%' AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.") WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();	
	}
	
	function get_dm_by_nip_tgl_akhir_limit($nip, $limit, $perihal, $tgl_akhir){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, C.ID_STATUS_SM, B.NM_STATUS_SM, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, C.ID_DISTRIBUSI_SURAT FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND C.PENERIMA_SURAT='".$nip."' AND C.WAKTU_KIRIM <= TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND UPPER(A.PERIHAL) LIKE '%".$perihal."%' AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.")";
		return $this->tnde->query($sql)->result();
	}
	
	function get_dm_by_nip_tgl_akhir_paging($nip, $limit, $page, $perihal, $tgl_akhir){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, WAKTU_PENCATATAN, ID_STATUS_SM, NM_STATUS_SM, ID_DISTRIBUSI_SURAT, WAKTU_KIRIM, WAKTU_PENYELESAIAN, ROWNUM R FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, C.ID_STATUS_SM, B.NM_STATUS_SM, C.ID_DISTRIBUSI_SURAT, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND C.PENERIMA_SURAT='".$nip."' AND C.WAKTU_KIRIM <= TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND UPPER(PERIHAL) LIKE '%".$perihal."%' AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.") WHERE R > ".$page." ) ";
		return $this->tnde->query($sql)->result();	
	}
	
	function get_dm_by_nip_periode_limit($nip, $limit, $perihal, $tgl_awal, $tgl_akhir){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, C.ID_STATUS_SM, B.NM_STATUS_SM, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, C.ID_DISTRIBUSI_SURAT FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND C.PENERIMA_SURAT='".$nip."' AND C.WAKTU_KIRIM BETWEEN TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND UPPER(A.PERIHAL) LIKE '%".$perihal."%' AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.")";
		return $this->tnde->query($sql)->result();	
	}
	
	function get_dm_by_nip_periode_paging($nip, $limit, $page, $perihal, $tgl_awal, $tgl_akhir){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, WAKTU_PENCATATAN, ID_STATUS_SM, NM_STATUS_SM, ID_DISTRIBUSI_SURAT, WAKTU_KIRIM, WAKTU_PENYELESAIAN, ROWNUM R FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, C.ID_STATUS_SM, B.NM_STATUS_SM, C.ID_DISTRIBUSI_SURAT, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND C.PENERIMA_SURAT='".$nip."' AND C.WAKTU_KIRIM BETWEEN TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND UPPER(PERIHAL) LIKE '%".$perihal."%' AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.") WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();	
	}

	function get_dm_by_nip_perihal_limit($nip, $limit, $perihal){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, C.ID_STATUS_SM, B.NM_STATUS_SM, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, C.ID_DISTRIBUSI_SURAT FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND C.PENERIMA_SURAT='".$nip."' AND UPPER(A.PERIHAL) LIKE '%".$perihal."%' AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.")";
		return $this->tnde->query($sql)->result();	
	}

	function get_dm_by_nip_perihal_paging($nip, $limit, $page, $perihal){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, WAKTU_PENCATATAN, ID_STATUS_SM, NM_STATUS_SM, ID_DISTRIBUSI_SURAT, WAKTU_KIRIM, WAKTU_PENYELESAIAN, ROWNUM R FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, C.ID_STATUS_SM, B.NM_STATUS_SM, C.ID_DISTRIBUSI_SURAT, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND C.PENERIMA_SURAT='".$nip."' AND UPPER(PERIHAL) LIKE '%".$perihal."%' AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.") WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();	
	}
	
	/*================================================*/
	function get_dm_by_kd_pegawai_limit($kd_pegawai, $limit){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, B.NM_STATUS_SM, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, C.ID_DISTRIBUSI_SURAT, C.ID_STATUS_SM FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE C.PENERIMA_SURAT='".$kd_pegawai."' AND C.KD_STATUS_DISTRIBUSI='D' AND A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.")";
		return $this->tnde->query($sql)->result();
	}

	function get_dm_by_kd_pegawai_paging($kd_pegawai, $limit, $page){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, NM_STATUS_SM, WAKTU_KIRIM, WAKTU_PENYELESAIAN, ID_DISTRIBUSI_SURAT, ID_STATUS_SM, ROWNUM R FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, B.NM_STATUS_SM, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, C.ID_DISTRIBUSI_SURAT, C.ID_STATUS_SM FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE C.PENERIMA_SURAT='".$kd_pegawai."' AND C.KD_STATUS_DISTRIBUSI='D' AND A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.") WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function get_dm_by_kd_pegawai_tgl_awal_limit($kd_pegawai, $limit, $perihal, $tgl_awal){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, C.ID_STATUS_SM, B.NM_STATUS_SM, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, C.ID_DISTRIBUSI_SURAT FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND C.PENERIMA_SURAT='".$kd_pegawai."' AND C.KD_STATUS_DISTRIBUSI='D' AND C.WAKTU_KIRIM >= TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND UPPER(A.PERIHAL) LIKE '%".$perihal."%' AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.")";
		return $this->tnde->query($sql)->result();
	}
	
	function get_dm_by_kd_pegawai_tgl_awal_paging($kd_pegawai, $limit, $page, $perihal, $tgl_awal){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, WAKTU_PENCATATAN, ID_STATUS_SM, NM_STATUS_SM, ID_DISTRIBUSI_SURAT, WAKTU_KIRIM, WAKTU_PENYELESAIAN, ROWNUM R FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, C.ID_STATUS_SM, B.NM_STATUS_SM, C.ID_DISTRIBUSI_SURAT, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND C.PENERIMA_SURAT='".$kd_pegawai."' AND C.KD_STATUS_DISTRIBUSI='D' AND C.WAKTU_KIRIM >= TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND UPPER(PERIHAL) LIKE '%".$perihal."%' AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.") WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();	
	}
	
	function get_dm_by_kd_pegawai_tgl_akhir_limit($kd_pegawai, $limit, $perihal, $tgl_akhir){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, C.ID_STATUS_SM, B.NM_STATUS_SM, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, C.ID_DISTRIBUSI_SURAT FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND C.PENERIMA_SURAT='".$kd_pegawai."' AND C.KD_STATUS_DISTRIBUSI='D' AND C.WAKTU_KIRIM <= TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND UPPER(A.PERIHAL) LIKE '%".$perihal."%' AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.")";
		return $this->tnde->query($sql)->result();
	}
	
	function get_dm_by_kd_pegawai_tgl_akhir_paging($kd_pegawai, $limit, $page, $perihal, $tgl_akhir){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, WAKTU_PENCATATAN, ID_STATUS_SM, NM_STATUS_SM, ID_DISTRIBUSI_SURAT, WAKTU_KIRIM, WAKTU_PENYELESAIAN, ROWNUM R FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, C.ID_STATUS_SM, B.NM_STATUS_SM, C.ID_DISTRIBUSI_SURAT, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND C.PENERIMA_SURAT='".$kd_pegawai."' AND C.KD_STATUS_DISTRIBUSI='D' AND C.WAKTU_KIRIM <= TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND UPPER(PERIHAL) LIKE '%".$perihal."%' AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.") WHERE R > ".$page." ) ";
		return $this->tnde->query($sql)->result();	
	}
	
	function get_dm_by_kd_pegawai_periode_limit($kd_pegawai, $limit, $perihal, $tgl_awal, $tgl_akhir){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, C.ID_STATUS_SM, B.NM_STATUS_SM, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, C.ID_DISTRIBUSI_SURAT FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND C.PENERIMA_SURAT='".$kd_pegawai."' AND C.KD_STATUS_DISTRIBUSI='D' AND C.WAKTU_KIRIM BETWEEN TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND UPPER(A.PERIHAL) LIKE '%".$perihal."%' AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.")";
		return $this->tnde->query($sql)->result();	
	}
	
	function get_dm_by_kd_pegawai_periode_paging($kd_pegawai, $limit, $page, $perihal, $tgl_awal, $tgl_akhir){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, WAKTU_PENCATATAN, ID_STATUS_SM, NM_STATUS_SM, ID_DISTRIBUSI_SURAT, WAKTU_KIRIM, WAKTU_PENYELESAIAN, ROWNUM R FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, C.ID_STATUS_SM, B.NM_STATUS_SM, C.ID_DISTRIBUSI_SURAT, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND C.PENERIMA_SURAT='".$kd_pegawai."' AND C.KD_STATUS_DISTRIBUSI='D' AND C.WAKTU_KIRIM BETWEEN TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND UPPER(PERIHAL) LIKE '%".$perihal."%' AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.") WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();	
	}

	function get_dm_by_kd_pegawai_perihal_limit($kd_pegawai, $limit, $perihal){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, C.ID_STATUS_SM, B.NM_STATUS_SM, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, C.ID_DISTRIBUSI_SURAT FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND C.PENERIMA_SURAT='".$kd_pegawai."' AND C.KD_STATUS_DISTRIBUSI='D' AND UPPER(A.PERIHAL) LIKE '%".$perihal."%' AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.")";
		return $this->tnde->query($sql)->result();	
	}

	function get_dm_by_kd_pegawai_perihal_paging($kd_pegawai, $limit, $page, $perihal){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, WAKTU_PENCATATAN, ID_STATUS_SM, NM_STATUS_SM, ID_DISTRIBUSI_SURAT, WAKTU_KIRIM, WAKTU_PENYELESAIAN, ROWNUM R FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, C.ID_STATUS_SM, B.NM_STATUS_SM, C.ID_DISTRIBUSI_SURAT, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND C.PENERIMA_SURAT='".$kd_pegawai."' AND C.KD_STATUS_DISTRIBUSI='D' AND UPPER(PERIHAL) LIKE '%".$perihal."%' AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.") WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();	
	}
	/*================================================*/
	function get_pencarian_mendalam_limit($limit, $data){
		$sql = "SELECT * FROM ( SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, A.ID_STATUS_SM, B.NM_STATUS_SM
			FROM MD_SURAT_MASUK A, MD_STATUS_SM B
			WHERE A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND A.TGL_MASUK =TO_DATE('".$data['TGL_MASUK']."', 'DD/MM/YYYY') AND A.TGL_SURAT=TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY')
			AND UPPER(A.DARI) LIKE '%".$data['DARI']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%'
			AND UPPER(A.INDEKS_BERKAS) LIKE '%".$data['INDEKS_BERKAS']."%' AND B.ID_STATUS_SM=A.ID_STATUS_SM ) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_pencarian_mendalam_paging($limit, $data, $page){
		$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, ID_STATUS_SM, NM_STATUS_SM, ROWNUM R FROM ( SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, A.ID_STATUS_SM, B.NM_STATUS_SM FROM MD_SURAT_MASUK A, MD_STATUS_SM B WHERE A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND A.TGL_MASUK = TO_DATE('".$data['TGL_MASUK']."', 'DD/MM/YYYY') AND A.TGL_SURAT = TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY') AND UPPER(A.DARI) LIKE '%".$data['DARI']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' AND UPPER(A.INDEKS_BERKAS) LIKE '%".$data['INDEKS_BERKAS']."%' AND B.ID_STATUS_SM=A.ID_STATUS_SM ) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}

	function get_pencarian_mendalam_limit2($limit, $data){
		$sql = "SELECT * FROM ( SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, A.ID_STATUS_SM, B.NM_STATUS_SM
			FROM MD_SURAT_MASUK A, MD_STATUS_SM B
			WHERE A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND A.TGL_MASUK =TO_DATE('".$data['TGL_MASUK']."', 'DD/MM/YYYY')
			AND UPPER(A.DARI) LIKE '%".$data['DARI']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%'
			AND UPPER(A.INDEKS_BERKAS) LIKE '%".$data['INDEKS_BERKAS']."%' AND B.ID_STATUS_SM=A.ID_STATUS_SM ) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}

	function get_pencarian_mendalam_paging2($limit, $data, $page){
		$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, ID_STATUS_SM, NM_STATUS_SM, ROWNUM R FROM ( SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, A.ID_STATUS_SM, B.NM_STATUS_SM FROM MD_SURAT_MASUK A, MD_STATUS_SM B WHERE A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND A.TGL_MASUK = TO_DATE('".$data['TGL_MASUK']."', 'DD/MM/YYYY') AND UPPER(A.DARI) LIKE '%".$data['DARI']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' AND UPPER(A.INDEKS_BERKAS) LIKE '%".$data['INDEKS_BERKAS']."%' AND B.ID_STATUS_SM=A.ID_STATUS_SM ) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}

	function get_pencarian_mendalam_limit3($limit, $data){
		$sql = "SELECT * FROM ( SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, A.ID_STATUS_SM, B.NM_STATUS_SM
			FROM MD_SURAT_MASUK A, MD_STATUS_SM B
			WHERE A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND A.TGL_SURAT=TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY')
			AND UPPER(A.DARI) LIKE '%".$data['DARI']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%'
			AND UPPER(A.INDEKS_BERKAS) LIKE '%".$data['INDEKS_BERKAS']."%' AND B.ID_STATUS_SM=A.ID_STATUS_SM ) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}

	function get_pencarian_mendalam_paging3($limit, $data, $page){
		$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, ID_STATUS_SM, NM_STATUS_SM, ROWNUM R FROM ( SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, A.ID_STATUS_SM, B.NM_STATUS_SM FROM MD_SURAT_MASUK A, MD_STATUS_SM B WHERE A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND A.TGL_SURAT = TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY') AND UPPER(A.DARI) LIKE '%".$data['DARI']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' AND UPPER(A.INDEKS_BERKAS) LIKE '%".$data['INDEKS_BERKAS']."%' AND B.ID_STATUS_SM=A.ID_STATUS_SM ) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}

	function get_pencarian_mendalam_limit4($limit, $data){
		$sql = "SELECT * FROM ( SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, A.ID_STATUS_SM, B.NM_STATUS_SM
			FROM MD_SURAT_MASUK A, MD_STATUS_SM B
			WHERE A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' 
			AND UPPER(A.DARI) LIKE '%".$data['DARI']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%'
			AND UPPER(A.INDEKS_BERKAS) LIKE '%".$data['INDEKS_BERKAS']."%' AND B.ID_STATUS_SM=A.ID_STATUS_SM ) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}

	function get_pencarian_mendalam_paging4($limit, $data, $page){
		$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, ID_STATUS_SM, NM_STATUS_SM, ROWNUM R FROM ( SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, A.ID_STATUS_SM, B.NM_STATUS_SM FROM MD_SURAT_MASUK A, MD_STATUS_SM B WHERE A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND UPPER(A.DARI) LIKE '%".$data['DARI']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' AND UPPER(A.INDEKS_BERKAS) LIKE '%".$data['INDEKS_BERKAS']."%' AND B.ID_STATUS_SM=A.ID_STATUS_SM ) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function get_pencarian_mendalam_limit5($limit, $data){
		$sql = "SELECT * FROM ( SELECT DISTINCT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, A.ID_STATUS_SM, B.NM_STATUS_SM
			FROM MD_SURAT_MASUK A, MD_STATUS_SM B, MD_DOKUMEN C
			WHERE (A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND A.TGL_MASUK =TO_DATE('".$data['TGL_MASUK']."', 'DD/MM/YYYY')
			AND UPPER(A.DARI) LIKE '%".$data['DARI']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(A.INDEKS_BERKAS) LIKE '%".$data['INDEKS_BERKAS']."%' AND B.ID_STATUS_SM=A.ID_STATUS_SM AND C.ID_SURAT_MASUK=A.ID_SURAT_MASUK) AND (UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%'
			OR UPPER(C.ISI_DOKUMEN) LIKE '%".$data['PERIHAL']."%')) WHERE ROWNUM <= ".$limit." ";
		return $this->tnde->query($sql)->result();
	}
	
	function get_pencarian_mendalam_paging5($limit, $data, $page){
		//$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, ID_STATUS_SM, NM_STATUS_SM, ROWNUM R FROM ( SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, A.ID_STATUS_SM, B.NM_STATUS_SM FROM MD_SURAT_MASUK A, MD_STATUS_SM B WHERE A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND A.TGL_MASUK = TO_DATE('".$data['TGL_MASUK']."', 'DD/MM/YYYY') AND UPPER(A.DARI) LIKE '%".$data['DARI']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' AND UPPER(A.INDEKS_BERKAS) LIKE '%".$data['INDEKS_BERKAS']."%' AND B.ID_STATUS_SM=A.ID_STATUS_SM ) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, ID_STATUS_SM, NM_STATUS_SM, ROWNUM R FROM ( SELECT DISTINCT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, A.ID_STATUS_SM, B.NM_STATUS_SM FROM MD_SURAT_MASUK A, MD_STATUS_SM B, MD_DOKUMEN C WHERE (A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND A.TGL_MASUK = TO_DATE('".$data['TGL_MASUK']."', 'DD/MM/YYYY') AND UPPER(A.DARI) LIKE '%".$data['DARI']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(A.INDEKS_BERKAS) LIKE '%".$data['INDEKS_BERKAS']."%' AND B.ID_STATUS_SM=A.ID_STATUS_SM AND C.ID_SURAT_MASUK=A.ID_SURAT_MASUK) AND (UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' OR UPPER(C.ISI_DOKUMEN) LIKE '%".$data['PERIHAL']."%')) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}

	function get_pencarian_mendalam_limit6($limit, $data){
		/*$sql = "SELECT * FROM ( SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, A.ID_STATUS_SM, B.NM_STATUS_SM
			FROM MD_SURAT_MASUK A, MD_STATUS_SM B
			WHERE A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND A.TGL_SURAT=TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY')
			AND UPPER(A.DARI) LIKE '%".$data['DARI']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%'
			AND UPPER(A.INDEKS_BERKAS) LIKE '%".$data['INDEKS_BERKAS']."%' AND B.ID_STATUS_SM=A.ID_STATUS_SM ) WHERE ROWNUM <= ".$limit." ";*/
		$sql = "SELECT * FROM ( SELECT DISTINCT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, A.ID_STATUS_SM, B.NM_STATUS_SM
			FROM MD_SURAT_MASUK A, MD_STATUS_SM B, MD_DOKUMEN C
			WHERE (A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND A.TGL_SURAT=TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY')
			AND UPPER(A.DARI) LIKE '%".$data['DARI']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(A.INDEKS_BERKAS) LIKE '%".$data['INDEKS_BERKAS']."%' AND B.ID_STATUS_SM=A.ID_STATUS_SM AND C.ID_SURAT_MASUK=A.ID_SURAT_MASUK) AND (UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' OR UPPER(C.ISI_DOKUMEN) LIKE '%".$data['PERIHAL']."%')) WHERE ROWNUM <= ".$limit." ";
		return $this->tnde->query($sql)->result();
	}

	function get_pencarian_mendalam_paging6($limit, $data, $page){
		//$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, ID_STATUS_SM, NM_STATUS_SM, ROWNUM R FROM ( SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, A.ID_STATUS_SM, B.NM_STATUS_SM FROM MD_SURAT_MASUK A, MD_STATUS_SM B WHERE A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND A.TGL_SURAT = TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY') AND UPPER(A.DARI) LIKE '%".$data['DARI']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' AND UPPER(A.INDEKS_BERKAS) LIKE '%".$data['INDEKS_BERKAS']."%' AND B.ID_STATUS_SM=A.ID_STATUS_SM ) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, ID_STATUS_SM, NM_STATUS_SM, ROWNUM R FROM ( SELECT DISTINCT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, A.ID_STATUS_SM, B.NM_STATUS_SM FROM MD_SURAT_MASUK A, MD_STATUS_SM B, MD_DOKUMEN C WHERE (A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND A.TGL_SURAT = TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY') AND UPPER(A.DARI) LIKE '%".$data['DARI']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(A.INDEKS_BERKAS) LIKE '%".$data['INDEKS_BERKAS']."%' AND B.ID_STATUS_SM=A.ID_STATUS_SM AND C.ID_SURAT_MASUK=A.ID_SURAT_MASUK) AND (UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' OR UPPER(C.ISI_DOKUMEN) LIKE '%".$data['PERIHAL']."%' )) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function get_pencarian_mendalam_limit7($limit, $data){
		/*$sql = "SELECT * FROM ( SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, A.ID_STATUS_SM, B.NM_STATUS_SM
			FROM MD_SURAT_MASUK A, MD_STATUS_SM B
			WHERE A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' 
			AND UPPER(A.DARI) LIKE '%".$data['DARI']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%'
			AND UPPER(A.INDEKS_BERKAS) LIKE '%".$data['INDEKS_BERKAS']."%' AND B.ID_STATUS_SM=A.ID_STATUS_SM ) WHERE ROWNUM <= ".$limit."";*/
		$sql = " SELECT * FROM ( SELECT DISTINCT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, A.ID_STATUS_SM, B.NM_STATUS_SM
			FROM MD_SURAT_MASUK A, MD_STATUS_SM B, MD_DOKUMEN C
			WHERE (A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' 
			AND UPPER(A.DARI) LIKE '%".$data['DARI']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(A.INDEKS_BERKAS) LIKE '%".$data['INDEKS_BERKAS']."%' AND B.ID_STATUS_SM=A.ID_STATUS_SM AND C.ID_SURAT_MASUK=A.ID_SURAT_MASUK) AND (UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' OR UPPER(C.ISI_DOKUMEN) LIKE '%".$data['PERIHAL']."%' )) WHERE ROWNUM <= ".$limit." ";
		return $this->tnde->query($sql)->result();
	}
	
	function get_pencarian_mendalam_paging7($limit, $data, $page){
		//$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, ID_STATUS_SM, NM_STATUS_SM, ROWNUM R FROM ( SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, A.ID_STATUS_SM, B.NM_STATUS_SM FROM MD_SURAT_MASUK A, MD_STATUS_SM B WHERE A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND UPPER(A.DARI) LIKE '%".$data['DARI']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' AND UPPER(A.INDEKS_BERKAS) LIKE '%".$data['INDEKS_BERKAS']."%' AND B.ID_STATUS_SM=A.ID_STATUS_SM ) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, ID_STATUS_SM, NM_STATUS_SM, ROWNUM R FROM ( SELECT DISTINCT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, A.ID_STATUS_SM, B.NM_STATUS_SM FROM MD_SURAT_MASUK A, MD_STATUS_SM B, MD_DOKUMEN C WHERE (A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND UPPER(A.DARI) LIKE '%".$data['DARI']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(A.INDEKS_BERKAS) LIKE '%".$data['INDEKS_BERKAS']."%' AND B.ID_STATUS_SM=A.ID_STATUS_SM AND C.ID_SURAT_MASUK=A.ID_SURAT_MASUK) AND (UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' OR UPPER(C.ISI_DOKUMEN) LIKE '%".$data['PERIHAL']."%')) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function get_pencarian_mendalam_limit8($limit, $data){
		/*$sql = "SELECT * FROM ( SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, A.ID_STATUS_SM, B.NM_STATUS_SM
			FROM MD_SURAT_MASUK A, MD_STATUS_SM B
			WHERE A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND A.TGL_MASUK =TO_DATE('".$data['TGL_MASUK']."', 'DD/MM/YYYY') AND A.TGL_SURAT=TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY')
			AND UPPER(A.DARI) LIKE '%".$data['DARI']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%'
			AND UPPER(A.INDEKS_BERKAS) LIKE '%".$data['INDEKS_BERKAS']."%' AND B.ID_STATUS_SM=A.ID_STATUS_SM ) WHERE ROWNUM <= ".$limit." ";*/
		$sql = "SELECT * FROM ( SELECT DISTINCT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, A.ID_STATUS_SM, B.NM_STATUS_SM
			FROM MD_SURAT_MASUK A, MD_STATUS_SM B, MD_DOKUMEN C
			WHERE (A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND A.TGL_MASUK =TO_DATE('".$data['TGL_MASUK']."', 'DD/MM/YYYY') AND A.TGL_SURAT=TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY')
			AND UPPER(A.DARI) LIKE '%".$data['DARI']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(A.INDEKS_BERKAS) LIKE '%".$data['INDEKS_BERKAS']."%' AND B.ID_STATUS_SM=A.ID_STATUS_SM AND C.ID_SURAT_MASUK=A.ID_SURAT_MASUK) AND (UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' OR UPPER(C.ISI_DOKUMEN) LIKE '%".$data['PERIHAL']."%')) WHERE ROWNUM <= ".$limit." ";
		return $this->tnde->query($sql)->result();
	}
	
	function get_pencarian_mendalam_paging8($limit, $data, $page){
		//$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, ID_STATUS_SM, NM_STATUS_SM, ROWNUM R FROM ( SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, A.ID_STATUS_SM, B.NM_STATUS_SM FROM MD_SURAT_MASUK A, MD_STATUS_SM B WHERE A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND A.TGL_MASUK = TO_DATE('".$data['TGL_MASUK']."', 'DD/MM/YYYY') AND A.TGL_SURAT = TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY') AND UPPER(A.DARI) LIKE '%".$data['DARI']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' AND UPPER(A.INDEKS_BERKAS) LIKE '%".$data['INDEKS_BERKAS']."%' AND B.ID_STATUS_SM=A.ID_STATUS_SM ) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, ID_STATUS_SM, NM_STATUS_SM, ROWNUM R FROM ( SELECT DISTINCT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, A.ID_STATUS_SM, B.NM_STATUS_SM FROM MD_SURAT_MASUK A, MD_STATUS_SM B, MD_DOKUMEN C WHERE (A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND A.TGL_MASUK = TO_DATE('".$data['TGL_MASUK']."', 'DD/MM/YYYY') AND A.TGL_SURAT = TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY') AND UPPER(A.DARI) LIKE '%".$data['DARI']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(A.INDEKS_BERKAS) LIKE '%".$data['INDEKS_BERKAS']."%' AND B.ID_STATUS_SM=A.ID_STATUS_SM AND C.ID_SURAT_MASUK=A.ID_SURAT_MASUK) AND (UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' OR UPPER(C.ISI_DOKUMEN) LIKE '%".$data['PERIHAL']."%')) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function pencarian_sk_all_tgl_surat_limit($data, $limit){
		$sql = "SELECT * FROM ( SELECT DISTINCT A.ID_SURAT_KELUAR, A.NO_SURAT, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.KEPADA, A.PERIHAL, TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN, A.PEMBUAT_SURAT, A.JABATAN_PEMBUAT, A.KD_TU_BAGIAN FROM MD_SURAT_KELUAR A, MD_DOKUMEN_SK B WHERE (UPPER(A.PEMBUAT_SURAT) LIKE '%".$data['PEMBUAT_SURAT']."%' AND A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND A.TGL_SURAT =TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY') AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND B.ID_SURAT_KELUAR=A.ID_SURAT_KELUAR) AND (UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' OR UPPER(B.ISI_DOKUMEN_SK) LIKE '%".$data['PERIHAL']."%')ORDER BY WAKTU_SIMPAN DESC) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function pencarian_sk_all_tgl_surat_paging($data, $limit, $page){
		$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TGL_SURAT, KEPADA, PERIHAL, WAKTU_SIMPAN, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN, ROWNUM R FROM ( SELECT DISTINCT A.ID_SURAT_KELUAR, A.NO_SURAT, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.KEPADA, A.PERIHAL, TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN, A.PEMBUAT_SURAT, A.JABATAN_PEMBUAT, A.KD_TU_BAGIAN FROM MD_SURAT_KELUAR A, MD_DOKUMEN_SK B WHERE (UPPER(A.PEMBUAT_SURAT) LIKE '%".$data['PEMBUAT_SURAT']."%' AND A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND A.TGL_SURAT = TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY') AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND B.ID_SURAT_KELUAR=A.ID_SURAT_KELUAR) AND (UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' OR UPPER(B.ISI_DOKUMEN_SK) LIKE '%".$data['PERIHAL']."%') ORDER BY WAKTU_SIMPAN DESC) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function pencarian_sk_all_limit($data, $limit){
		$sql = "SELECT * FROM ( SELECT DISTINCT A.ID_SURAT_KELUAR, A.NO_SURAT, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.KEPADA, A.PERIHAL, TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN, A.PEMBUAT_SURAT, A.JABATAN_PEMBUAT, A.KD_TU_BAGIAN FROM MD_SURAT_KELUAR A, MD_DOKUMEN_SK B WHERE (UPPER(A.PEMBUAT_SURAT) LIKE '%".$data['PEMBUAT_SURAT']."%' AND A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND B.ID_SURAT_KELUAR=A.ID_SURAT_KELUAR) AND (UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' OR UPPER(B.ISI_DOKUMEN_SK) LIKE '%".$data['PERIHAL']."%')ORDER BY WAKTU_SIMPAN DESC) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function pencarian_sk_all_paging($data, $limit, $page){
		$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TGL_SURAT, KEPADA, PERIHAL, WAKTU_SIMPAN, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN, ROWNUM R FROM ( SELECT DISTINCT A.ID_SURAT_KELUAR, A.NO_SURAT, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.KEPADA, A.PERIHAL, TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN, A.PEMBUAT_SURAT, A.JABATAN_PEMBUAT, A.KD_TU_BAGIAN FROM MD_SURAT_KELUAR A, MD_DOKUMEN_SK B WHERE (UPPER(A.PEMBUAT_SURAT) LIKE '%".$data['PEMBUAT_SURAT']."%' AND A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND B.ID_SURAT_KELUAR=A.ID_SURAT_KELUAR) AND (UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' OR UPPER(B.ISI_DOKUMEN_SK) LIKE '%".$data['PERIHAL']."%') ORDER BY WAKTU_SIMPAN DESC) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function pencarian_sk_tgl_surat_limit($data, $limit){
		$sql = "SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, KEPADA, PERIHAL, TO_CHAR(WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN FROM MD_SURAT_KELUAR WHERE UPPER(PEMBUAT_SURAT) LIKE '%".$data['PEMBUAT_SURAT']."%' AND KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND TGL_SURAT=TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY') AND UPPER(NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(PERIHAL) LIKE '%".$data['PERIHAL']."%' ORDER BY WAKTU_SIMPAN DESC )  WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function pencarian_sk_tgl_surat_paging($data, $limit, $page){
		$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TGL_SURAT, KEPADA, PERIHAL, WAKTU_SIMPAN, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN, ROWNUM R FROM ( SELECT DISTINCT A.ID_SURAT_KELUAR, A.NO_SURAT, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.KEPADA, A.PERIHAL, TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN, A.PEMBUAT_SURAT, A.JABATAN_PEMBUAT, A.KD_TU_BAGIAN FROM MD_SURAT_KELUAR A, MD_DOKUMEN_SK B WHERE UPPER(A.PEMBUAT_SURAT) LIKE '%".$data['PEMBUAT_SURAT']."%' AND A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND TGL_SURAT=TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY') AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND B.ID_SURAT_KELUAR=A.ID_SURAT_KELUAR AND UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' ORDER BY WAKTU_SIMPAN DESC) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function pencarian_sk_limit($data, $limit){
		$sql = "SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, KEPADA, PERIHAL, TO_CHAR(WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN FROM MD_SURAT_KELUAR WHERE UPPER(PEMBUAT_SURAT) LIKE '%".$data['PEMBUAT_SURAT']."%' AND KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND UPPER(NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(PERIHAL) LIKE '%".$data['PERIHAL']."%' ORDER BY WAKTU_SIMPAN DESC )  WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function pencarian_sk_paging($data, $limit, $page){
		$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TGL_SURAT, KEPADA, PERIHAL, WAKTU_SIMPAN, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN, ROWNUM R FROM ( SELECT DISTINCT A.ID_SURAT_KELUAR, A.NO_SURAT, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.KEPADA, A.PERIHAL, TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN, A.PEMBUAT_SURAT, A.JABATAN_PEMBUAT, A.KD_TU_BAGIAN FROM MD_SURAT_KELUAR A, MD_DOKUMEN_SK B WHERE UPPER(A.PEMBUAT_SURAT) LIKE '%".$data['PEMBUAT_SURAT']."%' AND A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND B.ID_SURAT_KELUAR=A.ID_SURAT_KELUAR AND UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' ORDER BY WAKTU_SIMPAN DESC) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function get_penerima_ts_by_id_surat_masuk($id_surat_masuk){
		$sql = "SELECT A.PENERIMA_SURAT, A.JABATAN_PENERIMA, A.ID_STATUS_SM, TO_CHAR(A.WAKTU_AKSES, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_AKSES, B.NM_PEGAWAI, C.NM_JABATAN, D.NM_STATUS_SM
			FROM D_DISTRIBUSI_SURAT A, D_PEGAWAI B, MD_JABATAN C, MD_STATUS_SM D
			WHERE A.ID_SURAT_MASUK='".$id_surat_masuk."' AND A.KD_STATUS_DISTRIBUSI='TS' AND B.KD_PEGAWAI=A.PENERIMA_SURAT AND C.KD_JABATAN=A.JABATAN_PENERIMA AND D.ID_STATUS_SM=A.ID_STATUS_SM";
		return $this->tnde->query($sql)->result();
	}
	
	function get_pengolah_surat_by_id_surat_masuk($id_surat_masuk){
		$sql = "SELECT A.PENERIMA_SURAT, A.JABATAN_PENERIMA, A.ID_GRUP_SURAT, A.ID_STATUS_SM, TO_CHAR(A.WAKTU_AKSES, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_AKSES, B.NM_PEGAWAI, C.NM_JABATAN, D.NM_STATUS_SM
			FROM D_DISTRIBUSI_SURAT A, D_PEGAWAI B, MD_JABATAN C, MD_STATUS_SM D
			WHERE A.ID_SURAT_MASUK='".$id_surat_masuk."' AND A.KD_STATUS_DISTRIBUSI='PS' AND B.KD_PEGAWAI=A.PENERIMA_SURAT AND C.KD_JABATAN=A.JABATAN_PENERIMA AND D.ID_STATUS_SM=A.ID_STATUS_SM";
		return $this->tnde->query($sql)->result();
	}
	
	function get_penerima_disposisi_by_id_grup_surat($id_surat_masuk, $id_grup_surat){
		$sql = "SELECT A.PENERIMA_SURAT, A.JABATAN_PENERIMA, A.ID_STATUS_SM, TO_CHAR(A.WAKTU_AKSES, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_AKSES, B.NM_PEGAWAI, C.NM_JABATAN, D.NM_STATUS_SM
			FROM D_DISTRIBUSI_SURAT A, D_PEGAWAI B, MD_JABATAN C, MD_STATUS_SM D
			WHERE A.ID_SURAT_MASUK='".$id_surat_masuk."' AND A.ID_GRUP_SURAT='".$id_grup_surat."' AND A.KD_STATUS_DISTRIBUSI='D' AND B.KD_PEGAWAI=A.PENERIMA_SURAT AND C.KD_JABATAN=A.JABATAN_PENERIMA AND D.ID_STATUS_SM=A.ID_STATUS_SM ORDER BY A.WAKTU_KIRIM ASC";
		return $this->tnde->query($sql)->result();		
	}
	
	function get_user_limit($limit){
		$sql = "SELECT * FROM ( SELECT A.ID_USER, A.KD_PEGAWAI, A.KD_TU_BAGIAN, A.AKTIF, B.NM_PEGAWAI, C.NM_GRUP_USER
			FROM D_USER_TNDE A, D_PEGAWAI B, MD_GRUP_USER C
			WHERE B.KD_PEGAWAI=A.KD_PEGAWAI AND C.ID_GRUP_USER=A.ID_GRUP_USER ORDER BY A.ID_USER DESC)WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_user_paging($limit, $page){
		$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_USER, KD_PEGAWAI, KD_TU_BAGIAN, AKTIF, NM_PEGAWAI, NM_GRUP_USER, ROWNUM R FROM ( SELECT A.ID_USER, A.KD_PEGAWAI, A.KD_TU_BAGIAN, A.AKTIF, B.NM_PEGAWAI, C.NM_GRUP_USER FROM D_USER_TNDE A, D_PEGAWAI B, MD_GRUP_USER C WHERE B.KD_PEGAWAI=A.KD_PEGAWAI AND C.ID_GRUP_USER=A.ID_GRUP_USER ORDER BY A.ID_USER DESC)WHERE ROWNUM <= ".$limit.") WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function get_user_by_nama_limit($limit, $nama){
		$sql = "SELECT * FROM ( SELECT A.ID_USER, A.KD_PEGAWAI, A.KD_TU_BAGIAN, A.AKTIF, B.NM_PEGAWAI, C.NM_GRUP_USER FROM D_USER_TNDE A, D_PEGAWAI B, MD_GRUP_USER C WHERE B.KD_PEGAWAI=A.KD_PEGAWAI AND UPPER(B.NM_PEGAWAI) LIKE '%".$nama."%' AND C.ID_GRUP_USER=A.ID_GRUP_USER ORDER BY A.ID_USER DESC)WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_user_by_nama_paging($limit, $nama, $page){
		$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_USER, KD_PEGAWAI, KD_TU_BAGIAN, AKTIF, NM_PEGAWAI, NM_GRUP_USER, ROWNUM R FROM ( SELECT A.ID_USER, A.KD_PEGAWAI, A.KD_TU_BAGIAN, A.AKTIF, B.NM_PEGAWAI, C.NM_GRUP_USER FROM D_USER_TNDE A, D_PEGAWAI B, MD_GRUP_USER C WHERE B.KD_PEGAWAI=A.KD_PEGAWAI AND UPPER(B.NM_PEGAWAI) LIKE '%".$nama."%' AND C.ID_GRUP_USER=A.ID_GRUP_USER ORDER BY A.ID_USER DESC)WHERE ROWNUM <= ".$limit.") WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}

	function get_user_by_nama_nip_limit($limit, $key){
		$sql = "SELECT * FROM ( SELECT A.ID_USER, A.KD_PEGAWAI, A.KD_TU_BAGIAN, A.AKTIF, B.NM_PEGAWAI, C.NM_GRUP_USER FROM D_USER_TNDE A, D_PEGAWAI B, MD_GRUP_USER C WHERE B.KD_PEGAWAI=A.KD_PEGAWAI AND  C.ID_GRUP_USER=A.ID_GRUP_USER AND (UPPER(B.NM_PEGAWAI) LIKE '%".$key."%' OR UPPER(B.KD_PEGAWAI) LIKE '%".$key."%') ORDER BY A.ID_USER DESC)WHERE ROWNUM <= ".$limit." ";
		return $this->tnde->query($sql)->result();
	}

	function get_user_by_nama_nip_paging($limit, $key, $page){
		$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_USER, KD_PEGAWAI, KD_TU_BAGIAN, AKTIF, NM_PEGAWAI, NM_GRUP_USER, ROWNUM R FROM ( SELECT A.ID_USER, A.KD_PEGAWAI, A.KD_TU_BAGIAN, A.AKTIF, B.NM_PEGAWAI, C.NM_GRUP_USER FROM D_USER_TNDE A, D_PEGAWAI B, MD_GRUP_USER C WHERE A.KD_PEGAWAI=B.KD_PEGAWAI AND C.ID_GRUP_USER=A.ID_GRUP_USER AND (UPPER(B.NM_PEGAWAI) LIKE '%".$key."%' OR UPPER(B.KD_PEGAWAI) LIKE '%".$key."%') ORDER BY A.ID_USER DESC)WHERE ROWNUM <= ".$limit.") WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function get_user_by_nama_kd_pegawai_limit($limit, $key){
		$sql = "SELECT * FROM ( SELECT A.ID_USER, A.KD_PEGAWAI, A.KD_TU_BAGIAN, A.AKTIF, B.NM_PEGAWAI, C.NM_GRUP_USER FROM D_USER_TNDE A, D_PEGAWAI B, MD_GRUP_USER C WHERE B.KD_PEGAWAI=A.KD_PEGAWAI AND  C.ID_GRUP_USER=A.ID_GRUP_USER AND (UPPER(B.NM_PEGAWAI) LIKE '%".$key."%' OR UPPER(B.KD_PEGAWAI) LIKE '%".$key."%') ORDER BY A.ID_USER DESC)WHERE ROWNUM <= ".$limit." ";
		return $this->tnde->query($sql)->result();
	}

	function get_user_by_nama_kd_pegawai_paging($limit, $key, $page){
		$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_USER, KD_PEGAWAI, KD_TU_BAGIAN, AKTIF, NM_PEGAWAI, NM_GRUP_USER, ROWNUM R FROM ( SELECT A.ID_USER, A.KD_PEGAWAI, A.KD_TU_BAGIAN, A.AKTIF, B.NM_PEGAWAI, C.NM_GRUP_USER FROM D_USER_TNDE A, D_PEGAWAI B, MD_GRUP_USER C WHERE A.KD_PEGAWAI=B.KD_PEGAWAI AND C.ID_GRUP_USER=A.ID_GRUP_USER AND (UPPER(B.NM_PEGAWAI) LIKE '%".$key."%' OR UPPER(B.KD_PEGAWAI) LIKE '%".$key."%') ORDER BY A.ID_USER DESC)WHERE ROWNUM <= ".$limit.") WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function get_pegawai_by_nip($nip){
		$sql = "SELECT A.NIP, A.NM_PEGAWAI, B.NM_JABATAN, C.NM_TU_BAGIAN FROM D_PEGAWAI A, MD_JABATAN B, MD_TU_BAGIAN C WHERE A.NIP='".$nip."' AND B.KD_JABATAN=A.KD_JABATAN AND C.KD_TU_BAGIAN=A.KD_TU_BAGIAN";
		return $this->tnde->query($sql)->row_array();
	}
	
	function get_pegawai_by_kd_pegawai($kd_pegawai){
		$sql = "SELECT A.NIP, A.KD_PEGAWAI, A.NM_PEGAWAI, B.NM_JABATAN, C.NM_TU_BAGIAN FROM D_PEGAWAI A, MD_JABATAN B, MD_TU_BAGIAN C WHERE A.KD_PEGAWAI='".$kd_pegawai."' AND B.KD_JABATAN=A.KD_JABATAN AND C.KD_TU_BAGIAN=A.KD_TU_BAGIAN";
		return $this->tnde->query($sql)->row_array();
	}
	
	function get_sk_by_kd_peg_limit($kd_pegawai, $limit){
		$sql = "SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, KEPADA, PERIHAL FROM MD_SURAT_KELUAR WHERE PEMBUAT_SURAT='".$kd_pegawai."' ORDER BY WAKTU_SIMPAN DESC ) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_sk_by_kd_peg_paging($kd_pegawai, $limit, $page){
		$sql = "SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TGL_SURAT, KEPADA, PERIHAL, ROWNUM R FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, KEPADA, PERIHAL FROM MD_SURAT_KELUAR WHERE PEMBUAT_SURAT='".$kd_pegawai."' ORDER BY WAKTU_SIMPAN DESC ) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_sk_by_kd_peg_or_jab_limit($kd_pegawai, $kd_jabatan, $limit){
		$sql = "SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, KEPADA, PERIHAL FROM MD_SURAT_KELUAR WHERE PEMBUAT_SURAT='".$kd_pegawai."' OR JABATAN_PEMBUAT='".$kd_jabatan."' ORDER BY WAKTU_SIMPAN DESC ) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_sk_by_kd_peg_or_jab_paging($kd_pegawai, $kd_jabatan, $limit, $page){
		$sql = "SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TGL_SURAT, KEPADA, PERIHAL, ROWNUM R FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, KEPADA, PERIHAL FROM MD_SURAT_KELUAR WHERE PEMBUAT_SURAT='".$kd_pegawai."' OR JABATAN_PEMBUAT='".$kd_jabatan."' ORDER BY WAKTU_SIMPAN DESC ) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_sk_by_kd_jabatan_limit($kd_jabatan, $limit){
		$sql = "SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, KEPADA, PERIHAL FROM MD_SURAT_KELUAR WHERE JABATAN_PEMBUAT='".$kd_jabatan."' ORDER BY WAKTU_SIMPAN DESC ) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_sk_by_kd_jabatan_paging($kd_jabatan, $limit, $page){
		$sql = "SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TGL_SURAT, KEPADA, PERIHAL, ROWNUM R FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, KEPADA, PERIHAL FROM MD_SURAT_KELUAR WHERE JABATAN_PEMBUAT='".$kd_jabatan."' ORDER BY WAKTU_SIMPAN DESC ) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_surat_keluar_limit($limit){
		$sql = "SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, KEPADA, PERIHAL FROM MD_SURAT_KELUAR ORDER BY WAKTU_SIMPAN DESC ) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_surat_keluar_paging($limit, $page){
		$sql = "SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TGL_SURAT, KEPADA, PERIHAL, ROWNUM R FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, KEPADA, PERIHAL FROM MD_SURAT_KELUAR  ORDER BY WAKTU_SIMPAN DESC ) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();	
	}
	
	function get_sk_by_kd_peg_perihal_limit($kd_pegawai, $perihal, $limit){
		$sql = "SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, KEPADA, PERIHAL FROM MD_SURAT_KELUAR WHERE UPPER(PERIHAL) LIKE '%".$perihal."%' AND PEMBUAT_SURAT='".$kd_pegawai."' ORDER BY WAKTU_SIMPAN DESC ) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_sk_by_kd_peg_perihal_paging($kd_pegawai, $perihal, $limit, $page){
		$sql = "SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TGL_SURAT, KEPADA, PERIHAL, ROWNUM R FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, KEPADA, PERIHAL FROM MD_SURAT_KELUAR WHERE UPPER(PERIHAL) LIKE '%".$perihal."%' AND PEMBUAT_SURAT='".$kd_pegawai."' ORDER BY WAKTU_SIMPAN DESC ) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_sk_by_kd_peg_or_jab_perihal_limit($kd_pegawai, $kd_jabatan, $perihal, $limit){
		$sql = "SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, KEPADA, PERIHAL FROM MD_SURAT_KELUAR WHERE UPPER(PERIHAL) LIKE '%".$perihal."%' AND ( PEMBUAT_SURAT='".$kd_pegawai."' OR JABATAN_PEMBUAT='".$kd_jabatan."' ) ORDER BY WAKTU_SIMPAN DESC ) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_sk_by_kd_peg_or_jab_perihal_paging($kd_pegawai, $kd_jabatan, $perihal, $limit, $page){
		$sql = "SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TGL_SURAT, KEPADA, PERIHAL, ROWNUM R FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, KEPADA, PERIHAL FROM MD_SURAT_KELUAR WHERE UPPER(PERIHAL) LIKE '%".$perihal."%' AND ( PEMBUAT_SURAT='".$kd_pegawai."' OR JABATAN_PEMBUAT='".$kd_jabatan."' ) ORDER BY WAKTU_SIMPAN DESC ) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_sk_by_kd_jabatan_perihal_limit($kd_jabatan, $perihal, $limit){
		$sql = "SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, KEPADA, PERIHAL FROM MD_SURAT_KELUAR WHERE UPPER(PERIHAL) LIKE '%".$perihal."%' AND JABATAN_PEMBUAT='".$kd_jabatan."' ORDER BY WAKTU_SIMPAN DESC ) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}

	function get_sk_by_kd_jabatan_perihal_paging($kd_jabatan, $perihal, $limit, $page){
		$sql = "SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TGL_SURAT, KEPADA, PERIHAL, ROWNUM R FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, KEPADA, PERIHAL FROM MD_SURAT_KELUAR WHERE UPPER(PERIHAL) LIKE '%".$perihal."%' AND JABATAN_PEMBUAT='".$kd_jabatan."' ORDER BY WAKTU_SIMPAN DESC ) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_surat_keluar_by_perihal_limit($perihal, $limit){
		$sql = "SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, KEPADA, PERIHAL FROM MD_SURAT_KELUAR WHERE UPPER(PERIHAL) LIKE '%".$perihal."%' ORDER BY WAKTU_SIMPAN DESC ) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_surat_keluar_by_perihal_paging($perihal, $limit, $page){
		$sql = "SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TGL_SURAT, KEPADA, PERIHAL, ROWNUM R FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, KEPADA, PERIHAL FROM MD_SURAT_KELUAR WHERE UPPER(PERIHAL) LIKE '%".$perihal."%' ORDER BY WAKTU_SIMPAN DESC ) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();	
	}
	
	function get_dokumen_sk($id_surat_keluar){
		$sql = "SELECT ID_DOKUMEN_SK, ID_SURAT_KELUAR, NM_DOKUMEN_SK, TO_CHAR(WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN FROM MD_DOKUMEN_SK WHERE ID_SURAT_KELUAR='".$id_surat_keluar."' ORDER BY WAKTU_SIMPAN DESC";
		return $this->tnde->query($sql)->result();
	}
	
	function get_lampiran_sk($id_surat_keluar){
		$sql = "SELECT ID_LAMPIRAN_SK, ID_SURAT_KELUAR, NM_LAMPIRAN_SK, TO_CHAR(WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN FROM MD_LAMPIRAN_SK WHERE ID_SURAT_KELUAR='".$id_surat_keluar."' ORDER BY WAKTU_SIMPAN ASC";
		return $this->tnde->query($sql)->result();
	}
	
	function get_scan_sk($id_surat_keluar){
		$sql = "SELECT ID_SCAN_SK, ID_SURAT_KELUAR, NM_SCAN_SK, TO_CHAR(WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN FROM MD_SCAN_SK WHERE ID_SURAT_KELUAR='".$id_surat_keluar."' ORDER BY WAKTU_SIMPAN ASC";
		return $this->tnde->query($sql)->result();
	}
	
	function detail_sm_by_id_surat_masuk($id_surat_masuk){
		$sql = "SELECT A.ID_SURAT_MASUK, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.INDEKS_BERKAS, A.KODE, A.PERIHAL, A.DARI, A.KEPADA, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.NO_SURAT, A.CATATAN, A.LAMPIRAN, A.KD_KEAMANAN_SURAT, A.PENCATAT_SURAT, A.KD_TU_BAGIAN, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, A.PENGARAH_SURAT, A.JABATAN_PENGARAH, A.ID_STATUS_SM, TO_CHAR(A.WAKTU_AKSES, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_AKSES, B.NM_SIFAT_SURAT, C.NM_KEAMANAN_SURAT, D.NM_TU_BAGIAN, E.NM_STATUS_SM FROM MD_SURAT_MASUK A, MD_SIFAT_SURAT B, MD_KEAMANAN_SURAT C, MD_TU_BAGIAN D, MD_STATUS_SM E WHERE A.ID_SURAT_MASUK='".$id_surat_masuk."' AND B.KD_SIFAT_SURAT=A.KD_SIFAT_SURAT AND C.KD_KEAMANAN_SURAT=A.KD_KEAMANAN_SURAT AND D.KD_TU_BAGIAN=A.KD_TU_BAGIAN AND E.ID_STATUS_SM=A.ID_STATUS_SM";
		return $this->tnde->query($sql)->row_array();
	}
	
	function detail_sm_by_id_surat_masuk2($id_surat_masuk){
		$sql = "SELECT A.ID_SURAT_MASUK, A.INDEKS_BERKAS, A.KD_TU_BAGIAN, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.KD_SIFAT_SURAT, A.KODE, A.KD_KEAMANAN_SURAT, A.PERIHAL, A.DARI, A.KEPADA, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.NO_SURAT, A.CATATAN, A.ID_STATUS_SM, A.PENCATAT_SURAT, A.PENGARAH_SURAT, A.LAMPIRAN, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, TO_CHAR(A.WAKTU_AKSES, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_AKSES, B.NM_SIFAT_SURAT, C.NM_KEAMANAN_SURAT, D.NM_STATUS_SM FROM MD_SURAT_MASUK A, MD_SIFAT_SURAT B, MD_KEAMANAN_SURAT C, MD_STATUS_SM D WHERE A.ID_SURAT_MASUK='".$id_surat_masuk."' AND B.KD_SIFAT_SURAT=A.KD_SIFAT_SURAT AND C.KD_KEAMANAN_SURAT=A.KD_KEAMANAN_SURAT AND D.ID_STATUS_SM=A.ID_STATUS_SM";
		return $this->tnde->query($sql)->row_array();
	}
	
	function detail_arsip_by_id_surat_masuk($id_surat_masuk){
		$sql = "SELECT A.NM_PEGAWAI, B.NM_JABATAN FROM D_PEGAWAI A, MD_JABATAN B, MD_SURAT_MASUK C WHERE C.ID_SURAT_MASUK='".$id_surat_masuk."' AND A.KD_PEGAWAI=C.PENCATAT_SURAT AND B.KD_JABATAN=A.KD_JABATAN";
		return $this->tnde->query($sql)->row_array();	
	}
	
	function detail_pengarah_by_id_surat_masuk($id_surat_masuk){
		$sql = "SELECT A.NM_PEGAWAI, B.NM_JABATAN FROM D_PEGAWAI A, MD_JABATAN B, MD_SURAT_MASUK C WHERE C.ID_SURAT_MASUK='".$id_surat_masuk."' AND A.KD_PEGAWAI=C.PENGARAH_SURAT AND B.KD_JABATAN=A.KD_JABATAN";
		return $this->tnde->query($sql)->row_array();		
	}
	
	function detail_disposisi_masuk($id_distribusi_surat){
		$sql = "SELECT A.ID_DISTRIBUSI_SURAT, A.ID_GRUP_SURAT, A.PEMROSES_SURAT, A.JABATAN_PEMROSES, A.PENERIMA_SURAT, A.JABATAN_PENERIMA, A.ISI_RINGKAS, A.ISI_DISPOSISI, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, A.ID_STATUS_SM, B.ID_SURAT_MASUK, B.INDEKS_BERKAS, 
				B.NO_SURAT, TO_CHAR(B.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, TO_CHAR(B.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, B.KODE, B.PERIHAL, B.DARI, B.KEPADA, B.CATATAN, B.KD_TU_BAGIAN, B.PENCATAT_SURAT, B.PENGARAH_SURAT, B.LAMPIRAN, B.ID_STATUS_SM AS ID_STATUS_SM2, TO_CHAR(B.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, TO_CHAR(B.WAKTU_AKSES, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_AKSES, C.NM_SIFAT_SURAT, D.NM_KEAMANAN_SURAT, E.NM_TU_BAGIAN, F.NM_STATUS_SM 
				FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B, MD_SIFAT_SURAT C, MD_KEAMANAN_SURAT D, MD_TU_BAGIAN E, MD_STATUS_SM F WHERE 
				A.ID_DISTRIBUSI_SURAT='".$id_distribusi_surat."' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND C.KD_SIFAT_SURAT=B.KD_SIFAT_SURAT AND 
				D.KD_KEAMANAN_SURAT=B.KD_KEAMANAN_SURAT AND E.KD_TU_BAGIAN=B.KD_TU_BAGIAN AND F.ID_STATUS_SM=B.ID_STATUS_SM";
		return $this->tnde->query($sql)->row_array();
	}
	
	function detail_sm_by_id_distribusi_surat($id_distribusi_surat){
		$sql = "SELECT A.ID_SURAT_MASUK, A.INDEKS_BERKAS, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.KD_SIFAT_SURAT, A.KODE, A.KD_KEAMANAN_SURAT, A.PERIHAL, A.DARI,
		A.KEPADA, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.NO_SURAT, A.CATATAN, A.KD_TU_BAGIAN, A.PENCATAT_SURAT, A.PENGARAH_SURAT, A.LAMPIRAN, 
		A.ID_STATUS_SM AS ID_STATUS_SM2, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, 
		TO_CHAR(A.WAKTU_AKSES, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_AKSES, B.NM_SIFAT_SURAT, C.NM_KEAMANAN_SURAT, D.ID_DISTRIBUSI_SURAT, D.ID_GRUP_SURAT, D.ID_STATUS_SM, 
		TO_CHAR(D.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, D.ISI_RINGKAS, D.ISI_DISPOSISI, D.PEMROSES_SURAT, D.JABATAN_PEMROSES, D.JABATAN_PENERIMA, 
		D.PENERIMA_SURAT, TO_CHAR(D.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, E.NM_TU_BAGIAN, F.NM_STATUS_SM, G.NM_PEGAWAI AS NM_PEMROSES, H.NM_JABATAN AS NM_JABATAN_PEMROSES 
		FROM MD_SURAT_MASUK A, MD_SIFAT_SURAT B, MD_KEAMANAN_SURAT C, D_DISTRIBUSI_SURAT D, MD_TU_BAGIAN E, MD_STATUS_SM F, D_PEGAWAI G, MD_JABATAN H 
		WHERE D.ID_DISTRIBUSI_SURAT='".$id_distribusi_surat."' AND A.ID_SURAT_MASUK=D.ID_SURAT_MASUK AND B.KD_SIFAT_SURAT=A.KD_SIFAT_SURAT AND 
		C.KD_KEAMANAN_SURAT=A.KD_KEAMANAN_SURAT AND E.KD_TU_BAGIAN=A.KD_TU_BAGIAN AND F.ID_STATUS_SM=A.ID_STATUS_SM AND G.KD_PEGAWAI=D.PEMROSES_SURAT AND H.KD_JABATAN=D.JABATAN_PEMROSES";
		return $this->tnde->query($sql)->row_array();
	}
	
	function detail_user_by_id_user($id_user){
		$sql = "SELECT A.ID_USER, A.KD_PEGAWAI, A.ID_GRUP_USER, A.KD_TU_BAGIAN, A.AKTIF, A.PENGARAHAN, B.NM_PEGAWAI FROM D_USER_TNDE A, D_PEGAWAI B WHERE A.ID_USER='".$id_user."' AND B.KD_PEGAWAI=A.KD_PEGAWAI ";
		return $this->tnde->query($sql)->row_array();
	}
	
	function detail_sk_by_id_surat_keluar($id_surat_keluar){
		$sql = "SELECT A.ID_SURAT_KELUAR, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.PERIHAL, A.KEPADA, A.NO_SURAT, A.KD_KEAMANAN_SURAT, A.KD_SIFAT_SURAT, A.PEMBUAT_SURAT, A.STATUS_SK, A.KD_JENIS_SURAT, B.KD_PEGAWAI, B.NM_PEGAWAI, A.JABATAN_PEMBUAT, C.KD_JABATAN, C.NM_JABATAN, A.KD_TU_BAGIAN, D.NM_TU_BAGIAN, TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN, E.NM_JENIS_SURAT FROM MD_SURAT_KELUAR A, D_PEGAWAI B, MD_JABATAN C, MD_TU_BAGIAN D, MD_JENIS_SURAT E WHERE A.ID_SURAT_KELUAR='".$id_surat_keluar."' AND B.KD_PEGAWAI=A.PEMBUAT_SURAT AND C.KD_JABATAN=A.JABATAN_PEMBUAT AND D.KD_TU_BAGIAN=A.KD_TU_BAGIAN AND E.KD_JENIS_SURAT=A.KD_JENIS_SURAT";
		return $this->tnde->query($sql)->row_array();
	}
	
	function tembusan_by_id_surat_masuk($id_surat_masuk){
		$sql = "SELECT A.NM_JABATAN FROM MD_JABATAN A, D_DISTRIBUSI_SURAT B WHERE B.ID_SURAT_MASUK='".$id_surat_masuk."' AND B.KD_STATUS_DISTRIBUSI='TS' 
				AND A.KD_JABATAN=B.JABATAN_PENERIMA";
		return $this->tnde->query($sql)->result();
	}
	
	function diteruskan_kepada($id_surat_masuk, $jabatan_pemroses, $kd_status_distribusi){
		$sql = "SELECT A.NM_PEGAWAI FROM D_PEGAWAI A, D_DISTRIBUSI_SURAT B WHERE B.ID_SURAT_MASUK='".$id_surat_masuk."' AND B.JABATAN_PEMROSES='".$jabatan_pemroses."' 
				AND B.KD_STATUS_DISTRIBUSI='".$kd_status_distribusi."' AND A.KD_PEGAWAI=B.PENERIMA_SURAT";
		return $this->tnde->query($sql)->result();
	}
	
	function update_status_sm($id_distribusi_surat, $id_status_sm, $date){
		$sql = "UPDATE D_DISTRIBUSI_SURAT SET ID_STATUS_SM='".$id_status_sm."', WAKTU_AKSES=TO_DATE('".$date."', 'DD/MM/YYYY HH24:MI:SS') WHERE ID_DISTRIBUSI_SURAT='".$id_distribusi_surat."' ";
		$this->tnde->query($sql);
		/*$data['ID_STATUS_SM'] = $id_status_sm;
		$data['WAKTU_AKSES'] = $date;
		$this->tnde->where('ID_DISTRIBUSI_SURAT', $id_distribusi_surat);
		$this->tnde->update('D_DISTRIBUSI_SURAT', $data);*/
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function insert_user($data){
		$this->tnde->insert('D_USER_TNDE', $data);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function update_user_by_id_user($id_user, $data){
		$this->tnde->where('ID_USER', $id_user);
		$this->tnde->update('D_USER_TNDE', $data);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function insert_surat_keluar($data){
		$sql = "INSERT INTO MD_SURAT_KELUAR(ID_SURAT_KELUAR, TGL_SURAT, PERIHAL, KEPADA, NO_SURAT, KD_KEAMANAN_SURAT, KD_SIFAT_SURAT, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN, WAKTU_SIMPAN, STATUS_SK, KD_JENIS_SURAT)VALUES('".$data['ID_SURAT_KELUAR']."', TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY'), '".$data['PERIHAL']."', '".$data['KEPADA']."', '".$data['NO_SURAT']."', '".$data['KD_KEAMANAN_SURAT']."', '".$data['KD_SIFAT_SURAT']."', '".$data['PEMBUAT_SURAT']."', '".$data['JABATAN_PEMBUAT']."', '".$data['KD_TU_BAGIAN']."', TO_DATE('".$data['WAKTU_SIMPAN']."', 'DD/MM/YYYY HH24:MI:SS'), '".$data['STATUS_SK']."', '".$data['KD_JENIS_SURAT']."')";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function insert_dokumen_sk($data){
		$sql = "INSERT INTO MD_DOKUMEN_SK(ID_SURAT_KELUAR, NM_DOKUMEN_SK, ISI_DOKUMEN_SK, WAKTU_SIMPAN)VALUES('".$data['ID_SURAT_KELUAR']."', '".$data['NM_DOKUMEN_SK']."', '".$data['ISI_DOKUMEN_SK']."', TO_DATE('".$data['WAKTU_SIMPAN']."', 'DD/MM/YYYY HH24:MI:SS'))";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function insert_lampiran_sk($data){
		$sql = "INSERT INTO MD_LAMPIRAN_SK(ID_SURAT_KELUAR, NM_LAMPIRAN_SK, WAKTU_SIMPAN)VALUES('".$data['ID_SURAT_KELUAR']."', '".$data['NM_LAMPIRAN_SK']."', TO_DATE('".$data['WAKTU_SIMPAN']."', 'DD/MM/YYYY HH24:MI:SS'))";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function insert_scan_sk($data){
		$sql = "INSERT INTO MD_SCAN_SK(ID_SURAT_KELUAR, NM_SCAN_SK, WAKTU_SIMPAN)VALUES('".$data['ID_SURAT_KELUAR']."', '".$data['NM_SCAN_SK']."', TO_DATE('".$data['WAKTU_SIMPAN']."', 'DD/MM/YYYY HH24:MI:SS'))";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function update_sk_by_id_surat_keluar($data){
		$sql = "UPDATE MD_SURAT_KELUAR SET TGL_SURAT=TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY'), PERIHAL='".$data['PERIHAL']."', KEPADA='".$data['KEPADA']."', NO_SURAT='".$data['NO_SURAT']."', KD_KEAMANAN_SURAT='".$data['KD_KEAMANAN_SURAT']."', KD_SIFAT_SURAT='".$data['KD_SIFAT_SURAT']."', KD_TU_BAGIAN='".$data['KD_TU_BAGIAN']."', STATUS_SK='".$data['STATUS_SK']."', KD_JENIS_SURAT='".$data['KD_JENIS_SURAT']."' WHERE ID_SURAT_KELUAR='".$data['ID_SURAT_KELUAR']."' ";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function update_lampiran_sk($id_lampiran_sk, $data){
		$sql = "UPDATE MD_LAMPIRAN_SK SET NM_LAMPIRAN_SK='".$data['NM_LAMPIRAN_SK']."', WAKTU_SIMPAN=TO_DATE('".$data['WAKTU_SIMPAN']."', 'DD/MM/YYYY HH24:MI:SS') WHERE ID_LAMPIRAN_SK='".$id_lampiran_sk."' ";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function update_scan_sk($id_scan_sk, $data){
		$sql = "UPDATE MD_SCAN_SK SET NM_SCAN_SK='".$data['NM_SCAN_SK']."', WAKTU_SIMPAN=TO_DATE('".$data['WAKTU_SIMPAN']."', 'DD/MM/YYYY HH24:MI:SS') WHERE ID_SCAN_SK='".$id_scan_sk."' ";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function total_pencarian_sk_all_2($data){
		$sql = "SELECT COUNT(DISTINCT(A.ID_SURAT_KELUAR)) AS TOTAL FROM MD_SURAT_KELUAR A, MD_DOKUMEN_SK B WHERE (A.JABATAN_PEMBUAT LIKE '%".$data['JABATAN_PEMBUAT']."%' AND A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND A.TGL_SURAT=TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY') AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND B.ID_SURAT_KELUAR=A.ID_SURAT_KELUAR) AND (UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' OR UPPER(B.ISI_DOKUMEN_SK) LIKE '%".$data['PERIHAL']."%')";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_pencarian_sk_all2_2($data){
		$sql = "SELECT COUNT(DISTINCT(A.ID_SURAT_KELUAR)) AS TOTAL FROM MD_SURAT_KELUAR A, MD_DOKUMEN_SK B WHERE (A.JABATAN_PEMBUAT LIKE '%".$data['JABATAN_PEMBUAT']."%' AND A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND B.ID_SURAT_KELUAR=A.ID_SURAT_KELUAR) AND (UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' OR UPPER(B.ISI_DOKUMEN_SK) LIKE '%".$data['PERIHAL']."%')";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_pencarian_sk_all3_2($data){
		$sql = "SELECT COUNT(*) AS TOTAL FROM MD_SURAT_KELUAR WHERE JABATAN_PEMBUAT LIKE '%".$data['JABATAN_PEMBUAT']."%' AND UPPER(KD_TU_BAGIAN) LIKE '%".$data['KD_TU_BAGIAN']."%' AND TGL_SURAT=TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY') AND UPPER(NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(PERIHAL) LIKE '%".$data['PERIHAL']."%'";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_pencarian_sk_all4_2($data){
		$sql = "SELECT COUNT(*) AS TOTAL FROM MD_SURAT_KELUAR WHERE JABATAN_PEMBUAT LIKE '%".$data['JABATAN_PEMBUAT']."%' AND UPPER(KD_TU_BAGIAN) LIKE '%".$data['KD_TU_BAGIAN']."%' AND UPPER(NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(PERIHAL) LIKE '%".$data['PERIHAL']."%'";
		return $this->tnde->query($sql)->row_array();
	}
	
	function pencarian_sk_all_tgl_surat_limit2($data, $limit){
		$sql = "SELECT * FROM ( SELECT DISTINCT A.ID_SURAT_KELUAR, A.NO_SURAT, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.KEPADA, A.PERIHAL, TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN, A.PEMBUAT_SURAT, A.JABATAN_PEMBUAT, A.KD_TU_BAGIAN FROM MD_SURAT_KELUAR A, MD_DOKUMEN_SK B WHERE (A.JABATAN_PEMBUAT LIKE '%".$data['JABATAN_PEMBUAT']."%' AND A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND A.TGL_SURAT =TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY') AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND B.ID_SURAT_KELUAR=A.ID_SURAT_KELUAR) AND (UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' OR UPPER(B.ISI_DOKUMEN_SK) LIKE '%".$data['PERIHAL']."%')ORDER BY WAKTU_SIMPAN DESC) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function pencarian_sk_all_tgl_surat_paging2($data, $limit, $page){
		$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TGL_SURAT, KEPADA, PERIHAL, WAKTU_SIMPAN, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN, ROWNUM R FROM ( SELECT DISTINCT A.ID_SURAT_KELUAR, A.NO_SURAT, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.KEPADA, A.PERIHAL, TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN, A.PEMBUAT_SURAT, A.JABATAN_PEMBUAT, A.KD_TU_BAGIAN FROM MD_SURAT_KELUAR A, MD_DOKUMEN_SK B WHERE (A.JABATAN_PEMBUAT LIKE '%".$data['JABATAN_PEMBUAT']."%' AND A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND A.TGL_SURAT = TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY') AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND B.ID_SURAT_KELUAR=A.ID_SURAT_KELUAR) AND (UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' OR UPPER(B.ISI_DOKUMEN_SK) LIKE '%".$data['PERIHAL']."%') ORDER BY WAKTU_SIMPAN DESC) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function pencarian_sk_all_limit2($data, $limit){
		$sql = "SELECT * FROM ( SELECT DISTINCT A.ID_SURAT_KELUAR, A.NO_SURAT, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.KEPADA, A.PERIHAL, TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN, A.PEMBUAT_SURAT, A.JABATAN_PEMBUAT, A.KD_TU_BAGIAN FROM MD_SURAT_KELUAR A, MD_DOKUMEN_SK B WHERE (A.JABATAN_PEMBUAT LIKE '%".$data['JABATAN_PEMBUAT']."%' AND A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND B.ID_SURAT_KELUAR=A.ID_SURAT_KELUAR) AND (UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' OR UPPER(B.ISI_DOKUMEN_SK) LIKE '%".$data['PERIHAL']."%')ORDER BY WAKTU_SIMPAN DESC) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function pencarian_sk_all_paging2($data, $limit, $page){
		$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TGL_SURAT, KEPADA, PERIHAL, WAKTU_SIMPAN, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN, ROWNUM R FROM ( SELECT DISTINCT A.ID_SURAT_KELUAR, A.NO_SURAT, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.KEPADA, A.PERIHAL, TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN, A.PEMBUAT_SURAT, A.JABATAN_PEMBUAT, A.KD_TU_BAGIAN FROM MD_SURAT_KELUAR A, MD_DOKUMEN_SK B WHERE (A.JABATAN_PEMBUAT LIKE '%".$data['JABATAN_PEMBUAT']."%' AND A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND B.ID_SURAT_KELUAR=A.ID_SURAT_KELUAR) AND (UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' OR UPPER(B.ISI_DOKUMEN_SK) LIKE '%".$data['PERIHAL']."%') ORDER BY WAKTU_SIMPAN DESC) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function pencarian_sk_tgl_surat_limit2($data, $limit){
		$sql = "SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, KEPADA, PERIHAL, TO_CHAR(WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN FROM MD_SURAT_KELUAR WHERE JABATAN_PEMBUAT LIKE '%".$data['JABATAN_PEMBUAT']."%' AND KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND TGL_SURAT=TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY') AND UPPER(NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(PERIHAL) LIKE '%".$data['PERIHAL']."%' ORDER BY WAKTU_SIMPAN DESC )  WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function pencarian_sk_tgl_surat_paging2($data, $limit, $page){
		$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TGL_SURAT, KEPADA, PERIHAL, WAKTU_SIMPAN, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN, ROWNUM R FROM ( SELECT DISTINCT A.ID_SURAT_KELUAR, A.NO_SURAT, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.KEPADA, A.PERIHAL, TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN, A.PEMBUAT_SURAT, A.JABATAN_PEMBUAT, A.KD_TU_BAGIAN FROM MD_SURAT_KELUAR A, MD_DOKUMEN_SK B WHERE A.JABATAN_PEMBUAT LIKE '%".$data['JABATAN_PEMBUAT']."%' AND A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND TGL_SURAT=TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY') AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND B.ID_SURAT_KELUAR=A.ID_SURAT_KELUAR AND UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' ORDER BY WAKTU_SIMPAN DESC) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function pencarian_sk_limit2($data, $limit){
		$sql = "SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, KEPADA, PERIHAL, TO_CHAR(WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN FROM MD_SURAT_KELUAR WHERE JABATAN_PEMBUAT LIKE '%".$data['JABATAN_PEMBUAT']."%' AND KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND UPPER(NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(PERIHAL) LIKE '%".$data['PERIHAL']."%' ORDER BY WAKTU_SIMPAN DESC )  WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function pencarian_sk_paging2($data, $limit, $page){
		$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TGL_SURAT, KEPADA, PERIHAL, WAKTU_SIMPAN, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN, ROWNUM R FROM ( SELECT DISTINCT A.ID_SURAT_KELUAR, A.NO_SURAT, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.KEPADA, A.PERIHAL, TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN, A.PEMBUAT_SURAT, A.JABATAN_PEMBUAT, A.KD_TU_BAGIAN FROM MD_SURAT_KELUAR A, MD_DOKUMEN_SK B WHERE A.JABATAN_PEMBUAT LIKE '%".$data['JABATAN_PEMBUAT']."%' AND A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND B.ID_SURAT_KELUAR=A.ID_SURAT_KELUAR AND UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' ORDER BY WAKTU_SIMPAN DESC) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function get_no_register($tahun, $waktu, $kd_tu_bagian){
		$sql = "SELECT COUNT(*) AS TOTAL FROM MD_SURAT_MASUK WHERE TO_CHAR(WAKTU_PENCATATAN, 'YYYY')='".$tahun."' AND WAKTU_PENCATATAN <= TO_DATE('".$waktu."', 'DD/MM/YYYY HH24:MI:SS') AND KD_TU_BAGIAN='".$kd_tu_bagian."'";
		return $this->tnde->query($sql)->row_array();
	}
	
	function get_disposisi_masuk($id_surat_masuk, $id_grup_surat, $jabatan_penerima, $kd_status_distribusi){
		$data['ID_SURAT_MASUK'] = $id_surat_masuk;
		$data['ID_GRUP_SURAT'] = $id_grup_surat;
		$data['JABATAN_PENERIMA'] = $jabatan_penerima;
		$data['KD_STATUS_DISTRIBUSI'] = $kd_status_distribusi;
		return $this->tnde->get_where('D_DISTRIBUSI_SURAT', $data)->row_array();
	}
	
	function get_disposisi_masuk2($id_surat_masuk, $id_grup_surat, $jabatan_pemroses, $kd_status_distribusi){
		$data['ID_SURAT_MASUK'] = $id_surat_masuk;
		$data['ID_GRUP_SURAT'] = $id_grup_surat;
		$data['JABATAN_PEMROSES'] = $jabatan_pemroses;
		$data['KD_STATUS_DISTRIBUSI'] = $kd_status_distribusi;
		return $this->tnde->get_where('D_DISTRIBUSI_SURAT', $data)->result();
	}
	
	function get_disposisi_masuk3($id_surat_masuk, $id_grup_surat, $jabatan_pemroses, $kd_status_distribusi){
		$sql = "SELECT A.ID_SURAT_MASUK, A.INDEKS_BERKAS, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.KD_SIFAT_SURAT, A.KODE, A.KD_KEAMANAN_SURAT, A.PERIHAL, A.DARI, A.KEPADA, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.NO_SURAT, A.CATATAN, A.KD_TU_BAGIAN, A.PENCATAT_SURAT, A.PENGARAH_SURAT, A.LAMPIRAN, A.ID_STATUS_SM AS ID_STATUS_SM2, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, TO_CHAR(A.WAKTU_AKSES, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_AKSES, B.NM_SIFAT_SURAT, C.NM_KEAMANAN_SURAT, D.ID_DISTRIBUSI_SURAT, D.ID_GRUP_SURAT, D.ID_STATUS_SM, TO_CHAR(D.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, D.ISI_RINGKAS, D.ISI_DISPOSISI, D.PEMROSES_SURAT, D.JABATAN_PEMROSES, D.JABATAN_PENERIMA, D.PENERIMA_SURAT, TO_CHAR(D.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, E.NM_TU_BAGIAN, F.NM_STATUS_SM, G.NM_JABATAN AS NM_JABATAN_PEMROSES 
		FROM MD_SURAT_MASUK A, MD_SIFAT_SURAT B, MD_KEAMANAN_SURAT C, D_DISTRIBUSI_SURAT D, MD_TU_BAGIAN E, MD_STATUS_SM F, MD_JABATAN G 
		WHERE D.ID_SURAT_MASUK='".$id_surat_masuk."' AND D.ID_GRUP_SURAT='".$id_grup_surat."' AND D.JABATAN_PENERIMA='".$jabatan_pemroses."' AND D.KD_STATUS_DISTRIBUSI='".$kd_status_distribusi."' AND A.ID_SURAT_MASUK=D.ID_SURAT_MASUK AND B.KD_SIFAT_SURAT=A.KD_SIFAT_SURAT AND C.KD_KEAMANAN_SURAT=A.KD_KEAMANAN_SURAT AND E.KD_TU_BAGIAN=A.KD_TU_BAGIAN AND F.ID_STATUS_SM=A.ID_STATUS_SM AND G.KD_JABATAN=D.JABATAN_PEMROSES";
		return $this->tnde->query($sql)->row_array();
	}
	
	function get_ds_by_id_surat_masuk($id_surat_masuk, $kd_status_distribusi){
		$sql = "SELECT B.ID_DISTRIBUSI_SURAT, A.NM_JABATAN, B.ID_STATUS_SM, C.NM_STATUS_SM FROM MD_JABATAN A, D_DISTRIBUSI_SURAT B, MD_STATUS_SM C WHERE A.KD_JABATAN=B.JABATAN_PENERIMA AND B.ID_SURAT_MASUK='".$id_surat_masuk."' AND B.KD_STATUS_DISTRIBUSI='".$kd_status_distribusi."' AND C.ID_STATUS_SM=B.ID_STATUS_SM";
		return $this->tnde->query($sql)->result();
	}
	
	function get_ds_by_id_surat_masuk2($id_surat_masuk, $kd_status_distribusi, $jabatan_pemroses){
		$sql = "SELECT B.ID_DISTRIBUSI_SURAT, A.KD_JABATAN, A.NM_JABATAN, B.JABATAN_PENERIMA, B.ID_STATUS_SM, C.KD_PEGAWAI, C.NM_PEGAWAI, D.NM_STATUS_SM 
		FROM MD_JABATAN A, D_DISTRIBUSI_SURAT B, D_PEGAWAI C, MD_STATUS_SM D 
		WHERE A.KD_JABATAN=B.JABATAN_PENERIMA AND B.ID_SURAT_MASUK='".$id_surat_masuk."' AND B.JABATAN_PEMROSES='".$jabatan_pemroses."' AND B.KD_STATUS_DISTRIBUSI='".$kd_status_distribusi."' AND C.KD_PEGAWAI=B.PENERIMA_SURAT AND D.ID_STATUS_SM=B.ID_STATUS_SM";
		return $this->tnde->query($sql)->result();	
	}
	
	function get_ds_by_id_surat_masuk3($id_surat_masuk, $kd_status_distribusi, $pemroses_surat){
		$sql = "SELECT B.ID_DISTRIBUSI_SURAT, A.KD_JABATAN, A.NM_JABATAN, B.JABATAN_PENERIMA, B.ID_STATUS_SM, C.KD_PEGAWAI, C.NM_PEGAWAI, D.NM_STATUS_SM 
		FROM MD_JABATAN A, D_DISTRIBUSI_SURAT B, D_PEGAWAI C, MD_STATUS_SM D 
		WHERE A.KD_JABATAN=B.JABATAN_PENERIMA AND B.ID_SURAT_MASUK='".$id_surat_masuk."' AND B.PEMROSES_SURAT='".$pemroses_surat."' AND B.KD_STATUS_DISTRIBUSI='".$kd_status_distribusi."' AND C.KD_PEGAWAI=B.PENERIMA_SURAT AND D.ID_STATUS_SM=B.ID_STATUS_SM";
		return $this->tnde->query($sql)->result();	
	}
	
	function get_distribusi_surat_by_id_ds_sebelumnya($id_distribusi_surat){
		$sql = "SELECT B.ID_DISTRIBUSI_SURAT, A.KD_JABATAN, A.NM_JABATAN, B.JABATAN_PENERIMA, B.ID_STATUS_SM, C.KD_PEGAWAI, C.NM_PEGAWAI, D.NM_STATUS_SM 
		FROM MD_JABATAN A, D_DISTRIBUSI_SURAT B, D_PEGAWAI C, MD_STATUS_SM D 
		WHERE A.KD_JABATAN=B.JABATAN_PENERIMA AND B.ID_DS_SEBELUMNYA='".$id_distribusi_surat."' AND C.KD_PEGAWAI=B.PENERIMA_SURAT AND D.ID_STATUS_SM=B.ID_STATUS_SM";
		return $this->tnde->query($sql)->result();	
	}
	
	function get_pegawai_on_disposisi($id_surat_masuk, $id_grup_surat){
		$sql = "SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, A.PEMROSES_SURAT, A.JABATAN_PEMROSES, A.PENERIMA_SURAT, A.JABATAN_PENERIMA, A.KD_STATUS_DISTRIBUSI, B.KD_PEGAWAI, 
		B.NM_PEGAWAI, C.KD_JABATAN, C.NM_JABATAN
		FROM D_DISTRIBUSI_SURAT A, D_PEGAWAI B, MD_JABATAN C
		WHERE A.ID_SURAT_MASUK='".$id_surat_masuk."' AND A.ID_GRUP_SURAT='".$id_grup_surat."' AND (A.KD_STATUS_DISTRIBUSI='D' OR A.KD_STATUS_DISTRIBUSI='PS') AND B.KD_PEGAWAI=A.PENERIMA_SURAT 
		AND C.KD_JABATAN=A.JABATAN_PENERIMA";
		return $this->tnde->query($sql)->result();
	}
	
	function get_pejabat(){
		$sql = "SELECT A.KD_PEGAWAI, A.NM_PEGAWAI, A.KD_JABATAN, A.NIP, B.NM_JABATAN FROM D_PEGAWAI A, MD_JABATAN B WHERE A.KD_JABATAN != '193' AND A.KD_JABATAN != '194' AND B.KD_JABATAN=A.KD_JABATAN";
		return $this->tnde->query($sql)->result();
	}
	
	function detail_dk_by_id_distribusi_surat($id_distribusi_surat){
		$sql = "SELECT A.ID_SURAT_MASUK, A.INDEKS_BERKAS, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.KD_SIFAT_SURAT, A.KODE, A.KD_KEAMANAN_SURAT, A.PERIHAL, A.DARI, A.KEPADA, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.NO_SURAT, A.CATATAN, A.KD_TU_BAGIAN, A.LAMPIRAN, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, B.NM_SIFAT_SURAT, C.NM_KEAMANAN_SURAT, D.ID_DISTRIBUSI_SURAT, D.ID_GRUP_SURAT, TO_CHAR(D.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, D.ISI_RINGKAS, D.ISI_DISPOSISI, D.PEMROSES_SURAT, D.JABATAN_PEMROSES, D.PENERIMA_SURAT, D.JABATAN_PENERIMA, E.NM_TU_BAGIAN, F.NM_PEGAWAI FROM MD_SURAT_MASUK A, MD_SIFAT_SURAT B, MD_KEAMANAN_SURAT C, D_DISTRIBUSI_SURAT D, MD_TU_BAGIAN E, D_PEGAWAI F WHERE D.ID_DISTRIBUSI_SURAT='".$id_distribusi_surat."' AND A.ID_SURAT_MASUK=D.ID_SURAT_MASUK AND B.KD_SIFAT_SURAT=A.KD_SIFAT_SURAT AND C.KD_KEAMANAN_SURAT=A.KD_KEAMANAN_SURAT AND E.KD_TU_BAGIAN=A.KD_TU_BAGIAN AND F.KD_PEGAWAI=D.PENERIMA_SURAT";
		return $this->tnde->query($sql)->row_array();
	}
	
	function detail_dk_by_id_distribusi_surat2($id_surat_masuk, $kd_status_distribusi, $jabatan_pemroses){
		$sql = "SELECT A.ID_SURAT_MASUK, A.INDEKS_BERKAS, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.KD_SIFAT_SURAT, A.KODE, A.KD_KEAMANAN_SURAT, A.PERIHAL, A.DARI, A.KEPADA, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.NO_SURAT, A.CATATAN, A.KD_TU_BAGIAN, A.PENCATAT_SURAT, A.PENGARAH_SURAT, A.LAMPIRAN, A.ID_STATUS_SM AS ID_STATUS_SM2, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, TO_CHAR(A.WAKTU_AKSES, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_AKSES, B.NM_SIFAT_SURAT, C.NM_KEAMANAN_SURAT, D.ID_DISTRIBUSI_SURAT, D.ID_GRUP_SURAT, D.ID_STATUS_SM, TO_CHAR(D.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, D.ISI_RINGKAS, D.ISI_DISPOSISI, D.PEMROSES_SURAT, D.JABATAN_PEMROSES, D.JABATAN_PENERIMA, D.PENERIMA_SURAT, TO_CHAR(D.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, E.NM_TU_BAGIAN, F.NM_STATUS_SM, G.NM_PEGAWAI AS NM_PEMROSES, H.NM_JABATAN AS NM_JABATAN_PEMROSES 
		FROM MD_SURAT_MASUK A, MD_SIFAT_SURAT B, MD_KEAMANAN_SURAT C, D_DISTRIBUSI_SURAT D, MD_TU_BAGIAN E, MD_STATUS_SM F, D_PEGAWAI G, MD_JABATAN H WHERE D.ID_SURAT_MASUK='".$id_surat_masuk."' AND D.JABATAN_PEMROSES='".$jabatan_pemroses."' AND D.KD_STATUS_DISTRIBUSI='".$kd_status_distribusi."' AND A.ID_SURAT_MASUK=D.ID_SURAT_MASUK AND B.KD_SIFAT_SURAT=A.KD_SIFAT_SURAT AND C.KD_KEAMANAN_SURAT=A.KD_KEAMANAN_SURAT AND E.KD_TU_BAGIAN=A.KD_TU_BAGIAN AND F.ID_STATUS_SM=A.ID_STATUS_SM AND G.KD_PEGAWAI=D.PEMROSES_SURAT AND H.KD_JABATAN=D.JABATAN_PEMROSES";
		return $this->tnde->query($sql)->row_array();
	}
	
	function detail_dk_by_id_distribusi_surat3($id_surat_masuk, $jabatan_pemroses){
		$sql = "SELECT A.ID_SURAT_MASUK, A.INDEKS_BERKAS, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.KD_SIFAT_SURAT, A.KODE, A.KD_KEAMANAN_SURAT, A.PERIHAL, A.DARI, A.KEPADA, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.NO_SURAT, A.CATATAN, A.KD_TU_BAGIAN, A.PENCATAT_SURAT, A.PENGARAH_SURAT, A.LAMPIRAN, A.ID_STATUS_SM AS ID_STATUS_SM2, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, TO_CHAR(A.WAKTU_AKSES, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_AKSES, B.NM_SIFAT_SURAT, C.NM_KEAMANAN_SURAT, D.ID_DISTRIBUSI_SURAT, D.ID_GRUP_SURAT, D.ID_STATUS_SM, TO_CHAR(D.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, D.ISI_RINGKAS, D.ISI_DISPOSISI, D.PEMROSES_SURAT, D.JABATAN_PEMROSES, D.JABATAN_PENERIMA, D.PENERIMA_SURAT, TO_CHAR(D.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, D.KD_STATUS_DISTRIBUSI, E.NM_TU_BAGIAN, F.NM_STATUS_SM, G.NM_PEGAWAI AS NM_PEMROSES, H.NM_JABATAN AS NM_JABATAN_PEMROSES 
		FROM MD_SURAT_MASUK A, MD_SIFAT_SURAT B, MD_KEAMANAN_SURAT C, D_DISTRIBUSI_SURAT D, MD_TU_BAGIAN E, MD_STATUS_SM F, D_PEGAWAI G, MD_JABATAN H WHERE D.ID_SURAT_MASUK='".$id_surat_masuk."' AND D.JABATAN_PEMROSES='".$jabatan_pemroses."' AND (D.KD_STATUS_DISTRIBUSI ='D' OR D.KD_STATUS_DISTRIBUSI='TM') AND A.ID_SURAT_MASUK=D.ID_SURAT_MASUK AND B.KD_SIFAT_SURAT=A.KD_SIFAT_SURAT AND C.KD_KEAMANAN_SURAT=A.KD_KEAMANAN_SURAT AND E.KD_TU_BAGIAN=A.KD_TU_BAGIAN AND F.ID_STATUS_SM=A.ID_STATUS_SM AND G.KD_PEGAWAI=D.PEMROSES_SURAT AND H.KD_JABATAN=D.JABATAN_PEMROSES";
		return $this->tnde->query($sql)->row_array();
	}
	
	function detail_dk_by_id_distribusi_surat4($id_surat_masuk, $pemroses_surat){
		$sql = "SELECT A.ID_SURAT_MASUK, A.INDEKS_BERKAS, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.KD_SIFAT_SURAT, A.KODE, A.KD_KEAMANAN_SURAT, A.PERIHAL, A.DARI, A.KEPADA, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.NO_SURAT, A.CATATAN, A.KD_TU_BAGIAN, A.PENCATAT_SURAT, A.PENGARAH_SURAT, A.LAMPIRAN, A.ID_STATUS_SM AS ID_STATUS_SM2, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, TO_CHAR(A.WAKTU_AKSES, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_AKSES, B.NM_SIFAT_SURAT, C.NM_KEAMANAN_SURAT, D.ID_DISTRIBUSI_SURAT, D.ID_GRUP_SURAT, D.ID_STATUS_SM, TO_CHAR(D.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, D.ISI_RINGKAS, D.ISI_DISPOSISI, D.PEMROSES_SURAT, D.JABATAN_PEMROSES, D.JABATAN_PENERIMA, D.PENERIMA_SURAT, TO_CHAR(D.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, D.KD_STATUS_DISTRIBUSI, E.NM_TU_BAGIAN, F.NM_STATUS_SM, G.NM_PEGAWAI AS NM_PEMROSES, H.NM_JABATAN AS NM_JABATAN_PEMROSES 
		FROM MD_SURAT_MASUK A, MD_SIFAT_SURAT B, MD_KEAMANAN_SURAT C, D_DISTRIBUSI_SURAT D, MD_TU_BAGIAN E, MD_STATUS_SM F, D_PEGAWAI G, MD_JABATAN H WHERE D.ID_SURAT_MASUK='".$id_surat_masuk."' AND D.PEMROSES_SURAT='".$pemroses_surat."' AND (D.KD_STATUS_DISTRIBUSI ='D' OR D.KD_STATUS_DISTRIBUSI='TM') AND A.ID_SURAT_MASUK=D.ID_SURAT_MASUK AND B.KD_SIFAT_SURAT=A.KD_SIFAT_SURAT AND C.KD_KEAMANAN_SURAT=A.KD_KEAMANAN_SURAT AND E.KD_TU_BAGIAN=A.KD_TU_BAGIAN AND F.ID_STATUS_SM=A.ID_STATUS_SM AND G.KD_PEGAWAI=D.PEMROSES_SURAT AND H.KD_JABATAN=D.JABATAN_PEMROSES";
		return $this->tnde->query($sql)->row_array();
	}
	
	function detail_dk_by_id_ds_sebelumnya($id_distribusi_surat){
		$sql = "SELECT A.ID_SURAT_MASUK, A.INDEKS_BERKAS, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.KD_SIFAT_SURAT, A.KODE, A.KD_KEAMANAN_SURAT, A.PERIHAL, A.DARI, A.KEPADA, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.NO_SURAT, A.CATATAN, A.KD_TU_BAGIAN, A.PENCATAT_SURAT, A.PENGARAH_SURAT, A.LAMPIRAN, A.ID_STATUS_SM AS ID_STATUS_SM2, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, TO_CHAR(A.WAKTU_AKSES, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_AKSES, B.NM_SIFAT_SURAT, C.NM_KEAMANAN_SURAT, D.ID_DISTRIBUSI_SURAT, D.ID_GRUP_SURAT, D.ID_STATUS_SM, TO_CHAR(D.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, D.ISI_RINGKAS, D.ISI_DISPOSISI, D.PEMROSES_SURAT, D.JABATAN_PEMROSES, D.JABATAN_PENERIMA, D.PENERIMA_SURAT, TO_CHAR(D.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, D.KD_STATUS_DISTRIBUSI, E.NM_TU_BAGIAN, F.NM_STATUS_SM, G.NM_PEGAWAI AS NM_PEMROSES, H.NM_JABATAN AS NM_JABATAN_PEMROSES 
		FROM MD_SURAT_MASUK A, MD_SIFAT_SURAT B, MD_KEAMANAN_SURAT C, D_DISTRIBUSI_SURAT D, MD_TU_BAGIAN E, MD_STATUS_SM F, D_PEGAWAI G, MD_JABATAN H WHERE D.ID_DS_SEBELUMNYA='".$id_distribusi_surat."' AND (D.KD_STATUS_DISTRIBUSI ='D' OR D.KD_STATUS_DISTRIBUSI='TM') AND A.ID_SURAT_MASUK=D.ID_SURAT_MASUK AND B.KD_SIFAT_SURAT=A.KD_SIFAT_SURAT AND C.KD_KEAMANAN_SURAT=A.KD_KEAMANAN_SURAT AND E.KD_TU_BAGIAN=A.KD_TU_BAGIAN AND F.ID_STATUS_SM=A.ID_STATUS_SM AND G.KD_PEGAWAI=D.PEMROSES_SURAT AND H.KD_JABATAN=D.JABATAN_PEMROSES";
		return $this->tnde->query($sql)->row_array();
	}
	
	function get_pejabat_by_kd_jabatan($kd_jabatan){
		$sql = "SELECT A.KD_PEGAWAI, A.NIP, A.NM_PEGAWAI, B.KD_JABATAN, B.NM_JABATAN, A.NM_PEGAWAI FROM D_PEGAWAI A, MD_JABATAN B WHERE B.KD_JABATAN='".$kd_jabatan."' AND A.KD_JABATAN=B.KD_JABATAN";
		return $this->tnde->query($sql)->row_array();
	}
	
	function pegawai_by_kd_jabatan($kd_jabatan){
		$sql = "SELECT A.NIP, A.KD_PEGAWAI, A.KD_JABATAN, A.NM_PEGAWAI, B.NM_JABATAN FROM D_PEGAWAI A, MD_JABATAN B WHERE A.KD_JABATAN='".$kd_jabatan."' AND B.KD_JABATAN=A.KD_JABATAN";
		return $this->tnde->query($sql)->result();			
	}
	
	function insert_distribusi_surat($data){
		$this->tnde->insert('D_DISTRIBUSI_SURAT',$data);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function insert_distribusi_surat2($data){
		$sql = "INSERT INTO D_DISTRIBUSI_SURAT(ID_SURAT_MASUK, ID_GRUP_SURAT, ISI_RINGKAS, ISI_DISPOSISI, KD_STATUS_DISTRIBUSI, WAKTU_KIRIM, WAKTU_PENYELESAIAN, ID_STATUS_SM, PEMROSES_SURAT, JABATAN_PEMROSES, PENERIMA_SURAT, JABATAN_PENERIMA, ID_DS_SEBELUMNYA)
		VALUES('".$data['ID_SURAT_MASUK']."', '".$data['ID_GRUP_SURAT']."', '".$data['ISI_RINGKAS']."', '".$data['ISI_DISPOSISI']."', '".$data['KD_STATUS_DISTRIBUSI']."', TO_DATE('".$data['WAKTU_KIRIM']."', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('".$data['WAKTU_PENYELESAIAN']."', 'DD/MM/YYYY'), '".$data['ID_STATUS_SM']."', '".$data['PEMROSES_SURAT']."', '".$data['JABATAN_PEMROSES']."', '".$data['PENERIMA_SURAT']."', '".$data['JABATAN_PENERIMA']."', '".$data['ID_DS_SEBELUMNYA']."')";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function update_status_surat($id_surat_masuk, $data){
		$this->tnde->where('ID_SURAT_MASUK',$id_surat_masuk);
		$this->tnde->update('MD_SURAT_MASUK',$data);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function update_status_surat2($id_distribusi_surat, $data){
		$sql = "UPDATE D_DISTRIBUSI_SURAT SET ID_STATUS_SM='".$data['ID_STATUS_SM']."', WAKTU_AKSES = TO_DATE('".$data['WAKTU_AKSES']."','DD/MM/YYYY HH24:MI:SS''') WHERE ID_DISTRIBUSI_SURAT='".$id_distribusi_surat."' ";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function update_status_surat_masuk($id_surat_masuk, $data){
		$sql = "UPDATE MD_SURAT_MASUK SET ID_STATUS_SM='".$data['ID_STATUS_SM']."', WAKTU_AKSES = TO_DATE('".$data['WAKTU_AKSES']."','DD/MM/YYYY HH24:MI:SS''') WHERE ID_SURAT_MASUK='".$id_surat_masuk."' ";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}	
	}
	
	function update_status_distribusi_surat($id_distribusi_surat, $data){
		$this->tnde->where('ID_DISTRIBUSI_SURAT',$id_distribusi_surat);
		$this->tnde->update('D_DISTRIBUSI_SURAT',$data);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}	
	}
	
	function update_status_distribusi_surat2($id_distribusi_surat, $data){
		$sql = "UPDATE D_DISTRIBUSI_SURAT SET ID_STATUS_SM='".$data['ID_STATUS_SM']."', WAKTU_AKSES=TO_DATE('".$data['WAKTU_AKSES']."', 'DD/MM/YYYY HH24:MI:SS') WHERE ID_DISTRIBUSI_SURAT='".$id_distribusi_surat."' ";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}		
	}
	
	function total_tm_masuk_by_kd_pegawai($kd_pegawai){
		$data['PENERIMA_SURAT'] = $kd_pegawai;
		$data['KD_STATUS_DISTRIBUSI'] = 'TM';
		$this->tnde->where($data);
		$this->tnde->from('D_DISTRIBUSI_SURAT');
		return $this->tnde->count_all_results();
	}
	
	function total_tm_masuk_by_kd_jabatan($kd_jabatan){
		$data['JABATAN_PENERIMA'] = $kd_jabatan;
		$data['KD_STATUS_DISTRIBUSI'] = 'TM';
		$this->tnde->where($data);
		$this->tnde->from('D_DISTRIBUSI_SURAT');
		return $this->tnde->count_all_results();
	}
	
	function get_tm_masuk_by_kd_pegawai_limit($kd_pegawai, $limit){
		$sql = "SELECT * FROM ( SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, A.PEMROSES_SURAT, A.JABATAN_PEMROSES, A.KD_STATUS_DISTRIBUSI, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM,
		A.ID_STATUS_SM, B.PERIHAL, C.KD_PEGAWAI, C.NM_PEGAWAI, D.KD_JABATAN, D.NM_JABATAN, E.NM_STATUS_SM
		FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B, D_PEGAWAI C, MD_JABATAN D, MD_STATUS_SM E
		WHERE A.PENERIMA_SURAT='".$kd_pegawai."' AND A.KD_STATUS_DISTRIBUSI='TM' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND C.KD_PEGAWAI=A.PEMROSES_SURAT AND D.KD_JABATAN=A.JABATAN_PEMROSES
		 AND E.ID_STATUS_SM=A.ID_STATUS_SM order BY A.WAKTU_KIRIM DESC ) WHERE ROWNUM <= ".$limit."";
		 return $this->tnde->query($sql)->result();
	}
	
	function get_tm_masuk_by_kd_pegawai_paging($kd_pegawai, $limit, $page){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_DISTRIBUSI_SURAT, ID_SURAT_MASUK, PEMROSES_SURAT, JABATAN_PEMROSES, KD_STATUS_DISTRIBUSI, WAKTU_KIRIM, ID_STATUS_SM, PERIHAL, KD_PEGAWAI, NM_PEGAWAI, KD_JABATAN, NM_JABATAN, NM_STATUS_SM, ROWNUM R FROM ( SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, A.PEMROSES_SURAT, A.JABATAN_PEMROSES, A.KD_STATUS_DISTRIBUSI, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM,
		A.ID_STATUS_SM, B.PERIHAL, C.KD_PEGAWAI, C.NM_PEGAWAI, D.KD_JABATAN, D.NM_JABATAN, E.NM_STATUS_SM
		FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B, D_PEGAWAI C, MD_JABATAN D, MD_STATUS_SM E
		WHERE A.PENERIMA_SURAT='".$kd_pegawai."' AND A.KD_STATUS_DISTRIBUSI='TM' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND C.KD_PEGAWAI=A.PEMROSES_SURAT AND D.KD_JABATAN=A.JABATAN_PEMROSES
		 AND E.ID_STATUS_SM=A.ID_STATUS_SM order BY A.WAKTU_KIRIM DESC ) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function get_tm_masuk_by_kd_jabatan_limit($kd_jabatan, $limit){
		$sql = "SELECT * FROM ( SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, A.PEMROSES_SURAT, A.JABATAN_PEMROSES, A.KD_STATUS_DISTRIBUSI, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM,
		A.ID_STATUS_SM, B.PERIHAL, C.KD_PEGAWAI, C.NM_PEGAWAI, D.KD_JABATAN, D.NM_JABATAN, E.NM_STATUS_SM
		FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B, D_PEGAWAI C, MD_JABATAN D, MD_STATUS_SM E
		WHERE A.JABATAN_PENERIMA='".$kd_jabatan."' AND A.KD_STATUS_DISTRIBUSI='TM' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND C.KD_PEGAWAI=A.PEMROSES_SURAT AND D.KD_JABATAN=A.JABATAN_PEMROSES
		 AND E.ID_STATUS_SM=A.ID_STATUS_SM order BY A.WAKTU_KIRIM DESC ) WHERE ROWNUM <= ".$limit."";
		 return $this->tnde->query($sql)->result();
	}
	
	function get_tm_masuk_by_kd_jabatan_paging($kd_jabatan, $limit, $page){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_DISTRIBUSI_SURAT, ID_SURAT_MASUK, PEMROSES_SURAT, JABATAN_PEMROSES, KD_STATUS_DISTRIBUSI, WAKTU_KIRIM, ID_STATUS_SM, PERIHAL, KD_PEGAWAI, NM_PEGAWAI, KD_JABATAN, NM_JABATAN, NM_STATUS_SM, ROWNUM R FROM ( SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, A.PEMROSES_SURAT, A.JABATAN_PEMROSES, A.KD_STATUS_DISTRIBUSI, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM,
		A.ID_STATUS_SM, B.PERIHAL, C.KD_PEGAWAI, C.NM_PEGAWAI, D.KD_JABATAN, D.NM_JABATAN, E.NM_STATUS_SM
		FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B, D_PEGAWAI C, MD_JABATAN D, MD_STATUS_SM E
		WHERE A.JABATAN_PENERIMA='".$kd_jabatan."' AND A.KD_STATUS_DISTRIBUSI='TM' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND C.KD_PEGAWAI=A.PEMROSES_SURAT AND D.KD_JABATAN=A.JABATAN_PEMROSES
		 AND E.ID_STATUS_SM=A.ID_STATUS_SM order BY A.WAKTU_KIRIM DESC ) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function total_tm_keluar_grup_by_kd_pegawai($kd_pegawai){
		$sql = "SELECT COUNT(DISTINCT(ID_SURAT_MASUK)) AS TOTAL FROM D_DISTRIBUSI_SURAT WHERE KD_STATUS_DISTRIBUSI='TM' AND PENERIMA_SURAT='".$kd_pegawai."'";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_tm_keluar_grup_by_kd_jabatan($kd_jabatan){
		$sql = "SELECT COUNT(DISTINCT(ID_SURAT_MASUK)) AS TOTAL FROM D_DISTRIBUSI_SURAT WHERE KD_STATUS_DISTRIBUSI='TM' AND JABATAN_PENERIMA='".$kd_jabatan."'";
		return $this->tnde->query($sql)->row_array();
	}
	
	function get_tm_keluar_grup_by_kd_pegawai_limit($kd_pegawai, $limit){
		$sql = "SELECT ID_DISTRIBUSI_SURAT, ID_SURAT_MASUK, WAKTU_KIRIM, WAKTU_PENYELESAIAN, NO_SURAT, DARI, PERIHAL, ROWNUM FROM ( WITH TEMP AS( SELECT * FROM 
		(SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') 
		AS WAKTU_PENYELESAIAN, B.NO_SURAT, B.DARI, B.PERIHAL, ROW_NUMBER() OVER(PARTITION BY A.ID_SURAT_MASUK ORDER BY A.ID_DISTRIBUSI_SURAT DESC) RN 
		FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B WHERE A.PEMROSES_SURAT='".$kd_pegawai."' AND A.KD_STATUS_DISTRIBUSI='TM' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK 
		ORDER BY A.WAKTU_KIRIM DESC))SELECT * FROM TEMP WHERE RN=1 ) WHERE ROWNUM <= ".$limit."";
		 return $this->tnde->query($sql)->result();
	}
	
	function get_tm_keluar_grup_by_kd_pegawai_paging($kd_pegawai, $limit, $page){
		$sql = "SELECT * FROM ( SELECT ID_DISTRIBUSI_SURAT, ID_SURAT_MASUK, WAKTU_KIRIM, WAKTU_PENYELESAIAN, NO_SURAT, DARI, PERIHAL, ROWNUM R FROM ( WITH TEMP AS( SELECT * FROM (SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, B.NO_SURAT, B.DARI, B.PERIHAL, ROW_NUMBER() OVER(PARTITION BY A.ID_SURAT_MASUK ORDER BY A.ID_DISTRIBUSI_SURAT DESC) RN FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B WHERE A.PEMROSES_SURAT='".$kd_pegawai."' AND A.KD_STATUS_DISTRIBUSI='TM' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK ORDER BY A.WAKTU_KIRIM DESC))SELECT * FROM TEMP WHERE RN=1 ) WHERE ROWNUM <= ".$limit.") WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_tm_keluar_grup_by_kd_jabatan_limit($kd_jabatan, $limit){
		$sql = "SELECT ID_DISTRIBUSI_SURAT, ID_SURAT_MASUK, WAKTU_KIRIM, WAKTU_PENYELESAIAN, NO_SURAT, DARI, PERIHAL, ROWNUM FROM ( WITH TEMP AS( SELECT * FROM 
		(SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') 
		AS WAKTU_PENYELESAIAN, B.NO_SURAT, B.DARI, B.PERIHAL, ROW_NUMBER() OVER(PARTITION BY A.ID_SURAT_MASUK ORDER BY A.ID_DISTRIBUSI_SURAT DESC) RN 
		FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B WHERE A.JABATAN_PEMROSES='".$kd_jabatan."' AND A.KD_STATUS_DISTRIBUSI='TM' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK 
		ORDER BY A.WAKTU_KIRIM DESC))SELECT * FROM TEMP WHERE RN=1 ) WHERE ROWNUM <= ".$limit."";
		 return $this->tnde->query($sql)->result();
	}
	
	function get_tm_keluar_grup_by_kd_jabatan_paging($kd_jabatan, $limit, $page){
		$sql = "SELECT * FROM ( SELECT ID_DISTRIBUSI_SURAT, ID_SURAT_MASUK, WAKTU_KIRIM, WAKTU_PENYELESAIAN, NO_SURAT, DARI, PERIHAL, ROWNUM R FROM ( WITH TEMP AS( SELECT * FROM (SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, B.NO_SURAT, B.DARI, B.PERIHAL, ROW_NUMBER() OVER(PARTITION BY A.ID_SURAT_MASUK ORDER BY A.ID_DISTRIBUSI_SURAT DESC) RN FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B WHERE A.JABATAN_PEMROSES='".$kd_jabatan."' AND A.KD_STATUS_DISTRIBUSI='TM' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK ORDER BY A.WAKTU_KIRIM DESC))SELECT * FROM TEMP WHERE RN=1 ) WHERE ROWNUM <= ".$limit.") WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();
	}
	
	function detail_dk_by_id_surat_masuk($id_surat_masuk, $jabatan_pemroses, $kd_status_distribusi){
		$sql = "SELECT A.ID_SURAT_MASUK, A.INDEKS_BERKAS, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.KD_SIFAT_SURAT, A.KODE, A.KD_KEAMANAN_SURAT, A.PERIHAL, A.DARI, A.KEPADA, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.NO_SURAT, A.CATATAN, A.KD_TU_BAGIAN, A.LAMPIRAN, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, B.NM_SIFAT_SURAT, C.NM_KEAMANAN_SURAT, D.ID_DISTRIBUSI_SURAT, D.ID_GRUP_SURAT, TO_CHAR(D.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, D.ISI_RINGKAS, D.ISI_DISPOSISI, D.PEMROSES_SURAT, D.JABATAN_PEMROSES, D.PENERIMA_SURAT, D.JABATAN_PENERIMA, TO_CHAR(D.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, E.NM_TU_BAGIAN 
		FROM MD_SURAT_MASUK A, MD_SIFAT_SURAT B, MD_KEAMANAN_SURAT C, D_DISTRIBUSI_SURAT D, MD_TU_BAGIAN E 
		WHERE D.ID_SURAT_MASUK='".$id_surat_masuk."' AND D.JABATAN_PEMROSES='".$jabatan_pemroses."' AND D.KD_STATUS_DISTRIBUSI='".$kd_status_distribusi."' AND A.ID_SURAT_MASUK=D.ID_SURAT_MASUK AND B.KD_SIFAT_SURAT=A.KD_SIFAT_SURAT AND C.KD_KEAMANAN_SURAT=A.KD_KEAMANAN_SURAT AND E.KD_TU_BAGIAN=A.KD_TU_BAGIAN";
		return $this->tnde->query($sql)->row_array();
	}
	
	function detail_dk_by_id_surat_masuk2($id_surat_masuk, $pemroses_surat, $kd_status_distribusi){
		$sql = "SELECT A.ID_SURAT_MASUK, A.INDEKS_BERKAS, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.KD_SIFAT_SURAT, A.KODE, A.KD_KEAMANAN_SURAT, A.PERIHAL, A.DARI, A.KEPADA, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.NO_SURAT, A.CATATAN, A.KD_TU_BAGIAN, A.LAMPIRAN, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, B.NM_SIFAT_SURAT, C.NM_KEAMANAN_SURAT, D.ID_DISTRIBUSI_SURAT, D.ID_GRUP_SURAT, TO_CHAR(D.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, D.ISI_RINGKAS, D.ISI_DISPOSISI, D.PEMROSES_SURAT, D.JABATAN_PEMROSES, D.PENERIMA_SURAT, D.JABATAN_PENERIMA, TO_CHAR(D.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, E.NM_TU_BAGIAN 
		FROM MD_SURAT_MASUK A, MD_SIFAT_SURAT B, MD_KEAMANAN_SURAT C, D_DISTRIBUSI_SURAT D, MD_TU_BAGIAN E 
		WHERE D.ID_SURAT_MASUK='".$id_surat_masuk."' AND D.PEMROSES_SURAT='".$pemroses_surat."' AND D.KD_STATUS_DISTRIBUSI='".$kd_status_distribusi."' AND A.ID_SURAT_MASUK=D.ID_SURAT_MASUK AND B.KD_SIFAT_SURAT=A.KD_SIFAT_SURAT AND C.KD_KEAMANAN_SURAT=A.KD_KEAMANAN_SURAT AND E.KD_TU_BAGIAN=A.KD_TU_BAGIAN";
		return $this->tnde->query($sql)->row_array();
	}
	
	function get_unit_jab_surat_masuk(){
		$sql = "SELECT DISTINCT(UNIT_ID) FROM D_JAB_SM";
		return $this->tnde->query($sql)->result();
	}
}
?>