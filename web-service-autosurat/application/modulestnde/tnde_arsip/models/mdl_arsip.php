<?php
if(! defined('BASEPATH'))exit('No direct script access allowed');
class Mdl_arsip extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->tnde = $this->load->database('tnde',TRUE);
	}
	
	function insert_meta_distribusi_surat($data){
		$isi_ringkas					= (!empty($data['ISI_RINGKAS']) ? $data['ISI_RINGKAS'] : '');
		$isi_pesan					= (!empty($data['ISI_PESAN']) ? $data['ISI_PESAN'] : '');
		$waktu_penyelesaian	= (!empty($data['WAKTU_PENYELESAIAN']) ? $data['WAKTU_PENYELESAIAN'] : '');
		$induk							= (!empty($data['INDUK']) ? $data['INDUK'] : '');
		$sql = "INSERT INTO META_DISTRIBUSI(ID_SURAT_MASUK, PENGIRIM, JAB_PENGIRIM, KD_GRUP_PENGIRIM, ISI_RINGKAS, ISI_PESAN, WAKTU_KIRIM, WAKTU_PENYELESAIAN, INDUK)VALUES('".$data['ID_SURAT_MASUK']."', '".$data['PENGIRIM']."', '".$data['JAB_PENGIRIM']."', '".$data['KD_GRUP_PENGIRIM']."', '".$isi_ringkas."', '".$isi_pesan."', TO_DATE('".date('d/m/Y H:i:s')."', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('".$waktu_penyelesaian."', 'DD/MM/YYYY HH24:MI:SS'), '".$induk."')";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			$sql_id	= "SELECT META_DISTRIBUSI_SEQ.CURRVAL AS ID FROM dual";
			return $this->tnde->query($sql_id)->result();
		}else{
			return '';
		}
	}
	
	function get_penerima_surat($data=''){
		$id_sm	= (!empty($data['ID_SURAT_MASUK']) ? $data['ID_SURAT_MASUK'] : '');
		if(!empty($data['KD_STATUS_DISTRIBUSI'])){
			if(is_array($data['KD_STATUS_DISTRIBUSI'])){
				$list = implode_to_string($data['KD_STATUS_DISTRIBUSI']);
				$kd_status_distribusi = " AND B.KD_STATUS_DISTRIBUSI IN(".$list.")";
			}else{
				$kd_status_distribusi = " AND B.KD_STATUS_DISTRIBUSI IN('".$data['KD_STATUS_DISTRIBUSI']."')";
			}
		}else{
			$kd_status_distribusi = '';
		}
		$sql = "SELECT A.ID_META_DISTRIBUSI, A.ID_SURAT_MASUK, A.PENGIRIM, A.JAB_PENGIRIM, A.KD_GRUP_PENGIRIM, A.ISI_RINGKAS, A.ISI_PESAN, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') WAKTU_KIRIM, TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_PENYELESAIAN, A.INDUK, B.ID_DISTRIBUSI_SURAT, B.ID_GRUP_SURAT, B.PENERIMA_SURAT, B.JABATAN_PENERIMA, B.KD_STATUS_DISTRIBUSI, B.ID_STATUS_SM, TO_CHAR(B.WAKTU_AKSES, 'DD/MM/YYYY HH24:MI:SS') WAKTU_AKSES, B.KD_GRUP_TUJUAN, C.NM_STATUS_SM FROM META_DISTRIBUSI A, D_DISTRIBUSI_SURAT B, MD_STATUS_SM C WHERE A.ID_SURAT_MASUK='".$id_sm."' AND B.ID_META_DISTRIBUSI=A.ID_META_DISTRIBUSI AND C.ID_STATUS_SM=B.ID_STATUS_SM ".$kd_status_distribusi." ORDER BY A.WAKTU_KIRIM ASC";
		return $this->tnde->query($sql)->result();
	}
}
?>