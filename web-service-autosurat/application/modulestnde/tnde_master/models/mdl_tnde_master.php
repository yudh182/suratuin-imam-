<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');
class Mdl_tnde_master extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->tnde = $this->load->database('tnde', TRUE);
	}
	
	function get_psd_limit($limit=''){
		$sql = "SELECT * FROM(SELECT * FROM MD_PSD ORDER BY KD_PSD ASC) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}	
	
	function get_psd_paging($limit='', $page=''){
		$sql = "SELECT * FROM (
														SELECT ID_PSD, KD_PSD, NM_PSD, KD_JABATAN, ROWNUM R 
														FROM( SELECT * 
																		FROM MD_PSD ORDER BY KD_PSD ASC
														) 
														WHERE ROWNUM <= ".$limit."
										) 
										WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_psd_cari_limit($q='', $limit=''){
		$sql = "SELECT * FROM(SELECT A.ID_PSD, A.KD_PSD, A.NM_PSD, B.* FROM MD_PSD A, MD_JABATAN B 
													WHERE (UPPER(A.KD_PSD) LIKE '%".$q."%' OR UPPER(A.NM_PSD) LIKE '%".$q."%') AND B.KD_JABATAN=A.KD_JABATAN ORDER BY KD_PSD ASC) 
										WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_psd_simple_limit($q='', $jab_list='', $limit=''){
		$sql = "SELECT * FROM (SELECT * FROM MD_PSD WHERE UPPER(KD_PSD) LIKE UPPER('%".$q."%') OR UPPER(NM_PSD) LIKE UPPER('%".$q."%') OR 
						KD_JABATAN IN(".$jab_list.") ORDER BY NO_URUT ASC) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_psd_simple_paging($q='', $jab_list='', $limit='', $page=''){
		$sql = "SELECT * FROM (SELECT ID_PSD, KD_PSD, NM_PSD, KD_JABATAN, NO_URUT, ROWNUM R FROM (SELECT * FROM MD_PSD 
						WHERE UPPER(KD_PSD) LIKE UPPER('%".$q."%') OR UPPER(NM_PSD) LIKE UPPER('%".$q."%') OR 
						KD_JABATAN IN(".$jab_list.") ORDER BY NO_URUT ASC) WHERE ROWNUM <= ".$limit.") WHERE R > ".$page."";
		return  $this->tnde->query($sql)->result();
	}
	
	function get_psd_cari_paging($q='', $limit='', $page=''){
		$sql = "SELECT * FROM (
														SELECT ID_PSD, KD_PSD, NM_PSD, KD_JABATAN, NM_JABATAN, MEMBAWAHI, ROWNUM R 
														FROM( SELECT A.ID_PSD, A.KD_PSD, A.NM_PSD, B.* 
																		FROM MD_PSD A, MD_JABATAN B 
																		WHERE (UPPER(A.KD_PSD) LIKE '%".$q."%' OR UPPER(A.NM_PSD) LIKE '%".$q."%') AND B.KD_JABATAN=A.KD_JABATAN 
																						ORDER BY KD_PSD ASC
														) 
														WHERE ROWNUM <= ".$limit."
										) 
										WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();
	}
	
	function detail_psd_by_id_psd($id_psd=''){
		$sql = "SELECT * FROM MD_PSD WHERE ID_PSD='".$id_psd."'";
		return $this->tnde->query($sql)->row_array();
	}
	
	function cek_kd_psd($id_psd='', $kd_psd=''){
		$sql = "SELECT * FROM MD_PSD WHERE KD_PSD='".$kd_psd."' AND ID_PSD !='".$id_psd."' ";
		return $this->tnde->query($sql)->result();
	}	
	
	function cek_kd_jabatan_psd($id_psd='', $kd_jabatan=''){
		$sql = "SELECT * FROM MD_PSD WHERE KD_JABATAN='".$kd_jabatan."' AND ID_PSD !='".$id_psd."' ";
		return $this->tnde->query($sql)->result();
	}
	
	function total_psd_cari($q=''){
		$sql = "SELECT COUNT(*) TOTAL FROM MD_PSD WHERE UPPER(KD_PSD) LIKE '%".$q."%' OR UPPER(NM_PSD) LIKE '%".$q."%' ";
		return $this->tnde->query($sql)->row_array();
	}

	function total_psd_simple_cari($q='', $jab_list=''){
		$sql = "SELECT COUNT(*) TOTAL FROM MD_PSD WHERE UPPER(KD_PSD) LIKE '%".$q."%' OR UPPER(NM_PSD) LIKE '%".$q."%' OR KD_JABATAN IN(".$jab_list.")";
		return $this->tnde->query($sql)->row_array();
	}	
	
	function total_klasifikasi_surat_cari($q=''){
		$q = strtoupper($q);
		$sql = "SELECT COUNT(*) TOTAL FROM MD_KLASIFIKASI_SURAT WHERE UPPER(KD_KLASIFIKASI_SURAT) LIKE '%".$q."%' OR UPPER(NM_KLASIFIKASI_SURAT) LIKE '%".$q."%' ";
		return $this->tnde->query($sql)->row_array();
	}
	
	function get_klasifikasi_surat_limit($limit='', $order='ASC'){
		$sql = "SELECT ID_KLASIFIKASI_SURAT, KD_KLASIFIKASI_SURAT, NM_KLASIFIKASI_SURAT, KETERANGAN, AKTIF, ROWNUM R 
					FROM ( SELECT * FROM MD_KLASIFIKASI_SURAT ORDER BY KD_KLASIFIKASI_SURAT ".$order." ) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}

	function get_klasifikasi_surat_paging($limit='', $page='', $order='ASC'){
		$sql = "SELECT * FROM (SELECT ID_KLASIFIKASI_SURAT, KD_KLASIFIKASI_SURAT, NM_KLASIFIKASI_SURAT, KETERANGAN, AKTIF, ROWNUM R 
												FROM ( SELECT * FROM MD_KLASIFIKASI_SURAT ORDER BY KD_KLASIFIKASI_SURAT ".$order." ) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_klasifikasi_surat_cari_limit($q='', $limit='', $order='ASC'){
		$q = strtoupper($q);
		$sql = "SELECT ID_KLASIFIKASI_SURAT, KD_KLASIFIKASI_SURAT, NM_KLASIFIKASI_SURAT, KETERANGAN, AKTIF, ROWNUM R 
					FROM ( SELECT * FROM MD_KLASIFIKASI_SURAT WHERE UPPER(KD_KLASIFIKASI_SURAT) LIKE '%".$q."%' OR UPPER(NM_KLASIFIKASI_SURAT) LIKE '%".$q."%' 
					ORDER BY KD_KLASIFIKASI_SURAT ".$order." ) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_klasifikasi_surat_cari_paging($q='', $limit='', $page='', $order='ASC'){
		$q = strtoupper($q);
		$sql = "SELECT * FROM (SELECT ID_KLASIFIKASI_SURAT, KD_KLASIFIKASI_SURAT, NM_KLASIFIKASI_SURAT, KETERANGAN, AKTIF, ROWNUM R 
												FROM ( SELECT * FROM MD_KLASIFIKASI_SURAT 
																			WHERE UPPER(KD_KLASIFIKASI_SURAT) LIKE '%".$q."%' OR UPPER(NM_KLASIFIKASI_SURAT) LIKE '%".$q."%' 
																			ORDER BY KD_KLASIFIKASI_SURAT ".$order." 
															) 
												WHERE ROWNUM <= ".$limit." 
											 ) WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();
	}
	
	function cek_kd_klasifikasi_surat($id_klasifikas_surat='', $kd_klasifikasi_surat=''){
		$sql = "SELECT * FROM MD_KLASIFIKASI_SURAT WHERE KD_KLASIFIKASI_SURAT='".$kd_klasifikasi_surat."' AND ID_KLASIFIKASI_SURAT !='".$id_klasifikas_surat."' ";
		return $this->tnde->query($sql)->result();
	}
	
	function get_psd_by_kd_tu_bagian($kd_tu_bagian=''){
		$sql = "SELECT A.* FROM MD_PSD A, MD_TU_BAGIAN B WHERE B.KD_TU_BAGIAN='".$kd_tu_bagian."' AND A.KD_TU_PENOMORAN=B.KD_TU_PENOMORAN";
		return $this->tnde->query($sql)->result();
	}
	
	function jenis_surat_by_field_in($kd_jenis_surat='', $order_by='NM_JENIS_SURAT', $order_val='ASC'){
		$sql = "SELECT A.*, B.* FROM MD_JENIS_SURAT A, MD_KAT_PENOMORAN B WHERE A.KD_JENIS_SURAT IN(".$kd_jenis_surat.") AND B.KD_KAT_PENOMORAN=A.KD_KAT_PENOMORAN ORDER BY A.".$order_by." ".$order_val."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_klasifikasi_surat_like($q=''){
		$sql = "SELECT * FROM MD_KLASIFIKASI_SURAT WHERE AKTIF='1' AND (UPPER(KD_KLASIFIKASI_SURAT) LIKE UPPER('%".$q."%') OR UPPER(NM_KLASIFIKASI_SURAT) LIKE UPPER('%".$q."%'))";
		return $this->tnde->query($sql)->result();
	}
	
	function surat_sekategori($kd_jenis_surat=''){
		$sql = "SELECT KD_JENIS_SURAT FROM MD_JENIS_SURAT WHERE KD_KAT_PENOMORAN=(SELECT KD_KAT_PENOMORAN FROM MD_JENIS_SURAT WHERE KD_JENIS_SURAT='".$kd_jenis_surat."')";
		return $this->tnde->query($sql)->result();
	}
	
	function psd_sk($jab_list){
		$sql = "SELECT * FROM MD_PSD WHERE KD_JABATAN IN(".$jab_list.") AND SURAT_KEPUTUSAN='1'";
		return $this->tnde->query($sql)->result();
	}
	
	function get_psd_in_kd_jabatan($jabatan_arr){
		$sql = "SELECT * FROM MD_PSD WHERE KD_JABATAN IN(".$jabatan_arr.") ORDER BY NO_URUT ASC";
		return $this->tnde->query($sql)->result();
	}
	
	function insert_procedure_config_hk($data){
		$hasil = $this->tnde->call_procedure_outside('TNDE','INSERT_CONFIG_HARI_KERJA',$data,'COMPLEX');
		return $hasil;
	}	
	
	function insert_procedur_hari_kerja($data){
		$hasil = $this->tnde->call_procedure_outside('TNDE','INSERT_D_HARI_KERJA',$data,'COMPLEX');
		return $hasil;
	}
	
	function get_config_hari_kerja(){
		$sql = "SELECT ID_CONFIG_HK, TO_CHAR(PERIODE_AWAL, 'DD/MM/YYYY') PERIODE_AWAL, TO_CHAR(PERIODE_SELESAI, 'DD/MM/YYYY') PERIODE_SELESAI, DIBUAT, TO_CHAR(TGL_DIBUAT, 'DD/MM/YYYY HH:MI:SS') TGL_DIBUAT, DIUPDATE, TO_CHAR(TGL_DIUPDATE, 'DD/MM/YYYY HH:MI:SS') TGL_DIUPDATE 
		FROM CONFIG_HARI_KERJA ORDER BY TGL_DIBUAT DESC";
		return $this->tnde->query($sql)->result();
	}
	
	function hari_kerja_by_id_config_hk($id_config_hk=''){
		$sql = "SELECT A.*, B.* FROM D_HARI_KERJA A, MD_HARI B WHERE A.ID_CONFIG_HK='".$id_config_hk."' AND B.KD_HARI=A.KD_HARI ";
		return $this->tnde->query($sql)->result();
	}
	
	// function get_hari_kerja_by_tgl($tgl=''){
		// $sql = "SELECT A.*, B.*, C.* FROM CONFIG_HARI_KERJA A, D_HARI_KERJA B, MD_HARI C
					// WHERE ((A.PERIODE_AWAL <= TO_DATE('".$tgl."', 'DD/MM/YYYY') AND A.PERIODE_SELESAI >= TO_DATE('".$tgl."', 'DD/MM/YYYY')) OR (A.PERIODE_AWAL <= TO_DATE('".$tgl."', 'DD/MM/YYYY') AND A.PERIODE_SELESAI IS NULL)) AND B.ID_CONFIG_HK=A.ID_CONFIG_HK AND C.KD_HARI=B.KD_HARI ORDER BY TGL_DIBUAT DESC";
		// return $this->tnde->query($sql)->result();
	// }
	
	function get_hari_kerja_by_tgl($tgl=''){
		$sql = "SELECT B.* FROM D_HARI_KERJA A, MD_HARI B 
                    WHERE A.ID_CONFIG_HK IN( SELECT ID_CONFIG_HK FROM (
                                                                        SELECT ID_CONFIG_HK FROM CONFIG_HARI_KERJA WHERE         
                                                                            ((PERIODE_AWAL <= TO_DATE('".$tgl."', 'DD/MM/YYYY') AND PERIODE_SELESAI >= TO_DATE('".$tgl."', 'DD/MM/YYYY')) OR (PERIODE_AWAL <= TO_DATE('".$tgl."', 'DD/MM/YYYY') AND PERIODE_SELESAI IS NULL)) ORDER BY TGL_DIBUAT DESC 
                                                                        ) WHERE ROWNUM=1
                                                                    ) AND B.KD_HARI=A.KD_HARI";
		return $this->tnde->query($sql)->result();
	}
	
	function detail_config_hari_kerja_by_id_config_hk($id=''){
		$sql = "SELECT ID_CONFIG_HK, TO_CHAR(PERIODE_AWAL, 'DD/MM/YYYY') PERIODE_AWAL, TO_CHAR(PERIODE_SELESAI, 'DD/MM/YYYY') PERIODE_SELESAI, DIBUAT, TO_CHAR(TGL_DIBUAT, 'DD/MM/YYYY HH:MI:SS') TGL_DIBUAT, DIUPDATE, TO_CHAR(TGL_DIUPDATE, 'DD/MM/YYYY HH:MI:SS') TGL_DIUPDATE FROM CONFIG_HARI_KERJA WHERE ID_CONFIG_HK='".$id."' ";
		return $this->tnde->query($sql)->row_array();
	}
	
	function update_config_hari_kerja($data, $id_config_hk=''){
		$sql = "UPDATE CONFIG_HARI_KERJA SET PERIODE_AWAL=TO_DATE('".$data['PERIODE_AWAL']."', 'DD/MM/YYYY'), PERIODE_SELESAI=TO_DATE('".$data['PERIODE_SELESAI']."', 'DD/MM/YYYY'), DIUPDATE='".$data['DIUPDATE']."', TGL_DIUPDATE=TO_DATE('".date('d/m/Y H:i:s')."', 'DD/MM/YYYY HH24:MI:SS') WHERE ID_CONFIG_HK='".$id_config_hk."' ";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function kd_terakhir_jab_arsip_surat(){
		$sql = "SELECT * FROM (SELECT * FROM D_JAB_SM WHERE STATUS='ARSIP' ORDER BY KD_JAB_SISTEM DESC) WHERE ROWNUM = 1";
		return $this->tnde->query($sql)->result();
	}	
	
	function kd_terakhir_jab_pengarah_surat(){
		$sql = "SELECT * FROM (SELECT * FROM D_JAB_SM WHERE STATUS='PENGARAH' ORDER BY KD_JAB_SISTEM DESC) WHERE ROWNUM =1";
		return $this->tnde->query($sql)->result();
	}
	
	function get_unit_jab_surat_masuk(){
		$sql = "SELECT DISTINCT(UNIT_ID) FROM D_JAB_SM";
		return $this->tnde->query($sql)->result();
	}	
	
	function get_unit_jab_surat_keluar(){
		$sql = "SELECT DISTINCT(UNIT_ID) FROM D_JAB_SK";
		return $this->tnde->query($sql)->result();
	}
	
	function kd_terakhir_jab_surat_keluar(){
		$sql = "SELECT * FROM (SELECT * FROM D_JAB_SK ORDER BY KD_JAB_SISTEM DESC) WHERE ROWNUM = 1";
		return $this->tnde->query($sql)->result();
	}
	
	function hak_akses_sk_by_unit($unit_id=''){
		$sql = "SELECT A.*, B.NM_JENIS_SURAT, C.NM_KELOMPOK_UNIT FROM MD_JENIS_SURAT B, D_JAB_SK A LEFT JOIN KELOMPOK_UNIT C ON C.GMU_SLUG=A.GMU_SLUG WHERE B.KD_JENIS_SURAT=A.KD_JENIS_SURAT AND A.UNIT_ID='".$unit_id."' ";
		return $this->tnde->query($sql)->result();
	}
	
	function his_hak_akses_by_kd_jab_sistem($kd_jab_sistem=''){
		$sql = "SELECT A.ID_HIS_JAB_SK, A.KD_JAB_SISTEM, A.GMU_SLUG, TO_CHAR(A.BERLAKU_DARI, 'DD/MM/YYYY') BERLAKU_DARI, TO_CHAR(A.BERLAKU_SAMPAI, 'DD/MM/YYYY') BERLAKU_SAMPAI, TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_SIMPAN, A.KD_PGW, A.STATUS_PENOMORAN, A.NO_REGISTER, B.KD_JENIS_SURAT, B.NM_JENIS_SURAT, C.NM_KELOMPOK_UNIT, D.UNIT_ID FROM MD_JENIS_SURAT B, D_JAB_SK D, HIS_JAB_SK A LEFT JOIN KELOMPOK_UNIT C ON C.GMU_SLUG=A.GMU_SLUG WHERE A.KD_JAB_SISTEM='".$kd_jab_sistem."' AND D.KD_JAB_SISTEM=A.KD_JAB_SISTEM AND B.KD_JENIS_SURAT=D.KD_JENIS_SURAT ORDER BY A.BERLAKU_DARI DESC";
		return $this->tnde->query($sql)->result();
	}
	
	function insert_his_jab_sk($data=''){
		$sql = "INSERT INTO HIS_JAB_SK(KD_JAB_SISTEM, GMU_SLUG, BERLAKU_DARI, BERLAKU_SAMPAI, WAKTU_SIMPAN, KD_PGW, STATUS_PENOMORAN, NO_REGISTER)VALUES('".$data['KD_JAB_SISTEM']."', '".$data['GMU_SLUG']."', TO_DATE('".$data['BERLAKU_DARI']."', 'DD/MM/YYYY'), TO_DATE('".$data['BERLAKU_SAMPAI']."', 'DD/MM/YYYY'), TO_DATE('".date('d/m/Y H:i:s')."', 'DD/MM/YYYY HH24:MI:SS'), '".$data['KD_PGW']."', '".$data['STATUS_PENOMORAN']."', '".$data['NO_REGISTER']."')";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function his_hak_akses_by_id_his_jab_sk($id_his_jab_sk=''){
		$sql = "SELECT A.ID_HIS_JAB_SK, A.KD_JAB_SISTEM, A.GMU_SLUG, TO_CHAR(A.BERLAKU_DARI, 'DD/MM/YYYY') BERLAKU_DARI, TO_CHAR(A.BERLAKU_SAMPAI, 'DD/MM/YYYY') BERLAKU_SAMPAI, TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_SIMPAN, A.KD_PGW, A.STATUS_PENOMORAN, A.NO_REGISTER, B.KD_JENIS_SURAT, B.NM_JENIS_SURAT, C.NM_KELOMPOK_UNIT, D.UNIT_ID 
		FROM MD_JENIS_SURAT B, D_JAB_SK D, HIS_JAB_SK A LEFT JOIN KELOMPOK_UNIT C ON C.GMU_SLUG=A.GMU_SLUG WHERE A.ID_HIS_JAB_SK='".$id_his_jab_sk."' AND D.KD_JAB_SISTEM=A.KD_JAB_SISTEM AND B.KD_JENIS_SURAT=D.KD_JENIS_SURAT";
		return $this->tnde->query($sql)->row_array();
	}
	
	function udpate_his_jab_sk($data='', $id_his_jab_sk=''){
		$sql = "UPDATE HIS_JAB_SK SET GMU_SLUG='".$data['GMU_SLUG']."', BERLAKU_DARI=TO_DATE('".$data['BERLAKU_DARI']."', 'DD/MM/YYYY'), BERLAKU_SAMPAI=TO_DATE('".$data['BERLAKU_SAMPAI']."', 'DD/MM/YYYY'), STATUS_PENOMORAN='".$data['STATUS_PENOMORAN']."', NO_REGISTER='".$data['NO_REGISTER']."' WHERE ID_HIS_JAB_SK='".$id_his_jab_sk."' ";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function hak_akses_sk_by_unit_tgl($unit_id='', $tgl=''){
		$tgl = (!empty($tgl) ? $tgl : date('d/m/Y'));
		$sql = "SELECT A.KD_JAB_SISTEM, A.UNIT_ID, A.KD_JENIS_SURAT, B.ID_HIS_JAB_SK, B.GMU_SLUG, B.STATUS_PENOMORAN, B.NO_REGISTER, C.NM_KELOMPOK_UNIT, D.NM_JENIS_SURAT FROM MD_JENIS_SURAT D, D_JAB_SK A LEFT JOIN HIS_JAB_SK B ON B.KD_JAB_SISTEM=A.KD_JAB_SISTEM AND ((B.BERLAKU_DARI <= TO_DATE('".$tgl."', 'DD/MM/YYYY') AND B.BERLAKU_SAMPAI IS NULL) OR (B.BERLAKU_DARI <= TO_DATE('".$tgl."', 'DD/MM/YYYY') AND B.BERLAKU_SAMPAI >= TO_DATE('".$tgl."', 'DD/MM/YYYY'))) LEFT JOIN KELOMPOK_UNIT C ON C.GMU_SLUG=B.GMU_SLUG WHERE A.UNIT_ID='".$unit_id."' AND D.KD_JENIS_SURAT=A.KD_JENIS_SURAT";
		return $this->tnde->query($sql)->result();
	}
	
	function semua_jenis_surat_by_tgl($tgl=''){
		$tgl = (!empty($tgl) ? $tgl : date('d/m/Y'));
		$sql = "SELECT A.KD_JENIS_SURAT, A.NM_JENIS_SURAT, A.KETERANGAN, B.KD_KAT_PENOMORAN, C.NM_KAT_PENOMORAN FROM MD_JENIS_SURAT A LEFT JOIN HIS_JENIS_SURAT B ON B.KD_JENIS_SURAT=A.KD_JENIS_SURAT AND ((B.BERLAKU_DARI <= TO_DATE('".$tgl."', 'DD/MM/YYYY') AND B.BERLAKU_SAMPAI IS NULL) OR (B.BERLAKU_DARI <= TO_DATE('".$tgl."', 'DD/MM/YYYY') AND B.BERLAKU_SAMPAI >= TO_DATE('".$tgl."', 'DD/MM/YYYY'))) LEFT JOIN MD_KAT_PENOMORAN C ON C.KD_KAT_PENOMORAN=B.KD_KAT_PENOMORAN";
		return $this->tnde->query($sql)->result();
	}
	
	function his_jenis_surat_by_kd_jenis_surat($kd_jenis_surat=''){
		$sql = "SELECT A.ID_HIS_JENIS_SURAT, A.KD_JENIS_SURAT, A.KD_KAT_PENOMORAN, TO_CHAR(A.BERLAKU_DARI, 'DD/MM/YYYY') BERLAKU_DARI, TO_CHAR(A.BERLAKU_SAMPAI, 'DD/MM/YYYY') BERLAKU_SAMPAI, TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY') WAKTU_SIMPAN, A.KD_PGW, B.NM_JENIS_SURAT, C.NM_KAT_PENOMORAN FROM HIS_JENIS_SURAT A, MD_JENIS_SURAT B, MD_KAT_PENOMORAN C WHERE A.KD_JENIS_SURAT='".$kd_jenis_surat."' AND B.KD_JENIS_SURAT=A.KD_JENIS_SURAT AND C.KD_KAT_PENOMORAN=A.KD_KAT_PENOMORAN ORDER BY A.BERLAKU_DARI DESC";
		return $this->tnde->query($sql)->result();
	}
	
	function insert_his_jenis_surat($data=''){
		$sql = "INSERT INTO HIS_JENIS_SURAT(KD_JENIS_SURAT, KD_KAT_PENOMORAN, BERLAKU_DARI, BERLAKU_SAMPAI, WAKTU_SIMPAN, KD_PGW)VALUES('".$data['KD_JENIS_SURAT']."', '".$data['KD_KAT_PENOMORAN']."', TO_DATE('".$data['BERLAKU_DARI']."', 'DD/MM/YYYY'), TO_DATE('".$data['BERLAKU_SAMPAI']."', 'DD/MM/YYYY'), TO_DATE('".date('d/m/Y H:i:s')."', 'DD/MM/YYYY HH24:MI:SS'), '".$data['KD_PGW']."')";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function his_jenis_surat_by_id_his_jenis_surat($id_his_jenis_surat=''){
		$sql = "SELECT A.ID_HIS_JENIS_SURAT, A.KD_JENIS_SURAT, A.KD_KAT_PENOMORAN, TO_CHAR(A.BERLAKU_DARI, 'DD/MM/YYYY') BERLAKU_DARI, TO_CHAR(A.BERLAKU_SAMPAI, 'DD/MM/YYYY') BERLAKU_SAMPAI, TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_SIMPAN, A.KD_PGW, B.NM_JENIS_SURAT, C.NM_KAT_PENOMORAN FROM HIS_JENIS_SURAT A, MD_JENIS_SURAT B, MD_KAT_PENOMORAN C WHERE A.ID_HIS_JENIS_SURAT='".$id_his_jenis_surat."' AND B.KD_JENIS_SURAT=A.KD_JENIS_SURAT AND C.KD_KAT_PENOMORAN=A.KD_KAT_PENOMORAN";
		return $this->tnde->query($sql)->row_array();
	}
	
	function update_his_jenis_surat($data='', $id_his_jenis_surat=''){
		$sql = "UPDATE HIS_JENIS_SURAT SET KD_KAT_PENOMORAN='".$data['KD_KAT_PENOMORAN']."', BERLAKU_DARI=TO_DATE('".$data['BERLAKU_DARI']."', 'DD/MM/YYYY'), BERLAKU_SAMPAI=TO_DATE('".$data['BERLAKU_SAMPAI']."', 'DD/MM/YYYY') WHERE ID_HIS_JENIS_SURAT='".$id_his_jenis_surat."' ";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function his_jenis_surat_by_tgl($par=''){
		$tgl						= (!empty($par['TANGGAL']) ? $par['TANGGAL'] : date('d/m/Y'));
		$kd_jenis_surat 	= (!empty($par['KD_JENIS_SURAT']) ? $par['KD_JENIS_SURAT'] : '');
		$sql = "SELECT A.KD_JENIS_SURAT, A.NM_JENIS_SURAT, A.KETERANGAN, B.ID_HIS_JENIS_SURAT, B.KD_KAT_PENOMORAN, TO_CHAR(B.BERLAKU_DARI, 'DD/MM/YYYY') BERLAKU_DARI, TO_CHAR(B.BERLAKU_SAMPAI, 'DD/MM/YYYY') BERLAKU_SAMPAI, TO_CHAR(B.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_SIMPAN, B.KD_PGW FROM MD_JENIS_SURAT A, HIS_JENIS_SURAT B WHERE A.KD_JENIS_SURAT='".$kd_jenis_surat."' AND B.KD_JENIS_SURAT=A.KD_JENIS_SURAT AND ((B.BERLAKU_DARI <= TO_DATE('".$tgl."', 'DD/MM/YYYY') AND B.BERLAKU_SAMPAI >= TO_DATE('".$tgl."', 'DD/MM/YYYY')) OR (B.BERLAKU_DARI <= TO_DATE('".$tgl."', 'DD/MM/YYYY') AND B.BERLAKU_SAMPAI IS NULL))";
		return $this->tnde->query($sql)->row_array();
	}
}
?>
