<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');
class Mdl_tnde_surat_masuk_v2 extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->tnde = $this->load->database('tnde', TRUE);
	}
	
	function total_distribusi_by_str_id($kd_status_distribusi='', $str_id=''){
		$sql = "SELECT COUNT(A.ID_DISTRIBUSI_SURAT) TOTAL FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B, META_DISTRIBUSI C  
					WHERE A.JABATAN_PENERIMA='".$str_id."' AND A.KD_GRUP_TUJUAN='PJB01' AND C.ID_META_DISTRIBUSI=A.ID_META_DISTRIBUSI AND B.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND A.KD_STATUS_DISTRIBUSI='".$kd_status_distribusi."' ";
		return $this->tnde->query($sql)->row_array();
	}	
	
	function get_distribusi_by_str_id_limit($kd_status_distribusi='', $str_id='', $limit=''){
		$sql = "SELECT * FROM (SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, B.ID_SURAT_KELUAR, A.ID_GRUP_SURAT, A.PEMROSES_SURAT, 
																	A.JABATAN_PEMROSES, A.KD_STATUS_DISTRIBUSI, TO_CHAR(D.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') WAKTU_KIRIM, 
																	TO_CHAR(D.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') WAKTU_PENYELESAIAN, A.ID_STATUS_SM, TO_CHAR(A.WAKTU_AKSES, 
																	'DD/MM/YYYY HH24:,I:SS') WAKTU_AKSES, A.ISI_RINGKAS, A.ISI_DISPOSISI, A.ID_DS_SEBELUMNYA, A.KD_GRUP_TUJUAN, 
																	TO_CHAR(B.TGL_MASUK, 'DD/MM/YYYY') TGL_MASUK, B.INDEKS_BERKAS, B.KODE, B.PERIHAL, B.DARI, B.KEPADA, TO_CHAR(B.TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT, B.NO_SURAT, B.CATATAN, B.KD_KEAMANAN_SURAT, B.KD_SIFAT_SURAT, 
																	B.PENCATAT_SURAT, TO_CHAR(B.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_PENCATATAN,
																	B.PENGARAH_SURAT, B.JABATAN_PENGARAH, B.ID_STATUS_SM AS ID_STATUS_SM2, TO_CHAR(B.WAKTU_AKSES, 
																	'DD/MM/YYYY HH24:MI:SS') WAKTU_AKSES2, B.LAMPIRAN, B.TEMBUSAN, B.UNIT_ID, B.GMU_SLUG, C.NM_STATUS_SM  
														FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B, MD_STATUS_SM C, META_DISTRIBUSI D  
														WHERE A.JABATAN_PENERIMA='".$str_id."' AND A.KD_GRUP_TUJUAN='PJB01' AND D.ID_META_DISTRIBUSI=A.ID_META_DISTRIBUSI AND  
																		B.ID_SURAT_MASUK=D.ID_SURAT_MASUK AND A.KD_STATUS_DISTRIBUSI='".$kd_status_distribusi."' AND C.ID_STATUS_SM=A.ID_STATUS_SM 
																		ORDER BY D.WAKTU_KIRIM DESC
													) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}	
	
	function get_distribusi_by_str_id_paging($kd_status_distribusi='', $str_id='', $limit='', $page=''){
			$sql = "SELECT * FROM (SELECT ROWNUM R, ID_DISTRIBUSI_SURAT, ID_SURAT_MASUK, ID_SURAT_KELUAR, ID_GRUP_SURAT, PEMROSES_SURAT, 	
																		JABATAN_PEMROSES, KD_STATUS_DISTRIBUSI, WAKTU_KIRIM,WAKTU_PENYELESAIAN, ID_STATUS_SM, 
																		WAKTU_AKSES, ISI_RINGKAS, ISI_DISPOSISI, ID_DS_SEBELUMNYA, KD_GRUP_TUJUAN, TGL_MASUK, INDEKS_BERKAS, KODE, 
																		PERIHAL, DARI, KEPADA, TGL_SURAT, NO_SURAT, CATATAN, KD_KEAMANAN_SURAT, KD_SIFAT_SURAT, PENCATAT_SURAT, 
																		WAKTU_PENCATATAN, PENGARAH_SURAT, JABATAN_PENGARAH, ID_STATUS_SM2, WAKTU_AKSES2, LAMPIRAN, 
																		TEMBUSAN, UNIT_ID, GMU_SLUG, NM_STATUS_SM  
															FROM (SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, B.ID_SURAT_KELUAR, A.ID_GRUP_SURAT, A.PEMROSES_SURAT, 
																							A.JABATAN_PEMROSES, A.KD_STATUS_DISTRIBUSI, TO_CHAR(D.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') 
																							WAKTU_KIRIM, TO_CHAR(D.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') WAKTU_PENYELESAIAN, A.ID_STATUS_SM, 
																							TO_CHAR(A.WAKTU_AKSES, 'DD/MM/YYYY HH24:,I:SS') WAKTU_AKSES, A.ISI_RINGKAS, A.ISI_DISPOSISI, 
																							A.ID_DS_SEBELUMNYA, A.KD_GRUP_TUJUAN, TO_CHAR(B.TGL_MASUK, 'DD/MM/YYYY') TGL_MASUK, 
																							B.INDEKS_BERKAS, B.KODE, B.PERIHAL, B.DARI, B.KEPADA, TO_CHAR(B.TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT,
																							B.NO_SURAT, B.CATATAN, B.KD_KEAMANAN_SURAT, B.KD_SIFAT_SURAT, B.PENCATAT_SURAT, 
																							TO_CHAR(B.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_PENCATATAN,
																							B.PENGARAH_SURAT, B.JABATAN_PENGARAH, B.ID_STATUS_SM AS ID_STATUS_SM2, TO_CHAR(B.WAKTU_AKSES, 
																							'DD/MM/YYYY HH24:MI:SS') WAKTU_AKSES2, B.LAMPIRAN, B.TEMBUSAN, B.UNIT_ID, B.GMU_SLUG, C.NM_STATUS_SM  
																			FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B, MD_STATUS_SM C, META_DISTRIBUSI D   
																			WHERE A.JABATAN_PENERIMA='".$str_id."' AND A.KD_GRUP_TUJUAN='PJB01' AND D.ID_META_DISTRIBUSI=A.ID_META_DISTRIBUSI AND 
																							B.ID_SURAT_MASUK=D.ID_SURAT_MASUK AND A.KD_STATUS_DISTRIBUSI='".$kd_status_distribusi."' AND 
																							C.ID_STATUS_SM=A.ID_STATUS_SM ORDER BY D.WAKTU_KIRIM DESC
																		) WHERE ROWNUM <= ".$limit."
														) WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();
	}
	
	function detail_sm_by_id_distribusi_surat($id_distribusi_surat=''){
		$sql = "SELECT A.ID_SURAT_MASUK, A.ID_SURAT_KELUAR, A.INDEKS_BERKAS, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.KD_SIFAT_SURAT, 
								A.KODE, A.KD_KEAMANAN_SURAT, A.PERIHAL, A.DARI, A.KEPADA, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, 
								A.NO_SURAT, A.CATATAN, A.UNIT_ID, A.PENCATAT_SURAT, A.PENGARAH_SURAT, A.LAMPIRAN, A.ID_STATUS_SM AS
								ID_STATUS_SM2, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, A.DIBUAT_DARI, A.ISI_SURAT, 
								TO_CHAR(A.WAKTU_AKSES, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_AKSES, B.NM_SIFAT_SURAT, C.NM_KEAMANAN_SURAT, 
								D.ID_DISTRIBUSI_SURAT, D.ID_GRUP_SURAT, D.ID_STATUS_SM, G.ID_META_DISTRIBUSI, TO_CHAR(G.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS 
								WAKTU_PENYELESAIAN, G.ISI_RINGKAS, G.ISI_PESAN, G.PENGIRIM, G.JAB_PENGIRIM, D.JABATAN_PENERIMA, 
								D.PENERIMA_SURAT, TO_CHAR(G.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, F.NM_STATUS_SM, D.KD_GRUP_TUJUAN, G.KD_GRUP_PENGIRIM
					FROM MD_SURAT_MASUK A, MD_SIFAT_SURAT B, MD_KEAMANAN_SURAT C, MD_STATUS_SM F, D_DISTRIBUSI_SURAT D, META_DISTRIBUSI G  
					WHERE D.ID_DISTRIBUSI_SURAT='".$id_distribusi_surat."' AND G.ID_META_DISTRIBUSI=D.ID_META_DISTRIBUSI AND A.ID_SURAT_MASUK=G.ID_SURAT_MASUK AND 
					B.KD_SIFAT_SURAT=A.KD_SIFAT_SURAT AND C.KD_KEAMANAN_SURAT=A.KD_KEAMANAN_SURAT AND F.ID_STATUS_SM=A.ID_STATUS_SM";
		return $this->tnde->query($sql)->row_array();
	}
	
	function histori_pesan_by_str_id($id_surat_masuk='', $pegawai=''){
		$kd_pgw	= (!empty($pegawai['KD_PGW']) ? $pegawai['KD_PGW'] : '');
		$str_id 	= (!empty($pegawai['STR_ID']) ? $pegawai['STR_ID'] : '');
		
		$sql = "SELECT ID_META_DISTRIBUSI, ID_SURAT_MASUK, PENGIRIM, JAB_PENGIRIM, KD_GRUP_PENGIRIM, ISI_RINGKAS, ISI_PESAN, TO_CHAR(WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') WAKTU_KIRIM, TO_CHAR(WAKTU_PENYELESAIAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_PENYELESAIAN, INDUK FROM META_DISTRIBUSI WHERE ID_META_DISTRIBUSI IN(SELECT DISTINCT(A.ID_META_DISTRIBUSI) FROM META_DISTRIBUSI A, D_DISTRIBUSI_SURAT B WHERE B.ID_META_DISTRIBUSI=A.ID_META_DISTRIBUSI AND B.KD_STATUS_DISTRIBUSI IN('D', 'TM') AND ((A.PENGIRIM='".$kd_pgw."' AND A.KD_GRUP_PENGIRIM='PGW01') OR (A.JAB_PENGIRIM='".$str_id."' AND A.KD_GRUP_PENGIRIM='PJB01') OR (B.PENERIMA_SURAT='".$kd_pgw."' AND B.KD_GRUP_TUJUAN='PGW01') OR (B.JABATAN_PENERIMA='".$str_id."' AND B.KD_GRUP_TUJUAN='PJB01')) AND A.ID_SURAT_MASUK='".$id_surat_masuk."') ORDER BY WAKTU_KIRIM ASC";
		return $this->tnde->query($sql)->result();
	}
	
	function distribusi_surat_by_id_meta_distribusi($id_meta_distribusi=''){
		$sql = "SELECT ID_DISTRIBUSI_SURAT, ID_GRUP_SURAT, PENERIMA_SURAT, JABATAN_PENERIMA, KD_STATUS_DISTRIBUSI, ID_STATUS_SM, TO_CHAR(WAKTU_AKSES, 'DD/MM/YYYY HH24:MI:SS') WAKTU_AKSES, ID_DS_SEBELUMNYA, KD_GRUP_TUJUAN, ID_META_DISTRIBUSI FROM D_DISTRIBUSI_SURAT WHERE ID_META_DISTRIBUSI='".$id_meta_distribusi."'";
		return $this->tnde->query($sql)->result();
	}
	
	function total_distribusi_by_kd_pgw_str_id($kd_status_distribusi='', $kd_pgw='', $str_id=''){
		$sql = "SELECT COUNT(A.ID_DISTRIBUSI_SURAT) TOTAL FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B, META_DISTRIBUSI C  
					WHERE ((A.PENERIMA_SURAT='".$kd_pgw."' AND A.KD_GRUP_TUJUAN='PGW01') OR (A.JABATAN_PENERIMA='".$str_id."' AND A.KD_GRUP_TUJUAN='PJB01')) AND C.ID_META_DISTRIBUSI=A.ID_META_DISTRIBUSI AND B.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND A.KD_STATUS_DISTRIBUSI='".$kd_status_distribusi."' ";
		return $this->tnde->query($sql)->row_array();
	}	
	
	function get_distribusi_by_kd_pgw_str_id_limit($kd_status_distribusi='', $kd_pgw='', $str_id='', $limit=''){
		$sql = "SELECT * FROM (SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, B.ID_SURAT_KELUAR, A.ID_GRUP_SURAT, A.PEMROSES_SURAT, 
																	A.JABATAN_PEMROSES, A.KD_STATUS_DISTRIBUSI, TO_CHAR(D.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') WAKTU_KIRIM, 
																	TO_CHAR(D.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') WAKTU_PENYELESAIAN, A.ID_STATUS_SM, TO_CHAR(A.WAKTU_AKSES, 
																	'DD/MM/YYYY HH24:,I:SS') WAKTU_AKSES, A.ISI_RINGKAS, A.ISI_DISPOSISI, A.ID_DS_SEBELUMNYA, A.KD_GRUP_TUJUAN, 
																	TO_CHAR(B.TGL_MASUK, 'DD/MM/YYYY') TGL_MASUK, B.INDEKS_BERKAS, B.KODE, B.PERIHAL, B.DARI, B.KEPADA, TO_CHAR(B.TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT, B.NO_SURAT, B.CATATAN, B.KD_KEAMANAN_SURAT, B.KD_SIFAT_SURAT, 
																	B.PENCATAT_SURAT, TO_CHAR(B.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_PENCATATAN,
																	B.PENGARAH_SURAT, B.JABATAN_PENGARAH, B.ID_STATUS_SM AS ID_STATUS_SM2, TO_CHAR(B.WAKTU_AKSES, 
																	'DD/MM/YYYY HH24:MI:SS') WAKTU_AKSES2, B.LAMPIRAN, B.TEMBUSAN, B.UNIT_ID, B.GMU_SLUG, C.NM_STATUS_SM, D.PENGIRIM, D.JAB_PENGIRIM   
														FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B, MD_STATUS_SM C, META_DISTRIBUSI D  
														WHERE ((A.PENERIMA_SURAT='".$kd_pgw."' AND A.KD_GRUP_TUJUAN='PGW01') OR (A.JABATAN_PENERIMA='".$str_id."' AND A.KD_GRUP_TUJUAN='PJB01')) AND D.ID_META_DISTRIBUSI=A.ID_META_DISTRIBUSI AND  
																		B.ID_SURAT_MASUK=D.ID_SURAT_MASUK AND A.KD_STATUS_DISTRIBUSI='".$kd_status_distribusi."' AND C.ID_STATUS_SM=A.ID_STATUS_SM 
																		ORDER BY D.WAKTU_KIRIM DESC
													) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_distribusi_by_kd_pgw_str_id_paging($kd_status_distribusi='', $kd_pgw='', $str_id='', $limit='', $page=''){
		$sql = "SELECT * FROM (SELECT ROWNUM R, ID_DISTRIBUSI_SURAT, ID_SURAT_MASUK, ID_SURAT_KELUAR, ID_GRUP_SURAT, PEMROSES_SURAT, 	
																		JABATAN_PEMROSES, KD_STATUS_DISTRIBUSI, WAKTU_KIRIM,WAKTU_PENYELESAIAN, ID_STATUS_SM, 
																		WAKTU_AKSES, ISI_RINGKAS, ISI_DISPOSISI, ID_DS_SEBELUMNYA, KD_GRUP_TUJUAN, TGL_MASUK, INDEKS_BERKAS, KODE, 
																		PERIHAL, DARI, KEPADA, TGL_SURAT, NO_SURAT, CATATAN, KD_KEAMANAN_SURAT, KD_SIFAT_SURAT, PENCATAT_SURAT, 
																		WAKTU_PENCATATAN, PENGARAH_SURAT, JABATAN_PENGARAH, ID_STATUS_SM2, WAKTU_AKSES2, LAMPIRAN, 
																		TEMBUSAN, UNIT_ID, GMU_SLUG, NM_STATUS_SM, PENGIRIM, JAB_PENGIRIM  
															FROM (SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, B.ID_SURAT_KELUAR, A.ID_GRUP_SURAT, A.PEMROSES_SURAT, 
																							A.JABATAN_PEMROSES, A.KD_STATUS_DISTRIBUSI, TO_CHAR(D.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') 
																							WAKTU_KIRIM, TO_CHAR(D.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') WAKTU_PENYELESAIAN, A.ID_STATUS_SM, 
																							TO_CHAR(A.WAKTU_AKSES, 'DD/MM/YYYY HH24:,I:SS') WAKTU_AKSES, A.ISI_RINGKAS, A.ISI_DISPOSISI, 
																							A.ID_DS_SEBELUMNYA, A.KD_GRUP_TUJUAN, TO_CHAR(B.TGL_MASUK, 'DD/MM/YYYY') TGL_MASUK, 
																							B.INDEKS_BERKAS, B.KODE, B.PERIHAL, B.DARI, B.KEPADA, TO_CHAR(B.TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT,
																							B.NO_SURAT, B.CATATAN, B.KD_KEAMANAN_SURAT, B.KD_SIFAT_SURAT, B.PENCATAT_SURAT, 
																							TO_CHAR(B.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_PENCATATAN,
																							B.PENGARAH_SURAT, B.JABATAN_PENGARAH, B.ID_STATUS_SM AS ID_STATUS_SM2, TO_CHAR(B.WAKTU_AKSES, 
																							'DD/MM/YYYY HH24:MI:SS') WAKTU_AKSES2, B.LAMPIRAN, B.TEMBUSAN, B.UNIT_ID, B.GMU_SLUG, C.NM_STATUS_SM, D.PENGIRIM, D.JAB_PENGIRIM   
																			FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B, MD_STATUS_SM C, META_DISTRIBUSI D   
																			WHERE ((A.PENERIMA_SURAT='".$kd_pgw."' AND A.KD_GRUP_TUJUAN='PGW01') OR (A.JABATAN_PENERIMA='".$str_id."' AND A.KD_GRUP_TUJUAN='PJB01')) AND D.ID_META_DISTRIBUSI=A.ID_META_DISTRIBUSI AND 
																							B.ID_SURAT_MASUK=D.ID_SURAT_MASUK AND A.KD_STATUS_DISTRIBUSI='".$kd_status_distribusi."' AND 
																							C.ID_STATUS_SM=A.ID_STATUS_SM ORDER BY D.WAKTU_KIRIM DESC
																		) WHERE ROWNUM <= ".$limit."
														) WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();
	}
	
	function total_distribusi_by_kd_pgw($kd_status_distribusi='', $kd_pgw=''){
		$sql = "SELECT COUNT(A.ID_DISTRIBUSI_SURAT) TOTAL FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B, META_DISTRIBUSI C  
					WHERE A.PENERIMA_SURAT='".$kd_pgw."' AND A.KD_GRUP_TUJUAN='PGW01' AND C.ID_META_DISTRIBUSI=A.ID_META_DISTRIBUSI AND B.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND A.KD_STATUS_DISTRIBUSI IN(".$kd_status_distribusi.") ";
		return $this->tnde->query($sql)->row_array();
	}	
	
	function get_distribusi_by_kd_pgw_limit($kd_status_distribusi='', $kd_pgw='', $limit=''){
		$sql = "SELECT * FROM (SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, B.ID_SURAT_KELUAR, A.ID_GRUP_SURAT, A.PEMROSES_SURAT, 
																	A.JABATAN_PEMROSES, A.KD_STATUS_DISTRIBUSI, TO_CHAR(D.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') WAKTU_KIRIM, 
																	TO_CHAR(D.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') WAKTU_PENYELESAIAN, A.ID_STATUS_SM, TO_CHAR(A.WAKTU_AKSES, 
																	'DD/MM/YYYY HH24:,I:SS') WAKTU_AKSES, A.ISI_RINGKAS, A.ISI_DISPOSISI, A.ID_DS_SEBELUMNYA, A.KD_GRUP_TUJUAN, 
																	TO_CHAR(B.TGL_MASUK, 'DD/MM/YYYY') TGL_MASUK, B.INDEKS_BERKAS, B.KODE, B.PERIHAL, B.DARI, B.KEPADA, TO_CHAR(B.TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT, B.NO_SURAT, B.CATATAN, B.KD_KEAMANAN_SURAT, B.KD_SIFAT_SURAT, 
																	B.PENCATAT_SURAT, TO_CHAR(B.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_PENCATATAN,
																	B.PENGARAH_SURAT, B.JABATAN_PENGARAH, B.ID_STATUS_SM AS ID_STATUS_SM2, TO_CHAR(B.WAKTU_AKSES, 
																	'DD/MM/YYYY HH24:MI:SS') WAKTU_AKSES2, B.LAMPIRAN, B.TEMBUSAN, B.UNIT_ID, B.GMU_SLUG, C.NM_STATUS_SM  
														FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B, MD_STATUS_SM C, META_DISTRIBUSI D  
														WHERE A.PENERIMA_SURAT='".$kd_pgw."' AND A.KD_GRUP_TUJUAN='PGW01' AND D.ID_META_DISTRIBUSI=A.ID_META_DISTRIBUSI AND  
																		B.ID_SURAT_MASUK=D.ID_SURAT_MASUK AND A.KD_STATUS_DISTRIBUSI IN(".$kd_status_distribusi.") AND C.ID_STATUS_SM=A.ID_STATUS_SM 
																		ORDER BY D.WAKTU_KIRIM DESC
													) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}	
	
	function get_distribusi_by_kd_pgw_paging($kd_status_distribusi='', $kd_pgw='', $limit='', $page=''){
			$sql = "SELECT * FROM (SELECT ROWNUM R, ID_DISTRIBUSI_SURAT, ID_SURAT_MASUK, ID_SURAT_KELUAR, ID_GRUP_SURAT, PEMROSES_SURAT, 	
																		JABATAN_PEMROSES, KD_STATUS_DISTRIBUSI, WAKTU_KIRIM,WAKTU_PENYELESAIAN, ID_STATUS_SM, 
																		WAKTU_AKSES, ISI_RINGKAS, ISI_DISPOSISI, ID_DS_SEBELUMNYA, KD_GRUP_TUJUAN, TGL_MASUK, INDEKS_BERKAS, KODE, 
																		PERIHAL, DARI, KEPADA, TGL_SURAT, NO_SURAT, CATATAN, KD_KEAMANAN_SURAT, KD_SIFAT_SURAT, PENCATAT_SURAT, 
																		WAKTU_PENCATATAN, PENGARAH_SURAT, JABATAN_PENGARAH, ID_STATUS_SM2, WAKTU_AKSES2, LAMPIRAN, 
																		TEMBUSAN, UNIT_ID, GMU_SLUG, NM_STATUS_SM  
															FROM (SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, B.ID_SURAT_KELUAR, A.ID_GRUP_SURAT, A.PEMROSES_SURAT, 
																							A.JABATAN_PEMROSES, A.KD_STATUS_DISTRIBUSI, TO_CHAR(D.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') 
																							WAKTU_KIRIM, TO_CHAR(D.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') WAKTU_PENYELESAIAN, A.ID_STATUS_SM, 
																							TO_CHAR(A.WAKTU_AKSES, 'DD/MM/YYYY HH24:,I:SS') WAKTU_AKSES, A.ISI_RINGKAS, A.ISI_DISPOSISI, 
																							A.ID_DS_SEBELUMNYA, A.KD_GRUP_TUJUAN, TO_CHAR(B.TGL_MASUK, 'DD/MM/YYYY') TGL_MASUK, 
																							B.INDEKS_BERKAS, B.KODE, B.PERIHAL, B.DARI, B.KEPADA, TO_CHAR(B.TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT,
																							B.NO_SURAT, B.CATATAN, B.KD_KEAMANAN_SURAT, B.KD_SIFAT_SURAT, B.PENCATAT_SURAT, 
																							TO_CHAR(B.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_PENCATATAN,
																							B.PENGARAH_SURAT, B.JABATAN_PENGARAH, B.ID_STATUS_SM AS ID_STATUS_SM2, TO_CHAR(B.WAKTU_AKSES, 
																							'DD/MM/YYYY HH24:MI:SS') WAKTU_AKSES2, B.LAMPIRAN, B.TEMBUSAN, B.UNIT_ID, B.GMU_SLUG, C.NM_STATUS_SM  
																			FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B, MD_STATUS_SM C, META_DISTRIBUSI D   
																			WHERE A.PENERIMA_SURAT='".$kd_pgw."' AND A.KD_GRUP_TUJUAN='PGW01' AND D.ID_META_DISTRIBUSI=A.ID_META_DISTRIBUSI AND 
																							B.ID_SURAT_MASUK=D.ID_SURAT_MASUK AND A.KD_STATUS_DISTRIBUSI IN(".$kd_status_distribusi.") AND 
																							C.ID_STATUS_SM=A.ID_STATUS_SM ORDER BY D.WAKTU_KIRIM DESC
																		) WHERE ROWNUM <= ".$limit."
														) WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_distribusi_surat_by_par($data=''){
		$id_surat_masuk = (!empty($data['ID_SURAT_MASUK']) ? $data['ID_SURAT_MASUK'] : '');
		if(!empty($data['KD_STATUS_DISTRIBUSI'])){ 
			if(is_array($data['KD_STATUS_DISTRIBUSI'])){
				$kd_status_distribusi = implode_to_string($data['KD_STATUS_DISTRIBUSI']);
			}else{
				$kd_status_distribusi = "'".$data['KD_STATUS_DISTRIBUSI']."'";
			}
			$sql_arr[] = "B.KD_STATUS_DISTRIBUSI IN(".$kd_status_distribusi.") ";
		}
		if(!empty($sql_arr)){
			$imp = implode(" AND ", $sql_arr);
			$sql_plus = " AND ".$imp;
		}else{
			$sql_plus = "";
		}
		
		$sql = "SELECT A.*, B.* FROM SURAT_MASUK_V A, DISTRIBUSI_SURAT_V B WHERE A.ID_SURAT_MASUK='".$id_surat_masuk."' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK ".$sql_plus;
		return $this->tnde->query($sql)->result();
	}
	
	function detail_sm_by_par($data=''){
		$id_surat_masuk = (!empty($data['ID_SURAT_MASUK']) ? $data['ID_SURAT_MASUK'] : '');
		$sql = "SELECT * FROM SURAT_MASUK_V WHERE ID_SURAT_MASUK='".$id_surat_masuk."'";
		return $this->tnde->query($sql)->row_array();
	}	
	
	function detail_distribusi_surat_by_par($data=''){
		$id_distribusi_surat = (!empty($data['ID_DISTRIBUSI_SURAT']) ? $data['ID_DISTRIBUSI_SURAT'] : '');
		$sql = "SELECT A.*, B.* FROM DISTRIBUSI_SURAT_V A, SURAT_MASUK_V B WHERE A.ID_DISTRIBUSI_SURAT='".$id_distribusi_surat."' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_distribusi_cari_by_par($data=''){
		if(!empty($data['PENERIMA_SURAT']) && !empty($data['JABATAN_PENERIMA'])){
			$sql_arr[] = "((A.PENERIMA_SURAT='".$data['PENERIMA_SURAT']."' AND A.KD_GRUP_TUJUAN='PGW01') OR (A.JABATAN_PENERIMA='".$data['JABATAN_PENERIMA']."' AND A.KD_GRUP_TUJUAN='PJB01'))";
		}else{
			if(!empty($data['PENERIMA_SURAT'])){ 
				$sql_arr[] = "A.PENERIMA_SURAT='".$data['PENERIMA_SURAT']."'";
			}
			if(!empty($data['JABATAN_PENERIMA'])){ 
				$sql_arr[] = "A.JABATAN_PENERIMA='".$data['JABATAN_PENERIMA']."'";
			}			
			if(!empty($data['KD_GRUP_TUJUAN'])){ 
				$sql_arr[] = "A.KD_GRUP_TUJUAN='".$data['KD_GRUP_TUJUAN']."'";
			}
		}
		if(!empty($data['KD_STATUS_DISTRIBUSI'])){ 
			if(is_array($data['KD_STATUS_DISTRIBUSI'])){
				$kd_status_distribusi = implode_to_string($data['KD_STATUS_DISTRIBUSI']);
			}else{
				$kd_status_distribusi = "'".$data['KD_STATUS_DISTRIBUSI']."'";
			}
			$sql_arr[] = "A.KD_STATUS_DISTRIBUSI IN(".$kd_status_distribusi.") ";
		}
		if(!empty($data['TGL_AWAL'])){
			$sql_arr[] = "C.WAKTU_KIRIM >= TO_DATE('".$data['TGL_AWAL']."', 'DD/MM/YYYY')";
		}
		if(!empty($data['TGL_AKHIR'])){
			$sql_arr[] = "C.WAKTU_KIRIM <= TO_DATE('".$data['TGL_AKHIR']." 23:59:59', 'DD/MM/YYYY HH24:MI:SS')";
		}
		if(!empty($data['Q'])){
			$sql_arr[] = "(TO_CHAR(B.TGL_MASUK, 'DD/MM/YYYY') LIKE '%".$data['Q']."%' OR TO_CHAR(B.TGL_SURAT, 'DD/MM/YYYY') LIKE '%".$data['Q']."%' OR UPPER(B.PERIHAL) LIKE UPPER('%".$data['Q']."%') OR UPPER(B.INDEKS_BERKAS) LIKE UPPER('%".$data['Q']."%') OR UPPER(B.DARI) LIKE UPPER('%".$data['Q']."%') OR UPPER(B.KEPADA) LIKE UPPER('%".$data['Q']."%') OR UPPER(B.NO_SURAT) LIKE UPPER('%".$data['Q']."%') OR UPPER(B.CATATAN) LIKE UPPER('%".$data['Q']."%') OR UPPER(B.TEMBUSAN) LIKE UPPER('%".$data['Q']."%') OR UPPER(C.ISI_RINGKAS) LIKE UPPER('%".$data['Q']."%') OR UPPER(C.ISI_PESAN) LIKE UPPER('%".$data['Q']."%') OR TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE UPPER('%".$data['Q']."%'))";
		}
		if(!empty($sql_arr)){
			$imp = implode(" AND ", $sql_arr);
			$sql_plus = " AND ".$imp;
		}else{
			$sql_plus = "";
		}
		$sql = "SELECT COUNT(A.ID_DISTRIBUSI_SURAT) TOTAL 
					 FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B, META_DISTRIBUSI C  
                     WHERE C.ID_META_DISTRIBUSI=A.ID_META_DISTRIBUSI AND                                    B.ID_SURAT_MASUK=C.ID_SURAT_MASUK ".$sql_plus;
		return $this->tnde->query($sql)->row_array();
	}
	
	function get_distribusi_cari_by_par($data=''){
		if(!empty($data['PENERIMA_SURAT']) && !empty($data['JABATAN_PENERIMA'])){
			$sql_arr[] = "((A.PENERIMA_SURAT='".$data['PENERIMA_SURAT']."' AND A.KD_GRUP_TUJUAN='PGW01') OR (A.JABATAN_PENERIMA='".$data['JABATAN_PENERIMA']."' AND A.KD_GRUP_TUJUAN='PJB01'))";
		}else{
			if(!empty($data['PENERIMA_SURAT'])){ 
				$sql_arr[] = "A.PENERIMA_SURAT='".$data['PENERIMA_SURAT']."'";
			}
			if(!empty($data['JABATAN_PENERIMA'])){ 
				$sql_arr[] = "A.JABATAN_PENERIMA='".$data['JABATAN_PENERIMA']."'";
			}			
			if(!empty($data['KD_GRUP_TUJUAN'])){ 
				$sql_arr[] = "A.KD_GRUP_TUJUAN='".$data['KD_GRUP_TUJUAN']."'";
			}
		}
		if(!empty($data['KD_STATUS_DISTRIBUSI'])){ 
			if(is_array($data['KD_STATUS_DISTRIBUSI'])){
				$kd_status_distribusi = implode_to_string($data['KD_STATUS_DISTRIBUSI']);
			}else{
				$kd_status_distribusi = "'".$data['KD_STATUS_DISTRIBUSI']."'";
			}
			$sql_arr[] = "A.KD_STATUS_DISTRIBUSI IN(".$kd_status_distribusi.") ";
		}
		if(!empty($data['TGL_AWAL'])){
			$sql_arr[] = "TO_DATE(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') >= TO_DATE('".$data['TGL_AWAL']."', 'DD/MM/YYYY')";
		}
		if(!empty($data['TGL_AKHIR'])){
			$sql_arr[] = "TO_DATE(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') <= TO_DATE('".$data['TGL_AKHIR']." 23:59:59', 'DD/MM/YYYY HH24:MI:SS')";
		}
		if(!empty($data['Q'])){
			$sql_arr[] = "(B.TGL_MASUK LIKE '%".$data['Q']."%' OR B.TGL_SURAT LIKE '%".$data['Q']."%' OR UPPER(B.PERIHAL) LIKE UPPER('%".$data['Q']."%') OR UPPER(B.INDEKS_BERKAS) LIKE UPPER('%".$data['Q']."%') OR UPPER(B.DARI) LIKE UPPER('%".$data['Q']."%') OR UPPER(B.KEPADA) LIKE UPPER('%".$data['Q']."%') OR UPPER(B.NO_SURAT) LIKE UPPER('%".$data['Q']."%') OR UPPER(B.CATATAN) LIKE UPPER('%".$data['Q']."%') OR UPPER(B.TEMBUSAN) LIKE UPPER('%".$data['Q']."%') OR UPPER(A.ISI_RINGKAS) LIKE UPPER('%".$data['Q']."%') OR UPPER(A.ISI_PESAN) LIKE UPPER('%".$data['Q']."%') OR A.WAKTU_KIRIM LIKE UPPER('%".$data['Q']."%'))";
		}
		if(!empty($sql_arr)){
			$imp = implode(" AND ", $sql_arr);
			$sql_plus = " AND ".$imp;
		}else{
			$sql_plus = "";
		}
		if(ISSET($data['INDEX'])){
			$index = $data['INDEX'];
		}else{
			$index = 0;
		}
		if(!empty($data['LIMIT'])){
			$limit = $data['LIMIT'] + $index;
		}else{
			$limit = 1000 + $index;
		}
			
		$sql = "SELECT * FROM (SELECT ROWNUM R, ID_META_DISTRIBUSI, ID_SURAT_MASUK, PENGIRIM, JAB_PENGIRIM, KD_GRUP_PENGIRIM, NM_GRUP_PENGIRIM, ISI_RINGKAS, ISI_PESAN, WAKTU_KIRIM, WAKTU_PENYELESAIAN, INDUK, ID_DISTRIBUSI_SURAT, ID_GRUP_SURAT, PENERIMA_SURAT, JABATAN_PENERIMA, KD_STATUS_DISTRIBUSI, ID_STATUS_SM, NM_STATUS_SM, WAKTU_AKSES, KD_GRUP_TUJUAN, NM_GRUP_TUJUAN,ID_SURAT_KELUAR, TGL_MASUK, INDEKS_BERKAS, KODE, PERIHAL, DARI, KEPADA, TGL_SURAT, NO_SURAT, CATATAN, KD_KEAMANAN_SURAT, KD_SIFAT_SURAT, PENCATAT_SURAT, WAKTU_PENCATATAN, PENGARAH_SURAT, JABATAN_PENGARAH, LAMPIRAN, TEMBUSAN, UNIT_ID, GMU_SLUG 
		FROM (SELECT A.*, B.ID_SURAT_KELUAR, B.TGL_MASUK, B.INDEKS_BERKAS, B.KODE, B.PERIHAL, B.DARI, B.KEPADA, B.TGL_SURAT, B.NO_SURAT, B.CATATAN, B.KD_KEAMANAN_SURAT, B.KD_SIFAT_SURAT, B.PENCATAT_SURAT, B.WAKTU_PENCATATAN, B.PENGARAH_SURAT, B.JABATAN_PENGARAH, B.LAMPIRAN, B.TEMBUSAN, B.UNIT_ID, B.GMU_SLUG FROM DISTRIBUSI_SURAT_V A, SURAT_MASUK_V B WHERE B.ID_SURAT_MASUK=A.ID_SURAT_MASUK ".$sql_plus." ORDER BY A.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.") WHERE R > ".$index;
		
		return $this->tnde->query($sql)->result();
	}
}
?>