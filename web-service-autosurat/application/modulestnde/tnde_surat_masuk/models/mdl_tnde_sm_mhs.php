<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');
class Mdl_tnde_sm_mhs extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->tnde = $this->load->database('tnde', TRUE);
	}
	
	function total_distribusi_status_akses_by_nim($kd_status_distribusi, $id_status_sm, $nim){
		$sql = "SELECT COUNT(A.ID_DISTRIBUSI_SURAT) TOTAL FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B 
					WHERE A.PENERIMA_SURAT='".$nim."' AND A.KD_GRUP_TUJUAN='MHS01' AND 
									B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND A.KD_STATUS_DISTRIBUSI IN(".$kd_status_distribusi.") AND A.ID_STATUS_SM IN(".$id_status_sm.")";
		return $this->tnde->query($sql)->row_array();
	}
	
	function get_distribusi_by_nim_limit($kd_status_distribusi='', $nim='', $limit=''){
		$sql = "SELECT * FROM (SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, B.ID_SURAT_KELUAR, A.ID_GRUP_SURAT, A.PEMROSES_SURAT, 
																	A.JABATAN_PEMROSES, A.KD_STATUS_DISTRIBUSI, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') WAKTU_KIRIM, 
																	TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') WAKTU_PENYELESAIAN, A.ID_STATUS_SM, TO_CHAR(A.WAKTU_AKSES, 
																	'DD/MM/YYYY HH24:,I:SS') WAKTU_AKSES, A.ISI_RINGKAS, A.ISI_DISPOSISI, A.ID_DS_SEBELUMNYA, A.KD_GRUP_TUJUAN, 
																	TO_CHAR(B.TGL_MASUK, 'DD/MM/YYYY') TGL_MASUK, B.INDEKS_BERKAS, B.KODE, B.PERIHAL, B.DARI, B.KEPADA, TO_CHAR(B.TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT, B.NO_SURAT, B.CATATAN, B.KD_KEAMANAN_SURAT, B.KD_SIFAT_SURAT, 
																	B.PENCATAT_SURAT, TO_CHAR(B.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_PENCATATAN,
																	B.PENGARAH_SURAT, B.JABATAN_PENGARAH, B.ID_STATUS_SM AS ID_STATUS_SM2, TO_CHAR(B.WAKTU_AKSES, 
																	'DD/MM/YYYY HH24:MI:SS') WAKTU_AKSES2, B.LAMPIRAN, B.TEMBUSAN, B.UNIT_ID, B.GMU_SLUG, C.NM_STATUS_SM  
														FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B, MD_STATUS_SM C 
														WHERE A.PENERIMA_SURAT='".$nim."' AND A.KD_GRUP_TUJUAN='MHS01' AND 
																		B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND A.KD_STATUS_DISTRIBUSI IN(".$kd_status_distribusi.") AND C.ID_STATUS_SM=A.ID_STATUS_SM 
																		ORDER BY A.WAKTU_KIRIM DESC
													) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_distribusi_by_nim_paging($kd_status_distribusi='', $nim='', $limit='', $page=''){
			$sql = "SELECT * FROM (SELECT ROWNUM R, ID_DISTRIBUSI_SURAT, ID_SURAT_MASUK, ID_SURAT_KELUAR, ID_GRUP_SURAT, PEMROSES_SURAT, 	
																		JABATAN_PEMROSES, KD_STATUS_DISTRIBUSI, WAKTU_KIRIM,WAKTU_PENYELESAIAN, ID_STATUS_SM, 
																		WAKTU_AKSES, ISI_RINGKAS, ISI_DISPOSISI, ID_DS_SEBELUMNYA, KD_GRUP_TUJUAN, TGL_MASUK, INDEKS_BERKAS, KODE, 
																		PERIHAL, DARI, KEPADA, TGL_SURAT, NO_SURAT, CATATAN, KD_KEAMANAN_SURAT, KD_SIFAT_SURAT, PENCATAT_SURAT, 
																		WAKTU_PENCATATAN, PENGARAH_SURAT, JABATAN_PENGARAH, ID_STATUS_SM2, WAKTU_AKSES2, LAMPIRAN, 
																		TEMBUSAN, UNIT_ID, GMU_SLUG, NM_STATUS_SM  
															FROM (SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, B.ID_SURAT_KELUAR, A.ID_GRUP_SURAT, A.PEMROSES_SURAT, 
																							A.JABATAN_PEMROSES, A.KD_STATUS_DISTRIBUSI, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') 
																							WAKTU_KIRIM, TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') WAKTU_PENYELESAIAN, A.ID_STATUS_SM, 
																							TO_CHAR(A.WAKTU_AKSES, 'DD/MM/YYYY HH24:,I:SS') WAKTU_AKSES, A.ISI_RINGKAS, A.ISI_DISPOSISI, 
																							A.ID_DS_SEBELUMNYA, A.KD_GRUP_TUJUAN, TO_CHAR(B.TGL_MASUK, 'DD/MM/YYYY') TGL_MASUK, 
																							B.INDEKS_BERKAS, B.KODE, B.PERIHAL, B.DARI, B.KEPADA, TO_CHAR(B.TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT,
																							B.NO_SURAT, B.CATATAN, B.KD_KEAMANAN_SURAT, B.KD_SIFAT_SURAT, B.PENCATAT_SURAT, 
																							TO_CHAR(B.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_PENCATATAN,
																							B.PENGARAH_SURAT, B.JABATAN_PENGARAH, B.ID_STATUS_SM AS ID_STATUS_SM2, TO_CHAR(B.WAKTU_AKSES, 
																							'DD/MM/YYYY HH24:MI:SS') WAKTU_AKSES2, B.LAMPIRAN, B.TEMBUSAN, B.UNIT_ID, B.GMU_SLUG, C.NM_STATUS_SM  
																			FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B, MD_STATUS_SM C  
																			WHERE A.PENERIMA_SURAT='".$nim."' AND A.KD_GRUP_TUJUAN='MHS01' AND 
																							B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND A.KD_STATUS_DISTRIBUSI IN(".$kd_status_distribusi.") AND 
																							C.ID_STATUS_SM=A.ID_STATUS_SM ORDER BY A.WAKTU_KIRIM DESC
																		) WHERE ROWNUM <= ".$limit."
														) WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();
	}
	
	/* VERSI 2 = META DISTRIBUSI */
	function total_distribusi_status_akses_by_nim_v2($kd_status_distribusi, $id_status_sm, $nim){
		$sql = "SELECT COUNT(A.ID_DISTRIBUSI_SURAT) TOTAL FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B, META_DISTRIBUSI C WHERE A.PENERIMA_SURAT='".$nim."' AND A.KD_GRUP_TUJUAN='MHS01' AND C.ID_META_DISTRIBUSI=A.ID_META_DISTRIBUSI AND B.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND A.KD_STATUS_DISTRIBUSI IN(".$kd_status_distribusi.") AND A.ID_STATUS_SM IN(".$id_status_sm.")";
		return $this->tnde->query($sql)->row_array();
	}
	function get_distribusi_by_nim_limit_v2($kd_status_distribusi='', $nim='', $limit=''){
		$sql = "SELECT * FROM (SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, B.ID_SURAT_KELUAR, A.ID_GRUP_SURAT, A.PEMROSES_SURAT, 
																	A.JABATAN_PEMROSES, A.KD_STATUS_DISTRIBUSI, TO_CHAR(D.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') WAKTU_KIRIM, 
																	TO_CHAR(D.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') WAKTU_PENYELESAIAN, A.ID_STATUS_SM, TO_CHAR(A.WAKTU_AKSES, 
																	'DD/MM/YYYY HH24:,I:SS') WAKTU_AKSES, A.ISI_RINGKAS, A.ISI_DISPOSISI, A.ID_DS_SEBELUMNYA, A.KD_GRUP_TUJUAN, 
																	TO_CHAR(B.TGL_MASUK, 'DD/MM/YYYY') TGL_MASUK, B.INDEKS_BERKAS, B.KODE, B.PERIHAL, B.DARI, B.KEPADA, TO_CHAR(B.TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT, B.NO_SURAT, B.CATATAN, B.KD_KEAMANAN_SURAT, B.KD_SIFAT_SURAT, 
																	B.PENCATAT_SURAT, TO_CHAR(B.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_PENCATATAN,
																	B.PENGARAH_SURAT, B.JABATAN_PENGARAH, B.ID_STATUS_SM AS ID_STATUS_SM2, TO_CHAR(B.WAKTU_AKSES, 
																	'DD/MM/YYYY HH24:MI:SS') WAKTU_AKSES2, B.LAMPIRAN, B.TEMBUSAN, B.UNIT_ID, B.GMU_SLUG, C.NM_STATUS_SM  
														FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B, MD_STATUS_SM C, META_DISTRIBUSI D  
														WHERE A.PENERIMA_SURAT='".$nim."' AND A.KD_GRUP_TUJUAN='MHS01' AND D.ID_META_DISTRIBUSI=A.ID_META_DISTRIBUSI AND 
																		B.ID_SURAT_MASUK=D.ID_SURAT_MASUK AND A.KD_STATUS_DISTRIBUSI IN(".$kd_status_distribusi.") AND C.ID_STATUS_SM=A.ID_STATUS_SM 
																		ORDER BY A.WAKTU_KIRIM DESC
													) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_distribusi_by_nim_paging_v2($kd_status_distribusi='', $nim='', $limit='', $page=''){
			$sql = "SELECT * FROM (SELECT ROWNUM R, ID_DISTRIBUSI_SURAT, ID_SURAT_MASUK, ID_SURAT_KELUAR, ID_GRUP_SURAT, PEMROSES_SURAT, 	
																		JABATAN_PEMROSES, KD_STATUS_DISTRIBUSI, WAKTU_KIRIM,WAKTU_PENYELESAIAN, ID_STATUS_SM, 
																		WAKTU_AKSES, ISI_RINGKAS, ISI_DISPOSISI, ID_DS_SEBELUMNYA, KD_GRUP_TUJUAN, TGL_MASUK, INDEKS_BERKAS, KODE, 
																		PERIHAL, DARI, KEPADA, TGL_SURAT, NO_SURAT, CATATAN, KD_KEAMANAN_SURAT, KD_SIFAT_SURAT, PENCATAT_SURAT, 
																		WAKTU_PENCATATAN, PENGARAH_SURAT, JABATAN_PENGARAH, ID_STATUS_SM2, WAKTU_AKSES2, LAMPIRAN, 
																		TEMBUSAN, UNIT_ID, GMU_SLUG, NM_STATUS_SM  
															FROM (SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, B.ID_SURAT_KELUAR, A.ID_GRUP_SURAT, A.PEMROSES_SURAT, 
																							A.JABATAN_PEMROSES, A.KD_STATUS_DISTRIBUSI, TO_CHAR(D.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') 
																							WAKTU_KIRIM, TO_CHAR(D.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') WAKTU_PENYELESAIAN, A.ID_STATUS_SM, 
																							TO_CHAR(A.WAKTU_AKSES, 'DD/MM/YYYY HH24:,I:SS') WAKTU_AKSES, A.ISI_RINGKAS, A.ISI_DISPOSISI, 
																							A.ID_DS_SEBELUMNYA, A.KD_GRUP_TUJUAN, TO_CHAR(B.TGL_MASUK, 'DD/MM/YYYY') TGL_MASUK, 
																							B.INDEKS_BERKAS, B.KODE, B.PERIHAL, B.DARI, B.KEPADA, TO_CHAR(B.TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT,
																							B.NO_SURAT, B.CATATAN, B.KD_KEAMANAN_SURAT, B.KD_SIFAT_SURAT, B.PENCATAT_SURAT, 
																							TO_CHAR(B.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_PENCATATAN,
																							B.PENGARAH_SURAT, B.JABATAN_PENGARAH, B.ID_STATUS_SM AS ID_STATUS_SM2, TO_CHAR(B.WAKTU_AKSES, 
																							'DD/MM/YYYY HH24:MI:SS') WAKTU_AKSES2, B.LAMPIRAN, B.TEMBUSAN, B.UNIT_ID, B.GMU_SLUG, C.NM_STATUS_SM  
																			FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B, MD_STATUS_SM C, META_DISTRIBUSI D   
																			WHERE A.PENERIMA_SURAT='".$nim."' AND A.KD_GRUP_TUJUAN='MHS01' AND D.ID_META_DISTRIBUSI=A.ID_META_DISTRIBUSI AND 
																							B.ID_SURAT_MASUK=D.ID_SURAT_MASUK AND A.KD_STATUS_DISTRIBUSI IN(".$kd_status_distribusi.") AND 
																							C.ID_STATUS_SM=A.ID_STATUS_SM ORDER BY A.WAKTU_KIRIM DESC
																		) WHERE ROWNUM <= ".$limit."
														) WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();
	}
	/* END VERSI 2*/
	
	function total_distribusi_cari_by_nim_tgl_awal($kd_status_distribusi='', $nim='', $q='', $tgl_awal){
		$sql = "SELECT COUNT(A.ID_DISTRIBUSI_SURAT) TOTAL 
					 FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B 
                     WHERE A.PENERIMA_SURAT='".$nim."' AND A.KD_GRUP_TUJUAN='MHS01' AND 
                                    B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND A.KD_STATUS_DISTRIBUSI IN(".$kd_status_distribusi.") AND 
									A.WAKTU_KIRIM >= TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND 
									(TO_CHAR(B.TGL_MASUK, 'DD/MM/YYYY') LIKE '%".$q."%' OR TO_CHAR(B.TGL_SURAT, 'DD/MM/YYYY') LIKE '%".$q."%' OR UPPER(B.PERIHAL) 
										LIKE UPPER('%".$q."%') OR UPPER(B.INDEKS_BERKAS) LIKE UPPER('%".$q."%') OR UPPER(B.DARI) LIKE UPPER('%".$q."%') OR UPPER(B.KEPADA) 
										LIKE UPPER('%".$q."%') OR UPPER(B.NO_SURAT) LIKE UPPER('%".$q."%') OR UPPER(B.CATATAN) LIKE UPPER('%".$q."%') 
										OR UPPER(B.TEMBUSAN) LIKE UPPER('%".$q."%') OR UPPER(A.ISI_RINGKAS) LIKE UPPER('%".$q."%') OR UPPER(A.ISI_DISPOSISI) LIKE 
										UPPER('%".$q."%') OR TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE UPPER('%".$q."%')
									)";
		return $this->tnde->query($sql)->row_array();
	}
	
	function get_distribusi_cari_by_nim_tgl_awal_limit($kd_status_distribusi='', $nim='', $q='', $tgl_awal, $limit=''){
		$sql = "SELECT * FROM (SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, B.ID_SURAT_KELUAR, A.ID_GRUP_SURAT, A.PEMROSES_SURAT, 
																	A.JABATAN_PEMROSES, A.KD_STATUS_DISTRIBUSI, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') WAKTU_KIRIM, 
																	TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') WAKTU_PENYELESAIAN, A.ID_STATUS_SM, TO_CHAR(A.WAKTU_AKSES, 
																	'DD/MM/YYYY HH24:,I:SS') WAKTU_AKSES, A.ISI_RINGKAS, A.ISI_DISPOSISI, A.ID_DS_SEBELUMNYA, A.KD_GRUP_TUJUAN, 
																	TO_CHAR(B.TGL_MASUK, 'DD/MM/YYYY') TGL_MASUK, B.INDEKS_BERKAS, B.KODE, B.PERIHAL, B.DARI, B.KEPADA, TO_CHAR(B.TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT, B.NO_SURAT, B.CATATAN, B.KD_KEAMANAN_SURAT, B.KD_SIFAT_SURAT, 
																	B.PENCATAT_SURAT, TO_CHAR(B.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_PENCATATAN,
																	B.PENGARAH_SURAT, B.JABATAN_PENGARAH, B.ID_STATUS_SM AS ID_STATUS_SM2, TO_CHAR(B.WAKTU_AKSES, 
																	'DD/MM/YYYY HH24:MI:SS') WAKTU_AKSES2, B.LAMPIRAN, B.TEMBUSAN, B.UNIT_ID, B.GMU_SLUG, C.NM_STATUS_SM  
														FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B, MD_STATUS_SM C 
														WHERE A.PENERIMA_SURAT='".$nim."' AND A.KD_GRUP_TUJUAN='MHS01' AND 
																		B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND A.KD_STATUS_DISTRIBUSI IN(".$kd_status_distribusi.") AND 
																		C.ID_STATUS_SM=A.ID_STATUS_SM
																		AND A.WAKTU_KIRIM >= TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND 
																		(TO_CHAR(B.TGL_MASUK, 'DD/MM/YYYY') LIKE '%".$q."%' OR TO_CHAR(B.TGL_SURAT, 'DD/MM/YYYY') LIKE '%".$q."%' 
																			OR UPPER(B.PERIHAL) LIKE UPPER('%".$q."%') OR UPPER(B.INDEKS_BERKAS) LIKE UPPER('%".$q."%') OR UPPER(B.DARI) 
																			LIKE UPPER('%".$q."%') OR UPPER(B.KEPADA) LIKE UPPER('%".$q."%') OR UPPER(B.NO_SURAT) LIKE UPPER('%".$q."%') OR 
																			UPPER(B.CATATAN) LIKE UPPER('%".$q."%') OR UPPER(B.TEMBUSAN) LIKE UPPER('%".$q."%') OR UPPER(A.ISI_RINGKAS) 
																			LIKE UPPER('%".$q."%') OR UPPER(A.ISI_DISPOSISI) LIKE UPPER('%".$q."%') OR 
																			TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE UPPER('%".$q."%')
																		)																		
																		ORDER BY A.WAKTU_KIRIM DESC
													) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_distribusi_cari_by_nim_tgl_awal_paging($kd_status_distribusi='', $nim='', $q='', $tgl_awal, $limit='', $page=''){
			$sql = "SELECT * FROM (SELECT ROWNUM R, ID_DISTRIBUSI_SURAT, ID_SURAT_MASUK, ID_SURAT_KELUAR, ID_GRUP_SURAT, PEMROSES_SURAT, 	
																		JABATAN_PEMROSES, KD_STATUS_DISTRIBUSI, WAKTU_KIRIM,WAKTU_PENYELESAIAN, ID_STATUS_SM, 
																		WAKTU_AKSES, ISI_RINGKAS, ISI_DISPOSISI, ID_DS_SEBELUMNYA, KD_GRUP_TUJUAN, TGL_MASUK, INDEKS_BERKAS, KODE, 
																		PERIHAL, DARI, KEPADA, TGL_SURAT, NO_SURAT, CATATAN, KD_KEAMANAN_SURAT, KD_SIFAT_SURAT, PENCATAT_SURAT, 
																		WAKTU_PENCATATAN, PENGARAH_SURAT, JABATAN_PENGARAH, ID_STATUS_SM2, WAKTU_AKSES2, LAMPIRAN, 
																		TEMBUSAN, UNIT_ID, GMU_SLUG, NM_STATUS_SM  
															FROM (SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, B.ID_SURAT_KELUAR, A.ID_GRUP_SURAT, A.PEMROSES_SURAT, 
																							A.JABATAN_PEMROSES, A.KD_STATUS_DISTRIBUSI, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') 
																							WAKTU_KIRIM, TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') WAKTU_PENYELESAIAN, A.ID_STATUS_SM, 
																							TO_CHAR(A.WAKTU_AKSES, 'DD/MM/YYYY HH24:,I:SS') WAKTU_AKSES, A.ISI_RINGKAS, A.ISI_DISPOSISI, 
																							A.ID_DS_SEBELUMNYA, A.KD_GRUP_TUJUAN, TO_CHAR(B.TGL_MASUK, 'DD/MM/YYYY') TGL_MASUK, 
																							B.INDEKS_BERKAS, B.KODE, B.PERIHAL, B.DARI, B.KEPADA, TO_CHAR(B.TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT,
																							B.NO_SURAT, B.CATATAN, B.KD_KEAMANAN_SURAT, B.KD_SIFAT_SURAT, B.PENCATAT_SURAT, 
																							TO_CHAR(B.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_PENCATATAN,
																							B.PENGARAH_SURAT, B.JABATAN_PENGARAH, B.ID_STATUS_SM AS ID_STATUS_SM2, TO_CHAR(B.WAKTU_AKSES, 
																							'DD/MM/YYYY HH24:MI:SS') WAKTU_AKSES2, B.LAMPIRAN, B.TEMBUSAN, B.UNIT_ID, B.GMU_SLUG, C.NM_STATUS_SM  
																			FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B, MD_STATUS_SM C  
																			WHERE A.PENERIMA_SURAT='".$nim."' AND A.KD_GRUP_TUJUAN='MHS01' AND 
																							B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND A.KD_STATUS_DISTRIBUSI IN(".$kd_status_distribusi.") AND 
																							C.ID_STATUS_SM=A.ID_STATUS_SM AND A.WAKTU_KIRIM >= TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND 
																							(TO_CHAR(B.TGL_MASUK, 'DD/MM/YYYY') LIKE '%".$q."%' OR TO_CHAR(B.TGL_SURAT, 'DD/MM/YYYY') LIKE '%".$q."%' 
																								OR UPPER(B.PERIHAL) LIKE UPPER('%".$q."%') OR UPPER(B.INDEKS_BERKAS) LIKE UPPER('%".$q."%') 
																								OR UPPER(B.DARI) LIKE UPPER('%".$q."%') OR UPPER(B.KEPADA) LIKE UPPER('%".$q."%') OR 
																								UPPER(B.NO_SURAT) LIKE UPPER('%".$q."%') OR UPPER(B.CATATAN) LIKE UPPER('%".$q."%') OR 
																								UPPER(B.TEMBUSAN) LIKE UPPER('%".$q."%') OR UPPER(A.ISI_RINGKAS) 
																								LIKE UPPER('%".$q."%') OR UPPER(A.ISI_DISPOSISI) LIKE UPPER('%".$q."%') OR 
																								TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE UPPER('%".$q."%')
																							)
																							ORDER BY A.WAKTU_KIRIM DESC
																		) WHERE ROWNUM <= ".$limit."
														) WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();
	}
	
	function total_distribusi_cari_by_nim_tgl_akhir($kd_status_distribusi='', $nim='', $q='', $tgl_akhir){
		$sql = "SELECT COUNT(A.ID_DISTRIBUSI_SURAT) TOTAL 
					 FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B 
                     WHERE A.PENERIMA_SURAT='".$nim."' AND A.KD_GRUP_TUJUAN='MHS01' AND 
                                    B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND A.KD_STATUS_DISTRIBUSI IN(".$kd_status_distribusi.") AND 
									A.WAKTU_KIRIM <= TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') 
									AND 
									(TO_CHAR(B.TGL_MASUK, 'DD/MM/YYYY') LIKE '%".$q."%' OR TO_CHAR(B.TGL_SURAT, 'DD/MM/YYYY') LIKE '%".$q."%' OR UPPER(B.PERIHAL) 
										LIKE UPPER('%".$q."%') OR UPPER(B.INDEKS_BERKAS) LIKE UPPER('%".$q."%') OR UPPER(B.DARI) LIKE UPPER('%".$q."%') OR UPPER(B.KEPADA) 
										LIKE UPPER('%".$q."%') OR UPPER(B.NO_SURAT) LIKE UPPER('%".$q."%') OR UPPER(B.CATATAN) LIKE UPPER('%".$q."%') 
										OR UPPER(B.TEMBUSAN) LIKE UPPER('%".$q."%') OR UPPER(A.ISI_RINGKAS) LIKE UPPER('%".$q."%') OR UPPER(A.ISI_DISPOSISI) LIKE 
										UPPER('%".$q."%') OR TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE UPPER('%".$q."%')
									)";
		return $this->tnde->query($sql)->row_array();
	}
	
	function get_distribusi_cari_by_nim_tgl_akhir_limit($kd_status_distribusi='', $nim='', $q='', $tgl_akhir, $limit=''){
		$sql = "SELECT * FROM (SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, B.ID_SURAT_KELUAR, A.ID_GRUP_SURAT, A.PEMROSES_SURAT, 
																	A.JABATAN_PEMROSES, A.KD_STATUS_DISTRIBUSI, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') WAKTU_KIRIM, 
																	TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') WAKTU_PENYELESAIAN, A.ID_STATUS_SM, TO_CHAR(A.WAKTU_AKSES, 
																	'DD/MM/YYYY HH24:,I:SS') WAKTU_AKSES, A.ISI_RINGKAS, A.ISI_DISPOSISI, A.ID_DS_SEBELUMNYA, A.KD_GRUP_TUJUAN, 
																	TO_CHAR(B.TGL_MASUK, 'DD/MM/YYYY') TGL_MASUK, B.INDEKS_BERKAS, B.KODE, B.PERIHAL, B.DARI, B.KEPADA, TO_CHAR(B.TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT, B.NO_SURAT, B.CATATAN, B.KD_KEAMANAN_SURAT, B.KD_SIFAT_SURAT, 
																	B.PENCATAT_SURAT, TO_CHAR(B.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_PENCATATAN,
																	B.PENGARAH_SURAT, B.JABATAN_PENGARAH, B.ID_STATUS_SM AS ID_STATUS_SM2, TO_CHAR(B.WAKTU_AKSES, 
																	'DD/MM/YYYY HH24:MI:SS') WAKTU_AKSES2, B.LAMPIRAN, B.TEMBUSAN, B.UNIT_ID, B.GMU_SLUG, C.NM_STATUS_SM  
														FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B, MD_STATUS_SM C 
														WHERE A.PENERIMA_SURAT='".$nim."' AND A.KD_GRUP_TUJUAN='MHS01' AND 
																		B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND A.KD_STATUS_DISTRIBUSI IN(".$kd_status_distribusi.") AND C.ID_STATUS_SM=A.ID_STATUS_SM
																		AND A.WAKTU_KIRIM <= TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND 
																		(TO_CHAR(B.TGL_MASUK, 'DD/MM/YYYY') LIKE '%".$q."%' OR TO_CHAR(B.TGL_SURAT, 'DD/MM/YYYY') LIKE '%".$q."%' 
																			OR UPPER(B.PERIHAL) LIKE UPPER('%".$q."%') OR UPPER(B.INDEKS_BERKAS) LIKE UPPER('%".$q."%') OR UPPER(B.DARI) 
																			LIKE UPPER('%".$q."%') OR UPPER(B.KEPADA) LIKE UPPER('%".$q."%') OR UPPER(B.NO_SURAT) LIKE UPPER('%".$q."%') OR 
																			UPPER(B.CATATAN) LIKE UPPER('%".$q."%') OR UPPER(B.TEMBUSAN) LIKE UPPER('%".$q."%') OR UPPER(A.ISI_RINGKAS) 
																			LIKE UPPER('%".$q."%') OR UPPER(A.ISI_DISPOSISI) LIKE UPPER('%".$q."%') OR 
																			TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE UPPER('%".$q."%')
																		)																		
																		ORDER BY A.WAKTU_KIRIM DESC
													) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_distribusi_cari_by_nim_tgl_akhir_paging($kd_status_distribusi='', $nim='', $q='', $tgl_akhir, $limit='', $page=''){
			$sql = "SELECT * FROM (SELECT ROWNUM R, ID_DISTRIBUSI_SURAT, ID_SURAT_MASUK, ID_SURAT_KELUAR, ID_GRUP_SURAT, PEMROSES_SURAT, 	
																		JABATAN_PEMROSES, KD_STATUS_DISTRIBUSI, WAKTU_KIRIM,WAKTU_PENYELESAIAN, ID_STATUS_SM, 
																		WAKTU_AKSES, ISI_RINGKAS, ISI_DISPOSISI, ID_DS_SEBELUMNYA, KD_GRUP_TUJUAN, TGL_MASUK, INDEKS_BERKAS, KODE, 
																		PERIHAL, DARI, KEPADA, TGL_SURAT, NO_SURAT, CATATAN, KD_KEAMANAN_SURAT, KD_SIFAT_SURAT, PENCATAT_SURAT, 
																		WAKTU_PENCATATAN, PENGARAH_SURAT, JABATAN_PENGARAH, ID_STATUS_SM2, WAKTU_AKSES2, LAMPIRAN, 
																		TEMBUSAN, UNIT_ID, GMU_SLUG, NM_STATUS_SM  
															FROM (SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, B.ID_SURAT_KELUAR, A.ID_GRUP_SURAT, A.PEMROSES_SURAT, 
																							A.JABATAN_PEMROSES, A.KD_STATUS_DISTRIBUSI, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') 
																							WAKTU_KIRIM, TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') WAKTU_PENYELESAIAN, A.ID_STATUS_SM, 
																							TO_CHAR(A.WAKTU_AKSES, 'DD/MM/YYYY HH24:,I:SS') WAKTU_AKSES, A.ISI_RINGKAS, A.ISI_DISPOSISI, 
																							A.ID_DS_SEBELUMNYA, A.KD_GRUP_TUJUAN, TO_CHAR(B.TGL_MASUK, 'DD/MM/YYYY') TGL_MASUK, 
																							B.INDEKS_BERKAS, B.KODE, B.PERIHAL, B.DARI, B.KEPADA, TO_CHAR(B.TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT,
																							B.NO_SURAT, B.CATATAN, B.KD_KEAMANAN_SURAT, B.KD_SIFAT_SURAT, B.PENCATAT_SURAT, 
																							TO_CHAR(B.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_PENCATATAN,
																							B.PENGARAH_SURAT, B.JABATAN_PENGARAH, B.ID_STATUS_SM AS ID_STATUS_SM2, TO_CHAR(B.WAKTU_AKSES, 
																							'DD/MM/YYYY HH24:MI:SS') WAKTU_AKSES2, B.LAMPIRAN, B.TEMBUSAN, B.UNIT_ID, B.GMU_SLUG, C.NM_STATUS_SM  
																			FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B, MD_STATUS_SM C  
																			WHERE A.PENERIMA_SURAT='".$nim."' AND A.KD_GRUP_TUJUAN='MHS01' AND 
																							B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND A.KD_STATUS_DISTRIBUSI IN(".$kd_status_distribusi.") AND 
																							C.ID_STATUS_SM=A.ID_STATUS_SM AND 
																							A.WAKTU_KIRIM <= TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND 
																							(TO_CHAR(B.TGL_MASUK, 'DD/MM/YYYY') LIKE '%".$q."%' OR TO_CHAR(B.TGL_SURAT, 'DD/MM/YYYY') LIKE '%".$q."%' 
																								OR UPPER(B.PERIHAL) LIKE UPPER('%".$q."%') OR UPPER(B.INDEKS_BERKAS) LIKE UPPER('%".$q."%') 
																								OR UPPER(B.DARI) LIKE UPPER('%".$q."%') OR UPPER(B.KEPADA) LIKE UPPER('%".$q."%') OR 
																								UPPER(B.NO_SURAT) LIKE UPPER('%".$q."%') OR UPPER(B.CATATAN) LIKE UPPER('%".$q."%') OR 
																								UPPER(B.TEMBUSAN) LIKE UPPER('%".$q."%') OR UPPER(A.ISI_RINGKAS) 
																								LIKE UPPER('%".$q."%') OR UPPER(A.ISI_DISPOSISI) LIKE UPPER('%".$q."%') OR 
																								TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE UPPER('%".$q."%')
																							)
																							ORDER BY A.WAKTU_KIRIM DESC
																		) WHERE ROWNUM <= ".$limit."
														) WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();
	}
	
	function total_distribusi_cari_by_nim_periode($kd_status_distribusi='', $nim='', $q='', $tgl_awal='', $tgl_akhir=''){
		$sql = "SELECT COUNT(A.ID_DISTRIBUSI_SURAT) TOTAL 
					 FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B 
                     WHERE A.PENERIMA_SURAT='".$nim."' AND A.KD_GRUP_TUJUAN='MHS01' AND 
                                    B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND A.KD_STATUS_DISTRIBUSI IN(".$kd_status_distribusi.") AND 
									A.WAKTU_KIRIM >= TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND 
									A.WAKTU_KIRIM <= TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') 
									AND 
									(TO_CHAR(B.TGL_MASUK, 'DD/MM/YYYY') LIKE '%".$q."%' OR TO_CHAR(B.TGL_SURAT, 'DD/MM/YYYY') LIKE '%".$q."%' OR UPPER(B.PERIHAL) 
										LIKE UPPER('%".$q."%') OR UPPER(B.INDEKS_BERKAS) LIKE UPPER('%".$q."%') OR UPPER(B.DARI) LIKE UPPER('%".$q."%') OR UPPER(B.KEPADA) 
										LIKE UPPER('%".$q."%') OR UPPER(B.NO_SURAT) LIKE UPPER('%".$q."%') OR UPPER(B.CATATAN) LIKE UPPER('%".$q."%') 
										OR UPPER(B.TEMBUSAN) LIKE UPPER('%".$q."%') OR UPPER(A.ISI_RINGKAS) LIKE UPPER('%".$q."%') OR UPPER(A.ISI_DISPOSISI) LIKE 
										UPPER('%".$q."%') OR TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE UPPER('%".$q."%')
									)";
		return $this->tnde->query($sql)->row_array();
	}
	
	function get_distribusi_cari_by_nim_periode_limit($kd_status_distribusi='', $nim='', $q='', $tgl_awal='', $tgl_akhir='', $limit=''){
		$sql = "SELECT * FROM (SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, B.ID_SURAT_KELUAR, A.ID_GRUP_SURAT, A.PEMROSES_SURAT, 
																	A.JABATAN_PEMROSES, A.KD_STATUS_DISTRIBUSI, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') WAKTU_KIRIM, 
																	TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') WAKTU_PENYELESAIAN, A.ID_STATUS_SM, TO_CHAR(A.WAKTU_AKSES, 
																	'DD/MM/YYYY HH24:,I:SS') WAKTU_AKSES, A.ISI_RINGKAS, A.ISI_DISPOSISI, A.ID_DS_SEBELUMNYA, A.KD_GRUP_TUJUAN, 
																	TO_CHAR(B.TGL_MASUK, 'DD/MM/YYYY') TGL_MASUK, B.INDEKS_BERKAS, B.KODE, B.PERIHAL, B.DARI, B.KEPADA, TO_CHAR(B.TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT, B.NO_SURAT, B.CATATAN, B.KD_KEAMANAN_SURAT, B.KD_SIFAT_SURAT, 
																	B.PENCATAT_SURAT, TO_CHAR(B.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_PENCATATAN,
																	B.PENGARAH_SURAT, B.JABATAN_PENGARAH, B.ID_STATUS_SM AS ID_STATUS_SM2, TO_CHAR(B.WAKTU_AKSES, 
																	'DD/MM/YYYY HH24:MI:SS') WAKTU_AKSES2, B.LAMPIRAN, B.TEMBUSAN, B.UNIT_ID, B.GMU_SLUG, C.NM_STATUS_SM  
														FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B, MD_STATUS_SM C 
														WHERE A.PENERIMA_SURAT='".$nim."' AND A.KD_GRUP_TUJUAN='MHS01' AND 
																		B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND A.KD_STATUS_DISTRIBUSI IN(".$kd_status_distribusi.") AND C.ID_STATUS_SM=A.ID_STATUS_SM
																		AND A.WAKTU_KIRIM >= TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND 
																		A.WAKTU_KIRIM <= TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND 
																		(TO_CHAR(B.TGL_MASUK, 'DD/MM/YYYY') LIKE '%".$q."%' OR TO_CHAR(B.TGL_SURAT, 'DD/MM/YYYY') LIKE '%".$q."%' 
																			OR UPPER(B.PERIHAL) LIKE UPPER('%".$q."%') OR UPPER(B.INDEKS_BERKAS) LIKE UPPER('%".$q."%') OR UPPER(B.DARI) 
																			LIKE UPPER('%".$q."%') OR UPPER(B.KEPADA) LIKE UPPER('%".$q."%') OR UPPER(B.NO_SURAT) LIKE UPPER('%".$q."%') OR 
																			UPPER(B.CATATAN) LIKE UPPER('%".$q."%') OR UPPER(B.TEMBUSAN) LIKE UPPER('%".$q."%') OR UPPER(A.ISI_RINGKAS) 
																			LIKE UPPER('%".$q."%') OR UPPER(A.ISI_DISPOSISI) LIKE UPPER('%".$q."%') OR 
																			TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE UPPER('%".$q."%')
																		)																		
																		ORDER BY A.WAKTU_KIRIM DESC
													) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_distribusi_cari_by_nim_periode_paging($kd_status_distribusi='', $nim='', $q='', $tgl_awal, $tgl_akhir, $limit='', $page=''){
			$sql = "SELECT * FROM (SELECT ROWNUM R, ID_DISTRIBUSI_SURAT, ID_SURAT_MASUK, ID_SURAT_KELUAR, ID_GRUP_SURAT, PEMROSES_SURAT, 	
																		JABATAN_PEMROSES, KD_STATUS_DISTRIBUSI, WAKTU_KIRIM,WAKTU_PENYELESAIAN, ID_STATUS_SM, 
																		WAKTU_AKSES, ISI_RINGKAS, ISI_DISPOSISI, ID_DS_SEBELUMNYA, KD_GRUP_TUJUAN, TGL_MASUK, INDEKS_BERKAS, KODE, 
																		PERIHAL, DARI, KEPADA, TGL_SURAT, NO_SURAT, CATATAN, KD_KEAMANAN_SURAT, KD_SIFAT_SURAT, PENCATAT_SURAT, 
																		WAKTU_PENCATATAN, PENGARAH_SURAT, JABATAN_PENGARAH, ID_STATUS_SM2, WAKTU_AKSES2, LAMPIRAN, 
																		TEMBUSAN, UNIT_ID, GMU_SLUG, NM_STATUS_SM  
															FROM (SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, B.ID_SURAT_KELUAR, A.ID_GRUP_SURAT, A.PEMROSES_SURAT, 
																							A.JABATAN_PEMROSES, A.KD_STATUS_DISTRIBUSI, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') 
																							WAKTU_KIRIM, TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') WAKTU_PENYELESAIAN, A.ID_STATUS_SM, 
																							TO_CHAR(A.WAKTU_AKSES, 'DD/MM/YYYY HH24:,I:SS') WAKTU_AKSES, A.ISI_RINGKAS, A.ISI_DISPOSISI, 
																							A.ID_DS_SEBELUMNYA, A.KD_GRUP_TUJUAN, TO_CHAR(B.TGL_MASUK, 'DD/MM/YYYY') TGL_MASUK, 
																							B.INDEKS_BERKAS, B.KODE, B.PERIHAL, B.DARI, B.KEPADA, TO_CHAR(B.TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT,
																							B.NO_SURAT, B.CATATAN, B.KD_KEAMANAN_SURAT, B.KD_SIFAT_SURAT, B.PENCATAT_SURAT, 
																							TO_CHAR(B.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_PENCATATAN,
																							B.PENGARAH_SURAT, B.JABATAN_PENGARAH, B.ID_STATUS_SM AS ID_STATUS_SM2, TO_CHAR(B.WAKTU_AKSES, 
																							'DD/MM/YYYY HH24:MI:SS') WAKTU_AKSES2, B.LAMPIRAN, B.TEMBUSAN, B.UNIT_ID, B.GMU_SLUG, C.NM_STATUS_SM  
																			FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B, MD_STATUS_SM C  
																			WHERE A.PENERIMA_SURAT='".$nim."' AND A.KD_GRUP_TUJUAN='MHS01' AND 
																							B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND A.KD_STATUS_DISTRIBUSI IN(".$kd_status_distribusi.") AND 
																							C.ID_STATUS_SM=A.ID_STATUS_SM AND A.WAKTU_KIRIM >= TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND 
																							A.WAKTU_KIRIM <= TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND 
																							(TO_CHAR(B.TGL_MASUK, 'DD/MM/YYYY') LIKE '%".$q."%' OR TO_CHAR(B.TGL_SURAT, 'DD/MM/YYYY') LIKE '%".$q."%' 
																								OR UPPER(B.PERIHAL) LIKE UPPER('%".$q."%') OR UPPER(B.INDEKS_BERKAS) LIKE UPPER('%".$q."%') 
																								OR UPPER(B.DARI) LIKE UPPER('%".$q."%') OR UPPER(B.KEPADA) LIKE UPPER('%".$q."%') OR 
																								UPPER(B.NO_SURAT) LIKE UPPER('%".$q."%') OR UPPER(B.CATATAN) LIKE UPPER('%".$q."%') OR 
																								UPPER(B.TEMBUSAN) LIKE UPPER('%".$q."%') OR UPPER(A.ISI_RINGKAS) 
																								LIKE UPPER('%".$q."%') OR UPPER(A.ISI_DISPOSISI) LIKE UPPER('%".$q."%') OR 
																								TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE UPPER('%".$q."%')
																							)
																							ORDER BY A.WAKTU_KIRIM DESC
																		) WHERE ROWNUM <= ".$limit."
														) WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();
	}
	
	function total_distribusi_cari_by_nim($kd_status_distribusi='', $nim='', $q=''){
		$sql = "SELECT COUNT(A.ID_DISTRIBUSI_SURAT) TOTAL 
					 FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B 
                     WHERE A.PENERIMA_SURAT='".$nim."' AND A.KD_GRUP_TUJUAN='MHS01' AND 
                                    B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND A.KD_STATUS_DISTRIBUSI IN(".$kd_status_distribusi.")
									AND 
									(TO_CHAR(B.TGL_MASUK, 'DD/MM/YYYY') LIKE '%".$q."%' OR TO_CHAR(B.TGL_SURAT, 'DD/MM/YYYY') LIKE '%".$q."%' OR UPPER(B.PERIHAL) 
										LIKE UPPER('%".$q."%') OR UPPER(B.INDEKS_BERKAS) LIKE UPPER('%".$q."%') OR UPPER(B.DARI) LIKE UPPER('%".$q."%') OR UPPER(B.KEPADA) 
										LIKE UPPER('%".$q."%') OR UPPER(B.NO_SURAT) LIKE UPPER('%".$q."%') OR UPPER(B.CATATAN) LIKE UPPER('%".$q."%') 
										OR UPPER(B.TEMBUSAN) LIKE UPPER('%".$q."%') OR UPPER(A.ISI_RINGKAS) LIKE UPPER('%".$q."%') OR UPPER(A.ISI_DISPOSISI) LIKE 
										UPPER('%".$q."%') OR TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE UPPER('%".$q."%')
									)";
		return $this->tnde->query($sql)->row_array();
	}
	
	function get_distribusi_cari_by_nim_limit($kd_status_distribusi='', $nim='', $q='', $limit=''){
		$sql = "SELECT * FROM (SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, B.ID_SURAT_KELUAR, A.ID_GRUP_SURAT, A.PEMROSES_SURAT, 
																	A.JABATAN_PEMROSES, A.KD_STATUS_DISTRIBUSI, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') WAKTU_KIRIM, 
																	TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') WAKTU_PENYELESAIAN, A.ID_STATUS_SM, TO_CHAR(A.WAKTU_AKSES, 
																	'DD/MM/YYYY HH24:,I:SS') WAKTU_AKSES, A.ISI_RINGKAS, A.ISI_DISPOSISI, A.ID_DS_SEBELUMNYA, A.KD_GRUP_TUJUAN, 
																	TO_CHAR(B.TGL_MASUK, 'DD/MM/YYYY') TGL_MASUK, B.INDEKS_BERKAS, B.KODE, B.PERIHAL, B.DARI, B.KEPADA, TO_CHAR(B.TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT, B.NO_SURAT, B.CATATAN, B.KD_KEAMANAN_SURAT, B.KD_SIFAT_SURAT, 
																	B.PENCATAT_SURAT, TO_CHAR(B.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_PENCATATAN,
																	B.PENGARAH_SURAT, B.JABATAN_PENGARAH, B.ID_STATUS_SM AS ID_STATUS_SM2, TO_CHAR(B.WAKTU_AKSES, 
																	'DD/MM/YYYY HH24:MI:SS') WAKTU_AKSES2, B.LAMPIRAN, B.TEMBUSAN, B.UNIT_ID, B.GMU_SLUG, C.NM_STATUS_SM  
														FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B, MD_STATUS_SM C 
														WHERE A.PENERIMA_SURAT='".$nim."' AND A.KD_GRUP_TUJUAN='MHS01' AND 
																		B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND A.KD_STATUS_DISTRIBUSI IN(".$kd_status_distribusi.") AND C.ID_STATUS_SM=A.ID_STATUS_SM
																		AND 
																		(TO_CHAR(B.TGL_MASUK, 'DD/MM/YYYY') LIKE '%".$q."%' OR TO_CHAR(B.TGL_SURAT, 'DD/MM/YYYY') LIKE '%".$q."%' 
																			OR UPPER(B.PERIHAL) LIKE UPPER('%".$q."%') OR UPPER(B.INDEKS_BERKAS) LIKE UPPER('%".$q."%') OR UPPER(B.DARI) 
																			LIKE UPPER('%".$q."%') OR UPPER(B.KEPADA) LIKE UPPER('%".$q."%') OR UPPER(B.NO_SURAT) LIKE UPPER('%".$q."%') OR 
																			UPPER(B.CATATAN) LIKE UPPER('%".$q."%') OR UPPER(B.TEMBUSAN) LIKE UPPER('%".$q."%') OR UPPER(A.ISI_RINGKAS) 
																			LIKE UPPER('%".$q."%') OR UPPER(A.ISI_DISPOSISI) LIKE UPPER('%".$q."%') OR 
																			TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE UPPER('%".$q."%')
																		)																		
																		ORDER BY A.WAKTU_KIRIM DESC
													) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_distribusi_cari_by_nim_paging($kd_status_distribusi='', $nim='', $q='', $limit='', $page=''){
			$sql = "SELECT * FROM (SELECT ROWNUM R, ID_DISTRIBUSI_SURAT, ID_SURAT_MASUK, ID_SURAT_KELUAR, ID_GRUP_SURAT, PEMROSES_SURAT, 	
																		JABATAN_PEMROSES, KD_STATUS_DISTRIBUSI, WAKTU_KIRIM,WAKTU_PENYELESAIAN, ID_STATUS_SM, 
																		WAKTU_AKSES, ISI_RINGKAS, ISI_DISPOSISI, ID_DS_SEBELUMNYA, KD_GRUP_TUJUAN, TGL_MASUK, INDEKS_BERKAS, KODE, 
																		PERIHAL, DARI, KEPADA, TGL_SURAT, NO_SURAT, CATATAN, KD_KEAMANAN_SURAT, KD_SIFAT_SURAT, PENCATAT_SURAT, 
																		WAKTU_PENCATATAN, PENGARAH_SURAT, JABATAN_PENGARAH, ID_STATUS_SM2, WAKTU_AKSES2, LAMPIRAN, 
																		TEMBUSAN, UNIT_ID, GMU_SLUG, NM_STATUS_SM  
															FROM (SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, B.ID_SURAT_KELUAR, A.ID_GRUP_SURAT, A.PEMROSES_SURAT, 
																							A.JABATAN_PEMROSES, A.KD_STATUS_DISTRIBUSI, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') 
																							WAKTU_KIRIM, TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') WAKTU_PENYELESAIAN, A.ID_STATUS_SM, 
																							TO_CHAR(A.WAKTU_AKSES, 'DD/MM/YYYY HH24:,I:SS') WAKTU_AKSES, A.ISI_RINGKAS, A.ISI_DISPOSISI, 
																							A.ID_DS_SEBELUMNYA, A.KD_GRUP_TUJUAN, TO_CHAR(B.TGL_MASUK, 'DD/MM/YYYY') TGL_MASUK, 
																							B.INDEKS_BERKAS, B.KODE, B.PERIHAL, B.DARI, B.KEPADA, TO_CHAR(B.TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT,
																							B.NO_SURAT, B.CATATAN, B.KD_KEAMANAN_SURAT, B.KD_SIFAT_SURAT, B.PENCATAT_SURAT, 
																							TO_CHAR(B.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_PENCATATAN,
																							B.PENGARAH_SURAT, B.JABATAN_PENGARAH, B.ID_STATUS_SM AS ID_STATUS_SM2, TO_CHAR(B.WAKTU_AKSES, 
																							'DD/MM/YYYY HH24:MI:SS') WAKTU_AKSES2, B.LAMPIRAN, B.TEMBUSAN, B.UNIT_ID, B.GMU_SLUG, C.NM_STATUS_SM  
																			FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B, MD_STATUS_SM C  
																			WHERE A.PENERIMA_SURAT='".$nim."' AND A.KD_GRUP_TUJUAN='MHS01' AND 
																							B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND A.KD_STATUS_DISTRIBUSI IN(".$kd_status_distribusi.") AND 
																							C.ID_STATUS_SM=A.ID_STATUS_SM AND 
																							(TO_CHAR(B.TGL_MASUK, 'DD/MM/YYYY') LIKE '%".$q."%' OR TO_CHAR(B.TGL_SURAT, 'DD/MM/YYYY') LIKE '%".$q."%' 
																								OR UPPER(B.PERIHAL) LIKE UPPER('%".$q."%') OR UPPER(B.INDEKS_BERKAS) LIKE UPPER('%".$q."%') 
																								OR UPPER(B.DARI) LIKE UPPER('%".$q."%') OR UPPER(B.KEPADA) LIKE UPPER('%".$q."%') OR 
																								UPPER(B.NO_SURAT) LIKE UPPER('%".$q."%') OR UPPER(B.CATATAN) LIKE UPPER('%".$q."%') OR 
																								UPPER(B.TEMBUSAN) LIKE UPPER('%".$q."%') OR UPPER(A.ISI_RINGKAS) 
																								LIKE UPPER('%".$q."%') OR UPPER(A.ISI_DISPOSISI) LIKE UPPER('%".$q."%') OR 
																								TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE UPPER('%".$q."%')
																							)
																							ORDER BY A.WAKTU_KIRIM DESC
																		) WHERE ROWNUM <= ".$limit."
														) WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();
	}
}
?>