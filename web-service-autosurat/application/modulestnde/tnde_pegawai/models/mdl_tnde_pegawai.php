<?php
if(! defined('BASEPATH'))exit('No direct script access allowed');
class Mdl_tnde_pegawai extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->tnde = $this->load->database('tnde', TRUE);
	}
	
	function tot_disposisi_by_jab_pemroses($id_surat_masuk, $kd_jabatan){
		$sql = "SELECT COUNT(*) AS TOTAL FROM D_DISTRIBUSI_SURAT WHERE ID_SURAT_MASUK='".$id_surat_masuk."' AND JABATAN_PEMROSES='".$kd_jabatan."' AND KD_STATUS_DISTRIBUSI='D'";
		return $this->tnde->query($sql)->row_array();
	}
	
	function tot_ds_by_kd_pegawai($id_surat_masuk, $kd_pegawai, $kd_status_distribusi){
		$sql = "SELECT COUNT(*) AS TOTAL FROM D_DISTRIBUSI_SURAT WHERE ID_SURAT_MASUK='".$id_surat_masuk."' AND PEMROSES_SURAT='".$kd_pegawai."' AND KD_STATUS_DISTRIBUSI='".$kd_status_distribusi."'";
		return $this->tnde->query($sql)->row_array();
	}
	
	function tot_ds_by_kd_jabatan($id_surat_masuk, $kd_jabatan, $kd_status_distribusi){
		$sql = "SELECT COUNT(*) AS TOTAL FROM D_DISTRIBUSI_SURAT WHERE ID_SURAT_MASUK='".$id_surat_masuk."' AND JABATAN_PEMROSES='".$kd_jabatan."' AND KD_STATUS_DISTRIBUSI='".$kd_status_distribusi."'";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_ts_by_kd_jabatan($kd_jabatan){
		$data['JABATAN_PENERIMA'] = $kd_jabatan;
		$data['KD_STATUS_DISTRIBUSI'] = 'TS';
		$this->tnde->where($data);
		$this->tnde->from('D_DISTRIBUSI_SURAT');
		return $this->tnde->count_all_results();
	}
	
	function total_ts_by_kd_jabatan_tgl_awal($kd_jabatan, $perihal, $tgl_awal){
		$sql = "SELECT COUNT(A.ID_DISTRIBUSI_SURAT) AS TOTAL FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B WHERE A.JABATAN_PENERIMA='".$kd_jabatan."' AND A.WAKTU_KIRIM >= TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND A.KD_STATUS_DISTRIBUSI='TS' AND UPPER(B.PERIHAL) LIKE '%".$perihal."%' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_ts_by_kd_jabatan_tgl_akhir($kd_jabatan, $perihal, $tgl_akhir){
		$sql = "SELECT COUNT(A.ID_DISTRIBUSI_SURAT) AS TOTAL FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B WHERE A.JABATAN_PENERIMA='".$kd_jabatan."' AND A.WAKTU_KIRIM <= TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND A.KD_STATUS_DISTRIBUSI='TS' AND UPPER(B.PERIHAL) LIKE '%".$perihal."%' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_ts_by_kd_jabatan_periode($kd_jabatan, $perihal, $tgl_awal, $tgl_akhir){
		$sql = "SELECT COUNT(A.ID_DISTRIBUSI_SURAT) AS TOTAL FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B WHERE A.JABATAN_PENERIMA='".$kd_jabatan."' AND A.WAKTU_KIRIM BETWEEN TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND A.KD_STATUS_DISTRIBUSI='TS' AND UPPER(B.PERIHAL) LIKE '%".$perihal."%' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_ts_by_kd_jabatan_perihal($kd_jabatan, $perihal){
		$perihal	= strtoupper($perihal);
		$sql = "SELECT COUNT(A.ID_DISTRIBUSI_SURAT) AS TOTAL 
					FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B 
					WHERE A.JABATAN_PENERIMA='".$kd_jabatan."' AND A.KD_STATUS_DISTRIBUSI='TS' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND 
								(UPPER(B.PERIHAL) LIKE '%".$perihal."%' OR UPPER(B.DARI) LIKE '%".$perihal."%' OR UPPER(B.NO_SURAT) LIKE '%".$perihal."%' OR 
								TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$perihal."%')";
		return $this->tnde->query($sql)->row_array();	
	}
	
	function total_sm_by_kd_jabatan($kd_jabatan){
		$data['JABATAN_PENERIMA'] = $kd_jabatan;
		$data['KD_STATUS_DISTRIBUSI'] = 'PS';
		$this->tnde->where($data);
		$this->tnde->from('D_DISTRIBUSI_SURAT');
		return $this->tnde->count_all_results();
	}
	
	function total_sm_by_kd_jabatan_tgl_awal($kd_jabatan, $perihal, $tgl_awal){
		$sql = "SELECT COUNT(A.ID_DISTRIBUSI_SURAT) AS TOTAL FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B 
		WHERE A.JABATAN_PENERIMA='".$kd_jabatan."' AND A.WAKTU_KIRIM >= TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND 
		A.KD_STATUS_DISTRIBUSI='PS' AND (UPPER(B.PERIHAL) LIKE '%".$perihal."%' OR UPPER(B.NO_SURAT) LIKE '%".$perihal."%' OR 
		TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$perihal."%' OR UPPER(B.DARI) LIKE '%".$perihal."%') 
		AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_sm_by_kd_jabatan_tgl_akhir($kd_jabatan, $perihal, $tgl_akhir){
		$sql = "SELECT COUNT(A.ID_DISTRIBUSI_SURAT) AS TOTAL FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B 
		WHERE A.JABATAN_PENERIMA='".$kd_jabatan."' AND A.WAKTU_KIRIM <= TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') 
		AND A.KD_STATUS_DISTRIBUSI='PS' AND (UPPER(B.PERIHAL) LIKE '%".$perihal."%' OR UPPER(B.NO_SURAT) LIKE '%".$perihal."%' OR 
		TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$perihal."%' OR UPPER(B.DARI) LIKE '%".$perihal."%') 
		AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK";
		return $this->tnde->query($sql)->row_array();	
	}
	
	function total_sm_by_kd_jabatan_periode($kd_jabatan, $perihal, $tgl_awal, $tgl_akhir){
		$sql = "SELECT COUNT(A.ID_DISTRIBUSI_SURAT) AS TOTAL FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B 
		WHERE A.JABATAN_PENERIMA='".$kd_jabatan."' AND A.WAKTU_KIRIM BETWEEN TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND 
		TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND A.KD_STATUS_DISTRIBUSI='PS' AND 
		(UPPER(B.PERIHAL) LIKE '%".$perihal."%' OR UPPER(B.NO_SURAT) LIKE '%".$perihal."%' OR 
		TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$perihal."%' OR UPPER(B.DARI) LIKE '%".$perihal."%') 
		AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_sm_by_kd_jabatan_perihal($kd_jabatan, $perihal){
		$sql = "SELECT COUNT(A.ID_DISTRIBUSI_SURAT) AS TOTAL FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B 
		WHERE A.JABATAN_PENERIMA='".$kd_jabatan."' AND A.KD_STATUS_DISTRIBUSI='PS' AND 
		(UPPER(B.PERIHAL) LIKE '%".$perihal."%' OR UPPER(B.NO_SURAT) LIKE '%".$perihal."%' OR 
		TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$perihal."%' OR UPPER(B.DARI) LIKE '%".$perihal."%') 
		AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_dm_by_kd_jabatan($kd_jabatan){
		$data['JABATAN_PENERIMA'] = $kd_jabatan;
		$data['KD_STATUS_DISTRIBUSI'] = 'D';
		$this->tnde->where($data);
		$this->tnde->from('D_DISTRIBUSI_SURAT');
		return $this->tnde->count_all_results();
	}
	
	function total_dm_by_kd_jabatan_tgl_awal($kd_jabatan, $perihal, $tgl_awal){
		$sql = "SELECT COUNT(A.ID_DISTRIBUSI_SURAT) AS TOTAL FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B WHERE A.JABATAN_PENERIMA='".$kd_jabatan."' AND A.WAKTU_KIRIM >= TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND A.KD_STATUS_DISTRIBUSI='D' AND UPPER(B.PERIHAL) LIKE '%".$perihal."%' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK";
		return $this->tnde->query($sql)->row_array();	
	}
	
	function total_dm_by_kd_jabatan_tgl_akhir($kd_jabatan, $perihal, $tgl_akhir){
		$sql = "SELECT COUNT(A.ID_DISTRIBUSI_SURAT) AS TOTAL FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B WHERE A.JABATAN_PENERIMA='".$kd_jabatan."' AND 
		A.WAKTU_KIRIM <= TO_DATE(' ".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND A.KD_STATUS_DISTRIBUSI='D' AND UPPER(B.PERIHAL) LIKE '%".$perihal."%' AND 
		B.ID_SURAT_MASUK=A.ID_SURAT_MASUK";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_dm_by_kd_jabatan_periode($kd_jabatan, $perihal, $tgl_awal, $tgl_akhir){
		$sql = "SELECT COUNT(A.ID_DISTRIBUSI_SURAT) AS TOTAL FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B WHERE A.JABATAN_PENERIMA='".$kd_jabatan."' AND 
		A.WAKTU_KIRIM BETWEEN TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND TO_DATE(' ".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND A.KD_STATUS_DISTRIBUSI='D' AND 
		UPPER(B.PERIHAL) LIKE '%".$perihal."%' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK";
		return $this->tnde->query($sql)->row_array();	
	}
	
	function total_dm_by_kd_jabatan_perihal($kd_jabatan, $perihal){
		$sql = "SELECT COUNT(A.ID_DISTRIBUSI_SURAT) AS TOTAL 
					FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B 
					WHERE A.JABATAN_PENERIMA='".$kd_jabatan."' AND A.KD_STATUS_DISTRIBUSI='D' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND 
								(UPPER(B.PERIHAL) LIKE '%".$perihal."%' OR UPPER(B.DARI) LIKE '%".$perihal."%' OR UPPER(B.NO_SURAT) LIKE '%".$perihal."%' OR 
								TO_CHAR(A.WAKTU_KIRIM) LIKE '%".$perihal."%')";
		return $this->tnde->query($sql)->row_array();	
	}
	
	function total_dm_by_nip($nip){
		$data['PENERIMA_SURAT'] = $nip;
		$data['KD_STATUS_DISTRIBUSI'] = 'D';
		$this->tnde->where($data);
		$this->tnde->from('D_DISTRIBUSI_SURAT');
		return $this->tnde->count_all_results();
	}
	
	function total_dm_by_nip_tgl_awal($nip, $perihal, $tgl_awal){
		$sql = "SELECT COUNT(A.ID_DISTRIBUSI_SURAT) AS TOTAL FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B WHERE A.PENERIMA_SURAT='".$nip."' AND A.WAKTU_KIRIM >= TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND A.KD_STATUS_DISTRIBUSI='D' AND UPPER(B.PERIHAL) LIKE '%".$perihal."%' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK";
		return $this->tnde->query($sql)->row_array();	
	}
	
	function total_dm_by_nip_tgl_akhir($nip, $perihal, $tgl_akhir){
		$sql = "SELECT COUNT(A.ID_DISTRIBUSI_SURAT) AS TOTAL FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B WHERE A.PENERIMA_SURAT='".$nip."' AND 
		A.WAKTU_KIRIM <= TO_DATE(' ".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND A.KD_STATUS_DISTRIBUSI='D' AND UPPER(B.PERIHAL) LIKE '%".$perihal."%' AND 
		B.ID_SURAT_MASUK=A.ID_SURAT_MASUK";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_dm_by_nip_periode($nip, $perihal, $tgl_awal, $tgl_akhir){
		$sql = "SELECT COUNT(A.ID_DISTRIBUSI_SURAT) AS TOTAL FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B WHERE A.PENERIMA_SURAT='".$nip."' AND 
		A.WAKTU_KIRIM BETWEEN TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND TO_DATE(' ".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND A.KD_STATUS_DISTRIBUSI='D' AND 
		UPPER(B.PERIHAL) LIKE '%".$perihal."%' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK";
		return $this->tnde->query($sql)->row_array();	
	}
	
	/*=========================================*/
	function total_dm_by_kd_pegawai($kd_pegawai){
		$data['PENERIMA_SURAT'] = $kd_pegawai;
		$data['KD_STATUS_DISTRIBUSI'] = 'D';
		$this->tnde->where($data);
		$this->tnde->from('D_DISTRIBUSI_SURAT');
		return $this->tnde->count_all_results();
	}
	
	function total_dm_by_kd_pegawai_tgl_awal($kd_pegawai, $perihal, $tgl_awal){
		$sql = "SELECT COUNT(A.ID_DISTRIBUSI_SURAT) AS TOTAL FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B WHERE A.PENERIMA_SURAT='".$kd_pegawai."' AND A.WAKTU_KIRIM >= TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND A.KD_STATUS_DISTRIBUSI='D' AND UPPER(B.PERIHAL) LIKE '%".$perihal."%' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_dm_by_kd_pegawai_tgl_akhir($kd_pegawai, $perihal, $tgl_akhir){
		$sql = "SELECT COUNT(A.ID_DISTRIBUSI_SURAT) AS TOTAL FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B WHERE A.PENERIMA_SURAT='".$kd_pegawai."' AND 
		A.WAKTU_KIRIM <= TO_DATE(' ".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND A.KD_STATUS_DISTRIBUSI='D' AND UPPER(B.PERIHAL) LIKE '%".$perihal."%' AND 
		B.ID_SURAT_MASUK=A.ID_SURAT_MASUK";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_dm_by_kd_pegawai_periode($kd_pegawai, $perihal, $tgl_awal, $tgl_akhir){
		$sql = "SELECT COUNT(A.ID_DISTRIBUSI_SURAT) AS TOTAL FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B WHERE A.PENERIMA_SURAT='".$kd_pegawai."' AND 
		A.WAKTU_KIRIM BETWEEN TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND TO_DATE(' ".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND A.KD_STATUS_DISTRIBUSI='D' AND 
		UPPER(B.PERIHAL) LIKE '%".$perihal."%' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK";
		return $this->tnde->query($sql)->row_array();	
	}
	
	function total_dm_by_kd_pegawai_perihal($kd_pegawai, $perihal){
		$perihal = strtoupper($perihal);
		$sql = "SELECT COUNT(A.ID_DISTRIBUSI_SURAT) AS TOTAL 
					FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B 
					WHERE A.PENERIMA_SURAT='".$kd_pegawai."' AND A.KD_STATUS_DISTRIBUSI='D' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND 
								(UPPER(B.PERIHAL) LIKE '%".$perihal."%' OR UPPER(B.DARI) LIKE '%".$perihal."%' OR UPPER(B.NO_SURAT) LIKE '%".$perihal."%' OR 
								TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$perihal."%')";
		return $this->tnde->query($sql)->row_array();	
	}
	/*=========================================*/
	
	function total_dk_grup_by_kd_jabatan($kd_jabatan){
		$sql = "SELECT COUNT(DISTINCT(ID_SURAT_MASUK)) AS TOTAL FROM D_DISTRIBUSI_SURAT WHERE KD_STATUS_DISTRIBUSI='D' AND JABATAN_PEMROSES='".$kd_jabatan."' ";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_dk_by_kd_jabatan($kd_jabatan){
		$data['JABATAN_PEMROSES'] = $kd_jabatan;
		$data['KD_STATUS_DISTRIBUSI'] = 'D';
		$this->tnde->where($data);
		$this->tnde->from('D_DISTRIBUSI_SURAT');
		return $this->tnde->count_all_results();
	}
	
	function total_dk_by_kd_jabatan_tgl_awal($kd_jabatan, $perihal, $tgl_awal){
		$sql = "SELECT COUNT(DISTINCT(A.ID_SURAT_MASUK)) AS TOTAL 
		FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B 
		WHERE A.KD_STATUS_DISTRIBUSI='D' AND A.JABATAN_PEMROSES='".$kd_jabatan."' AND A.WAKTU_KIRIM >= 
		TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND 
		(UPPER(B.PERIHAL) LIKE '%".$perihal."%' OR UPPER(B.NO_SURAT) LIKE '%".$perihal."%' OR UPPER(B.DARI) 
		LIKE '%".$perihal."%' OR TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$perihal."%' OR 
		TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') LIKE '%".$perihal."%') 
		AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_dk_by_kd_jabatan_tgl_akhir($kd_jabatan, $perihal, $tgl_akhir){
		$sql = "SELECT COUNT(DISTINCT(A.ID_SURAT_MASUK)) AS TOTAL 
		FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B 
		WHERE A.KD_STATUS_DISTRIBUSI='D' AND A.JABATAN_PEMROSES='".$kd_jabatan."' AND A.WAKTU_KIRIM <= 
		TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND 
		(UPPER(B.PERIHAL) LIKE '%".$perihal."%' OR UPPER(B.NO_SURAT) LIKE '%".$perihal."%' OR UPPER(B.DARI) 
		LIKE '%".$perihal."%' OR TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$perihal."%' OR 
		TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') LIKE '%".$perihal."%') AND 
		B.ID_SURAT_MASUK=A.ID_SURAT_MASUK";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_dk_by_kd_jabatan_periode($kd_jabatan, $perihal, $tgl_awal, $tgl_akhir){
		$sql = "SELECT COUNT(DISTINCT(A.ID_SURAT_MASUK)) AS TOTAL 
		FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B 
		WHERE A.KD_STATUS_DISTRIBUSI='D' AND A.JABATAN_PEMROSES='".$kd_jabatan."' AND A.WAKTU_KIRIM BETWEEN 
		TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') 
		AND (UPPER(B.PERIHAL) LIKE '%".$perihal."%' OR UPPER(B.NO_SURAT) LIKE '%".$perihal."%' OR UPPER(B.DARI) 
		LIKE '%".$perihal."%' OR TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$perihal."%' OR 
		TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') LIKE '%".$perihal."%') AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_dk_by_kd_jabatan_perihal($kd_jabatan, $perihal){
		$sql = "SELECT COUNT(DISTINCT(A.ID_SURAT_MASUK)) AS TOTAL 
		FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B 
		WHERE A.KD_STATUS_DISTRIBUSI='D' AND A.JABATAN_PEMROSES='".$kd_jabatan."' AND 
		(UPPER(B.PERIHAL) LIKE '%".$perihal."%' OR UPPER(B.NO_SURAT) LIKE '%".$perihal."%' OR UPPER(B.DARI) 
		LIKE '%".$perihal."%' OR TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$perihal."%' OR 
		TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') LIKE '%".$perihal."%') 
		AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK";
		return $this->tnde->query($sql)->row_array();
	}
	
	function get_ts_by_kd_jabatan_limit($kd_jabatan, $limit){
		$sql = "SELECT * FROM ( SELECT A.ID_DISTRIBUSI_SURAT, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, B.ID_STATUS_SM, B.NM_STATUS_SM, C.ID_SURAT_MASUK, C.NO_SURAT, C.DARI, C.PERIHAL FROM D_DISTRIBUSI_SURAT A, MD_STATUS_SM B, MD_SURAT_MASUK C WHERE A.JABATAN_PENERIMA='".$kd_jabatan."' AND A.KD_STATUS_DISTRIBUSI='TS' AND B.ID_STATUS_SM=A.ID_STATUS_SM AND C.ID_SURAT_MASUK=A.ID_SURAT_MASUK ORDER BY A.WAKTU_KIRIM DESC ) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_ts_by_kd_jabatan_paging($kd_jabatan, $limit, $page){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, WAKTU_PENCATATAN, ID_STATUS_SM, NM_STATUS_SM, ID_DISTRIBUSI_SURAT, WAKTU_KIRIM, ROWNUM R FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, B.ID_STATUS_SM, B.NM_STATUS_SM, C.ID_DISTRIBUSI_SURAT, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE C.JABATAN_PENERIMA='".$kd_jabatan."' AND C.KD_STATUS_DISTRIBUSI='TS' AND A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.") WHERE R > ".$page.")";
		return $this->tnde->query($sql)->result();
	}
	
	function get_ts_by_kd_jabatan_tgl_awal_limit($kd_jabatan, $limit, $perihal, $tgl_awal){
		$sql = "SELECT * FROM ( SELECT A.ID_DISTRIBUSI_SURAT, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, B.ID_STATUS_SM, B.NM_STATUS_SM, C.ID_SURAT_MASUK, C.NO_SURAT, C.DARI, C.PERIHAL FROM D_DISTRIBUSI_SURAT A, MD_STATUS_SM B, MD_SURAT_MASUK C WHERE A.JABATAN_PENERIMA='".$kd_jabatan."' AND A.KD_STATUS_DISTRIBUSI='TS' AND A.WAKTU_KIRIM >= TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND B.ID_STATUS_SM=A.ID_STATUS_SM AND C.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND UPPER(C.PERIHAL) LIKE '%".$perihal."%' ORDER BY A.WAKTU_KIRIM DESC ) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();	
	}
	
	function get_ts_by_kd_jabatan_tgl_awal_paging($kd_jabatan, $limit, $page, $perihal, $tgl_awal){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, WAKTU_PENCATATAN, ID_STATUS_SM, NM_STATUS_SM, ID_DISTRIBUSI_SURAT, WAKTU_KIRIM, ROWNUM R FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, B.ID_STATUS_SM, B.NM_STATUS_SM, C.ID_DISTRIBUSI_SURAT, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE C.JABATAN_PENERIMA='".$kd_jabatan."' AND C.KD_STATUS_DISTRIBUSI='TS' AND A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND UPPER(A.PERIHAL) LIKE '%".$perihal."%' AND B.ID_STATUS_SM=C.ID_STATUS_SM AND C.WAKTU_KIRIM >= TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.") WHERE R > ".$page.")";
		return $this->tnde->query($sql)->result();
	}
	
	function get_ts_by_kd_jabatan_tgl_akhir_limit($kd_jabatan, $limit, $perihal, $tgl_akhir){
		$sql = "SELECT * FROM ( SELECT A.ID_DISTRIBUSI_SURAT, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, B.ID_STATUS_SM, B.NM_STATUS_SM, C.ID_SURAT_MASUK, C.NO_SURAT, C.DARI, C.PERIHAL FROM D_DISTRIBUSI_SURAT A, MD_STATUS_SM B, MD_SURAT_MASUK C WHERE A.JABATAN_PENERIMA='".$kd_jabatan."' AND A.KD_STATUS_DISTRIBUSI='TS' AND A.WAKTU_KIRIM <= TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND B.ID_STATUS_SM=A.ID_STATUS_SM AND C.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND UPPER(C.PERIHAL) LIKE '%".$perihal."%' ORDER BY A.WAKTU_KIRIM DESC ) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();		
	}
	
	function get_ts_by_kd_jabatan_tgl_akhir_paging($kd_jabatan, $limit, $page, $perihal, $tgl_akhir){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, WAKTU_PENCATATAN, ID_STATUS_SM, NM_STATUS_SM, ID_DISTRIBUSI_SURAT, WAKTU_KIRIM, ROWNUM R FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, B.ID_STATUS_SM, B.NM_STATUS_SM, C.ID_DISTRIBUSI_SURAT, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE C.JABATAN_PENERIMA='".$kd_jabatan."' AND C.KD_STATUS_DISTRIBUSI='TS' AND A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND UPPER(A.PERIHAL) LIKE '%".$perihal."%' AND B.ID_STATUS_SM=C.ID_STATUS_SM AND C.WAKTU_KIRIM <= TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.") WHERE R > ".$page.")";
		return $this->tnde->query($sql)->result();
	}
	
	function get_ts_by_kd_jabatan_periode_limit($kd_jabatan, $limit, $perihal, $tgl_awal, $tgl_akhir){
		$sql = "SELECT * FROM ( SELECT A.ID_DISTRIBUSI_SURAT, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, B.ID_STATUS_SM, B.NM_STATUS_SM, C.ID_SURAT_MASUK, C.NO_SURAT, C.DARI, C.PERIHAL FROM D_DISTRIBUSI_SURAT A, MD_STATUS_SM B, MD_SURAT_MASUK C WHERE A.JABATAN_PENERIMA='".$kd_jabatan."' AND A.KD_STATUS_DISTRIBUSI='TS' AND A.WAKTU_KIRIM BETWEEN TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND B.ID_STATUS_SM=A.ID_STATUS_SM AND C.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND UPPER(C.PERIHAL) LIKE '%".$perihal."%' ORDER BY A.WAKTU_KIRIM DESC ) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_ts_by_kd_jabatan_periode_paging($kd_jabatan, $limit, $page, $perihal, $tgl_awal, $tgl_akhir){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, WAKTU_PENCATATAN, ID_STATUS_SM, NM_STATUS_SM, ID_DISTRIBUSI_SURAT, WAKTU_KIRIM, ROWNUM R FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, B.ID_STATUS_SM, B.NM_STATUS_SM, C.ID_DISTRIBUSI_SURAT, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE C.JABATAN_PENERIMA='".$kd_jabatan."' AND C.KD_STATUS_DISTRIBUSI='TS' AND A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND UPPER(A.PERIHAL) LIKE '%".$perihal."%' AND B.ID_STATUS_SM=C.ID_STATUS_SM AND C.WAKTU_KIRIM BETWEEN TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.") WHERE R > ".$page.")";
		return $this->tnde->query($sql)->result();	
	}
	
	function get_ts_by_kd_jabatan_perihal_limit($kd_jabatan, $limit, $perihal){
		$perihal = strtoupper($perihal);
		$sql = "SELECT * FROM ( SELECT A.ID_DISTRIBUSI_SURAT, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, B.ID_STATUS_SM, 
															B.NM_STATUS_SM, C.ID_SURAT_MASUK, C.NO_SURAT, C.DARI, C.PERIHAL 
												FROM D_DISTRIBUSI_SURAT A, MD_STATUS_SM B, MD_SURAT_MASUK C 
												WHERE A.JABATAN_PENERIMA='".$kd_jabatan."' AND A.KD_STATUS_DISTRIBUSI='TS' AND B.ID_STATUS_SM=A.ID_STATUS_SM AND 
															C.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND (UPPER(C.PERIHAL) LIKE '%".$perihal."%' OR UPPER(C.DARI) LIKE '%".$perihal."%' OR 
															UPPER(C.NO_SURAT) LIKE '%".$perihal."%' OR TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$perihal."%')  
															ORDER BY A.WAKTU_KIRIM DESC 
											 ) 
									WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}

	function get_ts_by_kd_jabatan_perihal_paging($kd_jabatan, $limit, $page, $perihal){
		$perihal = strtoupper($perihal);
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, WAKTU_PENCATATAN, ID_STATUS_SM, NM_STATUS_SM, 
																						ID_DISTRIBUSI_SURAT, WAKTU_KIRIM, ROWNUM R 
																			FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, 
																									A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, 
																									B.ID_STATUS_SM, B.NM_STATUS_SM, C.ID_DISTRIBUSI_SURAT, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') 
																									AS WAKTU_KIRIM 
																						FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C 
																						WHERE C.JABATAN_PENERIMA='".$kd_jabatan."' AND C.KD_STATUS_DISTRIBUSI='TS' AND A.ID_SURAT_MASUK=C.ID_SURAT_MASUK 
																										 AND B.ID_STATUS_SM=C.ID_STATUS_SM AND (UPPER(A.PERIHAL) LIKE '%".$perihal."%' OR UPPER(A.DARI) LIKE '%".$perihal."%' 
																										 OR UPPER(A.NO_SURAT) LIKE '%".$perihal."%' OR TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$perihal."%') 
																						ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.") WHERE R > ".$page.")";
		return $this->tnde->query($sql)->result();	
	}
	
	function get_sm_by_kd_jabatan_limit($kd_jabatan, $limit){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, B.ID_STATUS_SM, B.NM_STATUS_SM, C.ID_DISTRIBUSI_SURAT, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE C.JABATAN_PENERIMA='".$kd_jabatan."' AND C.KD_STATUS_DISTRIBUSI='PS' AND A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.") ";
		return $this->tnde->query($sql)->result();
	}
	
	function get_sm_by_kd_jabatan_paging($kd_jabatan, $limit, $page){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, WAKTU_PENCATATAN, ID_STATUS_SM, NM_STATUS_SM, ID_DISTRIBUSI_SURAT, WAKTU_KIRIM, ROWNUM R FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, B.ID_STATUS_SM, B.NM_STATUS_SM, C.ID_DISTRIBUSI_SURAT, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE C.JABATAN_PENERIMA='".$kd_jabatan."' AND C.KD_STATUS_DISTRIBUSI='PS' AND A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.") WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();	
	}
	
	function get_sm_by_kd_jabatan_tgl_awal_limit($kd_jabatan, $limit, $perihal, $tgl_awal){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') 
		AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, 
		B.ID_STATUS_SM, B.NM_STATUS_SM, C.ID_DISTRIBUSI_SURAT, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS 
		WAKTU_KIRIM FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE C.JABATAN_PENERIMA='".$kd_jabatan."' 
		AND C.KD_STATUS_DISTRIBUSI='PS' AND C.WAKTU_KIRIM >= TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND 
		A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND (UPPER(A.PERIHAL) LIKE '%".$perihal."%' OR UPPER(A.NO_SURAT) LIKE '%".$perihal."%' 
		OR TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$perihal."%' OR UPPER(A.DARI) LIKE '%".$perihal."%') 
		AND B.ID_STATUS_SM=C.ID_STATUS_SM 
		ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.") ";
		return $this->tnde->query($sql)->result();
	}
	
	function get_sm_by_kd_jabatan_tgl_awal_paging($kd_jabatan, $limit, $page, $perihal, $tgl_awal){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, WAKTU_PENCATATAN, 
		ID_STATUS_SM, NM_STATUS_SM, ID_DISTRIBUSI_SURAT, WAKTU_KIRIM, ROWNUM R FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, 
		TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') 
		AS WAKTU_PENCATATAN, B.ID_STATUS_SM, B.NM_STATUS_SM, C.ID_DISTRIBUSI_SURAT, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') 
		AS WAKTU_KIRIM 
		FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C 
		WHERE C.JABATAN_PENERIMA='".$kd_jabatan."' 
		AND C.KD_STATUS_DISTRIBUSI='PS' AND C.WAKTU_KIRIM >= TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND 
		A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND (UPPER(A.PERIHAL) LIKE '%".$perihal."%' OR UPPER(A.NO_SURAT) LIKE '%".$perihal."%' 
		OR TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$perihal."%' OR UPPER(A.DARI) LIKE '%".$perihal."%') 
		AND B.ID_STATUS_SM=C.ID_STATUS_SM 
		ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.") WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();	
	}
	
	function get_sm_by_kd_jabatan_tgl_akhir_limit($kd_jabatan, $limit, $perihal, $tgl_akhir){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, 
		A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, B.ID_STATUS_SM, B.NM_STATUS_SM, 
		C.ID_DISTRIBUSI_SURAT, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM 
		FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C 
		WHERE C.JABATAN_PENERIMA='".$kd_jabatan."' AND C.KD_STATUS_DISTRIBUSI='PS' AND 
		C.WAKTU_KIRIM <= TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND 
		(UPPER(A.PERIHAL) LIKE '%".$perihal."%' OR UPPER(A.NO_SURAT) LIKE '%".$perihal."%' 
		OR TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$perihal."%' OR UPPER(A.DARI) LIKE '%".$perihal."%') 
		AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) 
		WHERE ROWNUM <= ".$limit.")";
		return $this->tnde->query($sql)->result();
	}
	
	function get_sm_by_kd_jabatan_tgl_akhir_paging($kd_jabatan, $limit, $page, $perihal, $tgl_akhir){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, WAKTU_PENCATATAN, 
		ID_STATUS_SM, NM_STATUS_SM, ID_DISTRIBUSI_SURAT, WAKTU_KIRIM, ROWNUM R FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, 
		TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY') AS 
		WAKTU_PENCATATAN, B.ID_STATUS_SM, B.NM_STATUS_SM, C.ID_DISTRIBUSI_SURAT, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') 
		AS WAKTU_KIRIM 
		FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C 
		WHERE C.JABATAN_PENERIMA='".$kd_jabatan."' AND 
		C.KD_STATUS_DISTRIBUSI='PS' AND C.WAKTU_KIRIM <= TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND 
		A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND (UPPER(A.PERIHAL) LIKE '%".$perihal."%' OR UPPER(A.NO_SURAT) LIKE '%".$perihal."%' 
		OR TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$perihal."%' OR UPPER(A.DARI) LIKE '%".$perihal."%') 
		AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY 
		C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.") WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();		
	}
	
	function get_sm_by_kd_jabatan_periode_limit($kd_jabatan, $limit, $perihal, $tgl_awal, $tgl_akhir){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, 
		A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, B.ID_STATUS_SM, B.NM_STATUS_SM, 
		C.ID_DISTRIBUSI_SURAT, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM 
		FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C 
		WHERE C.JABATAN_PENERIMA='".$kd_jabatan."' AND C.KD_STATUS_DISTRIBUSI='PS' AND C.WAKTU_KIRIM BETWEEN 
		TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND 
		A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND (UPPER(A.PERIHAL) LIKE '%".$perihal."%' OR UPPER(A.NO_SURAT) LIKE '%".$perihal."%' 
		OR TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$perihal."%' OR UPPER(A.DARI) LIKE '%".$perihal."%') 
		AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.") ";
		return $this->tnde->query($sql)->result();
	}
	
	function get_sm_by_kd_jabatan_periode_paging($kd_jabatan, $limit, $page, $perihal, $tgl_awal, $tgl_akhir){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, WAKTU_PENCATATAN, 
		ID_STATUS_SM, NM_STATUS_SM, ID_DISTRIBUSI_SURAT, WAKTU_KIRIM, ROWNUM R FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, 
		TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') 
		AS WAKTU_PENCATATAN, B.ID_STATUS_SM, B.NM_STATUS_SM, C.ID_DISTRIBUSI_SURAT, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') 
		AS WAKTU_KIRIM 
		FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C 
		WHERE C.JABATAN_PENERIMA='".$kd_jabatan."' AND 
		C.KD_STATUS_DISTRIBUSI='PS' AND C.WAKTU_KIRIM BETWEEN TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND 
		TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND 
		(UPPER(A.PERIHAL) LIKE '%".$perihal."%' OR UPPER(A.NO_SURAT) LIKE '%".$perihal."%' 
		OR TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$perihal."%' OR UPPER(A.DARI) LIKE '%".$perihal."%') 
		AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.") 
		WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();	
	}
	
	function get_sm_by_kd_jabatan_perihal_limit($kd_jabatan, $limit, $perihal){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, 
		A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, B.ID_STATUS_SM, B.NM_STATUS_SM, 
		C.ID_DISTRIBUSI_SURAT, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM 
		FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C 
		WHERE C.JABATAN_PENERIMA='".$kd_jabatan."' AND C.KD_STATUS_DISTRIBUSI='PS' AND 
		A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND (UPPER(A.PERIHAL) LIKE '%".$perihal."%' OR UPPER(A.NO_SURAT) LIKE '%".$perihal."%' 
		OR TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$perihal."%' OR UPPER(A.DARI) LIKE '%".$perihal."%') 
		AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY 
		C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.") ";
		return $this->tnde->query($sql)->result();
	}

	function get_sm_by_kd_jabatan_perihal_paging($kd_jabatan, $limit, $page, $perihal){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, WAKTU_PENCATATAN, 
		ID_STATUS_SM, NM_STATUS_SM, ID_DISTRIBUSI_SURAT, WAKTU_KIRIM, ROWNUM R FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, 
		TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') 
		AS WAKTU_PENCATATAN, B.ID_STATUS_SM, B.NM_STATUS_SM, C.ID_DISTRIBUSI_SURAT, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') 
		AS WAKTU_KIRIM 
		FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C 
		WHERE C.JABATAN_PENERIMA='".$kd_jabatan."' AND 
		C.KD_STATUS_DISTRIBUSI='PS' AND A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND 
		(UPPER(A.PERIHAL) LIKE '%".$perihal."%' OR UPPER(A.NO_SURAT) LIKE '%".$perihal."%' 
		OR TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$perihal."%' OR UPPER(A.DARI) LIKE '%".$perihal."%') 
		AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.") WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();	
	}
	
	function get_dm_by_kd_jabatan_limit($kd_jabatan, $limit){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, B.ID_STATUS_SM, B.NM_STATUS_SM, C.ID_DISTRIBUSI_SURAT, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE C.JABATAN_PENERIMA='".$kd_jabatan."' AND C.KD_STATUS_DISTRIBUSI='D' AND A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.") ";
		return $this->tnde->query($sql)->result();
	}
	
	function get_dm_by_kd_jabatan_paging($kd_jabatan, $limit, $page){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, WAKTU_PENCATATAN, ID_STATUS_SM, NM_STATUS_SM, ID_DISTRIBUSI_SURAT, WAKTU_KIRIM, WAKTU_PENYELESAIAN, ROWNUM R FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, B.ID_STATUS_SM, B.NM_STATUS_SM, C.ID_DISTRIBUSI_SURAT, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE C.JABATAN_PENERIMA='".$kd_jabatan."' AND C.KD_STATUS_DISTRIBUSI='D' AND A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.") WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();	
	}
	
	function get_dm_by_kd_jabatan_tgl_awal_limit($kd_jabatan, $limit, $perihal, $tgl_awal){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, B.ID_STATUS_SM, 
		B.NM_STATUS_SM, C.ID_DISTRIBUSI_SURAT, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C 
		WHERE C.JABATAN_PENERIMA='".$kd_jabatan."' AND C.KD_STATUS_DISTRIBUSI='D' AND C.WAKTU_KIRIM >= TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND A.ID_SURAT_MASUK=C.ID_SURAT_MASUK 
		AND UPPER(A.PERIHAL) LIKE '%".$perihal."%' AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.") ";
		return $this->tnde->query($sql)->result();
	}
	
	function get_dm_by_kd_jabatan_tgl_awal_paging($kd_jabatan, $limit, $page, $perihal, $tgl_awal){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, WAKTU_PENCATATAN, ID_STATUS_SM, NM_STATUS_SM, 
		ID_DISTRIBUSI_SURAT, WAKTU_KIRIM, WAKTU_PENYELESAIAN, ROWNUM R FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, 
		TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, B.ID_STATUS_SM, B.NM_STATUS_SM, C.ID_DISTRIBUSI_SURAT, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN 
		FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C 
		WHERE C.JABATAN_PENERIMA='".$kd_jabatan."' AND C.KD_STATUS_DISTRIBUSI='D' AND C.WAKTU_KIRIM >= TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND A.ID_SURAT_MASUK=C.ID_SURAT_MASUK 
		AND UPPER(A.PERIHAL) LIKE '%".$perihal."%' AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.") 
		WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();	
	}
	
	function get_dm_by_kd_jabatan_tgl_akhir_limit($kd_jabatan, $limit, $perihal, $tgl_akhir){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, B.ID_STATUS_SM, 
		B.NM_STATUS_SM, C.ID_DISTRIBUSI_SURAT, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C 
		WHERE C.JABATAN_PENERIMA='".$kd_jabatan."' AND C.KD_STATUS_DISTRIBUSI='D' AND C.WAKTU_KIRIM <= TO_DATE(' ".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') 
		AND A.ID_SURAT_MASUK=C.ID_SURAT_MASUK 
		AND UPPER(A.PERIHAL) LIKE '%".$perihal."%' AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.") ";
		return $this->tnde->query($sql)->result();	
	}
	
	function get_dm_by_kd_jabatan_tgl_akhir_paging($kd_jabatan, $limit, $page, $perihal, $tgl_akhir){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, WAKTU_PENCATATAN, ID_STATUS_SM, NM_STATUS_SM, 
		ID_DISTRIBUSI_SURAT, WAKTU_KIRIM, WAKTU_PENYELESAIAN, ROWNUM R FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, 
		TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, B.ID_STATUS_SM, B.NM_STATUS_SM, C.ID_DISTRIBUSI_SURAT, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN 
		FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C 
		WHERE C.JABATAN_PENERIMA='".$kd_jabatan."' AND C.KD_STATUS_DISTRIBUSI='D' AND C.WAKTU_KIRIM <= TO_DATE(' ".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') 
		AND A.ID_SURAT_MASUK=C.ID_SURAT_MASUK 
		AND UPPER(A.PERIHAL) LIKE '%".$perihal."%' AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.") 
		WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();	
	}
	
	function get_dm_by_kd_jabatan_periode_limit($kd_jabatan, $limit, $perihal, $tgl_awal, $tgl_akhir){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, B.ID_STATUS_SM, 
		B.NM_STATUS_SM, C.ID_DISTRIBUSI_SURAT, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C 
		WHERE C.JABATAN_PENERIMA='".$kd_jabatan."' AND C.KD_STATUS_DISTRIBUSI='D' AND C.WAKTU_KIRIM 
		BETWEEN TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND TO_DATE(' ".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND A.ID_SURAT_MASUK=C.ID_SURAT_MASUK 
		AND UPPER(A.PERIHAL) LIKE '%".$perihal."%' AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.") ";
		return $this->tnde->query($sql)->result();	
	}
	
	function get_dm_by_kd_jabatan_periode_paging($kd_jabatan, $limit, $page, $perihal, $tgl_awal, $tgl_akhir){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, WAKTU_PENCATATAN, ID_STATUS_SM, NM_STATUS_SM, 
		ID_DISTRIBUSI_SURAT, WAKTU_KIRIM, WAKTU_PENYELESAIAN, ROWNUM R FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, 
		TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, B.ID_STATUS_SM, B.NM_STATUS_SM, C.ID_DISTRIBUSI_SURAT, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN 
		FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C 
		WHERE C.JABATAN_PENERIMA='".$kd_jabatan."' AND C.KD_STATUS_DISTRIBUSI='D' AND C.WAKTU_KIRIM BETWEEN TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND TO_DATE(' ".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') 
		AND A.ID_SURAT_MASUK=C.ID_SURAT_MASUK 
		AND UPPER(A.PERIHAL) LIKE '%".$perihal."%' AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.") 
		WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();	
	}
	
	function get_dm_by_kd_jabatan_perihal_limit($kd_jabatan, $limit, $perihal){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, 
																						TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, B.ID_STATUS_SM, 
																						B.NM_STATUS_SM, C.ID_DISTRIBUSI_SURAT, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS 
																						WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN 
																			FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C 
																			WHERE C.JABATAN_PENERIMA='".$kd_jabatan."' AND C.KD_STATUS_DISTRIBUSI='D' AND A.ID_SURAT_MASUK=C.ID_SURAT_MASUK 
																						AND B.ID_STATUS_SM=C.ID_STATUS_SM AND (UPPER(A.PERIHAL) LIKE '%".$perihal."%' OR UPPER(A.DARI) LIKE '%".$perihal."%' 
																						OR TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$perihal."%' OR UPPER(A.NO_SURAT) LIKE '%".$perihal."%')
																						ORDER BY C.WAKTU_KIRIM DESC
																		) WHERE ROWNUM <= ".$limit."
											) ";
		return $this->tnde->query($sql)->result();	
	}

	function get_dm_by_kd_jabatan_perihal_paging($kd_jabatan, $limit, $page, $perihal){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, WAKTU_PENCATATAN, ID_STATUS_SM, NM_STATUS_SM, 
																						ID_DISTRIBUSI_SURAT, WAKTU_KIRIM, WAKTU_PENYELESAIAN, ROWNUM R 
																			FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, 
																									A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, 
																									B.ID_STATUS_SM, B.NM_STATUS_SM, C.ID_DISTRIBUSI_SURAT, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') 
																									AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN 
																						FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C 
																						WHERE C.JABATAN_PENERIMA='".$kd_jabatan."' AND C.KD_STATUS_DISTRIBUSI='D' AND 
																									A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND B.ID_STATUS_SM=C.ID_STATUS_SM AND 
																									(UPPER(A.PERIHAL) LIKE '%".$perihal."%' OR UPPER(A.DARI) LIKE '%".$perihal."%' OR 
																									TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$perihal."%' OR UPPER(A.NO_SURAT) 
																									LIKE '%".$perihal."%') 
																									ORDER BY C.WAKTU_KIRIM DESC
																					) WHERE ROWNUM <= ".$limit."
																			) WHERE R > ".$page." 
												)";
		return $this->tnde->query($sql)->result();	
	}
	
	function get_dm_by_nip_limit($nip, $limit){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, B.NM_STATUS_SM, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, 
				C.ID_DISTRIBUSI_SURAT, C.ID_STATUS_SM FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE C.PENERIMA_SURAT='".$nip."' AND 
				A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.")";
		return $this->tnde->query($sql)->result();
	}
	
	function get_dm_by_nip_paging($nip, $limit, $page){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, NM_STATUS_SM, WAKTU_KIRIM, WAKTU_PENYELESAIAN, 
				ID_DISTRIBUSI_SURAT, ID_STATUS_SM, ROWNUM R FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, B.NM_STATUS_SM, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, 
				TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, C.ID_DISTRIBUSI_SURAT, C.ID_STATUS_SM FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE 
				C.PENERIMA_SURAT='".$nip."' AND A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE 
				ROWNUM <= ".$limit.") WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function get_dm_by_nip_tgl_awal_limit($nip, $limit, $perihal, $tgl_awal){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, C.ID_STATUS_SM, B.NM_STATUS_SM, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, C.ID_DISTRIBUSI_SURAT FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND C.PENERIMA_SURAT='".$nip."' AND C.WAKTU_KIRIM >= TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND UPPER(A.PERIHAL) LIKE '%".$perihal."%' AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.")";
		return $this->tnde->query($sql)->result();
	}
	
	function get_dm_by_nip_tgl_awal_paging($nip, $limit, $page, $perihal, $tgl_awal){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, WAKTU_PENCATATAN, ID_STATUS_SM, NM_STATUS_SM, ID_DISTRIBUSI_SURAT, WAKTU_KIRIM, WAKTU_PENYELESAIAN, ROWNUM R FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, C.ID_STATUS_SM, B.NM_STATUS_SM, C.ID_DISTRIBUSI_SURAT, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND C.PENERIMA_SURAT='".$nip."' AND C.WAKTU_KIRIM >= TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND UPPER(PERIHAL) LIKE '%".$perihal."%' AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.") WHERE R > ".$page." ) ";
		return $this->tnde->query($sql)->result();	
	}
	
	function get_dm_by_nip_tgl_akhir_limit($nip, $limit, $perihal, $tgl_akhir){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, C.ID_STATUS_SM, B.NM_STATUS_SM, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, C.ID_DISTRIBUSI_SURAT FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND C.PENERIMA_SURAT='".$nip."' AND C.WAKTU_KIRIM <= TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND UPPER(A.PERIHAL) LIKE '%".$perihal."%' AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.")";
		return $this->tnde->query($sql)->result();
	}
	
	function get_dm_by_nip_tgl_akhir_paging($nip, $limit, $page, $perihal, $tgl_akhir){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, WAKTU_PENCATATAN, ID_STATUS_SM, NM_STATUS_SM, ID_DISTRIBUSI_SURAT, WAKTU_KIRIM, WAKTU_PENYELESAIAN, ROWNUM R FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, C.ID_STATUS_SM, B.NM_STATUS_SM, C.ID_DISTRIBUSI_SURAT, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND C.PENERIMA_SURAT='".$nip."' AND C.WAKTU_KIRIM <= TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND UPPER(PERIHAL) LIKE '%".$perihal."%' AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.") WHERE R > ".$page." ) ";
		return $this->tnde->query($sql)->result();	
	}
	
	function get_dm_by_nip_periode_limit($nip, $limit, $perihal, $tgl_awal, $tgl_akhir){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, C.ID_STATUS_SM, B.NM_STATUS_SM, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, C.ID_DISTRIBUSI_SURAT FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND C.PENERIMA_SURAT='".$nip."' AND C.WAKTU_KIRIM BETWEEN TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND UPPER(A.PERIHAL) LIKE '%".$perihal."%' AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.")";
		return $this->tnde->query($sql)->result();	
	}
	
	function get_dm_by_nip_periode_paging($nip, $limit, $page, $perihal, $tgl_awal, $tgl_akhir){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, WAKTU_PENCATATAN, ID_STATUS_SM, NM_STATUS_SM, ID_DISTRIBUSI_SURAT, WAKTU_KIRIM, WAKTU_PENYELESAIAN, ROWNUM R FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, C.ID_STATUS_SM, B.NM_STATUS_SM, C.ID_DISTRIBUSI_SURAT, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND C.PENERIMA_SURAT='".$nip."' AND C.WAKTU_KIRIM BETWEEN TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND UPPER(PERIHAL) LIKE '%".$perihal."%' AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.") WHERE R > ".$page." ) ";
		return $this->tnde->query($sql)->result();	
	}
	
	/*==========================================*/
	function get_dm_by_kd_pegawai_limit($kd_pegawai, $limit){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, B.NM_STATUS_SM, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, 
				C.ID_DISTRIBUSI_SURAT, C.ID_STATUS_SM FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE C.PENERIMA_SURAT='".$kd_pegawai."' AND C.KD_STATUS_DISTRIBUSI='D' AND
				A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.")";
		return $this->tnde->query($sql)->result();
	}
	
	function get_dm_by_kd_pegawai_paging($kd_pegawai, $limit, $page){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, NM_STATUS_SM, WAKTU_KIRIM, WAKTU_PENYELESAIAN, ID_DISTRIBUSI_SURAT, ID_STATUS_SM, ROWNUM R FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, B.NM_STATUS_SM, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, C.ID_DISTRIBUSI_SURAT, C.ID_STATUS_SM 
		FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C 
		WHERE C.PENERIMA_SURAT='".$kd_pegawai."' AND C.KD_STATUS_DISTRIBUSI='D' AND A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.") WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function get_dm_by_kd_pegawai_tgl_awal_limit($kd_pegawai, $limit, $perihal, $tgl_awal){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, C.ID_STATUS_SM, B.NM_STATUS_SM, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, C.ID_DISTRIBUSI_SURAT FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND C.PENERIMA_SURAT='".$kd_pegawai."' AND C.KD_STATUS_DISTRIBUSI='D' AND C.WAKTU_KIRIM >= TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND UPPER(A.PERIHAL) LIKE '%".$perihal."%' AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.")";
		return $this->tnde->query($sql)->result();
	}
	
	function get_dm_by_kd_pegawai_tgl_awal_paging($kd_pegawai, $limit, $page, $perihal, $tgl_awal){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, WAKTU_PENCATATAN, ID_STATUS_SM, NM_STATUS_SM, ID_DISTRIBUSI_SURAT, WAKTU_KIRIM, WAKTU_PENYELESAIAN, ROWNUM R FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, C.ID_STATUS_SM, B.NM_STATUS_SM, C.ID_DISTRIBUSI_SURAT, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND C.PENERIMA_SURAT='".$kd_pegawai."' AND C.KD_STATUS_DISTRIBUSI='D' AND C.WAKTU_KIRIM >= TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND UPPER(PERIHAL) LIKE '%".$perihal."%' AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.") WHERE R > ".$page." ) ";
		return $this->tnde->query($sql)->result();	
	}
	
	function get_dm_by_kd_pegawai_tgl_akhir_limit($kd_pegawai, $limit, $perihal, $tgl_akhir){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, C.ID_STATUS_SM, B.NM_STATUS_SM, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, C.ID_DISTRIBUSI_SURAT FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND C.PENERIMA_SURAT='".$kd_pegawai."' AND C.KD_STATUS_DISTRIBUSI='D' AND C.WAKTU_KIRIM <= TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND UPPER(A.PERIHAL) LIKE '%".$perihal."%' AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.")";
		return $this->tnde->query($sql)->result();
	}
	
	function get_dm_by_kd_pegawai_tgl_akhir_paging($kd_pegawai, $limit, $page, $perihal, $tgl_akhir){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, WAKTU_PENCATATAN, ID_STATUS_SM, NM_STATUS_SM, ID_DISTRIBUSI_SURAT, WAKTU_KIRIM, WAKTU_PENYELESAIAN, ROWNUM R FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, C.ID_STATUS_SM, B.NM_STATUS_SM, C.ID_DISTRIBUSI_SURAT, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND C.PENERIMA_SURAT='".$kd_pegawai."' AND C.KD_STATUS_DISTRIBUSI='D' AND C.WAKTU_KIRIM <= TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND UPPER(PERIHAL) LIKE '%".$perihal."%' AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.") WHERE R > ".$page." ) ";
		return $this->tnde->query($sql)->result();	
	}
	
	function get_dm_by_kd_pegawai_periode_limit($kd_pegawai, $limit, $perihal, $tgl_awal, $tgl_akhir){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, C.ID_STATUS_SM, B.NM_STATUS_SM, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, C.ID_DISTRIBUSI_SURAT FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND C.PENERIMA_SURAT='".$kd_pegawai."' AND C.KD_STATUS_DISTRIBUSI='D' AND C.WAKTU_KIRIM BETWEEN TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND UPPER(A.PERIHAL) LIKE '%".$perihal."%' AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.")";
		return $this->tnde->query($sql)->result();	
	}
	
	function get_dm_by_kd_pegawai_periode_paging($kd_pegawai, $limit, $page, $perihal, $tgl_awal, $tgl_akhir){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, WAKTU_PENCATATAN, ID_STATUS_SM, NM_STATUS_SM, ID_DISTRIBUSI_SURAT, WAKTU_KIRIM, WAKTU_PENYELESAIAN, ROWNUM R FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, C.ID_STATUS_SM, B.NM_STATUS_SM, C.ID_DISTRIBUSI_SURAT, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C WHERE A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND C.PENERIMA_SURAT='".$kd_pegawai."' AND C.KD_STATUS_DISTRIBUSI='D' AND C.WAKTU_KIRIM BETWEEN TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND UPPER(PERIHAL) LIKE '%".$perihal."%' AND B.ID_STATUS_SM=C.ID_STATUS_SM ORDER BY C.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit.") WHERE R > ".$page." ) ";
		return $this->tnde->query($sql)->result();	
	}
	
	function get_dm_by_kd_pegawai_perihal_limit($kd_pegawai, $limit, $perihal){
		$perihal = strtoupper($perihal);
		$sql = "SELECT * FROM (SELECT * FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, A.PERIHAL, 
																						TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, C.ID_STATUS_SM, 
																						B.NM_STATUS_SM, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, 
																						TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, C.ID_DISTRIBUSI_SURAT 
																		FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C 
																		WHERE A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND C.PENERIMA_SURAT='".$kd_pegawai."' AND 
																					C.KD_STATUS_DISTRIBUSI='D' AND B.ID_STATUS_SM=C.ID_STATUS_SM AND 
																					(UPPER(A.PERIHAL) LIKE '%".$perihal."%' OR UPPER(A.DARI) LIKE '%".$perihal."%' OR UPPER(A.NO_SURAT) LIKE '%".$perihal."%' 
																					OR TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$perihal."%')
																		ORDER BY C.WAKTU_KIRIM DESC
																		) 
																WHERE ROWNUM <= ".$limit."
											)";
		return $this->tnde->query($sql)->result();	
	}

	function get_dm_by_kd_pegawai_perihal_paging($kd_pegawai, $limit, $page, $perihal){
		$perihal = strtoupper($perihal);
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_SURAT_MASUK, NO_SURAT, TGL_MASUK, DARI, PERIHAL, WAKTU_PENCATATAN, ID_STATUS_SM, NM_STATUS_SM, 
																						ID_DISTRIBUSI_SURAT, WAKTU_KIRIM, WAKTU_PENYELESAIAN, ROWNUM R 
																			FROM (SELECT A.ID_SURAT_MASUK, A.NO_SURAT, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.DARI, 
																									A.PERIHAL, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, 
																									C.ID_STATUS_SM, B.NM_STATUS_SM, C.ID_DISTRIBUSI_SURAT, TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') 
																									AS WAKTU_KIRIM, TO_CHAR(C.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN 
																						FROM MD_SURAT_MASUK A, MD_STATUS_SM B, D_DISTRIBUSI_SURAT C 
																						WHERE A.ID_SURAT_MASUK=C.ID_SURAT_MASUK AND C.PENERIMA_SURAT='".$kd_pegawai."' AND 
																									C.KD_STATUS_DISTRIBUSI='D' AND B.ID_STATUS_SM=C.ID_STATUS_SM AND 
																									(UPPER(A.PERIHAL) LIKE '%".$perihal."%' OR UPPER(A.DARI) LIKE '%".$perihal."%' OR UPPER(A.NO_SURAT) 
																									LIKE '%".$perihal."%' OR TO_CHAR(C.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$perihal."%')
																									ORDER BY C.WAKTU_KIRIM DESC
																						) WHERE ROWNUM <= ".$limit."
																		) WHERE R > ".$page." 
												) ";
		return $this->tnde->query($sql)->result();	
	}
	/*==========================================*/
	
	function get_dk_grup_by_kd_jabatan_limit($kd_jabatan, $limit){
		$sql = "SELECT ID_DISTRIBUSI_SURAT, ID_SURAT_MASUK, WAKTU_KIRIM, WAKTU_PENYELESAIAN, NO_SURAT, DARI, PERIHAL, ROWNUM FROM ( WITH TEMP AS( SELECT * FROM (SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, B.NO_SURAT, B.DARI, B.PERIHAL, ROW_NUMBER() OVER(PARTITION BY A.ID_SURAT_MASUK ORDER BY A.ID_DISTRIBUSI_SURAT DESC) RN FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B WHERE A.JABATAN_PEMROSES='".$kd_jabatan."' AND A.KD_STATUS_DISTRIBUSI='D' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK ORDER BY A.WAKTU_KIRIM DESC))SELECT * FROM TEMP WHERE RN=1 ) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_dk_grup_by_kd_jabatan_paging($kd_jabatan, $limit, $page){
		$sql = "SELECT * FROM ( SELECT ID_DISTRIBUSI_SURAT, ID_SURAT_MASUK, WAKTU_KIRIM, WAKTU_PENYELESAIAN, NO_SURAT, DARI, PERIHAL, ROWNUM R FROM ( WITH TEMP AS( SELECT * FROM (SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, B.NO_SURAT, B.DARI, B.PERIHAL, ROW_NUMBER() OVER(PARTITION BY A.ID_SURAT_MASUK ORDER BY A.ID_DISTRIBUSI_SURAT DESC) RN FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B WHERE A.JABATAN_PEMROSES='".$kd_jabatan."' AND A.KD_STATUS_DISTRIBUSI='D' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK ORDER BY A.WAKTU_KIRIM DESC))SELECT * FROM TEMP WHERE RN=1 ) WHERE ROWNUM <= ".$limit.") WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_dk_grup_by_kd_jabatan_tgl_awal_limit($kd_jabatan, $limit, $perihal, $tgl_awal){
		$sql = "SELECT ID_DISTRIBUSI_SURAT, ID_SURAT_MASUK, WAKTU_KIRIM, WAKTU_PENYELESAIAN, NO_SURAT, DARI, PERIHAL, ROWNUM 
		FROM ( WITH TEMP AS( SELECT * FROM (SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, 
		TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS 
		WAKTU_PENYELESAIAN, B.NO_SURAT, B.DARI, B.PERIHAL, ROW_NUMBER() OVER(PARTITION BY A.ID_SURAT_MASUK ORDER BY 
		A.ID_DISTRIBUSI_SURAT DESC) RN 
		FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B 
		WHERE A.JABATAN_PEMROSES='".$kd_jabatan."' 
		AND A.KD_STATUS_DISTRIBUSI='D' AND A.WAKTU_KIRIM >= TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND 
		B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND 
		(UPPER(B.PERIHAL) LIKE '%".$perihal."%' OR UPPER(B.NO_SURAT) LIKE '%".$perihal."%' OR UPPER(B.DARI) LIKE '%".$perihal."%' 
		OR TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$perihal."%' OR TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') 
		LIKE '%".$perihal."%') 
		ORDER BY A.WAKTU_KIRIM DESC))SELECT * FROM 
		TEMP WHERE RN=1 ) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_dk_grup_by_kd_jabatan_tgl_awal_paging($kd_jabatan, $limit, $page, $perihal, $tgl_awal){
		$sql = "SELECT * FROM ( SELECT ID_DISTRIBUSI_SURAT, ID_SURAT_MASUK, WAKTU_KIRIM, WAKTU_PENYELESAIAN, NO_SURAT, DARI, 
		PERIHAL, ROWNUM R FROM ( WITH TEMP AS( SELECT * FROM (SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, 
		TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS 
		WAKTU_PENYELESAIAN, B.NO_SURAT, B.DARI, B.PERIHAL, ROW_NUMBER() OVER(PARTITION BY A.ID_SURAT_MASUK ORDER BY 
		A.ID_DISTRIBUSI_SURAT DESC) RN 
		FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B 
		WHERE A.JABATAN_PEMROSES='".$kd_jabatan."' 
		AND A.KD_STATUS_DISTRIBUSI='D' AND A.WAKTU_KIRIM >= TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND 
		B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND 
		(UPPER(B.PERIHAL) LIKE '%".$perihal."%' OR UPPER(B.NO_SURAT) LIKE '%".$perihal."%' OR UPPER(B.DARI) LIKE '%".$perihal."%' 
		OR TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$perihal."%' OR TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') 
		LIKE '%".$perihal."%') 
		ORDER BY A.WAKTU_KIRIM DESC))SELECT * 
		FROM TEMP WHERE RN=1 ) WHERE ROWNUM <= ".$limit.") WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_dk_grup_by_kd_jabatan_tgl_akhir_limit($kd_jabatan, $limit, $perihal, $tgl_akhir){
		$sql = "SELECT ID_DISTRIBUSI_SURAT, ID_SURAT_MASUK, WAKTU_KIRIM, WAKTU_PENYELESAIAN, NO_SURAT, DARI, PERIHAL, 
		ROWNUM FROM ( WITH TEMP AS( SELECT * FROM (SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, 
		TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS 
		WAKTU_PENYELESAIAN, B.NO_SURAT, B.DARI, B.PERIHAL, ROW_NUMBER() OVER(PARTITION BY A.ID_SURAT_MASUK ORDER BY 
		A.ID_DISTRIBUSI_SURAT DESC) RN 
		FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B 
		WHERE A.JABATAN_PEMROSES='".$kd_jabatan."' 
		AND A.KD_STATUS_DISTRIBUSI='D' AND A.WAKTU_KIRIM <= TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') 
		AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND 
		(UPPER(B.PERIHAL) LIKE '%".$perihal."%' OR UPPER(B.NO_SURAT) LIKE '%".$perihal."%' OR UPPER(B.DARI) LIKE '%".$perihal."%' 
		OR TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$perihal."%' OR TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') 
		LIKE '%".$perihal."%')
		ORDER BY A.WAKTU_KIRIM DESC)) 
		SELECT * FROM TEMP WHERE RN=1 ) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_dk_grup_by_kd_jabatan_tgl_akhir_paging($kd_jabatan, $limit, $page, $perihal, $tgl_akhir){
		$sql = "SELECT * FROM ( SELECT ID_DISTRIBUSI_SURAT, ID_SURAT_MASUK, WAKTU_KIRIM, WAKTU_PENYELESAIAN, NO_SURAT, DARI, 
		PERIHAL, ROWNUM R FROM ( WITH TEMP AS( SELECT * FROM (SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, 
		TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS 
		WAKTU_PENYELESAIAN, B.NO_SURAT, B.DARI, B.PERIHAL, ROW_NUMBER() OVER(PARTITION BY A.ID_SURAT_MASUK ORDER BY 
		A.ID_DISTRIBUSI_SURAT DESC) RN 
		FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B 
		WHERE A.JABATAN_PEMROSES='".$kd_jabatan."' 
		AND A.KD_STATUS_DISTRIBUSI='D' AND A.WAKTU_KIRIM <= TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND 
		B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND 
		(UPPER(B.PERIHAL) LIKE '%".$perihal."%' OR UPPER(B.NO_SURAT) LIKE '%".$perihal."%' OR UPPER(B.DARI) LIKE '%".$perihal."%' 
		OR TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$perihal."%' OR TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') 
		LIKE '%".$perihal."%')
		ORDER BY A.WAKTU_KIRIM DESC)) 
		SELECT * FROM TEMP WHERE RN=1 ) WHERE ROWNUM <= ".$limit.") WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_dk_grup_by_kd_jabatan_periode_limit($kd_jabatan, $limit, $perihal, $tgl_awal, $tgl_akhir){
		$sql = "SELECT ID_DISTRIBUSI_SURAT, ID_SURAT_MASUK, WAKTU_KIRIM, WAKTU_PENYELESAIAN, NO_SURAT, DARI, PERIHAL, 
		ROWNUM FROM ( WITH TEMP AS( SELECT * FROM (SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, 
		TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS 
		WAKTU_PENYELESAIAN, B.NO_SURAT, B.DARI, B.PERIHAL, ROW_NUMBER() OVER(PARTITION BY A.ID_SURAT_MASUK ORDER BY 
		A.ID_DISTRIBUSI_SURAT DESC) RN 
		FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B 
		WHERE A.JABATAN_PEMROSES='".$kd_jabatan."' 
		AND A.KD_STATUS_DISTRIBUSI='D' AND A.WAKTU_KIRIM BETWEEN TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND 
		TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND 
		(UPPER(B.PERIHAL) LIKE '%".$perihal."%' OR UPPER(B.NO_SURAT) LIKE '%".$perihal."%' OR UPPER(B.DARI) LIKE '%".$perihal."%' 
		OR TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$perihal."%' OR TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') 
		LIKE '%".$perihal."%') ORDER BY A.WAKTU_KIRIM DESC))SELECT * FROM TEMP WHERE RN=1 ) 
		WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_dk_grup_by_kd_jabatan_periode_paging($kd_jabatan, $limit, $page, $perihal, $tgl_awal, $tgl_akhir){
		$sql = "SELECT * FROM ( SELECT ID_DISTRIBUSI_SURAT, ID_SURAT_MASUK, WAKTU_KIRIM, WAKTU_PENYELESAIAN, NO_SURAT, DARI, 
		PERIHAL, ROWNUM R FROM ( WITH TEMP AS( SELECT * FROM (SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, 
		TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS 
		WAKTU_PENYELESAIAN, B.NO_SURAT, B.DARI, B.PERIHAL, ROW_NUMBER() OVER(PARTITION BY A.ID_SURAT_MASUK ORDER BY 
		A.ID_DISTRIBUSI_SURAT DESC) RN 
		FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B 
		WHERE A.JABATAN_PEMROSES='".$kd_jabatan."' 
		AND A.KD_STATUS_DISTRIBUSI='D' AND A.WAKTU_KIRIM BETWEEN TO_DATE('".$tgl_awal."', 'DD/MM/YYYY') AND 
		TO_DATE('".$tgl_akhir." 23:59:59', 'DD/MM/YYYY HH24:MI:SS') AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND 
		(UPPER(B.PERIHAL) LIKE '%".$perihal."%' OR UPPER(B.NO_SURAT) LIKE '%".$perihal."%' OR UPPER(B.DARI) LIKE '%".$perihal."%' 
		OR TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$perihal."%' OR TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') 
		LIKE '%".$perihal."%') ORDER BY A.WAKTU_KIRIM DESC))SELECT * FROM TEMP WHERE RN=1 ) WHERE 
		ROWNUM <= ".$limit.") WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_dk_grup_by_kd_jabatan_perihal_limit($kd_jabatan, $limit, $perihal){
		$sql = "SELECT ID_DISTRIBUSI_SURAT, ID_SURAT_MASUK, WAKTU_KIRIM, WAKTU_PENYELESAIAN, NO_SURAT, DARI, PERIHAL, 
		ROWNUM FROM ( WITH TEMP AS( SELECT * FROM (SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, 
		TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS 
		WAKTU_PENYELESAIAN, B.NO_SURAT, B.DARI, B.PERIHAL, ROW_NUMBER() OVER(PARTITION BY A.ID_SURAT_MASUK ORDER BY 
		A.ID_DISTRIBUSI_SURAT DESC) RN 
		FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B 
		WHERE A.JABATAN_PEMROSES='".$kd_jabatan."' 
		AND A.KD_STATUS_DISTRIBUSI='D' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND 
		(UPPER(B.PERIHAL) LIKE '%".$perihal."%' OR UPPER(B.NO_SURAT) LIKE '%".$perihal."%' OR UPPER(B.DARI) LIKE '%".$perihal."%' 
		OR TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$perihal."%' OR TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') 
		LIKE '%".$perihal."%') 
		ORDER BY A.WAKTU_KIRIM DESC))SELECT * FROM TEMP WHERE RN=1 ) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}

	function get_dk_grup_by_kd_jabatan_perihal_paging($kd_jabatan, $limit, $page, $perihal){
		$sql = "SELECT * FROM ( SELECT ID_DISTRIBUSI_SURAT, ID_SURAT_MASUK, WAKTU_KIRIM, WAKTU_PENYELESAIAN, NO_SURAT, DARI, 
		PERIHAL, ROWNUM R FROM ( WITH TEMP AS( SELECT * FROM (SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, 
		TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS 
		WAKTU_PENYELESAIAN, B.NO_SURAT, B.DARI, B.PERIHAL, ROW_NUMBER() OVER(PARTITION BY A.ID_SURAT_MASUK ORDER BY 
		A.ID_DISTRIBUSI_SURAT DESC) RN 
		FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B 
		WHERE A.JABATAN_PEMROSES='".$kd_jabatan."' 
		AND A.KD_STATUS_DISTRIBUSI='D' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND 
		(UPPER(B.PERIHAL) LIKE '%".$perihal."%' OR UPPER(B.NO_SURAT) LIKE '%".$perihal."%' OR UPPER(B.DARI) LIKE '%".$perihal."%' 
		OR TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$perihal."%' OR TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') 
		LIKE '%".$perihal."%') 
		ORDER BY A.WAKTU_KIRIM DESC))SELECT * FROM TEMP WHERE RN=1 ) WHERE ROWNUM <= ".$limit.") WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();
	}
	

	function get_ds_by_id_surat_masuk($id_surat_masuk, $kd_status_distribusi){
		$sql = "SELECT B.ID_DISTRIBUSI_SURAT, TO_CHAR(B.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') WAKTU_KIRIM, B.PENERIMA_SURAT, A.NM_JABATAN, B.ID_STATUS_SM, C.NM_STATUS_SM, B.JABATAN_PEMROSES, B.JABATAN_PENERIMA 
				FROM MD_STATUS_SM C, D_DISTRIBUSI_SURAT B 
						LEFT JOIN MD_JABATAN A ON A.KD_JABATAN=B.JABATAN_PENERIMA 
					WHERE B.ID_SURAT_MASUK='".$id_surat_masuk."' AND 
						B.KD_STATUS_DISTRIBUSI='".$kd_status_distribusi."' AND C.ID_STATUS_SM=B.ID_STATUS_SM";
		return $this->tnde->query($sql)->result();
	}
	
	function get_ds_by_id_surat_masuk2($id_surat_masuk, $kd_status_distribusi, $jabatan_pemroses){
		$sql = "SELECT B.ID_DISTRIBUSI_SURAT, B.PENERIMA_SURAT, TO_CHAR(B.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') WAKTU_KIRIM, A.KD_JABATAN, 
								A.NM_JABATAN, B.JABATAN_PENERIMA, B.ID_STATUS_SM, C.KD_PEGAWAI, C.NM_PEGAWAI, D.NM_STATUS_SM 
					FROM MD_STATUS_SM D, D_DISTRIBUSI_SURAT B LEFT JOIN MD_JABATAN A ON A.KD_JABATAN=B.JABATAN_PENERIMA 
								LEFT JOIN D_PEGAWAI C ON UPPER(C.KD_PEGAWAI)=UPPER(B.PENERIMA_SURAT) 
					WHERE B.ID_SURAT_MASUK='".$id_surat_masuk."' AND B.JABATAN_PEMROSES='".$jabatan_pemroses."' 
							AND B.KD_STATUS_DISTRIBUSI='".$kd_status_distribusi."' AND D.ID_STATUS_SM=B.ID_STATUS_SM";
		return $this->tnde->query($sql)->result();
	}
	
	function get_ds_by_id_surat_masuk3($id_surat_masuk, $kd_status_distribusi, $pemroses_surat){
		$sql = "SELECT B.ID_DISTRIBUSI_SURAT, B.PENERIMA_SURAT, TO_CHAR(B.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') WAKTU_KIRIM, 
								A.KD_JABATAN, A.NM_JABATAN, B.JABATAN_PENERIMA, B.ID_STATUS_SM, C.KD_PEGAWAI, C.NM_PEGAWAI, D.NM_STATUS_SM 
					FROM MD_STATUS_SM D, D_DISTRIBUSI_SURAT B LEFT JOIN MD_JABATAN A ON A.KD_JABATAN=B.JABATAN_PENERIMA LEFT JOIN 
								D_PEGAWAI C ON C.KD_PEGAWAI=B.PENERIMA_SURAT
					WHERE B.ID_SURAT_MASUK='".$id_surat_masuk."' AND B.PEMROSES_SURAT='".$pemroses_surat."' AND 
								B.KD_STATUS_DISTRIBUSI='".$kd_status_distribusi."' AND D.ID_STATUS_SM=B.ID_STATUS_SM";
		return $this->tnde->query($sql)->result();	
	}
	
	function get_distribusi_surat_by_id_ds_sebelumnya($id_distribusi_surat){
		$sql = "SELECT B.ID_DISTRIBUSI_SURAT, B.PENERIMA_SURAT, A.NM_JABATAN, B.JABATAN_PENERIMA, B.ID_STATUS_SM, C.KD_PEGAWAI, 
								C.NM_PEGAWAI, D.NM_STATUS_SM, TO_CHAR(B.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') WAKTU_KIRIM 
					FROM MD_STATUS_SM D, D_DISTRIBUSI_SURAT B LEFT JOIN MD_JABATAN A ON A.KD_JABATAN=B.JABATAN_PENERIMA LEFT JOIN D_PEGAWAI 
								C ON C.KD_PEGAWAI=B.PENERIMA_SURAT  
					WHERE B.ID_DS_SEBELUMNYA='".$id_distribusi_surat."' AND D.ID_STATUS_SM=B.ID_STATUS_SM";
		return $this->tnde->query($sql)->result();	
	}
	
	function pegawai_by_kd_jabatan_kd_bagian($kd_jabatan, $kd_bagian){
		$sql = "SELECT * FROM D_PEGAWAI WHERE KD_JABATAN='".$kd_jabatan."' AND KD_BAGIAN='".$kd_bagian."' ";
		return $this->tnde->query($sql)->result();			
	}
	
	function pegawai_by_kd_jabatan($kd_jabatan){
		$sql = "SELECT A.NIP, A.KD_PEGAWAI, A.KD_JABATAN, A.NM_PEGAWAI, B.NM_JABATAN FROM D_PEGAWAI A, MD_JABATAN B WHERE A.KD_JABATAN='".$kd_jabatan."' AND B.KD_JABATAN=A.KD_JABATAN";
		return $this->tnde->query($sql)->result();			
	}
	
	function pegawai_by_kd_jabatan2($kd_jabatan){
		$sql = "SELECT A.NIP, A.KD_PEGAWAI, A.KD_JABATAN, A.NM_PEGAWAI, B.NM_JABATAN, B.MEMBAWAHI, A.KD_BAGIAN, C.NM_BAGIAN, A.KD_TU_BAGIAN, D.NM_TU_BAGIAN, 
									D.KD_TU_PENOMORAN 
					FROM D_PEGAWAI A, MD_JABATAN B, MD_BAGIAN C, MD_TU_BAGIAN D 
					WHERE A.KD_JABATAN='".$kd_jabatan."' AND B.KD_JABATAN=A.KD_JABATAN AND C.KD_BAGIAN=A.KD_BAGIAN AND D.KD_TU_BAGIAN=A.KD_TU_BAGIAN";
		return $this->tnde->query($sql)->row_array();			
	}
	
	function detail_ts_by_id_distribusi_surat($id_distribusi_surat){
		$sql = "SELECT A.ID_SURAT_MASUK, A.INDEKS_BERKAS, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.KODE, A.PERIHAL, A.PENCATAT_SURAT, A.PENGARAH_SURAT, A.DARI, A.KEPADA, A.NO_SURAT, A.CATATAN, A.KD_TU_BAGIAN, A.LAMPIRAN, A.ID_STATUS_SM AS ID_STATUS_SM2, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, TO_CHAR(A.WAKTU_AKSES, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_AKSES, B.NM_SIFAT_SURAT, C.NM_KEAMANAN_SURAT, D.ID_DISTRIBUSI_SURAT, D.ID_STATUS_SM, D.JABATAN_PENERIMA, E.NM_STATUS_SM, F.NM_TU_BAGIAN 
		FROM MD_SURAT_MASUK A, MD_SIFAT_SURAT B, MD_KEAMANAN_SURAT C, D_DISTRIBUSI_SURAT D, MD_STATUS_SM E, MD_TU_BAGIAN F 
		WHERE D.ID_DISTRIBUSI_SURAT='".$id_distribusi_surat."' AND A.ID_SURAT_MASUK=D.ID_SURAT_MASUK AND B.KD_SIFAT_SURAT=A.KD_SIFAT_SURAT AND C.KD_KEAMANAN_SURAT=A.KD_KEAMANAN_SURAT AND E.ID_STATUS_SM=A.ID_STATUS_SM AND F.KD_TU_BAGIAN=A.KD_TU_BAGIAN";
		return $this->tnde->query($sql)->row_array();
	}
	
	function detail_sm_by_id_distribusi_surat($id_distribusi_surat){
		$sql = "SELECT A.ID_SURAT_MASUK, A.INDEKS_BERKAS, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.KD_SIFAT_SURAT, 
								A.KODE, A.KD_KEAMANAN_SURAT, A.PERIHAL, A.DARI, A.KEPADA, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, 
								A.NO_SURAT, A.CATATAN, A.KD_TU_BAGIAN, A.PENCATAT_SURAT, A.PENGARAH_SURAT, A.LAMPIRAN, A.ID_STATUS_SM AS
								ID_STATUS_SM2, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, 
								TO_CHAR(A.WAKTU_AKSES, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_AKSES, B.NM_SIFAT_SURAT, C.NM_KEAMANAN_SURAT, 
								D.ID_DISTRIBUSI_SURAT, D.ID_GRUP_SURAT, D.ID_STATUS_SM, TO_CHAR(D.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS 
								WAKTU_PENYELESAIAN, D.ISI_RINGKAS, D.ISI_DISPOSISI, D.PEMROSES_SURAT, D.JABATAN_PEMROSES, D.JABATAN_PENERIMA, 
								D.PENERIMA_SURAT, TO_CHAR(D.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, E.NM_TU_BAGIAN, F.NM_STATUS_SM, 
								G.NM_PEGAWAI AS NM_PEMROSES, H.NM_JABATAN AS NM_JABATAN_PEMROSES 
					FROM MD_SURAT_MASUK A, MD_SIFAT_SURAT B, MD_KEAMANAN_SURAT C, MD_TU_BAGIAN E, MD_STATUS_SM F, D_DISTRIBUSI_SURAT D 
							LEFT JOIN D_PEGAWAI G ON G.KD_PEGAWAI=D.PEMROSES_SURAT LEFT JOIN MD_JABATAN H ON H.KD_JABATAN=D.JABATAN_PEMROSES 
					WHERE D.ID_DISTRIBUSI_SURAT='".$id_distribusi_surat."' AND A.ID_SURAT_MASUK=D.ID_SURAT_MASUK AND 
					B.KD_SIFAT_SURAT=A.KD_SIFAT_SURAT AND C.KD_KEAMANAN_SURAT=A.KD_KEAMANAN_SURAT AND E.KD_TU_BAGIAN=A.KD_TU_BAGIAN AND F.ID_STATUS_SM=A.ID_STATUS_SM";
		return $this->tnde->query($sql)->row_array();
	}
	
	function detail_sm_by_id_distribusi_surat_v2($id_distribusi_surat){
		$sql = "SELECT A.ID_SURAT_MASUK, A.ID_SURAT_KELUAR, A.INDEKS_BERKAS, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.KD_SIFAT_SURAT, 
								A.KODE, A.KD_KEAMANAN_SURAT, A.PERIHAL, A.DARI, A.KEPADA, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, 
								A.NO_SURAT, A.CATATAN, A.UNIT_ID, A.PENCATAT_SURAT, A.PENGARAH_SURAT, A.LAMPIRAN, A.ID_STATUS_SM AS
								ID_STATUS_SM2, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, A.DIBUAT_DARI, A.ISI_SURAT, 
								TO_CHAR(A.WAKTU_AKSES, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_AKSES, B.NM_SIFAT_SURAT, C.NM_KEAMANAN_SURAT, 
								D.ID_DISTRIBUSI_SURAT, D.ID_GRUP_SURAT, D.ID_STATUS_SM, TO_CHAR(D.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS 
								WAKTU_PENYELESAIAN, D.ISI_RINGKAS, D.ISI_DISPOSISI, D.PEMROSES_SURAT, D.JABATAN_PEMROSES, D.JABATAN_PENERIMA, 
								D.PENERIMA_SURAT, TO_CHAR(D.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, F.NM_STATUS_SM, 
								G.NM_PEGAWAI AS NM_PEMROSES, H.NM_JABATAN AS NM_JABATAN_PEMROSES, D.KD_GRUP_TUJUAN, D.KD_GRUP_PENGIRIM
					FROM MD_SURAT_MASUK A, MD_SIFAT_SURAT B, MD_KEAMANAN_SURAT C, MD_STATUS_SM F, D_DISTRIBUSI_SURAT D 
							LEFT JOIN D_PEGAWAI G ON G.KD_PEGAWAI=D.PEMROSES_SURAT LEFT JOIN MD_JABATAN H ON H.KD_JABATAN=D.JABATAN_PEMROSES 
					WHERE D.ID_DISTRIBUSI_SURAT='".$id_distribusi_surat."' AND A.ID_SURAT_MASUK=D.ID_SURAT_MASUK AND 
					B.KD_SIFAT_SURAT=A.KD_SIFAT_SURAT AND C.KD_KEAMANAN_SURAT=A.KD_KEAMANAN_SURAT AND F.ID_STATUS_SM=A.ID_STATUS_SM";
		return $this->tnde->query($sql)->row_array();
	}
	
	function detail_dk_by_id_distribusi_surat($id_distribusi_surat){
		$sql = "SELECT A.ID_SURAT_MASUK, A.INDEKS_BERKAS, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.KD_SIFAT_SURAT, A.KODE, A.KD_KEAMANAN_SURAT, A.PERIHAL, A.DARI, A.KEPADA, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.NO_SURAT, A.CATATAN, A.KD_TU_BAGIAN, A.LAMPIRAN, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, B.NM_SIFAT_SURAT, C.NM_KEAMANAN_SURAT, D.ID_DISTRIBUSI_SURAT, D.ID_GRUP_SURAT, TO_CHAR(D.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, D.ISI_RINGKAS, D.ISI_DISPOSISI, D.PEMROSES_SURAT, D.JABATAN_PEMROSES, D.PENERIMA_SURAT, D.JABATAN_PENERIMA, E.NM_TU_BAGIAN, F.NM_PEGAWAI FROM MD_SURAT_MASUK A, MD_SIFAT_SURAT B, MD_KEAMANAN_SURAT C, D_DISTRIBUSI_SURAT D, MD_TU_BAGIAN E, D_PEGAWAI F WHERE D.ID_DISTRIBUSI_SURAT='".$id_distribusi_surat."' AND A.ID_SURAT_MASUK=D.ID_SURAT_MASUK AND B.KD_SIFAT_SURAT=A.KD_SIFAT_SURAT AND C.KD_KEAMANAN_SURAT=A.KD_KEAMANAN_SURAT AND E.KD_TU_BAGIAN=A.KD_TU_BAGIAN AND UPPER(F.KD_PEGAWAI)=D.PENERIMA_SURAT";
		return $this->tnde->query($sql)->row_array();
	}
	
	function detail_dk_by_id_distribusi_surat2($id_surat_masuk, $kd_status_distribusi, $jabatan_pemroses){
		$sql = "SELECT A.ID_SURAT_MASUK, A.INDEKS_BERKAS, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.KD_SIFAT_SURAT, A.KODE, A.KD_KEAMANAN_SURAT, A.PERIHAL, A.DARI, A.KEPADA, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.NO_SURAT, A.CATATAN, A.KD_TU_BAGIAN, A.PENCATAT_SURAT, A.PENGARAH_SURAT, A.LAMPIRAN, A.ID_STATUS_SM AS ID_STATUS_SM2, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, TO_CHAR(A.WAKTU_AKSES, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_AKSES, B.NM_SIFAT_SURAT, C.NM_KEAMANAN_SURAT, D.ID_DISTRIBUSI_SURAT, D.ID_GRUP_SURAT, D.ID_STATUS_SM, TO_CHAR(D.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, D.ISI_RINGKAS, D.ISI_DISPOSISI, D.PEMROSES_SURAT, D.JABATAN_PEMROSES, D.JABATAN_PENERIMA, D.PENERIMA_SURAT, TO_CHAR(D.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, E.NM_TU_BAGIAN, F.NM_STATUS_SM, G.NM_PEGAWAI AS NM_PEMROSES, H.NM_JABATAN AS NM_JABATAN_PEMROSES 
		FROM MD_SURAT_MASUK A, MD_SIFAT_SURAT B, MD_KEAMANAN_SURAT C, D_DISTRIBUSI_SURAT D, MD_TU_BAGIAN E, MD_STATUS_SM F, D_PEGAWAI G, MD_JABATAN H WHERE D.ID_SURAT_MASUK='".$id_surat_masuk."' AND D.JABATAN_PEMROSES='".$jabatan_pemroses."' AND D.KD_STATUS_DISTRIBUSI='".$kd_status_distribusi."' AND A.ID_SURAT_MASUK=D.ID_SURAT_MASUK AND B.KD_SIFAT_SURAT=A.KD_SIFAT_SURAT AND C.KD_KEAMANAN_SURAT=A.KD_KEAMANAN_SURAT AND E.KD_TU_BAGIAN=A.KD_TU_BAGIAN AND F.ID_STATUS_SM=A.ID_STATUS_SM AND G.KD_PEGAWAI=D.PEMROSES_SURAT AND H.KD_JABATAN=D.JABATAN_PEMROSES";
		return $this->tnde->query($sql)->row_array();
	}
	
	function detail_dk_by_id_distribusi_surat3($id_surat_masuk, $jabatan_pemroses){
		$sql = "SELECT A.ID_SURAT_MASUK, A.INDEKS_BERKAS, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.KD_SIFAT_SURAT, A.KODE, A.KD_KEAMANAN_SURAT, A.PERIHAL, A.DARI, A.KEPADA, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.NO_SURAT, A.CATATAN, A.KD_TU_BAGIAN, A.PENCATAT_SURAT, A.PENGARAH_SURAT, A.LAMPIRAN, A.ID_STATUS_SM AS ID_STATUS_SM2, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, TO_CHAR(A.WAKTU_AKSES, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_AKSES, B.NM_SIFAT_SURAT, C.NM_KEAMANAN_SURAT, D.ID_DISTRIBUSI_SURAT, D.ID_GRUP_SURAT, D.ID_STATUS_SM, TO_CHAR(D.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, D.ISI_RINGKAS, D.ISI_DISPOSISI, D.PEMROSES_SURAT, D.JABATAN_PEMROSES, D.JABATAN_PENERIMA, D.PENERIMA_SURAT, TO_CHAR(D.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, D.KD_STATUS_DISTRIBUSI, E.NM_TU_BAGIAN, F.NM_STATUS_SM, G.NM_PEGAWAI AS NM_PEMROSES, H.NM_JABATAN AS NM_JABATAN_PEMROSES 
		FROM MD_SURAT_MASUK A, MD_SIFAT_SURAT B, MD_KEAMANAN_SURAT C, D_DISTRIBUSI_SURAT D, MD_TU_BAGIAN E, MD_STATUS_SM F, D_PEGAWAI G, MD_JABATAN H WHERE D.ID_SURAT_MASUK='".$id_surat_masuk."' AND D.JABATAN_PEMROSES='".$jabatan_pemroses."' AND (D.KD_STATUS_DISTRIBUSI ='D' OR D.KD_STATUS_DISTRIBUSI='TM') AND A.ID_SURAT_MASUK=D.ID_SURAT_MASUK AND B.KD_SIFAT_SURAT=A.KD_SIFAT_SURAT AND C.KD_KEAMANAN_SURAT=A.KD_KEAMANAN_SURAT AND E.KD_TU_BAGIAN=A.KD_TU_BAGIAN AND F.ID_STATUS_SM=A.ID_STATUS_SM AND G.KD_PEGAWAI=D.PEMROSES_SURAT AND H.KD_JABATAN=D.JABATAN_PEMROSES";
		return $this->tnde->query($sql)->row_array();
	}
	
	function detail_dk_by_id_distribusi_surat4($id_surat_masuk, $pemroses_surat){
		$sql = "SELECT A.ID_SURAT_MASUK, A.INDEKS_BERKAS, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.KD_SIFAT_SURAT, A.KODE, A.KD_KEAMANAN_SURAT, A.PERIHAL, A.DARI, A.KEPADA, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.NO_SURAT, A.CATATAN, A.KD_TU_BAGIAN, A.PENCATAT_SURAT, A.PENGARAH_SURAT, A.LAMPIRAN, A.ID_STATUS_SM AS ID_STATUS_SM2, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, TO_CHAR(A.WAKTU_AKSES, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_AKSES, B.NM_SIFAT_SURAT, C.NM_KEAMANAN_SURAT, D.ID_DISTRIBUSI_SURAT, D.ID_GRUP_SURAT, D.ID_STATUS_SM, TO_CHAR(D.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, D.ISI_RINGKAS, D.ISI_DISPOSISI, D.PEMROSES_SURAT, D.JABATAN_PEMROSES, D.JABATAN_PENERIMA, D.PENERIMA_SURAT, TO_CHAR(D.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, D.KD_STATUS_DISTRIBUSI, E.NM_TU_BAGIAN, F.NM_STATUS_SM, G.NM_PEGAWAI AS NM_PEMROSES, H.NM_JABATAN AS NM_JABATAN_PEMROSES 
		FROM MD_SURAT_MASUK A, MD_SIFAT_SURAT B, MD_KEAMANAN_SURAT C, D_DISTRIBUSI_SURAT D, MD_TU_BAGIAN E, MD_STATUS_SM F, D_PEGAWAI G, MD_JABATAN H WHERE D.ID_SURAT_MASUK='".$id_surat_masuk."' AND D.PEMROSES_SURAT='".$pemroses_surat."' AND (D.KD_STATUS_DISTRIBUSI ='D' OR D.KD_STATUS_DISTRIBUSI='TM') AND A.ID_SURAT_MASUK=D.ID_SURAT_MASUK AND B.KD_SIFAT_SURAT=A.KD_SIFAT_SURAT AND C.KD_KEAMANAN_SURAT=A.KD_KEAMANAN_SURAT AND E.KD_TU_BAGIAN=A.KD_TU_BAGIAN AND F.ID_STATUS_SM=A.ID_STATUS_SM AND G.KD_PEGAWAI=D.PEMROSES_SURAT AND H.KD_JABATAN=D.JABATAN_PEMROSES";
		return $this->tnde->query($sql)->row_array();
	}
	
	function detail_dk_by_id_ds_sebelumnya($id_distribusi_surat){
		$sql = "SELECT A.ID_SURAT_MASUK, A.INDEKS_BERKAS, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.KD_SIFAT_SURAT, A.KODE, A.KD_KEAMANAN_SURAT, A.PERIHAL, A.DARI, A.KEPADA, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.NO_SURAT, A.CATATAN, A.KD_TU_BAGIAN, A.PENCATAT_SURAT, A.PENGARAH_SURAT, A.LAMPIRAN, A.ID_STATUS_SM AS ID_STATUS_SM2, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, TO_CHAR(A.WAKTU_AKSES, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_AKSES, B.NM_SIFAT_SURAT, C.NM_KEAMANAN_SURAT, D.ID_DISTRIBUSI_SURAT, D.ID_GRUP_SURAT, D.ID_STATUS_SM, TO_CHAR(D.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, D.ISI_RINGKAS, D.ISI_DISPOSISI, D.PEMROSES_SURAT, D.JABATAN_PEMROSES, D.JABATAN_PENERIMA, D.PENERIMA_SURAT, TO_CHAR(D.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, D.KD_STATUS_DISTRIBUSI, E.NM_TU_BAGIAN, F.NM_STATUS_SM, G.NM_PEGAWAI AS NM_PEMROSES, H.NM_JABATAN AS NM_JABATAN_PEMROSES 
		FROM MD_SURAT_MASUK A, MD_SIFAT_SURAT B, MD_KEAMANAN_SURAT C, D_DISTRIBUSI_SURAT D, MD_TU_BAGIAN E, MD_STATUS_SM F, D_PEGAWAI G, MD_JABATAN H WHERE D.ID_DS_SEBELUMNYA='".$id_distribusi_surat."' AND (D.KD_STATUS_DISTRIBUSI ='D' OR D.KD_STATUS_DISTRIBUSI='TM') AND A.ID_SURAT_MASUK=D.ID_SURAT_MASUK AND B.KD_SIFAT_SURAT=A.KD_SIFAT_SURAT AND C.KD_KEAMANAN_SURAT=A.KD_KEAMANAN_SURAT AND E.KD_TU_BAGIAN=A.KD_TU_BAGIAN AND F.ID_STATUS_SM=A.ID_STATUS_SM AND G.KD_PEGAWAI=D.PEMROSES_SURAT AND H.KD_JABATAN=D.JABATAN_PEMROSES";
		return $this->tnde->query($sql)->row_array();
	}
	
	function update_status_surat($id_surat_masuk, $data){
		$this->tnde->where('ID_SURAT_MASUK',$id_surat_masuk);
		$this->tnde->update('MD_SURAT_MASUK',$data);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function update_status_surat2($id_distribusi_surat, $data){
		$sql = "UPDATE D_DISTRIBUSI_SURAT SET ID_STATUS_SM='".$data['ID_STATUS_SM']."', WAKTU_AKSES = TO_DATE('".$data['WAKTU_AKSES']."','DD/MM/YYYY HH24:MI:SS''') WHERE ID_DISTRIBUSI_SURAT='".$id_distribusi_surat."' ";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function update_status_surat_masuk($id_surat_masuk, $data){
		$sql = "UPDATE MD_SURAT_MASUK SET ID_STATUS_SM='".$data['ID_STATUS_SM']."', WAKTU_AKSES = TO_DATE('".$data['WAKTU_AKSES']."','DD/MM/YYYY HH24:MI:SS''') WHERE ID_SURAT_MASUK='".$id_surat_masuk."' ";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}	
	}
	
	function update_status_distribusi_surat($id_distribusi_surat, $data){
		$this->tnde->where('ID_DISTRIBUSI_SURAT',$id_distribusi_surat);
		$this->tnde->update('D_DISTRIBUSI_SURAT',$data);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}	
	}
	
	function update_status_distribusi_surat2($id_distribusi_surat, $data){
		$sql = "UPDATE D_DISTRIBUSI_SURAT SET ID_STATUS_SM='".$data['ID_STATUS_SM']."', WAKTU_AKSES=TO_DATE('".date('d/m/Y H:i:s')."', 'DD/MM/YYYY HH24:MI:SS') WHERE ID_DISTRIBUSI_SURAT='".$id_distribusi_surat."' ";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}	
	}

	function get_pejabat_by_kd_jabatan($kd_jabatan){
		$sql = "SELECT A.NIP, A.KD_PEGAWAI, B.KD_JABATAN, B.NM_JABATAN, A.NM_PEGAWAI FROM D_PEGAWAI A, MD_JABATAN B WHERE B.KD_JABATAN='".$kd_jabatan."' AND A.KD_JABATAN=B.KD_JABATAN";
		return $this->tnde->query($sql)->row_array();
	}

	function insert_distribusi_surat($data){
		$this->tnde->insert('D_DISTRIBUSI_SURAT',$data);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function insert_distribusi_surat2($data){
		$sql = "INSERT INTO D_DISTRIBUSI_SURAT(ID_SURAT_MASUK, ID_GRUP_SURAT, ISI_RINGKAS, ISI_DISPOSISI, KD_STATUS_DISTRIBUSI, WAKTU_KIRIM, WAKTU_PENYELESAIAN, ID_STATUS_SM, PEMROSES_SURAT, JABATAN_PEMROSES, PENERIMA_SURAT, JABATAN_PENERIMA, ID_DS_SEBELUMNYA)VALUES('".$data['ID_SURAT_MASUK']."', '".$data['ID_GRUP_SURAT']."', '".$data['ISI_RINGKAS']."', '".$data['ISI_DISPOSISI']."', '".$data['KD_STATUS_DISTRIBUSI']."', TO_DATE('".date('d/m/Y H:i:s')."', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('".$data['WAKTU_PENYELESAIAN']."', 'DD/MM/YYYY'), '".$data['ID_STATUS_SM']."', '".$data['PEMROSES_SURAT']."', '".$data['JABATAN_PEMROSES']."', '".$data['PENERIMA_SURAT']."', '".$data['JABATAN_PENERIMA']."', '".$data['ID_DS_SEBELUMNYA']."')";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}	
	}
	
	function update_ds_by_id_surat_masuk_jab_penerima($id_surat_masuk, $kd_jabatan, $id_status_sm){
		$sql = "UPDATE D_DISTRIBUSI_SURAT SET ID_STATUS_SM='".$id_status_sm."' WHERE ID_SURAT_MASUK='".$id_surat_masuk."' AND JABATAN_PENERIMA='".$kd_jabatan."'";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function get_sk_by_kd_peg_limit($kd_pegawai, $limit){
		$sql = "SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, KEPADA, PERIHAL, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN FROM MD_SURAT_KELUAR WHERE PEMBUAT_SURAT='".$kd_pegawai."' ORDER BY WAKTU_SIMPAN DESC ) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_sk_by_kd_peg_paging($kd_pegawai, $limit, $page){
		$sql = "SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TGL_SURAT, KEPADA, PERIHAL, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN, ROWNUM R FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, KEPADA, PERIHAL, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN FROM MD_SURAT_KELUAR WHERE PEMBUAT_SURAT='".$kd_pegawai."' ORDER BY WAKTU_SIMPAN DESC ) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_sk_by_kd_jabatan_limit($kd_jabatan, $limit){
		$sql = "SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, KEPADA, PERIHAL, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN FROM MD_SURAT_KELUAR WHERE JABATAN_PEMBUAT='".$kd_jabatan."' ORDER BY WAKTU_SIMPAN DESC ) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_sk_by_kd_jabatan_paging($kd_jabatan, $limit, $page){
		$sql = "SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TGL_SURAT, KEPADA, PERIHAL, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN, ROWNUM R FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, KEPADA, PERIHAL, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN FROM MD_SURAT_KELUAR WHERE JABATAN_PEMBUAT='".$kd_jabatan."' ORDER BY WAKTU_SIMPAN DESC ) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();
	}
	
	function total_sk_by_kd_peg($kd_pegawai){
		$this->tnde->where('PEMBUAT_SURAT',$kd_pegawai);
		$this->tnde->from('MD_SURAT_KELUAR');
		return $this->tnde->count_all_results();
	}
	
	function total_surat_keluar(){
		return $this->tnde->count_all('MD_SURAT_KELUAR');
	}
	
	function total_sk_by_kd_peg_or_jab($kd_pegawai, $kd_jabatan){
		$sql = "SELECT COUNT(*) AS TOTAL FROM MD_SURAT_KELUAR WHERE PEMBUAT_SURAT='".$kd_pegawai."' OR JABATAN_PEMBUAT='".$kd_jabatan."'";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_sk_by_kd_jab($kd_jabatan){
		$this->tnde->where('JABATAN_PEMBUAT', $kd_jabatan);
		$this->tnde->from('MD_SURAT_KELUAR');
		return $this->tnde->count_all_results();
	}
	
	function insert_surat_keluar($data){
		$sql = "INSERT INTO MD_SURAT_KELUAR(ID_SURAT_KELUAR, TGL_SURAT, PERIHAL, KEPADA, NO_SURAT, KD_KEAMANAN_SURAT, KD_SIFAT_SURAT, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN, WAKTU_SIMPAN, STATUS_SK, KD_JENIS_SURAT, KD_STATUS_SIMPAN)
				VALUES('".$data['ID_SURAT_KELUAR']."', TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY'), '".$data['PERIHAL']."', '".$data['KEPADA']."', '".$data['NO_SURAT']."', '".$data['KD_KEAMANAN_SURAT']."', '".$data['KD_SIFAT_SURAT']."', '".$data['PEMBUAT_SURAT']."', '".$data['JABATAN_PEMBUAT']."', '".$data['KD_TU_BAGIAN']."', TO_DATE('".$data['WAKTU_SIMPAN']."', 'DD/MM/YYYY HH24:MI:SS'), '".$data['STATUS_SK']."', '".$data['KD_JENIS_SURAT']."', '".$data['KD_STATUS_SIMPAN']."')";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function insert_dokumen_sk($data){
		$sql = "INSERT INTO MD_DOKUMEN_SK(ID_SURAT_KELUAR, NM_DOKUMEN_SK, ISI_DOKUMEN_SK, WAKTU_SIMPAN)VALUES('".$data['ID_SURAT_KELUAR']."', '".$data['NM_DOKUMEN_SK']."', '".$data['ISI_DOKUMEN_SK']."', TO_DATE('".$data['WAKTU_SIMPAN']."', 'DD/MM/YYYY HH24:MI:SS'))";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function insert_dokumen_sk2($data){
		$sql = "INSERT INTO MD_DOKUMEN_SK2(ID_SURAT_KELUAR, NM_DOKUMEN_SK, ISI_DOKUMEN_SK, WAKTU_SIMPAN)VALUES('".$data['ID_SURAT_KELUAR']."', '".$data['NM_DOKUMEN_SK']."', '".$data['ISI_DOKUMEN_SK']."', TO_DATE('".$data['WAKTU_SIMPAN']."', 'DD/MM/YYYY HH24:MI:SS'))";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function insert_lampiran_sk($data){
		$sql = "INSERT INTO MD_LAMPIRAN_SK(ID_SURAT_KELUAR, NM_LAMPIRAN_SK, WAKTU_SIMPAN)VALUES('".$data['ID_SURAT_KELUAR']."', '".$data['NM_LAMPIRAN_SK']."', TO_DATE('".$data['WAKTU_SIMPAN']."', 'DD/MM/YYYY HH24:MI:SS'))";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function insert_lampiran_sk2($data){
		$sql = "INSERT INTO MD_LAMPIRAN_SK2(ID_SURAT_KELUAR, NM_LAMPIRAN_SK, WAKTU_SIMPAN)VALUES('".$data['ID_SURAT_KELUAR']."', '".$data['NM_LAMPIRAN_SK']."', TO_DATE('".$data['WAKTU_SIMPAN']."', 'DD/MM/YYYY HH24:MI:SS'))";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function insert_scan_sk($data){
		$sql = "INSERT INTO MD_SCAN_SK(ID_SURAT_KELUAR, NM_SCAN_SK, WAKTU_SIMPAN)VALUES('".$data['ID_SURAT_KELUAR']."', '".$data['NM_SCAN_SK']."', TO_DATE('".$data['WAKTU_SIMPAN']."', 'DD/MM/YYYY HH24:MI:SS'))";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function insert_scan_sk2($data){
		$sql = "INSERT INTO MD_SCAN_SK2(ID_SURAT_KELUAR, NM_SCAN_SK, ISI_SCAN_SK, WAKTU_SIMPAN)VALUES('".$data['ID_SURAT_KELUAR']."', '".$data['NM_SCAN_SK']."', '".$data['ISI_SCAN_SK']."', TO_DATE('".$data['WAKTU_SIMPAN']."', 'DD/MM/YYYY HH24:MI:SS'))";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function total_sk_by_perihal($perihal){
		$sql = "SELECT COUNT(*) AS TOTAL FROM MD_SURAT_KELUAR WHERE UPPER(PERIHAL) LIKE '%".$perihal."%'";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_sk_by_perihal_kd_peg($kd_pegawai, $perihal){
		$sql = "SELECT COUNT(*) AS TOTAL FROM MD_SURAT_KELUAR WHERE PEMBUAT_SURAT='".$kd_pegawai."' AND UPPER(PERIHAL) LIKE '%".$perihal."%'";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_sk_by_perihal_kd_peg_or_jab($kd_pegawai, $kd_jabatan, $perihal){
		$sql = "SELECT COUNT(*) AS TOTAL FROM MD_SURAT_KELUAR WHERE (PEMBUAT_SURAT='".$kd_pegawai."' OR JABATAN_PEMBUAT='".$kd_jabatan."') AND (UPPER(PERIHAL) LIKE '%".$perihal."%')";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_sk_by_perihal_kd_jabatan($kd_jabatan, $perihal){
		$sql = "SELECT COUNT(*) AS TOTAL FROM MD_SURAT_KELUAR WHERE JABATAN_PEMBUAT='".$kd_jabatan."' AND UPPER(PERIHAL) LIKE '%".$perihal."%'";
		return $this->tnde->query($sql)->row_array();
	}
	
	function get_sk_by_kd_peg_perihal_limit($kd_pegawai, $perihal, $limit){
		$sql = "SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, KEPADA, PERIHAL, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN FROM MD_SURAT_KELUAR WHERE UPPER(PERIHAL) LIKE '%".$perihal."%' AND PEMBUAT_SURAT='".$kd_pegawai."' ORDER BY WAKTU_SIMPAN DESC ) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_sk_by_kd_peg_perihal_paging($kd_pegawai, $perihal, $limit, $page){
		$sql = "SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TGL_SURAT, KEPADA, PERIHAL, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN, ROWNUM R FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, KEPADA, PERIHAL, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN FROM MD_SURAT_KELUAR WHERE UPPER(PERIHAL) LIKE '%".$perihal."%' AND PEMBUAT_SURAT='".$kd_pegawai."' ORDER BY WAKTU_SIMPAN DESC ) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_sk_by_kd_jabatan_perihal_limit($kd_jabatan, $perihal, $limit){
		$sql = "SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, KEPADA, PERIHAL, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN FROM MD_SURAT_KELUAR WHERE UPPER(PERIHAL) LIKE '%".$perihal."%' AND JABATAN_PEMBUAT='".$kd_jabatan."' ORDER BY WAKTU_SIMPAN DESC ) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}

	function get_sk_by_kd_jabatan_perihal_paging($kd_jabatan, $perihal, $limit, $page){
		$sql = "SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TGL_SURAT, KEPADA, PERIHAL, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN, ROWNUM R FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, KEPADA, PERIHAL, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN FROM MD_SURAT_KELUAR WHERE UPPER(PERIHAL) LIKE '%".$perihal."%' AND JABATAN_PEMBUAT='".$kd_jabatan."' ORDER BY WAKTU_SIMPAN DESC ) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();
	}
	
	function update_sk_by_id_surat_keluar($data){
		$sql = "UPDATE MD_SURAT_KELUAR SET TGL_SURAT=TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY'), PERIHAL='".$data['PERIHAL']."', KEPADA='".$data['KEPADA']."', NO_SURAT='".$data['NO_SURAT']."', KD_KEAMANAN_SURAT='".$data['KD_KEAMANAN_SURAT']."', KD_SIFAT_SURAT='".$data['KD_SIFAT_SURAT']."', STATUS_SK='".$data['STATUS_SK']."', KD_JENIS_SURAT='".$data['KD_JENIS_SURAT']."' WHERE ID_SURAT_KELUAR='".$data['ID_SURAT_KELUAR']."' ";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function detail_sk_by_id_surat_keluar($id_surat_keluar){
		$sql = "SELECT A.ID_SURAT_KELUAR, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.PERIHAL, A.KEPADA, A.NO_SURAT, A.KD_KEAMANAN_SURAT, A.KD_SIFAT_SURAT, A.PEMBUAT_SURAT, A.STATUS_SK, A.KD_JENIS_SURAT, B.KD_PEGAWAI, B.NM_PEGAWAI, A.JABATAN_PEMBUAT, C.KD_JABATAN, C.NM_JABATAN, A.KD_TU_BAGIAN, D.NM_TU_BAGIAN, TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN, E.NM_KEAMANAN_SURAT, F.NM_SIFAT_SURAT, G.NM_JENIS_SURAT FROM MD_SURAT_KELUAR A, D_PEGAWAI B, MD_JABATAN C, MD_TU_BAGIAN D, MD_KEAMANAN_SURAT E, MD_SIFAT_SURAT F, MD_JENIS_SURAT G WHERE A.ID_SURAT_KELUAR='".$id_surat_keluar."' AND UPPER(B.KD_PEGAWAI)=A.PEMBUAT_SURAT AND C.KD_JABATAN=A.JABATAN_PEMBUAT AND D.KD_TU_BAGIAN=A.KD_TU_BAGIAN AND E.KD_KEAMANAN_SURAT=A.KD_KEAMANAN_SURAT AND F.KD_SIFAT_SURAT=A.KD_SIFAT_SURAT AND G.KD_JENIS_SURAT=A.KD_JENIS_SURAT";
		return $this->tnde->query($sql)->row_array();
	}
	
	function update_lampiran_sk($id_lampiran_sk, $data){
		$sql = "UPDATE MD_LAMPIRAN_SK SET NM_LAMPIRAN_SK='".$data['NM_LAMPIRAN_SK']."', WAKTU_SIMPAN=TO_DATE('".$data['WAKTU_SIMPAN']."', 'DD/MM/YYYY HH24:MI:SS') WHERE ID_LAMPIRAN_SK='".$id_lampiran_sk."' ";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function update_lampiran_sk_2($id_lampiran_sk, $data){
		$sql = "UPDATE MD_LAMPIRAN_SK2 SET NM_LAMPIRAN_SK='".$data['NM_LAMPIRAN_SK']."', WAKTU_SIMPAN=TO_DATE('".$data['WAKTU_SIMPAN']."', 'DD/MM/YYYY HH24:MI:SS') WHERE ID_LAMPIRAN_SK='".$id_lampiran_sk."' ";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function update_dokumen_sk($id_dokumen_sk, $data){
		$sql = "UPDATE MD_DOKUMEN_SK2 SET NM_DOKUMEN_SK='".$data['NM_DOKUMEN_SK']."', ISI_DOKUMEN_SK='".$data['ISI_DOKUMEN_SK']."', WAKTU_SIMPAN=TO_DATE('".$data['WAKTU_SIMPAN']."', 'DD/MM/YYYY HH24:MI:SS') WHERE ID_DOKUMEN_SK='".$id_dokumen_sk."' ";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function update_scan_sk($id_scan_sk, $data){
		$sql = "UPDATE MD_SCAN_SK SET NM_SCAN_SK='".$data['NM_SCAN_SK']."', WAKTU_SIMPAN=TO_DATE('".$data['WAKTU_SIMPAN']."', 'DD/MM/YYYY HH24:MI:SS') WHERE ID_SCAN_SK='".$id_scan_sk."' ";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function update_scan_sk_2($id_scan_sk, $data){
		$sql = "UPDATE MD_SCAN_SK2 SET NM_SCAN_SK='".$data['NM_SCAN_SK']."', ISI_SCAN_SK='".$data['ISI_SCAN_SK']."', WAKTU_SIMPAN=TO_DATE('".$data['WAKTU_SIMPAN']."', 'DD/MM/YYYY HH24:MI:SS') WHERE ID_SCAN_SK='".$id_scan_sk."' ";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function get_dokumen_sk($id_surat_keluar){
		$sql = "SELECT ID_DOKUMEN_SK, ID_SURAT_KELUAR, NM_DOKUMEN_SK, TO_CHAR(WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN FROM MD_DOKUMEN_SK WHERE ID_SURAT_KELUAR='".$id_surat_keluar."' ORDER BY WAKTU_SIMPAN DESC";
		return $this->tnde->query($sql)->result();
	}
	
	function get_dokumen_sk2($id_surat_keluar){
		$sql = "SELECT ID_DOKUMEN_SK, ID_SURAT_KELUAR, NM_DOKUMEN_SK, TO_CHAR(WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN, ISI_DOKUMEN_SK, TIPE_FILE, TIPE_DATA FROM MD_DOKUMEN_SK2 WHERE ID_SURAT_KELUAR='".$id_surat_keluar."' ORDER BY WAKTU_SIMPAN DESC";
		return $this->tnde->query($sql)->result();
	}
	
	function get_lampiran_sk($id_surat_keluar){
		$sql = "SELECT ID_LAMPIRAN_SK, ID_SURAT_KELUAR, NM_LAMPIRAN_SK, TO_CHAR(WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN FROM MD_LAMPIRAN_SK WHERE ID_SURAT_KELUAR='".$id_surat_keluar."' ORDER BY WAKTU_SIMPAN ASC";
		return $this->tnde->query($sql)->result();
	}
	
	function get_lampiran_sk2($id_surat_keluar){
		$sql = "SELECT ID_LAMPIRAN_SK, ID_SURAT_KELUAR, NM_LAMPIRAN_SK, TO_CHAR(WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN, TIPE_FILE, TIPE_DATA FROM MD_LAMPIRAN_SK2 WHERE ID_SURAT_KELUAR='".$id_surat_keluar."' ORDER BY WAKTU_SIMPAN ASC";
		return $this->tnde->query($sql)->result();
	}
	
	function get_scan_sk($id_surat_keluar){
		$sql = "SELECT ID_SCAN_SK, ID_SURAT_KELUAR, NM_SCAN_SK, TO_CHAR(WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN FROM MD_SCAN_SK WHERE ID_SURAT_KELUAR='".$id_surat_keluar."' ORDER BY WAKTU_SIMPAN ASC";
		return $this->tnde->query($sql)->result();
	}
	
	function get_scan_sk2($id_surat_keluar){
		$sql = "SELECT ID_SCAN_SK, ID_SURAT_KELUAR, NM_SCAN_SK, ISI_SCAN_SK, TO_CHAR(WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN, TIPE_FILE, TIPE_DATA FROM MD_SCAN_SK2 WHERE ID_SURAT_KELUAR='".$id_surat_keluar."' ORDER BY WAKTU_SIMPAN ASC";
		return $this->tnde->query($sql)->result();
	}
	
	function total_pencarian_sk_all($data){
		$sql = "SELECT COUNT(DISTINCT(A.ID_SURAT_KELUAR)) AS TOTAL FROM MD_SURAT_KELUAR A, MD_DOKUMEN_SK B WHERE (UPPER(A.PEMBUAT_SURAT) LIKE '%".$data['PEMBUAT_SURAT']."%' AND A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND A.TGL_SURAT=TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY') AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND B.ID_SURAT_KELUAR=A.ID_SURAT_KELUAR) AND (UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' OR UPPER(B.ISI_DOKUMEN_SK) LIKE '%".$data['PERIHAL']."%')";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_pencarian_sk_all2($data){
		$sql = "SELECT COUNT(DISTINCT(A.ID_SURAT_KELUAR)) AS TOTAL FROM MD_SURAT_KELUAR A, MD_DOKUMEN_SK B WHERE (UPPER(A.PEMBUAT_SURAT) LIKE '%".$data['PEMBUAT_SURAT']."%' AND A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND B.ID_SURAT_KELUAR=A.ID_SURAT_KELUAR) AND (UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' OR UPPER(B.ISI_DOKUMEN_SK) LIKE '%".$data['PERIHAL']."%')";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_pencarian_sk_all3($data){
		$sql = "SELECT COUNT(*) AS TOTAL FROM MD_SURAT_KELUAR WHERE UPPER(PEMBUAT_SURAT) LIKE '%".$data['PEMBUAT_SURAT']."%' AND UPPER(KD_TU_BAGIAN) LIKE '%".$data['KD_TU_BAGIAN']."%' AND TGL_SURAT=TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY') AND UPPER(NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(PERIHAL) LIKE '%".$data['PERIHAL']."%'";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_pencarian_sk_all4($data){
		$sql = "SELECT COUNT(*) AS TOTAL FROM MD_SURAT_KELUAR WHERE UPPER(PEMBUAT_SURAT) LIKE '%".$data['PEMBUAT_SURAT']."%' AND UPPER(KD_TU_BAGIAN) LIKE '%".$data['KD_TU_BAGIAN']."%' AND UPPER(NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(PERIHAL) LIKE '%".$data['PERIHAL']."%'";
		return $this->tnde->query($sql)->row_array();
	}
	
	function pencarian_sk_all_tgl_surat_limit($data, $limit){
		$sql = "SELECT * FROM ( SELECT DISTINCT A.ID_SURAT_KELUAR, A.NO_SURAT, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.KEPADA, A.PERIHAL, TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN, A.PEMBUAT_SURAT, A.JABATAN_PEMBUAT, A.KD_TU_BAGIAN FROM MD_SURAT_KELUAR A, MD_DOKUMEN_SK B WHERE (UPPER(A.PEMBUAT_SURAT) LIKE '%".$data['PEMBUAT_SURAT']."%' AND A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND A.TGL_SURAT =TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY') AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND B.ID_SURAT_KELUAR=A.ID_SURAT_KELUAR) AND (UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' OR UPPER(B.ISI_DOKUMEN_SK) LIKE '%".$data['PERIHAL']."%')ORDER BY WAKTU_SIMPAN DESC) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function pencarian_sk_all_tgl_surat_paging($data, $limit, $page){
		$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TGL_SURAT, KEPADA, PERIHAL, WAKTU_SIMPAN, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN, ROWNUM R FROM ( SELECT DISTINCT A.ID_SURAT_KELUAR, A.NO_SURAT, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.KEPADA, A.PERIHAL, TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN, A.PEMBUAT_SURAT, A.JABATAN_PEMBUAT, A.KD_TU_BAGIAN FROM MD_SURAT_KELUAR A, MD_DOKUMEN_SK B WHERE (UPPER(A.PEMBUAT_SURAT) LIKE '%".$data['PEMBUAT_SURAT']."%' AND A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND A.TGL_SURAT = TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY') AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND B.ID_SURAT_KELUAR=A.ID_SURAT_KELUAR) AND (UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' OR UPPER(B.ISI_DOKUMEN_SK) LIKE '%".$data['PERIHAL']."%') ORDER BY WAKTU_SIMPAN DESC) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function pencarian_sk_all_limit($data, $limit){
		$sql = "SELECT * FROM ( SELECT DISTINCT A.ID_SURAT_KELUAR, A.NO_SURAT, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.KEPADA, A.PERIHAL, TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN, A.PEMBUAT_SURAT, A.JABATAN_PEMBUAT, A.KD_TU_BAGIAN FROM MD_SURAT_KELUAR A, MD_DOKUMEN_SK B WHERE (UPPER(A.PEMBUAT_SURAT) LIKE '%".$data['PEMBUAT_SURAT']."%' AND A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND B.ID_SURAT_KELUAR=A.ID_SURAT_KELUAR) AND (UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' OR UPPER(B.ISI_DOKUMEN_SK) LIKE '%".$data['PERIHAL']."%')ORDER BY WAKTU_SIMPAN DESC) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function pencarian_sk_all_paging($data, $limit, $page){
		$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TGL_SURAT, KEPADA, PERIHAL, WAKTU_SIMPAN, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN, ROWNUM R FROM ( SELECT DISTINCT A.ID_SURAT_KELUAR, A.NO_SURAT, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.KEPADA, A.PERIHAL, TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN, A.PEMBUAT_SURAT, A.JABATAN_PEMBUAT, A.KD_TU_BAGIAN FROM MD_SURAT_KELUAR A, MD_DOKUMEN_SK B WHERE (UPPER(A.PEMBUAT_SURAT) LIKE '%".$data['PEMBUAT_SURAT']."%' AND A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND B.ID_SURAT_KELUAR=A.ID_SURAT_KELUAR) AND (UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' OR UPPER(B.ISI_DOKUMEN_SK) LIKE '%".$data['PERIHAL']."%') ORDER BY WAKTU_SIMPAN DESC) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function pencarian_sk_tgl_surat_limit($data, $limit){
		$sql = "SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, KEPADA, PERIHAL, TO_CHAR(WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN FROM MD_SURAT_KELUAR WHERE UPPER(PEMBUAT_SURAT) LIKE '%".$data['PEMBUAT_SURAT']."%' AND KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND TGL_SURAT=TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY') AND UPPER(NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(PERIHAL) LIKE '%".$data['PERIHAL']."%' ORDER BY WAKTU_SIMPAN DESC )  WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function pencarian_sk_tgl_surat_paging($data, $limit, $page){
		$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TGL_SURAT, KEPADA, PERIHAL, WAKTU_SIMPAN, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN, ROWNUM R FROM ( SELECT DISTINCT A.ID_SURAT_KELUAR, A.NO_SURAT, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.KEPADA, A.PERIHAL, TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN, A.PEMBUAT_SURAT, A.JABATAN_PEMBUAT, A.KD_TU_BAGIAN FROM MD_SURAT_KELUAR A, MD_DOKUMEN_SK B WHERE UPPER(A.PEMBUAT_SURAT) LIKE '%".$data['PEMBUAT_SURAT']."%' AND A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND TGL_SURAT=TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY') AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND B.ID_SURAT_KELUAR=A.ID_SURAT_KELUAR AND UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' ORDER BY WAKTU_SIMPAN DESC) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function pencarian_sk_limit($data, $limit){
		$sql = "SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, KEPADA, PERIHAL, TO_CHAR(WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN FROM MD_SURAT_KELUAR WHERE UPPER(PEMBUAT_SURAT) LIKE '%".$data['PEMBUAT_SURAT']."%' AND KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND UPPER(NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(PERIHAL) LIKE '%".$data['PERIHAL']."%' ORDER BY WAKTU_SIMPAN DESC )  WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function pencarian_sk_paging($data, $limit, $page){
		$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TGL_SURAT, KEPADA, PERIHAL, WAKTU_SIMPAN, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN, ROWNUM R FROM ( SELECT DISTINCT A.ID_SURAT_KELUAR, A.NO_SURAT, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.KEPADA, A.PERIHAL, TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN, A.PEMBUAT_SURAT, A.JABATAN_PEMBUAT, A.KD_TU_BAGIAN FROM MD_SURAT_KELUAR A, MD_DOKUMEN_SK B WHERE UPPER(A.PEMBUAT_SURAT) LIKE '%".$data['PEMBUAT_SURAT']."%' AND A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND B.ID_SURAT_KELUAR=A.ID_SURAT_KELUAR AND UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' ORDER BY WAKTU_SIMPAN DESC) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function total_pencarian_sk_all_2($data){
		$sql = "SELECT COUNT(DISTINCT(A.ID_SURAT_KELUAR)) AS TOTAL FROM MD_SURAT_KELUAR A, MD_DOKUMEN_SK B WHERE (A.JABATAN_PEMBUAT LIKE '%".$data['JABATAN_PEMBUAT']."%' AND A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND A.TGL_SURAT=TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY') AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND B.ID_SURAT_KELUAR=A.ID_SURAT_KELUAR) AND (UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' OR UPPER(B.ISI_DOKUMEN_SK) LIKE '%".$data['PERIHAL']."%')";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_pencarian_sk_all2_2($data){
		$sql = "SELECT COUNT(DISTINCT(A.ID_SURAT_KELUAR)) AS TOTAL FROM MD_SURAT_KELUAR A, MD_DOKUMEN_SK B WHERE (A.JABATAN_PEMBUAT LIKE '%".$data['JABATAN_PEMBUAT']."%' AND A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND B.ID_SURAT_KELUAR=A.ID_SURAT_KELUAR) AND (UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' OR UPPER(B.ISI_DOKUMEN_SK) LIKE '%".$data['PERIHAL']."%')";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_pencarian_sk_all3_2($data){
		$sql = "SELECT COUNT(*) AS TOTAL FROM MD_SURAT_KELUAR WHERE JABATAN_PEMBUAT LIKE '%".$data['JABATAN_PEMBUAT']."%' AND UPPER(KD_TU_BAGIAN) LIKE '%".$data['KD_TU_BAGIAN']."%' AND TGL_SURAT=TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY') AND UPPER(NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(PERIHAL) LIKE '%".$data['PERIHAL']."%'";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_pencarian_sk_all4_2($data){
		$sql = "SELECT COUNT(*) AS TOTAL FROM MD_SURAT_KELUAR WHERE JABATAN_PEMBUAT LIKE '%".$data['JABATAN_PEMBUAT']."%' AND UPPER(KD_TU_BAGIAN) LIKE '%".$data['KD_TU_BAGIAN']."%' AND UPPER(NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(PERIHAL) LIKE '%".$data['PERIHAL']."%'";
		return $this->tnde->query($sql)->row_array();
	}
	
	function pencarian_sk_all_tgl_surat_limit2($data, $limit){
		$sql = "SELECT * FROM ( SELECT DISTINCT A.ID_SURAT_KELUAR, A.NO_SURAT, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.KEPADA, A.PERIHAL, TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN, A.PEMBUAT_SURAT, A.JABATAN_PEMBUAT, A.KD_TU_BAGIAN FROM MD_SURAT_KELUAR A, MD_DOKUMEN_SK B WHERE (A.JABATAN_PEMBUAT LIKE '%".$data['JABATAN_PEMBUAT']."%' AND A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND A.TGL_SURAT =TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY') AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND B.ID_SURAT_KELUAR=A.ID_SURAT_KELUAR) AND (UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' OR UPPER(B.ISI_DOKUMEN_SK) LIKE '%".$data['PERIHAL']."%')ORDER BY WAKTU_SIMPAN DESC) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function pencarian_sk_all_tgl_surat_paging2($data, $limit, $page){
		$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TGL_SURAT, KEPADA, PERIHAL, WAKTU_SIMPAN, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN, ROWNUM R FROM ( SELECT DISTINCT A.ID_SURAT_KELUAR, A.NO_SURAT, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.KEPADA, A.PERIHAL, TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN, A.PEMBUAT_SURAT, A.JABATAN_PEMBUAT, A.KD_TU_BAGIAN FROM MD_SURAT_KELUAR A, MD_DOKUMEN_SK B WHERE (A.JABATAN_PEMBUAT LIKE '%".$data['JABATAN_PEMBUAT']."%' AND A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND A.TGL_SURAT = TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY') AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND B.ID_SURAT_KELUAR=A.ID_SURAT_KELUAR) AND (UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' OR UPPER(B.ISI_DOKUMEN_SK) LIKE '%".$data['PERIHAL']."%') ORDER BY WAKTU_SIMPAN DESC) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function pencarian_sk_all_limit2($data, $limit){
		$sql = "SELECT * FROM ( SELECT DISTINCT A.ID_SURAT_KELUAR, A.NO_SURAT, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.KEPADA, A.PERIHAL, TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN, A.PEMBUAT_SURAT, A.JABATAN_PEMBUAT, A.KD_TU_BAGIAN FROM MD_SURAT_KELUAR A, MD_DOKUMEN_SK B WHERE (A.JABATAN_PEMBUAT LIKE '%".$data['JABATAN_PEMBUAT']."%' AND A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND B.ID_SURAT_KELUAR=A.ID_SURAT_KELUAR) AND (UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' OR UPPER(B.ISI_DOKUMEN_SK) LIKE '%".$data['PERIHAL']."%')ORDER BY WAKTU_SIMPAN DESC) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function pencarian_sk_all_paging2($data, $limit, $page){
		$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TGL_SURAT, KEPADA, PERIHAL, WAKTU_SIMPAN, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN, ROWNUM R FROM ( SELECT DISTINCT A.ID_SURAT_KELUAR, A.NO_SURAT, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.KEPADA, A.PERIHAL, TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN, A.PEMBUAT_SURAT, A.JABATAN_PEMBUAT, A.KD_TU_BAGIAN FROM MD_SURAT_KELUAR A, MD_DOKUMEN_SK B WHERE (A.JABATAN_PEMBUAT LIKE '%".$data['JABATAN_PEMBUAT']."%' AND A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND B.ID_SURAT_KELUAR=A.ID_SURAT_KELUAR) AND (UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' OR UPPER(B.ISI_DOKUMEN_SK) LIKE '%".$data['PERIHAL']."%') ORDER BY WAKTU_SIMPAN DESC) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function pencarian_sk_tgl_surat_limit2($data, $limit){
		$sql = "SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, KEPADA, PERIHAL, TO_CHAR(WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN FROM MD_SURAT_KELUAR WHERE JABATAN_PEMBUAT LIKE '%".$data['JABATAN_PEMBUAT']."%' AND KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND TGL_SURAT=TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY') AND UPPER(NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(PERIHAL) LIKE '%".$data['PERIHAL']."%' ORDER BY WAKTU_SIMPAN DESC )  WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function pencarian_sk_tgl_surat_paging2($data, $limit, $page){
		$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TGL_SURAT, KEPADA, PERIHAL, WAKTU_SIMPAN, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN, ROWNUM R FROM ( SELECT DISTINCT A.ID_SURAT_KELUAR, A.NO_SURAT, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.KEPADA, A.PERIHAL, TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN, A.PEMBUAT_SURAT, A.JABATAN_PEMBUAT, A.KD_TU_BAGIAN FROM MD_SURAT_KELUAR A, MD_DOKUMEN_SK B WHERE A.JABATAN_PEMBUAT LIKE '%".$data['JABATAN_PEMBUAT']."%' AND A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND TGL_SURAT=TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY') AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND B.ID_SURAT_KELUAR=A.ID_SURAT_KELUAR AND UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' ORDER BY WAKTU_SIMPAN DESC) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function pencarian_sk_limit2($data, $limit){
		$sql = "SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, KEPADA, PERIHAL, TO_CHAR(WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN FROM MD_SURAT_KELUAR WHERE JABATAN_PEMBUAT LIKE '%".$data['JABATAN_PEMBUAT']."%' AND KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND UPPER(NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(KEPADA) LIKE '%".$data['KEPADA']."%' AND UPPER(PERIHAL) LIKE '%".$data['PERIHAL']."%' ORDER BY WAKTU_SIMPAN DESC )  WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function pencarian_sk_paging2($data, $limit, $page){
		$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TGL_SURAT, KEPADA, PERIHAL, WAKTU_SIMPAN, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN, ROWNUM R FROM ( SELECT DISTINCT A.ID_SURAT_KELUAR, A.NO_SURAT, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.KEPADA, A.PERIHAL, TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN, A.PEMBUAT_SURAT, A.JABATAN_PEMBUAT, A.KD_TU_BAGIAN FROM MD_SURAT_KELUAR A, MD_DOKUMEN_SK B WHERE A.JABATAN_PEMBUAT LIKE '%".$data['JABATAN_PEMBUAT']."%' AND A.KD_TU_BAGIAN LIKE '%".$data['KD_TU_BAGIAN']."%' AND UPPER(A.NO_SURAT) LIKE '%".$data['NO_SURAT']."%' AND UPPER(A.KEPADA) LIKE '%".$data['KEPADA']."%' AND B.ID_SURAT_KELUAR=A.ID_SURAT_KELUAR AND UPPER(A.PERIHAL) LIKE '%".$data['PERIHAL']."%' ORDER BY WAKTU_SIMPAN DESC) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function get_surat_keluar_limit($limit){
		$sql = "SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, KEPADA, PERIHAL, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN FROM MD_SURAT_KELUAR ORDER BY WAKTU_SIMPAN DESC ) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_surat_keluar_paging($limit, $page){
		$sql = "SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TGL_SURAT, KEPADA, PERIHAL, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN, ROWNUM R FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, KEPADA, PERIHAL, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN FROM MD_SURAT_KELUAR  ORDER BY WAKTU_SIMPAN DESC ) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();	
	}
	
	function get_surat_keluar_by_perihal_limit($perihal, $limit){
		$sql = "SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, KEPADA, PERIHAL, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN FROM MD_SURAT_KELUAR WHERE UPPER(PERIHAL) LIKE '%".$perihal."%' ORDER BY WAKTU_SIMPAN DESC ) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_surat_keluar_by_perihal_paging($perihal, $limit, $page){
		$sql = "SELECT * FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TGL_SURAT, KEPADA, PERIHAL, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN, ROWNUM R FROM ( SELECT ID_SURAT_KELUAR, NO_SURAT, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, KEPADA, PERIHAL, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_TU_BAGIAN FROM MD_SURAT_KELUAR WHERE UPPER(PERIHAL) LIKE '%".$perihal."%' ORDER BY WAKTU_SIMPAN DESC ) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();	
	}
	
	function get_no_register($tahun, $waktu, $kd_tu_bagian){
		$sql = "SELECT COUNT(*) AS TOTAL FROM MD_SURAT_MASUK WHERE TO_CHAR(WAKTU_PENCATATAN, 'YYYY')='".$tahun."' AND WAKTU_PENCATATAN <= TO_DATE('".$waktu."', 'DD/MM/YYYY HH24:MI:SS') AND KD_TU_BAGIAN='".$kd_tu_bagian."'";
		return $this->tnde->query($sql)->row_array();
	}
	
	function detail_arsip_by_id_surat_masuk($id_surat_masuk){
		$sql = "SELECT A.KD_PEGAWAI, A.NM_PEGAWAI, B.NM_JABATAN FROM D_PEGAWAI A, MD_JABATAN B, MD_SURAT_MASUK C WHERE C.ID_SURAT_MASUK='".$id_surat_masuk."' AND UPPER(A.KD_PEGAWAI)=C.PENCATAT_SURAT AND B.KD_JABATAN=A.KD_JABATAN";
		return $this->tnde->query($sql)->row_array();	
	}
	
	function detail_pengarah_by_id_surat_masuk($id_surat_masuk){
		$sql = "SELECT A.KD_PEGAWAI, A.NM_PEGAWAI, B.NM_JABATAN FROM D_PEGAWAI A, MD_JABATAN B, MD_SURAT_MASUK C WHERE C.ID_SURAT_MASUK='".$id_surat_masuk."' AND UPPER(A.KD_PEGAWAI)=C.PENGARAH_SURAT AND B.KD_JABATAN=A.KD_JABATAN";
		return $this->tnde->query($sql)->row_array();		
	}
	
	function detail_sm_by_id_surat_masuk($id_surat_masuk){
		$sql = "SELECT A.ID_SURAT_MASUK, A.ID_SURAT_KELUAR, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.INDEKS_BERKAS, A.KODE, A.PERIHAL, A.DARI, A.KEPADA, A.DIBUAT_DARI, 
									TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.NO_SURAT, A.CATATAN, A.KD_KEAMANAN_SURAT, A.KD_SIFAT_SURAT, A.PENCATAT_SURAT, A.KD_TU_BAGIAN, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, A.PENGARAH_SURAT, A.JABATAN_PENGARAH, A.ID_STATUS_SM, TO_CHAR(A.WAKTU_AKSES, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_AKSES, A.LAMPIRAN, B.NM_KEAMANAN_SURAT, C.NM_SIFAT_SURAT, D.NM_STATUS_SM, A.UNIT_ID 
					FROM MD_SURAT_MASUK A, MD_KEAMANAN_SURAT B, MD_SIFAT_SURAT C, MD_STATUS_SM D 
					WHERE A.ID_SURAT_MASUK='".$id_surat_masuk."' AND B.KD_KEAMANAN_SURAT=A.KD_KEAMANAN_SURAT AND C.KD_SIFAT_SURAT=A.KD_SIFAT_SURAT AND D.ID_STATUS_SM=A.ID_STATUS_SM";
		return $this->tnde->query($sql)->row_array();
	}
	
	function get_penerima_ts_by_id_surat_masuk($id_surat_masuk){
		$sql = "SELECT A.PENERIMA_SURAT, A.JABATAN_PENERIMA, A.ID_STATUS_SM, TO_CHAR(A.WAKTU_AKSES, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_AKSES, B.NM_PEGAWAI, C.NM_JABATAN, D.NM_STATUS_SM
			FROM D_DISTRIBUSI_SURAT A, D_PEGAWAI B, MD_JABATAN C, MD_STATUS_SM D
			WHERE A.ID_SURAT_MASUK='".$id_surat_masuk."' AND A.KD_STATUS_DISTRIBUSI='TS' AND UPPER(B.KD_PEGAWAI)=A.PENERIMA_SURAT AND C.KD_JABATAN=A.JABATAN_PENERIMA AND D.ID_STATUS_SM=A.ID_STATUS_SM";
		return $this->tnde->query($sql)->result();
	}
	
	function get_pengolah_surat_by_id_surat_masuk($id_surat_masuk){
		$sql = "SELECT A.PENERIMA_SURAT, A.JABATAN_PENERIMA, A.ID_GRUP_SURAT, A.ID_STATUS_SM, TO_CHAR(A.WAKTU_AKSES, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_AKSES, B.NM_PEGAWAI, C.NM_JABATAN, D.NM_STATUS_SM
			FROM D_DISTRIBUSI_SURAT A, D_PEGAWAI B, MD_JABATAN C, MD_STATUS_SM D
			WHERE A.ID_SURAT_MASUK='".$id_surat_masuk."' AND A.KD_STATUS_DISTRIBUSI='PS' AND UPPER(B.KD_PEGAWAI)=A.PENERIMA_SURAT AND C.KD_JABATAN=A.JABATAN_PENERIMA AND D.ID_STATUS_SM=A.ID_STATUS_SM";
		return $this->tnde->query($sql)->result();
	}
	
	function get_penerima_disposisi_by_id_grup_surat($id_surat_masuk, $id_grup_surat){
		$sql = "SELECT A.PENERIMA_SURAT, A.JABATAN_PENERIMA, A.ID_STATUS_SM, TO_CHAR(A.WAKTU_AKSES, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_AKSES, B.NM_PEGAWAI, C.NM_JABATAN, D.NM_STATUS_SM
			FROM D_DISTRIBUSI_SURAT A, D_PEGAWAI B, MD_JABATAN C, MD_STATUS_SM D
			WHERE A.ID_SURAT_MASUK='".$id_surat_masuk."' AND A.ID_GRUP_SURAT='".$id_grup_surat."' AND A.KD_STATUS_DISTRIBUSI='D' AND UPPER(B.KD_PEGAWAI)=A.PENERIMA_SURAT AND C.KD_JABATAN=A.JABATAN_PENERIMA AND D.ID_STATUS_SM=A.ID_STATUS_SM ORDER BY A.WAKTU_KIRIM ASC";
		return $this->tnde->query($sql)->result();		
	}
	
	function get_disposisi_masuk($id_surat_masuk, $id_grup_surat, $jabatan_penerima, $kd_status_distribusi){
		$data['ID_SURAT_MASUK'] = $id_surat_masuk;
		$data['ID_GRUP_SURAT'] = $id_grup_surat;
		$data['JABATAN_PENERIMA'] = $jabatan_penerima;
		$data['KD_STATUS_DISTRIBUSI'] = $kd_status_distribusi;
		return $this->tnde->get_where('D_DISTRIBUSI_SURAT', $data)->row_array();
	}
	
	function get_disposisi_masuk2($id_surat_masuk, $id_grup_surat, $jabatan_pemroses, $kd_status_distribusi){
		$data['ID_SURAT_MASUK'] = $id_surat_masuk;
		$data['ID_GRUP_SURAT'] = $id_grup_surat;
		$data['JABATAN_PEMROSES'] = $jabatan_pemroses;
		$data['KD_STATUS_DISTRIBUSI'] = $kd_status_distribusi;
		return $this->tnde->get_where('D_DISTRIBUSI_SURAT', $data)->result();
	}
	
	function get_disposisi_masuk3($id_surat_masuk, $id_grup_surat, $jabatan_pemroses, $kd_status_distribusi){
		$sql = "SELECT A.ID_SURAT_MASUK, A.INDEKS_BERKAS, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.KD_SIFAT_SURAT, A.KODE, A.KD_KEAMANAN_SURAT, A.PERIHAL, A.DARI, A.KEPADA, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.NO_SURAT, A.CATATAN, A.KD_TU_BAGIAN, A.PENCATAT_SURAT, A.PENGARAH_SURAT, A.LAMPIRAN, A.ID_STATUS_SM AS ID_STATUS_SM2, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, TO_CHAR(A.WAKTU_AKSES, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_AKSES, B.NM_SIFAT_SURAT, C.NM_KEAMANAN_SURAT, D.ID_DISTRIBUSI_SURAT, D.ID_GRUP_SURAT, D.ID_STATUS_SM, TO_CHAR(D.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, D.ISI_RINGKAS, D.ISI_DISPOSISI, D.PEMROSES_SURAT, D.JABATAN_PEMROSES, D.JABATAN_PENERIMA, D.PENERIMA_SURAT, TO_CHAR(D.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, E.NM_TU_BAGIAN, F.NM_STATUS_SM, G.NM_PEGAWAI AS NM_PEMROSES, H.NM_JABATAN AS NM_JABATAN_PEMROSES 
		FROM MD_SURAT_MASUK A, MD_SIFAT_SURAT B, MD_KEAMANAN_SURAT C, D_DISTRIBUSI_SURAT D, MD_TU_BAGIAN E, MD_STATUS_SM F, D_PEGAWAI G, MD_JABATAN H WHERE D.ID_SURAT_MASUK='".$id_surat_masuk."' AND D.ID_GRUP_SURAT='".$id_grup_surat."' AND D.JABATAN_PENERIMA='".$jabatan_pemroses."' AND D.KD_STATUS_DISTRIBUSI='".$kd_status_distribusi."' AND A.ID_SURAT_MASUK=D.ID_SURAT_MASUK AND B.KD_SIFAT_SURAT=A.KD_SIFAT_SURAT AND C.KD_KEAMANAN_SURAT=A.KD_KEAMANAN_SURAT AND E.KD_TU_BAGIAN=A.KD_TU_BAGIAN AND F.ID_STATUS_SM=A.ID_STATUS_SM AND G.KD_PEGAWAI=D.PEMROSES_SURAT AND H.KD_JABATAN=D.JABATAN_PEMROSES";
		return $this->tnde->query($sql)->row_array();
	}
	
	function get_disposisi_masuk3_v2($id_surat_masuk, $id_grup_surat, $jabatan_pemroses, $kd_status_distribusi){
		$sql = "SELECT A.ID_SURAT_MASUK, A.INDEKS_BERKAS, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.KD_SIFAT_SURAT, A.KODE, 
									A.KD_KEAMANAN_SURAT, A.PERIHAL, A.DARI, A.KEPADA, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.NO_SURAT, A.CATATAN, A.UNIT_ID, A.PENCATAT_SURAT, A.PENGARAH_SURAT, A.LAMPIRAN, A.ID_STATUS_SM AS ID_STATUS_SM2, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, TO_CHAR(A.WAKTU_AKSES, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_AKSES, B.NM_SIFAT_SURAT, C.NM_KEAMANAN_SURAT, D.ID_DISTRIBUSI_SURAT, D.ID_GRUP_SURAT, D.ID_STATUS_SM, TO_CHAR(D.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, D.ISI_RINGKAS, D.ISI_DISPOSISI, D.PEMROSES_SURAT, D.JABATAN_PEMROSES, D.JABATAN_PENERIMA, D.PENERIMA_SURAT, TO_CHAR(D.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, F.NM_STATUS_SM, G.NM_PEGAWAI AS NM_PEMROSES, H.NM_JABATAN AS NM_JABATAN_PEMROSES 
					FROM MD_SURAT_MASUK A, MD_SIFAT_SURAT B, MD_KEAMANAN_SURAT C, MD_STATUS_SM F, D_DISTRIBUSI_SURAT D LEFT JOIN D_PEGAWAI G ON 
								G.KD_PEGAWAI=D.PEMROSES_SURAT LEFT JOIN MD_JABATAN H ON H.KD_JABATAN=D.JABATAN_PEMROSES
					WHERE D.ID_SURAT_MASUK='".$id_surat_masuk."' AND D.ID_GRUP_SURAT='".$id_grup_surat."' AND D.JABATAN_PENERIMA='".$jabatan_pemroses."' AND D.KD_STATUS_DISTRIBUSI='".$kd_status_distribusi."' AND A.ID_SURAT_MASUK=D.ID_SURAT_MASUK AND B.KD_SIFAT_SURAT=A.KD_SIFAT_SURAT AND C.KD_KEAMANAN_SURAT=A.KD_KEAMANAN_SURAT AND F.ID_STATUS_SM=A.ID_STATUS_SM";
		return $this->tnde->query($sql)->row_array();
	}
	
	function get_pejabat(){
		$sql = "SELECT A.KD_PEGAWAI, A.NM_PEGAWAI, A.KD_JABATAN, B.NM_JABATAN FROM D_PEGAWAI A, MD_JABATAN B WHERE A.KD_JABATAN != '193' AND A.KD_JABATAN != '194' AND B.KD_JABATAN=A.KD_JABATAN";
		return $this->tnde->query($sql)->result();
	}
	
	function detail_dk_by_id_surat_masuk_v2($id_surat_masuk, $pemroses_surat, $kd_status_distribusi){
		$sql = "SELECT A.ID_SURAT_MASUK, A.INDEKS_BERKAS, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.KD_SIFAT_SURAT, A.KODE, 
									A.KD_KEAMANAN_SURAT, A.PERIHAL, A.DARI, A.KEPADA, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.NO_SURAT, A.CATATAN, A.UNIT_ID, A.LAMPIRAN, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, B.NM_SIFAT_SURAT, C.NM_KEAMANAN_SURAT, D.ID_DISTRIBUSI_SURAT, D.ID_GRUP_SURAT, TO_CHAR(D.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, D.ISI_RINGKAS, D.ISI_DISPOSISI, D.PEMROSES_SURAT, D.JABATAN_PEMROSES, D.PENERIMA_SURAT, D.JABATAN_PENERIMA, TO_CHAR(D.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM
					FROM MD_SURAT_MASUK A, MD_SIFAT_SURAT B, MD_KEAMANAN_SURAT C, D_DISTRIBUSI_SURAT D
					WHERE D.ID_SURAT_MASUK='".$id_surat_masuk."' AND D.PEMROSES_SURAT='".$pemroses_surat."' AND D.KD_STATUS_DISTRIBUSI='".$kd_status_distribusi."' AND A.ID_SURAT_MASUK=D.ID_SURAT_MASUK AND B.KD_SIFAT_SURAT=A.KD_SIFAT_SURAT AND C.KD_KEAMANAN_SURAT=A.KD_KEAMANAN_SURAT";
		return $this->tnde->query($sql)->row_array();
	}	
	
	function detail_dk_by_id_surat_masuk2_v2($id_surat_masuk, $jabatan_pemroses, $kd_status_distribusi){
		$sql = "SELECT A.ID_SURAT_MASUK, A.INDEKS_BERKAS, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.KD_SIFAT_SURAT, A.KODE, 
									A.KD_KEAMANAN_SURAT, A.PERIHAL, A.DARI, A.KEPADA, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.NO_SURAT, A.CATATAN, A.UNIT_ID, A.LAMPIRAN, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, B.NM_SIFAT_SURAT, C.NM_KEAMANAN_SURAT, D.ID_DISTRIBUSI_SURAT, D.ID_GRUP_SURAT, TO_CHAR(D.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, D.ISI_RINGKAS, D.ISI_DISPOSISI, D.PEMROSES_SURAT, D.JABATAN_PEMROSES, D.PENERIMA_SURAT, D.JABATAN_PENERIMA, TO_CHAR(D.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, D.ID_DS_SEBELUMNYA 
					FROM MD_SURAT_MASUK A, MD_SIFAT_SURAT B, MD_KEAMANAN_SURAT C, D_DISTRIBUSI_SURAT D
					WHERE D.ID_SURAT_MASUK='".$id_surat_masuk."' AND D.JABATAN_PEMROSES='".$jabatan_pemroses."' AND D.KD_STATUS_DISTRIBUSI='".$kd_status_distribusi."' AND A.ID_SURAT_MASUK=D.ID_SURAT_MASUK AND B.KD_SIFAT_SURAT=A.KD_SIFAT_SURAT AND C.KD_KEAMANAN_SURAT=A.KD_KEAMANAN_SURAT";
		return $this->tnde->query($sql)->row_array();
	}
	
	function detail_dk_by_id_surat_masuk($id_surat_masuk, $jabatan_pemroses, $kd_status_distribusi){
		$sql = "SELECT A.ID_SURAT_MASUK, A.INDEKS_BERKAS, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.KD_SIFAT_SURAT, A.KODE, A.KD_KEAMANAN_SURAT, A.PERIHAL, A.DARI, A.KEPADA, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.NO_SURAT, A.CATATAN, A.KD_TU_BAGIAN, A.LAMPIRAN, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, B.NM_SIFAT_SURAT, C.NM_KEAMANAN_SURAT, D.ID_DISTRIBUSI_SURAT, D.ID_GRUP_SURAT, TO_CHAR(D.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, D.ISI_RINGKAS, D.ISI_DISPOSISI, D.PEMROSES_SURAT, D.JABATAN_PEMROSES, D.PENERIMA_SURAT, D.JABATAN_PENERIMA, TO_CHAR(D.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, E.NM_TU_BAGIAN 
		FROM MD_SURAT_MASUK A, MD_SIFAT_SURAT B, MD_KEAMANAN_SURAT C, D_DISTRIBUSI_SURAT D, MD_TU_BAGIAN E 
		WHERE D.ID_SURAT_MASUK='".$id_surat_masuk."' AND D.JABATAN_PEMROSES='".$jabatan_pemroses."' AND D.KD_STATUS_DISTRIBUSI='".$kd_status_distribusi."' AND A.ID_SURAT_MASUK=D.ID_SURAT_MASUK AND B.KD_SIFAT_SURAT=A.KD_SIFAT_SURAT AND C.KD_KEAMANAN_SURAT=A.KD_KEAMANAN_SURAT AND E.KD_TU_BAGIAN=A.KD_TU_BAGIAN";
		return $this->tnde->query($sql)->row_array();
	}
	
	function detail_dk_by_id_surat_masuk2($id_surat_masuk, $pemroses_surat, $kd_status_distribusi){
		$sql = "SELECT A.ID_SURAT_MASUK, A.INDEKS_BERKAS, TO_CHAR(A.TGL_MASUK, 'DD/MM/YYYY') AS TGL_MASUK, A.KD_SIFAT_SURAT, A.KODE, A.KD_KEAMANAN_SURAT, A.PERIHAL, A.DARI, A.KEPADA, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.NO_SURAT, A.CATATAN, A.KD_TU_BAGIAN, A.LAMPIRAN, TO_CHAR(A.WAKTU_PENCATATAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_PENCATATAN, B.NM_SIFAT_SURAT, C.NM_KEAMANAN_SURAT, D.ID_DISTRIBUSI_SURAT, D.ID_GRUP_SURAT, TO_CHAR(D.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, D.ISI_RINGKAS, D.ISI_DISPOSISI, D.PEMROSES_SURAT, D.JABATAN_PEMROSES, D.PENERIMA_SURAT, D.JABATAN_PENERIMA, TO_CHAR(D.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, E.NM_TU_BAGIAN 
		FROM MD_SURAT_MASUK A, MD_SIFAT_SURAT B, MD_KEAMANAN_SURAT C, D_DISTRIBUSI_SURAT D, MD_TU_BAGIAN E 
		WHERE D.ID_SURAT_MASUK='".$id_surat_masuk."' AND D.PEMROSES_SURAT='".$pemroses_surat."' AND D.KD_STATUS_DISTRIBUSI='".$kd_status_distribusi."' AND A.ID_SURAT_MASUK=D.ID_SURAT_MASUK AND B.KD_SIFAT_SURAT=A.KD_SIFAT_SURAT AND C.KD_KEAMANAN_SURAT=A.KD_KEAMANAN_SURAT AND E.KD_TU_BAGIAN=A.KD_TU_BAGIAN";
		return $this->tnde->query($sql)->row_array();
	}
	
	function get_pegawai_on_disposisi($id_surat_masuk, $id_grup_surat){
		$sql = "SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, A.PEMROSES_SURAT, A.JABATAN_PEMROSES, A.PENERIMA_SURAT, A.JABATAN_PENERIMA, A.KD_STATUS_DISTRIBUSI, B.KD_PEGAWAI, 
		B.NM_PEGAWAI, C.KD_JABATAN, C.NM_JABATAN
		FROM D_DISTRIBUSI_SURAT A, D_PEGAWAI B, MD_JABATAN C
		WHERE A.ID_SURAT_MASUK='".$id_surat_masuk."' AND A.ID_GRUP_SURAT='".$id_grup_surat."' AND (A.KD_STATUS_DISTRIBUSI='D' OR A.KD_STATUS_DISTRIBUSI='PS') AND B.KD_PEGAWAI=A.PENERIMA_SURAT 
		AND C.KD_JABATAN=A.JABATAN_PENERIMA";
		return $this->tnde->query($sql)->result();
	}
	
	function get_pegawai_on_disposisi_v2($id_surat_masuk, $id_grup_surat){
		$sql = "SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, A.PEMROSES_SURAT, A.JABATAN_PEMROSES, A.PENERIMA_SURAT, A.JABATAN_PENERIMA, 
									A.KD_STATUS_DISTRIBUSI, B.KD_PEGAWAI, B.NM_PEGAWAI, C.KD_JABATAN, C.NM_JABATAN
					FROM D_DISTRIBUSI_SURAT A LEFT JOIN D_PEGAWAI B ON B.KD_PEGAWAI=A.PENERIMA_SURAT LEFT JOIN MD_JABATAN C ON 
								C.KD_JABATAN=A.JABATAN_PENERIMA
					WHERE A.ID_SURAT_MASUK='".$id_surat_masuk."' AND A.ID_GRUP_SURAT='".$id_grup_surat."' AND (A.KD_STATUS_DISTRIBUSI='D' OR 
								A.KD_STATUS_DISTRIBUSI='PS')";
		return $this->tnde->query($sql)->result();
	}
	
	function total_tm_masuk_by_kd_pegawai($kd_pegawai){
		$data['PENERIMA_SURAT'] = $kd_pegawai;
		$data['KD_STATUS_DISTRIBUSI'] = 'TM';
		$this->tnde->where($data);
		$this->tnde->from('D_DISTRIBUSI_SURAT');
		return $this->tnde->count_all_results();
	}
	
	function total_tm_masuk_by_kd_jabatan($kd_jabatan){
		$data['JABATAN_PENERIMA'] = $kd_jabatan;
		$data['KD_STATUS_DISTRIBUSI'] = 'TM';
		$this->tnde->where($data);
		$this->tnde->from('D_DISTRIBUSI_SURAT');
		return $this->tnde->count_all_results();
	}
	
	function get_tm_masuk_by_kd_pegawai_limit($kd_pegawai, $limit){
		$sql = "SELECT * FROM ( SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, A.PEMROSES_SURAT, A.JABATAN_PEMROSES, A.KD_STATUS_DISTRIBUSI, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM,
		A.ID_STATUS_SM, B.PERIHAL, C.KD_PEGAWAI, C.NM_PEGAWAI, D.KD_JABATAN, D.NM_JABATAN, E.NM_STATUS_SM
		FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B, D_PEGAWAI C, MD_JABATAN D, MD_STATUS_SM E
		WHERE A.PENERIMA_SURAT='".$kd_pegawai."' AND A.KD_STATUS_DISTRIBUSI='TM' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND C.KD_PEGAWAI=A.PEMROSES_SURAT AND D.KD_JABATAN=A.JABATAN_PEMROSES
		 AND E.ID_STATUS_SM=A.ID_STATUS_SM order BY A.WAKTU_KIRIM DESC ) WHERE ROWNUM <= ".$limit."";
		 return $this->tnde->query($sql)->result();
	}
	
	function get_tm_masuk_by_kd_pegawai_paging($kd_pegawai, $limit, $page){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_DISTRIBUSI_SURAT, ID_SURAT_MASUK, PEMROSES_SURAT, JABATAN_PEMROSES, KD_STATUS_DISTRIBUSI, WAKTU_KIRIM, ID_STATUS_SM, PERIHAL, KD_PEGAWAI, NM_PEGAWAI, KD_JABATAN, NM_JABATAN, NM_STATUS_SM, ROWNUM R FROM ( SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, A.PEMROSES_SURAT, A.JABATAN_PEMROSES, A.KD_STATUS_DISTRIBUSI, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM,
		A.ID_STATUS_SM, B.PERIHAL, C.KD_PEGAWAI, C.NM_PEGAWAI, D.KD_JABATAN, D.NM_JABATAN, E.NM_STATUS_SM
		FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B, D_PEGAWAI C, MD_JABATAN D, MD_STATUS_SM E
		WHERE A.PENERIMA_SURAT='".$kd_pegawai."' AND A.KD_STATUS_DISTRIBUSI='TM' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND C.KD_PEGAWAI=A.PEMROSES_SURAT AND D.KD_JABATAN=A.JABATAN_PEMROSES
		 AND E.ID_STATUS_SM=A.ID_STATUS_SM order BY A.WAKTU_KIRIM DESC ) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function get_tm_masuk_by_kd_pegawai_limit_v2($kd_pegawai, $limit){
		$sql = "SELECT * FROM ( SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, A.PEMROSES_SURAT, A.JABATAN_PEMROSES, A.KD_STATUS_DISTRIBUSI, 
								TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, A.ID_STATUS_SM, B.PERIHAL, C.KD_PEGAWAI, C.NM_PEGAWAI, D.KD_JABATAN, D.NM_JABATAN, E.NM_STATUS_SM
					FROM MD_SURAT_MASUK B, MD_STATUS_SM E, D_DISTRIBUSI_SURAT A LEFT JOIN D_PEGAWAI C ON C.KD_PEGAWAI=A.PENERIMA_SURAT LEFT JOIN
								MD_JABATAN D ON D.KD_JABATAN=A.JABATAN_PEMROSES 
					WHERE A.PENERIMA_SURAT='".$kd_pegawai."' AND A.KD_STATUS_DISTRIBUSI='TM' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK
									AND E.ID_STATUS_SM=A.ID_STATUS_SM order BY A.WAKTU_KIRIM DESC ) WHERE ROWNUM <= ".$limit."";
		 return $this->tnde->query($sql)->result();
	}
	
	function get_tm_masuk_by_kd_pegawai_paging_v2($kd_pegawai, $limit, $page){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_DISTRIBUSI_SURAT, ID_SURAT_MASUK, PEMROSES_SURAT, JABATAN_PEMROSES, 
									KD_STATUS_DISTRIBUSI, WAKTU_KIRIM, ID_STATUS_SM, PERIHAL, KD_PEGAWAI, NM_PEGAWAI, KD_JABATAN, NM_JABATAN, NM_STATUS_SM, ROWNUM R FROM ( SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, A.PEMROSES_SURAT, A.JABATAN_PEMROSES, A.KD_STATUS_DISTRIBUSI, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, A.ID_STATUS_SM, B.PERIHAL, C.KD_PEGAWAI, C.NM_PEGAWAI, D.KD_JABATAN, D.NM_JABATAN, E.NM_STATUS_SM
					FROM MD_SURAT_MASUK B, MD_STATUS_SM E, D_DISTRIBUSI_SURAT A LEFT JOIN D_PEGAWAI C ON C.KD_PEGAWAI=A.PEMROSES_SURAT LEFT JOIN MD_JABATAN D ON D.KD_JABATAN=A.JABATAN_PEMROSES  
					WHERE A.PENERIMA_SURAT='".$kd_pegawai."' AND A.KD_STATUS_DISTRIBUSI='TM' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND E.ID_STATUS_SM=A.ID_STATUS_SM order BY A.WAKTU_KIRIM DESC ) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function get_tm_masuk_by_kd_jabatan_limit($kd_jabatan, $limit){
		$sql = "SELECT * FROM ( SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, A.PEMROSES_SURAT, A.JABATAN_PEMROSES, A.KD_STATUS_DISTRIBUSI, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM,
		A.ID_STATUS_SM, B.PERIHAL, C.KD_PEGAWAI, C.NM_PEGAWAI, D.KD_JABATAN, D.NM_JABATAN, E.NM_STATUS_SM
		FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B, D_PEGAWAI C, MD_JABATAN D, MD_STATUS_SM E
		WHERE A.JABATAN_PENERIMA='".$kd_jabatan."' AND A.KD_STATUS_DISTRIBUSI='TM' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND C.KD_PEGAWAI=A.PEMROSES_SURAT AND D.KD_JABATAN=A.JABATAN_PEMROSES
		 AND E.ID_STATUS_SM=A.ID_STATUS_SM order BY A.WAKTU_KIRIM DESC ) WHERE ROWNUM <= ".$limit."";
		 return $this->tnde->query($sql)->result();
	}
	
	function get_tm_masuk_by_kd_jabatan_paging($kd_jabatan, $limit, $page){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_DISTRIBUSI_SURAT, ID_SURAT_MASUK, PEMROSES_SURAT, JABATAN_PEMROSES, 
		KD_STATUS_DISTRIBUSI, WAKTU_KIRIM, ID_STATUS_SM, PERIHAL, KD_PEGAWAI, NM_PEGAWAI, KD_JABATAN, NM_JABATAN, 
		NM_STATUS_SM, ROWNUM R FROM ( SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, A.PEMROSES_SURAT, A.JABATAN_PEMROSES, 
		A.KD_STATUS_DISTRIBUSI, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM,
		A.ID_STATUS_SM, B.PERIHAL, C.KD_PEGAWAI, C.NM_PEGAWAI, D.KD_JABATAN, D.NM_JABATAN, E.NM_STATUS_SM
		FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B, D_PEGAWAI C, MD_JABATAN D, MD_STATUS_SM E
		WHERE A.JABATAN_PENERIMA='".$kd_jabatan."' AND A.KD_STATUS_DISTRIBUSI='TM' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND C.KD_PEGAWAI=A.PEMROSES_SURAT AND D.KD_JABATAN=A.JABATAN_PEMROSES
		 AND E.ID_STATUS_SM=A.ID_STATUS_SM order BY A.WAKTU_KIRIM DESC ) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function total_tm_keluar_grup_by_kd_pegawai($kd_pegawai){
		$sql = "SELECT COUNT(DISTINCT(ID_SURAT_MASUK)) AS TOTAL FROM D_DISTRIBUSI_SURAT WHERE KD_STATUS_DISTRIBUSI='TM' AND PENERIMA_SURAT='".$kd_pegawai."'";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_tm_keluar_grup_by_kd_jabatan($kd_jabatan){
		$sql = "SELECT COUNT(DISTINCT(ID_SURAT_MASUK)) AS TOTAL FROM D_DISTRIBUSI_SURAT WHERE KD_STATUS_DISTRIBUSI='TM' AND JABATAN_PENERIMA='".$kd_jabatan."'";
		return $this->tnde->query($sql)->row_array();
	}
	
	function get_tm_keluar_grup_by_kd_pegawai_limit($kd_pegawai, $limit){
		$sql = "SELECT ID_DISTRIBUSI_SURAT, ID_SURAT_MASUK, WAKTU_KIRIM, WAKTU_PENYELESAIAN, NO_SURAT, DARI, PERIHAL, ISI_DISPOSISI, ROWNUM 
					FROM ( WITH TEMP AS( SELECT * FROM (SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, 
																							TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, A.ISI_DISPOSISI, 
																							B.NO_SURAT, B.DARI, B.PERIHAL, ROW_NUMBER() 
																							OVER(PARTITION BY A.ID_SURAT_MASUK ORDER BY A.ID_DISTRIBUSI_SURAT DESC) RN 
																						FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B 
																						WHERE A.PEMROSES_SURAT='".$kd_pegawai."' AND A.KD_STATUS_DISTRIBUSI='TM' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK 
																						ORDER BY A.WAKTU_KIRIM DESC
																					)
														) SELECT * FROM TEMP WHERE RN=1 
								) 
					WHERE ROWNUM <= ".$limit."";
		 return $this->tnde->query($sql)->result();
	}
	
	function get_tm_keluar_grup_by_kd_pegawai_paging($kd_pegawai, $limit, $page){
		$sql = "SELECT * FROM ( SELECT ID_DISTRIBUSI_SURAT, ID_SURAT_MASUK, WAKTU_KIRIM, WAKTU_PENYELESAIAN, NO_SURAT, DARI, PERIHAL, ISI_DISPOSISI, ROWNUM R 
												FROM ( WITH TEMP AS( SELECT * FROM (SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, 
																																TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, A.ISI_DISPOSISI, 
																																B.NO_SURAT, B.DARI, B.PERIHAL, ROW_NUMBER() OVER(PARTITION BY A.ID_SURAT_MASUK 
																																ORDER BY A.ID_DISTRIBUSI_SURAT DESC) RN 
																													FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B 
																													WHERE A.PEMROSES_SURAT='".$kd_pegawai."' AND A.KD_STATUS_DISTRIBUSI='TM' AND 
																														B.ID_SURAT_MASUK=A.ID_SURAT_MASUK 
																														ORDER BY A.WAKTU_KIRIM DESC
																												)
																					)
																					SELECT * FROM TEMP WHERE RN=1 
															) 
												WHERE ROWNUM <= ".$limit."
											) 
									WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_tm_keluar_grup_by_kd_jabatan_limit($kd_jabatan, $limit){
		$sql = "SELECT ID_DISTRIBUSI_SURAT, ID_SURAT_MASUK, WAKTU_KIRIM, WAKTU_PENYELESAIAN, NO_SURAT, DARI, PERIHAL, ISI_DISPOSISI, ROWNUM 
					FROM ( WITH TEMP AS( SELECT * FROM (SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, 
																									TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, A.ISI_DISPOSISI, 
																									B.NO_SURAT, B.DARI, B.PERIHAL, 
																									ROW_NUMBER() OVER(PARTITION BY A.ID_SURAT_MASUK ORDER BY A.ID_DISTRIBUSI_SURAT DESC) RN 
																						FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B WHERE A.JABATAN_PEMROSES='".$kd_jabatan."' AND 
																									A.KD_STATUS_DISTRIBUSI='TM' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK 
																						ORDER BY A.WAKTU_KIRIM DESC
																					)
														) SELECT * FROM TEMP WHERE RN=1 
								) 
					WHERE ROWNUM <= ".$limit."";
		 return $this->tnde->query($sql)->result();
	}
	
	function get_tm_keluar_grup_by_kd_jabatan_paging($kd_jabatan, $limit, $page){
		$sql = "SELECT * FROM ( SELECT ID_DISTRIBUSI_SURAT, ID_SURAT_MASUK, WAKTU_KIRIM, WAKTU_PENYELESAIAN, NO_SURAT, DARI, PERIHAL, ISI_DISPOSISI, ROWNUM R 
												FROM ( WITH TEMP AS( SELECT * FROM ( SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, 
																														TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') 
																														AS WAKTU_PENYELESAIAN, A.ISI_DISPOSISI, B.NO_SURAT, B.DARI, B.PERIHAL, ROW_NUMBER() 
																														OVER(PARTITION BY A.ID_SURAT_MASUK ORDER BY A.ID_DISTRIBUSI_SURAT DESC) RN 
																													FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B 
																													WHERE A.JABATAN_PEMROSES='".$kd_jabatan."' AND A.KD_STATUS_DISTRIBUSI='TM' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK 
																													ORDER BY A.WAKTU_KIRIM DESC
																												)
																					) SELECT * FROM TEMP WHERE RN=1 
															) 
												WHERE ROWNUM <= ".$limit."
											) 
									WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_tm_grup_by_kd_pegawai_kcari_limit($kd_pegawai='', $keyword='', $limit=''){
		$sql = "SELECT ID_DISTRIBUSI_SURAT, ID_SURAT_MASUK, WAKTU_KIRIM, WAKTU_PENYELESAIAN, NO_SURAT, DARI, PERIHAL, ISI_DISPOSISI, ROWNUM 
					FROM ( WITH TEMP AS( SELECT * FROM (SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, 
																									TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, A.ISI_DISPOSISI, 
																									B.NO_SURAT, B.DARI, B.PERIHAL, 
																									ROW_NUMBER() OVER(PARTITION BY A.ID_SURAT_MASUK ORDER BY A.ID_DISTRIBUSI_SURAT DESC) RN 
																						FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B WHERE A.PEMROSES_SURAT='".$kd_pegawai."' AND 
																									A.KD_STATUS_DISTRIBUSI='TM' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND 
																									(TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$keyword."%' OR UPPER(A.ISI_DISPOSISI) LIKE '%".$keyword."%' 
																									OR UPPER(B.PERIHAL) LIKE '%".$keyword."%')
																						ORDER BY A.WAKTU_KIRIM DESC
																					)
														) SELECT * FROM TEMP WHERE RN=1 
								) 
					WHERE ROWNUM <= ".$limit."";
		 return $this->tnde->query($sql)->result();
	}
	
	function get_tm_grup_by_kd_pegawai_kcari_paging($kd_pegawai='', $keyword='', $limit='', $page=''){
		$sql = "SELECT * FROM ( SELECT ID_DISTRIBUSI_SURAT, ID_SURAT_MASUK, WAKTU_KIRIM, WAKTU_PENYELESAIAN, NO_SURAT, DARI, PERIHAL, ISI_DISPOSISI, ROWNUM R 
												FROM ( WITH TEMP AS( SELECT * FROM ( SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, 
																														TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') 
																														AS WAKTU_PENYELESAIAN, A.ISI_DISPOSISI, B.NO_SURAT, B.DARI, B.PERIHAL, ROW_NUMBER() 
																														OVER(PARTITION BY A.ID_SURAT_MASUK ORDER BY A.ID_DISTRIBUSI_SURAT DESC) RN 
																													FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B 
																													WHERE A.PEMROSES_SURAT='".$kd_pegawai."' AND A.KD_STATUS_DISTRIBUSI='TM' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND 
                                                                                                                            (TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$keyword."%' OR UPPER(A.ISI_DISPOSISI) LIKE '%".$keyword."%' OR 
                                                                                                                            UPPER(B.PERIHAL) LIKE '%".$keyword."%')
																													ORDER BY A.WAKTU_KIRIM DESC
																												)
																					) SELECT * FROM TEMP WHERE RN=1 
															) 
												WHERE ROWNUM <= ".$limit."
											) 
									WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_tm_grup_by_kd_jabatan_kcari_limit($kd_jabatan='', $keyword='', $limit=''){
		$sql = "SELECT ID_DISTRIBUSI_SURAT, ID_SURAT_MASUK, WAKTU_KIRIM, WAKTU_PENYELESAIAN, NO_SURAT, DARI, PERIHAL, ISI_DISPOSISI, ROWNUM 
					FROM ( WITH TEMP AS( SELECT * FROM (SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, 
																									TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, A.ISI_DISPOSISI, 
																									B.NO_SURAT, B.DARI, B.PERIHAL, 
																									ROW_NUMBER() OVER(PARTITION BY A.ID_SURAT_MASUK ORDER BY A.ID_DISTRIBUSI_SURAT DESC) RN 
																						FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B WHERE A.JABATAN_PEMROSES='".$kd_jabatan."' AND 
																									A.KD_STATUS_DISTRIBUSI='TM' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND 
																									(TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$keyword."%' OR UPPER(A.ISI_DISPOSISI) LIKE '%".$keyword."%' 
																									OR UPPER(B.PERIHAL) LIKE '%".$keyword."%')
																						ORDER BY A.WAKTU_KIRIM DESC
																					)
														) SELECT * FROM TEMP WHERE RN=1 
								) 
					WHERE ROWNUM <= ".$limit."";
		 return $this->tnde->query($sql)->result();
	}
	
	function get_tm_grup_by_kd_jabatan_kcari_paging($kd_jabatan='', $keyword='', $limit='', $page=''){
		$sql = "SELECT * FROM ( SELECT ID_DISTRIBUSI_SURAT, ID_SURAT_MASUK, WAKTU_KIRIM, WAKTU_PENYELESAIAN, NO_SURAT, DARI, PERIHAL, ISI_DISPOSISI, ROWNUM R 
												FROM ( WITH TEMP AS( SELECT * FROM ( SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, 
																														TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') 
																														AS WAKTU_PENYELESAIAN, A.ISI_DISPOSISI, B.NO_SURAT, B.DARI, B.PERIHAL, ROW_NUMBER() 
																														OVER(PARTITION BY A.ID_SURAT_MASUK ORDER BY A.ID_DISTRIBUSI_SURAT DESC) RN 
																													FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B 
																													WHERE A.JABATAN_PEMROSES='".$kd_jabatan."' AND A.KD_STATUS_DISTRIBUSI='TM' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND 
                                                                                                                            (TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$keyword."%' OR UPPER(A.ISI_DISPOSISI) LIKE '%".$keyword."%' OR 
                                                                                                                            UPPER(B.PERIHAL) LIKE '%".$keyword."%')
																													ORDER BY A.WAKTU_KIRIM DESC
																												)
																					) SELECT * FROM TEMP WHERE RN=1 
															) 
												WHERE ROWNUM <= ".$limit."
											) 
									WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();
	}
	
	function total_tm_by_kd_pegawai_mcari($kd_pegawai, $keyword){
		$sql = "SELECT COUNT(A.ID_DISTRIBUSI_SURAT) AS TOTAL
		FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B, D_PEGAWAI C, MD_JABATAN D
		WHERE A.PENERIMA_SURAT='".$kd_pegawai."' AND A.KD_STATUS_DISTRIBUSI='TM' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND C.KD_PEGAWAI=A.PEMROSES_SURAT AND 
		D.KD_JABATAN=A.JABATAN_PEMROSES AND (UPPER(B.NO_SURAT) LIKE '%".$keyword."%' OR UPPER(B.PERIHAL) LIKE '%".$keyword."%' OR 
		TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$keyword."%' OR UPPER(C.NM_PEGAWAI) LIKE '%".$keyword."%' OR UPPER(D.NM_JABATAN) LIKE '%".$keyword."%')";
		// $sql = "SELECT COUNT(ID_DISTRIBUSI_SURAT) TOTAL FROM ( SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, A.PEMROSES_SURAT, A.JABATAN_PEMROSES, A.KD_STATUS_DISTRIBUSI, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM,
		// A.ID_STATUS_SM, B.PERIHAL, C.KD_PEGAWAI, C.NM_PEGAWAI, D.KD_JABATAN, D.NM_JABATAN, E.NM_STATUS_SM
		// FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B, D_PEGAWAI C, MD_JABATAN D, MD_STATUS_SM E
		// WHERE A.PENERIMA_SURAT='195505131979031001' AND A.KD_STATUS_DISTRIBUSI='TM' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND C.KD_PEGAWAI=A.PEMROSES_SURAT AND D.KD_JABATAN=A.JABATAN_PEMROSES
		 // AND E.ID_STATUS_SM=A.ID_STATUS_SM AND (TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%CONTOH%' OR UPPER(C.NM_PEGAWAI) LIKE '%CONTOH%' OR UPPER(B.PERIHAL) LIKE '%CONTOH%'))
		// ";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_tm_by_kd_pegawai_mcari_v2($kd_pegawai, $keyword){
		$sql = "SELECT COUNT(A.ID_DISTRIBUSI_SURAT) AS TOTAL
		FROM MD_SURAT_MASUK B, D_DISTRIBUSI_SURAT A LEFT JOIN D_PEGAWAI C ON C.KD_PEGAWAI=A.PEMROSES_SURAT 
		WHERE A.PENERIMA_SURAT='".$kd_pegawai."' AND A.KD_STATUS_DISTRIBUSI='TM' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND 
		(UPPER(B.NO_SURAT) LIKE '%".$keyword."%' OR UPPER(B.PERIHAL) LIKE '%".$keyword."%' OR 
		TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$keyword."%' OR UPPER(C.NM_PEGAWAI) LIKE '%".$keyword."%')";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_tm_by_kd_jabatan_mcari($kd_jabatan, $keyword){
		$sql = "SELECT COUNT(A.ID_DISTRIBUSI_SURAT) AS TOTAL
		FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B, D_PEGAWAI C, MD_JABATAN D
		WHERE A.JABATAN_PENERIMA='".$kd_jabatan."' AND A.KD_STATUS_DISTRIBUSI='TM' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND C.KD_PEGAWAI=A.PEMROSES_SURAT AND 
		D.KD_JABATAN=A.JABATAN_PEMROSES AND (UPPER(B.NO_SURAT) LIKE '%".$keyword."%' OR UPPER(B.PERIHAL) LIKE '%".$keyword."%' OR 
		TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$keyword."%' OR UPPER(C.NM_PEGAWAI) LIKE '%".$keyword."%' OR UPPER(D.NM_JABATAN) LIKE '%".$keyword."%')";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_tm_by_kd_jabatan_mcari_v2($kd_jabatan, $keyword){
		$sql = "SELECT COUNT(A.ID_DISTRIBUSI_SURAT) AS TOTAL
					FROM MD_SURAT_MASUK B, D_DISTRIBUSI_SURAT A LEFT JOIN D_PEGAWAI C ON C.KD_PEGAWAI=A.PEMROSES_SURAT
					WHERE A.JABATAN_PENERIMA='".$kd_jabatan."' AND A.KD_STATUS_DISTRIBUSI='TM' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND 
					(UPPER(B.NO_SURAT) LIKE '%".$keyword."%' OR UPPER(B.PERIHAL) LIKE '%".$keyword."%' OR 
					TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$keyword."%' OR UPPER(C.NM_PEGAWAI) LIKE '%".$keyword."%')";
		return $this->tnde->query($sql)->row_array();
	}
	
	function get_tm_by_kd_pegawai_mcari_limit($kd_pegawai, $keyword, $limit){
		$sql = "SELECT * FROM (SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, 	
								A.KD_STATUS_DISTRIBUSI, A.PEMROSES_SURAT, A.JABATAN_PEMROSES, A.ID_STATUS_SM, B.NO_SURAT, B.PERIHAL, C.KD_PEGAWAI, C.NM_PEGAWAI, D.KD_JABATAN, D.NM_JABATAN, E.NM_STATUS_SM
					FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B, D_PEGAWAI C, MD_JABATAN D, MD_STATUS_SM E
					WHERE A.PENERIMA_SURAT='".$kd_pegawai."' AND A.KD_STATUS_DISTRIBUSI='TM' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND 	
							C.KD_PEGAWAI=A.PEMROSES_SURAT AND D.KD_JABATAN=A.JABATAN_PEMROSES AND E.ID_STATUS_SM=A.ID_STATUS_SM AND (UPPER(B.NO_SURAT) LIKE '%".$keyword."%' OR UPPER(B.PERIHAL) LIKE '%".$keyword."%' OR 
							TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$keyword."%' OR UPPER(C.NM_PEGAWAI) LIKE '%".$keyword."%' OR UPPER(D.NM_JABATAN) LIKE '%".$keyword."%')
							ORDER BY A.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_tm_by_kd_pegawai_mcari_paging($kd_pegawai, $keyword, $limit, $page){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_DISTRIBUSI_SURAT, ID_SURAT_MASUK, WAKTU_KIRIM, KD_STATUS_DISTRIBUSI, PEMROSES_SURAT, JABATAN_PEMROSES, 
		ID_STATUS_SM, NO_SURAT, PERIHAL, KD_PEGAWAI, NM_PEGAWAI, KD_JABATAN, NM_JABATAN, NM_STATUS_SM, ROWNUM R FROM ( SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, 
		TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, A.KD_STATUS_DISTRIBUSI, A.PEMROSES_SURAT, A.JABATAN_PEMROSES, 
		A.ID_STATUS_SM, B.NO_SURAT, B.PERIHAL, C.KD_PEGAWAI, C.NM_PEGAWAI, D.KD_JABATAN, D.NM_JABATAN, E.NM_STATUS_SM
		FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B, D_PEGAWAI C, MD_JABATAN D, MD_STATUS_SM E
		WHERE A.PENERIMA_SURAT='".$kd_pegawai."' AND A.KD_STATUS_DISTRIBUSI='TM' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND C.KD_PEGAWAI=A.PEMROSES_SURAT AND 
		D.KD_JABATAN=A.JABATAN_PEMROSES AND E.ID_STATUS_SM=A.ID_STATUS_SM AND (UPPER(B.NO_SURAT) LIKE '%".$keyword."%' OR UPPER(B.PERIHAL) LIKE '%".$keyword."%' OR 
		TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$keyword."%' OR UPPER(C.NM_PEGAWAI) LIKE '%".$keyword."%' OR UPPER(D.NM_JABATAN) LIKE '%".$keyword."%')
		order BY A.WAKTU_KIRIM DESC ) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function get_tm_by_kd_pegawai_mcari_limit_v2($kd_pegawai, $keyword, $limit){
		$sql = "SELECT * FROM (SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, 	
								A.KD_STATUS_DISTRIBUSI, A.PEMROSES_SURAT, A.JABATAN_PEMROSES, A.ID_STATUS_SM, B.NO_SURAT, B.PERIHAL, C.KD_PEGAWAI, C.NM_PEGAWAI, D.KD_JABATAN, D.NM_JABATAN, E.NM_STATUS_SM
					FROM MD_SURAT_MASUK B, MD_STATUS_SM E, D_DISTRIBUSI_SURAT A LEFT JOIN D_PEGAWAI C ON 
								C.KD_PEGAWAI=A.PEMROSES_SURAT LEFT JOIN MD_JABATAN D ON D.KD_JABATAN=A.JABATAN_PEMROSES 
					WHERE A.PENERIMA_SURAT='".$kd_pegawai."' AND A.KD_STATUS_DISTRIBUSI='TM' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND 	
							E.ID_STATUS_SM=A.ID_STATUS_SM AND (UPPER(B.NO_SURAT) LIKE '%".$keyword."%' OR UPPER(B.PERIHAL) LIKE '%".$keyword."%' OR 
							TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$keyword."%' OR UPPER(C.NM_PEGAWAI) LIKE '%".$keyword."%')
							ORDER BY A.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_tm_by_kd_pegawai_mcari_paging_v2($kd_pegawai, $keyword, $limit, $page){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_DISTRIBUSI_SURAT, ID_SURAT_MASUK, WAKTU_KIRIM, KD_STATUS_DISTRIBUSI, PEMROSES_SURAT, 
									JABATAN_PEMROSES, ID_STATUS_SM, NO_SURAT, PERIHAL, KD_PEGAWAI, NM_PEGAWAI, KD_JABATAN, NM_JABATAN, NM_STATUS_SM, ROWNUM R FROM ( SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, A.KD_STATUS_DISTRIBUSI, A.PEMROSES_SURAT, A.JABATAN_PEMROSES, A.ID_STATUS_SM, B.NO_SURAT, B.PERIHAL, C.KD_PEGAWAI, C.NM_PEGAWAI, D.KD_JABATAN, D.NM_JABATAN, E.NM_STATUS_SM
					FROM MD_SURAT_MASUK B, MD_STATUS_SM E, D_DISTRIBUSI_SURAT A LEFT JOIN D_PEGAWAI C ON C.KD_PEGAWAI=A.PEMROSES_SURAT LEFT 
								JOIN MD_JABATAN D ON D.KD_JABATAN=A.JABATAN_PEMROSES
					WHERE A.PENERIMA_SURAT='".$kd_pegawai."' AND A.KD_STATUS_DISTRIBUSI='TM' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND 
									E.ID_STATUS_SM=A.ID_STATUS_SM AND (UPPER(B.NO_SURAT) LIKE '%".$keyword."%' OR UPPER(B.PERIHAL) LIKE '%".$keyword."%' OR 
									TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$keyword."%' OR UPPER(C.NM_PEGAWAI) LIKE '%".$keyword."%')
									order BY A.WAKTU_KIRIM DESC ) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function get_tm_by_kd_jabatan_mcari_limit($kd_jabatan, $keyword, $limit){
		$sql = "SELECT * FROM (SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, A.KD_STATUS_DISTRIBUSI,
		A.PEMROSES_SURAT, A.JABATAN_PEMROSES, A.ID_STATUS_SM, B.NO_SURAT, B.PERIHAL, C.KD_PEGAWAI, C.NM_PEGAWAI, D.KD_JABATAN, D.NM_JABATAN, E.NM_STATUS_SM
		FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B, D_PEGAWAI C, MD_JABATAN D, MD_STATUS_SM E
		WHERE A.JABATAN_PENERIMA='".$kd_jabatan."' AND A.KD_STATUS_DISTRIBUSI='TM' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND C.KD_PEGAWAI=A.PEMROSES_SURAT AND 
		D.KD_JABATAN=A.JABATAN_PEMROSES AND E.ID_STATUS_SM=A.ID_STATUS_SM AND (UPPER(B.NO_SURAT) LIKE '%".$keyword."%' OR UPPER(B.PERIHAL) LIKE '%".$keyword."%' OR 
		TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$keyword."%' OR UPPER(C.NM_PEGAWAI) LIKE '%".$keyword."%' OR UPPER(D.NM_JABATAN) LIKE '%".$keyword."%')
		 ORDER BY A.WAKTU_KIRIM DESC) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_tm_by_kd_jabatan_mcari_paging($kd_jabatan, $keyword, $limit, $page){
		$sql = "SELECT * FROM (SELECT * FROM (SELECT ID_DISTRIBUSI_SURAT, ID_SURAT_MASUK, WAKTU_KIRIM, KD_STATUS_DISTRIBUSI, PEMROSES_SURAT, JABATAN_PEMROSES, 
		ID_STATUS_SM, NO_SURAT, PERIHAL, KD_PEGAWAI, NM_PEGAWAI, KD_JABATAN, NM_JABATAN, NM_STATUS_SM, ROWNUM R FROM ( SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, 
		TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, A.KD_STATUS_DISTRIBUSI, A.PEMROSES_SURAT, A.JABATAN_PEMROSES, 
		A.ID_STATUS_SM, B.NO_SURAT, B.PERIHAL, C.KD_PEGAWAI, C.NM_PEGAWAI, D.KD_JABATAN, D.NM_JABATAN, E.NM_STATUS_SM
		FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B, D_PEGAWAI C, MD_JABATAN D, MD_STATUS_SM E
		WHERE A.JABATAN_PENERIMA='".$kd_jabatan."' AND A.KD_STATUS_DISTRIBUSI='TM' AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK AND C.KD_PEGAWAI=A.PEMROSES_SURAT AND 
		D.KD_JABATAN=A.JABATAN_PEMROSES AND E.ID_STATUS_SM=A.ID_STATUS_SM AND (UPPER(B.NO_SURAT) LIKE '%".$keyword."%' OR UPPER(B.PERIHAL) LIKE '%".$keyword."%' OR 
		TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$keyword."%' OR UPPER(C.NM_PEGAWAI) LIKE '%".$keyword."%' OR UPPER(D.NM_JABATAN) LIKE '%".$keyword."%')
		order BY A.WAKTU_KIRIM DESC ) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function total_tm_grup_by_kd_pegawai_kcari($kd_pegawai, $keyword){
		$sql = "SELECT COUNT(DISTINCT(A.ID_SURAT_MASUK)) AS TOTAL FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B
		WHERE A.KD_STATUS_DISTRIBUSI='TM' AND A.PENERIMA_SURAT='".$kd_pegawai."' AND (TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$keyword."%' 
		OR UPPER(B.NO_SURAT) LIKE '%".$keyword."%' OR UPPER(B.DARI) LIKE '%".$keyword."%' OR UPPER(B.PERIHAL) LIKE '%".$keyword."%')";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_tm_grup_by_kd_pegawai_kcari2($kd_pegawai, $keyword){
		$sql = "SELECT COUNT(ID_SURAT_MASUK) TOTAL  
                    FROM ( WITH TEMP AS( SELECT * FROM (SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, 
                                                                                                    TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, A.ISI_DISPOSISI, 
                                                                                                    B.NO_SURAT, B.DARI, B.PERIHAL, 
                                                                                                    ROW_NUMBER() OVER(PARTITION BY A.ID_SURAT_MASUK ORDER BY A.ID_DISTRIBUSI_SURAT DESC) RN 
																						FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B 
                                                                                        WHERE A.PEMROSES_SURAT='".$kd_pegawai."' AND A.KD_STATUS_DISTRIBUSI='TM' AND ( UPPER(A.ISI_DISPOSISI) LIKE '%".$keyword."%' OR 
                                                                                                TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$keyword."%' OR UPPER(B.PERIHAL) LIKE '%".$keyword."%')
                                                                                                AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK
																						ORDER BY A.WAKTU_KIRIM DESC
																					)
														) SELECT * FROM TEMP WHERE RN=1
					)";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_tm_grup_by_kd_jabatan_kcari($kd_jabatan, $keyword){
		$sql = "SELECT COUNT(DISTINCT(A.ID_SURAT_MASUK)) AS TOTAL FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B
		WHERE A.KD_STATUS_DISTRIBUSI='TM' AND A.JABATAN_PENERIMA='".$kd_jabatan."' AND (TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$keyword."%' 
		OR UPPER(B.NO_SURAT) LIKE '%".$keyword."%' OR UPPER(B.DARI) LIKE '%".$keyword."%' OR UPPER(B.PERIHAL) LIKE '%".$keyword."%')";
		return $this->tnde->query($sql)->row_array();
	}	
	
	function total_tm_grup_by_kd_jabatan_kcari2($kd_jabatan, $keyword){
		$sql = "SELECT COUNT(ID_SURAT_MASUK) TOTAL  
                    FROM ( WITH TEMP AS( SELECT * FROM (SELECT A.ID_DISTRIBUSI_SURAT, A.ID_SURAT_MASUK, TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_KIRIM, 
                                                                                                    TO_CHAR(A.WAKTU_PENYELESAIAN, 'DD/MM/YYYY') AS WAKTU_PENYELESAIAN, A.ISI_DISPOSISI, 
                                                                                                    B.NO_SURAT, B.DARI, B.PERIHAL, 
                                                                                                    ROW_NUMBER() OVER(PARTITION BY A.ID_SURAT_MASUK ORDER BY A.ID_DISTRIBUSI_SURAT DESC) RN 
																						FROM D_DISTRIBUSI_SURAT A, MD_SURAT_MASUK B 
                                                                                        WHERE A.JABATAN_PEMROSES='".$kd_jabatan."' AND A.KD_STATUS_DISTRIBUSI='TM' AND ( UPPER(A.ISI_DISPOSISI) LIKE '%".$keyword."%' OR 
                                                                                                TO_CHAR(A.WAKTU_KIRIM, 'DD/MM/YYYY HH24:MI:SS') LIKE '%".$keyword."%' OR UPPER(B.PERIHAL) LIKE '%".$keyword."%')
                                                                                                AND B.ID_SURAT_MASUK=A.ID_SURAT_MASUK
																						ORDER BY A.WAKTU_KIRIM DESC
																					)
														) SELECT * FROM TEMP WHERE RN=1
								)";
		return $this->tnde->query($sql)->row_array();
	}
	
	function get_grup_tujuan(){
		$sql = "SELECT * FROM MD_GRUP_TUJUAN WHERE KD_GRUP_TUJUAN !='MHS01' AND KD_GRUP_TUJUAN !='PGW01' AND KD_GRUP_TUJUAN !='PJB01' AND KD_GRUP_TUJUAN !='EKS01'";
		return $this->tnde->query($sql)->result();
	}
	
	function get_no_urut($kd_tu_bagian, $tahun){
		$sql = "SELECT * FROM (
													SELECT B.NO_URUT, B.NO_SELA, ROWNUM R 
													FROM MD_TU_BAGIAN A, MD_SURAT_KELUAR2 B 
													WHERE A.KD_TU_PENOMORAN=(SELECT KD_TU_PENOMORAN FROM MD_TU_BAGIAN WHERE KD_TU_BAGIAN='".$kd_tu_bagian."') AND B.KD_TU_BAGIAN=A.KD_TU_BAGIAN AND TO_CHAR(B.TGL_SURAT, 'YYYY')='".$tahun."' AND B.KD_JENIS_SURAT !='1' AND B.KD_STATUS_SIMPAN='2' ORDER BY B.NO_URUT DESC, B.NO_SELA DESC
													) 
					WHERE ROWNUM=1";
		return $this->tnde->query($sql)->row_array();
	}
	
	function get_no_urut2($kd_tu_bagian, $tgl_surat){
		$sql = "SELECT * FROM (
													SELECT B.*, ROWNUM R 
													FROM MD_TU_BAGIAN A, MD_SURAT_KELUAR2 B 
													WHERE A.KD_TU_PENOMORAN=(SELECT KD_TU_PENOMORAN FROM MD_TU_BAGIAN WHERE KD_TU_BAGIAN='".$kd_tu_bagian."') AND B.KD_TU_BAGIAN=A.KD_TU_BAGIAN AND B.TGL_SURAT <= TO_DATE('".$tgl_surat."', 'DD/MM/YYYY') AND B.KD_JENIS_SURAT !='1' AND B.KD_STATUS_SIMPAN='2' ORDER BY B.NO_URUT DESC, B.NO_SELA DESC
													) 
					WHERE ROWNUM=1";
		return $this->tnde->query($sql)->row_array();
	}
	
	function get_no_urut_surat_keputusan($kd_tu_bagian, $tahun){
		$sql = "SELECT * FROM (
													SELECT B.NO_URUT, ROWNUM R 
													FROM MD_TU_BAGIAN A, MD_SURAT_KELUAR2 B 
													WHERE A.KD_TU_PENOMORAN=(SELECT KD_TU_PENOMORAN FROM MD_TU_BAGIAN WHERE KD_TU_BAGIAN='".$kd_tu_bagian."') AND B.KD_TU_BAGIAN=A.KD_TU_BAGIAN AND TO_CHAR(B.TGL_SURAT, 'YYYY')='".$tahun."' AND B.KD_JENIS_SURAT ='1' AND B.KD_STATUS_SIMPAN='2' ORDER BY B.NO_URUT DESC, B.NO_SELA DESC
													) 
					WHERE ROWNUM=1";
		return $this->tnde->query($sql)->row_array();
	}
	
	function get_no_urut_surat_keputusan2($kd_tu_bagian, $tgl_surat){
		$sql = "SELECT * FROM (
													SELECT B.*, ROWNUM R 
													FROM MD_TU_BAGIAN A, MD_SURAT_KELUAR2 B 
													WHERE A.KD_TU_PENOMORAN=(SELECT KD_TU_PENOMORAN FROM MD_TU_BAGIAN WHERE KD_TU_BAGIAN='".$kd_tu_bagian."') AND B.KD_TU_BAGIAN=A.KD_TU_BAGIAN AND B.TGL_SURAT <= TO_DATE('".$tgl_surat."', 'DD/MM/YYYY') AND B.KD_JENIS_SURAT ='1' AND B.KD_STATUS_SIMPAN='2' ORDER BY B.NO_URUT DESC, B.NO_SELA DESC
													) 
					WHERE ROWNUM=1";
		return $this->tnde->query($sql)->row_array();
	}
	
	function insert_surat_kaluar($data){
		$sql = "INSERT INTO MD_SURAT_KELUAR2(ID_SURAT_KELUAR, TGL_SURAT, PERIHAL, TEMPAT_DIBUAT, TEMPAT_TUJUAN, KD_SIFAT_SURAT, KD_JENIS_SURAT,
				STATUS_SK, PEMBUAT_SURAT, JABATAN_PEMBUAT, KD_JENIS_ORANG, KD_TU_BAGIAN, ID_PSD, ATAS_NAMA, ID_KLASIFIKASI_SURAT, NO_URUT,
				NO_SELA, NO_SURAT, ISI_SURAT, WAKTU_SIMPAN, LAMPIRAN, KD_STATUS_SIMPAN, KD_PRODI)
				VALUES('".$data['ID_SURAT_KELUAR']."', TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY'), '".$data['PERIHAL']."', '".$data['TEMPAT_DIBUAT']."', 
				'".$data['TEMPAT_TUJUAN']."', '".$data['KD_SIFAT_SURAT']."', '".$data['KD_JENIS_SURAT']."', '".$data['STATUS_SK']."', '".$data['PEMBUAT_SURAT']."', 
				'".$data['JABATAN_PEMBUAT']."', '".$data['KD_JENIS_ORANG']."', '".$data['KD_TU_BAGIAN']."', '".$data['ID_PSD']."', '".$data['ATAS_NAMA']."', 
				'".$data['ID_KLASIFIKASI_SURAT']."', '".$data['NO_URUT']."', '".$data['NO_SELA']."', '".$data['NO_SURAT']."', '".$data['ISI_SURAT']."', 
				TO_DATE('".$data['WAKTU_SIMPAN']."', 'DD/MM/YYYY HH24:MI:SS'), '".$data['LAMPIRAN']."', '".$data['KD_STATUS_SIMPAN']."', '".$data['KD_PRODI']."')";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function insert_penerima_sk($data){
		$sql = "INSERT INTO D_PENERIMA_SK(ID_SURAT_KELUAR, PENERIMA_SURAT, JABATAN_PENERIMA, KD_GRUP_TUJUAN, KD_STATUS_DISTRIBUSI, ID_STATUS_SM, KD_JENIS_TEMBUSAN)
					VALUES('".$data['ID_SURAT_KELUAR']."', '".$data['PENERIMA_SURAT']."', '".$data['JABATAN_PENERIMA']."', '".$data['KD_GRUP_TUJUAN']."', '".$data['KD_STATUS_DISTRIBUSI']."', '".$data['ID_STATUS_SM']."', '".$data['KD_JENIS_TEMBUSAN']."')";
		// $sql = "INSERT INTO D_PENERIMA_SK(ID_SURAT_KELUAR, PENERIMA_SURAT, JABATAN_PENERIMA, KD_GRUP_TUJUAN, KD_STATUS_DISTRIBUSI, ID_STATUS_SM, KD_JENIS_TEMBUSAN)
// VALUES('5346498d9fc00', 'Semua Pegawai', '194', 'SPGW01', 'TS', '1', '1')";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function total_sk_2_by_kd_pegawai($kd_pegawai){
		$sql = "SELECT COUNT(*) TOTAL FROM MD_SURAT_KELUAR2 WHERE KD_JENIS_SURAT != '1' AND PEMBUAT_SURAT = '".$kd_pegawai."'";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_sk_2_by_kd_jabatan($kd_jabatan){
		$sql = "SELECT COUNT(*) TOTAL FROM MD_SURAT_KELUAR2 WHERE KD_JENIS_SURAT != '1' AND JABATAN_PEMBUAT = '".$kd_jabatan."'";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_sk_2_by_tu_bagian_jenis_surat($tu_bagian_list, $jenis_surat_list){
		$sql = "SELECT COUNT(*) TOTAL FROM MD_SURAT_KELUAR2 WHERE KD_TU_BAGIAN IN(".$tu_bagian_list.") AND KD_JENIS_SURAT IN(".$jenis_surat_list.") ";
		return $this->tnde->query($sql)->row_array();
	}	
	
	function total_sk_2_by_tu_bagian_jenis_surat_v2($unit_list, $jenis_surat_list){
		$sql = "SELECT COUNT(*) TOTAL FROM MD_SURAT_KELUAR2 WHERE UNIT_ID IN(".$unit_list.") AND KD_JENIS_SURAT IN(".$jenis_surat_list.") ";
		return $this->tnde->query($sql)->row_array();
	}
	
	function get_sk_2_by_kd_pegawai_limit($kd_pegawai, $limit){
		$sql = "SELECT * FROM (SELECT ID_SURAT_KELUAR, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT, PERIHAL, NO_SURAT, 
					ROWNUM R FROM MD_SURAT_KELUAR2 WHERE KD_JENIS_SURAT != '1' AND PEMBUAT_SURAT='".$kd_pegawai."' ORDER BY WAKTU_SIMPAN DESC) 
				WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_sk_2_by_kd_pegawai_paging($kd_pegawai, $limit, $page){
		$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_SURAT_KELUAR, TGL_SURAT, PERIHAL, NO_SURAT, WAKTU_SIMPAN, 
					ROWNUM R FROM (SELECT ID_SURAT_KELUAR, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT, PERIHAL, NO_SURAT, 
					TO_CHAR(WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_SIMPAN FROM MD_SURAT_KELUAR2 
				WHERE KD_JENIS_SURAT != '1' AND PEMBUAT_SURAT='".$kd_pegawai."' ORDER BY WAKTU_SIMPAN DESC) 
				WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function get_sk_2_by_kd_jabatan_limit($kd_jabatan, $limit){
		$sql = "SELECT * FROM (SELECT ID_SURAT_KELUAR, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT, PERIHAL, NO_SURAT, 
					ROWNUM R FROM MD_SURAT_KELUAR2 WHERE KD_JENIS_SURAT != '1' AND JABATAN_PEMBUAT='".$kd_jabatan."' ORDER BY WAKTU_SIMPAN DESC) 
				WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_sk_2_by_kd_jabatan_paging($kd_jabatan, $limit, $page){
		$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_SURAT_KELUAR, TGL_SURAT, PERIHAL, NO_SURAT, WAKTU_SIMPAN, 
					ROWNUM R FROM (SELECT ID_SURAT_KELUAR, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT, PERIHAL, NO_SURAT, 
					TO_CHAR(WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_SIMPAN FROM MD_SURAT_KELUAR2 
				WHERE KD_JENIS_SURAT != '1' AND JABATAN_PEMBUAT='".$kd_jabatan."' ORDER BY WAKTU_SIMPAN DESC) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function get_sk_2_by_tu_bagian_jenis_surat_limit($tu_sk_list, $jenis_surat_list, $limit){
		$sql = "SELECT * FROM (SELECT A.ID_SURAT_KELUAR, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT, A.PERIHAL, A.NO_SURAT, A.KD_STATUS_SIMPAN, A.KD_JENIS_SURAT, B.NM_JENIS_SURAT, 
					ROWNUM R FROM MD_SURAT_KELUAR2 A, MD_JENIS_SURAT B WHERE A.KD_TU_BAGIAN IN(".$tu_sk_list.") AND A.KD_JENIS_SURAT IN(".$jenis_surat_list.") AND B.KD_JENIS_SURAT=A.KD_JENIS_SURAT ORDER BY A.WAKTU_SIMPAN DESC) 
				WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_sk_2_by_tu_bagian_jenis_surat_paging($tu_sk_list, $jenis_surat_list, $limit, $page){
		$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_SURAT_KELUAR, TGL_SURAT, PERIHAL, NO_SURAT, WAKTU_SIMPAN, KD_STATUS_SIMPAN, KD_JENIS_SURAT, NM_JENIS_SURAT,
					ROWNUM R FROM (SELECT A.ID_SURAT_KELUAR, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT, A.PERIHAL, A.NO_SURAT, 
					TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_SIMPAN , A.KD_STATUS_SIMPAN, A.KD_JENIS_SURAT, B.NM_JENIS_SURAT FROM MD_SURAT_KELUAR2 A, MD_JENIS_SURAT B 
				WHERE A.KD_TU_BAGIAN IN(".$tu_sk_list.") AND A.KD_JENIS_SURAT IN(".$jenis_surat_list.") AND B.KD_JENIS_SURAT=A.KD_JENIS_SURAT ORDER BY A.WAKTU_SIMPAN DESC) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function get_sk_2_by_tu_bagian_jenis_surat_limit_v2($unit_list, $jenis_surat_list, $limit){
		$sql = "SELECT * FROM (SELECT A.ID_SURAT_KELUAR, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT, A.PERIHAL, A.NO_SURAT, 
									A.KD_STATUS_SIMPAN, A.KD_JENIS_SURAT, B.NM_JENIS_SURAT, B.KD_KAT_PENOMORAN, ROWNUM R 
					FROM MD_SURAT_KELUAR2 A, MD_JENIS_SURAT B WHERE A.UNIT_ID IN(".$unit_list.") AND A.KD_JENIS_SURAT IN(".$jenis_surat_list.") AND B.KD_JENIS_SURAT=A.KD_JENIS_SURAT ORDER BY A.WAKTU_SIMPAN DESC) 
				WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_sk_2_by_tu_bagian_jenis_surat_paging_v2($unit_list, $jenis_surat_list, $limit, $page){
		$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_SURAT_KELUAR, TGL_SURAT, PERIHAL, NO_SURAT, WAKTU_SIMPAN, KD_STATUS_SIMPAN, KD_JENIS_SURAT, NM_JENIS_SURAT, KD_KAT_PENOMORAN, 
					ROWNUM R FROM (SELECT A.ID_SURAT_KELUAR, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT, A.PERIHAL, A.NO_SURAT, 
					TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_SIMPAN , A.KD_STATUS_SIMPAN, A.KD_JENIS_SURAT, B.NM_JENIS_SURAT, B.KD_KAT_PENOMORAN FROM MD_SURAT_KELUAR2 A, MD_JENIS_SURAT B 
				WHERE A.UNIT_ID IN(".$unit_list.") AND A.KD_JENIS_SURAT IN(".$jenis_surat_list.") AND B.KD_JENIS_SURAT=A.KD_JENIS_SURAT ORDER BY A.WAKTU_SIMPAN DESC) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function total_sk_2_cari_by_kd_pegawai($kd_pegawai, $q){
		$sql = "SELECT COUNT(*) AS TOTAL FROM MD_SURAT_KELUAR2 WHERE KD_JENIS_SURAT != '1' AND PEMBUAT_SURAT='".$kd_pegawai."' AND (TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') LIKE '%".$q."%' OR 
					UPPER(NO_SURAT) LIKE '%".$q."%' OR UPPER(PERIHAL) LIKE '%".$q."%' OR UPPER(ISI_SURAT) LIKE '%".$q."%')";
		return $this->tnde->query($sql)->row_array();
	}
	
	function total_sk_2_cari_by_kd_jabatan($kd_jabatan, $q){
		$sql = "SELECT COUNT(*) AS TOTAL FROM MD_SURAT_KELUAR2 WHERE KD_JENIS_SURAT != '1' AND JABATAN_PEMBUAT='".$kd_jabatan."' AND (TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') LIKE '%".$q."%' OR 
					UPPER(NO_SURAT) LIKE '%".$q."%' OR UPPER(PERIHAL) LIKE '%".$q."%' OR UPPER(ISI_SURAT) LIKE '%".$q."%')";
		return $this->tnde->query($sql)->row_array();
	}	
	
	function total_sk_2_cari_by_tu_bagian_jenis_surat($tu_bagian_list, $jenis_surat_list, $q){
		$sql = "SELECT COUNT(A.ID_SURAT_KELUAR) AS TOTAL FROM MD_SURAT_KELUAR2 A, MD_JENIS_SURAT B 
					WHERE A.KD_TU_BAGIAN IN(".$tu_bagian_list.") AND A.KD_JENIS_SURAT IN(".$jenis_surat_list.") AND B.KD_JENIS_SURAT=A.KD_JENIS_SURAT AND 
								(TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') LIKE '%".$q."%' OR UPPER(A.NO_SURAT) LIKE '%".$q."%' OR UPPER(A.PERIHAL) LIKE '%".$q."%' OR 
								UPPER(A.ISI_SURAT) LIKE '%".$q."%' OR UPPER(B.NM_JENIS_SURAT) LIKE '%".$q."%')";
		return $this->tnde->query($sql)->row_array();
	}	
	
	function total_sk_2_cari_by_tu_bagian_jenis_surat_v2($unit_list, $jenis_surat_list, $q){
		$sql = "SELECT COUNT(A.ID_SURAT_KELUAR) AS TOTAL FROM MD_SURAT_KELUAR2 A, MD_JENIS_SURAT B 
					WHERE A.UNIT_ID IN(".$unit_list.") AND A.KD_JENIS_SURAT IN(".$jenis_surat_list.") AND B.KD_JENIS_SURAT=A.KD_JENIS_SURAT AND 
								(TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') LIKE '%".$q."%' OR UPPER(A.NO_SURAT) LIKE '%".$q."%' OR UPPER(A.PERIHAL) LIKE '%".$q."%' OR 
								UPPER(A.ISI_SURAT) LIKE '%".$q."%' OR UPPER(B.NM_JENIS_SURAT) LIKE '%".$q."%')";
		return $this->tnde->query($sql)->row_array();
	}
	
	function get_sk_2_cari_by_kd_pegawai_limit($kd_pegawai, $q, $limit){
		$sql = "SELECT * FROM (SELECT ID_SURAT_KELUAR, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT, PERIHAL, NO_SURAT, ROWNUM R 
				FROM MD_SURAT_KELUAR2 WHERE KD_JENIS_SURAT != '1' AND PEMBUAT_SURAT='".$kd_pegawai."' AND (TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') LIKE '%".$q."%' OR 
					UPPER(NO_SURAT) LIKE '%".$q."%' OR UPPER(PERIHAL) LIKE '%".$q."%' OR UPPER(ISI_SURAT) LIKE '%".$q."%') ORDER BY WAKTU_SIMPAN DESC) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_sk_2_cari_by_kd_pegawai_paging($kd_pegawai, $q, $limit, $page){
		$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_SURAT_KELUAR, TGL_SURAT, PERIHAL, NO_SURAT, WAKTU_SIMPAN, 
					ROWNUM R FROM (SELECT ID_SURAT_KELUAR, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT, PERIHAL, NO_SURAT, 
					TO_CHAR(WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_SIMPAN FROM MD_SURAT_KELUAR2 
				WHERE KD_JENIS_SURAT != '1' AND PEMBUAT_SURAT='".$kd_pegawai."' AND (TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') LIKE '%".$q."%' OR 
					UPPER(NO_SURAT) LIKE '%".$q."%' OR UPPER(PERIHAL) LIKE '%".$q."%' OR UPPER(ISI_SURAT) LIKE '%".$q."%') 
					ORDER BY WAKTU_SIMPAN DESC) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function get_sk_2_cari_by_kd_jabatan_limit($kd_jabatan, $q, $limit){
		$sql = "SELECT * FROM (SELECT ID_SURAT_KELUAR, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT, PERIHAL, NO_SURAT, ROWNUM R 
				FROM MD_SURAT_KELUAR2 WHERE KD_JENIS_SURAT != '1' AND JABATAN_PEMBUAT='".$kd_jabatan."' AND (TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') LIKE '%".$q."%' OR 
					UPPER(NO_SURAT) LIKE '%".$q."%' OR UPPER(PERIHAL) LIKE '%".$q."%' OR UPPER(ISI_SURAT) LIKE '%".$q."%') ORDER BY WAKTU_SIMPAN DESC) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}	
	
	function get_sk_2_cari_by_kd_jabatan_paging($kd_jabatan, $q, $limit, $page){
		$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_SURAT_KELUAR, TGL_SURAT, PERIHAL, NO_SURAT, WAKTU_SIMPAN, 
					ROWNUM R FROM (SELECT ID_SURAT_KELUAR, TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT, PERIHAL, NO_SURAT, 
					TO_CHAR(WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_SIMPAN FROM MD_SURAT_KELUAR2 
				WHERE KD_JENIS_SURAT != '1' AND JABATAN_PEMBUAT='".$kd_jabatan."' AND (TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') LIKE '%".$q."%' OR 
					UPPER(NO_SURAT) LIKE '%".$q."%' OR UPPER(PERIHAL) LIKE '%".$q."%' OR UPPER(ISI_SURAT) LIKE '%".$q."%') 
					ORDER BY WAKTU_SIMPAN DESC) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}	
	
	function get_sk_2_cari_by_tu_bagian_jenis_surat_limit($tu_bagian_list, $jenis_surat_list, $q, $limit){
		$sql = "SELECT * FROM (SELECT A.ID_SURAT_KELUAR, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT, A.PERIHAL, A.NO_SURAT, A.KD_STATUS_SIMPAN, A.KD_JENIS_SURAT, B.NM_JENIS_SURAT, ROWNUM R 
				FROM MD_SURAT_KELUAR2 A, MD_JENIS_SURAT B WHERE A.KD_TU_BAGIAN IN(".$tu_bagian_list.") AND A.KD_JENIS_SURAT IN(".$jenis_surat_list.") 
				AND B.KD_JENIS_SURAT=A.KD_JENIS_SURAT AND (TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') LIKE '%".$q."%' OR UPPER(A.NO_SURAT) LIKE '%".$q."%' OR 
				UPPER(A.PERIHAL) LIKE '%".$q."%' OR UPPER(A.ISI_SURAT) LIKE '%".$q."%' OR UPPER(B.NM_JENIS_SURAT) LIKE '%".$q."%') ORDER BY A.WAKTU_SIMPAN DESC) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_sk_2_cari_by_tu_bagian_jenis_surat_paging($tu_bagian_list, $jenis_surat_list, $q, $limit, $page){
		$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_SURAT_KELUAR, TGL_SURAT, PERIHAL, NO_SURAT, WAKTU_SIMPAN, KD_STATUS_SIMPAN, KD_JENIS_SURAT, NM_JENIS_SURAT, 
					ROWNUM R FROM (SELECT A.ID_SURAT_KELUAR, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT, A.PERIHAL, A.NO_SURAT, A.KD_STATUS_SIMPAN, 
					TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_SIMPAN,  A.KD_JENIS_SURAT, B.NM_JENIS_SURAT 
				FROM MD_SURAT_KELUAR2 A, MD_JENIS_SURAT B 
				WHERE A.KD_TU_BAGIAN IN(".$tu_bagian_list.") AND A.KD_JENIS_SURAT IN(".$jenis_surat_list.") AND B.KD_JENIS_SURAT=A.KD_JENIS_SURAT AND 
					(TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') LIKE '%".$q."%' OR 
					UPPER(A.NO_SURAT) LIKE '%".$q."%' OR UPPER(A.PERIHAL) LIKE '%".$q."%' OR UPPER(A.ISI_SURAT) LIKE '%".$q."%' OR UPPER(B.NM_JENIS_SURAT) LIKE '%".$q."%')  
					ORDER BY A.WAKTU_SIMPAN DESC) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function get_sk_2_cari_by_tu_bagian_jenis_surat_limit_v2($unit_list, $jenis_surat_list, $q, $limit){
		$sql = "SELECT * FROM (SELECT A.ID_SURAT_KELUAR, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT, A.PERIHAL, A.NO_SURAT, A.KD_STATUS_SIMPAN, A.KD_JENIS_SURAT, B.NM_JENIS_SURAT, ROWNUM R 
				FROM MD_SURAT_KELUAR2 A, MD_JENIS_SURAT B WHERE A.UNIT_ID IN(".$unit_list.") AND A.KD_JENIS_SURAT IN(".$jenis_surat_list.") 
				AND B.KD_JENIS_SURAT=A.KD_JENIS_SURAT AND (TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') LIKE '%".$q."%' OR UPPER(A.NO_SURAT) LIKE '%".$q."%' OR 
				UPPER(A.PERIHAL) LIKE '%".$q."%' OR UPPER(A.ISI_SURAT) LIKE '%".$q."%' OR UPPER(B.NM_JENIS_SURAT) LIKE '%".$q."%') ORDER BY A.WAKTU_SIMPAN DESC) WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_sk_2_cari_by_tu_bagian_jenis_surat_paging_v2($unit_list, $jenis_surat_list, $q, $limit, $page){
		$sql = "SELECT * FROM ( SELECT * FROM ( SELECT ID_SURAT_KELUAR, TGL_SURAT, PERIHAL, NO_SURAT, WAKTU_SIMPAN, KD_STATUS_SIMPAN, 		
									KD_JENIS_SURAT, NM_JENIS_SURAT, ROWNUM R FROM (SELECT A.ID_SURAT_KELUAR, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT, A.PERIHAL, A.NO_SURAT, A.KD_STATUS_SIMPAN, TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_SIMPAN,  A.KD_JENIS_SURAT, B.NM_JENIS_SURAT 
				FROM MD_SURAT_KELUAR2 A, MD_JENIS_SURAT B 
				WHERE A.UNIT_ID IN(".$unit_list.") AND A.KD_JENIS_SURAT IN(".$jenis_surat_list.") AND B.KD_JENIS_SURAT=A.KD_JENIS_SURAT AND 
					(TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') LIKE '%".$q."%' OR 
					UPPER(A.NO_SURAT) LIKE '%".$q."%' OR UPPER(A.PERIHAL) LIKE '%".$q."%' OR UPPER(A.ISI_SURAT) LIKE '%".$q."%' OR UPPER(B.NM_JENIS_SURAT) LIKE '%".$q."%')  
					ORDER BY A.WAKTU_SIMPAN DESC) WHERE ROWNUM <= ".$limit." ) WHERE R > ".$page." )";
		return $this->tnde->query($sql)->result();
	}
	
	function detail_sk_2_by_id_surat_keluar($id_surat_keluar){
		$sql = "SELECT A.ID_SURAT_KELUAR, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') AS TGL_SURAT, A.PERIHAL, A.NO_SURAT, 
							A.TEMPAT_DIBUAT, A.TEMPAT_TUJUAN, A.KD_JENIS_ORANG, A.ID_PSD, A.ATAS_NAMA, A.ID_KLASIFIKASI_SURAT,
							A.NO_URUT, A.NO_SELA, A.ISI_SURAT, A.KD_SIFAT_SURAT, A.PEMBUAT_SURAT, A.STATUS_SK, A.KD_JENIS_SURAT, 
							A.LAMPIRAN, B.KD_PEGAWAI, B.NM_PEGAWAI, A.JABATAN_PEMBUAT, C.KD_JABATAN, C.NM_JABATAN, A.KD_TU_BAGIAN, A.KD_STATUS_SIMPAN, 
							A.KD_PRODI, A.UNTUK_BELIAU, A.KOTA_TUJUAN, D.KD_FAK_SIA,
							D.NM_TU_BAGIAN, D.ALAMAT, TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_SIMPAN, E.NM_SIFAT_SURAT, 
							F.NM_JENIS_SURAT, G.NM_JENIS_ORANG, H.KD_PSD, H.NM_PSD, H.KD_JABATAN AS KD_JABATAN_2, I.KD_KLASIFIKASI_SURAT, I.NM_KLASIFIKASI_SURAT,
							J.NM_STATUS_SIMPAN
					FROM MD_SURAT_KELUAR2 A, D_PEGAWAI B, MD_JABATAN C, MD_TU_BAGIAN D, MD_SIFAT_SURAT E, MD_JENIS_SURAT F,
								MD_JENIS_ORANG G, MD_PSD H, MD_KLASIFIKASI_SURAT I, MD_STATUS_SIMPAN J
					WHERE A.ID_SURAT_KELUAR='".$id_surat_keluar."' AND UPPER(B.KD_PEGAWAI)=A.PEMBUAT_SURAT AND 
							C.KD_JABATAN=A.JABATAN_PEMBUAT AND D.KD_TU_BAGIAN=A.KD_TU_BAGIAN AND E.KD_SIFAT_SURAT=A.KD_SIFAT_SURAT AND 
							F.KD_JENIS_SURAT=A.KD_JENIS_SURAT AND G.KD_JENIS_ORANG=A.KD_JENIS_ORANG AND H.ID_PSD=A.ID_PSD AND 
							I.ID_KLASIFIKASI_SURAT=A.ID_KLASIFIKASI_SURAT AND J.KD_STATUS_SIMPAN=A.KD_STATUS_SIMPAN";
		return $this->tnde->query($sql)->row_array();
	}
	
	function get_penerima_sk($id_surat_keluar, $kd_status_distribusi){
		$sql = "SELECT A.ID_PENERIMA_SK, A.ID_SURAT_KELUAR, A.PENERIMA_SURAT, A.JABATAN_PENERIMA, A.KD_GRUP_TUJUAN, 
										A.KD_STATUS_DISTRIBUSI, A.ID_STATUS_SM, TO_CHAR(A.WAKTU_AKSES, 'DD/MM/YYYY HH24:MI:SS') AS WAKTU_AKSES, 
										B.NM_GRUP_TUJUAN, B.KD_JENIS_ORANG, C.NM_STATUS_DISTRIBUSI, D.NM_STATUS_SM, E.NM_JENIS_ORANG, F.NM_JABATAN, G.NM_PEGAWAI, 
										A.KD_JENIS_TEMBUSAN, H.NM_JENIS_TEMBUSAN
					FROM MD_GRUP_TUJUAN B, MD_STATUS_DISTRIBUSI C, MD_STATUS_SM D, MD_JENIS_ORANG E, MD_JENIS_TEMBUSAN H, 
								D_PENERIMA_SK A LEFT JOIN MD_JABATAN F ON F.KD_JABATAN=A.JABATAN_PENERIMA LEFT JOIN D_PEGAWAI G ON G.KD_PEGAWAI=A.PENERIMA_SURAT
					WHERE A.ID_SURAT_KELUAR='".$id_surat_keluar."' AND A.KD_STATUS_DISTRIBUSI='".$kd_status_distribusi."' AND 
						B.KD_GRUP_TUJUAN=A.KD_GRUP_TUJUAN AND C.KD_STATUS_DISTRIBUSI=A.KD_STATUS_DISTRIBUSI AND 
						D.ID_STATUS_SM=A.ID_STATUS_SM AND E.KD_JENIS_ORANG=B.KD_JENIS_ORANG AND H.KD_JENIS_TEMBUSAN=A.KD_JENIS_TEMBUSAN ";
		return $this->tnde->query($sql)->result();
	}
	
	function get_pegawai_by_kd_pegawai($kd_pegawai){
		$sql = "SELECT A.*, B.*, C.*, D.*, E.* FROM D_PEGAWAI A, MD_JABATAN B, MD_TU_BAGIAN C, MD_BAGIAN D, MD_TU_PENOMORAN E
				WHERE A.KD_PEGAWAI='".$kd_pegawai."' AND B.KD_JABATAN=A.KD_JABATAN AND C.KD_TU_BAGIAN=A.KD_TU_BAGIAN AND 
				D.KD_BAGIAN=A.KD_BAGIAN AND E.KD_TU_PENOMORAN=C.KD_TU_PENOMORAN";
		return $this->tnde->query($sql)->row_array();
	}
	
	function get_pejabat_by_nm_jabatan($q){
		$sql = "SELECT A.KD_PEGAWAI, A.NM_PEGAWAI, A.KD_JABATAN, B.NM_JABATAN FROM D_PEGAWAI A, MD_JABATAN B WHERE A.KD_JABATAN != '193' AND A.KD_JABATAN != '194' AND B.KD_JABATAN=A.KD_JABATAN AND UPPER(B.NM_JABATAN) LIKE '%".$q."%'";
		return $this->tnde->query($sql)->result();
	}
	
	function get_pegawai_by_like($q=''){
		$q = strtoupper($q);
		$sql = "SELECT A.KD_PEGAWAI, A.NM_PEGAWAI, A.KD_JABATAN, B.NM_JABATAN FROM D_PEGAWAI A, MD_JABATAN B WHERE (UPPER(A.KD_PEGAWAI) LIKE '%".$q."%' OR UPPER(A.NM_PEGAWAI) LIKE '%".$q."%') AND B.KD_JABATAN=A.KD_JABATAN";
		return $this->tnde->query($sql)->result();
	}
	
	function get_klasifikasi_surat(){
		$sql = "SELECT * FROM MD_KLASIFIKASI_SURAT WHERE ID_KLASIFIKASI_SURAT != '21'";
		return $this->tnde->query($sql)->result();
	}
	
	function update_surat_keluar($id_surat_keluar, $data){
		$sql = "UPDATE MD_SURAT_KELUAR2 SET TGL_SURAT=TO_DATE('".$data['TGL_SURAT']."', 'DD/MM/YYYY'), PERIHAL='".$data['PERIHAL']."', 
					TEMPAT_DIBUAT='".$data[	'TEMPAT_DIBUAT']."', TEMPAT_TUJUAN='".$data['TEMPAT_TUJUAN']."', KD_SIFAT_SURAT='".$data['KD_SIFAT_SURAT']."', KD_JENIS_SURAT='".$data['KD_JENIS_SURAT']."', STATUS_SK='".$data['STATUS_SK']."', KD_TU_BAGIAN='".$data['KD_TU_BAGIAN']."', ID_PSD='".$data['ID_PSD']."', 
					 ATAS_NAMA='".$data['ATAS_NAMA']."', ID_KLASIFIKASI_SURAT='".$data['ID_KLASIFIKASI_SURAT']."', NO_URUT='".$data['NO_URUT']."', NO_SELA='".$data['NO_SELA']."', NO_SURAT='".$data['NO_SURAT']."', ISI_SURAT='".$data['ISI_SURAT']."', LAMPIRAN='".$data['LAMPIRAN']."', KD_STATUS_SIMPAN='".$data['KD_STATUS_SIMPAN']."', KD_PRODI='".$data['KD_PRODI']."' 
					 WHERE ID_SURAT_KELUAR='".$id_surat_keluar."' ";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	function insert_detail_stugas($data){
		$sql = "INSERT INTO D_DETAIL_STUGAS(ID_SURAT_KELUAR, KEPERLUAN, TGL_MULAI, TGL_SELESAI, TEMPAT)
					VALUES('".$data['ID_SURAT_KELUAR']."', '".$data['KEPERLUAN']."', TO_DATE('".$data['TGL_MULAI']."', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('".$data['TGL_SELESAI']."', 'DD/MM/YYYY HH24:MI:SS'), '".$data['TEMPAT']."')";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function detail_stugas_by_id_surat_keluar($id_surat_keluar){
		$sql = "SELECT ID_DETAIL_STUGAS, ID_SURAT_KELUAR, KEPERLUAN, TO_CHAR(TGL_MULAI, 'DD/MM/YYYY HH24:MI:SS') TGL_MULAI, TO_CHAR(TGL_SELESAI, 'DD/MM/YYYY HH24:MI:SS') TGL_SELESAI, TEMPAT FROM D_DETAIL_STUGAS WHERE ID_SURAT_KELUAR='".$id_surat_keluar."' ";
		return $this->tnde->query($sql)->row_array();
	}
	
	function update_detail_stugas_by_id_surat_keluar($id_surat_keluar, $data){
		$sql = "UPDATE D_DETAIL_STUGAS SET KEPERLUAN='".$data['KEPERLUAN']."', TGL_MULAI=TO_DATE('".$data['TGL_MULAI']."', 'DD/MM/YYYY HH24:MI:SS'), TGL_SELESAI=TO_DATE('".$data['TGL_SELESAI']."', 'DD/MM/YYYY HH24:MI:SS'), TEMPAT='".$data['TEMPAT']."' WHERE ID_SURAT_KELUAR='".$id_surat_keluar."' ";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
		}else{
			return FALSE;
		}
			return TRUE;
	}
	
	function insert_detail_undangan($data){
		$sql = "INSERT INTO D_DETAIL_SUNDANGAN(ID_SURAT_KELUAR, TGL_MULAI, TGL_SELESAI, TEMPAT, ACARA)VALUES('".$data['ID_SURAT_KELUAR']."', TO_DATE('".$data['TGL_MULAI']."', 'DD/MM/YYYY HH24:MI:SS'), TO_DATE('".$data['TGL_SELESAI']."', 'DD/MM/YYYY HH24:MI:SS'), '".$data['TEMPAT']."', '".$data['ACARA']."')";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return TRUE;
		}
	}
	
	function detail_sundangan_by_id_surat_keluar($id_surat_keluar){
		$sql = "SELECT ID_DETAIL_SUNDANGAN, ID_SURAT_KELUAR, TO_CHAR(TGL_MULAI, 'DD/MM/YYYY HH24:MI:SS') TGL_MULAI, TO_CHAR(TGL_SELESAI, 'DD/MM/YYYY HH24:MI:SS') TGL_SELESAI, 
					TEMPAT, ACARA, KD_PEMINJAMAN FROM D_DETAIL_SUNDANGAN WHERE ID_SURAT_KELUAR='".$id_surat_keluar."' ";
		return $this->tnde->query($sql)->row_array();
	}
	
	function update_detail_sundangan_by_id_surat_keluar($id_surat_keluar, $data){
		$sql = "UPDATE D_DETAIL_SUNDANGAN SET TGL_MULAI=TO_DATE('".$data['TGL_MULAI']."', 'DD/MM/YYYY HH24:MI:SS'), TGL_SELESAI=TO_DATE('".$data['TGL_SELESAI']."', 'DD/MM/YYYY HH24:MI:SS'), TEMPAT='".$data['TEMPAT']."', ACARA='".$data['ACARA']."', KD_PEMINJAMAN='".$data['KD_PEMINJAMAN']."' WHERE ID_SURAT_KELUAR='".$id_surat_keluar."' ";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
}
?>