<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');
class Tnde_surat_terkait extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->model('tnde_surat_terkait/mdl_tnde_surat_terkait', 'mdl_70007');
	}
	
	function total_surat_terkait($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 70007: switch($subkode){
				default:
					case 1: $query = $this->mdl_70007->total_surat_terkait($api_search[0], $api_search[1]); break; #kd_pegawai, kd_jabatan
					case 2: $query = $this->mdl_70007->total_surat_terkait_cari($api_search[0], $api_search[1], $api_search[2]); break; #kd_pegawai, kd_jabatan, keyword
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}	
	
	function total_st_by_kd_pegawai($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 70007: switch($subkode){
				default:
					case 1: $query = $this->mdl_70007->total_st_by_kd_pegawai($api_search[0]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function total_st_by_kd_jabatan($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 70007: switch($subkode){
				default:
					case 1: $query = $this->mdl_70007->total_st_by_kd_jabatan($api_search[0]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function get_surat_terkait($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 70007: switch($subkode){
				default:
					case 1: $query = $this->mdl_70007->get_surat_terkait_limit($api_search[0], $api_search[1], $api_search[2]); break;
					case 2: $limit = $api_search[2] + $api_search[3]; $query = $this->mdl_70007->get_surat_terkait_paging($api_search[0], $api_search[1], $limit, $api_search[3]); break;
					case 3: $query = $this->mdl_70007->get_surat_terkait_cari_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3]); break;
					case 4: $limit = $api_search[3] + $api_search[4]; $query = $this->mdl_70007->get_surat_terkait_cari_paging($api_search[0], $api_search[1], $api_search[2], $limit, $api_search[4]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}	
	
	function get_sk_terkait_by_kd_pegawai($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 70007: switch($subkode){
				default:
					case 1: $query = $this->mdl_70007->get_sk_terkait_by_kd_pegawai_limit($api_search[0], $api_search[1]); break;
					case 2: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_70007->get_sk_terkait_by_kd_pegawai_paging($api_search[0], $limit, $api_search[2]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function get_sk_terkait_by_kd_jabatan($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 70007: switch($subkode){
				default:
					case 1: $query = $this->mdl_70007->get_sk_terkait_by_kd_jabatan_limit($api_search[0], $api_search[1]); break;
					case 2: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_70007->get_sk_terkait_by_kd_jabatan_paging($api_search[0], $limit, $api_search[2]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}	
	
	function detail_sk_2_by_id_penerima_sk($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 70007: switch($subkode){
				default:
					case 1: $query = $this->mdl_70007->detail_sk_2_by_id_penerima_sk($api_search[0]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function penerima_surat_keluar($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 70007: switch($subkode){
				default:
					case 1: $query = $this->mdl_70007->penerima_surat_keluar($api_search[0], $api_search[1], $api_search[2]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function update_status_sm($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 70007: switch($subkode){
				default:
					case 1: $query = $this->mdl_70007->update_status_sm($api_search[0], $api_search[1], $api_search[2]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
}
?>