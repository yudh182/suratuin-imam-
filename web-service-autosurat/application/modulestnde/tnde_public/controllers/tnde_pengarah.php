<?php
if(! defined('BASEPATH'))exit('No direct script access allowed');
Class Tnde_pengarah extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('tnde_general/mdl_tnde_general', 'mdl_1001');
		$this->load->model('tnde_pengarah/mdl_tnde_pengarah', 'mdl_3003');
	}
	
	function tot_disposisi_by_jab_pemroses($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->tot_disposisi_by_jab_pemroses($api_search[0], $api_search[1]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function total_sm_by_tu_bagian($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->total_sm_by_tu_bagian($api_search[0]);break;
					case 2: $query = $this->mdl_3003->total_sm_by_tu_bagian_tgl_awal($api_search[0], $api_search[1], $api_search[2]);break;
					case 3: $query = $this->mdl_3003->total_sm_by_tu_bagian_tgl_akhir($api_search[0], $api_search[1], $api_search[2]);break;
					case 4: $query = $this->mdl_3003->total_sm_by_tu_bagian_periode($api_search[0], $api_search[1], $api_search[2], $api_search[3]);break;
					case 5: $query = $this->mdl_3003->total_sm_by_tu_bagian_perihal($api_search[0], $api_search[1]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function total_sm_by_kd_jabatan($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->total_sm_by_kd_jabatan($api_search[0]);break;
					case 2: $query = $this->mdl_3003->total_sm_by_kd_jabatan_tgl_awal($api_search[0],$api_search[1],$api_search[2]);break;
					case 3: $query = $this->mdl_3003->total_sm_by_kd_jabatan_tgl_akhir($api_search[0],$api_search[1],$api_search[2]);break;
					case 4: $query = $this->mdl_3003->total_sm_by_kd_jabatan_periode($api_search[0],$api_search[1],$api_search[2], $api_search[3]);break;
					case 5: $query = $this->mdl_3003->total_sm_by_kd_jabatan_perihal($api_search[0],$api_search[1]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);	
	}
	
	function total_dm_by_kd_jabatan($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->total_dm_by_kd_jabatan($api_search[0]);break;
					case 2: $query = $this->mdl_3003->total_dm_by_kd_jabatan_tgl_awal($api_search[0], $api_search[1], $api_search[2]);break;
					case 3: $query = $this->mdl_3003->total_dm_by_kd_jabatan_tgl_akhir($api_search[0], $api_search[1], $api_search[2]);break;
					case 4: $query = $this->mdl_3003->total_dm_by_kd_jabatan_periode($api_search[0], $api_search[1], $api_search[2], $api_search[3]);break;
					case 5: $query = $this->mdl_3003->total_dm_by_kd_jabatan_perihal($api_search[0], $api_search[1]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);	
	}
	
	function total_dm_by_nip($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->total_dm_by_nip($api_search[0]);break;
					case 2: $query = $this->mdl_3003->total_dm_by_nip_tgl_awal($api_search[0], $api_search[1], $api_search[2]);break;
					case 3: $query = $this->mdl_3003->total_dm_by_nip_tgl_akhir($api_search[0], $api_search[1], $api_search[2]);break;
					case 4: $query = $this->mdl_3003->total_dm_by_nip_periode($api_search[0], $api_search[1], $api_search[2], $api_search[3]);break;
					case 5: $query = $this->mdl_3003->total_dm_by_nip_perihal($api_search[0], $api_search[1]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);	
	}
	
	function total_dm_by_kd_pegawai($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->total_dm_by_kd_pegawai($api_search[0]);break;
					case 2: $query = $this->mdl_3003->total_dm_by_kd_pegawai_tgl_awal($api_search[0], $api_search[1], $api_search[2]);break;
					case 3: $query = $this->mdl_3003->total_dm_by_kd_pegawai_tgl_akhir($api_search[0], $api_search[1], $api_search[2]);break;
					case 4: $query = $this->mdl_3003->total_dm_by_kd_pegawai_periode($api_search[0], $api_search[1], $api_search[2], $api_search[3]);break;
					case 5: $query = $this->mdl_3003->total_dm_by_kd_pegawai_perihal($api_search[0], $api_search[1]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);	
	}
	
	function total_dk_by_kd_jabatan($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->total_dk_by_kd_jabatan($api_search[0]);break;
					case 2: $query = $this->mdl_3003->total_dk_by_kd_jabatan_tgl_awal($api_search[0], $api_search[1], $api_search[2]);break;
					case 3: $query = $this->mdl_3003->total_dk_by_kd_jabatan_tgl_akhir($api_search[0], $api_search[1], $api_search[2]);break;
					case 4: $query = $this->mdl_3003->total_dk_by_kd_jabatan_periode($api_search[0], $api_search[1], $api_search[2], $api_search[3]);break;
					case 5: $query = $this->mdl_3003->total_dk_by_kd_jabatan_perihal($api_search[0], $api_search[1]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function total_dk_grup_by_kd_jabatan($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->total_dk_grup_by_kd_jabatan($api_search[0]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function total_ts_by_kd_jabatan($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->total_ts_by_kd_jabatan($api_search[0]);break;
					case 2: $query = $this->mdl_3003->total_ts_by_kd_jabatan_tgl_awal($api_search[0], $api_search[1], $api_search[2]);break;
					case 3: $query = $this->mdl_3003->total_ts_by_kd_jabatan_tgl_akhir($api_search[0], $api_search[1], $api_search[2]);break;
					case 4: $query = $this->mdl_3003->total_ts_by_kd_jabatan_periode($api_search[0], $api_search[1], $api_search[2], $api_search[3]);break;
					case 5: $query = $this->mdl_3003->total_ts_by_kd_jabatan_perihal($api_search[0], $api_search[1]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function get_sm_by_tu_bagian($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->sm_by_tu_bagian_limit($api_search[0],$api_search[1]); break;
					case 2: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->sm_by_tu_bagian_paging($api_search[0], $limit, $api_search[2]); break;
					case 3: $query = $this->mdl_3003->sm_by_tu_bagian_tgl_awal_limit($api_search[0],$api_search[1], $api_search[2], $api_search[3]); break;
					case 4: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->sm_by_tu_bagian_tgl_awal_paging($api_search[0], $limit, $api_search[2], $api_search[3], $api_search[4]); break;
					case 5: $query = $this->mdl_3003->sm_by_tu_bagian_tgl_akhir_limit($api_search[0],$api_search[1], $api_search[2], $api_search[3]); break;
					case 6: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->sm_by_tu_bagian_tgl_akhir_paging($api_search[0], $limit, $api_search[2], $api_search[3], $api_search[4]); break;
					case 7: $query = $this->mdl_3003->sm_by_tu_bagian_periode_limit($api_search[0],$api_search[1], $api_search[2], $api_search[3], $api_search[4]); break;
					case 8: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->sm_by_tu_bagian_periode_paging($api_search[0], $limit, $api_search[2], $api_search[3], $api_search[4], $api_search[5]); break;
					case 9: $query = $this->mdl_3003->sm_by_tu_bagian_perihal_limit($api_search[0],$api_search[1], $api_search[2]); break;
					case 10: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->sm_by_tu_bagian_perihal_paging($api_search[0], $limit, $api_search[2], $api_search[3]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query,$format);	
	}
	
	function detail_sm_by_id_surat_masuk($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->detail_sm_by_id_surat_masuk($api_search[0]);break;
					case 2: $query = $this->mdl_3003->detail_sm_by_id_surat_masuk2($api_search[0]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query,$format);
	}
	
	function get_pejabat($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->get_pejabat();break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query,$format);
	}
	
	function get_pejabat_by_tu_bagian($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->get_pejabat_by_tu_bagian($api_search[0]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query,$format);	
	}
	
	function update_sm_by_id_surat_masuk($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->update_sm_by_id_surat_masuk($api_search[0], $api_search[1]);break;
					case 2: $query = $this->mdl_3003->update_sm_by_id_surat_masuk2($api_search[0], $api_search[1]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query,$format);	
	}
	
	function insert_distribusi_surat($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->insert_distribusi_surat($api_search[0]);break;
					case 2: $query = $this->mdl_3003->insert_distribusi_surat2($api_search[0]);break;
					case 3: $query = $this->mdl_3003->insert_distribusi_surat3($api_search[0]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query,$format);		
	}
	
	function update_status_surat($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->update_status_surat($api_search[0], $api_search[1]);break;
					case 2: $query = $this->mdl_3003->update_status_distribusi_surat($api_search[0], $api_search[1]);break;
					case 3: $query = $this->mdl_3003->update_status_surat2($api_search[0], $api_search[1]);break;
					case 4: $query = $this->mdl_3003->update_status_surat_masuk($api_search[0], $api_search[1]);break;
					case 5: $query = $this->mdl_3003->update_status_distribusi_surat2($api_search[0], $api_search[1]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query,$format);	
	}
	
	function get_distribusi_surat_by_id_surat_masuk($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->get_ds_by_id_surat_masuk($api_search[0],$api_search[1]);break;
					case 2: $query = $this->mdl_3003->get_ds_by_id_surat_masuk2($api_search[0],$api_search[1], $api_search[2]);break;
					case 3: $query = $this->mdl_3003->get_ds_by_id_surat_masuk3($api_search[0],$api_search[1], $api_search[2]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query,$format);		
	}
	
	function get_sm_by_kd_jabatan($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->get_sm_by_kd_jabatan_limit($api_search[0], $api_search[1]);break;
					case 2: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->get_sm_by_kd_jabatan_paging($api_search[0], $limit, $api_search[2]);break;
					case 3: $query = $this->mdl_3003->get_sm_by_kd_jabatan_tgl_awal_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3]);break;
					case 4: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->get_sm_by_kd_jabatan_tgl_awal_paging($api_search[0], $limit, $api_search[2], $api_search[3], $api_search[4]);break;
					case 5: $query = $this->mdl_3003->get_sm_by_kd_jabatan_tgl_akhir_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3]);break;
					case 6: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->get_sm_by_kd_jabatan_tgl_akhir_paging($api_search[0], $limit, $api_search[2], $api_search[3], $api_search[4]);break;
					case 7: $query = $this->mdl_3003->get_sm_by_kd_jabatan_periode_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3], $api_search[4]);break;
					case 8: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->get_sm_by_kd_jabatan_periode_paging($api_search[0], $limit, $api_search[2], $api_search[3], $api_search[4], $api_search[5]);break;
					case 9: $query = $this->mdl_3003->get_sm_by_kd_jabatan_perihal_limit($api_search[0], $api_search[1], $api_search[2]);break;
					case 10: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->get_sm_by_kd_jabatan_perihal_paging($api_search[0], $limit, $api_search[2], $api_search[3]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query,$format);			
	}
	
	function get_dm_by_nip($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->get_dm_by_nip_limit($api_search[0],$api_search[1]); break;
					case 2: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->get_dm_by_nip_paging($api_search[0], $limit, $api_search[2]); break;
					case 3: $query = $this->mdl_3003->get_dm_by_nip_tgl_awal_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3]); break;
					case 4: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->get_dm_by_nip_tgl_awal_paging($api_search[0], $limit, $api_search[2], $api_search[3], $api_search[4]); break;
					case 5: $query = $this->mdl_3003->get_dm_by_nip_tgl_akhir_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3]); break;
					case 6: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->get_dm_by_nip_tgl_akhir_paging($api_search[0], $limit, $api_search[2], $api_search[3], $api_search[4]); break;
					case 7: $query = $this->mdl_3003->get_dm_by_nip_periode_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3], $api_search[4]); break;
					case 8: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->get_dm_by_nip_periode_paging($api_search[0], $limit, $api_search[2], $api_search[3], $api_search[4], $api_search[5]); break;
					case 9: $query = $this->mdl_3003->get_dm_by_nip_perihal_limit($api_search[0], $api_search[1], $api_search[2]); break;
					case 10: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->get_dm_by_nip_perihal_paging($api_search[0], $limit, $api_search[2], $api_search[3]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}

	function get_dm_by_kd_pegawai($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->get_dm_by_kd_pegawai_limit($api_search[0],$api_search[1]); break;
					case 2: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->get_dm_by_kd_pegawai_paging($api_search[0], $limit, $api_search[2]); break;
					case 3: $query = $this->mdl_3003->get_dm_by_kd_pegawai_tgl_awal_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3]); break;
					case 4: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->get_dm_by_kd_pegawai_tgl_awal_paging($api_search[0], $limit, $api_search[2], $api_search[3], $api_search[4]); break;
					case 5: $query = $this->mdl_3003->get_dm_by_kd_pegawai_tgl_akhir_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3]); break;
					case 6: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->get_dm_by_kd_pegawai_tgl_akhir_paging($api_search[0], $limit, $api_search[2], $api_search[3], $api_search[4]); break;
					case 7: $query = $this->mdl_3003->get_dm_by_kd_pegawai_periode_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3], $api_search[4]); break;
					case 8: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->get_dm_by_kd_pegawai_periode_paging($api_search[0], $limit, $api_search[2], $api_search[3], $api_search[4], $api_search[5]); break;
					case 9: $query = $this->mdl_3003->get_dm_by_kd_pegawai_perihal_limit($api_search[0], $api_search[1], $api_search[2]); break;
					case 10: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->get_dm_by_kd_pegawai_perihal_paging($api_search[0], $limit, $api_search[2], $api_search[3]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function detail_sm_by_id_distribusi_surat($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->detail_sm_by_id_distribusi_surat($api_search[0]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query,$format);
	}
	
	function get_dm_by_kd_jabatan($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->get_dm_by_kd_jabatan_limit($api_search[0], $api_search[1]);break;
					case 2: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->get_dm_by_kd_jabatan_paging($api_search[0], $limit, $api_search[2]);break;
					case 3: $query = $this->mdl_3003->get_dm_by_kd_jabatan_tgl_awal_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3]);break;
					case 4: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->get_dm_by_kd_jabatan_tgl_awal_paging($api_search[0], $limit, $api_search[2], $api_search[3], $api_search[4]);break;
					case 5: $query = $this->mdl_3003->get_dm_by_kd_jabatan_tgl_akhir_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3]);break;
					case 6: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->get_dm_by_kd_jabatan_tgl_akhir_paging($api_search[0], $limit, $api_search[2], $api_search[3], $api_search[4]);break;
					case 7: $query = $this->mdl_3003->get_dm_by_kd_jabatan_periode_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3], $api_search[4]);break;
					case 8: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->get_dm_by_kd_jabatan_periode_paging($api_search[0], $limit, $api_search[2], $api_search[3], $api_search[4], $api_search[5]);break;
					case 9: $query = $this->mdl_3003->get_dm_by_kd_jabatan_perihal_limit($api_search[0], $api_search[1], $api_search[2]);break;
					case 10: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->get_dm_by_kd_jabatan_perihal_paging($api_search[0], $limit, $api_search[2], $api_search[3]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query,$format);
	}
	
	function get_dk_by_kd_jabatan($format = ''){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->get_dk_by_kd_jabatan_limit($api_search[0], $api_search[1]);break;
					case 2: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->get_dk_by_kd_jabatan_paging($api_search[0], $limit, $api_search[2]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query,$format);
	}
	
	function get_dk_grup_by_kd_jabatan($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->get_dk_grup_by_kd_jabatan_limit($api_search[0], $api_search[1]);break;
					case 2: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->get_dk_grup_by_kd_jabatan_paging($api_search[0], $limit, $api_search[2]);break;
					case 3: $query = $this->mdl_3003->get_dk_grup_by_kd_jabatan_tgl_awal_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3]);break;
					case 4: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->get_dk_grup_by_kd_jabatan_tgl_awal_paging($api_search[0], $limit, $api_search[2], $api_search[3], $api_search[4]);break;
					case 5: $query = $this->mdl_3003->get_dk_grup_by_kd_jabatan_tgl_akhir_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3]);break;
					case 6: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->get_dk_grup_by_kd_jabatan_tgl_akhir_paging($api_search[0], $limit, $api_search[2], $api_search[3], $api_search[4]);break;
					case 7: $query = $this->mdl_3003->get_dk_grup_by_kd_jabatan_periode_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3], $api_search[4]);break;
					case 8: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->get_dk_grup_by_kd_jabatan_periode_paging($api_search[0], $limit, $api_search[2], $api_search[3], $api_search[4], $api_search[5]);break;
					case 9: $query = $this->mdl_3003->get_dk_grup_by_kd_jabatan_perihal_limit($api_search[0], $api_search[1], $api_search[2]);break;
					case 10: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->get_dk_grup_by_kd_jabatan_perihal_paging($api_search[0], $limit, $api_search[2], $api_search[3]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query,$format);
	}
	
	function get_ts_by_kd_jabatan($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->get_ts_by_kd_jabatan_limit($api_search[0], $api_search[1]);break;
					case 2: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->get_ts_by_kd_jabatan_paging($api_search[0], $limit, $api_search[2]);break;
					case 3: $query = $this->mdl_3003->get_ts_by_kd_jabatan_tgl_awal_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3]);break;
					case 4: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->get_ts_by_kd_jabatan_tgl_awal_paging($api_search[0], $limit, $api_search[2], $api_search[3], $api_search[4]);break;
					case 5: $query = $this->mdl_3003->get_ts_by_kd_jabatan_tgl_akhir_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3]);break;
					case 6: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->get_ts_by_kd_jabatan_tgl_akhir_paging($api_search[0], $limit, $api_search[2], $api_search[3], $api_search[4]);break;
					case 7: $query = $this->mdl_3003->get_ts_by_kd_jabatan_periode_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3], $api_search[4]);break;
					case 8: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->get_ts_by_kd_jabatan_periode_paging($api_search[0], $limit, $api_search[2], $api_search[3], $api_search[4], $api_search[5]);break;
					case 9: $query = $this->mdl_3003->get_ts_by_kd_jabatan_perihal_limit($api_search[0], $api_search[1], $api_search[2]);break;
					case 10: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->get_ts_by_kd_jabatan_perihal_paging($api_search[0], $limit, $api_search[2], $api_search[3]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query,$format);
	}
	
	function get_pejabat_by_kd_jabatan($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->get_pejabat_by_kd_jabatan($api_search[0]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query,$format);	
	}
	
	function detail_dk_by_id_distribusi_surat($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->detail_dk_by_id_distribusi_surat($api_search[0]);break;
					case 2: $query = $this->mdl_3003->detail_dk_by_id_distribusi_surat2($api_search[0], $api_search[1], $api_search[2]);break;
					case 3: $query = $this->mdl_3003->detail_dk_by_id_distribusi_surat3($api_search[0], $api_search[1]);break;
					case 4: $query = $this->mdl_3003->detail_dk_by_id_distribusi_surat4($api_search[0], $api_search[1]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query,$format);		
	}
	
	function detail_dk_by_id_surat_masuk($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->detail_dk_by_id_surat_masuk($api_search[0], $api_search[1], $api_search[2]);break;
					case 2: $query = $this->mdl_3003->detail_dk_by_id_surat_masuk2($api_search[0], $api_search[1], $api_search[2]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query,$format);		
	}
	
	function detail_ts_by_id_distribusi_surat($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->detail_ts_by_id_distribusi_surat($api_search[0]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query,$format);	
	}
	
	function update_ds_by_id_surat_masuk_jab_penerima($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->update_ds_by_id_surat_masuk_jab_penerima($api_search[0], $api_search[1], $api_search[2]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query,$format);	
	}
	
	function total_surat_keluar($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->total_surat_keluar(); break;
					case 2: $query = $this->mdl_3003->total_sk_by_kd_peg_or_jab($api_search[0], $api_search[1]); break;
					case 3: $query = $this->mdl_3003->total_sk_by_kd_peg($api_search[0]); break;
					case 4: $query = $this->mdl_3003->total_sk_by_kd_jab($api_search[0]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function get_sk_by_kd_peg($format = 'json'){
		$kode 			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode 		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search 	= $this->input->post('api_search');		
		switch($kode){
			case 3003: switch($subkode){
				default: 
					case 1: $query = $this->mdl_3003->get_sk_by_kd_peg_limit($api_search[0], $api_search[1]);break; 
					case 2: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->get_sk_by_kd_peg_paging($api_search[0], $limit, $api_search[2]);break; 
			}
			break;		
		}
		$this->sia_api_lib_format->output($query, $format);		
	}
	
	function get_sk_by_kd_jabatan($format = 'json'){
		$kode 			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode 		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search 	= $this->input->post('api_search');		
		switch($kode){
			case 3003: switch($subkode){
				default: 
					case 1: $query = $this->mdl_3003->get_sk_by_kd_jabatan_limit($api_search[0], $api_search[1]);break; 
					case 2: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->get_sk_by_kd_jabatan_paging($api_search[0], $limit, $api_search[2]);break; 
			}
			break;		
		}
		$this->sia_api_lib_format->output($query, $format);	
	}
	
	function insert_surat_keluar($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->insert_surat_keluar($api_search[0]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);	
	}
	
	function insert_dokumen_sk($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->insert_dokumen_sk($api_search[0]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);	
	}
	
	function insert_lampiran_sk($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->insert_lampiran_sk($api_search[0]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);	
	}
	
	function insert_scan_sk($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->insert_scan_sk($api_search[0]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);	
	}
	
	function total_surat_keluar_by_perihal($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->total_sk_by_perihal($api_search[0]); break;
					case 2: $query = $this->mdl_3003->total_sk_by_perihal_kd_peg_or_jab($api_search[0], $api_search[1], $api_search[2]); break;
					case 3: $query = $this->mdl_3003->total_sk_by_perihal_kd_peg($api_search[0], $api_search[1]); break;
					case 4: $query = $this->mdl_3003->total_sk_by_perihal_kd_jabatan($api_search[0], $api_search[1]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function get_sk_by_kd_peg_perihal($format = 'json'){
		$kode 			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode 		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search 	= $this->input->post('api_search');		
		switch($kode){
			case 3003: switch($subkode){
				default: 
					case 1: $query = $this->mdl_3003->get_sk_by_kd_peg_perihal_limit($api_search[0], $api_search[1], $api_search[2]);break; 
					case 2: $limit = $api_search[2] + $api_search[3]; $query = $this->mdl_3003->get_sk_by_kd_peg_perihal_paging($api_search[0], $api_search[1], $limit, $api_search[3]);break; 
			}
			break;		
		}
		$this->sia_api_lib_format->output($query, $format);		
	}
	
	function get_sk_by_kd_jabatan_perihal($format = 'json'){
		$kode 			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode 		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search 	= $this->input->post('api_search');		
		switch($kode){
			case 3003: switch($subkode){
				default: 
					case 1: $query = $this->mdl_3003->get_sk_by_kd_jabatan_perihal_limit($api_search[0], $api_search[1], $api_search[2]);break; 
					case 2: $limit = $api_search[2] + $api_search[3]; $query = $this->mdl_3003->get_sk_by_kd_jabatan_perihal_paging($api_search[0], $api_search[1], $limit, $api_search[3]);break; 
			}
			break;		
		}
		$this->sia_api_lib_format->output($query, $format);		
	}
	
	function update_sk_by_id_surat_keluar($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->update_sk_by_id_surat_keluar($api_search[0]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);	
	}
	
	function detail_sk_by_id_surat_keluar($format = 'json'){
		$kode 			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode 		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search 	= $this->input->post('api_search');		
		switch($kode){
			case 3003: switch($subkode){
				default: 
					case 1: $query = $this->mdl_3003->detail_sk_by_id_surat_keluar($api_search[0]);break;
			}
			break;		
		}
		$this->sia_api_lib_format->output($query, $format);	
	}
	
	function update_lampiran_sk($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->update_lampiran_sk($api_search[0], $api_search[1]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);	
	}
	
	function update_scan_sk($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->update_scan_sk($api_search[0], $api_search[1]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);	
	}
	
	function get_dokumen_sk($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->get_dokumen_sk($api_search[0]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);	
	}
	
	function get_lampiran_sk($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->get_lampiran_sk($api_search[0]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);	
	}
	
	function get_scan_sk($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->get_scan_sk($api_search[0]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);	
	}
	
	function total_pencarian_sk($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->total_pencarian_sk_all3($api_search[0]); break;
					case 2: $query = $this->mdl_3003->total_pencarian_sk_all4($api_search[0]); break;
					case 3: $query = $this->mdl_3003->total_pencarian_sk_all($api_search[0]); break;
					case 4: $query = $this->mdl_3003->total_pencarian_sk_all2($api_search[0]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function pencarian_sk_all_tgl_surat($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->pencarian_sk_all_tgl_surat_limit($api_search[0], $api_search[1]); break;
					case 2: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->pencarian_sk_all_tgl_surat_paging($api_search[0], $limit, $api_search[2]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);		
	}
	
	function pencarian_sk_all($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->pencarian_sk_all_limit($api_search[0], $api_search[1]); break;
					case 2: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->pencarian_sk_all_paging($api_search[0], $limit, $api_search[2]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);		
	}
	
	function pencarian_sk_tgl_surat($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->pencarian_sk_tgl_surat_limit($api_search[0], $api_search[1]); break;
					case 2: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->pencarian_sk_tgl_surat_paging($api_search[0], $limit, $api_search[2]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);		
	}
	
	function pencarian_sk($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->pencarian_sk_limit($api_search[0], $api_search[1]); break;
					case 2: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->pencarian_sk_paging($api_search[0], $limit, $api_search[2]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);		
	}
	
	function total_pencarian_sk2($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->total_pencarian_sk_all3_2($api_search[0]); break;
					case 2: $query = $this->mdl_3003->total_pencarian_sk_all4_2($api_search[0]); break;
					case 3: $query = $this->mdl_3003->total_pencarian_sk_all_2($api_search[0]); break;
					case 4: $query = $this->mdl_3003->total_pencarian_sk_all2_2($api_search[0]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function pencarian_sk_all_tgl_surat2($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->pencarian_sk_all_tgl_surat_limit2($api_search[0], $api_search[1]); break;
					case 2: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->pencarian_sk_all_tgl_surat_paging2($api_search[0], $limit, $api_search[2]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);		
	}
	
	function pencarian_sk_all2($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->pencarian_sk_all_limit2($api_search[0], $api_search[1]); break;
					case 2: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->pencarian_sk_all_paging2($api_search[0], $limit, $api_search[2]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);		
	}
	
	function pencarian_sk_tgl_surat2($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->pencarian_sk_tgl_surat_limit2($api_search[0], $api_search[1]); break;
					case 2: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->pencarian_sk_tgl_surat_paging2($api_search[0], $limit, $api_search[2]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);		
	}
	
	function pencarian_sk2($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->pencarian_sk_limit2($api_search[0], $api_search[1]); break;
					case 2: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->pencarian_sk_paging2($api_search[0], $limit, $api_search[2]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);		
	}
	
	function total_surat_keluar_tu_bagian($format = 'json'){
		$kode 			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode 		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search 	= $this->input->post('api_search');		
		switch($kode){
			case 3003: switch($subkode){
				default: 
					case 1: $query = $this->mdl_3003->total_surat_keluar_tu_bagian($api_search[0]);break;
			}
			break;		
		}
		$this->sia_api_lib_format->output($query, $format);	
	}
	
	function get_surat_keluar($format = 'json'){
		$kode	 			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode 		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search 	= $this->input->post('api_search');		
		switch($kode){
			case 3003: switch($subkode){
				default: 
					case 1: $query = $this->mdl_3003->get_surat_keluar_limit($api_search[0]);break; 
					case 2: $limit = $api_search[0] + $api_search[1]; $query = $this->mdl_3003->get_surat_keluar_paging($limit, $api_search[1]);break; 
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);	
	}

	function get_surat_keluar_by_tu_bagian($format = 'json'){
		$kode	 			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode 		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search 	= $this->input->post('api_search');		
		switch($kode){
			case 3003: switch($subkode){
				default: 
					case 1: $query = $this->mdl_3003->get_surat_keluar_by_tu_bagian_limit($api_search[0], $api_search[1]);break; 
					case 2: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->get_surat_keluar_by_tu_bagian_paging($api_search[0], $limit, $api_search[2]);break; 
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function total_sk_by_tu_bagian_perihal($format = 'json'){
		$kode	 			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode 		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search 	= $this->input->post('api_search');		
		switch($kode){
			case 3003: switch($subkode){
				default: 
					case 1: $query = $this->mdl_3003->total_sk_by_tu_bagian_perihal($api_search[0], $api_search[1]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function get_surat_keluar_by_perihal($format = 'json'){
		$kode 			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode 		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search 	= $this->input->post('api_search');		
		switch($kode){
			case 3003: switch($subkode){
				default: 
					case 1: $query = $this->mdl_3003->get_surat_keluar_by_perihal_limit($api_search[0], $api_search[1]);break; 
					case 2: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->get_surat_keluar_by_perihal_paging($api_search[0], $limit, $api_search[2]);break; 
			}
			break;		
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function get_sk_by_tu_bagian_perihal($format = 'json'){
		$kode	 			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode 		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search 	= $this->input->post('api_search');		
		switch($kode){
			case 3003: switch($subkode){
				default: 
					case 1: $query = $this->mdl_3003->get_sk_by_tu_bagian_perihal_limit($api_search[0], $api_search[1], $api_search[2]);break; 
					case 2: $limit = $api_search[2] + $api_search[3]; $query = $this->mdl_3003->get_sk_by_tu_bagian_perihal_paging($api_search[0], $api_search[1], $limit, $api_search[3]);break; 
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function get_no_register($format = 'json'){
		$kode 			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode 		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search 	= $this->input->post('api_search');		
		switch($kode){
			case 3003: switch($subkode){
				default: 
					case 1: $query = $this->mdl_3003->get_no_register($api_search[0], $api_search[1], $api_search[2]);break;
			}
			break;		
		}
		$this->sia_api_lib_format->output($query, $format);	
	}
	
	function detail_arsip_by_id_surat_masuk($format = 'json'){
		$kode 			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode 		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search 	= $this->input->post('api_search');		
		switch($kode){
			case 3003: switch($subkode){
				default: 
					case 1: $query = $this->mdl_3003->detail_arsip_by_id_surat_masuk($api_search[0]);break; 
			}
			break;		
		}
		$this->sia_api_lib_format->output($query, $format);		
	}
	
	function detail_pengarah_by_id_surat_masuk($format = 'json'){
		$kode 			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode 		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search 	= $this->input->post('api_search');		
		switch($kode){
			case 3003: switch($subkode){
				default: 
					case 1: $query = $this->mdl_3003->detail_pengarah_by_id_surat_masuk($api_search[0]);break; 
			}
			break;		
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function get_penerima_ts_by_id_surat_masuk($format = 'json'){
		$kode 			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode 		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search 	= $this->input->post('api_search');		
		switch($kode){
			case 3003: switch($subkode){
				default: 
					case 1: $query = $this->mdl_3003->get_penerima_ts_by_id_surat_masuk($api_search[0]);break; 
			}
			break;		
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function get_pengolah_surat_by_id_surat_masuk($format = 'json'){
		$kode 			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode 		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search 	= $this->input->post('api_search');		
		switch($kode){
			case 3003: switch($subkode){
				default: 
					case 1: $query = $this->mdl_3003->get_pengolah_surat_by_id_surat_masuk($api_search[0]);break; 
			}
			break;		
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function get_penerima_disposisi_by_id_grup_surat($format = 'json'){
		$kode	 			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode 		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search 	= $this->input->post('api_search');		
		switch($kode){
			case 3003: switch($subkode){
				default: 
					case 1: $query = $this->mdl_3003->get_penerima_disposisi_by_id_grup_surat($api_search[0], $api_search[1]);break; 
			}
			break;		
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function get_disposisi_masuk($format = 'json'){
		$kode	 			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode 		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search 	= $this->input->post('api_search');		
		switch($kode){
			case 3003: switch($subkode){
				default: 
					case 1: $query = $this->mdl_3003->get_disposisi_masuk($api_search[0], $api_search[1], $api_search[2], $api_search[3]);break; 
					case 2: $query = $this->mdl_3003->get_disposisi_masuk2($api_search[0], $api_search[1], $api_search[2], $api_search[3]);break; 
					case 3: $query = $this->mdl_3003->get_disposisi_masuk3($api_search[0], $api_search[1], $api_search[2], $api_search[3]);break; 
			}
			break;		
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function pegawai_by_kd_jabatan($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->pegawai_by_kd_jabatan($api_search[0]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query,$format);	
	}
	
	function get_pegawai_on_disposisi($format = 'json'){
		$kode	 			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode 		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search 	= $this->input->post('api_search');		
		switch($kode){
			case 3003: switch($subkode){
				default: 
					case 1: $query = $this->mdl_3003->get_pegawai_on_disposisi($api_search[0], $api_search[1]);break; 
			}
			break;		
		}
		$this->sia_api_lib_format->output($query, $format);	
	}
	
	function total_tm_masuk_by_kd_pegawai($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search			= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->total_tm_masuk_by_kd_pegawai($api_search[0]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function total_tm_masuk_by_kd_jabatan($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search			= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->total_tm_masuk_by_kd_jabatan($api_search[0]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function get_tm_masuk_by_kd_pegawai($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search			= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->get_tm_masuk_by_kd_pegawai_limit($api_search[0], $api_search[1]); break;
					case 2: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->get_tm_masuk_by_kd_pegawai_paging($api_search[0], $limit, $api_search[2]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function get_tm_masuk_by_kd_jabatan($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search			= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->get_tm_masuk_by_kd_jabatan_limit($api_search[0], $api_search[1]); break;
					case 2: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->get_tm_masuk_by_kd_jabatan_paging($api_search[0], $limit, $api_search[2]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function total_tm_keluar_grup_by_kd_pegawai($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search			= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->total_tm_keluar_grup_by_kd_pegawai($api_search[0]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function total_tm_keluar_grup_by_kd_jabatan($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search			= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->total_tm_keluar_grup_by_kd_jabatan($api_search[0]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function get_tm_keluar_grup_by_kd_pegawai($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search			= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->get_tm_keluar_grup_by_kd_pegawai_limit($api_search[0], $api_search[1]); break;
					case 2: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->get_tm_keluar_grup_by_kd_pegawai_paging($api_search[0], $limit, $api_search[2]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function get_tm_keluar_grup_by_kd_jabatan($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search			= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->get_tm_keluar_grup_by_kd_jabatan_limit($api_search[0], $api_search[1]); break;
					case 2: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_3003->get_tm_keluar_grup_by_kd_jabatan_paging($api_search[0], $limit, $api_search[2]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function tot_ds_by_kd_pegawai($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->tot_ds_by_kd_pegawai($api_search[0], $api_search[1], $api_search[2]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function tot_ds_by_kd_jabatan($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->tot_ds_by_kd_jabatan($api_search[0], $api_search[1], $api_search[2]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function detail_dk_by_id_ds_sebelumnya($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->detail_dk_by_id_ds_sebelumnya($api_search[0]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function get_distribusi_surat_by_id_ds_sebelumnya($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 3003: switch($subkode){
				default:
					case 1: $query = $this->mdl_3003->get_distribusi_surat_by_id_ds_sebelumnya($api_search[0]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
}
?>