 <?php
if(!defined('BASEPATH'))exit('No direct script access allowed');
class Tnde_surat_keluar extends MX_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('tnde_surat_keluar/mdl_tnde_surat_keluar', 'mdl_90009');
	}

	function plain_post($format='json'){
		$query = [
			'message' => 'Sedang diakses : tnde_public/Tnde_surat_keluar_test/plain'
		];
		$this->response($query);
	}

	#edited MUHAJIROS
	function get_surat_keluar($format = 'json'){
		$kode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode	= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 90000: switch($subkode){ #total
				default:
					case 1: $query = $this->mdl_90009->total_surat_keluar($api_search[0], $api_search[1], $api_search[2]);break;
					case 2: $query = $this->mdl_90009->total_surat_keluar_cari($api_search[0], $api_search[1], $api_search[2], $api_search[3]);break;
			}
			break;
			case 90006: switch($subkode){ #Dataset
				default:
					// case 1: $query = $this->mdl_90009->get_surat_keluar_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3]);break;
					case 1: $query = $this->mdl_90009->get_surat_keluar_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3]);break; #FIX BY MUHAJIROS


					case 2: $limit = $api_search[3] + $api_search[4]; $query = $this->mdl_90009->get_surat_keluar_paging($api_search[0], $api_search[1], $api_search[2], $limit, $api_search[4]);break; #FIX BY MUHAJIROS
			}
			break;
			case 90007: switch($subkode){ #Pencarian
				default:
					case 1: $query = $this->mdl_90009->get_surat_keluar_cari_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3], $api_search[4]);break;
					case 2: $limit = $api_search[4] + $api_search[5]; $query = $this->mdl_90009->get_surat_keluar_cari_paging($api_search[0], $api_search[1], $api_search[2], $api_search[3], $limit, $api_search[5]);break;
			}
			break;
		}
		// $this->sia_api_lib_format->output($query, $format);
		// $this->response($query, 200);
		$this->output
	        ->set_content_type('application/json')
	        ->set_output(json_encode($query));
	}
	
	function get_surat_keluar_pencarian($format = 'json'){
		$kode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode	= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 90009: switch($subkode){
				default:
					case 1: $query = $this->mdl_90009->get_surat_keluar_pencarian_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3]);break;
					case 2: $limit = $api_search[3] + $api_search[4]; $query = $this->mdl_90009->get_surat_keluar_pencarian_paging($api_search[0], $api_search[1], $api_search[2], $limit, $api_search[4]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	#edited MUHAJIROS
	function detail_surat_keluar($format = 'json'){
		$kode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode	= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 90001: switch($subkode){ #VERSI 2 (LEBIH BARU)
				default:
					case 1: $query = $this->mdl_90009->detail_surat_keluar_by_id_surat_keluar_v2($api_search[0]);break; #FIX muhajiros
					case 2: $query = $this->mdl_90009->detail_sk_2_by_id_surat_keluar_v2($api_search[0]);break; #FIX muhajiros
					case 3: $query = $this->mdl_90009->detail_sk_by_no_surat_v2($api_search[0]);break;
			}
			break;			
			case 90002: switch($subkode){ #VERSI 2.1 (PALING BARU)
				default:
					case 1: $query = $this->mdl_90009->detail_surat_keluar_by_no_surat($api_search[0]);break;
			}
			break;
			case 90009: switch($subkode){ #VERSI 1 (LAMA)
				default:
					case 1: $query = $this->mdl_90009->detail_surat_keluar_by_id_surat_keluar($api_search[0]);break;
					case 2: $query = $this->mdl_90009->detail_sk_2_by_id_surat_keluar($api_search[0]);break;
					case 3: $query = $this->mdl_90009->detail_sk_by_no_surat($api_search[0]);break;
			}
			break;
		}
		// $this->response($query);
		$this->output
	        ->set_content_type('application/json')
	        ->set_output(json_encode($query));
	}	
	
	function penomoran($format = 'json'){
		$kode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode	= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 90001: switch($subkode){ #VERSI 2 (BARU)
				default:
					case 1: $query = $this->mdl_90009->get_no_urut_v2($api_search[0], $api_search[1], $api_search[2]);break; // tahun, unit_id, kd_jenis_surat
					case 2: $query = $this->mdl_90009->get_no_urut2_v2($api_search[0], $api_search[1], $api_search[2], $api_search[3]);break; // tahun, unit_id, kd_jenis_surat, tgl_surat
					case 3: $query = $this->mdl_90009->get_no_urut_sk($api_search[0], $api_search[1], $api_search[2], $api_search[3]);break; // tahun, unit_id, kd_jenis_surat, tgl_surat
			}
			break;
			case 90002: switch($subkode){ #VERSI 2.1 (PALING BARU)
				default:
					case 1: 
					$this->load->library('lib_surat_keluar');
					$query = $this->lib_surat_keluar->get_no_surat($api_search[0]);
					break;
					case 2: 
					$this->load->library('lib_surat_keluar');
					$query = $this->lib_surat_keluar->get_no_surat_his($api_search[0]);
					break;
					case 3: 
					$this->load->library('lib_surat_keluar');
					$query = $this->lib_surat_keluar->get_no_surat_his_format($api_search[0]); #Format no surat baru
					break;
			}
			break;
			case 90009: switch($subkode){
				default:
					case 1: $query = $this->mdl_90009->get_no_urut($api_search[0], $api_search[1], $api_search[2]);break; // tahun, kd_tu_bagian, kd_jenis_surat
					case 2: $query = $this->mdl_90009->get_no_urut2($api_search[0], $api_search[1], $api_search[2], $api_search[3]);break; // tahun, kd_tu_bagian, kd_jenis_surat, tgl_surat
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}	
	
	function order_nomor_terakhir($format = 'json'){
		$kode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode	= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 90001: switch($subkode){
				default:
					case 1: $query = $this->mdl_90009->order_nomor_terakhir($api_search[0], $api_search[1], $api_search[2]);break;
					case 2: $query = $this->mdl_90009->order_nomor_terakhir_mundur($api_search[0], $api_search[1], $api_search[2], $api_search[3]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	
	function insert_penerima_sk($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 90009: switch($subkode){
				default:
					case 1: $query = $this->mdl_90009->insert_penerima_sk($api_search[0]);break;
			}
			break;
		}
		// $this->sia_api_lib_format->output($query, $format);
		$this->output
	        ->set_content_type('application/json')
	        ->set_output(json_encode($query));
	}
	
	#edited MUHAJIROS
	function get_penerima_sk($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 90008: switch($subkode){
				default:
					case 1: $query = $this->mdl_90009->get_penerima_sk_for_sm($api_search[0]);break;
			}
			break;
			case 90009: switch($subkode){
				default:
					case 1: $query = $this->mdl_90009->get_penerima_sk($api_search[0], $api_search[1]);break;
					case 2: $query = $this->mdl_90009->get_penerima_sk_by_no_surat($api_search[0], $api_search[1]);break;
			}
			break;
		}
		// $this->sia_api_lib_format->output($query, $format);
		// $this->response($query);
		$this->output
	        ->set_content_type('application/json')
	        ->set_output(json_encode($query));
	}
	
	function update_surat_keluar($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 90001: switch($subkode){
				default:
					case 1: $query = $this->mdl_90009->update_surat_keluar_v2($api_search[0], $api_search[1]);break;
			}
			break;
			case 90002: switch($subkode){
				default:
					case 1: $query = $this->mdl_90009->update_isi_surat_by_no_surat($api_search[0], $api_search[1]);break;
			}
			break;
			case 90009: switch($subkode){
				default:
					case 1: $query = $this->mdl_90009->update_surat_keluar($api_search[0], $api_search[1]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	
	function insert_procedure_penerima_sk($format='json'){
		$kode = (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode = (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search = $this->input->post('api_search');
		$data = array(
            array('method' => 'IN', 'name' => ':ID_SURAT_KELUAR', 'value' => $api_search[0]['ID_SURAT_KELUAR'], 'length' => 15, 'type' => SQLT_CHR),
            array('method' => 'IN', 'name' => ':PENERIMA_SURAT', 'value' => $api_search[0]['PENERIMA_SURAT'], 'length' => 100, 'type' => SQLT_CHR),
            array('method' => 'IN', 'name' => ':JABATAN_PENERIMA', 'value' => $api_search[0]['JABATAN_PENERIMA'], 'length' => 200, 'type' => SQLT_CHR),
            array('method' => 'IN', 'name' => ':KD_GRUP_TUJUAN', 'value' => $api_search[0]['KD_GRUP_TUJUAN'], 'length' => 6, 'type' => SQLT_CHR),
            array('method' => 'IN', 'name' => ':KD_STATUS_DISTRIBUSI', 'value' => $api_search[0]['KD_STATUS_DISTRIBUSI'], 'length' => 6, 'type' => SQLT_CHR),
            array('method' => 'IN', 'name' => ':ID_STATUS_SM', 'value' => $api_search[0]['ID_STATUS_SM'], 'length' => 6, 'type' => SQLT_CHR),
            array('method' => 'IN', 'name' => ':KD_JENIS_TEMBUSAN', 'value' => $api_search[0]['KD_JENIS_TEMBUSAN'], 'length' => 6, 'type' => SQLT_CHR),
            array('method' => 'IN', 'name' => ':KD_JENIS_KEPADA', 'value' => $api_search[0]['KD_JENIS_KEPADA'], 'length' => 6, 'type' => SQLT_CHR),
            array('method' => 'IN', 'name' => ':NO_URUT', 'value' => $api_search[0]['NO_URUT'], 'length' => 6, 'type' => SQLT_CHR),
            array('method' => 'OUT', 'name' => ':hasil', 'value' => '', 'length' => 32, 'type' => SQLT_CHR)
        );
		switch($kode){
			case 90009: switch($subkode){
				default:
				case 1: $query = $this->mdl_90009->insert_procedure_penerima_sk($data); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
		
	#DIUBAH OLEH MUHAJIROS
	function insert_surat_kaluar($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 90001: switch($subkode){
				default:
					case 1: $query = $this->mdl_90009->insert_surat_kaluar_v2($api_search[0]);break; #iki sing ta nggo
					case 2: $query = $this->mdl_90009->insert_surat_kaluar_v3($api_search[0]);break;
			}
			break;
			case 90009: switch($subkode){
				default:
					case 1: $query = $this->mdl_90009->insert_surat_kaluar($api_search[0]);break;
			}
			break;
		}
		// $this->sia_api_lib_format->output($query, $format);	
		// $this->response($query);
		$this->output
	        ->set_content_type('application/json')
	        ->set_output(json_encode($query));
	}
	
	
	function set_nomor_surat($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 90009: switch($subkode){
				default:
					case 1: $query = $this->mdl_90009->set_nomor_surat($api_search[0], $api_search[1]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function cek_pegawai_psd($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 90009: switch($subkode){
				default:
					case 1: $query = $this->mdl_90009->cek_pegawai_psd($api_search[0], $api_search[1]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}	
	
	function delete_pegawai_psd($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 90009: switch($subkode){
				default:
					case 1: $query = $this->mdl_90009->delete_pegawai_psd($api_search[0], $api_search[1]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}	
	
	function surat_keluar_search($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 90009: switch($subkode){
				default:
					case 1: $query = $this->mdl_90009->surat_keluar_search($api_search[0]);break;
					case 2: $query = $this->mdl_90009->surat_keluar_search2($api_search[0], $api_search[1]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
			
	
	function simpan_surat_personal($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 90001: switch($subkode){
				default:
					case 1: $query = $this->mdl_90009->simpan_surat_personal($api_search[0]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}	
	
	function get_surat_personal($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 90001: switch($subkode){
				default:
					case 1: $query = $this->mdl_90009->tot_surat_personal($api_search[0]);break;
			}
			break;			
			case 90006: switch($subkode){
				default:
					case 1: $query = $this->mdl_90009->get_surat_personal_limit($api_search[0], $api_search[1]);break;
					case 2: $limit	= $api_search[1] + $api_search[2]; $query = $this->mdl_90009->get_surat_personal_paging($api_search[0], $limit, $api_search[2]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}	
	
	function get_surat_personal_cari($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 9000: switch($subkode){
				default:
					case 1: $query = $this->mdl_9000->tot_surat_personal_cari($api_search[0], $api_search[1], $api_search[2]);break;
					case 2: $query = $this->mdl_9000->get_surat_personal_cari($api_search[0], $api_search[1], $api_search[2], $api_search[3]);break;
			}
			break;
			case 90001: switch($subkode){
				default:
					case 1: $query = $this->mdl_90009->tot_surat_personal_cari($api_search[0], $api_search[1], $api_search[2]);break;
			}
			break;			
			case 90006: switch($subkode){
				default:
					case 1: $query = $this->mdl_90009->get_surat_personal_cari_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3]);break;
					case 2: $limit	= $api_search[3] + $api_search[4]; $query = $this->mdl_90009->get_surat_personal_cari_paging($api_search[0], $api_search[1], $api_search[2], $limit, $api_search[4]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function detail_surat_personal($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 90001: switch($subkode){
				default:
					case 1: $query = $this->mdl_90009->detail_surat_personal($api_search[0]);break; #id_surat_personal
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}		
	
	function update_surat_personal($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 90001: switch($subkode){
				default:
					case 1: $query = $this->mdl_90009->update_surat_personal($api_search[0], $api_search[1]);break; #data, id_surat_personal
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function insert_procedure_sp_tujuan($format='json'){
		$kode = (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode = (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search = $this->input->post('api_search');
		$data = array(
            array('method' => 'IN', 'name' => ':ID_SURAT_PERSONAL', 'value' => $api_search[0]['ID_SURAT_PERSONAL'], 'length' => 15, 'type' => SQLT_CHR),
            array('method' => 'IN', 'name' => ':PENERIMA_SURAT', 'value' => $api_search[0]['PENERIMA_SURAT'], 'length' => 25, 'type' => SQLT_CHR),
            array('method' => 'IN', 'name' => ':JABATAN_PENERIMA', 'value' => $api_search[0]['JABATAN_PENERIMA'], 'length' => 10, 'type' => SQLT_CHR),
            array('method' => 'IN', 'name' => ':KD_GRUP_TUJUAN', 'value' => $api_search[0]['KD_GRUP_TUJUAN'], 'length' => 10, 'type' => SQLT_CHR),
            array('method' => 'IN', 'name' => ':NO_URUT', 'value' => $api_search[0]['NO_URUT'], 'length' => 6, 'type' => SQLT_CHR),
            array('method' => 'OUT', 'name' => ':hasil', 'value' => '', 'length' => 32, 'type' => SQLT_CHR)
        );
		switch($kode){
			case 90001: switch($subkode){
				default:
				case 1: $query = $this->mdl_90009->insert_procedure_sp_tujuan($data); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function order_nomor($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 90001: switch($subkode){
				default:
					case 1: $query = $this->mdl_90009->order_nomor($api_search[0]);break;
			}
			break;
			case 90006: switch($subkode){
				default:
					case 1: $query = $this->mdl_90009->get_order_nomor_limit($api_search[0]);break;
					case 2: $limit = $api_search[0] + $api_search[1]; $query = $this->mdl_90009->get_order_nomor_paging($limit, $api_search[1]);break;
			}
			break;
			case 90007: switch($subkode){
				default:
					case 1: $query = $this->mdl_90009->detail_order_nomor($api_search[0]);break;
			}
			break;
			case 90010: switch($subkode){
				default:
					case 1: $query = $this->mdl_90009->total_order_nomor();break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function daftar_order_nomor($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 90001: switch($subkode){
				default:
					case 1: $query = $this->mdl_90009->daftar_order_nomor($api_search[0]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}	
			
	function get_tujuan_eks($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 90001: switch($subkode){
				default:
					case 1: $query = $this->mdl_90009->get_tujuan_eks($api_search[0]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
}
?>