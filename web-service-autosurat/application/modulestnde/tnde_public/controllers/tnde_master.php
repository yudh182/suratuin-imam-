<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');
class Tnde_master extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('tnde_master/mdl_tnde_master', 'mdl_11001');
	}
	
	function get_psd($format = 'json'){
		$kode				=	(int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		=	(int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	=	$this->input->post('api_search');
		switch($kode){
			case 11001:	switch($subkode){
				default:
					case 1: $query = $this->mdl_11001->get_psd_limit($api_search[0]); break;
					case 2: $limit = $api_search[0] + $api_search[1]; $query = $this->mdl_11001->get_psd_paging($limit, $api_search[1]); break;
					case 3: $query = $this->mdl_11001->get_psd_cari_limit($api_search[0], $api_search[1]); break;
					case 4: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_11001->get_psd_cari_paging($api_search[0], $limit, $api_search[2]); break;
			}
			break;
			case 11002:	switch($subkode){
				default:
					case 1: $query = $this->mdl_11001->get_psd_simple_limit($api_search[0], $api_search[1], $api_search[2]); break;
					case 2: $limit = $api_search[2] + $api_search[3]; $query = $this->mdl_11001->get_psd_simple_paging($api_search[0], $api_search[1], $limit, $api_search[3]); break;
			}
			break;
			case 110012: switch($subkode){
				default:
					case 1: $query = $this->mdl_11001->get_psd_by_kd_tu_bagian($api_search[0]); break;
					case 2: $query = $this->mdl_11001->get_psd_in_kd_jabatan($api_search[0]); break;
					case 3: $query = $this->mdl_11001->get_psd_in_kd_jabatan2($api_search[0]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}		
	
	function get_klasifikasi_surat($format = 'json'){
		$kode				=	(int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		=	(int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	=	$this->input->post('api_search');
		switch($kode){
			case 11000:	switch($subkode){
				default:
					case 1: $query = $this->mdl_11001->get_klasifikasi_surat_like($api_search[0]); break;
			}
			break;
			case 11001:	switch($subkode){
				default:
					case 1: $query = $this->mdl_11001->get_klasifikasi_surat_limit($api_search[0]); break;
					case 2: $limit = $api_search[0] + $api_search[1]; $query = $this->mdl_11001->get_klasifikasi_surat_paging($limit, $api_search[1]); break;
					case 3: $query = $this->mdl_11001->get_klasifikasi_surat_cari_limit($api_search[0], $api_search[1]); break;
					case 4: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_11001->get_klasifikasi_surat_cari_paging($api_search[0], $limit, $api_search[2]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function detail_psd($format = 'json'){
		$kode				=	(int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		=	(int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	=	$this->input->post('api_search');
		switch($kode){
			case 11001:	switch($subkode){
				default:
					case 1: $query = $this->mdl_11001->detail_psd_by_id_psd($api_search[0]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}	
	
	function cek_psd($format = 'json'){
		$kode				=	(int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		=	(int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	=	$this->input->post('api_search');
		switch($kode){
			case 11001:	switch($subkode){
				default:
					case 1: $query = $this->mdl_11001->cek_kd_psd($api_search[0], $api_search[1]);  break; // id_psd, kd_psd
					case 2: $query = $this->mdl_11001->cek_kd_jabatan_psd($api_search[0], $api_search[1]);  break; // id_psd, kd_jabatan
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}	
	
	function psd_sk($format = 'json'){
		$kode				=	(int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		=	(int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	=	$this->input->post('api_search');
		switch($kode){
			case 11001:	switch($subkode){
				default:
					case 1: $query = $this->mdl_11001->psd_sk($api_search[0]);  break; 
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}	
	
	function cek_klasifikasi_surat($format = 'json'){
		$kode				=	(int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		=	(int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	=	$this->input->post('api_search');
		switch($kode){
			case 11001:	switch($subkode){
				default:
					case 1: $query = $this->mdl_11001->cek_kd_klasifikasi_surat($api_search[0], $api_search[1]);  break; 
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}	
	
	function total_psd($format = 'json'){
		$kode				=	(int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		=	(int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	=	$this->input->post('api_search');
		switch($kode){
			case 11001:	switch($subkode){
				default:
					case 1: $query = $this->mdl_11001->total_psd_cari($api_search[0]);  break; // id_psd, kd_psd
			}
			break;
			case 11006:	switch($subkode){
				default:
					case 1: $query = $this->mdl_11001->total_psd_simple_cari($api_search[0], $api_search[1]);  break; // id_psd, kd_psd
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}	
	
	function total_klasifikasi_surat($format = 'json'){
		$kode				=	(int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		=	(int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	=	$this->input->post('api_search');
		switch($kode){
			case 11001:	switch($subkode){
				default:
					case 1: $query = $this->mdl_11001->total_klasifikasi_surat_cari($api_search[0]);  break; // id_psd, kd_psd
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function get_jenis_surat($format = 'json'){
		$kode				=	(int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		=	(int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	=	$this->input->post('api_search');
		switch($kode){
			case 11001:	switch($subkode){
				default:
					case 1: $query = $this->mdl_11001->jenis_surat_by_field_in($api_search[0], $api_search[1], $api_search[2]);  break;
					case 2: $query = $this->mdl_11001->surat_sekategori($api_search[0]);  break;
					case 3: $query = $this->mdl_11001->semua_jenis_surat_by_tgl($api_search[0]);  break;
					case 4: $query = $this->mdl_11001->his_jenis_surat_by_kd_jenis_surat($api_search[0]);  break;
					case 5: $query = $this->mdl_11001->his_jenis_surat_by_id_his_jenis_surat($api_search[0]);  break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function jenis_surat($format = 'json'){
		$kode				=	(int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		=	(int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	=	$this->input->post('api_search');
		switch($kode){
			case 11001:	switch($subkode){
				default:
					case 1: $query = $this->mdl_11001->insert_his_jenis_surat($api_search[0]);  break;
					case 2: $query = $this->mdl_11001->update_his_jenis_surat($api_search[0], $api_search[1]);  break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function update_config_hari_kerja($format = 'json'){
		$kode				=	(int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		=	(int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	=	$this->input->post('api_search');
		switch($kode){
			case 11001:	switch($subkode){
				default:
					case 1: $query = $this->mdl_11001->update_config_hari_kerja($api_search[0], $api_search[1]);  break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function insert_procedure_config_hk($format='json'){
		$kode = (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode = (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search = $this->input->post('api_search');
		$data = array(
            array('method' => 'IN', 'name' => ':PERIODE_AWAL', 'value' => $api_search[0]['PERIODE_AWAL'], 'length' => 15, 'type' => SQLT_CHR),
            array('method' => 'IN', 'name' => ':PERIODE_SELESAI', 'value' => $api_search[0]['PERIODE_SELESAI'], 'length' => 15, 'type' => SQLT_CHR),
            array('method' => 'IN', 'name' => ':DIBUAT', 'value' => $api_search[0]['DIBUAT'], 'length' => 30, 'type' => SQLT_CHR),
            array('method' => 'IN', 'name' => ':TGL_DIBUAT', 'value' => $api_search[0]['TGL_DIBUAT'], 'length' => 30, 'type' => SQLT_CHR),
            array('method' => 'OUT', 'name' => ':hasil', 'value' => '', 'length' => 32, 'type' => SQLT_CHR)
        );
		switch($kode){
			case 11001: switch($subkode){
				default:
				case 1: $query = $this->mdl_11001->insert_procedure_config_hk($data); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}	
	
	function insert_procedur_hari_kerja($format='json'){
		$kode = (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode = (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search = $this->input->post('api_search');
		$data = array(
            array('method' => 'IN', 'name' => ':ID_CONFIG_HK', 'value' => $api_search[0]['ID_CONFIG_HK'], 'length' => 15, 'type' => SQLT_CHR),
            array('method' => 'IN', 'name' => ':KD_HARI', 'value' => $api_search[0]['KD_HARI'], 'length' => 15, 'type' => SQLT_CHR),
            array('method' => 'OUT', 'name' => ':hasil', 'value' => '', 'length' => 32, 'type' => SQLT_CHR)
        );
		switch($kode){
			case 11001: switch($subkode){
				default:
				case 1: $query = $this->mdl_11001->insert_procedur_hari_kerja($data); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function get_config_hari_kerja($format = 'json'){
		$kode				=	(int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		=	(int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	=	$this->input->post('api_search');
		switch($kode){
			case 11001:	switch($subkode){
				default:
					case 1: $query = $this->mdl_11001->get_config_hari_kerja();  break;
			}
			break;
			case 11002:	switch($subkode){
				default:
					case 1: 
						$tmp 			= $this->mdl_11001->get_hari_kerja_by_tgl($api_search[0]); 
						// $id_list 		= array();
						// $grup_satu	= array();
						// if(!empty($tmp)){
							// foreach($tmp as $val){
								// $grup_satu[$val['ID_CONFIG_HK']]['ID_CONFIG_HK'] 	= $val['ID_CONFIG_HK'];
								// $grup_satu[$val['ID_CONFIG_HK']]['PERIODE_AWAL'] 	= $val['PERIODE_SELESAI'];
								// $grup_satu[$val['ID_CONFIG_HK']]['DIBUAT'] 					= $val['DIBUAT'];
								// $grup_satu[$val['ID_CONFIG_HK']]['TGL_DIBUAT'] 			= $val['TGL_DIBUAT'];
								// $grup_satu[$val['ID_CONFIG_HK']]['DIUPDATE'] 			= $val['DIUPDATE'];
								// $grup_satu[$val['ID_CONFIG_HK']]['TGL_DIUPDATE'] 	= $val['TGL_DIUPDATE'];
								// $grup_satu[$val['ID_CONFIG_HK']]['ID_HARI_KERJA'] 	= $val['ID_HARI_KERJA'];
								// $grup_satu[$val['ID_CONFIG_HK']]['HARI_KERJA'][] 		= array('KD_HARI' => $val['KD_HARI'], 
																																		// 'NM_HARI' => $val['NM_HARI'],
																																		// 'NM_HARI_ENG' => $val['NM_HARI_ENG']
																																		// );
							// }
						// }
						$query = $tmp;
					break;
					case 2: $query = $this->mdl_11001->detail_config_hari_kerja_by_id_config_hk($api_search[0]); break;
			}
			break;
			case 11006:	switch($subkode){
				default:
					case 1: $query = $this->mdl_11001->hari_kerja_by_id_config_hk($api_search[0]);  break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function jabatan_surat_masuk($format = 'json'){
		$kode				=	(int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		=	(int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	=	$this->input->post('api_search');
		switch($kode){
			case 11001:	switch($subkode){
				default:
					case 1: $query = $this->mdl_11001->kd_terakhir_jab_arsip_surat();  break;
					case 2: $query = $this->mdl_11001->kd_terakhir_jab_pengarah_surat();  break;
			}
			break;
			case 11002:	switch($subkode){
				default:
					case 1: $query = $this->mdl_11001->get_unit_jab_surat_masuk();break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function jabatan_surat_keluar($format = 'json'){
		$kode				=	(int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		=	(int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	=	$this->input->post('api_search');
		switch($kode){
			case 11001:	switch($subkode){
				default:
					case 1: $query = $this->mdl_11001->kd_terakhir_jab_surat_keluar();  break;
			}
			break;
			case 11002:	switch($subkode){
				default:
					case 1: $query = $this->mdl_11001->get_unit_jab_surat_keluar();break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function hak_akses_sk($format = 'json'){
		$kode				=	(int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		=	(int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	=	$this->input->post('api_search');
		switch($kode){
			case 11001:	switch($subkode){
				default:
					case 1: $query = $this->mdl_11001->hak_akses_sk_by_unit($api_search[0]);  break;
					case 2: $query = $this->mdl_11001->his_hak_akses_by_kd_jab_sistem($api_search[0]);  break;
					case 3: $query = $this->mdl_11001->his_hak_akses_by_id_his_jab_sk($api_search[0]);  break;
					case 4: 
						$tgl			= (!empty($api_search[1]) ? $api_search[1] : date('d/m/Y'));
						$query 	= $this->mdl_11001->hak_akses_sk_by_unit_tgl($api_search[0], $tgl);  
					break;
			}
			break;
			case 11002:	switch($subkode){
				default:
					case 1: $query = $this->mdl_11001->insert_his_jab_sk($api_search[0]);  break;
					case 2: $query = $this->mdl_11001->udpate_his_jab_sk($api_search[0], $api_search[1]);  break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
}
?>
