<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');
class Tnde_config extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('tnde_config/mdl_tnde_config', 'mdl_10001');
	}
	
	function get_config_penomoran($format = 'json'){
		$kode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode	= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 10000: switch($subkode){
				default:
					case 1: $query = $this->mdl_10001->config_penomoran_by_unit_id($api_search[0], $api_search[1]);break; // kd_kat_penomoran, kd_unit_id
					case 2: $query = $this->mdl_10001->config_penomoran_by_unit_id2($api_search[0], $api_search[1], $api_search[2]);break; // tgl, unit_id, kd_kat_penomoran
					case 3: $query = $this->mdl_10001->config_penomoran($api_search[0]);break;
			}
			break;
			case 10001: switch($subkode){
				default:
					case 1: $query = $this->mdl_10001->config_penomoran_by_kd_tu_bagian($api_search[0], $api_search[1]);break; // kd_kat_penomoran, kd_tu_bagian
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
}
?>