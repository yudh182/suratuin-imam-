<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');
class Tnde_surat_masuk extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->model('tnde_surat_masuk/mdl_tnde_surat_masuk', 'mdl_80008');
		$this->load->model('tnde_surat_masuk/mdl_tnde_surat_masuk_v2', 'mdl_8000');
		$this->load->model('tnde_surat_masuk/mdl_tnde_sm_mhs', 'mdl_1000');
	}
	
	function get_surat_masuk($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 80001: switch($subkode){
				default:
					case 1: $query = $this->mdl_80008->get_surat_masuk_by_unit_id_limit($api_search[0], $api_search[1]);break; //by unit_id_arr, limit
					case 2: $limit= $api_search[1] + $api_search[2]; $query = $this->mdl_80008->get_surat_masuk_by_unit_id_paging($api_search[0], $limit, $api_search[2]);break; //by unit_id_arr	
					case 3: $query = $this->mdl_80008->get_surat_masuk_by_unit_id_cari_limit($api_search[0], $api_search[1], $api_search[2]);break; //by unit_id
					case 4: $limit= $api_search[2] + $api_search[3]; $query = $this->mdl_80008->get_surat_masuk_by_unit_id_cari_paging($api_search[0], $api_search[1], $limit, $api_search[3]);break; //by unit_id
					case 5: $query = $this->mdl_80008->get_surat_masuk_by_unit_id_bln($api_search[0]); break;
					case 6: $query = $this->mdl_80008->get_surat_masuk_by_unit_id_periode($api_search[0]); break;
					case 7: $query = $this->mdl_80008->tot_surat_masuk_by_periode($api_search[0]); break;
			}
			break;
			case 80008: switch($subkode){
				default:
					case 1: $query = $this->mdl_80008->get_surat_masuk_by_tu_bagian_limit($api_search[0], $api_search[1]);break; //by tu_bagian
					case 2: $limit= $api_search[1] + $api_search[2]; $query = $this->mdl_80008->get_surat_masuk_by_tu_bagian_paging($api_search[0], $limit, $api_search[2]);break; //by tu_bagian					
					case 3: $query = $this->mdl_80008->get_surat_masuk_by_tu_bagian_cari_limit($api_search[0], $api_search[1], $api_search[2]);break; //by tu_bagian
					case 4: $limit= $api_search[2] + $api_search[3]; $query = $this->mdl_80008->get_surat_masuk_by_tu_bagian_cari_paging($api_search[0], $api_search[1], $limit, $api_search[3]);break; //by tu_bagian
					case 5: $query = $this->mdl_80008->get_surat_masuk_by_tu_bagian_bln($api_search[0]); break;
					case 6: $query = $this->mdl_80008->get_surat_masuk_by_tu_bagian_periode($api_search[0]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function get_surat_masuk_pegawai($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 80001: switch($subkode){ #TOTAL
				default:
					case 1: $query = $this->mdl_80008->total_surat_masuk_by_kd_pgw_str_id($api_search[0], $api_search[1]);break; //by kd_pgw, str_id
			}
			break;
			case 80006: switch($subkode){ #DATA
				default:
					case 1: $query = $this->mdl_80008->get_surat_masuk_by_kd_pgw_str_id_limit($api_search[0], $api_search[1], $api_search[2]);break; //by kd_pgw, str_id
					case 2: $limit = $api_search[2] + $api_search[3]; $query = $this->mdl_80008->get_surat_masuk_by_kd_pgw_str_id_paging($api_search[0], $api_search[1], $limit, $api_search[3]);break; 
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function get_distribusi_personal($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 8000: switch($subkode){
				default:
					case 1: $query = $this->mdl_8000->total_distribusi_by_kd_pgw($api_search[0], $api_search[1]);break;
					case 2: $query = $this->mdl_8000->get_distribusi_by_kd_pgw_limit($api_search[0], $api_search[1], $api_search[2]);break; 
					case 3: $limit = $api_search[2] + $api_search[3]; $query = $this->mdl_8000->get_distribusi_by_kd_pgw_paging($api_search[0], $api_search[1], $limit, $api_search[3]);break; 
			}
			break;
			case 1000: switch($subkode){
				default:
					case 2: $query = $this->mdl_1000->total_distribusi_status_akses_by_nim($api_search[0], $api_search[1], $api_search[2]);break; //by kd_status_distribusi, id_status_sm, nim
			}
			break;
			case 1001: switch($subkode){
				default:
					case 1: $query = $this->mdl_1000->get_distribusi_by_nim_limit($api_search[0], $api_search[1], $api_search[2]);break;
					case 2: $limit = $api_search[2] + $api_search[3]; $query = $this->mdl_1000->get_distribusi_by_nim_paging($api_search[0], $api_search[1], $limit, $api_search[3]);break; 
			}
			break;
			case 1003: switch($subkode){
				default:
					case 1: $query = $this->mdl_1000->total_distribusi_status_akses_by_nim_v2($api_search[0], $api_search[1], $api_search[2]);break;
					case 2: $query = $this->mdl_1000->get_distribusi_by_nim_limit_v2($api_search[0], $api_search[1], $api_search[2]);break;
					case 3: $limit = $api_search[2] + $api_search[3]; $query = $this->mdl_1000->get_distribusi_by_nim_paging_v2($api_search[0], $api_search[1], $limit, $api_search[3]);break; 
			}
			break;
			case 80001: switch($subkode){ #TOTAL
				default:
					case 1: $query = $this->mdl_80008->total_distribusi_by_kd_pgw($api_search[0], $api_search[1]);break; //by kd_status_distribusi, str_id
					case 2: $query = $this->mdl_80008->total_distribusi_status_akses_by_kd_pgw($api_search[0], $api_search[1], $api_search[2]);break; //by kd_status_distribusi, id_status_sm, str_id
			}
			break;
			case 80006: switch($subkode){ #DATA
				default:
					case 1: $query = $this->mdl_80008->get_distribusi_by_kd_pgw_limit($api_search[0], $api_search[1], $api_search[2]);break; #by kd_status_distribusi, str_id
					case 2: $limit = $api_search[2] + $api_search[3]; $query = $this->mdl_80008->get_distribusi_by_kd_pgw_paging($api_search[0], $api_search[1], $limit, $api_search[3]);break; 
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function get_distribusi_pejabat($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 8000: switch($subkode){ #TOTAL
				default:
					case 1: $query = $this->mdl_8000->total_distribusi_by_str_id($api_search[0], $api_search[1]);break; //by kd_status_distribusi, str_id
					case 2: $query = $this->mdl_8000->get_distribusi_by_str_id_limit($api_search[0], $api_search[1], $api_search[2]);break; #by kd_status_distribusi, str_id
					case 3: $limit = $api_search[2] + $api_search[3]; $query = $this->mdl_8000->get_distribusi_by_str_id_paging($api_search[0], $api_search[1], $limit, $api_search[3]);break; 
			}
			break;
			case 80001: switch($subkode){ #TOTAL
				default:
					case 1: $query = $this->mdl_80008->total_distribusi_by_str_id($api_search[0], $api_search[1]);break; //by kd_status_distribusi, str_id
					case 2: $query = $this->mdl_80008->total_distribusi_status_akses_by_str_id($api_search[0], $api_search[1], $api_search[2]);break; //by kd_status_distribusi, id_status_sm, str_id
			}
			break;
			case 80006: switch($subkode){ #DATA
				default:
					case 1: $query = $this->mdl_80008->get_distribusi_by_str_id_limit($api_search[0], $api_search[1], $api_search[2]);break; #by kd_status_distribusi, str_id
					case 2: $limit = $api_search[2] + $api_search[3]; $query = $this->mdl_80008->get_distribusi_by_str_id_paging($api_search[0], $api_search[1], $limit, $api_search[3]);break; 
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function get_distribusi_pegawai($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 8000: switch($subkode){
				default:
					case 1: $query = $this->mdl_8000->total_distribusi_by_kd_pgw_str_id($api_search[0], $api_search[1], $api_search[2]);break; 
					case 2: $query = $this->mdl_8000->get_distribusi_by_kd_pgw_str_id_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3]);break; #by kd_status_distribusi, kd_pgw, str_id
					case 3: $limit = $api_search[3] + $api_search[4]; $query = $this->mdl_8000->get_distribusi_by_kd_pgw_str_id_paging($api_search[0], $api_search[1], $api_search[2], $limit, $api_search[4]);break; 
			}
			break;
			case 80001: switch($subkode){ #TOTAL
				default:
					case 1: $query = $this->mdl_80008->total_distribusi_by_kd_pgw_str_id($api_search[0], $api_search[1], $api_search[2]);break; //by kd_status_distribusi, kd_pgw, str_id
					case 2: $query = $this->mdl_80008->total_distribusi_by_status_akses_kd_pgw_str_id($api_search[0], $api_search[1], $api_search[2], $api_search[3]);break; //by kd_status_distribusi, id_status_sm, kd_pgw, str_id
			}
			break;
			case 80006: switch($subkode){ #DATA
				default:
					case 1: $query = $this->mdl_80008->get_distribusi_by_kd_pgw_str_id_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3]);break; #by kd_status_distribusi, kd_pgw, str_id
					case 2: $limit = $api_search[3] + $api_search[4]; $query = $this->mdl_80008->get_distribusi_by_kd_pgw_str_id_paging($api_search[0], $api_search[1], $api_search[2], $limit, $api_search[4]);break; 
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function get_surat_masuk_pegawai_cari($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 80001: switch($subkode){ #TOTAL
				default:
					case 1: $query = $this->mdl_80008->total_sm_cari_by_kd_pgw_str_id_tgl_awal($api_search[0], $api_search[1], $api_search[2], $api_search[3]);break;
					case 2: $query = $this->mdl_80008->total_sm_cari_by_kd_pgw_str_id_tgl_akhir($api_search[0], $api_search[1], $api_search[2], $api_search[3]);break;
					case 3: $query = $this->mdl_80008->total_sm_cari_by_kd_pgw_str_id_periode($api_search[0], $api_search[1], $api_search[2], $api_search[3], $api_search[4]);break;
					case 4: $query = $this->mdl_80008->total_sm_cari_by_kd_pgw_str_id($api_search[0], $api_search[1], $api_search[2]);break;
			}
			break;
			case 80006: switch($subkode){ #DATA
				default:
					case 1: 
						$query = $this->mdl_80008->get_sm_cari_by_kd_pgw_str_id_tgl_awal_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3], $api_search[4]);
						break;					
					case 2: 
						$limit	= $api_search[4] + $api_search[5];
						$query = $this->mdl_80008->get_sm_cari_by_kd_pgw_str_id_tgl_awal_paging($api_search[0], $api_search[1], $api_search[2], $api_search[3], $limit, $api_search[5]);
						break;
					case 3: 
						$query = $this->mdl_80008->get_sm_cari_by_kd_pgw_str_id_tgl_akhir_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3], $api_search[4]);
						break;					
					case 4: 
						$limit	= $api_search[4] + $api_search[5];
						$query = $this->mdl_80008->get_sm_cari_by_kd_pgw_str_id_tgl_akhir_paging($api_search[0], $api_search[1], $api_search[2], $api_search[3], $limit, $api_search[5]);
						break;
					case 5: 
						$query = $this->mdl_80008->get_sm_cari_by_kd_pgw_str_id_periode_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3], $api_search[4], $api_search[5]);
						break;					
					case 6: 
						$limit	= $api_search[5] + $api_search[6];
						$query = $this->mdl_80008->get_sm_cari_by_kd_pgw_str_id_periode_paging($api_search[0], $api_search[1], $api_search[2], $api_search[3], $api_search[4], $limit, $api_search[6]);
						break;
					case 7: 
						$query = $this->mdl_80008->get_sm_cari_by_kd_pgw_str_id_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3]);
						break;					
					case 8: 
						$limit	= $api_search[3] + $api_search[4];
						$query = $this->mdl_80008->get_sm_cari_by_kd_pgw_str_id_paging($api_search[0], $api_search[1], $api_search[2], $limit, $api_search[4]);
						break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function get_distribusi_pejabat_cari($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 80001: switch($subkode){ #TOTAL
				default:
					case 1: $query = $this->mdl_80008->total_distribusi_cari_by_str_id_tgl_awal($api_search[0], $api_search[1], $api_search[2], $api_search[3]);break;
					case 2: $query = $this->mdl_80008->total_distribusi_cari_by_str_id_tgl_akhir($api_search[0], $api_search[1], $api_search[2], $api_search[3]);break;
					case 3: $query = $this->mdl_80008->total_distribusi_cari_by_str_id_periode($api_search[0], $api_search[1], $api_search[2], $api_search[3], $api_search[4]);break;
					case 4: $query = $this->mdl_80008->total_distribusi_cari_by_str_id($api_search[0], $api_search[1], $api_search[2]);break;
			}
			break;
			case 80006: switch($subkode){ #DATA
				default:
					case 1: 
						$query = $this->mdl_80008->get_distribusi_cari_by_str_id_tgl_awal_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3], $api_search[4]);
						break;					
					case 2: 
						$limit	= $api_search[4] + $api_search[5];
						$query = $this->mdl_80008->get_distribusi_cari_by_str_id_tgl_awal_paging($api_search[0], $api_search[1], $api_search[2], $api_search[3], $limit, $api_search[5]);
						break;
					case 3: 
						$query = $this->mdl_80008->get_distribusi_cari_by_str_id_tgl_akhir_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3], $api_search[4]);
						break;					
					case 4: 
						$limit	= $api_search[4] + $api_search[5];
						$query = $this->mdl_80008->get_distribusi_cari_by_str_id_tgl_akhir_paging($api_search[0], $api_search[1], $api_search[2], $api_search[3], $limit, $api_search[5]);
						break;
					case 5: 
						$query = $this->mdl_80008->get_distribusi_cari_by_str_id_periode_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3], $api_search[4], $api_search[5]);
						break;					
					case 6: 
						$limit	= $api_search[5] + $api_search[6];
						$query = $this->mdl_80008->get_distribusi_cari_by_str_id_periode_paging($api_search[0], $api_search[1], $api_search[2], $api_search[3], $api_search[4], $limit, $api_search[6]);
						break;
					case 7: 
						$query = $this->mdl_80008->get_distribusi_cari_by_str_id_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3]);
						break;					
					case 8: 
						$limit	= $api_search[3] + $api_search[4];
						$query = $this->mdl_80008->get_distribusi_cari_by_str_id_paging($api_search[0], $api_search[1], $api_search[2], $limit, $api_search[4]);
						break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function get_distribusi_personal_cari($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 80001: switch($subkode){ #TOTAL
				default:
					case 1: $query = $this->mdl_80008->total_distribusi_cari_by_kd_pgw_tgl_awal($api_search[0], $api_search[1], $api_search[2], $api_search[3]);break;
					case 2: $query = $this->mdl_80008->total_distribusi_cari_by_kd_pgw_tgl_akhir($api_search[0], $api_search[1], $api_search[2], $api_search[3]);break;
					case 3: $query = $this->mdl_80008->total_distribusi_cari_by_kd_pgw_periode($api_search[0], $api_search[1], $api_search[2], $api_search[3], $api_search[4]);break;
					case 4: $query = $this->mdl_80008->total_distribusi_cari_by_kd_pgw($api_search[0], $api_search[1], $api_search[2]);break;
			}
			break;
			case 80006: switch($subkode){ #DATA
				default:
					case 1: 
						$query = $this->mdl_80008->get_distribusi_cari_by_kd_pgw_tgl_awal_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3], $api_search[4]);
						break;					
					case 2: 
						$limit	= $api_search[4] + $api_search[5];
						$query = $this->mdl_80008->get_distribusi_cari_by_kd_pgw_tgl_awal_paging($api_search[0], $api_search[1], $api_search[2], $api_search[3], $limit, $api_search[5]);
						break;
					case 3: 
						$query = $this->mdl_80008->get_distribusi_cari_by_kd_pgw_tgl_akhir_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3], $api_search[4]);
						break;					
					case 4: 
						$limit	= $api_search[4] + $api_search[5];
						$query = $this->mdl_80008->get_distribusi_cari_by_kd_pgw_tgl_akhir_paging($api_search[0], $api_search[1], $api_search[2], $api_search[3], $limit, $api_search[5]);
						break;
					case 5: 
						$query = $this->mdl_80008->get_distribusi_cari_by_kd_pgw_periode_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3], $api_search[4], $api_search[5]);
						break;					
					case 6: 
						$limit	= $api_search[5] + $api_search[6];
						$query = $this->mdl_80008->get_distribusi_cari_by_kd_pgw_periode_paging($api_search[0], $api_search[1], $api_search[2], $api_search[3], $api_search[4], $limit, $api_search[6]);
						break;
					case 7: 
						$query = $this->mdl_80008->get_distribusi_cari_by_kd_pgw_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3]);
						break;					
					case 8: 
						$limit	= $api_search[3] + $api_search[4];
						$query = $this->mdl_80008->get_distribusi_cari_by_kd_pgw_paging($api_search[0], $api_search[1], $api_search[2], $limit, $api_search[4]);
						break;
			}
			break;
			case 1001: switch($subkode){ #TOTAL
				default:
					case 1: $query = $this->mdl_1000->total_distribusi_cari_by_nim_tgl_awal($api_search[0], $api_search[1], $api_search[2], $api_search[3]);break;
					case 2: $query = $this->mdl_1000->total_distribusi_cari_by_nim_tgl_akhir($api_search[0], $api_search[1], $api_search[2], $api_search[3]);break;
					case 3: $query = $this->mdl_1000->total_distribusi_cari_by_nim_periode($api_search[0], $api_search[1], $api_search[2], $api_search[3], $api_search[4]);break;
					case 4: $query = $this->mdl_1000->total_distribusi_cari_by_nim($api_search[0], $api_search[1], $api_search[2]);break;
			}
			break;
			case 1006: switch($subkode){ #DATA
				default:
					case 1: 
						$query = $this->mdl_1000->get_distribusi_cari_by_nim_tgl_awal_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3], $api_search[4]);
						break;					
					case 2: 
						$limit	= $api_search[4] + $api_search[5];
						$query = $this->mdl_1000->get_distribusi_cari_by_nim_tgl_awal_paging($api_search[0], $api_search[1], $api_search[2], $api_search[3], $limit, $api_search[5]);
						break;
					case 3: 
						$query = $this->mdl_1000->get_distribusi_cari_by_nim_tgl_akhir_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3], $api_search[4]);
						break;					
					case 4: 
						$limit	= $api_search[4] + $api_search[5];
						$query = $this->mdl_1000->get_distribusi_cari_by_nim_tgl_akhir_paging($api_search[0], $api_search[1], $api_search[2], $api_search[3], $limit, $api_search[5]);
						break;
					case 5: 
						$query = $this->mdl_1000->get_distribusi_cari_by_nim_periode_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3], $api_search[4], $api_search[5]);
						break;					
					case 6: 
						$limit	= $api_search[5] + $api_search[6];
						$query = $this->mdl_1000->get_distribusi_cari_by_nim_periode_paging($api_search[0], $api_search[1], $api_search[2], $api_search[3], $api_search[4], $limit, $api_search[6]);
						break;
					case 7: 
						$query = $this->mdl_1000->get_distribusi_cari_by_nim_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3]);
						break;					
					case 8: 
						$limit	= $api_search[3] + $api_search[4];
						$query = $this->mdl_1000->get_distribusi_cari_by_nim_paging($api_search[0], $api_search[1], $api_search[2], $limit, $api_search[4]);
						break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function get_distribusi_surat_cari($format = 'json'){
		$query				= '';
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 8000: switch($subkode){ #TOTAL
				default:
					case 1: $query = $this->mdl_8000->total_distribusi_cari_by_par($api_search[0]);break;
					case 2: $query = $this->mdl_8000->get_distribusi_cari_by_par($api_search[0]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	function get_distribusi_pegawai_cari($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 80001: switch($subkode){ #TOTAL
				default:
					case 1: $query = $this->mdl_80008->total_distribusi_cari_by_kd_pgw_str_id_tgl_awal($api_search[0], $api_search[1], $api_search[2], $api_search[3], $api_search[4]);break;
					case 2: $query = $this->mdl_80008->total_distribusi_cari_by_kd_pgw_str_id_tgl_akhir($api_search[0], $api_search[1], $api_search[2], $api_search[3], $api_search[4]);break;
					case 3: $query = $this->mdl_80008->total_distribusi_cari_by_kd_pgw_str_id_periode($api_search[0], $api_search[1], $api_search[2], $api_search[3], $api_search[4], $api_search[5]);break;
					case 4: $query = $this->mdl_80008->total_distribusi_cari_by_kd_pgw_str_id($api_search[0], $api_search[1], $api_search[2], $api_search[3]);break;
			}
			break;
			case 80006: switch($subkode){ #DATA
				default:
					case 1: 
						$query = $this->mdl_80008->get_distribusi_cari_by_kd_pgw_str_id_tgl_awal_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3], $api_search[4], $api_search[5]);
						break;					
					case 2: 
						$limit	= $api_search[5] + $api_search[6];
						$query = $this->mdl_80008->get_distribusi_cari_by_kd_pgw_str_id_tgl_awal_paging($api_search[0], $api_search[1], $api_search[2], $api_search[3], $api_search[4], $limit, $api_search[6]);
						break;
					case 3: 
						$query = $this->mdl_80008->get_distribusi_cari_by_kd_pgw_str_id_tgl_akhir_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3], $api_search[4], $api_search[5]);
						break;					
					case 4: 
						$limit	= $api_search[5] + $api_search[6];
						$query = $this->mdl_80008->get_distribusi_cari_by_kd_pgw_str_id_tgl_akhir_paging($api_search[0], $api_search[1], $api_search[2], $api_search[3], $api_search[4], $limit, $api_search[6]);
						break;
					case 5: 
						$query = $this->mdl_80008->get_distribusi_cari_by_kd_pgw_str_id_periode_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3], $api_search[4], $api_search[5], $api_search[6]);
						break;					
					case 6: 
						$limit	= $api_search[6] + $api_search[7];
						$query = $this->mdl_80008->get_distribusi_cari_by_kd_pgw_str_id_periode_paging($api_search[0], $api_search[1], $api_search[2], $api_search[3], $api_search[4], $api_search[5], $limit, $api_search[7]);
						break;
					case 7: 
						$query = $this->mdl_80008->get_distribusi_cari_by_kd_pgw_str_id_limit($api_search[0], $api_search[1], $api_search[2], $api_search[3], $api_search[4]);
						break;					
					case 8: 
						$limit	= $api_search[4] + $api_search[5];
						$query = $this->mdl_80008->get_distribusi_cari_by_kd_pgw_str_id_paging($api_search[0], $api_search[1], $api_search[2], $api_search[3], $limit, $api_search[5]);
						break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function total_surat_masuk_cari($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 80001: switch($subkode){
				default:
					case 1: $query = $this->mdl_80008->total_surat_masuk_cari_v2($api_search[0], $api_search[1]);break; //by in unit_id, keyword
			}
			break;
			case 80008: switch($subkode){
				default:
					case 1: $query = $this->mdl_80008->total_surat_masuk_cari($api_search[0], $api_search[1]);break; //by in tu_bagian, keyword
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}	
	
	function detail_sm_by_id_surat_masuk($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 8000: switch($subkode){
				default:
					case 1: $query = $this->mdl_8000->detail_sm_by_par($api_search[0]);break;
			}
			break;
			case 80001: switch($subkode){
				default:
					case 1: $query = $this->mdl_80008->detail_sm_by_id_surat_masuk_v2($api_search[0]);break; //by  id_surat_masuk
			}
			break;
			case 80008: switch($subkode){
				default:
					case 1: $query = $this->mdl_80008->detail_sm_by_id_surat_masuk($api_search[0]);break; //by  id_surat_masuk
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function detail_sm_by_no_surat($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 80008: switch($subkode){
				default:
					case 1: $query = $this->mdl_80008->detail_sm_by_no_surat($api_search[0]);break; //by  no_surat
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function get_distribusi_surat($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 8000: switch($subkode){
				default:
					case 1: $query = $this->mdl_8000->get_distribusi_surat_by_par($api_search[0]);break;
					case 2: $query = $this->mdl_8000->detail_distribusi_surat_by_par($api_search[0]);break;
			}
			break;	
			case 80001: switch($subkode){
				default:
					case 3: $query = $this->mdl_80008->get_distribusi_surat_by_id_surat_keluar($api_search[0]);break; //by  id_surat_keluar
			}
			break;				
			case 80002: switch($subkode){
				default:
					case 1: $query = $this->mdl_80008->get_distribusi_surat_by_id_sm_kd_pgw_str_id($api_search[0], $api_search[1], $api_search[2]);break;
			}
			break;			
			case 80008: switch($subkode){
				default:
					case 1: $query = $this->mdl_80008->get_distribusi_surat_by_id_surat_masuk($api_search[0]);break; //by  id_surat_masuk
					case 2: $query = $this->mdl_80008->get_distribusi_surat_by_id_and_status_distribusi($api_search[0], $api_search[1]);break; //by  id_surat_masuk
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}		
	
	function insert_surat_masuk($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 80001: switch($subkode){
				default:
					case 1: $query = $this->mdl_80008->insert_surat_masuk_v2($api_search[0]);break;
			}
			break;
			case 80008: switch($subkode){
				default:
					case 1: $query = $this->mdl_80008->insert_surat_masuk($api_search[0]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function insert_surat_masuk_from_sk($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 80001: switch($subkode){
				default:
					case 1: $query = $this->mdl_80008->insert_surat_masuk_from_sk($api_search[0]);break;
			}
			break;
			case 80002: switch($subkode){
				default:
					case 1: 
					$this->load->library('lib_surat_masuk');
					$query = $this->lib_surat_masuk->insert_surat_masuk_from_sk($api_search[0], $api_search[1], $api_search[2]);
					break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}

	/*function insert_surat_masuk_from_sk2($format = 'json'){
		$this->load->library('lib_surat_masuk');
		$query = $this->lib_surat_masuk->insert_surat_masuk_from_sk('551caec94748e', 'PTIPD', 'B');
		echo '<pre>';
		print_r($query);
		echo '</pre>';
	}*/
	
	function insert_surat_masuk_from_sp($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 80001: switch($subkode){
				default:
					case 1: $query = $this->mdl_80008->insert_surat_masuk_from_sp($api_search[0]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}	
	
	function update_surat_masuk_from_sk($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 80001: switch($subkode){
				default:
					case 1: $query = $this->mdl_80008->update_surat_masuk_from_sk($api_search[0], $api_search[1]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function update_surat_masuk($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 80001: switch($subkode){
				default:
					case 1: $query = $this->mdl_80008->update_surat_masuk_v2($api_search[0], $api_search[1]); break; // id_surat_masuk, data
			}
			break;
			case 80008: switch($subkode){
				default:
					case 1: $query = $this->mdl_80008->update_surat_masuk($api_search[0], $api_search[1]); break; // id_surat_masuk, data
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function insert_dokumen($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 80008: switch($subkode){
				default:
					case 1: $query = $this->mdl_80008->insert_dokumen($api_search[0]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}	
	
	function insert_distribusi_surat($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 80001: switch($subkode){
				default:
					case 1: $query = $this->mdl_80008->insert_distribusi_surat_from_sk($api_search[0]);break;
					case 2: $query = $this->mdl_80008->insert_distribusi_surat_v2($api_search[0]);break;
			}
			break;
			case 80002: switch($subkode){
				default:
					case 1:
						$waktu_penyelesaian	= (!empty($api_search[0]['WAKTU_PENELESAIAN']) ? $api_search[0]['WAKTU_PENELESAIAN'] : '');
						$isi_ringkas					= (!empty($api_search[0]['ISI_RINGKAS']) ? $api_search[0]['ISI_RINGKAS'] : '');
						$isi_disposisi				= (!empty($api_search[0]['ISI_DISPOSISI']) ? $api_search[0]['ISI_DISPOSISI'] : '');
						$kd_grup_pengirim		= (!empty($api_search[0]['KD_GRUP_PENGIRIM']) ? $api_search[0]['KD_GRUP_PENGIRIM'] : '');
						$id_ds_sebelumnya		= (!empty($api_search[0]['ID_DS_SEBELUMNYA']) ? $api_search[0]['ID_DS_SEBELUMNYA'] : '');
						$data = array(
							array('method' => 'IN', 'name' => ':ID_SURAT_MASUK', 'value' => $api_search[0]['ID_SURAT_MASUK'], 'length' => 15, 'type' => SQLT_CHR),
							array('method' => 'IN', 'name' => ':ID_GRUP_SURAT', 'value' => $api_search[0]['ID_GRUP_SURAT'], 'length' => 8, 'type' => SQLT_CHR),
							array('method' => 'IN', 'name' => ':PEMROSES_SURAT', 'value' => $api_search[0]['PEMROSES_SURAT'], 'length' => 25, 'type' => SQLT_CHR),
							array('method' => 'IN', 'name' => ':JABATAN_PEMROSES', 'value' => $api_search[0]['JABATAN_PEMROSES'], 'length' => 10, 'type' => SQLT_CHR),
							array('method' => 'IN', 'name' => ':PENERIMA_SURAT', 'value' => $api_search[0]['PENERIMA_SURAT'], 'length' => 200, 'type' => SQLT_CHR),
							array('method' => 'IN', 'name' => ':JABATAN_PENERIMA', 'value' => $api_search[0]['JABATAN_PENERIMA'], 'length' => 10, 'type' => SQLT_CHR),
							array('method' => 'IN', 'name' => ':KD_STATUS_DISTRIBUSI', 'value' => $api_search[0]['KD_STATUS_DISTRIBUSI'], 'length' => 6, 'type' => SQLT_CHR),
							array('method' => 'IN', 'name' => ':WAKTU_KIRIM', 'value' => $api_search[0]['WAKTU_KIRIM'], 'length' => 32, 'type' => SQLT_CHR),
							array('method' => 'IN', 'name' => ':ID_STATUS_SM', 'value' => $api_search[0]['ID_STATUS_SM'], 'length' => 4, 'type' => SQLT_CHR),
							array('method' => 'IN', 'name' => ':KD_GRUP_TUJUAN', 'value' => $api_search[0]['KD_GRUP_TUJUAN'], 'length' => 8, 'type' => SQLT_CHR),
							array('method' => 'IN', 'name' => ':WAKTU_PENELESAIAN', 'value' => $waktu_penyelesaian, 'length' => 32, 'type' => SQLT_CHR),
							array('method' => 'IN', 'name' => ':ISI_RINGKAS', 'value' => $isi_ringkas, 'length' => 300, 'type' => SQLT_CHR),
							array('method' => 'IN', 'name' => ':ISI_DISPOSISI', 'value' => $isi_disposisi, 'length' => 300, 'type' => SQLT_CHR),
							array('method' => 'IN', 'name' => ':ID_DS_SEBELUMNYA', 'value' => $id_ds_sebelumnya, 'length' => 6, 'type' => SQLT_CHR),
							array('method' => 'IN', 'name' => ':KD_GRUP_PENGIRIM', 'value' => $api_search[0]['KD_GRUP_PENGIRIM'], 'length' => 8, 'type' => SQLT_CHR),
							array('method' => 'OUT', 'name' => ':hasil', 'value' => '', 'length' => 32, 'type' => SQLT_CHR)
						);
						$query = $this->mdl_80008->insert_procedure_distribusi_surat($data);
						break;
			}
			break;
			case 80008: switch($subkode){
				default:
					case 1: $query = $this->mdl_80008->insert_distribusi_surat($api_search[0]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}	
	
	function total_pencarian_surat_masuk($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 80001: switch($subkode){
				default:
					case 1: $query = $this->mdl_80008->total_pencarian_surat_masuk_v2($api_search[0], $api_search[1]);break;
					case 2: $query = $this->mdl_80008->total_pencarian_surat_masuk_all_v2($api_search[0], $api_search[1]);break;
			}
			break;
			case 80008: switch($subkode){
				default:
					case 1: $query = $this->mdl_80008->total_pencarian_surat_masuk($api_search[0], $api_search[1]);break;
					case 2: $query = $this->mdl_80008->total_pencarian_surat_masuk_all($api_search[0], $api_search[1]);break;
					case 3: $query = $this->mdl_80008->total_pencarian_surat_masuk_public($api_search[0], $api_search[1]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}	
	
	function get_pencarian_surat_masuk($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 80001: switch($subkode){
				default:
					case 1: $query = $this->mdl_80008->get_pencarian_surat_masuk_limit_v2($api_search[0], $api_search[1], $api_search[2]);break;
					case 2: $limit = $api_search[2] + $api_search[3]; $query = $this->mdl_80008->get_pencarian_surat_masuk_paging_v2($api_search[0], $api_search[1], $limit, $api_search[3]);break;
			}
			break;
			case 80008: switch($subkode){
				default:
					case 1: $query = $this->mdl_80008->get_pencarian_surat_masuk_limit($api_search[0], $api_search[1], $api_search[2]);break;
					case 2: $limit = $api_search[2] + $api_search[3]; $query = $this->mdl_80008->get_pencarian_surat_masuk_paging($api_search[0], $api_search[1], $limit, $api_search[3]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}	
	
	function get_pencarian_surat_masuk_all($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 80001: switch($subkode){
				default:
					case 1: $query = $this->mdl_80008->get_pencarian_surat_masuk_all_limit_v2($api_search[0], $api_search[1], $api_search[2]);break;
					case 2: $limit = $api_search[2] + $api_search[3]; $query = $this->mdl_80008->get_pencarian_surat_masuk_all_paging_v2($api_search[0], $api_search[1], $limit, $api_search[3]);break;
			}
			break;
			case 80008: switch($subkode){
				default:
					case 1: $query = $this->mdl_80008->get_pencarian_surat_masuk_all_limit($api_search[0], $api_search[1], $api_search[2]);break;
					case 2: $limit = $api_search[2] + $api_search[3]; $query = $this->mdl_80008->get_pencarian_surat_masuk_all_paging($api_search[0], $api_search[1], $limit, $api_search[3]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function pencarian_surat_masuk_public($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 80008: switch($subkode){
				default:
					case 1: $query = $this->mdl_80008->pencarian_surat_masuk_public_limit($api_search[0], $api_search[1], $api_search[2]);break;
					case 2: $limit = $api_search[2] + $api_search[3]; $query = $this->mdl_80008->pencarian_surat_masuk_public_paging($api_search[0], $api_search[1], $limit, $api_search[3]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function get_no_kendali($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 80001: switch($subkode){
				default:
					case 2: $query = $this->mdl_80008->get_no_kendali2_v2($api_search[0], $api_search[1], $api_search[2]);break;
			}
			break;
			case 80008: switch($subkode){
				default:
					case 1: $query = $this->mdl_80008->get_no_kendali($api_search[0], $api_search[1], $api_search[2]);break;
					case 2: $query = $this->mdl_80008->get_no_kendali2($api_search[0], $api_search[1], $api_search[2]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function detail_ts_by_id_distribusi_surat($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 80008: switch($subkode){
				default:
					case 1: $query = $this->mdl_80008->detail_ts_by_id_distribusi_surat($api_search[0]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query,$format);	
	}	
	
	function list_laporan_surat_masuk($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 80008: switch($subkode){
				default:
					case 1: $query = $this->mdl_80008->list_bulan_surat_masuk($api_search[0], $api_search[1]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query,$format);	
	}
	
	function detail_dk_by_id_ds_sebelumnya($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 80001: switch($subkode){
				default:
					case 1: $query = $this->mdl_80008->detail_dk_by_id_ds_sebelumnya($api_search[0]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}	
	
	function simpan_log($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 80001: switch($subkode){
				default:
					case 1: $query = $this->mdl_80008->simpan_log_md_surat_masuk($api_search[0]);break;
					case 2: $query = $this->mdl_80008->update_log_md_surat_masuk($api_search[0], $api_search[1]);break;
			}
			break;			
			case 80002: switch($subkode){
				default:
					case 1: $query = $this->mdl_80008->simpan_log_d_distribusi_surat($api_search[0]);break;
					case 2: $query = $this->mdl_80008->update_log_d_distribusi_surat($api_search[0], $api_search[1]);break;
			}
			break;
			case 80006: switch($subkode){
				default:
					case 1: $query = $this->mdl_80008->update_status_distribusi($api_search[0], $api_search[1]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function get_distribusi_surat_by_id_ds_sebelumnya($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 80001: switch($subkode){
				default:
					case 1: $query = $this->mdl_80008->get_distribusi_surat_by_id_ds_sebelumnya($api_search[0]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}	
	
	function insert_lampiran($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 80001: switch($subkode){
				default:
					case 1: $query = $this->mdl_80008->insert_lampiran($api_search[0]);break;
			}
			break;
			case 80002: switch($subkode){
				default:
					case 1: $query = $this->mdl_80008->insert_lampiran_blob($api_search[0]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function distribusi_surat_by_no_surat($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 80001: switch($subkode){ #DETAIL SURAT
				default:
					case 1: $query = $this->mdl_80008->distribusi_surat_by_no_surat_status($api_search[0], $api_search[1]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function no_agenda($format = 'json'){
		$kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 80001: switch($subkode){ #DETAIL SURAT
				default:
					case 1: $query = $this->mdl_80008->no_agenda($api_search[0], $api_search[1]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}	
	
	function detail_sm_by_id_distribusi_surat($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 8000: switch($subkode){
				default:
					case 1: $query = $this->mdl_8000->detail_sm_by_id_distribusi_surat($api_search[0]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query,$format);
	}
	
	function meta_distribusi_surat($format = 'json'){
		$kode			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 8000: switch($subkode){
				default:
					case 1: $query = $this->mdl_8000->histori_pesan_by_str_id($api_search[0], $api_search[1]);break;
					case 2: $query = $this->mdl_8000->distribusi_surat_by_id_meta_distribusi($api_search[0]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	// function get_distribusi_mhs($format = 'json'){
		// $kode				= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		// $subkode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		// $api_search	= $this->input->post('api_search');
		// switch($kode){
			// case 80001: switch($subkode){ #DETAIL SURAT
				// default:
					// case 1: $query = $this->mdl_1000->get_distribusi_mhs($api_search[0], $api_search[1], $api_search[2]);break;
			// }
			// break;
		// }
		// $this->sia_api_lib_format->output($query, $format);
	// }
}

?>