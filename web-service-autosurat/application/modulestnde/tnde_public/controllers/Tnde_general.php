<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');
class Tnde_general extends MX_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->model('tnde_general/mdl_tnde_general', 'mdl_1001');
	}
	
	function index(){
		echo "^^";
	}
	
	function jajal(){
		$this->sia_api_lib_format->output('aseeeh', 'json');
	}
	
	function get_data($format = 'json'){
		$kode = (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode = (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search = $this->input->post('api_search');
		switch($kode){
			case 1001: switch($subkode){
				default:
					case 1: $query = $this->mdl_1001->get_data($api_search[0]); break;
					case 2: $query = $this->mdl_1001->get_data_by_field($api_search[0],$api_search[1],$api_search[2]); break; #output row_array
					case 3: $query = $this->mdl_1001->get_data_by_field2($api_search[0],$api_search[1],$api_search[2]); break; #output result
					case 4: $query = $this->mdl_1001->get_data_by_field3($api_search[0],$api_search[1]); break;#output row_array
					case 5: $query = $this->mdl_1001->get_data_by_field_like($api_search[0],$api_search[1], $api_search[2]); break;
					case 6: $query = $this->mdl_1001->get_data_by_field_in($api_search[0],$api_search[1], $api_search[2]); break;
					case 7: $query = $this->mdl_1001->get_data_by_field_not_in($api_search[0],$api_search[1], $api_search[2]); break;
			}
			break;
			case 1002:
				default:
					case 1:
						$order	= (!empty($api_search[2]) ? $api_search[2] : '');
						$query = $this->mdl_1001->get_data_by_field_array($api_search[0], $api_search[1], $order); break;
				break;
			case 1003:
				default:
					case 1:
						$table		= (!empty($api_search[0]) ? $api_search[0] : '');
						$where	= (!empty($api_search[1]) ? $api_search[1] : '');
						$order		= (!empty($api_search[2]) ? $api_search[2] : '');
						$query 	= $this->mdl_1001->get_data_by_par($table, $where, $order); break;
				break;
		}
		// $this->sia_api_lib_format->output($query, $format);
		$this->output
	        ->set_content_type('application/json')
	        ->set_output(json_encode($query));
	}
	
	function insert_data($format = 'json'){
		$kode = (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode = (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search = $this->input->post('api_search');
		switch($kode){
			case 1001: switch($subkode){
				default:
					case 1: $query = $this->mdl_1001->insert_data($api_search[0], $api_search[1]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);	
	}
	
	function update_data($format = 'json'){
		$kode = (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode = (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search = $this->input->post('api_search');
		switch($kode){
			case 1001: switch($subkode){
				default:
					case 1: $query = $this->mdl_1001->update_data($api_search[0], $api_search[1], $api_search[2]); break; //table, array_data, array_where
			}
			break;
		}
		// $this->sia_api_lib_format->output($query, $format);	
		$this->output
	        ->set_content_type('application/json')
	        ->set_output(json_encode($query));
	
	}
	
	function delete_data($format = 'json'){
		$kode = (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode = (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search = $this->input->post('api_search');
		switch($kode){
			case 1001: switch($subkode){
				default:
					case 1: $query = $this->mdl_1001->delete_data($api_search[0], $api_search[1]); break;
					case 2: $query = $this->mdl_1001->delete_data_in($api_search[0], $api_search[1], $api_search[2]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);	
	}	
	
	function order_data($format = 'json'){
		$kode = (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode = (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search = $this->input->post('api_search');
		switch($kode){
			case 1001: switch($subkode){
				default:
					case 1: $query = $this->mdl_1001->order_data($api_search[0], $api_search[1], $api_search[2]); break;
					case 2: $query = $this->mdl_1001->order_data_result($api_search[0], $api_search[1], $api_search[2]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);	
	}
	
	// function order_data($format = 'json'){
		// $kode	= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		// $subkode = (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		// $api_search = $this->input->post('api_search');
		// switch($kode){
			// case 1001: switch($subkode){
				// default:
					// case 1: $query = $this->mdl_1001->order_data($api_search[0], $api_search[1], $api_search[2]); break;
			// }
			// break;
		// }
		// $query = 1;
		// $this->sia_api_lib_format->output($query, $format);
	// }
	
	function auth($format = 'json'){
		
		$kode = (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode = (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search = $this->input->post('api_search');
		switch($kode){
			case 1001: switch($subkode){
				default:
					case 1: $query = $this->mdl_1001->pegawai_by_nip($api_search[0]);
					case 2: $query = $this->mdl_1001->pegawai_by_kd_pegawai($api_search[0]);
				break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function user_by_nip($format='json'){
		$kode = (int)preg_replace("/[^0-9]/","",$_POST['api_kode']);
		$subkode = (int)preg_replace("/[^0-9]/","",$_POST['api_subkode']);
		$api_search = $this->input->post('api_search');
		switch($kode){
			case 1001: switch($subkode){
				default:
					case 1: $query = $this->mdl_1001->user_by_nip($api_search[0]);
				break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function user_by_kd_pegawai($format='json'){
		$kode = (int)preg_replace("/[^0-9]/","",$_POST['api_kode']);
		$subkode = (int)preg_replace("/[^0-9]/","",$_POST['api_subkode']);
		$api_search = $this->input->post('api_search');
		switch($kode){
			case 1001: switch($subkode){
				default:
					case 1: $query = $this->mdl_1001->user_by_kd_pegawai($api_search[0]);
				break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function count_data($format = 'json'){
		$kode = (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode = (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search = $this->input->post('api_search');
		switch($kode){
			case 1001: switch($subkode){
				default:
					case 1: $query = $this->mdl_1001->count_data($api_search[0]); break;
					case 2: $query = $this->mdl_1001->count_data_by_field($api_search[0],$api_search[1],$api_search[2]); break;
					case 3: $query = $this->mdl_1001->count_by_field_in($api_search[0], $api_search[1], $api_search[2]); break;
			}
			break;
		}
		// $this->sia_api_lib_format->output($query, $format);
		$this->output
	        ->set_content_type('application/json')
	        ->set_output(json_encode($query));
	}
	
	function delete_by_field_id($format = 'json'){
		$kode = (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode = (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search = $this->input->post('api_search');
		switch($kode){
			case 1001: switch($subkode){
				default:
					case 1: $query = $this->mdl_1001->delete_by_field_id($api_search[0],$api_search[1],$api_search[2]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);	
	}
	
	function notif_dm_by_nip($format = 'json'){
		$kode = (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode = (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search = $this->input->post('api_search');
		switch($kode){
			case 1001: switch($subkode){
				default:
					case 1: $query = $this->mdl_1001->notif_dm_by_nip($api_search[0]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function notif_dm_by_kd_pegawai($format = 'json'){
		$kode = (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode = (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search = $this->input->post('api_search');
		switch($kode){
			case 1001: switch($subkode){
				default:
					case 1: $query = $this->mdl_1001->notif_dm_by_kd_pegawai($api_search[0]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function notif_dm_by_kd_jabatan($format = 'json'){
		$kode = (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode = (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search = $this->input->post('api_search');
		switch($kode){
			case 1001: switch($subkode){
				default:
					case 1: $query = $this->mdl_1001->notif_dm_by_kd_jabatan($api_search[0]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);	
	}
	
	function notif_sm_by_kd_tu_bagian($format = 'json'){
		$kode = (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode = (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search = $this->input->post('api_search');
		switch($kode){
			case 1001: switch($subkode){
				default:
					case 1: $query = $this->mdl_1001->notif_sm_by_kd_tu_bagian($api_search[0]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);		
	}
	
	function notif_sm_by_kd_jabatan($format = 'json'){
		$kode = (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode = (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search = $this->input->post('api_search');
		switch($kode){
			case 1001: switch($subkode){
				default:
					case 1: $query = $this->mdl_1001->notif_sm_by_kd_jabatan($api_search[0]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);	
	}
	
	function notif_ts_by_kd_jabatan($format = 'json'){
		$kode = (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode = (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search = $this->input->post('api_search');
		switch($kode){
			case 1001: switch($subkode){
				default:
					case 1: $query = $this->mdl_1001->notif_ts_by_kd_jabatan($api_search[0]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);		
	}
	
	function notif_tm_by_kd_pegawai($format = 'json'){
		$kode = (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode = (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search = $this->input->post('api_search');
		switch($kode){
			case 1001: switch($subkode){
				default:
					case 1: $query = $this->mdl_1001->notif_tm_by_kd_pegawai($api_search[0]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);		
	}
	
	function notif_tm_by_kd_jabatan($format = 'json'){
		$kode = (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode = (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search = $this->input->post('api_search');
		switch($kode){
			case 1001: switch($subkode){
				default:
					case 1: $query = $this->mdl_1001->notif_tm_by_kd_jabatan($api_search[0]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);		
	}	
	
	function notif_surat_terkait($format = 'json'){
		$kode = (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode = (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search = $this->input->post('api_search');
		switch($kode){
			case 1001: switch($subkode){
				default:
					case 1: $query = $this->mdl_1001->notif_surat_terkait($api_search[0], $api_search[1]); break; //kd_pegawai, kd_jabatan
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);		
	}
	
	function act_query($format = 'json'){
		$kode = (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode = (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search = $this->input->post('api_search');
		switch($kode){
			case 1001: switch($subkode){
				default:
					case 1: $query = $this->mdl_1001->act_query($api_search[0]); break;
					case 2: $query = $this->mdl_1001->act_query2($api_search[0]); break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);		
	}
	
	function get_md_jabatan($format = 'json'){
		$kode 			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode 		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search 	= $this->input->post('api_search');		
		switch($kode){
			case 1001: switch($subkode){
				default: 
					case 1: $query = $this->mdl_1001->get_md_jabatan_by_nm_jabatan($api_search[0]);break;
			}
			break;		
		}
		$this->sia_api_lib_format->output($query, $format);
	}	
	
	function update_status_sm_by_id_surat_masuk($format = 'json'){
		$kode 			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode 		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search 	= $this->input->post('api_search');		
		switch($kode){
			case 1001: switch($subkode){
				default: 
					case 1: $query = $this->mdl_1001->update_status_sm_by_id_surat_masuk($api_search[0], $api_search[1]);break;
			}
			break;		
		}
		$this->sia_api_lib_format->output($query, $format);
	}	
	
	function notifications($format = 'json'){
		$kode 			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode 		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search 	= $this->input->post('api_search');		
		switch($kode){
			case 1001: switch($subkode){
				default: 
					case 1: $query = $this->mdl_1001->notif_pengarah_sm($api_search[0]);break;
			}
			break;	
			case 1002: switch($subkode){
				default: 
					case 1: $query = $this->mdl_1001->notif_pengarah_sm_v2($api_search[0]);break;
			}
			break;				
		}
		$this->sia_api_lib_format->output($query, $format);
	}	
	
	function get_config_surat($format = 'json'){
		$kode 			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode 		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search 	= $this->input->post('api_search');		
		switch($kode){
			case 1001: switch($subkode){
				default: 
					case 1: $query = $this->mdl_1001->get_config_surat();break;
					case 2: $query = $this->mdl_1001->detail_config_surat_by_kd_kat_penomoran($api_search[0]);break;
			}
			break;		
		}
		$this->sia_api_lib_format->output($query, $format);
	}	
	
	function insert_blob($format = 'json'){
		$kode 			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode 		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search 	= $this->input->post('api_search');		
		switch($kode){
			case 1001: switch($subkode){
				default: 
					case 1: $query = $this->mdl_1001->insert_blob($api_search[0], $api_search[1], $api_search[2]);break;
			}
			break;		
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function get_blob($format = 'json'){
		$kode 			= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode 		= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search 	= $this->input->post('api_search');		
		switch($kode){
			case 1001: switch($subkode){
				default: 
					case 1: $query = $this->mdl_1001->get_blob($api_search[0], $api_search[1], $api_search[2]);break;
			}
			break;		
		}
		$this->sia_api_lib_format->output($query, $format);
	}
}
?>