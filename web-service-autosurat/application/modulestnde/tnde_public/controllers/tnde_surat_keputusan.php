<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');
class Tnde_surat_keputusan extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('tnde_surat_keputusan/mdl_tnde_surat_keputusan', 'mdl_60006');
	}
	
	function index(){
		echo ":|";
	}
	
	function total_surat_keputusan($format = 'json'){
		$kode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode	= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 60001: switch($subkode){
				default:
					case 1: $query = $this->mdl_60006->tot_surat_keputusan_in_v2($api_search[0], $api_search[1]);break;
					case 2: $query = $this->mdl_60006->tot_surat_keputusan_in_q_v2($api_search[0], $api_search[1], $api_search[2]);break;
			}
			break;
			case 60006: switch($subkode){
				default:
					case 1: $query = $this->mdl_60006->tot_surat_keputusan_in($api_search[0]);break;
					case 2: $query = $this->mdl_60006->tot_surat_keputusan_in_q($api_search[0], $api_search[1]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function get_surat_keputusan($format = 'json'){
		$kode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode	= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 60001: switch($subkode){
				default:
					case 1: $query = $this->mdl_60006->get_surat_keputusan_in_limit_v2($api_search[0], $api_search[1], $api_search[2]);break;
					case 2: $limit = $api_search[2] + $api_search[3]; $query = $this->mdl_60006->get_surat_keputusan_in_paging_v2($api_search[0], $api_search[1], $limit, $api_search[3]);break;
			}
			break;			
			case 60006: switch($subkode){
				default:
					case 1: $query = $this->mdl_60006->get_surat_keputusan_in_limit($api_search[0], $api_search[1]);break;
					case 2: $limit = $api_search[1] + $api_search[2]; $query = $this->mdl_60006->get_surat_keputusan_in_paging($api_search[0], $limit, $api_search[2]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function get_surat_keputusan_cari($format = 'json'){
		$kode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode	= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 60001: switch($subkode){
				default:
					case 1: $query = $this->mdl_60006->get_surat_keputusan_in_q_limit_v2($api_search[0], $api_search[1], $api_search[2], $api_search[3]);break;
					case 2: $limit = $api_search[3] + $api_search[4]; $query = $this->mdl_60006->get_surat_keputusan_in_q_paging_v2($api_search[0], $api_search[1], $api_search[2], $limit, $api_search[4]);break;
			}
			break;			
			case 60006: switch($subkode){
				default:
					case 1: $query = $this->mdl_60006->get_surat_keputusan_in_q_limit($api_search[0], $api_search[1], $api_search[2]);break;
					case 2: $limit = $api_search[2] + $api_search[3]; $query = $this->mdl_60006->get_surat_keputusan_in_q_paging($api_search[0], $api_search[1], $limit, $api_search[3]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function detail_surat_keputusan($format = 'json'){
		$kode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode	= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 60006: switch($subkode){
				default:
					case 1: $query = $this->mdl_60006->detail_surat_keputusan($api_search[0]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function update_surat_keputusan($format = 'json'){
		$kode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode	= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 60006: switch($subkode){
				default:
					case 1: $query = $this->mdl_60006->update_surat_keputusan($api_search[0], $api_search[1]);break;
			}
			break;
			case 60003: switch($subkode){
				default:
					case 1: $query = $this->mdl_60006->update_simple_surat_keputusan($api_search[0], $api_search[1]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function get_detail_pertimbangan($format = 'json'){
		$kode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode	= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 60006: switch($subkode){
				default:
					case 1: $query = $this->mdl_60006->get_detail_pertimbangan($api_search[0], $api_search[1]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function get_anggaran($format = 'json'){
		$kode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode	= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 60006: switch($subkode){
				default:
					case 1: $query = $this->mdl_60006->get_anggaran_like($api_search[0]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}	
	
	function get_d_sk_anggaran($format = 'json'){
		$kode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode	= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 60006: switch($subkode){
				default:
					case 1: $query = $this->mdl_60006->get_d_sk_anggaran($api_search[0]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}
	
	function get_data_lampiran($format = 'json'){
		$kode		= (int)preg_replace("/[^0-9]/", "", $_POST['api_kode']);
		$subkode	= (int)preg_replace("/[^0-9]/", "", $_POST['api_subkode']);
		$api_search	= $this->input->post('api_search');
		switch($kode){
			case 60006: switch($subkode){
				default:
					case 1: $query = $this->mdl_60006->get_data_lampiran($api_search[0]);break;
			}
			break;
		}
		$this->sia_api_lib_format->output($query, $format);
	}	
	
}
?>