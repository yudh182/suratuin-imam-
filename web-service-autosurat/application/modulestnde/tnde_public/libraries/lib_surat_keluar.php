<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');
Class Lib_surat_keluar extends CI_Controller{
	function __construct(){
	}
	
	function cek(){
		$ci =& get_instance();
		$ci->load->model('tnde_general/mdl_tnde_general');
		$cek = $ci->mdl_tnde_general->get_data('MD_STATUS_SIMPAN');
		return $cek;
	}
	
	function get_no_surat($par=''){
		$hasil = array();
		$ci =& get_instance();
		$ci->load->model('tnde_general/mdl_tnde_general');
		$ci->load->model('tnde_config/mdl_tnde_config');
		$ci->load->library('lib_general');

		$unit_id						= (!empty($par['UNIT_ID']) ? $par['UNIT_ID'] : '');
		$kd_status_simpan	= (!empty($par['KD_STATUS_SIMPAN']) ? $par['KD_STATUS_SIMPAN'] : '');
		$id_psd						= (!empty($par['ID_PSD']) ? $par['ID_PSD'] : '');
		$id_klasifikasi_surat	= (!empty($par['ID_KLASIFIKASI_SURAT']) ? $par['ID_KLASIFIKASI_SURAT'] : '');
		$kd_jenis_surat			= (!empty($par['KD_JENIS_SURAT']) ? $par['KD_JENIS_SURAT'] : '');
		$tgl_surat					= (!empty($par['TGL_SURAT']) ? $par['TGL_SURAT'] : '');
		
		
		$uinsk 						= $ci->mdl_tnde_general->get_data_by_field('CONFIG', 'NM_CONFIG', 'KD_UINSK');
		$kd_uinsk 					= $uinsk['DESKRIPSI'];
		
		$psd 							= $ci->mdl_tnde_general->get_data_by_field('MD_PSD', 'ID_PSD', $id_psd);
		$kd_psd 						= (!empty($psd['KD_PSD']) ? $psd['KD_PSD'] : '');
		
		$klasifikasi_surat 		= $ci->mdl_tnde_general->get_data_by_field('MD_KLASIFIKASI_SURAT', 'ID_KLASIFIKASI_SURAT', $id_klasifikasi_surat);
		$kd_klasifikasi_surat 	= (!empty($klasifikasi_surat['KD_KLASIFIKASI_SURAT']) ? $klasifikasi_surat['KD_KLASIFIKASI_SURAT'] : '');
		
		$jns_surat					=	$ci->mdl_tnde_general->get_data_by_field('MD_JENIS_SURAT', 'KD_JENIS_SURAT', $kd_jenis_surat);
		$kd_kat_penomoran 	= (!empty($jns_surat['KD_KAT_PENOMORAN']) ? $jns_surat['KD_KAT_PENOMORAN'] : '');
		
		$config_penomoran	= $ci->mdl_tnde_config->config_penomoran_by_unit_id($kd_kat_penomoran, $unit_id);
		$buat_nomor	=	FALSE;
		if(empty($config_penomoran)){
			$save_sk	= FALSE;
			$buat_nomor	=	FALSE;
			$hasil['ERRORS'][]	= "Data unit tidak ditemukan. Silahkan hubungi PTIPD.";
		}			
		if(empty($config_penomoran['STATUS_PENOMORAN'])){
			$config_penomoran['STATUS_PENOMORAN'] = 0;
		}
		if(empty($hasil['ERRORS'])){
			if($config_penomoran['STATUS_PENOMORAN'] == 1){
				$tgl_default	=	date_to_number(tgl($config_penomoran['TGL_MULAI']));
				$tgl_surat_in	=	date_to_number($tgl_surat);
				if($tgl_surat_in >= $tgl_default){
					if($kd_status_simpan == 2){
						if($kd_kat_penomoran == 1){
							$hari_libur	= $ci->lib_general->tgl_libur_pns_by_tgl($tgl_surat);
							if(!empty($hari_libur)){
								$buat_nomor		=	FALSE;
								$hasil['NO_URUT'] 		= '';
								$hasil['NO_SELA'] 			= '';
								$hasil['NO_SURAT'] 		= '';
								$hasil['ERRORS'][] 	= "Surat Keputusan tidak dapat dibuat dihari libur. ".$hari_libur[0];
							}else{
								$no_sk	= $this->no_surat_keputusan(tahun($tgl_surat), $unit_id, $kd_jenis_surat, $tgl_surat);
								$hasil['NO_URUT']	= (!empty($no_sk['NO_URUT']) ? $no_sk['NO_URUT'] : 0);
								$hasil['NO_SELA']	= (!empty($no_sk['NO_SELA']) ? $no_sk['NO_SELA'] : 0);
								$no_urut					= (!empty($no_sk['NOMOR']) ? $no_sk['NOMOR'] : 0);
								$buat_nomor		=	TRUE;
							}
						}else{
							if($tgl_surat == date('d/m/Y')){
								$no 					= $this->no_surat_terakhir(date('Y'), $unit_id, $kd_jenis_surat);
								$no_db			= (!empty($no['NO_URUT']) ? $no['NO_URUT'] : 0);
								$cek_setting	= $ci->mdl_tnde_config->config_penomoran_by_unit_id2($tgl_surat, $unit_id, $kd_kat_penomoran);
								$no_setting 	= (!empty($cek_setting['NO_SURAT']) ? $cek_setting['NO_SURAT'] : 0);
								$no_terakhir	= max($no_db, $no_setting);
								
								$hasil['NO_URUT'] 	= $no_terakhir + 1;
								$hasil['NO_SELA'] 		= "0";
								$no_urut				= ($hasil['NO_URUT'] > 9 ? $hasil['NO_URUT'] : "0".$hasil['NO_URUT']);
							}else{
								$no 		= $this->no_surat_mundur(tahun2($tgl_surat), $unit_id, $kd_jenis_surat, $tgl_surat);
			
								if(empty($no)){
									$no['NO_URUT'] 	= $config_penomoran['NO_SURAT'];
									$no['NO_SELA'] 		= 0;
								}
								$hasil['NO_URUT'] 	= $no['NO_URUT'];
								$hasil['NO_SELA'] 		= $no['NO_SELA'] + 1;
								$no_urut						= ($hasil['NO_URUT'] > 9 ? $hasil['NO_URUT'] : "0".$hasil['NO_URUT']).".".$hasil['NO_SELA'];
							}
							$buat_nomor	=	TRUE;
						}

					}else{
						$buat_nomor	=	FALSE;
						$hasil['NO_URUT']		= '';
						$hasil['NO_SELA']		= '';
						$hasil['NO_SURAT']	= '';
					}
					$save_sk = TRUE;
				}else{
					$save_sk = FALSE;
					$hasil['ERRORS'][]	=	"Surat tidak boleh kurang dari tanggal ".tgl_bulan_indo($config_penomoran['TGL_MULAI']);
				}
			}else{
				$no_urut						= (!empty($par['NO_URUT']) ? $par['NO_URUT'] : '');
				$no_sela						= (!empty($par['NO_SELA']) ? $par['NO_SELA'] : '');
				$buat_nomor	=	TRUE;
				$save_sk = TRUE;
				$hasil['NO_URUT']		= $no_urut;
				$hasil['NO_SELA']		= $no_sela;
				if($hasil['NO_SELA'] == 0){ $hasil['NO_SELA'] = '';}
				if(empty($hasil['NO_SELA'])){
					$no_urut	=	"0".$hasil['NO_URUT'];
				}else{
					$no_urut	=	"0".$hasil['NO_URUT'].".".$hasil['NO_SELA'];
				}
			}
		}
		
		if($buat_nomor == TRUE){
			$kat_penomoran	=	$ci->mdl_tnde_general->get_data_by_field('MD_JENIS_SURAT', 'KD_JENIS_SURAT', $kd_jenis_surat);
			switch($kat_penomoran['KD_KAT_PENOMORAN']){
				case 0:
					$hasil['NO_SURAT']	= "";
					break;
				case 1:
					if($kd_jenis_surat == 1){
						$pau 		= $this->status_penomoran_pau($unit_id);	
						if($pau == TRUE){
							$hasil['NO_SURAT'] 	= $no_urut." TAHUN ".tahun2($tgl_surat);
						}else{
							$hasil['NO_SURAT'] 	= $no_urut."/".$kd_psd."/".tahun2($tgl_surat);
						}
					}else{
						$hasil['NO_SURAT'] 	= $no_urut." TAHUN ".tahun2($tgl_surat);
					}
					break;
				case 2:
					$hasil['NO_SURAT'] 	= $kd_uinsk."/".$kd_psd."/".$kd_klasifikasi_surat."/".$no_urut."/".tahun2($tgl_surat);
					break;
				case 3:
					if($kd_jenis_surat == 19){
						#$pgw = explode("#", $kpd_pegawai[0]);
						$kd_pegawai 		= (!empty($par['SPD_PGW']) ? $par['SPD_PGW'] : '');
						#$hie_id				= (!empty($par['HIE_ID']) ? $par['HIE_ID'] : '');
						if(!empty($kd_pegawai)){
							$parameter 	= array('api_kode' => 1123, 'api_subkode' => 3, 'api_search' => array($tgl_surat, $kd_pegawai));
							$pang_gol 	= $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
							$gol				= (!empty($pang_gol[0]['HIE_GOLONGAN']) ? $pang_gol[0]['HIE_GOLONGAN'] : 'I');
							
							$spd				= $ci->mdl_tnde_general->get_data_by_field('MD_GOL_SPD', 'GOL', $gol);
							$no_agenda	= (!empty($spd['NO_AGENDA']) ? $spd['NO_AGENDA'] : '');
							$hasil['NO_SURAT'] 	= $no_urut."/".$no_agenda."/".tahun2($tgl_surat);
						}else{
							$gol				= 'I';
							$spd				= $ci->mdl_tnde_general->get_data_by_field('MD_GOL_SPD', 'GOL', $gol);
							$no_agenda	= (!empty($spd['NO_AGENDA']) ? $spd['NO_AGENDA'] : '');
							$hasil['NO_SURAT'] 	= $no_urut."/".$no_agenda."/".tahun2($tgl_surat);
						}
					}
					break;
				case 4:
					$no_urut	=	$no_urut;
					$hasil['NO_SURAT'] 	= $no_urut." TAHUN ".tahun2($tgl_surat);
					break;
			}
		}
		return $hasil;
	}
	
	function status_penomoran_pau($unit_id=''){
		$ci =& get_instance();
		$ci->load->model('mdl_tnde');
		$parameter 	= array('api_kode' => 1225, 'api_subkode' => 5, 'api_search' => array($unit_id, 'SRT0002A', array('SRT0002B')));
		$d_unit 	= $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
		if(!empty($d_unit[0]['GRU_INSIDE']['SRT0002B'])){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function no_surat_keputusan($tahun='', $unit_id='', $kd_jenis_surat='', $tgl=''){
		#DISESUAIKAN DENGAN FITUR ORDER NOMOR SURAT
		$ci =& get_instance();
		$ci->load->model('mdl_tnde');
		$ci->load->model('tnde_master/mdl_tnde_master');
		$ci->load->model('tnde_surat_keluar/mdl_tnde_surat_keluar');
		$ci->load->library('lib_general');
		
		$unit_arr = array();
		$parameter = array('api_kode' => 1225, 'api_subkode' => 5, 'api_search' => array($unit_id, 'SRT0002A', array('SRT0002B')));
		$d_unit = $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
		if($d_unit[0]['GRU_INSIDE']['SRT0002B'] == 1){
			$parameter	= array('api_kode' => 1225, 'api_subkode' => 3, 'api_search' => array('SRT0002B'));
			$pau = $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
			$unit_arr = array_column($pau, 'UNIT_ID');
		}else{
			$unit_arr[] = $unit_id;
		}
		$unit_list 				= implode_to_string($unit_arr);
		$jns_surat 			= $ci->mdl_tnde_master->surat_sekategori($kd_jenis_surat);
		$surat_arr			= array_column_obj($jns_surat, 'KD_JENIS_SURAT');
		$jns_surat_list 	= implode_to_string($surat_arr);

		$nomor	= $ci->mdl_tnde_surat_keluar->get_no_urut_sk($tahun, $unit_list, $jns_surat_list, $tgl);
		$order		= $ci->mdl_tnde_surat_keluar->order_nomor_terakhir_mundur($tahun, $unit_list, $jns_surat_list, $tgl);
		$no1		= (!empty($nomor['NO_URUT']) ? $nomor['NO_URUT'] : 0);
		$no2		= (!empty($order['NO_TERAKHIR']) ? $order['NO_TERAKHIR'] : 0);
		
		if($no1 >= $no2){
			$hasil['NOMOR_ASAL']	= 'SISTEM';
			if($no1 == 0){
				/*$hasil['NO_SELA']	= 0;
				$hasil['NO_URUT']	= 1;
				$hasil['NOMOR']	= 1;
				$hasil['RULE']		= 1;*/
				$awal_tahun	= '01/01/'.$tahun;
				$selisih 	= selisih_hari($awal_tahun, $tgl);
				$libur		= $ci->lib_general->tgl_libur_pns_by_periode($awal_tahun, $tgl);
				$tot_libur	= count($libur);
				$hasil['SELISIH'] 	= $selisih;
				$hasil['LIBUR'] 		= $tot_libur;
				$hasil['NO_SELA'] 	= 0;
				$hasil['NO_URUT']	= 1 + $selisih - $tot_libur;
				$hasil['RUMUS']		= "AWAL TAHUN = 1 + ".$selisih." - ".$tot_libur;
				$hasil['NOMOR']		= $hasil['NO_URUT'];
				$hasil['RULE']	= 1;
			}else{
				if($nomor['TGL_SURAT'] == $tgl){
					$no_induk	= $nomor['NO_URUT'];
					$no_sela	= $nomor['NO_SELA'] + 1;
					$hasil['NO_SELA'] = $no_sela;
					$hasil['NO_URUT'] = $no_induk;
					$hasil['NOMOR'] 	= $no_induk.".".$no_sela;
					$hasil['RULE']	= 2;
				}else{
					$selisih 	= selisih_hari($nomor['TGL_SURAT'], $tgl);
					$libur		= $ci->lib_general->tgl_libur_pns_by_periode($nomor['TGL_SURAT'], $tgl);
					$tot_libur	= count($libur);
					$hasil['SELISIH'] 	= $selisih;
					$hasil['LIBUR'] 		= $tot_libur;
					$hasil['NO_SELA'] 	= 0;
					$hasil['NO_URUT']	= $nomor['NO_URUT'] + $selisih - $tot_libur;
					$hasil['RUMUS']		= "TERAKHIR ".$nomor['NO_URUT']." + ".$selisih." - ".$tot_libur;
					$hasil['NOMOR']		= $hasil['NO_URUT'];
					$hasil['RULE']	= 3;
				}
			}
		}else{
			$hasil['NO_SELA']			= 0;
			$hasil['NO_URUT']			= $no2 + 1;
			$hasil['NOMOR']			= $hasil['NO_URUT'];
			$hasil['NOMOR_ASAL']	= 'ORDER';
			$hasil['RULE']	= 4;
		}
		
		$hasil['raw'] = $nomor;
		$hasil['UNIT_LIST'] = $unit_list;
		$hasil['JENIS_SURAT_LIST'] = $jns_surat_list;
		
		return $hasil;
	}
	
	function no_surat_terakhir($tahun='', $unit_id='', $kd_jenis_surat=''){
		#DISESUAIKAN DENGAN FITUR ORDER NOMOR SURAT
		$hasil		= array();
		$ci =& get_instance();
		$ci->load->model('mdl_tnde');
		$ci->load->model('tnde_master/mdl_tnde_master');
		$ci->load->model('tnde_surat_keluar/mdl_tnde_surat_keluar');
		$unit_arr 	= array();
		$parameter = array('api_kode' => 1225, 'api_subkode' => 5, 'api_search' => array($unit_id, 'SRT0002A', array('SRT0002B')));
		$d_unit = $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
		if(!empty($d_unit[0]['GRU_INSIDE']['SRT0002B'])){
			if($d_unit[0]['GRU_INSIDE']['SRT0002B'] == 1){
				$parameter	= array('api_kode' => 1225, 'api_subkode' => 3, 'api_search' => array('SRT0002B'));
				$pau = $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
				$unit_arr = array_column($pau, 'UNIT_ID');
			}else{
				$unit_arr[] = $unit_id;
			}			
		}else{
			$unit_arr[] = $unit_id;
		}
		$unit_list 				= implode_to_string($unit_arr);
		$jns_surat 			= $ci->mdl_tnde_master->surat_sekategori($kd_jenis_surat);
		$surat_arr			= array_column_obj($jns_surat, 'KD_JENIS_SURAT');
		$jns_surat_list 	= implode_to_string($surat_arr);

		$nomor				= $ci->mdl_tnde_surat_keluar->get_no_urut_v2($tahun, $unit_list, $jns_surat_list);
		$order					= $ci->mdl_tnde_surat_keluar->order_nomor_terakhir($tahun, $unit_list, $jns_surat_list);
		$no1					= (!empty($nomor['NO_URUT']) ? $nomor['NO_URUT'] : 0);
		$no2					= (!empty($order['NO_TERAKHIR']) ? $order['NO_TERAKHIR'] : 0);

		$hasil['NO_URUT'] = max($no1, $no2);
		$hasil['NO1']	= $no1;
		$hasil['NO2']	= $no2;
		$hasil['RAW_NO1'] = $nomor;
		
		return $hasil;
	}
	
	function no_surat_mundur($tahun='', $unit_id='', $kd_jenis_surat='', $tgl=''){
		#DISESUAIKAN DENGAN FITUR ORDER NOMOR SURAT
		$ci =& get_instance();
		$ci->load->model('mdl_tnde');
		$ci->load->model('tnde_master/mdl_tnde_master');
		$ci->load->model('tnde_surat_keluar/mdl_tnde_surat_keluar');
		
		$unit_arr = array();
		$parameter = array('api_kode' => 1225, 'api_subkode' => 5, 'api_search' => array($unit_id, 'SRT0002A', array('SRT0002B')));
		$d_unit = $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
		if($d_unit[0]['GRU_INSIDE']['SRT0002B'] == 1){
			$parameter	= array('api_kode' => 1225, 'api_subkode' => 3, 'api_search' => array('SRT0002B'));
			$pau = $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
			$unit_arr = array_column($pau, 'UNIT_ID');
		}else{
			$unit_arr[] = $unit_id;
		}
		$unit_list 				= implode_to_string($unit_arr);
		$jns_surat 			= $ci->mdl_tnde_master->surat_sekategori($kd_jenis_surat);
		$surat_arr			= array_column_obj($jns_surat, 'KD_JENIS_SURAT');
		$jns_surat_list 	= implode_to_string($surat_arr);
		
		$nomor				= $ci->mdl_tnde_surat_keluar->get_no_urut2_v2($tahun, $unit_list, $jns_surat_list, $tgl);
		$order					= $ci->mdl_tnde_surat_keluar->order_nomor_terakhir_mundur($tahun, $unit_list, $jns_surat_list, $tgl);
		$no1					= (!empty($nomor['NO_URUT']) ? $nomor['NO_URUT'] : 0);
		$no2					= (!empty($order['NO_TERAKHIR']) ? $order['NO_TERAKHIR'] : 0);
		
		$hasil['raw'] = $nomor;
		$hasil['UNIT_LIST'] = $unit_list;
		$hasil['JENIS_SURAT_LIST'] = $jns_surat_list;
		$hasil['NO_URUT'] = max($no1, $no2);
		if($no1 >= $no2){
			$hasil['NOMOR_ASAL']	= 'SISTEM';
			$hasil['NO1']	= $no1;
			$hasil['NO2']	= $no2;
			$hasil['NO_SELA'] = (!empty($nomor['NO_SELA']) ? $nomor['NO_SELA'] : 0);
		}else{
			$hasil['NO1']	= $no1;
			$hasil['NO2']	= $no2;
			$hasil['NOMOR_ASAL']	= 'ORDER';
			$hasil['NO_SELA'] = 0;
		}
		
		return $hasil;
	}	
	
	#PENOMORAN VERSI 2 [BERDASARKAN PERIODE TERTENTU]
	function get_no_surat_his($par=''){
		$hasil = array();
		$ci =& get_instance();
		$ci->load->model('tnde_general/mdl_tnde_general');
		$ci->load->model('tnde_config/mdl_tnde_config');
		$ci->load->model('tnde_master/mdl_tnde_master');
		$ci->load->library('lib_general');

		$unit_id						= (!empty($par['UNIT_ID']) ? $par['UNIT_ID'] : '');
		$kd_status_simpan	= (!empty($par['KD_STATUS_SIMPAN']) ? $par['KD_STATUS_SIMPAN'] : '');
		$id_psd						= (!empty($par['ID_PSD']) ? $par['ID_PSD'] : '');
		$id_klasifikasi_surat	= (!empty($par['ID_KLASIFIKASI_SURAT']) ? $par['ID_KLASIFIKASI_SURAT'] : '');
		$kd_jenis_surat			= (!empty($par['KD_JENIS_SURAT']) ? $par['KD_JENIS_SURAT'] : '');
		$tgl_surat					= (!empty($par['TGL_SURAT']) ? $par['TGL_SURAT'] : '');
		
		
		$uinsk 						= $ci->mdl_tnde_general->get_data_by_field('CONFIG', 'NM_CONFIG', 'KD_UINSK');
		$kd_uinsk 					= $uinsk['DESKRIPSI'];
		
		$psd 							= $ci->mdl_tnde_general->get_data_by_field('MD_PSD', 'ID_PSD', $id_psd);
		$kd_psd 						= (!empty($psd['KD_PSD']) ? $psd['KD_PSD'] : '');
		
		$klasifikasi_surat 		= $ci->mdl_tnde_general->get_data_by_field('MD_KLASIFIKASI_SURAT', 'ID_KLASIFIKASI_SURAT', $id_klasifikasi_surat);
		$kd_klasifikasi_surat 	= (!empty($klasifikasi_surat['KD_KLASIFIKASI_SURAT']) ? $klasifikasi_surat['KD_KLASIFIKASI_SURAT'] : '');
		
		$jns_surat					=	$ci->mdl_tnde_general->get_data_by_field('MD_JENIS_SURAT', 'KD_JENIS_SURAT', $kd_jenis_surat);
		$kd_kat_penomoran 	= (!empty($jns_surat['KD_KAT_PENOMORAN']) ? $jns_surat['KD_KAT_PENOMORAN'] : '');
		
		$con['TANGGAL']				= $tgl_surat;
		$con['UNIT_ID']					= $unit_id;
		$con['KD_JENIS_SURAT']	= $kd_jenis_surat;
		$config_penomoran	= $ci->mdl_tnde_config->config_penomoran($con);
		$buat_nomor	=	FALSE;
		if(empty($config_penomoran)){
			$save_sk	= FALSE;
			$buat_nomor	=	FALSE;
			$hasil['ERRORS'][]	= "Penomoran untuk unit ini belum di-setting.";
		}			
		if(empty($config_penomoran['STATUS_PENOMORAN'])){
			$config_penomoran['STATUS_PENOMORAN'] = 0;
		}
		
		$js['TANGGAL']					= $tgl_surat;
		$js['KD_JENIS_SURAT']	= $kd_jenis_surat;
		$jenis_surat		= $ci->mdl_tnde_master->his_jenis_surat_by_tgl($js);
		if(empty($jenis_surat['ID_HIS_JENIS_SURAT'])){
			$hasil['ERRORS'][] = "Kelompok jenis surat ini belum di-setting.";
		}

		if(empty($hasil['ERRORS'])){
			if($config_penomoran['STATUS_PENOMORAN'] == 1){
				#$tgl_default	=	date_to_number(tgl($config_penomoran['BERLAKU_DARI']));
				#$tgl_surat_in	=	date_to_number($tgl_surat);
					if($kd_status_simpan == 2){
						if($kd_kat_penomoran == 1){
							$hari_libur	= $ci->lib_general->tgl_libur_pns_by_tgl($tgl_surat);
							if(!empty($hari_libur)){
								$buat_nomor		=	FALSE;
								$hasil['NO_URUT'] 		= '';
								$hasil['NO_SELA'] 			= '';
								$hasil['NO_SURAT'] 		= '';
								$hasil['ERRORS'][] 	= "Surat Keputusan tidak dapat dibuat dihari libur. ".$hari_libur[0];
							}else{
								$no_sk	= $this->no_surat_keputusan_his(tahun($tgl_surat), $unit_id, $kd_jenis_surat, $tgl_surat);
								$hasil['NO_URUT']	= (!empty($no_sk['NO_URUT']) ? $no_sk['NO_URUT'] : 0);
								$hasil['NO_SELA']	= (!empty($no_sk['NO_SELA']) ? $no_sk['NO_SELA'] : 0);
								$no_urut					= (!empty($no_sk['NOMOR']) ? $no_sk['NOMOR'] : 0);
								$buat_nomor		=	TRUE;
							}
						}else{
							if($tgl_surat == date('d/m/Y')){
								$no 					= $this->no_surat_terakhir_his(date('Y'), $unit_id, $kd_jenis_surat);
								$no_db			= (!empty($no['NO_URUT']) ? $no['NO_URUT'] : 0);
								$no_setting 	= (!empty($config_penomoran['NO_REGISTER']) ? $config_penomoran['NO_REGISTER'] : 0);
								$no_terakhir	= max($no_db, $no_setting);
								
								$hasil['NO_URUT'] 	= $no_terakhir + 1;
								$hasil['NO_SELA'] 		= "0";
								$no_urut				= ($hasil['NO_URUT'] > 9 ? $hasil['NO_URUT'] : "0".$hasil['NO_URUT']);
							}else{
								$no 		= $this->no_surat_mundur_his(tahun2($tgl_surat), $unit_id, $kd_jenis_surat, $tgl_surat);
			
								if(empty($no)){
									$no['NO_URUT'] 	= $config_penomoran['NO_REGISTER'];
									$no['NO_SELA'] 		= 0;
								}
								$hasil['NO_URUT'] 	= $no['NO_URUT'];
								$hasil['NO_SELA'] 		= $no['NO_SELA'] + 1;
								$no_urut						= ($hasil['NO_URUT'] > 9 ? $hasil['NO_URUT'] : "0".$hasil['NO_URUT']).".".$hasil['NO_SELA'];
							}
							$buat_nomor	=	TRUE;
						}

					}else{
						$buat_nomor	=	FALSE;
						$hasil['NO_URUT']		= '';
						$hasil['NO_SELA']		= '';
						$hasil['NO_SURAT']	= '';
					}
					$save_sk = TRUE;

			}else{
				$no_urut						= (!empty($par['NO_URUT']) ? $par['NO_URUT'] : '');
				$no_sela						= (!empty($par['NO_SELA']) ? $par['NO_SELA'] : '');
				$buat_nomor	=	TRUE;
				$save_sk = TRUE;
				$hasil['NO_URUT']		= $no_urut;
				$hasil['NO_SELA']		= $no_sela;
				if($hasil['NO_SELA'] == 0){ $hasil['NO_SELA'] = '';}
				if(empty($hasil['NO_SELA'])){
					$no_urut	=	"0".$hasil['NO_URUT'];
				}else{
					$no_urut	=	"0".$hasil['NO_URUT'].".".$hasil['NO_SELA'];
				}
			}
		}
		
		if($buat_nomor == TRUE){
			$kat_penomoran	=	$ci->mdl_tnde_general->get_data_by_field('MD_JENIS_SURAT', 'KD_JENIS_SURAT', $kd_jenis_surat);
			switch($kat_penomoran['KD_KAT_PENOMORAN']){
				case 0:
					$hasil['NO_SURAT']	= "";
					break;
				case 1:
					if($kd_jenis_surat == 1){
						#$pau 		= $this->status_penomoran_pau($unit_id);	
						if($config_penomoran['GMU_SLUG'] == 'SRT0002B'){
							#FORMAT SK REKTOR
							$hasil['NO_SURAT'] 	= $no_urut." TAHUN ".tahun2($tgl_surat);
						}else{
							#FORMAT SK DEKAN
							$hasil['NO_SURAT'] 	= $no_urut."/".$kd_psd."/".tahun2($tgl_surat);
						}
					}else{
						$hasil['NO_SURAT'] 	= $no_urut." TAHUN ".tahun2($tgl_surat);
					}
					break;
				case 2:
					$hasil['NO_SURAT'] 	= $kd_uinsk."/".$kd_psd."/".$kd_klasifikasi_surat."/".$no_urut."/".tahun2($tgl_surat);
					break;
				case 3:
					if($kd_jenis_surat == 19){
						#$pgw = explode("#", $kpd_pegawai[0]);
						$kd_pegawai 		= (!empty($par['SPD_PGW']) ? $par['SPD_PGW'] : '');
						#$hie_id				= (!empty($par['HIE_ID']) ? $par['HIE_ID'] : '');
						if(!empty($kd_pegawai)){
							$parameter 	= array('api_kode' => 1123, 'api_subkode' => 3, 'api_search' => array($tgl_surat, $kd_pegawai));
							$pang_gol 	= $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
							$gol				= (!empty($pang_gol[0]['HIE_GOLONGAN']) ? $pang_gol[0]['HIE_GOLONGAN'] : 'I');
							
							$spd				= $ci->mdl_tnde_general->get_data_by_field('MD_GOL_SPD', 'GOL', $gol);
							$no_agenda	= (!empty($spd['NO_AGENDA']) ? $spd['NO_AGENDA'] : '');
							$hasil['NO_SURAT'] 	= $no_urut."/".$no_agenda."/".tahun2($tgl_surat);
						}else{
							$gol				= 'I';
							$spd				= $ci->mdl_tnde_general->get_data_by_field('MD_GOL_SPD', 'GOL', $gol);
							$no_agenda	= (!empty($spd['NO_AGENDA']) ? $spd['NO_AGENDA'] : '');
							$hasil['NO_SURAT'] 	= $no_urut."/".$no_agenda."/".tahun2($tgl_surat);
						}
					}
					break;
				case 4:
					$no_urut	=	$no_urut;
					$hasil['NO_SURAT'] 	= $no_urut." TAHUN ".tahun2($tgl_surat);
					break;
			}
		}
		return $hasil;
	}
	
	#PENOMORAN VERSI 2.1 [BERDASARKAN PERIODE TERTENTU & FORMAT BARU] 
	#by Dedy F. Setyawan (18/05/2016)
	function get_no_surat_his_format($par=''){
		$hasil = array();
		$ci =& get_instance();
		$ci->load->model('tnde_general/mdl_tnde_general');
		$ci->load->model('tnde_config/mdl_tnde_config');
		$ci->load->model('tnde_master/mdl_tnde_master');
		$ci->load->library('lib_general');

		$unit_id						= (!empty($par['UNIT_ID']) ? $par['UNIT_ID'] : '');
		$kd_status_simpan	= (!empty($par['KD_STATUS_SIMPAN']) ? $par['KD_STATUS_SIMPAN'] : '');
		$id_psd						= (!empty($par['ID_PSD']) ? $par['ID_PSD'] : '');
		$id_klasifikasi_surat	= (!empty($par['ID_KLASIFIKASI_SURAT']) ? $par['ID_KLASIFIKASI_SURAT'] : '');
		$kd_jenis_surat			= (!empty($par['KD_JENIS_SURAT']) ? $par['KD_JENIS_SURAT'] : '');
		$kd_keamanan_surat	= (!empty($par['KD_KEAMANAN_SURAT']) ? $par['KD_KEAMANAN_SURAT'] : 'B');
		$tgl_surat					= (!empty($par['TGL_SURAT']) ? $par['TGL_SURAT'] : '');
		
		
		$uinsk 						= $ci->mdl_tnde_general->get_data_by_field('CONFIG', 'NM_CONFIG', 'KD_UINSK');
		$kd_uinsk 					= $uinsk['DESKRIPSI'];
		
		$psd 							= $ci->mdl_tnde_general->get_data_by_field('MD_PSD', 'ID_PSD', $id_psd);
		$kd_psd 						= (!empty($psd['KD_PSD']) ? $psd['KD_PSD'] : '');
		
		$klasifikasi_surat 		= $ci->mdl_tnde_general->get_data_by_field('MD_KLASIFIKASI_SURAT', 'ID_KLASIFIKASI_SURAT', $id_klasifikasi_surat);
		$kd_klasifikasi_surat 	= (!empty($klasifikasi_surat['KD_KLASIFIKASI_SURAT']) ? $klasifikasi_surat['KD_KLASIFIKASI_SURAT'] : '');
		
		$jns_surat					=	$ci->mdl_tnde_general->get_data_by_field('MD_JENIS_SURAT', 'KD_JENIS_SURAT', $kd_jenis_surat);
		$kd_kat_penomoran 	= (!empty($jns_surat['KD_KAT_PENOMORAN']) ? $jns_surat['KD_KAT_PENOMORAN'] : '');
		
		$con['TANGGAL']				= $tgl_surat;
		$con['UNIT_ID']					= $unit_id;
		$con['KD_JENIS_SURAT']	= $kd_jenis_surat;
		$config_penomoran	= $ci->mdl_tnde_config->config_penomoran($con);
		$buat_nomor	=	FALSE;
		if(empty($config_penomoran)){
			$save_sk	= FALSE;
			$buat_nomor	=	FALSE;
			$hasil['ERRORS'][]	= "Penomoran untuk unit ini belum di-setting.";
		}			
		if(empty($config_penomoran['STATUS_PENOMORAN'])){
			$config_penomoran['STATUS_PENOMORAN'] = 0;
		}
		
		$js['TANGGAL']					= $tgl_surat;
		$js['KD_JENIS_SURAT']	= $kd_jenis_surat;
		$jenis_surat		= $ci->mdl_tnde_master->his_jenis_surat_by_tgl($js);
		if(empty($jenis_surat['ID_HIS_JENIS_SURAT'])){
			$hasil['ERRORS'][] = "Kelompok jenis surat ini belum di-setting.";
		}

		if(empty($hasil['ERRORS'])){
			if($config_penomoran['STATUS_PENOMORAN'] == 1){
				#$tgl_default	=	date_to_number(tgl($config_penomoran['BERLAKU_DARI']));
				#$tgl_surat_in	=	date_to_number($tgl_surat);
					if($kd_status_simpan == 2){
						if($kd_kat_penomoran == 1){
							$hari_libur	= $ci->lib_general->tgl_libur_pns_by_tgl($tgl_surat);
							if(!empty($hari_libur)){
								$buat_nomor		=	FALSE;
								$hasil['NO_URUT'] 		= '';
								$hasil['NO_SELA'] 			= '';
								$hasil['NO_SURAT'] 		= '';
								$hasil['ERRORS'][] 	= "Surat Keputusan tidak dapat dibuat dihari libur. ".$hari_libur[0];
							}else{
								$no_sk	= $this->no_surat_keputusan_his(tahun($tgl_surat), $unit_id, $kd_jenis_surat, $tgl_surat);
								$hasil['NO_URUT']	= (!empty($no_sk['NO_URUT']) ? $no_sk['NO_URUT'] : 0);
								$hasil['NO_SELA']	= (!empty($no_sk['NO_SELA']) ? $no_sk['NO_SELA'] : 0);
								$no_urut					= (!empty($no_sk['NOMOR']) ? $no_sk['NOMOR'] : 0);
								$buat_nomor		=	TRUE;
							}
						}else{
							if($tgl_surat == date('d/m/Y')){
								$no 					= $this->no_surat_terakhir_his(date('Y'), $unit_id, $kd_jenis_surat);
								$no_db			= (!empty($no['NO_URUT']) ? $no['NO_URUT'] : 0);
								$no_setting 	= (!empty($config_penomoran['NO_REGISTER']) ? $config_penomoran['NO_REGISTER'] : 0);
								$no_terakhir	= max($no_db, $no_setting);
								
								$hasil['NO_URUT'] 	= $no_terakhir + 1;
								$hasil['NO_SELA'] 		= "0";
								$no_urut				= ($hasil['NO_URUT'] > 9 ? $hasil['NO_URUT'] : "0".$hasil['NO_URUT']);
							}else{
								$no 		= $this->no_surat_mundur_his(tahun2($tgl_surat), $unit_id, $kd_jenis_surat, $tgl_surat);
			
								if(empty($no)){
									$no['NO_URUT'] 	= $config_penomoran['NO_REGISTER'];
									$no['NO_SELA'] 		= 0;
								}
								$hasil['NO_URUT'] 	= $no['NO_URUT'];
								$hasil['NO_SELA'] 		= $no['NO_SELA'] + 1;
								$no_urut						= ($hasil['NO_URUT'] > 9 ? $hasil['NO_URUT'] : "0".$hasil['NO_URUT']).".".$hasil['NO_SELA'];
							}
							$buat_nomor	=	TRUE;
						}

					}else{
						$buat_nomor	=	FALSE;
						$hasil['NO_URUT']		= '';
						$hasil['NO_SELA']		= '';
						$hasil['NO_SURAT']	= '';
					}
					$save_sk = TRUE;

			}else{
				$no_urut						= (!empty($par['NO_URUT']) ? $par['NO_URUT'] : '');
				$no_sela						= (!empty($par['NO_SELA']) ? $par['NO_SELA'] : '');
				$buat_nomor	=	TRUE;
				$save_sk = TRUE;
				$hasil['NO_URUT']		= $no_urut;
				$hasil['NO_SELA']		= $no_sela;
				if($hasil['NO_SELA'] == 0){ $hasil['NO_SELA'] = '';}
				if(empty($hasil['NO_SELA'])){
					$no_urut	=	"0".$hasil['NO_URUT'];
				}else{
					$no_urut	=	"0".$hasil['NO_URUT'].".".$hasil['NO_SELA'];
				}
			}
		}
		
		if($buat_nomor == TRUE){
			#$kat_penomoran	=	$ci->mdl_tnde_general->get_data_by_field('MD_JENIS_SURAT', 'KD_JENIS_SURAT', $kd_jenis_surat);
			$format_no	= format_no_surat($kd_jenis_surat);
			switch($format_no){
				case 'KOSONG':
					$hasil['NO_SURAT']	= "";
					break;
				case 'TUNGGAL':
					if($kd_jenis_surat == 1){
						#$pau 		= $this->status_penomoran_pau($unit_id);	
						if($config_penomoran['GMU_SLUG'] == 'SRT0002B'){
							#FORMAT SK REKTOR
							$hasil['NO_SURAT'] 	= $no_urut." TAHUN ".tahun2($tgl_surat);
						}else{
							#FORMAT SK DEKAN
							$hasil['NO_SURAT'] 	= $no_urut."/".$kd_psd."/".tahun2($tgl_surat);
						}
					}else{
						$hasil['NO_SURAT'] 	= $no_urut." TAHUN ".tahun2($tgl_surat);
					}
					break;
				case 'UMUM':
					$hasil['NO_SURAT'] 	= $kd_keamanan_surat."-".$no_urut."/".$kd_uinsk."/".($kd_psd == 'R' ? '' : $kd_psd.'/').$kd_klasifikasi_surat."/".bulan($tgl_surat)."/".tahun2($tgl_surat);
					#$hasil['NO_SURAT'] 	= $kd_uinsk."/".$kd_psd."/".$kd_klasifikasi_surat."/".$no_urut."/".tahun2($tgl_surat);
					break;				
				case 'KHUSUS':
					$hasil['NO_SURAT'] 	= $no_urut."/".$kd_uinsk."/".($kd_psd == 'R' ? '' : $kd_psd.'/').$kd_klasifikasi_surat."/".bulan($tgl_surat)."/".tahun2($tgl_surat);
					break;
				case 'SPD':
					if($kd_jenis_surat == 19){
						#$pgw = explode("#", $kpd_pegawai[0]);
						$kd_pegawai 		= (!empty($par['SPD_PGW']) ? $par['SPD_PGW'] : '');
						#$hie_id				= (!empty($par['HIE_ID']) ? $par['HIE_ID'] : '');
						if(!empty($kd_pegawai)){
							$parameter 	= array('api_kode' => 1123, 'api_subkode' => 3, 'api_search' => array($tgl_surat, $kd_pegawai));
							$pang_gol 	= $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
							$gol				= (!empty($pang_gol[0]['HIE_GOLONGAN']) ? $pang_gol[0]['HIE_GOLONGAN'] : 'I');
							
							$spd				= $ci->mdl_tnde_general->get_data_by_field('MD_GOL_SPD', 'GOL', $gol);
							$no_agenda	= (!empty($spd['NO_AGENDA']) ? $spd['NO_AGENDA'] : '');
							$hasil['NO_SURAT'] 	= $no_urut."/".$no_agenda."/".tahun2($tgl_surat);
						}else{
							$gol				= 'I';
							$spd				= $ci->mdl_tnde_general->get_data_by_field('MD_GOL_SPD', 'GOL', $gol);
							$no_agenda	= (!empty($spd['NO_AGENDA']) ? $spd['NO_AGENDA'] : '');
							$hasil['NO_SURAT'] 	= $no_urut."/".$no_agenda."/".tahun2($tgl_surat);
						}
					}
					break;
			}
		}
		return $hasil;
	}
	
	function no_surat_keputusan_his($tahun='', $unit_id='', $kd_jenis_surat='', $tgl=''){
		#DISESUAIKAN DENGAN FITUR ORDER NOMOR SURAT
		$ci =& get_instance();
		$ci->load->model('mdl_tnde');
		$ci->load->model('tnde_surat_keluar/mdl_tnde_surat_keluar');
		$ci->load->library('lib_general');

		$nomor	= $ci->mdl_tnde_surat_keluar->get_no_urut_sk_his($tahun, $unit_id, $kd_jenis_surat, $tgl);
		$order		= $ci->mdl_tnde_surat_keluar->order_nomor_terakhir_mundur_his($tahun, $unit_id, $kd_jenis_surat, $tgl);
		$no1		= (!empty($nomor['NO_URUT']) ? $nomor['NO_URUT'] : 0);
		$no2		= (!empty($order['NO_TERAKHIR']) ? $order['NO_TERAKHIR'] : 0);
		
		if($no1 >= $no2){
			$hasil['NOMOR_ASAL']	= 'SISTEM';
			if($no1 == 0){
				/*$hasil['NO_SELA']	= 0;
				$hasil['NO_URUT']	= 1;
				$hasil['NOMOR']	= 1;
				$hasil['RULE']		= 1;*/
				$awal_tahun	= '01/01/'.$tahun;
				$selisih 	= selisih_hari($awal_tahun, $tgl);
				$libur		= $ci->lib_general->tgl_libur_pns_by_periode($awal_tahun, $tgl);
				$tot_libur	= count($libur);
				$hasil['SELISIH'] 	= $selisih;
				$hasil['LIBUR'] 		= $tot_libur;
				$hasil['NO_SELA'] 	= 0;
				$hasil['NO_URUT']	= 1 + $selisih - $tot_libur;
				$hasil['RUMUS']		= "AWAL TAHUN = 1 + ".$selisih." - ".$tot_libur;
				$hasil['NOMOR']		= $hasil['NO_URUT'];
				$hasil['RULE']	= 1;
			}else{
				if($nomor['TGL_SURAT'] == $tgl){
					$no_induk	= $nomor['NO_URUT'];
					$no_sela	= $nomor['NO_SELA'] + 1;
					$hasil['NO_SELA'] = $no_sela;
					$hasil['NO_URUT'] = $no_induk;
					$hasil['NOMOR'] 	= $no_induk.".".$no_sela;
					$hasil['RULE']	= 2;
				}else{
					$selisih 	= selisih_hari($nomor['TGL_SURAT'], $tgl);
					$libur		= $ci->lib_general->tgl_libur_pns_by_periode($nomor['TGL_SURAT'], $tgl);
					$tot_libur	= count($libur);
					$hasil['SELISIH'] 	= $selisih;
					$hasil['LIBUR'] 		= $tot_libur;
					$hasil['NO_SELA'] 	= 0;
					$hasil['NO_URUT']	= $nomor['NO_URUT'] + $selisih - $tot_libur;
					$hasil['RUMUS']		= "TERAKHIR ".$nomor['NO_URUT']." + ".$selisih." - ".$tot_libur;
					$hasil['NOMOR']		= $hasil['NO_URUT'];
					$hasil['RULE']	= 3;
				}
			}
		}else{
			$hasil['NO_SELA']			= 0;
			$hasil['NO_URUT']			= $no2 + 1;
			$hasil['NOMOR']			= $hasil['NO_URUT'];
			$hasil['NOMOR_ASAL']	= 'ORDER';
			$hasil['RULE']	= 4;
		}
		
		$hasil['raw'] = $nomor;
		
		return $hasil;
	}
	
	function no_surat_terakhir_his($tahun='', $unit_id='', $kd_jenis_surat=''){
		#DISESUAIKAN DENGAN FITUR ORDER NOMOR SURAT
		$hasil		= array();
		$ci =& get_instance();
		$ci->load->model('mdl_tnde');
		$ci->load->model('tnde_surat_keluar/mdl_tnde_surat_keluar');

		$nomor				= $ci->mdl_tnde_surat_keluar->get_no_urut_his($tahun, $unit_id, $kd_jenis_surat);
		$order					= $ci->mdl_tnde_surat_keluar->order_nomor_terakhir_his($tahun, $unit_id, $kd_jenis_surat);
		$no1					= (!empty($nomor['NO_URUT']) ? $nomor['NO_URUT'] : 0);
		$no2					= (!empty($order['NO_TERAKHIR']) ? $order['NO_TERAKHIR'] : 0);

		$hasil['NO_URUT'] = max($no1, $no2);
		$hasil['NO1']	= $no1;
		$hasil['NO2']	= $no2;
		$hasil['RAW_NO1'] = $nomor;
		
		return $hasil;
	}
	
	function no_surat_mundur_his($tahun='', $unit_id='', $kd_jenis_surat='', $tgl=''){
		#DISESUAIKAN DENGAN FITUR ORDER NOMOR SURAT
		$ci =& get_instance();
		$ci->load->model('mdl_tnde');
		$ci->load->model('tnde_surat_keluar/mdl_tnde_surat_keluar');
		
		$nomor				= $ci->mdl_tnde_surat_keluar->get_no_urut_mundur_his($tahun, $unit_id, $kd_jenis_surat, $tgl);
		$order					= $ci->mdl_tnde_surat_keluar->order_nomor_terakhir_mundur_his($tahun, $unit_id, $kd_jenis_surat, $tgl);
		$no1					= (!empty($nomor['NO_URUT']) ? $nomor['NO_URUT'] : 0);
		$no2					= (!empty($order['NO_TERAKHIR']) ? $order['NO_TERAKHIR'] : 0);
		
		$hasil['raw'] = $nomor;
		$hasil['NO_URUT'] = max($no1, $no2);
		if($no1 >= $no2){
			$hasil['NOMOR_ASAL']	= 'SISTEM';
			$hasil['NO1']	= $no1;
			$hasil['NO2']	= $no2;
			$hasil['NO_SELA'] = (!empty($nomor['NO_SELA']) ? $nomor['NO_SELA'] : 0);
		}else{
			$hasil['NO1']	= $no1;
			$hasil['NO2']	= $no2;
			$hasil['NOMOR_ASAL']	= 'ORDER';
			$hasil['NO_SELA'] = 0;
		}
		
		return $hasil;
	}
}
?>