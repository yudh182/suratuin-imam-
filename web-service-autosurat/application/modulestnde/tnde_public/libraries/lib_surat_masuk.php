<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');
Class Lib_surat_masuk extends CI_Controller{
	function __construct(){
	}
	
	function insert_surat_masuk_from_sk($id_surat_keluar='', $kd_pgw='', $kd_keamanan_surat=''){
		$ci =& get_instance();
		$ci->load->model('mdl_tnde');
		$ci->load->model('tnde_general/mdl_tnde_general');
		$ci->load->model('tnde_surat_keluar/mdl_tnde_surat_keluar');
		$ci->load->model('tnde_surat_masuk/mdl_tnde_surat_masuk');
		$ci->load->model('tnde_arsip/mdl_arsip');
		$ci->load->library('lib_general');
		
		$data['VALID'] 	= FALSE;
		$surat				= $ci->mdl_tnde_surat_keluar->detail_sk_2_by_id_surat_keluar_v2($id_surat_keluar);
		if(!empty($surat['ID_SURAT_KELUAR'])){
			$psd = $ci->mdl_tnde_general->get_data_by_field('MD_PSD', 'ID_PSD', $surat['ID_PSD']);
			if(!empty($psd['KD_JABATAN'])){
				$parameter 	= array('api_kode' => 1101, 'api_subkode' => 1, 'api_search' => array($psd['KD_JABATAN']));
				$jabatan 		= $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
			}
			$parameter = array('api_kode' => 1121, 'api_subkode' => 3, 'api_search' => array(date('d/m/Y'), $kd_pgw, 1)); 
			$pencatat 		= $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
			$jab_pencatat	= (!empty($pencatat[0]['STR_ID']) ? $pencatat[0]['STR_ID'] : '');
			
			$data_sm['ID_SURAT_MASUK'] 			= uniqidku();
			$data_sm['ID_SURAT_KELUAR'] 			= $id_surat_keluar;
			$data_sm['TGL_MASUK'] 						= date('d/m/Y');
			$data_sm['INDEKS_BERKAS'] 				= $surat['NM_JENIS_SURAT'];
			$data_sm['LAMPIRAN'] 							= (!empty($surat['LAMPIRAN']) ? $surat['LAMPIRAN'] : '-');
			$data_sm['KODE'] 								= '';
			$data_sm['PERIHAL'] 							= (!empty($surat['PERIHAL']) ? $surat['PERIHAL'] : '-');
			$data_sm['DARI'] 									= (!empty($jabatan[0]['STR_NAMA']) ? $jabatan[0]['STR_NAMA'] : '-');
			$data_sm['KEPADA'] 							= '-';
			$data_sm['TEMBUSAN'] 						= '-';
			$data_sm['TGL_SURAT'] 						= $surat['TGL_SURAT'];
			$data_sm['NO_SURAT'] 						= (!empty($surat['NO_SURAT']) ? $surat['NO_SURAT'] : '');
			$data_sm['CATATAN'] 							= '';
			$data_sm['KD_KEAMANAN_SURAT'] 	= $kd_keamanan_surat;
			$data_sm['KD_SIFAT_SURAT'] 			= (!empty($surat['KD_SIFAT_SURAT']) ? $surat['KD_SIFAT_SURAT'] : '');
			$data_sm['PENCATAT_SURAT'] 			= $kd_pgw;
			$data_sm['PENGARAH_SURAT'] 			= $kd_pgw;
			$data_sm['DIBUAT_DARI'] 					= 'SURAT_KELUAR';
			$data_sm['JABATAN_PENGARAH'] 		= $jab_pencatat;
			$data_sm['ID_STATUS_SM'] 				= 3;
			$data_sm['WAKTU_PENCATATAN'] 	= date("d/m/Y H:i:s");
			$data_sm['WAKTU_AKSES'] 				= date("d/m/Y H:i:s");
			$simpan_surat 	= $ci->mdl_tnde_surat_masuk->insert_surat_masuk_from_sk($data_sm);
			if($simpan_surat == TRUE){
				$meta['ID_SURAT_MASUK']		= $data_sm['ID_SURAT_MASUK'];
				$meta['PENGIRIM']						= $kd_pgw;
				$meta['JAB_PENGIRIM']			= $jab_pencatat;
				$meta['KD_GRUP_PENGIRIM']	= $ci->lib_general->grup_user($meta['PENGIRIM'], $meta['JAB_PENGIRIM']); 
				$meta['ISI_RINGKAS']						= '';
				$meta['ISI_PESAN']							= '';
				$meta['WAKTU_PENYELESAIAN'] 	= '';
				$meta['INDUK']									= '';
				$insert_meta			= $ci->mdl_arsip->insert_meta_distribusi_surat($meta);
				if(empty($insert_meta[0]->ID)){
					$data['ERRORS'][] = "Gagal mengirim surat. #MS00";
				}
			}else{
				$data['ERRORS'][] = "Gagal mengirim surat. #SM00";
			}
			
			if(empty($data['ERRORS'])){
				$d_log['ID_SURAT_MASUK']		= $data_sm['ID_SURAT_MASUK'];
				$d_log['ACTION']						= "CREATE";
				$d_log['KD_PEGAWAI']				= $kd_pgw;
				$d_log['STR_ID']							= (!empty($pencatat[0]['STR_ID']) ? $pencatat[0]['STR_ID'] : '-');
				$d_log['WAKTU_PERTAMA']		= date('d/m/Y H:i:s');
				$d_log['WAKTU_TERAKHIR']	= date('d/m/Y H:i:s');
				$simpan_log		= $ci->mdl_tnde_surat_masuk->simpan_log_md_surat_masuk($d_log);
				if($simpan_log == FALSE){
					$data['ERRORS'][] = "Gagal mencatat log surat masuk.";
				}
				
				$penerima_sk	= $ci->mdl_tnde_surat_keluar->get_penerima_sk_for_sm($id_surat_keluar);
				if(!empty($penerima_sk)){
					$n = 1;
					foreach($penerima_sk as $val){
						if($val->KD_GRUP_TUJUAN != 'EKS01'){
							$data_t['ID_GRUP_SURAT'] 				= $n;
							$data_t['PENERIMA_SURAT'] 			= $val->PENERIMA_SURAT;
							$data_t['JABATAN_PENERIMA'] 		= (!empty($val->JABATAN_PENERIMA) ? $val->JABATAN_PENERIMA : '');
							$data_t['KD_STATUS_DISTRIBUSI']	= $val->KD_STATUS_DISTRIBUSI;
							$data_t['ID_STATUS_SM'] 				= 1;
							$data_t['KD_GRUP_TUJUAN'] 			= $val->KD_GRUP_TUJUAN;
							$data_t['ID_META_DISTRIBUSI']		= $insert_meta[0]->ID;
							
							$data['insert'][] 									= $data_t;
							$arahkan 		= $ci->mdl_tnde_surat_masuk->insert_distribusi_surat_v2($data_t);
							if($arahkan == FALSE){
								switch($data_t['KD_GRUP_TUJUAN']){
									case 'PJB01':
										$parameter = array('api_kode' => 1101, 'api_subkode' => 1, 'api_search' => array($data_t['JABATAN_PENERIMA']));
										$jab = $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
										$nama = (!empty($jab[0]['STR_NAMA']) ? $jab[0]['STR_NAMA'] : '');
										break;
									case 'PGW01':
										$parameter = array('api_kode' => 2001, 'api_subkode' => 2, 'api_search' => array($data_t['PENERIMA_SURAT']));
										$pgw = $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
										$nama = (!empty($pgw[0]['NM_PGW_F']) ? $pgw[0]['NM_PGW_F'] : '');
										break;
									case 'MHS01':
										$parameter	= array('api_kode'=>26000, 'api_subkode' => 12, 'api_search' => array($data_t['PENERIMA_SURAT']));
										$get_mhs 		= $ci->mdl_tnde->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);
										$nama = (!empty($get_mhs[0]['NAMA_F']) ? $get_mhs[0]['NAMA_F'] : '');
										break;
								}
								$data['ERRORS'][] = "Gagal mengirim surat ke ".$nama;
							}else{
								$n++;
							}
						}
					}
				}
				if(empty($data['ERRORS'])){
					$wr['ID_SURAT_KELUAR'] = $id_surat_keluar;
					$dd['KD_STATUS_SIMPAN'] = 3; #final
					$kesshou = $ci->mdl_tnde_general->update_data('MD_SURAT_KELUAR2', $dd, $wr);
					if($kesshou){
						$data['VALID'] = TRUE;
					}else{
						$data['ERRORS'][] = "Gagal memperbaharui surat keluar";
					}
				}
			}
		}else{
			$data['ERRORS'][] = 'Data tidak ditemukan';
		}
		$data['RAW'] = $data_sm;
		$data['OUTPUT'] = $meta;
		$data['INSERT_META'] = $penerima_sk;
		return $data;
	}
}
?>