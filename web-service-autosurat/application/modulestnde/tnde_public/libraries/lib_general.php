<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');
Class Lib_general extends CI_Controller{
	function __construct(){
	}
	
	function status_jabatan_by_str_id($str_id=''){
		$ci =& get_instance();
		$ci->load->model('mdl_tnde');
		
		$parameter = array('api_kode' => 1101, 'api_subkode' => 1, 'api_search' => array($str_id));
		$detail = $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
		if(!empty($detail[0])){
			$hasil = status_jabatan($detail[0]);
		}else{
			$hasil = '';
		}
		return $hasil;
	}
	
	function grup_user($kode='', $jabatan=''){
		$ci =& get_instance();
		$ci->load->model('mdl_tnde');
		
		$parameter	= array('api_kode'=>26000, 'api_subkode' => 12, 'api_search' => array($kode));
		$mhs		 	= $ci->mdl_tnde->api_sia('sia_mahasiswa/data_search', 'json', 'POST', $parameter);
		$hasil = '';
		if(empty($mhs[0])){
			$parameter 	= array('api_kode' => 2001, 'api_subkode' => 2, 'api_search' => array($kode)); #kd_pgw
			$pegawai 		= $ci->mdl_tnde->api_simpeg('simpeg_mix/data_search', 'json', 'POST', $parameter);
			if(!empty($pegawai[0])){
				$status	= $this->status_jabatan_by_str_id($jabatan);
				switch($status){
					case 'PEJABAT':
						$hasil = 'PJB01';
						break;
					case 'PELAKSANA':
						$hasil = 'PGW01';
						break;
					default:
						$hasil = 'PGW01';
						break;
				}
			}else{
				$hasil = '';
			}
		}else{
			$hasil	= 'MHS01';
		}
		return $hasil;
	}
	
	function tgl_libur_pns_by_tgl($tgl){
		$ci =& get_instance();
		$ci->load->model('mdl_tnde');
		$ci->load->model('tnde_master/mdl_tnde_master');

		$hari_kerja	= $ci->mdl_tnde_master->get_hari_kerja_by_tgl($tgl);
		$list_kerja	= (!empty($hari_kerja) ? array_column_obj($hari_kerja, 'NM_HARI') : array());
		
		$hari			= hari($tgl);
		$hasil			= array();
		if(! in_array($hari, $list_kerja)){
			$hasil[] = "Hari ".$hari." bukan hari kerja.";
		}else{
			$tgl_ikd		= str_replace("/", "-", $tgl);
			$parameter	= array('api_kode' => 1000, 'api_subkode' => 3, 'api_search' => array($tgl_ikd));
			$tgl_libur 	= $ci->mdl_tnde->api_ikd('ikd_admin/Lihat_hari_lbur', 'json', 'POST', $parameter);
			if(!empty($tgl_libur[0])){
				$hasil[]	= "Hari Libur ".huruf_kapital($tgl_libur[0]['KETERANGAN']);
			}
		}
		return $hasil;
	}
	
	function tgl_libur_pns_by_periode($tgl1='', $tgl2=''){
		$ci =& get_instance();
		$ci->load->model('mdl_tnde');
		$ci->load->model('tnde_master/mdl_tnde_master');
		
		$asal	= str_replace("/", "-", $tgl1);
		$dt		= new DateTime($asal);
		$dt->modify('-1 day');
		$from	= $dt->format('d-m-Y');
		$to 		= str_replace("/", "-", $tgl2);
		$hasil	= array();
		
		$hari_kerja	= $ci->mdl_tnde_master->get_hari_kerja_by_tgl($tgl2);
		$list_kerja	= (!empty($hari_kerja) ? array_column_obj($hari_kerja, 'NM_HARI') : array());
		
		while (strtotime($from)<strtotime($to)){
			$from  	= mktime(0,0,0,date("m",strtotime($from)),date("d",strtotime($from))+1,date("Y",strtotime($from)));
			$from	=	date("d-m-Y", $from);
			$format = str_replace("-","/", $from);
			$hari		= hari($format);
			if(! in_array($hari, $list_kerja)){
				$hasil[$format][] = "Hari ".$hari." bukan hari kerja.";
			}
			$parameter	= array('api_kode' => 1000, 'api_subkode' => 3, 'api_search' => array($from));
			$tgl_libur 	= $ci->mdl_tnde->api_ikd('ikd_admin/Lihat_hari_lbur', 'json', 'POST', $parameter);
			if(!empty($tgl_libur)){
				foreach($tgl_libur as $val){
					$tgl_fix	= str_replace("-","/",$val['TANGGAL']);
					$hasil[$tgl_fix][] = huruf_kapital($val['KETERANGAN']);
				}
			}
		}
		return $hasil;
	}
}
?>