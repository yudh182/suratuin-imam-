<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');
class Mdl_tnde_general extends CI_Model{
	
	function __construct(){
		parent::__construct();
		$this->tnde = $this->load->database('tndeprod',TRUE);
	}
	
	function get_config_surat(){
		$sql = "SELECT TO_CHAR(TGL_MULAI_SURAT, 'DD/MM/YYYY') TGL_MULAI_SURAT, NO_SURAT FROM MD_CONFIG_SURAT";
		return $this->tnde->query($sql)->row_array();
	}
	
	function detail_config_surat_by_kd_kat_penomoran($kd_kat_penomoran=''){
		$sql = "SELECT TO_CHAR(TGL_MULAI_SURAT, 'DD/MM/YYYY') TGL_MULAI_SURAT, NO_SURAT, KD_KAT_PENOMORAN FROM MD_CONFIG_SURAT WHERE KD_KAT_PENOMORAN='".$kd_kat_penomoran."' ";
		return $this->tnde->query($sql)->row_array();
	}
	
	function count_by_table($table){
		return $this->tnde->count_all_results($table);
	}	
	
	function count_by_field_in($table, $field, $data){
		$this->tnde->where_in($field, $data);
		$this->tnde->from($table);
		return $this->tnde->count_all_results();
	}
	
	function get_data($table=''){
		$query = $this->tnde->get($table);
		$hasil = $query->result();
		
		return $hasil;
	}
	
	function get_data_by_field_array($table, $where){
		$query = $this->tnde->get_where($table, $where);
		return $query->result();
	}
	
	function insert_data($table, $data){
		$this->tnde->insert($table, $data);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function update_data($table, $data, $where){
		$this->tnde->update($table, $data, $where);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function delete_data($table, $where){
		$this->tnde->delete($table, $where);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}	
	
	function delete_data_in($table, $field, $in){
		$this->tnde->where_in($field, $in);
		$this->tnde->delete($table);
		// $par = array();
		// if(!empty($in)){
			// foreach($in as $val){
				// $par[] = "'".$val."'";
			// }
		// }
		// $data = implode(",", $par);
		// $sql = "DELETE FROM ".$table." WHERE ".$field." IN(".$data.")";
		// $this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function order_data($table, $where, $order){
		$this->tnde->from($table);
		$this->tnde->where($where);
		$this->tnde->order_by(key($order), $order[key($order)]);
		$query = $this->tnde->get();
		return $query->row_array();
	}	
	
	function order_data_result($table, $where, $order){
		$this->tnde->from($table);
		$this->tnde->where($where);
		$this->tnde->order_by(key($order), $order[key($order)]);
		$query = $this->tnde->get();
		return $query->result();
	}
	
	function pegawai_by_nip($nip){
		$query = $this->tnde->query("SELECT A.NIP, A.NM_PEGAWAI, A.KD_JABATAN, A.KD_TU_BAGIAN, A.KD_BAGIAN, B.NM_JABATAN, C.NM_BAGIAN
		FROM D_PEGAWAI A, MD_JABATAN B, MD_BAGIAN C WHERE A.NIP='".$nip."' AND B.KD_JABATAN=A.KD_JABATAN AND C.KD_BAGIAN=A.KD_BAGIAN ");
		return $query->row_array();
	}
	
	function pegawai_by_kd_pegawai($kd_pegawai){
		$query = $this->tnde->query("SELECT A.KD_PEGAWAI, A.NIP, A.NM_PEGAWAI, A.KD_JABATAN, A.KD_TU_BAGIAN, A.KD_BAGIAN, B.NM_JABATAN, B.MEMBAWAHI, C.NM_BAGIAN, D.NM_TU_BAGIAN, D.KD_TU_PENOMORAN
		FROM D_PEGAWAI A, MD_JABATAN B, MD_BAGIAN C, MD_TU_BAGIAN D WHERE UPPER(A.KD_PEGAWAI)='".$kd_pegawai."' AND B.KD_JABATAN=A.KD_JABATAN AND C.KD_BAGIAN=A.KD_BAGIAN AND D.KD_TU_BAGIAN=A.KD_TU_BAGIAN");
		return $query->row_array();
	}
	
	function user_by_nip($nip){
		$data['NIP'] = $nip;
		$query = $this->tnde->get_where('D_USER_TNDE',$data);
		return $query->row_array();
	}
	
	function user_by_kd_pegawai($kd_pegawai){
		$data['KD_PEGAWAI'] = $kd_pegawai;
		$query = $this->tnde->get_where('D_USER_TNDE',$data);
		return $query->row_array();
	}
	
	function get_data_by_field($table,$field,$value){
		$data[$field] = $value;
		$query = $this->tnde->get_where($table,$data);
		return $query->row_array();
	}
	
	function get_data_by_field2($table,$field,$value){
		$data[$field] = $value;
		$query = $this->tnde->get_where($table,$data);
		return $query->result();
	}
	
	function get_data_by_field3($table,$data){
		$query = $this->tnde->get_where($table,$data);
		return $query->row_array();
	}
	
	function get_data_by_field_like($table, $field, $value){
		$sql = "SELECT * FROM ".$table." WHERE UPPER(".$field.") LIKE UPPER('%".$value."%')";
		return $this->tnde->query($sql)->result();
	}
	
	function get_data_by_field_in($table, $field, $value){
		$this->tnde->where_in($field, $value);
		$this->tnde->from($table);
		return $this->tnde->get()->result();
	}
	
	function get_data_by_field_not_in($table, $field, $value){
		$this->tnde->where_not_in($field, $value);
		$this->tnde->from($table);
		return $this->tnde->get()->result();
	}
	
	function count_data($table){
		return $this->tnde->count_all_results($table);
	}
	
	function count_data_by_field($table,$field,$value){
		$this->tnde->where($field, $value);
		$this->tnde->from($table);
		return $this->tnde->count_all_results();
	}
	
	function delete_by_field_id($table, $field, $value){
		$data[$field] = $value;
		$this->tnde->delete($table,$data);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function notif_dm_by_nip($nip){
		$data['PENERIMA_SURAT'] = $nip;
		$data['KD_STATUS_DISTRIBUSI'] = 'D';
		$data['ID_STATUS_SM'] = 1;
		$this->tnde->where($data);
		$this->tnde->from('D_DISTRIBUSI_SURAT');
		return $this->tnde->count_all_results();
	}
	
	function notif_dm_by_kd_pegawai($kd_pegawai){
		$data['PENERIMA_SURAT'] = $kd_pegawai;
		$data['KD_STATUS_DISTRIBUSI'] = 'D';
		$data['ID_STATUS_SM'] = 1;
		$this->tnde->where($data);
		$this->tnde->from('D_DISTRIBUSI_SURAT');
		return $this->tnde->count_all_results();
	}
	
	function notif_dm_by_kd_jabatan($kd_jabatan){
		$data['JABATAN_PENERIMA'] = $kd_jabatan;
		$data['KD_STATUS_DISTRIBUSI'] = 'D';
		$data['ID_STATUS_SM'] = 1;
		$this->tnde->where($data);
		$this->tnde->from('D_DISTRIBUSI_SURAT');
		return $this->tnde->count_all_results();
	}
	
	function notif_sm_by_kd_tu_bagian($kd_tu_bagian){
		$data['KD_TU_BAGIAN'] = $kd_tu_bagian;
		$data['ID_STATUS_SM'] = 1;
		$this->tnde->where($data);
		$this->tnde->from('MD_SURAT_MASUK');
		return $this->tnde->count_all_results();
	}
	
	function notif_sm_by_kd_jabatan($kd_jabatan){
		$data['JABATAN_PENERIMA'] = $kd_jabatan;
		$data['KD_STATUS_DISTRIBUSI'] = 'PS';
		$data['ID_STATUS_SM'] = 1;
		$this->tnde->where($data);
		$this->tnde->from('D_DISTRIBUSI_SURAT');
		return $this->tnde->count_all_results();
	}
	
	function notif_ts_by_kd_jabatan($kd_jabatan){
		$data['JABATAN_PENERIMA'] = $kd_jabatan;
		$data['KD_STATUS_DISTRIBUSI'] = 'TS';
		$data['ID_STATUS_SM'] = 1;
		$this->tnde->where($data);
		$this->tnde->from('D_DISTRIBUSI_SURAT');
		return $this->tnde->count_all_results();
	}
	
	function notif_tm_by_kd_pegawai($kd_pegawai){
		$data['PENERIMA_SURAT'] = $kd_pegawai;
		$data['ID_STATUS_SM'] = 1;
		$data['KD_STATUS_DISTRIBUSI'] = 'TM';
		$this->tnde->where($data);
		$this->tnde->from('D_DISTRIBUSI_SURAT');
		return $this->tnde->count_all_results();
	}
	
	function notif_tm_by_kd_jabatan($kd_jabatan){
		$data['JABATAN_PENERIMA'] = $kd_jabatan;
		$data['ID_STATUS_SM'] = 1;
		$data['KD_STATUS_DISTRIBUSI'] = 'TM';
		$this->tnde->where($data);
		$this->tnde->from('D_DISTRIBUSI_SURAT');
		return $this->tnde->count_all_results();
	}
	
	function notif_surat_terkait($kd_pegawai='', $kd_jabatan='', $id_status_sm='1'){
		$sql = "SELECT COUNT(A.ID_SURAT_KELUAR) TOTAL FROM D_PENERIMA_SK A, MD_SURAT_KELUAR2 B 
					WHERE ((A.PENERIMA_SURAT='".$kd_pegawai."' AND A.KD_GRUP_TUJUAN='PGW01') OR (A.JABATAN_PENERIMA='".$kd_jabatan."' AND A.KD_GRUP_TUJUAN='PJB01')) 
									AND B.ID_SURAT_KELUAR=A.ID_SURAT_KELUAR AND B.KD_STATUS_SIMPAN='3' AND A.ID_STATUS_SM='".$id_status_sm."' ";
		return $this->tnde->query($sql)->row_array();
	}
	
	function act_query($sql){
		return $this->tnde->query($sql)->result();
	}
	
	function act_query2($sql){
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			$hasil[0]['result'] = "BERHASIL MEMPERBAHARUI <b>".$this->tnde->affected_rows()."</b> DATA";
			return $hasil;
		}else{
			$hasil[0]['result'] = "GAGAL MEMPERBAHARUI DATA";
			return $hasil;
		}
	}
	
	function get_md_jabatan_by_nm_jabatan($data){
		$sql = "SELECT * FROM MD_JABATAN WHERE UPPER(NM_JABATAN) LIKE '%".$data."%'";
		return $this->tnde->query($sql)->result();
	}
	
	function update_status_sm_by_id_surat_masuk($id_surat_masuk, $data){
		$sql = "UPDATE MD_SURAT_MASUK SET ID_STATUS_SM='".$data['ID_STATUS_SM']."', PENGARAH_SURAT='".$data['PENGARAH_SURAT']."', JABATAN_PENGARAH='".$data['JABATAN_PENGARAH']."', WAKTU_AKSES=TO_DATE('".$data['WAKTU_AKSES']."', 'DD/MM/YYYY HH24:MI:SS') WHERE ID_SURAT_MASUK='".$id_surat_masuk."' ";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function notif_pengarah_sm($kd_tu_bagian){
		$data['ID_STATUS_SM']	= 1;
		$this->tnde->where($data);
		$this->tnde->where_in('KD_TU_BAGIAN', $kd_tu_bagian);
		$this->tnde->from('MD_SURAT_MASUK');
		return $this->tnde->count_all_results();
	}
	
	function notif_pengarah_sm_v2($unit_id){ #array unit
		if(!empty($unit_id)){
			foreach($unit_id as $val){
				$arr[] = "'".$val."'";
			}
			$unit_id = implode(",", $arr);
		}else{
			$unit_id='';
		}
		
		$sql = "SELECT COUNT(*) TOTAL FROM MD_SURAT_MASUK WHERE UNIT_ID IN(".$unit_id.") AND ID_STATUS_SM='1' ";
		return $this->tnde->query($sql)->row_array();	
	}
	
	function insert_blob($table='', $data='', $where){
		$col_id		= key($where);
		$val_id			= $where[$col_id];
		$blob_col	= key($data);
		$blob_val		= $data[$blob_col];
		$sql_cek	= "SELECT COUNT(*) TOTAL FROM ".$table." WHERE ".$col_id."='".$val_id."'";
		$cek		= $this->tnde->query($sql_cek)->row_array();
		$where__ = array(
					array('method' => 'IN', 'name' => ':kodeb', 'value' => $val_id, 'length' => 32, 'type' => 'SQLT_CHR'),
				);
		$blob__ = array(
					array('method' => 'IN', 'name' => ':sesuatu', 'value' => $blob_val, 'length' => 0, 'type' => 'BLOB')
				);
		if(empty($cek['TOTAL'])){
			$insert	= $this->insert_data($table, $where);
			if($insert){
				$sql = "UPDATE ".$table." SET ".$blob_col." = EMPTY_BLOB() WHERE ".$col_id." = :kodeb RETURNING ".$blob_col." INTO :sesuatu";
				$act = $this->tnde->call_blob($sql, $where__, $blob__);
				$hasil = $act['result'];
				#$hasil = array($act['result'], '#2');
			}else{
				$hasil = FALSE;
				#$hasil = array($act['result'], '#1');
			}
		}else{
				$sql = "UPDATE ".$table." SET ".$blob_col." = EMPTY_BLOB() WHERE ".$col_id." = :kodeb RETURNING ".$blob_col." INTO :sesuatu";
				$act = $this->tnde->call_blob($sql, $where__, $blob__);
				$hasil = $act['result'];
				#$hasil = array($act['result'], '#0');
		}
		return $hasil;
	}
	
	function get_blob($table, $blob_col, $where){
		$col_id		= key($where);
		$val_id			= $where[$col_id];
		$sql			= "SELECT ".$blob_col." FROM ".$table." WHERE ".$col_id."='".$val_id."'";
		$hasil		= $this->tnde->query($sql)->row_array();
		// return strlen($hasil[$blob_col]->load());
		return $hasil[$blob_col]->load();
		// return base64_encode(2);
	}
	
	function get_data_by_par($table='', $where='', $order=''){
		$this->tnde->from($table);
		if(!empty($where)){
			$this->tnde->where($where);
		}
		if(!empty($order)){
			$this->tnde->order_by($order);
		}
		$data	= $this->tnde->get();
		return $data->result();
	}
}
?>