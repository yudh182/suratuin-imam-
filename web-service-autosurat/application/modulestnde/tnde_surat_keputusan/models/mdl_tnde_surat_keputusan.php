<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');
class Mdl_tnde_surat_keputusan extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->tnde = $this->load->database('tnde', TRUE);
	}
	
	function tot_surat_keputusan_in($data){
		$sql = "SELECT COUNT(*) TOTAL FROM MD_SURAT_KELUAR2 WHERE KD_JENIS_SURAT='1' AND KD_TU_BAGIAN IN(".$data.")";
		return $this->tnde->query($sql)->row_array();
	}	
	
	function tot_surat_keputusan_in_v2($unit, $status_simpan){
		$sql = "SELECT COUNT(*) TOTAL FROM MD_SURAT_KELUAR2 WHERE KD_JENIS_SURAT='1' AND UNIT_ID IN(".$unit.") AND KD_STATUS_SIMPAN IN(".$status_simpan.")";
		return $this->tnde->query($sql)->row_array();
	}
	
	function tot_surat_keputusan_in_q($data, $q){
		$sql = "SELECT COUNT(*) TOTAL FROM MD_SURAT_KELUAR2 WHERE KD_JENIS_SURAT='1' AND KD_TU_BAGIAN IN(".$data.") AND 
						(UPPER(NO_SURAT) LIKE '%".$q."%' OR TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') LIKE '%".$q."%' OR 
						 UPPER(PERIHAL) LIKE '%".$q."%' OR UPPER(ISI_SURAT) LIKE '%".$q."%')";
		return $this->tnde->query($sql)->row_array();
	}	
	
	function tot_surat_keputusan_in_q_v2($data, $q){
		$sql = "SELECT COUNT(*) TOTAL FROM MD_SURAT_KELUAR2 WHERE KD_JENIS_SURAT='1' AND UNIT_ID IN(".$data.") AND 
						(UPPER(NO_SURAT) LIKE '%".$q."%' OR TO_CHAR(TGL_SURAT, 'DD/MM/YYYY') LIKE '%".$q."%' OR 
						 UPPER(PERIHAL) LIKE '%".$q."%' OR UPPER(ISI_SURAT) LIKE '%".$q."%')";
		return $this->tnde->query($sql)->row_array();
	}
	
	function get_surat_keputusan_in_limit($data, $limit){
		$sql = "SELECT ID_SURAT_KELUAR, TGL_SURAT, PERIHAL, STATUS_SK, PEMBUAT_SURAT, JABATAN_PEMBUAT, 
						KD_JENIS_SURAT, NO_SELA, NO_SURAT, WAKTU_SIMPAN, KD_TU_BAGIAN, KD_STATUS_SIMPAN, ROWNUM R 
				FROM(SELECT A.ID_SURAT_KELUAR, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT, A.PERIHAL, A.STATUS_SK, 
						A.PEMBUAT_SURAT, A.JABATAN_PEMBUAT, A.KD_JENIS_SURAT, A.NO_SELA, A.NO_SURAT, 
						TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_SIMPAN, A.KD_TU_BAGIAN, A.KD_STATUS_SIMPAN 
					 FROM MD_SURAT_KELUAR2 A
					 WHERE A.KD_JENIS_SURAT='1' AND A.KD_TU_BAGIAN IN(".$data.") ORDER BY A.WAKTU_SIMPAN DESC 
					)
				WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_surat_keputusan_in_paging($data, $limit, $page){
		$sql = "SELECT * FROM (SELECT ID_SURAT_KELUAR, TGL_SURAT, PERIHAL, STATUS_SK, PEMBUAT_SURAT, JABATAN_PEMBUAT, 
									KD_JENIS_SURAT, NO_SELA, NO_SURAT, WAKTU_SIMPAN, KD_TU_BAGIAN, KD_STATUS_SIMPAN, ROWNUM R 
							   FROM ( SELECT A.ID_SURAT_KELUAR, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT, A.PERIHAL, A.STATUS_SK, A.PEMBUAT_SURAT, A.JABATAN_PEMBUAT, 
											 A.KD_JENIS_SURAT, A.NO_SELA, A.NO_SURAT, TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_SIMPAN, A.KD_TU_BAGIAN, A.KD_STATUS_SIMPAN
									  FROM MD_SURAT_KELUAR2 A
									  WHERE A.KD_JENIS_SURAT='1' AND A.KD_TU_BAGIAN IN(".$data.") ORDER BY A.WAKTU_SIMPAN DESC 
									)
							   WHERE ROWNUM <= ".$limit." 
							   )
						  WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_surat_keputusan_in_limit_v2($unit, $status_simpan, $limit){
		$sql = "SELECT ID_SURAT_KELUAR, TGL_SURAT, PERIHAL, STATUS_SK, PEMBUAT_SURAT, JABATAN_PEMBUAT, 
						KD_JENIS_SURAT, NO_SELA, NO_SURAT, WAKTU_SIMPAN, UNIT_ID, KD_STATUS_SIMPAN, UNTUK_UNIT, ROWNUM R 
				FROM(SELECT A.ID_SURAT_KELUAR, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT, A.PERIHAL, A.STATUS_SK, 
						A.PEMBUAT_SURAT, A.JABATAN_PEMBUAT, A.KD_JENIS_SURAT, A.NO_SELA, A.NO_SURAT, A.UNTUK_UNIT, 
						TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_SIMPAN, A.UNIT_ID, A.KD_STATUS_SIMPAN 
					 FROM MD_SURAT_KELUAR2 A
					 WHERE A.KD_JENIS_SURAT='1' AND A.UNIT_ID IN(".$unit.") AND A.KD_STATUS_SIMPAN IN(".$status_simpan.") ORDER BY A.WAKTU_SIMPAN DESC 
					)
				WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_surat_keputusan_in_paging_v2($unit, $status_simpan, $limit, $page){
		$sql = "SELECT * FROM (SELECT ID_SURAT_KELUAR, TGL_SURAT, PERIHAL, STATUS_SK, PEMBUAT_SURAT, JABATAN_PEMBUAT, 
									KD_JENIS_SURAT, NO_SELA, NO_SURAT, WAKTU_SIMPAN, UNIT_ID, KD_STATUS_SIMPAN, UNTUK_UNIT, ROWNUM R 
							   FROM ( SELECT A.ID_SURAT_KELUAR, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT, A.PERIHAL, A.STATUS_SK, A.PEMBUAT_SURAT, A.JABATAN_PEMBUAT, A.UNTUK_UNIT,  
											 A.KD_JENIS_SURAT, A.NO_SELA, A.NO_SURAT, TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_SIMPAN, A.UNIT_ID, A.KD_STATUS_SIMPAN
									  FROM MD_SURAT_KELUAR2 A
									  WHERE A.KD_JENIS_SURAT='1' AND A.UNIT_ID IN(".$unit.") AND A.KD_STATUS_SIMPAN IN(".$status_simpan.") ORDER BY A.WAKTU_SIMPAN DESC 
									)
							   WHERE ROWNUM <= ".$limit." 
							   )
						  WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_surat_keputusan_in_q_limit($data, $q, $limit){
		$sql = "SELECT ID_SURAT_KELUAR, TGL_SURAT, PERIHAL, STATUS_SK, PEMBUAT_SURAT, JABATAN_PEMBUAT, 
						KD_JENIS_SURAT, NO_SELA, NO_SURAT, WAKTU_SIMPAN, KD_TU_BAGIAN, KD_STATUS_SIMPAN, ROWNUM R 
				FROM(SELECT A.ID_SURAT_KELUAR, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT, A.PERIHAL, A.STATUS_SK, 
						A.PEMBUAT_SURAT, A.JABATAN_PEMBUAT, A.KD_JENIS_SURAT, A.NO_SELA, A.NO_SURAT, 
						TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_SIMPAN, A.KD_TU_BAGIAN, A.KD_STATUS_SIMPAN
					 FROM MD_SURAT_KELUAR2 A
					 WHERE A.KD_JENIS_SURAT='1' AND A.KD_TU_BAGIAN IN(".$data.") AND 
							(UPPER(A.NO_SURAT) LIKE '%".$q."%' OR TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') LIKE '%".$q."%' OR 
							UPPER(A.PERIHAL) LIKE '%".$q."%' OR UPPER(A.ISI_SURAT) LIKE '%".$q."%')
							ORDER BY A.WAKTU_SIMPAN DESC 
					)
				WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_surat_keputusan_in_q_paging($data, $q, $limit, $page){
		$sql = "SELECT * FROM (SELECT ID_SURAT_KELUAR, TGL_SURAT, PERIHAL, STATUS_SK, PEMBUAT_SURAT, JABATAN_PEMBUAT, 
									KD_JENIS_SURAT, NO_SELA, NO_SURAT, WAKTU_SIMPAN, KD_TU_BAGIAN, KD_STATUS_SIMPAN, ROWNUM R 
							   FROM ( SELECT A.ID_SURAT_KELUAR, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT, A.PERIHAL, A.STATUS_SK, A.PEMBUAT_SURAT, A.JABATAN_PEMBUAT, 
											 A.KD_JENIS_SURAT, A.NO_SELA, A.NO_SURAT, TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_SIMPAN, A.KD_TU_BAGIAN, A.KD_STATUS_SIMPAN
									  FROM MD_SURAT_KELUAR2 A
									  WHERE A.KD_JENIS_SURAT='1' AND A.KD_TU_BAGIAN IN(".$data.") AND 
											(UPPER(A.NO_SURAT) LIKE '%".$q."%' OR TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') LIKE '%".$q."%' OR 
											 UPPER(A.PERIHAL) LIKE '%".$q."%' OR UPPER(A.ISI_SURAT) LIKE '%".$q."%') 
											ORDER BY A.WAKTU_SIMPAN DESC 
									)
							   WHERE ROWNUM <= ".$limit." 
							   )
						  WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_surat_keputusan_in_q_limit_v2($unit, $status_simpan, $q, $limit){
		$sql = "SELECT ID_SURAT_KELUAR, TGL_SURAT, PERIHAL, STATUS_SK, PEMBUAT_SURAT, JABATAN_PEMBUAT, UNTUK_UNIT, 
						KD_JENIS_SURAT, NO_SELA, NO_SURAT, WAKTU_SIMPAN, UNIT_ID, KD_STATUS_SIMPAN, ROWNUM R 
				FROM(SELECT A.ID_SURAT_KELUAR, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT, A.PERIHAL, A.STATUS_SK, 
						A.PEMBUAT_SURAT, A.JABATAN_PEMBUAT, A.KD_JENIS_SURAT, A.NO_SELA, A.NO_SURAT, A.UNTUK_UNIT, 
						TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_SIMPAN, A.UNIT_ID, A.KD_STATUS_SIMPAN
					 FROM MD_SURAT_KELUAR2 A
					 WHERE A.KD_JENIS_SURAT='1' AND A.UNIT_ID IN(".$unit.") AND A.KD_STATUS_SIMPAN IN(".$status_simpan.") AND 
							(UPPER(A.NO_SURAT) LIKE '%".$q."%' OR TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') LIKE '%".$q."%' OR 
							UPPER(A.PERIHAL) LIKE '%".$q."%' OR UPPER(A.ISI_SURAT) LIKE '%".$q."%')
							ORDER BY A.WAKTU_SIMPAN DESC 
					)
				WHERE ROWNUM <= ".$limit."";
		return $this->tnde->query($sql)->result();
	}
	
	function get_surat_keputusan_in_q_paging_v2($unit, $status_simpan, $q, $limit, $page){
		$sql = "SELECT * FROM (SELECT ID_SURAT_KELUAR, TGL_SURAT, PERIHAL, STATUS_SK, PEMBUAT_SURAT, JABATAN_PEMBUAT, 
									KD_JENIS_SURAT, NO_SELA, NO_SURAT, WAKTU_SIMPAN, UNIT_ID, KD_STATUS_SIMPAN, UNTUK_UNIT, ROWNUM R 
							   FROM ( SELECT A.ID_SURAT_KELUAR, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT, A.PERIHAL, A.STATUS_SK, A.PEMBUAT_SURAT, A.JABATAN_PEMBUAT, A.UNTUK_UNIT, 
											 A.KD_JENIS_SURAT, A.NO_SELA, A.NO_SURAT, TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_SIMPAN, A.UNIT_ID, A.KD_STATUS_SIMPAN
									  FROM MD_SURAT_KELUAR2 A
									  WHERE A.KD_JENIS_SURAT='1' AND A.UNIT_ID IN(".$unit.") AND A.KD_STATUS_SIMPAN IN(".$status_simpan.") AND 
											(UPPER(A.NO_SURAT) LIKE '%".$q."%' OR TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') LIKE '%".$q."%' OR 
											 UPPER(A.PERIHAL) LIKE '%".$q."%' OR UPPER(A.ISI_SURAT) LIKE '%".$q."%') 
											ORDER BY A.WAKTU_SIMPAN DESC 
									)
							   WHERE ROWNUM <= ".$limit." 
							   )
						  WHERE R > ".$page."";
		return $this->tnde->query($sql)->result();
	}
	
	function detail_surat_keputusan($id_surat_keluar){
		$sql = "SELECT A.ID_SURAT_KELUAR, A.PERIHAL, TO_CHAR(A.TGL_SURAT, 'DD/MM/YYYY') TGL_SURAT, A.NO_SURAT, A.UNIT_ID, A.ID_PSD, 
					   A.KD_TU_BAGIAN, A.ATAS_NAMA, A.KD_JENIS_SURAT, A.TEMPAT_DIBUAT, A.LAMPIRAN, A.KD_STATUS_SIMPAN, A.KOTA_TUJUAN, 
					   TO_CHAR(A.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_SIMPAN, A.ISI_SURAT, A.NO_URUT, A.NO_SELA, A.UNTUK_BELIAU, 
					   A.UNTUK_UNIT, B.NM_PSD, B.KD_JABATAN KD_JABATAN_PSD, D.NM_JENIS_SURAT, E.NM_STATUS_SIMPAN
				FROM MD_SURAT_KELUAR2 A, MD_PSD B, MD_JENIS_SURAT D, MD_STATUS_SIMPAN E
				WHERE A.ID_SURAT_KELUAR='".$id_surat_keluar."' AND B.ID_PSD=A.ID_PSD 
					  AND D.KD_JENIS_SURAT=A.KD_JENIS_SURAT AND E.KD_STATUS_SIMPAN=A.KD_STATUS_SIMPAN";
		return $this->tnde->query($sql)->row_array();
	}
	
	function update_surat_keputusan($id_surat_keluar, $data){
		$untuk_unit	= (!empty($data['UNTUK_UNIT']) ? $data['UNTUK_UNIT'] : '');
		$sql = "UPDATE MD_SURAT_KELUAR2 SET ID_PSD='".$data['ID_PSD']."', ATAS_NAMA='".$data['ATAS_NAMA']."', KD_JENIS_SURAT='".$data['KD_JENIS_SURAT']."', 
				NO_URUT='".$data['NO_URUT']."', NO_SELA='".$data['NO_SELA']."', NO_SURAT='".$data['NO_SURAT']."', KD_STATUS_SIMPAN='".$data['KD_STATUS_SIMPAN']."',
				PERIHAL='".$data['PERIHAL']."', LAMPIRAN='".$data['LAMPIRAN']."', UNTUK_BELIAU='".$data['UNTUK_BELIAU']."', TEMPAT_DIBUAT='".$data['TEMPAT_DIBUAT']."', UNTUK_UNIT='".$untuk_unit."', WAKTU_SIMPAN=TO_DATE('".$data['WAKTU_SIMPAN']."', 'DD/MM/YYYY HH24:MI:SS')  
				WHERE ID_SURAT_KELUAR='".$id_surat_keluar."'";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function get_detail_pertimbangan($id_surat_keluar='', $kd_pertimbangan=''){
		$sql = "SELECT A.*, B.* FROM D_DETAIL_PERTIMBANGAN A, MD_PERTIMBANGAN B WHERE A.ID_SURAT_KELUAR='".$id_surat_keluar."' AND 
					A.KD_PERTIMBANGAN='".$kd_pertimbangan."' AND B.KD_PERTIMBANGAN=A.KD_PERTIMBANGAN ORDER BY A.NO_URUT ASC";
		return $this->tnde->query($sql)->result();
	}
	
	function get_anggaran_like($q=''){
		$q = strtoupper($q);
		$sql = "SELECT A.*, B.* FROM MD_ANGGARAN A, MD_THN_ANGGARAN B WHERE B.KD_THN_ANGGARAN=A.KD_THN_ANGGARAN AND (UPPER(A.KD_ANGGARAN) 
					LIKE '%".$q."%' OR UPPER(A.NM_ANGGARAN) LIKE '%".$q."%' OR B.TAHUN_ANGGARAN LIKE '%".$q."%')";
		return $this->tnde->query($sql)->result();
	}
	
	function get_d_sk_anggaran($id_surat_keluar=''){
		$sql = "SELECT A.*, B.*, C.* FROM D_SK_ANGGARAN A, MD_ANGGARAN B, MD_THN_ANGGARAN C WHERE A.ID_SURAT_KELUAR='".$id_surat_keluar."' AND 
		B.KD_ANGGARAN=A.KD_ANGGARAN AND C.KD_THN_ANGGARAN=B.KD_THN_ANGGARAN";
		return $this->tnde->query($sql)->result();
	}
	
	function get_data_lampiran($id_surat_keluar=''){
		$sql = "SELECT * FROM DATA_LAMPIRAN WHERE ID_SURAT_KELUAR='".$id_surat_keluar."' ORDER BY NO_URUT ASC";
		return $this->tnde->query($sql)->result();
	}
	
	function update_simple_surat_keputusan($data, $id_surat_keluar){
		$sql = "UPDATE MD_SURAT_KELUAR2 SET NO_URUT='".$data['NO_URUT']."', NO_SELA='".$data['NO_SELA']."', NO_SURAT='".$data['NO_SURAT']."', KD_STATUS_SIMPAN='".$data['KD_STATUS_SIMPAN']."', WAKTU_SIMPAN=TO_DATE('".$data['WAKTU_SIMPAN']."', 'DD/MM/YYYY HH24:MI:SS') WHERE ID_SURAT_KELUAR='".$id_surat_keluar."' ";
		$this->tnde->query($sql);
		if($this->tnde->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
}
?>