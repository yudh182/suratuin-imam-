<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');
class Mdl_tnde_config extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->tnde	=	$this->load->database('tnde', TRUE);
	}
	
	function config_penomoran_by_kd_tu_bagian($kd_kat_penomoran='', $kd_tu_bagian=''){
		$sql = "SELECT TO_CHAR(A.TGL_MULAI, 'DD/MM/YYYY') TGL_MULAI, A.NO_SURAT, A.KD_KAT_PENOMORAN, A.STATUS_PENOMORAN, B.*
					FROM CONFIG_PENOMORAN A, MD_TU_BAGIAN B
					WHERE B.KD_TU_BAGIAN='".$kd_tu_bagian."' AND A.KD_TU_PENOMORAN=B.KD_TU_PENOMORAN AND A.KD_KAT_PENOMORAN='".$kd_kat_penomoran."' ";
		return $this->tnde->query($sql)->row_array();
	}	
	
	function config_penomoran_by_unit_id($kd_kat_penomoran='', $unit_id=''){
		$sql = "SELECT TO_CHAR(A.TGL_MULAI, 'DD/MM/YYYY') TGL_MULAI, A.NO_SURAT, A.KD_KAT_PENOMORAN, A.STATUS_PENOMORAN, B.*
					FROM CONFIG_PENOMORAN A, MD_KAT_PENOMORAN B
					WHERE A.UNIT_ID='".$unit_id."' AND A.KD_KAT_PENOMORAN='".$kd_kat_penomoran."' AND B.KD_KAT_PENOMORAN=A.KD_KAT_PENOMORAN";
		return $this->tnde->query($sql)->row_array();
	}
	
	function config_penomoran_by_unit_id2($tgl, $unit_id, $kd_kat_penomoran){
		$sql = "SELECT TO_CHAR(A.TGL_MULAI, 'DD/MM/YYYY') TGL_MULAI, A.NO_SURAT, A.KD_KAT_PENOMORAN, A.STATUS_PENOMORAN, B.*
					FROM CONFIG_PENOMORAN A, MD_KAT_PENOMORAN B
					WHERE A.UNIT_ID='".$unit_id."' AND A.KD_KAT_PENOMORAN='".$kd_kat_penomoran."' AND A.TGL_MULAI >= TO_DATE('".$tgl."', 'DD/MM/YYYY') AND B.KD_KAT_PENOMORAN=A.KD_KAT_PENOMORAN";
		return $this->tnde->query($sql)->row_array();
	}
	
	function config_penomoran($par=''){
		$tgl_surat			= (!empty($par['TANGGAL']) ? $par['TANGGAL'] : date('d/m/Y'));
		$unit_id 				= (!empty($par['UNIT_ID']) ? $par['UNIT_ID'] : '');
		$kd_jenis_surat 	= (!empty($par['KD_JENIS_SURAT']) ? $par['KD_JENIS_SURAT'] : '');
		$sql = "SELECT A.KD_JAB_SISTEM, A.UNIT_ID, A.KD_JENIS_SURAT, B.ID_HIS_JAB_SK, B.GMU_SLUG, TO_CHAR(B.BERLAKU_DARI, 'DD/MM/YYYY') BERLAKU_DARI, TO_CHAR(B.BERLAKU_SAMPAI, 'DD/MM/YYYY') BERLAKU_SAMPAI, TO_CHAR(B.WAKTU_SIMPAN, 'DD/MM/YYYY HH24:MI:SS') WAKTU_SIMPAN, B.KD_PGW, B.STATUS_PENOMORAN, B.NO_REGISTER FROM D_JAB_SK A, HIS_JAB_SK B WHERE A.UNIT_ID='".$unit_id."' AND A.KD_JENIS_SURAT='".$kd_jenis_surat."' AND B.KD_JAB_SISTEM=A.KD_JAB_SISTEM AND ((B.BERLAKU_DARI <= TO_DATE('".$tgl_surat."', 'DD/MM/YYYY') AND B.BERLAKU_SAMPAI >= TO_DATE('".$tgl_surat."', 'DD/MM/YYYY')) OR (B.BERLAKU_DARI <= TO_DATE('".$tgl_surat."', 'DD/MM/YYYY') AND B.BERLAKU_SAMPAI IS NULL))";
		return $this->tnde->query($sql)->row_array();
	}
}
?>