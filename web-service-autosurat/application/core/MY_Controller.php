<?php
/**
* Custom Base Controller untuk Modularity dan Implementasi JSON Web Token
* @link https://bitbucket.org/wiredesignz/codeigniter-modular-extensions-hmvc
* @link https://github.com/firebase/php-jwt
*/

class MY_Controller extends MX_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->module('Template');
		
	}
	function home_run()
	{
		$this->template->call_admin_template();
	}
}
